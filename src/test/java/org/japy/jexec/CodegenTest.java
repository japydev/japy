package org.japy.jexec;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import org.japy.ErrorHelper;
import org.japy.TestFiles;
import org.japy.compiler.CompilerConfig;
import org.japy.compiler.PackageCompiler;
import org.japy.compiler.code.CBlock;
import org.japy.compiler.code.CPackage;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.validation.Debug;
import org.japy.jexec.codegen.CodegenConfig;
import org.japy.jexec.pkg.JPackage;
import org.japy.kernel.exec.OptimizationLevel;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CodegenTest {
  private static void test(JPackage pkg, CBlock block) {
    try {
      @SuppressWarnings("unused")
      Class<?> cls = pkg.getBlockClass(block.binName);
    } catch (Throwable ex) {
      if (ex instanceof IHasLocation) {
        throw ex;
      }

      throw new InternalErrorException(block.binName + ": " + ex.getMessage(), block.location, ex);
    }
  }

  private static void test(Path path) {
    ErrorHelper.wrap(
        () -> {
          CPackage cpkg =
              PackageCompiler.compile(
                  path,
                  CompilerConfig.builder().optimizationLevel(OptimizationLevel.ZERO).build(),
                  null);
          if (!Debug.ENABLED && !Debug.PROFILING_ENABLED) {
            cpkg.validate();
          }
          JPackage jpkg = new JPackage(cpkg, CodegenConfig.builder().sneakyLocals(false).build());
          test(jpkg, cpkg.module);
          Arrays.stream(cpkg.children).forEach(b -> test(jpkg, b));
        },
        path);
  }

  @Test
  public void aTestSmoke() {
    test(Paths.get(TestFiles.COMPILER_FILE_PY));
  }

  @Test
  public void bTestAdHoc() {
    TestFiles.iterateAdhoc("compiler", CodegenTest::test);
  }

  @Test
  public void cTestLib() {
    TestFiles.iterateLibParallel(CodegenTest::test);
  }
}
