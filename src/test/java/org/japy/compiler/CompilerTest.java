package org.japy.compiler;

import static org.japy.infra.coll.CollUtil.last;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import org.japy.parser.python.impl.Scanner;
import org.japy.parser.python.impl.javacc.Token;

public class CompilerTest {
  private static class TokenRepr {
    private final Token token;

    private TokenRepr(Token token) {
      this.token = token;
    }

    @Override
    public String toString() {
      return "(" + Token.tokenImage[token.kind] + ", \"" + token.image + "\")";
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof TokenRepr)) return false;
      TokenRepr tokenRepr = (TokenRepr) o;
      return token.kind == tokenRepr.token.kind && token.image.equals(tokenRepr.token.image);
    }

    @Override
    public int hashCode() {
      return 31 * token.kind + token.image.hashCode();
    }
  }

  private static List<TokenRepr> scan(String text) {
    Scanner scanner = new Scanner(text, "input");
    Token first = scanner.getNextToken();
    Assert.assertEquals(Token.START, first.kind);
    List<TokenRepr> tokens = new ArrayList<>();
    while (true) {
      Token tok = scanner.getNextToken();
      if (tok.kind == Token.EOF) {
        break;
      }
      tokens.add(new TokenRepr(tok));
    }
    Assert.assertFalse(tokens.isEmpty());
    Assert.assertEquals(Token.NEWLINE, last(tokens).token.kind);
    tokens.remove(tokens.size() - 1);
    return tokens;
  }

  private static void check(String string, TokenRepr... expected) {
    List<TokenRepr> actual = scan(string);
    Assert.assertEquals(Arrays.asList(expected), actual);
  }

  private static TokenRepr str(String image) {
    return new TokenRepr(new Token(Token.STRING, image));
  }

  @Test
  public void testStringLiteral() {
    check("\"\\N{EM SPACE}\"", str("\u2003"));
    check("\"\\N{EN SPACE}\"", str("\u2002"));
    check("\"\\N{EM SPACE}-3\\N{EN SPACE}\"", str("\u2003-3\u2002"));
  }
}
