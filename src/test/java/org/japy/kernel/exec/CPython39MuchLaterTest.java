package org.japy.kernel.exec;

import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class CPython39MuchLaterTest extends CPython39TestBase {
  @Test
  public void testArgParse() {
    runTestFile("argparse");
  }

  @Test
  public void testArray() {
    runTestFile("array");
  }

  @Test
  public void testAsyncIO() {
    runTestPackage("asyncio");
  }

  @Test
  public void testBase64() {
    runTestFile("base64");
  }

  @Test
  public void testBinAscii() {
    runTestFile("binascii");
  }

  @Test
  public void testBisect() {
    runTestFile("bisect");
  }

  @Test
  public void testCAPI() {
    runTestFile("capi");
  }

  @Test
  public void testCharMapCodec() {
    runTestFile("charmapcode");
  }

  @Test
  public void testCodecCallbacks() {
    runTestFile("codecallbacks");
  }

  @Test
  public void testCodecEncodingsCN() {
    runTestFile("codecencodings_cn");
  }

  @Test
  public void testCodecEncodingsHK() {
    runTestFile("codecencodings_hk");
  }

  @Test
  public void testCodecEncodingsISO2022() {
    runTestFile("codecencodings_iso2022");
  }

  @Test
  public void testCodecEncodingsJP() {
    runTestFile("codecencodings_jp");
  }

  @Test
  public void testCodecEncodingsKR() {
    runTestFile("codecencodings_kr");
  }

  @Test
  public void testCodecEncodingsTW() {
    runTestFile("codecencodings_tw");
  }

  @Test
  public void testCodecMapsCN() {
    runTestFile("codecmaps_cn");
  }

  @Test
  public void testCodecMapsHK() {
    runTestFile("codecmaps_hk");
  }

  @Test
  public void testCodecMapsJP() {
    runTestFile("codecmaps_jp");
  }

  @Test
  public void testCodecMapsKR() {
    runTestFile("codecmaps_kr");
  }

  @Test
  public void testCodecMapsTW() {
    runTestFile("codecmaps_tw");
  }

  @Test
  public void testContextLib() {
    runTestFile("contextlib");
  }

  @Test
  public void testDateTime() {
    runTestFile("datetime");
  }

  @Test
  public void testDeque() {
    runTestFile("deque");
  }

  @Test
  public void testDiffLib() {
    runTestFile("difflib");
  }

  @Test
  public void testDocTest() {
    runTestFile("doctest");
  }

  @Test
  public void testDocTest2() {
    runTestFile("doctest2");
  }

  @Test
  public void testFileInput() {
    runTestFile("fileinput");
  }

  @Test
  public void testFnMatch() {
    runTestFile("fnmatch");
  }

  @Test
  public void testFuture() {
    runTestFile("future");
  }

  @Test
  public void testFuture5() {
    runTestFile("future5");
  }

  @Test
  public void testGenericPath() {
    runTestFile("genericpath");
  }

  @Test
  public void testGetOpt() {
    runTestFile("getopt");
  }

  @Test
  public void testGetText() {
    runTestFile("gettext");
  }

  @Test
  public void testGlob() {
    runTestFile("glob");
  }

  @Test
  public void testGzip() {
    runTestFile("gzip");
  }

  @Test
  public void testHashLib() {
    runTestFile("hashlib");
  }

  @Test
  public void testHeapq() {
    runTestFile("heapq");
  }

  @Test
  public void testHmac() {
    runTestFile("hmac");
  }

  @Test
  public void testHtml() {
    runTestFile("html");
  }

  @Test
  public void testHtmlParser() {
    runTestFile("htmlparser");
  }

  @Test
  public void testHttpCookieJar() {
    runTestFile("http_cookiejar");
  }

  @Test
  public void testHttpCookies() {
    runTestFile("http_cookies");
  }

  @Test
  public void testImgHdr() {
    runTestFile("imghdr");
  }

  @Test
  public void testInspect() {
    runTestFile("inspect");
  }

  @Test
  public void testIPAddress() {
    runTestFile("ipaddress");
  }

  @Test
  public void testLineCache() {
    runTestFile("linecache");
  }

  @Test
  public void test_Locale() {
    runTestFile("_locale");
  }

  @Test
  public void testLocale() {
    runTestFile("locale");
  }

  @Test
  public void testLogging() {
    runTestFile("logging");
  }

  @Test
  public void testLzma() {
    runTestFile("lzma");
  }

  @Test
  public void testMimeTypes() {
    runTestFile("mimetypes");
  }

  @Test
  public void testMiniDom() {
    runTestFile("minidom");
  }

  @Test
  public void testModuleFinder() {
    runTestFile("modulefinder");
  }

  @Test
  public void testMultibyteCodec() {
    runTestFile("multibytecodec");
  }

  @Test
  public void testOptParse() {
    runTestFile("optparse");
  }

  @Test
  public void testOS() {
    runTestFile("os");
  }

  @Test
  public void testPathLib() {
    runTestFile("pathlib");
  }

  @Test
  public void testPickle() {
    runTestFile("pickle");
  }

  @Test
  public void testPickleBuffer() {
    runTestFile("picklebuffer");
  }

  @Test
  public void testPickleTools() {
    runTestFile("pickletools");
  }

  @Test
  public void testPkgUtil() {
    runTestFile("pkgutil");
  }

  @Test
  public void testPosix() {
    runTestFile("posix");
  }

  @Test
  public void testPPrint() {
    runTestFile("pprint");
  }

  @Test
  public void testPullDom() {
    runTestFile("pulldom");
  }

  @Test
  public void testPwd() {
    runTestFile("pwd");
  }

  @Test
  public void testPyDoc() {
    runTestFile("pydoc");
  }

  @Test
  public void testQueue() {
    runTestFile("queue");
  }

  @Test
  public void testQuoPri() {
    runTestFile("quopri");
  }

  @Test
  public void testRandom() {
    runTestFile("random");
  }

  @Test
  public void testRE() {
    runTestFile("re");
  }

  @Test
  public void testReprLib() {
    runTestFile("reprlib");
  }

  @Test
  public void testRobotParser() {
    runTestFile("robotparser");
  }

  @Test
  public void testRunPy() {
    runTestFile("runpy");
  }

  @Test
  public void testSAX() {
    runTestFile("sax");
  }

  @Test
  public void testSecrets() {
    runTestFile("secrets");
  }

  @Test
  public void testShelve() {
    runTestFile("shelve");
  }

  @Test
  public void testShlex() {
    runTestFile("shlex");
  }

  @Test
  public void testShutil() {
    runTestFile("shutil");
  }

  @Test
  public void testSndHdr() {
    runTestFile("sndhdr");
  }

  @Test
  public void testSourceEncoding() {
    runTestFile("source_encoding");
  }

  @Test
  public void testStat() {
    runTestFile("stat");
  }

  @Test
  public void testStatistics() {
    runTestFile("statistics");
  }

  @Test
  public void testStrftime() {
    runTestFile("strftime");
  }

  @Test
  public void testStringPrep() {
    runTestFile("stringprep");
  }

  @Test
  public void testStrptime() {
    runTestFile("strptime");
  }

  @Test
  public void testStrtod() {
    runTestFile("strtod");
  }

  @Test
  public void testStruct() {
    runTestFile("struct");
  }

  @Test
  public void testStructMembers() {
    runTestFile("structmembers");
  }

  @Test
  public void testStructSeq() {
    runTestFile("structseq");
  }

  @Test
  public void testSubProcess() {
    runTestFile("subprocess");
  }

  @Test
  public void testSupport() {
    runTestFile("support");
  }

  @Test
  public void testTabNanny() {
    runTestFile("tabnanny");
  }

  @Test
  public void testTarFile() {
    runTestFile("tarfile");
  }

  @Test
  public void testTempFile() {
    runTestFile("tempfile");
  }

  @Test
  public void testTextWrap() {
    runTestFile("textwrap");
  }

  @Test
  public void testTime() {
    runTestFile("time");
  }

  @Test
  public void testTimeIt() {
    runTestFile("timeit");
  }

  @Test
  public void testTraceback() {
    runTestFile("traceback");
  }

  @Test
  public void testTypeComments() {
    runTestFile("type_comments");
  }

  @Test
  public void testTyping() {
    runTestFile("typing");
  }

  @Test
  public void testUCN() {
    runTestFile("ucn");
  }

  @Test
  public void testUnicodeFile() {
    runTestFile("unicode_file");
  }

  @Test
  public void testUnicodeFileFunctions() {
    runTestFile("unicode_file_functions");
  }

  @Test
  public void testUnicodeIdentifiers() {
    runTestFile("unicode_identifiers");
  }

  @Test
  public void testUnicodeData() {
    runTestFile("unicodedata");
  }

  @Test
  public void testUnitTest() {
    runTestFile("unittest");
  }

  @Test
  public void testUnpack() {
    runTestFile("unpack");
  }

  @Test
  public void testUnpackEx() {
    runTestFile("unpack_ex");
  }

  @Test
  public void testUrlLib() {
    runTestFile("urllib");
  }

  @Test
  public void testUrlLibResponse() {
    runTestFile("urllib_response");
  }

  @Test
  public void testUrlLibNet() {
    runTestFile("urllibnet");
  }

  @Test
  public void testUrlLib2() {
    runTestFile("urllib2");
  }

  @Test
  public void testUrlLib2LocalNet() {
    runTestFile("urllib2_localnet");
  }

  @Test
  public void testUrlLib2Net() {
    runTestFile("urllib2net");
  }

  @Test
  public void testUrlParse() {
    runTestFile("urlparse");
  }

  @Test
  public void testUu() {
    runTestFile("uu");
  }

  @Test
  public void testUuid() {
    runTestFile("uuid");
  }

  @Test
  public void testWebBrowser() {
    runTestFile("webbrowser");
  }

  @Test
  public void testXmlDomMiniCompat() {
    runTestFile("xml_dom_minicompat");
  }

  @Test
  public void testXmlETree() {
    runTestFile("xml_etree");
  }

  @Test
  public void testXmlETreeC() {
    runTestFile("xml_etree_c");
  }

  @Test
  public void testZipFile() {
    runTestFile("zipfile");
  }

  @Test
  public void testZipFile64() {
    runTestFile("zipfile64");
  }

  @Test
  public void testZipImport() {
    runTestFile("zipimport");
  }

  @Test
  public void testZlib() {
    runTestFile("zlib");
  }

  @Test
  public void testEmail() {
    runTestPackage("email");
  }

  @Test
  public void testImportLib() {
    runTestPackage("importlib");
  }

  @Test
  public void testJson() {
    runTestPackage("json");
  }

  @Test
  public void testWarnings() {
    runTestPackage("warnings");
  }

  @Test
  public void testZoneInfo() {
    runTestPackage("zoneinfo");
  }
}
