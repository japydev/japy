package org.japy.kernel.exec;

import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.KernelTest.LIBDIR_39;

import java.nio.file.Paths;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import org.japy.TestFiles;
import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.Config;
import org.japy.kernel.Kernel;
import org.japy.kernel.KernelTest;

public class CPython39TestBase {
  private static @Nullable Kernel kernel;

  @BeforeClass
  public static void beforeClass() {
    kernel = new Kernel(Config.builder().libDirs(KernelTest.getLibDirs(LIBDIR_39)).build());
  }

  @AfterClass
  public static void afterClass() {
    if (kernel != null) {
      kernel.close();
      kernel = null;
    }
  }

  protected void runTestFile(String name) {
    try (PyContext ctx = new PyContext(dcheckNotNull(kernel), kernel.mainThread)) {
      KernelTest.runFile(
          Paths.get(TestFiles.PYTHON39_LIB_DIR, "test", "test_" + name + ".py"), ctx);
    }
  }

  protected void runTestPackage(String name) {
    try (PyContext ctx = new PyContext(dcheckNotNull(kernel), kernel.mainThread)) {
      throw new NotImplementedException("runTestPackage");
    }
  }
}
