package org.japy.kernel.exec;

import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class CPython39LaterTest extends CPython39TestBase {
  @Test
  public void testAsyncGen() {
    runTestFile("asyncgen");
  }

  @Test
  public void testBigAddrSpace() {
    runTestFile("bigaddrspace");
  }

  @Test
  public void testBigMem() {
    runTestFile("bigmem");
  }

  @Test
  public void testBufIO() {
    runTestFile("bufio");
  }

  @Test
  public void testCall() {
    runTestFile("call");
  }

  @Test
  public void testCMath() {
    runTestFile("cmath");
  }

  @Test
  public void testCodecs() {
    runTestFile("codecs");
  }

  @Test
  public void testCodeOp() {
    runTestFile("codeop");
  }

  @Test
  public void testCompile() {
    runTestFile("compile");
  }

  @Test
  public void testContextLibAsync() {
    runTestFile("contextlib_async");
  }

  @Test
  public void testCoroutines() {
    runTestFile("coroutines");
  }

  @Test
  public void testDecimal() {
    runTestFile("decimal");
  }

  @Test
  public void testEOF() {
    runTestFile("eof");
  }

  @Test
  public void testFile() {
    runTestFile("file");
  }

  @Test
  public void testFileCmp() {
    runTestFile("filecmp");
  }

  @Test
  public void testFileIO() {
    runTestFile("fileio");
  }

  @Test
  public void testFractions() {
    runTestFile("fractions");
  }

  @Test
  public void testFrame() {
    runTestFile("frame");
  }

  @Test
  public void testIO() {
    runTestFile("io");
  }

  @Test
  public void testLargeFile() {
    runTestFile("largefile");
  }

  @Test
  public void testLong() {
    runTestFile("long");
  }

  @Test
  public void testLongExp() {
    runTestFile("longexp");
  }

  @Test
  public void testMath() {
    runTestFile("math");
  }

  @Test
  public void testMemoryIO() {
    runTestFile("memoryio");
  }

  @Test
  public void testMemoryView() {
    runTestFile("memoryview");
  }

  @Test
  public void testNTPath() {
    runTestFile("ntpath");
  }

  @Test
  public void testNumericTower() {
    runTestFile("numeric_tower");
  }

  @Test
  public void testOrderedDict() {
    runTestFile("ordered_dict");
  }

  @Test
  public void testPkg() {
    runTestFile("pkg");
  }

  @Test
  public void testPlatform() {
    runTestFile("platform");
  }

  @Test
  public void testPosixPath() {
    runTestFile("posixpath");
  }

  @Test
  public void testPow() {
    runTestFile("pow");
  }

  @Test
  public void testSys() {
    runTestFile("sys");
  }

  @Test
  public void testUnicode() {
    runTestFile("unicode");
  }

  @Test
  public void testUnivNewLines() {
    runTestFile("univnewlines");
  }

  @Test
  public void testUserDict() {
    runTestFile("userdict");
  }

  @Test
  public void testUserList() {
    runTestFile("userlist");
  }

  @Test
  public void testUserString() {
    runTestFile("userstring");
  }

  @Test
  public void testUtf8Mode() {
    runTestFile("utf8_mode");
  }

  @Test
  public void testUtf8Source() {
    runTestFile("utf8source");
  }

  @Test
  public void testWeakRef() {
    runTestFile("weakref");
  }

  @Test
  public void testWeakSet() {
    runTestFile("weakset");
  }
}
