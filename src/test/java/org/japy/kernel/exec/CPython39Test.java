package org.japy.kernel.exec;

import org.junit.Ignore;
import org.junit.Test;

public class CPython39Test extends CPython39TestBase {
  @Test
  public void testAbc() {
    runTestFile("abc");
  }

  @Test
  public void testAbstractNumbers() {
    runTestFile("abstract_numbers");
  }

  @Test
  public void testAugAssign() {
    runTestFile("augassign");
  }

  @Test
  public void testBaseException() {
    runTestFile("baseexception");
  }

  @Test
  public void testBinOp() {
    runTestFile("binop");
  }

  @Test
  public void testBool() {
    runTestFile("bool");
  }

  @Test
  public void testBuiltin() {
    runTestFile("builtin");
  }

  @Test
  public void testBytes() {
    runTestFile("bytes");
  }

  @Test
  public void testClass() {
    runTestFile("class");
  }

  @Test
  public void testCollections() {
    runTestFile("collections");
  }

  @Test
  public void testCompare() {
    runTestFile("compare");
  }

  @Test
  public void testContains() {
    runTestFile("contains");
  }

  @Test
  public void testContext() {
    runTestFile("context");
  }

  @Test
  public void testDataClasses() {
    runTestFile("dataclasses");
  }

  @Test
  public void testDecorators() {
    runTestFile("decorators");
  }

  @Test
  public void testDefaultDict() {
    runTestFile("defaultdict");
  }

  @Test
  public void testDescr() {
    runTestFile("descr");
  }

  @Test
  public void testDescrTut() {
    runTestFile("descrtut");
  }

  @Test
  public void testDict() {
    runTestFile("dict");
  }

  @Test
  public void testDictComps() {
    runTestFile("dictcomps");
  }

  @Test
  public void testDictViews() {
    runTestFile("dictviews");
  }

  @Test
  public void testDynamic() {
    runTestFile("dynamic");
  }

  @Test
  public void testDynamicClassAttribute() {
    runTestFile("dynamicclassattribute");
  }

  @Test
  public void testEnum() {
    runTestFile("enum");
  }

  @Test
  public void testEnumerate() {
    runTestFile("enumerate");
  }

  @Test
  public void testErrno() {
    runTestFile("errno");
  }

  @Test
  public void testExceptionHierarchy() {
    runTestFile("exception_hierarchy");
  }

  @Test
  public void testExceptionVariations() {
    runTestFile("exception_variations");
  }

  @Test
  public void testExceptions() {
    runTestFile("exceptions");
  }

  @Test
  @Ignore("doctest")
  public void testExtCall() {
    runTestFile("extcall");
  }

  @Test
  public void testFloat() {
    runTestFile("float");
  }

  @Test
  public void testFormat() {
    runTestFile("format");
  }

  @Test
  public void testFString() {
    runTestFile("fstring");
  }

  @Test
  public void testFuncAttrs() {
    runTestFile("funcattrs");
  }

  @Test
  public void testFuncTools() {
    runTestFile("functools");
  }

  @Test
  public void testFuture3() {
    runTestFile("future3");
  }

  @Test
  public void testFuture4() {
    runTestFile("future4");
  }

  @Test
  public void testGeneratorStop() {
    runTestFile("generator_stop");
  }

  @Test
  public void testGenerators() {
    runTestFile("generators");
  }

  @Test
  public void testGenericAlias() {
    runTestFile("genericalias");
  }

  @Test
  public void testGenericClass() {
    runTestFile("genericclass");
  }

  @Test
  public void testGenExps() {
    runTestFile("genexps");
  }

  @Test
  public void testGlobal() {
    runTestFile("global");
  }

  @Test
  public void testGrammar() {
    runTestFile("grammar");
  }

  @Test
  @Ignore("infinite loop")
  public void testGraphLib() {
    runTestFile("graphlib");
  }

  @Test
  public void testHash() {
    runTestFile("hash");
  }

  @Test
  public void testIndex() {
    runTestFile("index");
  }

  @Test
  public void testInt() {
    runTestFile("int");
  }

  @Test
  public void testIntLiteral() {
    runTestFile("int_literal");
  }

  @Test
  public void testIsInstance() {
    runTestFile("isinstance");
  }

  @Test
  public void testIter() {
    runTestFile("iter");
  }

  @Test
  public void testIterLen() {
    runTestFile("iterlen");
  }

  @Test
  public void testIterTools() {
    runTestFile("itertools");
  }

  @Test
  public void testKeywordOnlyArg() {
    runTestFile("keywordonlyarg");
  }

  @Test
  public void testList() {
    runTestFile("list");
  }

  @Test
  @Ignore("doctest")
  public void testListComps() {
    runTestFile("listcomps");
  }

  @Test
  public void testMetaClass() {
    runTestFile("metaclass");
  }

  @Test
  public void testModule() {
    runTestFile("module");
  }

  @Test
  public void testNamedExpressions() {
    runTestFile("named_expressions");
  }

  @Test
  public void testOperator() {
    runTestFile("operator");
  }

  @Test
  public void testPositionalOnlyArg() {
    runTestFile("positional_only_arg");
  }

  @Test
  public void testPrint() {
    runTestFile("print");
  }

  @Test
  public void testProperty() {
    runTestFile("property");
  }

  @Test
  public void testRaise() {
    runTestFile("raise");
  }

  @Test
  public void testRange() {
    runTestFile("range");
  }

  @Test
  public void testRichCmp() {
    runTestFile("richcmp");
  }

  @Test
  public void testScope() {
    runTestFile("scope");
  }

  @Test
  public void testSet() {
    runTestFile("set");
  }

  @Test
  @Ignore("doctest")
  public void testSetComps() {
    runTestFile("setcomps");
  }

  @Test
  public void testSlice() {
    runTestFile("slice");
  }

  @Test
  public void testSort() {
    runTestFile("sort");
  }

  @Test
  public void testString() {
    runTestFile("string");
  }

  @Test
  public void testStringLiterals() {
    runTestFile("string_literals");
  }

  @Test
  public void testSubClassInit() {
    runTestFile("subclassinit");
  }

  @Test
  public void testSuper() {
    runTestFile("super");
  }

  @Test
  public void testSyntax() {
    runTestFile("syntax");
  }

  @Test
  public void testTuple() {
    runTestFile("tuple");
  }

  @Test
  public void testTypeChecks() {
    runTestFile("typechecks");
  }

  @Test
  public void testTypes() {
    runTestFile("types");
  }

  @Test
  public void testUnary() {
    runTestFile("unary");
  }

  @Test
  public void testWith() {
    runTestFile("with");
  }
}
