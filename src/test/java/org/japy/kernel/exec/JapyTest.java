package org.japy.kernel.exec;

import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.KernelTest.LIBDIR_39;
import static org.japy.kernel.util.PyUtil.wrapUcs2;

import java.nio.file.Paths;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import org.japy.TestFiles;
import org.japy.kernel.KernelTest;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.mod.IPyModule;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JapyTest {
  @Test
  public void aTestSmoke() {
    KernelTest.test(LIBDIR_39, ctx -> KernelTest.runFile(Paths.get(TestFiles.EXEC_FILE_PY), ctx));
  }

  @Test
  public void bTestAdHoc() {
    KernelTest.test(
        LIBDIR_39, ctx -> TestFiles.iterateAdhoc("exec", path -> KernelTest.runFile(path, ctx)));
  }

  @Test
  public void cTestJapy() {
    KernelTest.test(
        LIBDIR_39,
        ctx -> {
          IPyModule sys = ctx.kernel.sys();
          PyList path = dcheckNotNull((PyList) sys.get("path"));
          path.append(wrapUcs2("src/test/testfiles"));
          IPyModule builtins = ctx.kernel.builtins();
          FuncLib.callMeth(
              builtins, "exec", PyStr.get("from japytest import main; main()"), new PyDict(), ctx);
        });
  }
}
