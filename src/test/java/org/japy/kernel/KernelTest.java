package org.japy.kernel;

import static org.japy.kernel.types.misc.PyNone.None;

import java.nio.file.Path;
import java.util.function.Consumer;

import org.japy.ErrorHelper;
import org.japy.TestFiles;
import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.ExecLib;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxSystemExit;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.mod.PyModule;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.util.Constants;
import org.japy.lib.ModuleRegistry;

public class KernelTest {
  public enum LibDir {
    None,
    Python39,
    Python310,
  }

  public static final LibDir LIBDIR_NONE = LibDir.None;
  public static final LibDir LIBDIR_39 = LibDir.Python39;
  public static final LibDir LIBDIR_310 = LibDir.Python310;

  public static String[] getLibDirs(LibDir libDir) {
    switch (libDir) {
      case None:
        return new String[0];
      case Python39:
        return new String[] {TestFiles.JAPY39_LIB_DIR, TestFiles.PYTHON39_LIB_DIR};
      case Python310:
        return new String[] {TestFiles.JAPY310_LIB_DIR, TestFiles.PYTHON310_LIB_DIR};
    }

    throw InternalErrorException.notReached();
  }

  public static void test(LibDir libDir, Consumer<PyContext> testFunc) {
    Config config = Config.builder().libDirs(getLibDirs(libDir)).build();
    try (Kernel kernel = new Kernel(config);
        PyContext ctx = new PyContext(kernel, kernel.mainThread)) {
      ErrorHelper.wrap(() -> testFunc.accept(ctx), null);
    }
  }

  private static void checkExitStatus(PxSystemExit ex, PyContext ctx) {
    PyBaseException pex = ex.pyExc;
    if (pex.args.isEmpty()) {
      throw new AssertionError("SystemExit with no args", ex);
    }
    IPyObj status = pex.args.get(0);
    if (status == None || (status instanceof PyInt && ((PyInt) status).isZero())) {
      return;
    }
    throw new AssertionError("sys.exit() with non-zero status: " + PrintLib.repr(status, ctx), ex);
  }

  public static void runFile(Path path, PyContext ctx) {
    ErrorHelper.wrap(
        () -> {
          PyModule module = new PyModule(Constants.__MAIN__);
          module.set(Constants.__NAME__, PyStr.ucs2(Constants.__MAIN__));
          module.set(Constants.__FILE__, PyStr.get(path.toString()));
          ModuleRegistry registry = (ModuleRegistry) ctx.kernel.moduleRegistry;
          try {
            registry.modules.set(Constants.__MAIN__, module);
            ExecLib.execFile(module, path, ctx);
          } catch (PxSystemExit ex) {
            checkExitStatus(ex, ctx);
          } finally {
            registry.modules.del(Constants.__MAIN__);
          }
        },
        path);
  }
}
