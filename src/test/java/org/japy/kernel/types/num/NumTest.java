package org.japy.kernel.types.num;

import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;

import java.util.function.Supplier;

import org.junit.Assert;
import org.junit.Test;

import org.japy.kernel.misc.Comparisons;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.obj.ObjLib;

@SuppressWarnings("ConstantConditions")
public class NumTest {
  private static void testComparison(IPyObj obj1, IPyObj obj2, int comp, boolean checkInverse) {
    if (comp == 0) {
      Assert.assertTrue(obj1.isEqual(obj2, null));
    } else {
      Assert.assertFalse(obj1.isEqual(obj2, null));
    }

    if (comp < 0) {
      Assert.assertSame(True, Comparisons.lt(obj1, obj2, null));
      Assert.assertSame(True, Comparisons.le(obj1, obj2, null));
      Assert.assertSame(True, Comparisons.ne(obj1, obj2, null));
      Assert.assertSame(False, Comparisons.eq(obj1, obj2, null));
      Assert.assertSame(False, Comparisons.gt(obj1, obj2, null));
      Assert.assertSame(False, Comparisons.ge(obj1, obj2, null));
    } else if (comp == 0) {
      Assert.assertSame(False, Comparisons.lt(obj1, obj2, null));
      Assert.assertSame(True, Comparisons.le(obj1, obj2, null));
      Assert.assertSame(False, Comparisons.ne(obj1, obj2, null));
      Assert.assertSame(True, Comparisons.eq(obj1, obj2, null));
      Assert.assertSame(False, Comparisons.gt(obj1, obj2, null));
      Assert.assertSame(True, Comparisons.ge(obj1, obj2, null));
    } else {
      Assert.assertSame(False, Comparisons.lt(obj1, obj2, null));
      Assert.assertSame(False, Comparisons.le(obj1, obj2, null));
      Assert.assertSame(True, Comparisons.ne(obj1, obj2, null));
      Assert.assertSame(False, Comparisons.eq(obj1, obj2, null));
      Assert.assertSame(True, Comparisons.gt(obj1, obj2, null));
      Assert.assertSame(True, Comparisons.ge(obj1, obj2, null));
    }

    if (checkInverse) {
      testComparison(obj2, obj1, -comp, false);
    }
  }

  public static void testComparison(IPyObj obj1, IPyObj obj2, int comp) {
    testComparison(obj1, obj2, comp, true);
  }

  private static void assertNotSupported(Supplier<IPyObj> func) {
    try {
      @SuppressWarnings("unused")
      IPyObj r = func.get();
    } catch (PxException ex) {
      if (ex.pyExc.isInstanceRaw(PyBuiltinExc.TypeError)) {
        return;
      }
      throw ex;
    }

    Assert.fail("operation did not fail as expected");
  }

  private static void testEqOnly(IPyObj obj1, IPyObj obj2, boolean eq, boolean checkInverse) {
    if (eq) {
      Assert.assertSame(True, Comparisons.eq(obj1, obj2, null));
      Assert.assertSame(False, Comparisons.ne(obj1, obj2, null));
    } else {
      Assert.assertSame(False, Comparisons.eq(obj1, obj2, null));
      Assert.assertSame(True, Comparisons.ne(obj1, obj2, null));
    }

    assertNotSupported(() -> Comparisons.lt(obj1, obj2, null));
    assertNotSupported(() -> Comparisons.le(obj1, obj2, null));
    assertNotSupported(() -> Comparisons.gt(obj1, obj2, null));
    assertNotSupported(() -> Comparisons.ge(obj1, obj2, null));

    if (checkInverse) {
      testEqOnly(obj2, obj1, eq, false);
    }
  }

  private static void testEqOnly(IPyObj obj1, IPyObj obj2, boolean eq) {
    testEqOnly(obj1, obj2, eq, true);
  }

  private static void testNaN(IPyObj obj1, IPyObj obj2, boolean eqOnly, boolean checkInverse) {
    Assert.assertSame(False, Comparisons.eq(obj1, obj2, null));
    Assert.assertSame(True, Comparisons.ne(obj1, obj2, null));

    if (!eqOnly) {
      Assert.assertSame(False, Comparisons.lt(obj1, obj2, null));
      Assert.assertSame(False, Comparisons.le(obj1, obj2, null));
      Assert.assertSame(False, Comparisons.gt(obj1, obj2, null));
      Assert.assertSame(False, Comparisons.ge(obj1, obj2, null));
    } else {
      assertNotSupported(() -> Comparisons.lt(obj1, obj2, null));
      assertNotSupported(() -> Comparisons.le(obj1, obj2, null));
      assertNotSupported(() -> Comparisons.gt(obj1, obj2, null));
      assertNotSupported(() -> Comparisons.ge(obj1, obj2, null));
    }

    if (checkInverse) {
      testNaN(obj2, obj1, eqOnly, false);
    }
  }

  private static void testNaN(IPyObj obj1, IPyObj obj2, boolean eqOnly) {
    testNaN(obj1, obj2, eqOnly, true);
  }

  @Test
  public void testComparison() {
    testComparison(PyInt.ZERO, PyInt.ZERO, 0);
    testComparison(PyInt.ONE, PyInt.ONE, 0);
    testComparison(PyInt.get(-1), PyInt.ZERO, -1);
    testComparison(PyInt.ZERO, PyInt.ONE, -1);
    testComparison(PyInt.get(-1), PyInt.ONE, -1);

    testComparison(PyFloat.ZERO, PyFloat.ZERO, 0);
    testComparison(PyFloat.ONE, PyFloat.ONE, 0);
    testComparison(new PyFloat(-1), PyFloat.ZERO, -1);
    testComparison(PyFloat.ZERO, PyFloat.ONE, -1);
    testComparison(new PyFloat(-1), PyFloat.ONE, -1);
    testComparison(new PyFloat(-1), PyFloat.POSITIVE_INFINITY, -1);
    testComparison(PyFloat.NEGATIVE_INFINITY, PyFloat.ONE, -1);
    testComparison(PyFloat.NEGATIVE_INFINITY, PyFloat.POSITIVE_INFINITY, -1);
    testNaN(PyFloat.NAN, PyFloat.NAN, false);
    testNaN(PyFloat.NAN, PyFloat.ONE, false);
    testNaN(PyFloat.NAN, PyFloat.POSITIVE_INFINITY, false);

    testComparison(PyInt.ZERO, PyFloat.ZERO, 0);
    testComparison(PyInt.ONE, PyFloat.ONE, 0);
    testComparison(PyInt.get(-1), PyFloat.ZERO, -1);
    testComparison(PyInt.ZERO, PyFloat.ONE, -1);
    testComparison(PyInt.get(-1), PyFloat.ONE, -1);
    testComparison(PyInt.ONE, PyFloat.POSITIVE_INFINITY, -1);
    testComparison(PyInt.get(-1), PyFloat.NEGATIVE_INFINITY, 1);
    testNaN(PyFloat.NAN, PyInt.ONE, false);
    testNaN(PyFloat.NAN, PyInt.ZERO, false);

    testEqOnly(PyInt.ZERO, PyComplex.ZERO, true);
    testEqOnly(PyInt.ONE, PyComplex.ONE, true);
    testEqOnly(PyInt.get(-1), PyComplex.ZERO, false);
    testEqOnly(PyInt.ZERO, PyComplex.ONE, false);
    testEqOnly(PyInt.get(-1), PyComplex.ONE, false);
    testEqOnly(PyInt.get(-1), PyComplex.I, false);
    testEqOnly(PyInt.get(-1), PyComplex.I.negate(), false);
    testNaN(PyInt.ZERO, new PyComplex(Double.NaN, 0), true);
    testNaN(PyInt.ONE, new PyComplex(0, Double.NaN), true);

    testEqOnly(PyFloat.ZERO, PyComplex.ZERO, true);
    testEqOnly(PyFloat.ONE, PyComplex.ONE, true);
    testEqOnly(new PyFloat(-1), PyComplex.ZERO, false);
    testEqOnly(PyFloat.ZERO, PyComplex.ONE, false);
    testEqOnly(new PyFloat(-1), PyComplex.ONE, false);
    testEqOnly(new PyFloat(-1), PyComplex.I, false);
    testEqOnly(new PyFloat(-1), PyComplex.I.negate(), false);
    testNaN(PyFloat.ZERO, new PyComplex(Double.NaN, 0), true);
    testNaN(PyFloat.ONE, new PyComplex(0, Double.NaN), true);

    Assert.assertTrue(PyFloat.ZERO.isEqual(ObjLib.getAttr(PyComplex.I, "real", null), null));
    Assert.assertTrue(PyFloat.ONE.isEqual(ObjLib.getAttr(PyComplex.I, "imag", null), null));

    testEqOnly(PyComplex.ZERO, PyComplex.ZERO, true);
    testEqOnly(PyComplex.ONE, PyComplex.ONE, true);
    testEqOnly(new PyComplex(-1, 0), PyComplex.ZERO, false);
    testEqOnly(PyComplex.ZERO, PyComplex.ONE, false);
    testEqOnly(new PyComplex(-1, 0), PyComplex.ONE, false);
    testEqOnly(PyComplex.I, PyComplex.I, true);
    testEqOnly(PyComplex.I, PyComplex.I.negate(), false);
    testEqOnly(PyComplex.I, PyComplex.ZERO, false);
    testEqOnly(PyComplex.I, PyComplex.ONE, false);
    testNaN(PyComplex.ZERO, new PyComplex(Double.NaN, 0), true);
    testNaN(PyComplex.ONE, new PyComplex(0, Double.NaN), true);
  }
}
