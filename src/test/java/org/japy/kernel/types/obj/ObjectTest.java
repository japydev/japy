package org.japy.kernel.types.obj;

import static org.japy.kernel.KernelTest.LIBDIR_NONE;
import static org.japy.kernel.types.coll.seq.PyTuple.EMPTY_TUPLE;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;

import org.junit.Assert;
import org.junit.Test;

import org.japy.kernel.KernelTest;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.num.PyComplex;
import org.japy.kernel.types.num.PyFloat;
import org.japy.kernel.types.num.PyInt;

public class ObjectTest {
  @Test
  public void testTruthiness() {
    KernelTest.test(
        LIBDIR_NONE,
        ctx -> {
          Assert.assertTrue(ObjLib.isTrue(True, ctx));
          Assert.assertTrue(ObjLib.isTrue(PyInt.ONE, ctx));
          Assert.assertTrue(ObjLib.isTrue(PyInt.NEGATIVE_ONE, ctx));
          Assert.assertTrue(ObjLib.isTrue(PyFloat.ONE, ctx));
          Assert.assertTrue(ObjLib.isTrue(PyFloat.NEGATIVE_ONE, ctx));
          Assert.assertTrue(ObjLib.isTrue(PyFloat.POSITIVE_INFINITY, ctx));
          Assert.assertTrue(ObjLib.isTrue(PyFloat.NEGATIVE_INFINITY, ctx));
          Assert.assertTrue(ObjLib.isTrue(PyFloat.NAN, ctx));
          Assert.assertTrue(ObjLib.isTrue(PyComplex.ONE, ctx));
          Assert.assertTrue(ObjLib.isTrue(PyComplex.I, ctx));
          Assert.assertTrue(ObjLib.isTrue(PyComplex.INF, ctx));
          Assert.assertTrue(ObjLib.isTrue(PyComplex.NaN, ctx));
          Assert.assertTrue(ObjLib.isTrue(new PyList(False), ctx));
          Assert.assertTrue(ObjLib.isTrue(new PyTuple(False), ctx));

          Assert.assertFalse(ObjLib.isTrue(False, ctx));
          Assert.assertFalse(ObjLib.isTrue(PyInt.ZERO, ctx));
          Assert.assertFalse(ObjLib.isTrue(PyFloat.ZERO, ctx));
          Assert.assertFalse(ObjLib.isTrue(PyComplex.ZERO, ctx));
          Assert.assertFalse(ObjLib.isTrue(new PyList(), ctx));
          Assert.assertFalse(ObjLib.isTrue(EMPTY_TUPLE, ctx));

          Assert.assertNotNull(ObjLib.getAttr(EMPTY_TUPLE, "__len__", ctx));
          //          ObjLib.delAttr(PyTuple.EMPTY, PyStr.get("__len__"), ctx);
          //          ObjLib.setAttr(PyTuple.EMPTY, PyStr.get("__len__"), PyInt.ONE, ctx);
          Assert.assertEquals(0, CollLib.len(EMPTY_TUPLE, ctx));
          Assert.assertEquals(1, CollLib.len(PyTuple.of(EMPTY_TUPLE), ctx));

          PyComplex c = PyComplex.I.add(2);
          Assert.assertEquals(2, (int) ((PyFloat) ObjLib.getAttr(c, "real", ctx)).value);
          Assert.assertEquals(1, (int) ((PyFloat) ObjLib.getAttr(c, "imag", ctx)).value);

          IPyObj getReal = ObjLib.getAttr(PyComplex.TYPE, "real", ctx);
          Assert.assertEquals(
              2,
              (int)
                  ((PyFloat) FuncLib.call(ObjLib.getAttr(getReal, "__get__", ctx), c, ctx)).value);
        });
  }
}
