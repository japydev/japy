package org.japy;

import java.nio.file.Path;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.IHasSourceFile;
import org.japy.infra.loc.Location;
import org.japy.infra.util.ExcUtil;
import org.japy.infra.util.ThrowingRunnable;
import org.japy.infra.util.ThrowingSupplier;

public class ErrorHelper {
  public static <T> T wrap(ThrowingSupplier<T> func, @Nullable Path file) {
    try {
      return func.get();
    } catch (Throwable ex) {
      printError(file, ex);
      throw ExcUtil.rethrow(ex);
    }
  }

  public static void wrap(ThrowingRunnable func, @Nullable Path file) {
    @SuppressWarnings("unused")
    Integer ignored =
        wrap(
            () -> {
              func.run();
              return 1;
            },
            file);
  }

  @SuppressWarnings("SystemOut")
  public static void printError(@Nullable Path file, Throwable ex) {
    @Var
    @Nullable
    String sourceFile = null;
    @Var
    @Nullable
    Location loc = null;

    @Var Throwable t = ex;
    while ((loc == null || sourceFile == null) && t != null) {
      if (sourceFile == null
          && t instanceof IHasSourceFile
          && !((IHasSourceFile) t).isUnknownSourceFile()) {
        sourceFile = ((IHasSourceFile) t).sourceFile();
      }
      if (loc == null && t instanceof IHasLocation && !((IHasLocation) t).location().isUnknown()) {
        loc = ((IHasLocation) t).location();
      }
      t = t.getCause();
    }

    if (sourceFile == null && file != null) {
      sourceFile = file.toString();
    }

    if (sourceFile != null) {
      if (loc != null && !loc.isUnknown()) {
        System.err.printf("%s%n\tat %s:%s:%s%n", ex.getMessage(), sourceFile, loc.line, loc.col);
      } else {
        System.err.printf("%s%n\tat %s:1%n", ex.getMessage(), sourceFile);
      }
    }
  }
}
