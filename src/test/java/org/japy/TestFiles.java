package org.japy;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import com.google.common.collect.Iterators;
import org.checkerframework.checker.nullness.qual.Nullable;

public class TestFiles {
  public static final String JAPY39_LIB_DIR = "src/test/testFiles/lib/japy39";
  public static final String JAPY310_LIB_DIR = "src/test/testFiles/lib/japy310";
  public static final String PYTHON39_LIB_DIR = "src/test/testFiles/lib/39";
  public static final String PYTHON310_LIB_DIR = "src/test/testFiles/lib/310";
  public static final String COMPILER_FILE_PY = "src/test/testFiles/compiler/file.py";
  public static final String EXEC_FILE_PY = "src/test/testFiles/exec/file.py";

  private static final int N_THREADS = 4;

  private static final String[] COMPILER_AD_HOC_FILES = {
    "src/test/testFiles/compiler/exc.py",
    "src/test/testFiles/compiler/misc.py",
    "src/test/testFiles/compiler/test_arith.py",
    "src/test/testFiles/compiler/test_collections.py",
    "src/test/testFiles/compiler/langrussianmodel.py",
    COMPILER_FILE_PY,
  };

  private static final String[] EXEC_AD_HOC_FILES = {
    EXEC_FILE_PY,
    "src/test/testFiles/exec/exc.py",
    "src/test/testFiles/exec/test_import.py",
    "src/test/testFiles/exec/test_class.py",
    "src/test/testFiles/exec/misc.py",
    "src/test/testFiles/exec/test_builtin.py",
    "src/test/testFiles/exec/test_func.py",
    "src/test/testFiles/exec/test_arith.py",
    "src/test/testFiles/exec/test_collections.py",
    "src/test/testFiles/exec/test_datamodel.py",
  };

  private static final String[] LIB_DIRS = {
    PYTHON310_LIB_DIR,
    //     PYTHON39_LIB_DIR,
  };

  private static final String[] IGNORED_FILES = {
    "/test/badsyntax_pep3120.py",
    "/test/badsyntax_future3.py",
    "/test/badsyntax_future4.py",
    "/test/badsyntax_future5.py",
    "/test/badsyntax_future7.py",
    "/test/badsyntax_future8.py",
    "/test/badsyntax_future9.py",
    // TODO
    "/310/test/test_grammar.py",
    "/310/test/test_named_expressions.py",
    "/310/test/test_patma.py",
    "/310/test/test_asyncio/test_locks.py",
  };

  private static final String[] IGNORED_DIRS = {
    "/lib2to3/tests/data/",
  };

  public static Iterator<Path> iterAdHoc(String dir) {
    switch (dir) {
      case "compiler":
        return Iterators.concat(iter(COMPILER_AD_HOC_FILES), iter(EXEC_AD_HOC_FILES));
      case "exec":
        return iter(EXEC_AD_HOC_FILES);
      default:
        throw new AssertionError("unknown test file directory " + dir);
    }
  }

  public static Iterator<Path> iterLib() {
    return Iterators.filter(iter(LIB_DIRS), p -> !ignoreFile(p));
  }

  public static void iterateAdhoc(String dir, Consumer<Path> func) {
    iterate(iterAdHoc(dir), func);
  }

  public static void iterateLibParallel(Consumer<Path> func) {
    AtomicReference<Path> failed = new AtomicReference<>();
    Iterator<Path> iter = iterLib();

    for (int i = 0; i < 4; ++i) {
      func.accept(iter.next());
    }

    ExecutorService executor = Executors.newFixedThreadPool(N_THREADS);
    List<Future<?>> futures = new ArrayList<>();
    for (int i = 0; i < N_THREADS; ++i) {
      futures.add(
          executor.submit(
              () -> {
                while (failed.get() == null) {
                  @Nullable Path path;
                  synchronized (iter) {
                    path = iter.hasNext() ? iter.next() : null;
                  }
                  if (path == null) {
                    break;
                  }
                  try {
                    func.accept(path);
                  } catch (Throwable ex) {
                    failed.compareAndSet(null, path);
                    break;
                  }
                }
              }));
    }

    for (Future<?> f : futures) {
      try {
        f.get();
      } catch (InterruptedException | ExecutionException ignored) {
      }
    }

    executor.shutdown();

    if (failed.get() != null) {
      func.accept(failed.get());
      throw new AssertionError(
          String.format("parallel run failed for %s but it did not fail on rerun", failed.get()));
    }
  }

  private static void iterate(Iterator<Path> iter, Consumer<Path> func) {
    while (iter.hasNext()) {
      func.accept(iter.next());
    }
  }

  @SuppressWarnings("unchecked")
  private static Iterator<Path> iter(String[] fileNames) {
    return Iterators.concat(Arrays.stream(fileNames).map(TestFiles::iter).toArray(Iterator[]::new));
  }

  @SuppressWarnings("StreamResourceLeak")
  private static Iterator<Path> iter(String fileName) {
    try {
      return Files.walk(Paths.get(fileName)).iterator();
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    }
  }

  private static boolean ignoreFile(Path path) {
    if (path.getFileName().toString().startsWith("._")) {
      return true;
    }

    String pathString = path.toString().toLowerCase();
    if (!pathString.endsWith(".py")) {
      return true;
    }

    for (String f : IGNORED_DIRS) {
      if (pathString.contains(Paths.get(f).toString().toLowerCase())) {
        return true;
      }
    }

    for (String f : IGNORED_FILES) {
      if (pathString.endsWith(Paths.get(f).toString().toLowerCase())) {
        return true;
      }
    }

    return false;
  }
}
