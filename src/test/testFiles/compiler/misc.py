""" Test module """

manager = object()

# if 1:
#     with manager() as x:
#         pass
#
# if 1:
#     with (
#             manager()
#     ):
#         pass
#
# if 1:
#     with (
#             1 + 2 as x
#     ):
#         pass
#
# if 1:
#     with (
#             1 + 2
#     ) as x:
#         pass
#
# if 1:
#     with (
#             manager()
#     ).x:
#         pass
#
# if 1:
#     with (
#         manager() as x
#     ):
#         pass
#
# if 1:
#     with (
#         manager() as (x, y),
#         manager() as z,
#     ):
#         pass
#
# if 1:
#     with (
#         manager(),
#         manager()
#     ):
#         pass
#
# if 1:
#     with (
#         manager() as x,
#         manager() as y
#     ):
#         pass
#
# if 1:
#     with (
#         manager() as x,
#         manager()
#     ):
#         pass
#
# if 1:
#     with (
#         manager() as x,
#         manager() as y,
#         manager() as z,
#     ):
#         pass
#
# if 1:
#     with (
#         manager() as x,
#         manager() as y,
#         manager(),
#     ):
#         pass

x = (i for i in [1, 2, 3])

x = 1
if x == 1:
    z = 2
else:
    z = 3
assert z == 2

x = 2
if x == 2:
    z = 4
assert z == 4

if not None:
    z = 5
assert z == 5

class P:
    def __init__(self):
        self._x = 1

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

p = P()
assert p.x == 1
p.x = 2
assert p.x == 2

class C:
    @classmethod
    def foo(cls, *args):
        return args

def assert_eq(x, y):
    assert x == y

assert_eq(C.foo(1, 2, 3), [1, 2, 3])

import sys
#import unittest
#import os.path

def gen():
    x = yield 1
    y = yield 2
    # yield from [3, 4, 5]

g = gen()
assert g.__next__() == 1
assert g.__next__() == 2
# assert g.__next__() == 3
# assert g.__next__() == 4
# assert g.__next__() == 5
i = 0
for x in gen():
    i += 1
# assert i == 5
assert i == 2

l = [1,2,3]
itr = l.__iter__()
i = 0
while True:
    try:
        i += itr.__next__()
    except:
        break
assert i == 6

s = set([1, 2, 3])
i = 0
for o in s:
    i += o
assert i == 6

(*a,) = [1,2,3]
assert a == [1,2,3]
(*a, b) = [1,2,3]
assert a == [1,2]
assert b == 3
(a, *b) = [1,2,3]
assert a == 1
assert b == [2,3]

class X:
    """ Class which does something """
    def x(self):
        """ Method which does something """
        return self

x = X()
assert x.x() is x

def blah(a, b, *c):
    return a + b + c[0]
assert blah(1, 2, 3) == 6
assert blah(1, 2, 3, 4) == 6
def blah(a, b, *c):
    locals()
    return a + b + c[0]

def fun(i):
    return i
def fun2():
    i = 0
    sum = 0
    # while i < 50_000_000:
    while i < 50:
        sum += fun(i)
        i += 1
fun2()

def func():
    x = 1
    class X:
        class Y:
            y = x
            def meth(self):
                return x
            def meth2(self):
                return X.Y.y
            def meth3(self):
                return Y.y
    return X

def nop():
    pass

def func():
    i = 1
    def sub():
        nonlocal i
        i += 1
        if i < 10:
            return sub()
        else:
            return i
    return sub
assert func()() == 10

l = [1,2,3]
l2 = [*l]
assert l2 == l

d = {'a': 1, 'b': 2}
c = {**d, 'c': 3}
assert c == {'a': 1, 'b': 2, 'c': 3}

l = tuple(x*j for x in [1, 2, 3] if x > 2 for j in [1, 2, 3] if j < 3)
assert l == (3, 6)

s = 0
s += 2 * 3
assert s == 6
s = 0
iter_count = 0
for i in [1, 2, 3, 4]:
    for j in [1, 2, 3, 4]:
        old = s
        s += i * j
        assert s - old == i * j
        iter_count += 1
assert iter_count == 16
assert s == 100

s = "123"
def f():
    a = s
    def x():
        return a
    return x
assert f()() is s

def slow():
    i = 0
    limit = 100_000_000
#     limit = 10
    def fun():
        nonlocal i
        i = i + 1
    while i < limit:
        fun()
    assert i == limit
slow()

def get_unpatched_class(cls):
    return [cls for cls in cls]
assert get_unpatched_class([1,2,3]) == [1,2,3]

f = X()
f.new_attr = 1
assert f.new_attr is 1
del f.new_attr
seen_exc = False
try:
    f.new_attr
except AttributeError:
    seen_exc = True
else:
    assert False
assert seen_exc

class CtxMgr:
    def __enter__(self):
        pass
    def __exit__(self, *args):
        pass

with CtxMgr():
    try:
        pass
    except AttributeError:
        pass
    else:
        pass

def func_with_loop():
    for zzz in [1,2,3]:
        with CtxMgr():
            try:
                zzz += 2
            except AttributeError:
                pass
            else:
                pass
func_with_loop()

@staticmethod
def _convert_metadata(zf, destination_eggdir, dist_info, egg_info):
    install_requires = list(sorted(map(raw_req, dist.requires())))
    extras_require = {
        extra: sorted(
            req
            for req in map(raw_req, dist.requires((extra,)))
            if req not in install_requires
        )
        for extra in dist.extras
    }

async def foo():
    return [x async for x in [y async for y in z]]

def __new__(mcls, name, bases, namespace, **kwargs):
    cls = super().__new__(**kwargs)
    return cls

def _find_spec(name, path, target=None):
    for finder in meta_path:
        pass
    else:
        return None

""" Test module """

def gen():
    x = yield 1
    y = yield 2
    yield from [3, 4, 5]

g = gen()
assert g.__next__() == 1
assert g.__next__() == 2
# assert g.__next__() == 3
# assert g.__next__() == 4
# assert g.__next__() == 5
i = 0
for x in gen():
    i += 1
# assert i == 5
assert i == 2

l = [1,2,3]
itr = l.__iter__()
i = 0
while True:
    try:
        i += itr.__next__()
    except:
        break
assert i == 6

(*a,) = [1,2,3]
assert a == [1,2,3]
(*a, b) = [1,2,3]
assert a == [1,2]
assert b == 3
(a, *b) = [1,2,3]
assert a == 1
assert b == [2,3]

[func(*args) for func, args in items]

(a, *b) = c

class X:
    """ Class which does something """
    def x(self):
        """ Method which does something """
        return self

print(f"{cls}")

x = X()
assert x.x() is x

def blah(a, b, *c):
    return a + b + c[0]
assert blah(1, 2, 3) == 6

def fun(i):
    return i
def fun2():
    i = 0
    sum = 0
    # while i < 50_000_000:
    while i < 50:
        sum += fun(i)
        i += 1
fun2()

def func():
    x = 1
    class X:
        class Y:
            y = x
            def meth(self):
                return x
            def meth2(self):
                return X.Y.y
            def meth3(self):
                return Y.y
    return X

def nop():
    pass

def func():
    i = 1
    def sub():
        nonlocal i
        i += 1
        if i < 10:
            return sub()
        else:
            return i
    return sub
assert func()() == 10

l = [1,2,3]
l2 = [*l]
assert l2 == l

d = {'a': 1, 'b': 2}
c = {**d, 'c': 3}
assert c == {'a': 1, 'b': 2, 'c': 3}

l = (x*j for x in [1, 2, 3] if x > 2 for j in [1, 2, 3] if j < 3)
assert l == (3, 6)

s = 0
s += 2 * 3
assert s == 6
s = 0
iter_count = 0
for i in [1, 2, 3, 4]:
    for j in [1, 2, 3, 4]:
        old = s
        s += i * j
        assert s - old == i * j
        iter_count += 1
assert iter_count == 16
assert s == 100

s = "123"
def f():
    a = s
    def x():
        return a
    return x
assert f()() is s

def slow():
    i = 0
    limit = 100_000_000
    limit = 10
    def fun():
        nonlocal i
        i = i + 1
    while i < limit:
        fun()
    assert i == limit
slow()

# i = 2**64 + 1
# limit = 2**64 + 100_000_000
# while i < limit:
#     i += 1
#     f()()

def get_unpatched_class(cls):
    external_bases = [
        cls for cls in cls
    ]
    return external_bases
assert get_unpatched_class([1,2,3]) == [1,2,3]

f = X()
f.new_attr = 1
f.new_attr
del f.new_attr
seen_exc = False
try:
    f.new_attr
except:
    pass
else:
    assert False

class TestHook:
    @property
    def seen_events(self):
        return [i[0] for i in self.seen]

def _loop_reading(self, fut=None):
    try:
        pass
    except ConnectionResetError as exc:
        pass
    finally:
        pass

(x async for x in (y async for y in z))

if __name__ == '__main__':
    import sys
    if not sys.argv[1:]:
        sys.argv.append('/usr/demos/data/audio/bach.aiff')
    fn = sys.argv[1]
    with open(fn, 'r') as f:
        print("Reading", fn)
        print("nchannels =", f.getnchannels())
        print("nframes   =", f.getnframes())
        print("sampwidth =", f.getsampwidth())
        print("framerate =", f.getframerate())
        print("comptype  =", f.getcomptype())
        print("compname  =", f.getcompname())
        if sys.argv[2:]:
            gn = sys.argv[2]
            print("Writing", gn)
            with open(gn, 'w') as g:
                g.setparams(f.getparams())
                while 1:
                    data = f.readframes(1024)
                    if not data:
                        break
                    g.writeframes(data)
            print("Done.")

def _fill_text(self, text, width, indent):
    return ''.join(indent + line for line in text.splitlines(keepends=True))

[x := y for y in (1,2,3)]

async def wait_for(fut, timeout):
    try:
        # wait until the future completes or the timeout
        try:
            await waiter
        except exceptions.CancelledError:
            if fut.done():
                return fut.result()
            else:
                raise

        if fut.done():
            return fut.result()
        else:
            try:
                fut.result()
            except exceptions.CancelledError as exc:
                raise exceptions.TimeoutError() from exc
            else:
                raise exceptions.TimeoutError()
    finally:
        timeout_handle.cancel()

def wait_for():
    try:
        try:
            pass
        except:
            return 1
    finally:
        pass

def submit():
    try:
        try:
            return f
        finally:
            pass
    finally:
        pass

def submit():
    with self._shutdown_lock, _global_shutdown_lock:
        return f

async def run_list():
    return [await c for c in [f(1), f(41)]]
