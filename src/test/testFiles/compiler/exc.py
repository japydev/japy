e = StopIteration(8)
assert e.args == (8,)
assert e.value == 8

z = 8
try:
    try:
        x = 1
        raise Exception('blah')
    except:
        y = 2
        raise Exception('foo')
    finally:
        t = 4
        raise RuntimeError('baz')
except:
    pass
else:
    z = 5

assert x == 1
assert y == 2
assert t == 4
assert z == 8
