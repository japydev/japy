MAX_INT = 2**32 - 1
MIN_INT = -2**32
MAX_LONG = 2**64 - 1
MIN_LONG = -2**64

assert 0 == 0
assert 1 == 1
assert 0 != 1
assert MAX_INT + 1 == -MIN_INT
assert MAX_LONG + 1 == -MIN_LONG
assert MIN_INT << 32 == MIN_LONG
