def func():
    def child():
        nonlocal a
        return a
    a = 1
    del a
    child()

try:
    func()
except NameError as ex:
    assert str(ex) == "free variable 'a' referenced before assignment in enclosing scope"
except:
    assert False, "unexpected exception"
else:
    assert False, "no exception"

def func():
    a = 1
    del a
    return a

try:
    func()
except UnboundLocalError as ex:
    assert str(ex) == "local variable 'a' referenced before assignment"
except:
    assert False, "unexpected exception"
else:
    assert False, "no exception"

a = 1
del a
try:
    a
except NameError as ex:
    assert str(ex) == "name 'a' is not defined"
except:
    assert False, "unexpected exception"
else:
    assert False, "no exception"
