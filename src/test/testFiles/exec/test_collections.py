assert len([]) == 0
assert len([1]) == 1
assert len([1, 2]) == 2
assert len(()) == 0
assert () is ()
assert len((1,)) == 1
assert len((1, 2)) == 2
assert len({}) == 0
assert len({1}) == 1
assert len({1, 2}) == 2
assert len(dict()) == 0
assert len({1:2}) == 1
assert len({1:2, 2:3}) == 2

assert tuple() == ()
assert tuple(tuple()) == tuple()

assert repr(slice(1, 2, 3)) == 'slice(1, 2, 3)'

i = 1
for x in [1, 2, 3]:
    assert x == i
    i += 1

i = 1
for x in (1, 2, 3):
    assert x == i
    i += 1

i = 0
for x in {1, 2, 3}:
    i += 1
assert i == 3

i = 1
for k in {1: 2, 2: 3, 3: 4}:
    assert k == i
    i += 1
