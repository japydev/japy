import sys
sys.path.insert(0, 'src/test/testfiles/exec/testlib')

import namespacepkg.submodule
assert namespacepkg.submodule.submodule == 16

from pkg import submodule as pkgsubmodule
assert pkgsubmodule.foo == 4
import pkg
assert pkg.bar == 5

import amodule
assert amodule.amodule == 8
