# https://docs.python.org/3/reference/datamodel.html

# https://docs.python.org/3/reference/datamodel.html#the-standard-type-hierarchy

class X:
    def foo(self):
        pass

assert X.__dict__['foo'].__name__ == 'foo'
assert X.__dict__['foo'].__objclass__ == X

def return_nothing():
    pass

assert return_nothing() is None
assert not None

try:
    assert False
except AssertionError:
    pass
except BaseException as ex:
    raise Exception(repr(ex))
else:
    raise Exception("expected AssertionError, got nothing")

try:
    if NotImplemented:
        assert False
except TypeError:
    pass
except:
    assert False
else:
    assert False

assert ... is Ellipsis
assert ...

assert type('x') is str
assert type(()) is tuple
assert type(b'') is bytes
assert type(b'x') is bytes
assert type(b'1234567890') is bytes
assert type([]) is list
assert type({1}) is set
assert type({1,2,3}) is set
assert type({}) is dict
assert type({1:2}) is dict
assert type(1) is int
assert type(1.0) is float
assert type(1j) is complex

assert __name__ == '__main__'

def func(a: int) -> int:
    return 1

assert func.__annotations__ == {'a': int, 'return': int}, func.__annotations__

def func(self):
    """A function"""
    return 1

assert dict(func.__dict__) == dict(), func.__dict__
assert func.__annotations__ == dict()
assert func.__doc__ == 'A function'
assert func.__name__ == 'func'
assert func.__qualname__ == 'func'
assert func.__module__ == '__main__'
func.__doc__ = 'zz'
assert func.__doc__ == 'zz'
func.__name__ = 'foo'
assert func.__name__ == 'foo'
func.__qualname__ = 'bar'
assert func.__qualname__ == 'bar'
func.__module__ = 'mod'
assert func.__module__ == 'mod'

class X:
    """A class"""
    def func(self):
        """A method"""
        return 1

assert X.__module__ == '__main__'
X.__module__ = 'y'
assert X.__module__ == 'y'
assert X.__doc__ == 'A class'
X.__doc__ = 'zz'
assert X.__doc__ == 'zz'
assert X.__name__ == 'X'
X.__name__ = 'Y'
assert X.__name__ == 'Y'
assert X.__qualname__ == 'X'
X.__qualname__ = 'Y'
assert X.__qualname__ == 'Y'

assert X.func.__doc__ == 'A method'
assert X.func.__name__ == 'func'
assert X.func.__qualname__ == 'X.func'
assert X.func.__module__ == '__main__'
X.func.__func__.__doc__ = 'zz'
assert X.func.__doc__ == 'zz'
X.func.__func__.__name__ = 'foo'
assert func.__name__ == 'foo'
X.func.__func__.__qualname__ = 'bar'
assert func.__qualname__ == 'bar'
X.func.__func__.__module__ = 'mod'
assert func.__module__ == 'mod'

assert type(bytearray()) is bytearray
assert type(frozenset()) is frozenset
assert type(frozenset([1,2,3])) is frozenset

class SuperHash(str):
    def __hash__(self):
        return super().__hash__()

h = SuperHash('foo')
assert h == 'foo'
assert hash(h) == hash('foo')

# TODO-JAPY
if False:
    class SuperHash2(str):
        def __hash__(self):
            return str.__hash__(self)

    h = SuperHash2('foo')
    assert h == 'foo'
    assert hash(h) == hash('foo')
