e = StopIteration(8)
assert e.args == (8,)
assert e.value == 8

z = 8
try:
    try:
        x = 1
        raise Exception('blah')
    except:
        y = 2
        raise Exception('foo')
    finally:
        t = 4
        raise RuntimeError('baz')
except Exception as ex:
    z = ex.args[0]
else:
    z = 5

assert x == 1
assert y == 2
assert t == 4
assert z == 'baz', z

z = 17
try:
    try:
        x = 3
        raise Exception('blah')
    except:
        y = 5
        raise Exception('foo')
    finally:
        t = 6
except Exception as ex:
    z = ex.args[0]
else:
    z = 5

assert x == 3
assert y == 5
assert t == 6
assert z == 'foo'

z = 21
try:
    raise RuntimeError('xxx')
except:
    z = 22
assert z == 22

z = 25
try:
    x = 1
    y = 0
    x / y
except:
    z = 26
assert z == 26

z = 23
try:
    1 / 0
    z = 24
except:
    pass
assert z == 23

z = 25
try:
    1 / 0
except:
    z = 26
assert z == 26

try:
    1 / 0
except EOFError: pass
except TypeError as msg: pass
except: pass

class CtxMgr:
    def __enter__(self):
        pass
    def __exit__(self, *args, **kwargs):
        pass
try:
    with CtxMgr():
        raise RuntimeError('blah')
except RuntimeError as ex:
    z = ex.args[0]
assert z == 'blah'


try:
    with CtxMgr(), CtxMgr():
        raise RuntimeError('bom')
except RuntimeError as ex:
    z = ex.args[0]
assert z == 'bom'

try:
    try:
        raise RuntimeError('aaa')
    except:
        raise
except RuntimeError as ex:
    z = ex.args[0]
assert z == 'aaa'

try:
    try:
        raise RuntimeError('aaa')
    except:
        raise
except RuntimeError:
    z = 'bbb'
assert z == 'bbb'
