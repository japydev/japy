class Int(int):
    def __new__(cls, x):
        return int.__new__(cls, x)

    def __init__(self, x):
        int.__init__(self)

x = Int(17)
assert x == 17

class Float(float):
    def __new__(cls, x=0):
        return super().__new__(cls, x)

    def __init__(self, x=0):
        super().__init__()

y = Float(17.)
assert y == 17
assert y == 17.0
assert y == x

class X:
    pass

x = X()
o = object()

class Z:
    def __repr__(self):
        return "repr1"

z = Z()
assert repr(z) == "repr1"
Z.__repr__ = lambda self: "repr2"
assert repr(z) == "repr2"
