""" Test module """

s = 100
assert s == 100

def commonpath(paths):
    drivesplits = [p for p in paths]
    try:
        set(p for p in drivesplits)
    except:
        pass

commonpath([])

x = (i for i in [1, 2, 3])

x = 1
if x == 1:
    z = 2
else:
    z = 3
assert z == 2

x = 2
if x == 2:
    z = 4
assert z == 4

if not None:
    z = 5
assert z == 5

class P:
    def __init__(self):
        self._x = 1

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

p = P()
assert p.x == 1
p.x = 2
assert p.x == 2

class C:
    @classmethod
    def foo(cls, *args):
        return args

def assert_eq(x, y):
    assert x == y

assert_eq(C.foo(1, 2, 3), (1, 2, 3))

#import sys
#import unittest
#import os.path

(*a,) = [1,2,3]
assert a == (1,2,3)
(*a, b) = [1,2,3]
assert a == (1,2)
assert b == 3
(a, *b) = [1,2,3]
assert a == 1
assert b == (2,3)

class X:
    """ Class which does something """
    def x(self):
        """ Method which does something """
        return self

x = X()
assert x.x() is x

def blah(a, b, *c):
    return a + b + c[0]
assert blah(1, 2, 3) == 6
assert blah(1, 2, 3, 4) == 6

def fun(i):
    return i
def fun2():
    i = 0
    sum = 0
    # while i < 50_000_000:
    while i < 50:
        sum += fun(i)
        i += 1
fun2()

def func():
    x = 1
    class X:
        class Y:
            y = x
            def meth(self):
                return x
            def meth2(self):
                return X.Y.y
            def meth3(self):
                return Y.y
    return X

def nop():
    pass

def func():
    i = 1
    def sub():
        nonlocal i
        i += 1
        if i < 10:
            return sub()
        else:
            return i
    return sub
assert func()() == 10

l = [1,2,3]
l2 = [*l]
assert l2 == l

d = {'a': 1, 'b': 2}
c = {**d, 'c': 3}
assert c == {'a': 1, 'b': 2, 'c': 3}, c

l = tuple(x*j for x in [1, 2, 3] if x > 2 for j in [1, 2, 3] if j < 3)
assert l == (3, 6)

s = 0
s += 2 * 3
assert s == 6
s = 0
iter_count = 0
for i in [1, 2, 3, 4]:
    for j in [1, 2, 3, 4]:
        old = s
        s += i * j
        assert s - old == i * j
        iter_count += 1
assert iter_count == 16
assert s == 100, s

s = "123"
def f():
    a = s
    def x():
        return a
    return x
assert f()() is s

def slow():
    i = 0
    limit = 100_000
#     limit = 10
    def fun():
        nonlocal i
        i = i + 1
    while i < limit:
        fun()
    assert i == limit
slow()

def get_unpatched_class(cls):
    return [cls for cls in cls]
assert get_unpatched_class([1,2,3]) == [1,2,3]

f = X()
f.new_attr = 1
assert f.new_attr is 1
del f.new_attr
seen_exc = False
try:
    f.new_attr
except AttributeError:
    seen_exc = True
else:
    assert False
assert seen_exc

class CtxMgr:
    def __enter__(self):
        pass
    def __exit__(self, *args):
        pass

with CtxMgr():
    try:
        pass
    except AttributeError:
        pass
    else:
        pass

def func_with_loop():
    for zzz in [1,2,3]:
        with CtxMgr():
            try:
                zzz += 2
            except AttributeError:
                pass
            else:
                pass
func_with_loop()

def gen():
    x = yield 1
    y = yield 2
    # yield from [3, 4, 5]

g = gen()
assert g.__next__() == 1
assert g.__next__() == 2
# assert g.__next__() == 3
# assert g.__next__() == 4
# assert g.__next__() == 5
i = 0
for x in gen():
    i += 1
# assert i == 5
assert i == 2

l = [1,2,3]
itr = l.__iter__()
i = 0
while True:
    try:
        i += itr.__next__()
    except:
        break
assert i == 6

s = set([1, 2, 3])
i = 0
for o in s:
    i += o
assert i == 6

