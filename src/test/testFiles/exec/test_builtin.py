# https://docs.python.org/3/library/stdtypes.html

def test_assert_works():
    try:
        assert False
    except AssertionError:
        assert_works = True
    assert_works
    assert assert_works
test_assert_works()
del test_assert_works

def assert_is_true(v):
    assert v is True
    assert_true(v)

def assert_is_false(v):
    assert v is False
    assert_false(v)

def assert_true(v):
    assert v
    seen_it = False
    try:
        assert not v
    except AssertionError:
        seen_it = True
    assert seen_it

def assert_false(v):
    assert not v
    seen_it = False
    try:
        assert v
    except AssertionError:
        seen_it = True
    assert seen_it

#------------------------------------------------------------------------------
# https://docs.python.org/3/library/stdtypes.html#truth-value-testing

assert_false(None)
assert_is_true(not None)
assert_is_true(True)
assert_is_false(False)
assert_is_true(not False)
assert_is_false(not True)

class Bool:
    def __init__(self, true):
        self.true = true

    def __bool__(self):
        return self.true

assert not Bool(False)
assert Bool(True)

class Len:
    def __init__(self, len):
        self.len = len

    def __len__(self):
        return self.len

assert not Len(0)
assert Len(1)

#------------------------------------------------------------------------------
# https://docs.python.org/3/library/stdtypes.html#boolean-operations-and-or-not

def do_not_call():
    assert False

def id(x):
    return x

t1 = Bool(True)
t2 = Bool(True)
f1 = Bool(False)
f2 = Bool(False)

assert not not t1
assert not f1
assert not not id(t1)
assert not id(f1)

assert (t1 and t2) is t2
assert (t1 or t2) is t1
assert (t1 and f1) is f1
assert (t1 or f1) is t1
assert (f1 and t1) is f1
assert (f1 or t1) is t1
assert (f1 and f2) is f1
assert (f1 or f2) is f2

assert (id(t1) and id(t2)) is t2
assert (id(t1) or do_not_call(t2)) is t1
assert (id(t1) and id(f1)) is f1
assert (id(t1) or do_not_call(f1)) is t1
assert (id(f1) and do_not_call(t1)) is f1
assert (id(f1) or id(t1)) is t1
assert (id(f1) and do_not_call(f2)) is f1
assert (id(f1) or id(f2)) is f2

#------------------------------------------------------------------------------
# https://docs.python.org/3/library/stdtypes.html#comparisons

class Val:
    def __init__(self, value):
        self.value = value
    def __eq__(self, o):
        return self.value == o.value
    def __ne__(self, o):
        return self.value != o.value
    def __lt__(self, o):
        return self.value < o.value
    def __le__(self, o):
        return self.value <= o.value
    def __gt__(self, o):
        return self.value > o.value
    def __ge__(self, o):
        return self.value >= o.value

def test_comp(v1, v2, comp):
    if comp == 0:
        assert_is_true(v1 == v2)
        assert_is_false(v1 != v2)
        assert_is_false(v1 < v2)
        assert_is_true(v1 <= v2)
        assert_is_false(v1 > v2)
        assert_is_true(v1 >= v2)
    elif comp > 0:
        assert_is_false(v1 == v2)
        assert_is_true(v1 != v2)
        assert_is_false(v1 < v2)
        assert_is_false(v1 <= v2)
        assert_is_true(v1 > v2)
        assert_is_true(v1 >= v2)
    else:
        assert_is_false(v1 == v2)
        assert_is_true(v1 != v2)
        assert_is_true(v1 < v2)
        assert_is_true(v1 <= v2)
        assert_is_false(v1 > v2)
        assert_is_false(v1 >= v2)

test_comp(True, True, 0)
test_comp(False, False, 0)
test_comp(False, True, -1)
test_comp(True, False, 1)
test_comp(True, 1, 0)
test_comp(1, True, 0)
test_comp(False, 0, 0)
test_comp(0, False, 0)
test_comp(0, 0, 0)
test_comp(0, 1, -1)
test_comp(1, 0, 1)

assert_is_true(None == None)
assert_is_false(None != None)
assert_is_false(None == True)
assert_is_true(None != True)
assert_is_false(True == None)
assert_is_true(True != None)
assert_is_false(None == False)
assert_is_true(None != False)
assert_is_false(False == None)
assert_is_true(False != None)

test_comp(Val(0), Val(0), 0)
test_comp(Val(0), Val(1), -1)
test_comp(Val(1), Val(0), 1)

assert_is_true(Val(0) < Val(1) < Val(2))
assert_is_true(Val(0) < Val(1) > Val(0))
assert_is_true(Val(0) != Val(1) != Val(2))
assert_is_false(Val(0) < Val(1) > Val(2))

# TODO in, not in
