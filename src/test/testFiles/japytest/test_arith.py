import unittest

MAX_INT = 2**31 - 1
MIN_INT = -2**31
MAX_LONG = 2**63 - 1
MIN_LONG = -2**63


class ArithmeticsTest(unittest.TestCase):
    def test_basic(self):
        self.assert_eq(0, 0)
        self.assert_eq(1, 1)
        self.assert_lt(0, 1)
        self.assert_eq(MAX_INT + 1, -MIN_INT)
        self.assert_eq(MAX_LONG + 1, -MIN_LONG)
        self.assert_eq(MIN_INT << 32, MIN_LONG)
        self.assert_eq(0.0, +0.0)
        self.assert_eq(0.0, -0.0)
        self.assert_lt(0.0, 1.0)
        self.assert_eq(0, 0.0)
        self.assert_lt(0, 1.0)
        self.assert_lt(0.0, 1)

    def test_numbers(self):
        self.assert_eq(1, 0.99999999999999999)
        self.assert_eq(1, 1.0000000000000001)
        self.assert_lt(0.9999999999999999, 1)
        self.assert_lt(1, 1.000000000000001)
        self.assert_lt(0, 0.00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001)
        self.assert_eq(0, 0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001)
        self.assert_eq(0, 0.0)
        self.assert_eq(1, 1.0)
        self.assert_eq(MAX_INT, MAX_INT)
        self.assert_eq(float(MAX_INT), MAX_INT)
        self.assert_eq(int(float(MAX_INT)), MAX_INT)
        self.assert_eq(MAX_INT + 1, MAX_INT + 1)
        self.assert_eq(float(MAX_INT + 1), MAX_INT + 1)
        self.assert_eq(int(float(MAX_INT + 1)), MAX_INT + 1)
        self.assert_eq(float(MAX_INT) + 1, MAX_INT + 1)
        self.assert_eq(int(float(MAX_INT) + 1), MAX_INT + 1)
        self.assert_eq(2**63 - 1, 2**63 - 1)
        self.assert_lt(2**63 - 1, 2**63)
        self.assert_gt(2**63 - 1, 2**63 - 2)
        self.assert_eq(int(float(2**63 - 1)), 2**63)
        self.assert_eq(float(2**63 - 1), 2**63)
        self.assert_lt(float(2**63 - 1), 2**63+1)
        self.assert_gt(float(2**63 - 1), 2**63-1)
        self.assert_eq(2**63, 2**63)
        self.assert_lt(2**63, 2**63 + 1)
        self.assert_gt(2**63, 2**63 - 1)
        self.assert_eq(float(2**63), 2**63)
        self.assert_eq(int(float(2**63)), 2**63)
        self.assert_eq(float(2**63 - 1) + 1, 2**63)
        self.assert_eq(int(float(2**63 - 1) + 1), 2**63)
        self.assert_eq(2**63 + 1, 2**63 + 1)
        self.assert_lt(2**63 + 1, 2**63 + 2)
        self.assert_gt(2**63 + 1, 2**63)
        self.assert_eq(float(2**63 + 1), 2**63)
        self.assert_eq(int(float(2**63 + 1)), 2**63)
        self.assert_eq(float(2**63 - 1) + 2, 2**63)
        self.assert_eq(int(float(2**63 - 1) + 2), 2**63)
        self.assert_eq(2**63 - 2, 2**63 - 2)
        self.assert_eq(float(2**63 - 2), 2**63)
        self.assert_eq(int(float(2**63 - 2)), 2**63)
        self.assert_eq(int(float(2**63 - 1) - 1), 2**63)
        self.assert_eq(-2**63, -2**63)
        self.assert_lt(-2**63, -2**63+1)
        self.assert_gt(-2**63, -2**63-1)
        self.assert_eq(float(-2**63), -2**63)
        self.assert_lt(float(-2**63), -2**63+1)
        self.assert_gt(float(-2**63), -2**63-1)
        self.assert_eq(int(float(-2**63)), -2**63)
        self.assert_eq(-2**63 + 1, -2**63 + 1)
        self.assert_lt(-2**63 + 1, -2**63 + 2)
        self.assert_eq(float(-2**63 + 1), -2**63)
        self.assert_eq(int(float(-2**63 + 1)), -2**63)
        self.assert_eq(float(-2**63) + 1, -2**63)
        self.assert_eq(int(float(-2**63) + 1), -2**63)
        self.assert_eq(-2**63 - 1, -2**63 - 1)
        self.assert_gt(-2**63 - 1, -2**63 - 2)
        self.assert_eq(float(-2**63 - 1), -2**63)
        self.assert_eq(int(float(-2**63 - 1)), -2**63)
        self.assert_eq(float(-2**63) - 1, -2**63)
        self.assert_eq(int(float(-2**63) - 1), -2**63)
        self.assert_eq(2**100, 2**100)
        self.assert_lt(2**100, 2**100 + 1)
        self.assert_gt(2**100, 2**100 - 1)
        self.assert_eq(float(2**100), 2**100)
        self.assert_lt(float(2**100), 2**100 + 1)
        self.assert_gt(float(2**100), 2**100 - 1)
        self.assert_eq(float(2**100 + 1), 2**100)
        self.assert_eq(float(2**100 - 1), 2**100)
        self.assert_eq(float(2**100) + 1, 2**100)
        self.assert_eq(float(2**100) - 1, 2**100)
        self.assert_eq(int(float(2**100)), 2**100)
        self.assert_eq(int(float(2**100 + 1)), 2**100)
        self.assert_eq(int(float(2**100 - 1)), 2**100)

    def assert_lt(self, n1, n2):
        self.assert_cmp(-1, n1, n2)

    def assert_gt(self, n1, n2):
        self.assert_cmp(1, n1, n2)

    def assert_eq(self, n1, n2):
        self.assert_cmp(0, n1, n2)

    def assert_cmp(self, expected, v1, v2, invert=True):
        if expected == -1:
            self.assertIs(False, v1 == v2)
            self.assertIs(True, v1 != v2)
        elif expected == 0:
            self.assertIs(True, v1 == v2)
            self.assertIs(False, v1 != v2)
        elif expected == 1:
            self.assertIs(False, v1 == v2)
            self.assertIs(True, v1 != v2)
        else:
            self.fail("bad cmp result")

        if True:
            if expected == -1:
                self.assertIs(True, v1 < v2)
                self.assertIs(True, v1 <= v2)
                self.assertIs(False, v1 > v2)
                self.assertIs(False, v1 >= v2)
            elif expected == 0:
                self.assertIs(False, v1 < v2)
                self.assertIs(True, v1 <= v2)
                self.assertIs(False, v1 > v2)
                self.assertIs(True, v1 >= v2)
            elif expected == 1:
                self.assertIs(False, v1 < v2)
                self.assertIs(False, v1 <= v2)
                self.assertIs(True, v1 > v2)
                self.assertIs(True, v1 >= v2)
            else:
                self.fail("bad cmp result")

        if invert:
            self.assert_cmp(-expected, v2, v1, False)
