import unittest
from test import support
from test.support import japy_not_implemented
from collections import UserList
from collections import UserDict

class CallTest(unittest.TestCase):
    def test(self):
        def e(a, b):
            return ' '.join(map(str, [a, b]))

        def f(*a, **k):
            return ' '.join(map(str, [a, support.sortdict(k)]))

        def g(x, *y, **z):
            return ' '.join(map(str, [x, y, support.sortdict(z)]))

        def h(j=1, a=2, h=3):
            return ' '.join(map(str, [j, a, h]))

        self.assertEqual(f(), '() {}')
        self.assertEqual(f(1), '(1,) {}')
        self.assertEqual(f(1, 2), '(1, 2) {}')
        self.assertEqual(f(1, 2, 3), '(1, 2, 3) {}')
        self.assertEqual(f(1, 2, 3, *(4, 5)), '(1, 2, 3, 4, 5) {}')
        self.assertEqual(f(1, 2, 3, *[4, 5]), '(1, 2, 3, 4, 5) {}')
        self.assertEqual(f(*[1, 2, 3], 4, 5), '(1, 2, 3, 4, 5) {}')
        self.assertEqual(f(1, 2, 3, *UserList([4, 5])), '(1, 2, 3, 4, 5) {}')
        self.assertEqual(f(1, 2, 3, *[4, 5], *[6, 7]), '(1, 2, 3, 4, 5, 6, 7) {}')
        self.assertEqual(f(1, *[2, 3], 4, *[5, 6], 7), '(1, 2, 3, 4, 5, 6, 7) {}')
        self.assertEqual(f(*UserList([1, 2]), *UserList([3, 4]), 5, *UserList([6, 7])), '(1, 2, 3, 4, 5, 6, 7) {}')

        self.assertEqual(f(1, 2, 3, **{'a':4, 'b':5}), "(1, 2, 3) {'a': 4, 'b': 5}")

        with self.assertRaises(TypeError):
            f(1, 2, **{'a': -1, 'b': 5}, **{'a': 4, 'c': 6})
        with self.assertRaises(TypeError):
            f(1, 2, **{'a': -1, 'b': 5}, a=4, c=6)
        with self.assertRaises(TypeError):
            f(1, 2, a=3, **{'a': 4}, **{'a': 5})

        self.assertEqual(f(1, 2, 3, *[4, 5], **{'a':6, 'b':7}), "(1, 2, 3, 4, 5) {'a': 6, 'b': 7}")
        self.assertEqual(f(1, 2, 3, x=4, y=5, *(6, 7), **{'a':8, 'b': 9}), "(1, 2, 3, 6, 7) {'a': 8, 'b': 9, 'x': 4, 'y': 5}")
        self.assertEqual(f(1, 2, 3, *[4, 5], **{'c': 8}, **{'a':6, 'b':7}), "(1, 2, 3, 4, 5) {'a': 6, 'b': 7, 'c': 8}")
        self.assertEqual(f(1, 2, 3, *(4, 5), x=6, y=7, **{'a':8, 'b': 9}), "(1, 2, 3, 4, 5) {'a': 8, 'b': 9, 'x': 6, 'y': 7}")

        self.assertEqual(f(1, 2, 3, **UserDict(a=4, b=5)), "(1, 2, 3) {'a': 4, 'b': 5}")
        self.assertEqual(f(1, 2, 3, *(4, 5), **UserDict(a=6, b=7)), "(1, 2, 3, 4, 5) {'a': 6, 'b': 7}")
        self.assertEqual(f(1, 2, 3, x=4, y=5, *(6, 7), **UserDict(a=8, b=9)), "(1, 2, 3, 6, 7) {'a': 8, 'b': 9, 'x': 4, 'y': 5}")
        self.assertEqual(f(1, 2, 3, *(4, 5), x=6, y=7, **UserDict(a=8, b=9)), "(1, 2, 3, 4, 5) {'a': 8, 'b': 9, 'x': 6, 'y': 7}")

        d1 = {'a':1}
        d2 = {'c':3}

        self.assertEqual(f(b=2, **d1, **d2), "() {'a': 1, 'b': 2, 'c': 3}")
        self.assertEqual(f(**d1, b=2, **d2), "() {'a': 1, 'b': 2, 'c': 3}")
        self.assertEqual(f(**d1, **d2, b=2), "() {'a': 1, 'b': 2, 'c': 3}")
        self.assertEqual(f(**d1, b=2, **d2, d=4), "() {'a': 1, 'b': 2, 'c': 3, 'd': 4}")

        with self.assertRaises(TypeError):
            e(c=4)
        with self.assertRaises(TypeError):
            g()
        with self.assertRaises(TypeError):
            g(*())
        with self.assertRaises(TypeError):
            g(*(), **{})

        self.assertEqual(g(1), "1 () {}")
        self.assertEqual(g(1, 2), "1 (2,) {}")
        self.assertEqual(g(1, 2, 3), "1 (2, 3) {}")
        self.assertEqual(g(1, 2, 3, *(4, 5)), "1 (2, 3, 4, 5) {}")

        class Nothing: pass

        with self.assertRaises(TypeError):
            g(*Nothing())

        class Nothing:
            def __len__(self): return 5

        with self.assertRaises(TypeError):
            g(*Nothing())

        class Nothing():
            def __len__(self): return 5
            def __getitem__(self, i):
                if i<3: return i
                else: raise IndexError(i)

        self.assertEqual(g(*Nothing()), "0 (1, 2) {}")

        class Nothing:
            def __init__(self): self.c = 0
            def __iter__(self): return self
            def __next__(self):
                if self.c == 4:
                    raise StopIteration
                c = self.c
                self.c += 1
                return c

        self.assertEqual(g(*Nothing()), "0 (1, 2, 3) {}")

        def broken(): raise TypeError("myerror")

        with self.assertRaises(TypeError):
            g(*(broken() for i in range(1)))
        with self.assertRaises(TypeError):
            g(*range(1), *(broken() for i in range(1)))

        class BrokenIterable1:
            def __iter__(self):
                raise TypeError('myerror')

        with self.assertRaises(TypeError):
            g(*BrokenIterable1())
        with self.assertRaises(TypeError):
            g(*range(1), *BrokenIterable1())

        class BrokenIterable2:
            def __iter__(self):
                yield 0
                raise TypeError('myerror')

        with self.assertRaises(TypeError):
            g(*BrokenIterable2())
        with self.assertRaises(TypeError):
            g(*range(1), *BrokenIterable2())

        class BrokenSequence:
            def __getitem__(self, idx):
                raise TypeError('myerror')

        with self.assertRaises(TypeError):
            g(*BrokenSequence())
        with self.assertRaises(TypeError):
            g(*range(1), *BrokenSequence())

        d = {'a': 1, 'b': 2, 'c': 3}
        d2 = d.copy()
        self.assertEqual(g(1, d=4, **d), "1 () {'a': 1, 'b': 2, 'c': 3, 'd': 4}")
        self.assertEqual(d, d2)

        def saboteur(**kw):
            kw['x'] = 'm'
            return kw

        d = {}
        kw = saboteur(a=1, **d)
        self.assertEqual(d, {})

        with self.assertRaises(TypeError):
            g(1, 2, 3, **{'x': 4, 'y': 5})
        with self.assertRaises(TypeError):
            f(**{1:2})
        with self.assertRaises(TypeError):
            h(**{'e': 2})
        with self.assertRaises(TypeError):
            h(*h)
        with self.assertRaises(TypeError):
            h(1, *h)
        with self.assertRaises(TypeError):
            h(*[1], *h)
        with self.assertRaises(TypeError):
            dir(*h)
        with self.assertRaises(TypeError):
            h(**h)
        # TODO-JAPY
        if False:
            with self.assertRaises(TypeError):
                h(**[])
        with self.assertRaises(TypeError):
            h(a=1, **h)
        # TODO-JAPY
        if False:
            with self.assertRaises(TypeError):
                h(a=1, **[])
        with self.assertRaises(TypeError):
            h(**{'a': 1}, **h)
        # TODO-JAPY
        if False:
            with self.assertRaises(TypeError):
                h(**{'a': 1}, **[])
        with self.assertRaises(TypeError):
            dir(**h)
        with self.assertRaises(TypeError):
            dir(b=1, **{'b': 1})

        from collections.abc import Mapping
        class MultiDict(Mapping):
            def __init__(self, items):
                self._items = items

            def __iter__(self):
                return (k for k, v in self._items)

            def __getitem__(self, key):
                for k, v in self._items:
                    if k == key:
                        return v
                raise KeyError(key)

            def __len__(self):
                return len(self._items)

            def keys(self):
                return [k for k, v in self._items]

            def values(self):
                return [v for k, v in self._items]

            def items(self):
                return [(k, v) for k, v in self._items]

        self.assertEqual(g(**MultiDict([('x', 1), ('y', 2)])), "1 () {'y': 2}")

        with self.assertRaises(TypeError):
            g(**MultiDict([('x', 1), ('x', 2)]))
        with self.assertRaises(TypeError):
            g(a=3, **MultiDict([('x', 1), ('x', 2)]))
        with self.assertRaises(TypeError):
            g(**MultiDict([('a', 3)]), **MultiDict([('x', 1), ('x', 2)]))

        def f2(*a, **b):
            return a, b

        d = {}
        for i in range(512):
            key = 'k%d' % i
            d[key] = i
        a, b = f2(1, *(2,3), **d)
        self.assertEqual((len(a), len(b), b == d), (3, 512, True))

        class Foo:
            def method(self, arg1, arg2):
                return arg1+arg2

        x = Foo()
        self.assertEqual(Foo.method(*(x, 1, 2)), 3)
        self.assertEqual(Foo.method(x, *(1, 2)), 3)
        self.assertEqual(Foo.method(*(1, 2, 3)), 5)
        self.assertEqual(Foo.method(1, *[2, 3]), 5)

        try:
            silence = id(1, *{})
            z = True
        except:
            z = False
        self.assertTrue(z)

        with self.assertRaises(TypeError):
            id(1, **{'foo': 1})

        # TODO-JAPY
        if False:
            class Name(str):
                def __eq__(self, other):
                    try:
                        del x[self]
                    except KeyError:
                        pass
                    return str.__eq__(self, other)

                def __hash__(self):
                    return str.__hash__(self)

            x = {Name("a"):1, Name("b"):2}
            def f(a, b):
                print(a,b)
            self.assertEqual(f(**x), "1 2")

        def f(): pass
        with self.assertRaises(TypeError):
            f(1)
        def f(a): pass
        with self.assertRaises(TypeError):
            f(1, 2)
        def f(a, b=1): pass
        with self.assertRaises(TypeError):
            f(1, 2, 3)
        def f(*, kw): pass
        with self.assertRaises(TypeError):
            f(1, kw=3)
        def f(*, kw, b): pass
        with self.assertRaises(TypeError):
            f(1, 2, 3, b=3, kw=3)
        def f(a, b=2, *, kw): pass
        with self.assertRaises(TypeError):
            f(2, 3, 4, kw=4)

        def f(*, w): pass
        with self.assertRaises(TypeError):
            f()
        with self.assertRaises(TypeError):
            f(1)
        f(w=1)
        def f(*, a, b, c, d, e): pass
        with self.assertRaises(TypeError):
            f()
        with self.assertRaises(TypeError):
            f(1, 2, 3, 4, 5)
        f(a=1, b=2, c=3, d=4, e=5)
