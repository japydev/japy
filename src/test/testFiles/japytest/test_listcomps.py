import unittest
from test.support import japy_not_implemented


class ListCompTest(unittest.TestCase):
    def test(self):
        self.assertEqual(sum([i*i for i in range(100) if i&1 == 1]), 166650)
        self.assertEqual([(i,j) for i in range(3) for j in range(4)],
                         [(0, 0), (0, 1), (0, 2), (0, 3), (1, 0), (1, 1), (1, 2), (1, 3), (2, 0), (2, 1), (2, 2), (2, 3)])
        self.assertEqual([(i,j) for i in range(4) for j in range(i)],
                         [(1, 0), (2, 0), (2, 1), (3, 0), (3, 1), (3, 2)])

        self.assertEqual([j*j for i in range(4) for j in [i+1]],
                         [1, 4, 9, 16])
        self.assertEqual([j*k for i in range(4) for j in [i+1] for k in [j+1]],
                         [2, 6, 12, 20])
        self.assertEqual([j*k for i in range(4) for j, k in [(i+1, i+2)]],
                         [2, 6, 12, 20])

        self.assertEqual([i*i for i in [*range(4)]],
                         [0, 1, 4, 9])
        self.assertEqual([i*i for i in (*range(4),)],
                         [0, 1, 4, 9])

        self.assertEqual([None for i in range(10)], [None, None, None, None, None, None, None, None, None, None])

    def test_scope(self):
        # TODO-JAPY
        if False:
            i = 20
            self.assertEqual(sum([i*i for i in range(100)]), 328350)
            self.assertEqual(i, 20)

        items = [(lambda i=i: i) for i in range(5)]
        self.assertEqual([x() for x in items], [0, 1, 2, 3, 4])

        items = [(lambda: i) for i in range(5)]
        self.assertEqual([x() for x in items], [4, 4, 4, 4, 4])

        # TODO-JAPY
        if False:
            items = [(lambda: i) for i in range(5)]
            i = 20
            self.assertEqual([x() for x in items], [4, 4, 4, 4, 4])

        items = [(lambda: y) for i in range(5)]
        y = 2
        self.assertEqual([x() for x in items], [2, 2, 2, 2, 2])

        def test_func():
            items = [(lambda i=i: i) for i in range(5)]
            return [x() for x in items]
        self.assertEqual(test_func(), [0, 1, 2, 3, 4])

        def test_func():
            items = [(lambda: i) for i in range(5)]
            return [x() for x in items]
        self.assertEqual(test_func(), [4, 4, 4, 4, 4])

        # TODO-JAPY
        if False:
            def test_func():
                items = [(lambda: i) for i in range(5)]
                i = 20
                return [x() for x in items]
            self.assertEqual(test_func(), [4, 4, 4, 4, 4])

        def test_func():
            items = [(lambda: y) for i in range(5)]
            y = 2
            return [x() for x in items]
        self.assertEqual(test_func(), [2, 2, 2, 2, 2])

    def test_syntax(self):
        with self.assertRaises(SyntaxError):
            compile('[y for y in (1,2)] = 10', '<test>', 'exec')
        with self.assertRaises(SyntaxError):
            compile('[y for y in (1,2)] += 10', '<test>', 'exec')

    def test_gen(self):
        def frange(n):
            return [i for i in range(n)]
        self.assertEqual(frange(10), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

        lrange = lambda n:  [i for i in range(n)]
        self.assertEqual(lrange(10), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

        def grange(n):
            for x in [i for i in range(n)]:
                yield x
        self.assertEqual(list(grange(5)), [0, 1, 2, 3, 4])
