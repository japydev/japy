import unittest
from test.support import japy_not_implemented


class GeneratorExpressionTest(unittest.TestCase):
    def test(self):
        self.assertEqual(sum(i * i for i in range(100) if i & 1 == 1), 166650)
        self.assertEqual(list((i, j) for i in range(3) for j in range(4)),
                         [(0, 0), (0, 1), (0, 2), (0, 3), (1, 0), (1, 1), (1, 2), (1, 3), (2, 0), (2, 1), (2, 2),
                          (2, 3)])
        self.assertEqual(list((i, j) for i in range(4) for j in range(i)),
                         [(1, 0), (2, 0), (2, 1), (3, 0), (3, 1), (3, 2)])

        self.assertEqual(list((j * j for i in range(4) for j in [i + 1])),
                         [1, 4, 9, 16])
        self.assertEqual(list((j * k for i in range(4) for j in [i + 1] for k in [j + 1])),
                         [2, 6, 12, 20])
        self.assertEqual(list((j * k for i in range(4) for j, k in [(i + 1, i + 2)])),
                         [2, 6, 12, 20])

        self.assertEqual(list((i * i for i in [*range(4)])),
                         [0, 1, 4, 9])
        self.assertEqual(list((i * i for i in (*range(4),))),
                         [0, 1, 4, 9])

    def test_induction_variable_not_exposed(self):
        i = 20
        self.assertEqual(sum(i * i for i in range(100)), 328350)
        self.assertEqual(i, 20)

    def test_first_class(self):
        g = (i * i for i in range(4))
        self.assertEqual(repr(type(g)), "<class 'generator'>")
        self.assertEqual(list(g), [0, 1, 4, 9])

    def test_next(self):
        g = (i * i for i in range(3))
        self.assertEqual(next(g), 0)
        self.assertEqual(next(g), 1)
        self.assertEqual(next(g), 4)
        self.assertRaises(StopIteration, next, g)
        self.assertRaises(StopIteration, next, g)
        self.assertEqual([], list(g))

    def test_def_func_out_of_scope(self):
        def f(n):
            return (i * i for i in range(n))

        self.assertEqual(list(f(10)), [0, 1, 4, 9, 16, 25, 36, 49, 64, 81])

        def f(n):
            return ((i, j) for i in range(3) for j in range(n))

        self.assertEqual(list(f(4)),
                         [(0, 0), (0, 1), (0, 2), (0, 3), (1, 0), (1, 1), (1, 2), (1, 3), (2, 0), (2, 1), (2, 2),
                          (2, 3)])

        def f(n):
            return ((i, j) for i in range(3) for j in range(4) if j in range(n))

        self.assertEqual(list(f(4)),
                         [(0, 0), (0, 1), (0, 2), (0, 3), (1, 0), (1, 1), (1, 2), (1, 3), (2, 0), (2, 1), (2, 2),
                          (2, 3)])
        self.assertEqual(list(f(2)),
                         [(0, 0), (0, 1), (1, 0), (1, 1), (2, 0), (2, 1)])

    def test_parentheses_required(self):
        with self.assertRaises(SyntaxError):
            compile('def f(n):\n    return i*i for i in range(n)', '<test>', 'exec')

        with self.assertRaises(SyntaxError):
            compile('dict(a = i for i in range(10))', '<test>', 'exec')

        g = dict(a = (i for i in range(10)))['a']
        self.assertEqual(list(g), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

    def test_binding1(self):
        x = 10
        g = (i * i for i in range(x))
        x = 5
        self.assertEqual(list(g), [0, 1, 4, 9, 16, 25, 36, 49, 64, 81])

    def test_binding2(self):
        with self.assertRaises(TypeError):
            (i for i in 6)

    def test_binding3(self):
        include = (2, 4, 6, 8)
        g = (i * i for i in range(10) if i in include)
        include = (1, 3, 5, 7, 9)
        self.assertEqual(list(g), [1, 9, 25, 49, 81])

    def test_binding4(self):
        g = ((i, j) for i in range(3) for j in range(x))
        x = 4
        self.assertEqual(list(g),
                         [(0, 0), (0, 1), (0, 2), (0, 3), (1, 0), (1, 1), (1, 2), (1, 3), (2, 0), (2, 1), (2, 2),
                          (2, 3)])

    def test_syntax_error_lvalue(self):
        with self.assertRaises(SyntaxError):
            compile('(y for y in (1,2)) = 10', '<test>', 'exec')

        with self.assertRaises(SyntaxError):
            compile('(y for y in (1,2)) += 10', '<test>', 'exec')

    def test_acts_like_range(self):
        yrange = lambda n: (i for i in range(n))
        self.assertEqual(list(yrange(10)), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

        def creator():
            r = yrange(5)
            print("creator", next(r))
            return r

        def caller():
            r = creator()
            for i in r:
                print("caller", i)

        caller()

        def zrange(n):
            for i in yrange(n):
                yield i

        self.assertEqual(list(zrange(5)), [0, 1, 2, 3, 4])

    def test_cannot_be_resumed_while_running(self):
        g = (next(me) for i in range(10))
        me = g
        with self.assertRaises(ValueError):
            next(me)

    def test_exception_propagation(self):
        g = (10 // i for i in (5, 0, 2))
        self.assertEqual(next(g), 2)
        with self.assertRaises(ZeroDivisionError):
            next(g)
        with self.assertRaises(StopIteration):
            next(g)

    def test_none_is_valid_return_value(self):
        self.assertEqual(list(None for i in range(10)), [None, None, None, None, None, None, None, None, None, None])

    @japy_not_implemented
    def test_generator_attributes(self):
        g = (i * i for i in range(3))
        expected = set(['gi_frame', 'gi_running'])
        self.assertTrue(set(attr for attr in dir(g) if not attr.startswith('__')) >= expected)

        from test.support import HAVE_DOCSTRINGS
        if HAVE_DOCSTRINGS:
            self.assertEqual(g.__next__.__doc__, 'Implement next(self).')

    def test_generator_attributes2(self):
        g = (i * i for i in range(3))

        import types
        self.assertTrue(isinstance(g, types.GeneratorType))

        self.assertIs(iter(g), g)

    @japy_not_implemented
    def test_generator_attributes3(self):
        g = (me.gi_running for i in (0, 1))
        me = g
        self.assertEqual(me.gi_running, 0)
        self.assertEqual(next(me), 1)
        self.assertEqual(me.gi_running, 0)

    @japy_not_implemented
    def test_weakref(self):
        import weakref
        g = (i * i for i in range(4))
        wr = weakref.ref(g)
        self.assertIs(wr(), g)
        p = weakref.proxy(g)
        self.assertEqual(list(p), [0, 1, 4, 9])
