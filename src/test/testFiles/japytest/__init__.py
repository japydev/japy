import unittest

def skip_not_implemented(*args, **kwargs):
    return unittest.skip("TODO-JAPY")(*args, *kwargs)

def main():
    runner = unittest.runner.TextTestRunner(verbosity=1, failfast=False)
    test = unittest.loader.defaultTestLoader.discover("src/test/testfiles/japytest", top_level_dir="src/test/testfiles")
    result = runner.run(test)
    if not result.wasSuccessful():
        raise Exception
