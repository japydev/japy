package org.japy.base;

public enum AugOp {
  IADD("+=", BinOp.ADD),
  ISUB("-=", BinOp.SUB),
  IMUL("*=", BinOp.MUL),
  IMATMUL("@=", BinOp.MATMUL),
  ITRUEDIV("/=", BinOp.TRUEDIV),
  IFLOORDIV("//=", BinOp.FLOORDIV),
  IMOD("%=", BinOp.MOD),
  IPOW("**=", BinOp.POW),
  IRSHIFT(">>=", BinOp.RSHIFT),
  ILSHIFT("<<=", BinOp.LSHIFT),
  IAND("&=", BinOp.AND),
  IOR("|=", BinOp.OR),
  IXOR("^=", BinOp.XOR);

  public final String op;
  public final BinOp binOp;

  AugOp(String op, BinOp binOp) {
    this.op = op;
    this.binOp = binOp;
  }
}
