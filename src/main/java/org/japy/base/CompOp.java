package org.japy.base;

public enum CompOp {
  EQ("=="),
  NE("!="),
  LT("<"),
  LE("<="),
  GT(">"),
  GE(">="),
  IS("is"),
  IS_NOT("is not"),
  IN("in"),
  NOT_IN("not in"),
  ;

  public final String op;

  CompOp(String op) {
    this.op = op;
  }
}
