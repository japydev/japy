package org.japy.base;

public enum RFConversion {
  A("a"),
  S("s"),
  R("r");

  public final String token;

  RFConversion(String token) {
    this.token = token;
  }
}
