package org.japy.base;

public enum UnaryOp {
  INVERT("unary ~"),
  POS("unary +"),
  NEG("unary -"),
  ABS("abs()"),
  ;

  public final String op;

  UnaryOp(String op) {
    this.op = op;
  }
}
