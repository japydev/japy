package org.japy.base;

public enum BinOp {
  ADD("+"),
  SUB("-"),
  MUL("*"),
  TRUEDIV("/"),
  FLOORDIV("//"),
  MOD("mod"),
  DIVMOD("divmod"),
  POW("**"),
  OR("|"),
  AND("&"),
  XOR("^"),
  LSHIFT("<<"),
  RSHIFT(">>"),
  MATMUL("@");

  public final String op;

  BinOp(String op) {
    this.op = op;
  }
}
