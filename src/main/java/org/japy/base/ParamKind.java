package org.japy.base;

public enum ParamKind {
  POS_OR_KW,
  POS_ONLY,
  KW_ONLY,
  VAR_POS,
  VAR_KW;

  public static final int IPOS_OR_KW = 0;
  public static final int IPOS_ONLY = 1;
  public static final int IKW_ONLY = 2;
  public static final int IVAR_POS = 3;
  public static final int IVAR_KW = 4;
}
