import regen
from common import *


def write_binary_op(f, name, operator, extra_param='', extra_arg=''):
    if name != 'matmul':
        f.write(f"""\
  public static IPyObj {name}(IPyObj arg1, IPyObj arg2{extra_param}, PyContext ctx) {{
    //if (arg1 instanceof IPyNumber) {{
    //  IPyObj value = ((IPyNumber)arg1).__{name}__(arg2{extra_arg}, ctx);
    //  if (value != NotImplemented) {{
    //    return value;
    //  }}
    //
    //  return callBinOp(arg2, arg1{extra_arg}, SpecialMethod.R{name.upper()}, BinOp.{name.upper()}, ctx);
    //}}

    return binOp(arg1, arg2, BinOp.{name.upper()}, SpecialMethod.{name.upper()}, SpecialMethod.R{name.upper()}, ctx);
  }}

""")
    else:
        f.write(f"""\
  public static IPyObj {name}(IPyObj arg1, IPyObj arg2{extra_param}, PyContext ctx) {{
    return binOp(arg1, arg2, BinOp.{name.upper()}, SpecialMethod.{name.upper()}, SpecialMethod.R{name.upper()}, ctx);
  }}

""")


def write_binary_iop(f, name, operator):
    if name != 'matmul':
        f.write(f"""\
  public static IPyObj i{name}(IPyObj arg1, IPyObj arg2, PyContext ctx) {{
    //if (arg1 instanceof IPyNumber) {{
    //  return ((IPyNumber)arg1).__{name}__(arg2, ctx);
    //}}
    
    return augOp(arg1, arg2, AugOp.I{name.upper()}, SpecialMethod.I{name.upper()}, ctx);
  }}

""")
    else:
        f.write(f"""\
  public static IPyObj i{name}(IPyObj arg1, IPyObj arg2, PyContext ctx) {{
    return augOp(arg1, arg2, AugOp.I{name.upper()}, SpecialMethod.I{name.upper()}, ctx);
  }}

""")


def write_unary_op(f, name, operator):
    f.write(f"""\
  public static IPyObj {name}(IPyObj obj, PyContext ctx) {{
    //if (obj instanceof IPyNumber) {{
    //  return ((IPyNumber)obj).__{name}__(ctx);
    //}}
    return unaryOp(obj, UnaryOp.{name.upper()}, SpecialMethod.{name.upper()}, ctx);
  }}

""")


def write_arithmetics(f):
    for name, operator in TERNARY_OPS.items():
        #write_binary_op(f, name, operator, ', IPyObj arg3', ', arg3')
        write_binary_op(f, name, operator)
        write_binary_iop(f, name, operator + '=')
    for name, operator in BINARY_OPS.items():
        write_binary_op(f, name, operator)
        if name not in ('divmod',):
            write_binary_iop(f, name, operator + '=')
    for name, operator in UNARY_OPS.items():
        write_unary_op(f, name, operator)


regen.regen("../kernel/misc/Arithmetics.java", write_arithmetics)
