import traceback


def regen(file: str, func):
    before = []
    after = []
    seen_begin = False
    seen_end = False
    with open(file) as f:
        for line in f.readlines():
            if not seen_begin:
                before.append(line)
                if 'BEGIN GENERATED CODE' in line:
                    seen_begin = True
            elif not seen_end:
                if 'END GENERATED CODE' in line:
                    after.append(line)
                    seen_end = True
            else:
                after.append(line)

    assert seen_begin and seen_end

    with open(file, 'wt', encoding='utf-8') as f:
        for line in before:
            f.write(line)
        # noinspection PyBroadException
        try:
            f.write('\n')
            func(f)
        except Exception:
            traceback.print_exc()
        for line in after:
            f.write(line)
