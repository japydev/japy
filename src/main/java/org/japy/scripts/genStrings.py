import regen
from codewriter import MethodBuilder
from common import COMP_OPS

T_STRING2 = 0
T_STRING4 = 1
TYPES = ['T_STRING2', 'T_STRING4']
N_TYPES = len(TYPES)

GET_VALUE = ['PyString2.v(%s)', 'PyString4.v(%s)']

COMPARISONS = {'lt': '< 0', 'le': '<= 0', 'gt': '> 0', 'ge': '>= 0'}


def get_value(i, arg):
    return GET_VALUE[i] % arg


def write_strings(f):
    for name, op in COMP_OPS.items():
        m = MethodBuilder('private static', f'__{name}__', 'PyObj', 'PyObj l, PyObj r')
        m.begin_switch('l.baseType()')
        for i in range(N_TYPES):
            m.begin_case(TYPES[i])
            m.begin_switch('r.baseType()')
            for j in range(N_TYPES):
                m.begin_case(TYPES[j])
                m.write_line(f'return PyBool.of(compare({get_value(i, "l")}, {get_value(j, "r")}) '
                             f'{COMPARISONS[name]});')
                m.end_case()
            m.begin_default()
            m.write_line('return PyBool.False;')
            m.end_case()
            m.end_switch()
            m.end_case()
        m.end_switch()
        m.write_line('throw new InternalErrorException("oops");')
        m.end()
        f.write(m.text)
        f.write('\n')


regen.regen("../../../../../z2/components/str/PyStrings.java", write_strings)
