import regen
from codewriter import *
from pdefs import *


def write_pexprkind(f):
    cw = CodeWriter(f)
    for grp in EXPR_GROUPS:
        for expr in grp:
            cw.write_line(f'{expr},')
        cw.write_line()


def write_pinstrkind(f):
    cw = CodeWriter(f)
    for instr in INSTRS:
        cw.write_line(f'{instr},')
    cw.write_line()


def generate_visit_expr():
    m = MethodBuilder('public static', 'visit', 'void', 'IPExpr expr, IPExprVisitor visitor')
    m.begin_switch('expr.exprKind()')
    for expr in EXPRS:
        m.begin_case(expr)
        m.write_line(f'visitor.visit{expr_name(expr)}(({expr_java_class(expr)})expr);')
        m.write_line('break;')
        m.end_case()
    m.begin_default()
    m.write_line('throw new InternalCompilerError(expr.exprKind().toString(), expr);')
    m.end_case()
    m.end_switch()
    m.end()
    return m


def generate_eval_expr():
    m = MethodBuilder('public static <T>', 'eval', 'T', 'IPExpr expr, IPExprEval<T> eval')
    m.begin_switch('expr.exprKind()')
    for expr in EXPRS:
        m.begin_case(expr)
        m.write_line(f'return eval.eval{expr_name(expr)}(({expr_java_class(expr)})expr);')
        m.end_case()
    m.begin_default()
    m.write_line('throw new InternalCompilerError(expr.exprKind().toString(), expr);')
    m.end_case()
    m.end_switch()
    m.end()
    return m


def write_ipexprvisitor(f):
    cw = CodeWriter(f)
    for expr in EXPRS:
        cw.write_line(f'void visit{expr_name(expr)}({expr_java_class(expr)} expr);')
    cw.write_line()


def write_ipexpreval(f):
    cw = CodeWriter(f)
    for expr in EXPRS:
        cw.write_line(f'T eval{expr_name(expr)}({expr_java_class(expr)} expr);')
    cw.write_line()


def write_pexprvisit(f):
    cw = CodeWriter(f)
    f.write(generate_visit_expr().text)
    cw.write_line()
    f.write(generate_eval_expr().text)
    cw.write_line()


def write_pinstrvisit(f):
    m = MethodBuilder('public static', 'visit', 'void', 'IPInstr instr, IPInstrVisitor visitor')
    m.begin_switch('instr.instrKind()')
    for instr in INSTRS:
        m.begin_case(instr)
        m.write_line(f'visitor.visit{instr_name(instr)}(({instr_java_class(instr)})instr);')
        m.write_line('break;')
        m.end_case()
    m.begin_default()
    m.write_line('throw new InternalCompilerError(instr.instrKind().toString(), instr);')
    m.end_case()
    m.end_switch()
    m.end()
    f.write(m.text)


def write_ipinstrvisitor(f):
    cw = CodeWriter(f)
    for instr in INSTRS:
        cw.write_line(f'void visit{instr_name(instr)}({instr_java_class(instr)} instr);')
    cw.write_line()


def write_pbaseexprvisitor(f):
    for expr in EXPRS:
        m = MethodBuilder('public', f'visit{expr_name(expr)}', 'void', f'{expr_java_class(expr)} expr', annotations=('@Override',))
        m.write_line('catchAll(expr);')
        m.end()
        f.write(m.text)
        f.write('\n')


def write_pbaseexpreval(f):
    for expr in EXPRS:
        m = MethodBuilder('public', f'eval{expr_name(expr)}', 'T', f'{expr_java_class(expr)} expr', annotations=('@Override',))
        m.write_line('return catchAll(expr);')
        m.end()
        f.write(m.text)
        f.write('\n')


regen.regen("../ir/expr/PExprKind.java", write_pexprkind)
regen.regen("../ir/visit/PExprVisit.java", write_pexprvisit)
regen.regen("../ir/visit/IPExprVisitor.java", write_ipexprvisitor)
regen.regen("../ir/visit/IPExprEval.java", write_ipexpreval)
regen.regen("../ir/instr/PInstrKind.java", write_pinstrkind)
regen.regen("../ir/visit/PInstrVisit.java", write_pinstrvisit)
regen.regen("../ir/visit/IPInstrVisitor.java", write_ipinstrvisitor)
regen.regen("../ir/visit/PBaseExprVisitor.java", write_pbaseexprvisitor)
regen.regen("../ir/visit/PBaseExprEval.java", write_pbaseexpreval)
