from pathlib import Path
from typing import Callable, Optional

import regen
from codewriter import *

EXCEPTIONS = {
    'BaseException': {
        'Exception': {
            'StopIteration': None,
            'StopAsyncIteration': None,
            'SystemError': None,
            'BufferError': None,
            'AssertionError': None,
            'AttributeError': None,
            'EOFError': None,
            'MemoryError': None,
            'ReferenceError': None,
            'TypeError': None,
            'NameError': {
                'UnboundLocalError': None,
            },
            'ArithmeticError': {
                'OverflowError': None,
                'ZeroDivisionError': None,
                'FloatingPointError': None,
            },
            'LookupError': {
                'IndexError': None,
                'KeyError': None,
            },
            'ImportError': {
                'ModuleNotFoundError': None,
            },
            'SyntaxError': {
                'IndentationError': None,
                'TabError': None,
            },
            'RuntimeError': {
                'NotImplementedError': None,
                'RecursionError': None,
            },
            'ValueError': {
                'UnicodeError': {
                    'UnicodeEncodeError': None,
                    'UnicodeDecodeError': None,
                    'UnicodeTranslateError': None,
                },
            },
            'OSError': {
                'BlockingIOError': None,
                'ChildProcessError': None,
                'ConnectionError': {
                    'BrokenPipeError': None,
                    'ConnectionAbortedError': None,
                    'ConnectionRefusedError': None,
                    'ConnectionResetError': None,
                },
                'FileExistsError': None,
                'FileNotFoundError': None,
                'InterruptedError': None,
                'DirectoryError': None,
                'NotADirectoryError': None,
                'PermissionError': None,
                'ProcessLookupError': None,
                'TimeoutError': None,
            },
            'Warning': {
                'UserWarning': None,
                'DeprecationWarning': None,
                'PendingDeprecationWarning': None,
                'SyntaxWarning': None,
                'RuntimeWarning': None,
                'FutureWarning': None,
                'ImportWarning': None,
                'UnicodeWarning': None,
                'BytesWarning': None,
                'ResourceWarning': None,
            },
        },
        'SystemExit': None,
        'KeyboardInterrupt': None,
        'GeneratorExit': None,
    },
}


def iterate(func: Callable[[str, Optional[str]], None], include_base: bool):
    def iter(trie, parent, func):
        for k, v in trie.items():
            if include_base or parent:
                func(k, parent)
            if v:
                iter(v, k, func)
    iter(EXCEPTIONS, None, func)


def generate_one_pex(ex: str, parent: Optional[str], file: Path):
    if ex in ('ModuleNotFoundError',):
        return
    with open(file, 'w') as f:
        w = CodeWriter(f, 0)
        f.write('''\
// GENERATED FILE, DO NOT EDIT
package org.japy.kernel.types.exc.px;

import org.japy.kernel.components.PyContext;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.coll.seq.PyTuple;
''')
        if parent == 'BaseException':
            w.write_line('import org.japy.kernel.types.exc.PXBaseException;')
        f.write('''\
import org.japy.kernel.types.exc.PXClass;
import org.japy.kernel.util.Args;
''')
        w.write_line()
        w.write_line(f'@SuppressWarnings("unused")')
        w.write_line(f'public class PX{ex} extends PX{parent} {{')
        f.write(f'''\
  public PX{ex}() {{
    this(PXClass.{ex});
  }}

  public PX{ex}(String message) {{
    this(PXClass.{ex}, message);
  }}

  public PX{ex}(PyTuple args) {{
    this(PXClass.{ex}, args);
  }}

  public PX{ex}(PXClass cls) {{
    super(cls);
  }}

  public PX{ex}(PXClass cls, String message) {{
    super(cls, message);
  }}

  public PX{ex}(PXClass cls, PyTuple args) {{
    super(cls, args);
  }}

  public PX{ex}(IPyClass cls, Args args, PyContext ctx) {{
    super(cls, args, ctx);
  }}
''')
        if ex == 'StopIteration':
            f.write('''
  @org.japy.kernel.types.annotations.InstanceAttribute
  public org.japy.kernel.types.IPyObj value(org.japy.kernel.components.PyContext ctx) {
    if (args.content.length == 0) {
      return org.japy.kernel.types.misc.PyNone.None;
    } else {
      return args.content[0];
    }
  }
''')
        w.write_line(f'}}')


def generate_one_jex(ex: str, parent: Optional[str], file: Path):
    with open(file, 'w') as f:
        w = CodeWriter(f, 0)
        base_exc = f'org.japy.kernel.types.exc.px.PX{ex}'
        if ex == 'ModuleNotFoundError':
            base_exc = f'org.japy.kernel.types.exc.PX{ex}'
        f.write('''\
// GENERATED FILE, DO NOT EDIT
package org.japy.kernel.types.exc.py;

import org.japy.kernel.components.PyContext;
import org.japy.kernel.types.coll.PyCollUtil;
import org.japy.kernel.types.coll.str.PyStr;
''')
        if parent == 'BaseException':
            w.write_line('import org.japy.kernel.types.exc.px.PxBaseException;')
        f.write(f'''\
import org.japy.kernel.types.exc.PXBaseException;
import {base_exc};
''')
        w.write_line()
        w.write_line(f'@SuppressWarnings("unused")')
        w.write_line(f'public class Py{ex} extends Py{parent} {{')
        f.write(f'''\
  public Py{ex}() {{
    super(new PX{ex}());
  }}

  public Py{ex}(String message) {{
    super(new PX{ex}(message), message);
  }}

  public Py{ex}(PyStr message) {{
    super(new PX{ex}(PyCollUtil.tupleOf(message)), message.toString());
  }}

  public Py{ex}(String message, PyContext ctx) {{
    super(new PX{ex}(message), ctx, message);
  }}

  public Py{ex}(String message, Throwable cause) {{
    super(new PX{ex}(message), message, cause);
  }}

  public Py{ex}(Throwable cause, PyContext ctx) {{
    super(new PX{ex}(cause.getMessage()), ctx);
  }}

  public Py{ex}(Throwable cause) {{
    super(cause.getMessage() != null ? new PX{ex}(cause.getMessage()) : new PX{ex}(), cause);
  }}

  public Py{ex}(PX{ex} pyExc) {{
    super(pyExc);
  }}

  public Py{ex}(PX{ex} pyExc, String message) {{
    super(pyExc, message);
  }}

  public Py{ex}(PX{ex} pyExc, Throwable cause) {{
    super(pyExc, cause);
  }}

  public Py{ex}(PX{ex} pyExc, String message, Throwable cause) {{
    super(pyExc, message, cause);
  }}

  public Py{ex}(PXBaseException pyExc, PyContext ctx, Throwable cause) {{
    super(pyExc, ctx, cause);
  }}

  public Py{ex}(PXBaseException pyExc, PyContext ctx) {{
    super(pyExc, ctx);
  }}

  public Py{ex}(PXBaseException pyExc, PyContext ctx, String message) {{
    super(pyExc, ctx, message);
  }}
''')
        w.write_line(f'}}')


def generate_pex(dir: Path):
    iterate(lambda ex, parent: generate_one_pex(ex, parent, dir / f'PX{ex}.java'), False)


def generate_jex(dir: Path):
    iterate(lambda ex, parent: generate_one_jex(ex, parent, dir / f'Py{ex}.java'), False)


def write_pxclass(ex, parent, f):
    if parent:
        f.write(f'''\
  public static final PXClass {ex} = new PXClass(Builtin.{ex}, {parent}, PX{ex}.class, Py{ex}.class);
''')
    else:
        f.write(f'''\
  public static final PXClass {ex} = new PXClass();
''')


def generate_pxclass(f):
    iterate(lambda ex, parent: write_pxclass(ex, parent, f), True)
    f.write('''\
    
  public enum Builtin {
''')
    iterate(lambda ex, parent: f.write(f'    {ex},\n'), True)
    f.write('''\
  }
    
''')


if __name__ == '__main__':
    generate_pex(Path('../kernel/types/exc/px'))
    generate_jex(Path('../kernel/types/exc/py'))
    regen.regen('../kernel/types/exc/PXClass.java', generate_pxclass)
