import regen

def write_methdesc(f):
    for paramCount in range(1, 8):
        type_params = ', '.join(f'P{i+1}' for i in range(paramCount))
        type_args = ', '.join(f'pt{i+1}' for i in range(paramCount))
        func_type_params = '<T, ' + ', '.join(f'P{i+1}' for i in range(paramCount)) + ', R>'
        f.write(f"""\
    public <T, R, {type_params}>
    MethDesc(Class<T> klass, String name, Class<R> retType,
             {', '.join(f'Class<P{i+1}> pt{i+1}' for i in range(paramCount))},
             @SuppressWarnings("unused") Function{paramCount+1}{func_type_params} dummy) {{
        this(klass, name, 0, retType, {type_args});
    }}

    public <T, R, {type_params}>
    MethDesc(Class<T> klass, String name, int modifiers, Class<R> retType,
             {', '.join(f'Class<P{i+1}> pt{i+1}' for i in range(paramCount))}) {{
        this.klass = klass;
        this.name = name;
        this.retType = retType;
        this.paramTypes = new Class<?>[]{{{type_args}}};
        this.modifiers = modifiers;
    }}

""")

def write_methdescv(f):
    for paramCount in range(1, 5):
        type_params = ', '.join(f'P{i+1}' for i in range(paramCount))
        type_args = ', '.join(f'pt{i+1}' for i in range(paramCount))
        func_type_params = '<T, ' + ', '.join(f'P{i+1}' for i in range(paramCount)) + '>'
        f.write(f"""\
    public <T, {type_params}>
    MethDescV(Class<T> klass, String name,
              {', '.join(f'Class<P{i+1}> pt{i+1}' for i in range(paramCount))},
              @SuppressWarnings("unused") Consumer{paramCount+1}{func_type_params} dummy) {{
        this(klass, name, 0, {type_args});
    }}

    public <T, {type_params}>
    MethDescV(Class<T> klass, String name, int modifiers,
              {', '.join(f'Class<P{i+1}> pt{i+1}' for i in range(paramCount))}) {{
        this.klass = klass;
        this.name = name;
        this.paramTypes = new Class<?>[]{{{type_args}}};
        this.modifiers = modifiers;
    }}

""")

def write_constrdesc(f):
    for paramCount in range(1, 9):
        type_params = ', '.join(f'P{i+1}' for i in range(paramCount))
        type_args = ', '.join(f'pt{i+1}' for i in range(paramCount))
        func_type_params = '<' + ', '.join(f'P{i+1}' for i in range(paramCount)) + ', T>'
        f.write(f"""\
    public <T, {type_params}>
    ConstrDesc(Class<T> klass,
               {', '.join(f'Class<P{i+1}> pt{i+1}' for i in range(paramCount))},
               @SuppressWarnings("unused") Function{paramCount}{func_type_params} dummy) {{
        this(klass, 0, {type_args});
    }}

    public <T, {type_params}>
    ConstrDesc(Class<T> klass, int modifiers,
               {', '.join(f'Class<P{i+1}> pt{i+1}' for i in range(paramCount))}) {{
        this.klass = klass;
        this.paramTypes = new Class<?>[]{{{type_args}}};
        this.modifiers = modifiers;
    }}

""")


regen.regen("../infra/types/MethDesc.java", write_methdesc)
regen.regen("../infra/types/MethDescV.java", write_methdescv)
regen.regen("../infra/types/ConstrDesc.java", write_constrdesc)
