import regen
from common import SPECIAL_METHODS


def write_pyconstants(f):
    for name in list(SPECIAL_METHODS.keys()) + ['eq', 'ne', 'bool', 'len', 'hash']:
        f.write(f"""\
    public static final PyStr __{name.upper()}__ = PyStr.ucs2("__{name.lower()}__");
""")
    f.write('\n')


def write_constants(f):
    for name in list(SPECIAL_METHODS.keys()) + ['eq', 'ne', 'bool', 'len', 'hash']:
        f.write(f"""\
    public static final String __{name.upper()}__ = "__{name.lower()}__";
""")
    f.write('\n')


regen.regen("../kernel/util/Constants.java", write_constants)
regen.regen("../kernel/util/PyConstants.java", write_pyconstants)
