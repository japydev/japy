TERNARY_OPS = {'pow': '**'}
BINARY_OPS = {'add': '+', 'sub': '-', 'mul': '*', 'matmul': '@', 'truediv': '/', 'floordiv': '//', 'mod': '%',
              'divmod': 'divmod', 'lshift': '<<', 'rshift': '>>', 'and': '&', 'xor': "^", 'or': "|", }
UNARY_OPS = {'neg': '-', 'pos': '+', 'abs': 'abs', 'invert': '~'}
EQ_OPS = {'eq': '==', 'ne': '!='}
COMP_OPS = {'lt': '<', 'le': '<=', 'gt': '>', 'ge': '>='}
ALL_COMP_OPS = EQ_OPS | COMP_OPS


class SpecialMethod:
    def __init__(self, param_count, op):
        self.param_count = param_count
        self.op = op


SPECIAL_METHODS = {
    'REPR': SpecialMethod(1, 'repr()'),
    'STR': SpecialMethod(1, 'str()'),
    'BYTES': SpecialMethod(1, 'bytes()'),
    'FORMAT': SpecialMethod(1, 'format()'),
    'LT': SpecialMethod(2, '<'),
    'LE': SpecialMethod(2, '<='),
    'GT': SpecialMethod(2, '>'),
    'GE': SpecialMethod(2, '>='),
    'GETATTR': SpecialMethod(2, '__getattr__'),
    'GETATTRIBUTE': SpecialMethod(2, '__getattribute__'),
    'SETATTR': SpecialMethod(3, '__setattr__'),
    'DELATTR': SpecialMethod(2, '__delattr__'),
    'DIR': SpecialMethod(1, 'dir()'),
    'LENGTH_HINT': SpecialMethod(1, '__length_hint__'),
    'GETITEM': SpecialMethod(2, '__getitem__'),
    'SETITEM': SpecialMethod(3, '__setitem__'),
    'DELITEM': SpecialMethod(2, '__delitem__'),
    'MISSING': SpecialMethod(2, '__missing__'),
    'ITER': SpecialMethod(1, 'iter()'),
    'REVERSED': SpecialMethod(1, 'reversed()'),
    'CONTAINS': SpecialMethod(2, 'in'),
    'ADD': SpecialMethod(2, '+'),
    'SUB': SpecialMethod(2, '-'),
    'MUL': SpecialMethod(2, '*'),
    'MATMUL': SpecialMethod(2, '@'),
    'DIV': SpecialMethod(2, '/'),
    'FLOORDIV': SpecialMethod(2, '//'),
    'MOD': SpecialMethod(2, '%'),
    'DIVMOD': SpecialMethod(2, 'divmod()'),
    'POW': SpecialMethod(3, '**'),
    'LSHIFT': SpecialMethod(2, '<<'),
    'RSHIFT': SpecialMethod(2, '>>'),
    'AND': SpecialMethod(2, '&'),
    'OR': SpecialMethod(2, '|'),
    'XOR': SpecialMethod(2, '^'),
    'RADD': SpecialMethod(2, '+'),
    'RSUB': SpecialMethod(2, '-'),
    'RMUL': SpecialMethod(2, '*'),
    'RMATMUL': SpecialMethod(2, '@'),
    'RDIV': SpecialMethod(2, '/'),
    'RFLOORDIV': SpecialMethod(2, '//'),
    'RMOD': SpecialMethod(2, '%'),
    'RDIVMOD': SpecialMethod(2, 'divmod'),
    'RPOW': SpecialMethod(3, '**'),
    'RLSHIFT': SpecialMethod(2, '<<'),
    'RRSHIFT': SpecialMethod(2, '>>'),
    'RAND': SpecialMethod(2, '&'),
    'ROR': SpecialMethod(2, '|'),
    'RXOR': SpecialMethod(2, '^'),
    'IADD': SpecialMethod(2, '+='),
    'ISUB': SpecialMethod(2, '-='),
    'IMUL': SpecialMethod(2, '*='),
    'IMATMUL': SpecialMethod(2, '@='),
    'IDIV': SpecialMethod(2, '/='),
    'IFLOORDIV': SpecialMethod(2, '//='),
    'IMOD': SpecialMethod(2, '%='),
    'IPOW': SpecialMethod(3, '**='),
    'ILSHIFT': SpecialMethod(2, '<<='),
    'IRSHIFT': SpecialMethod(2, '>>='),
    'IAND': SpecialMethod(2, '&='),
    'IOR': SpecialMethod(2, '|='),
    'IXOR': SpecialMethod(2, '^='),
    'NEG': SpecialMethod(1, '-'),
    'POS': SpecialMethod(1, '+'),
    'ABS': SpecialMethod(1, 'abs()'),
    'INVERT': SpecialMethod(1, '~'),
    'COMPLEX': SpecialMethod(1, 'complex()'),
    'INT': SpecialMethod(1, 'int()'),
    'FLOAT': SpecialMethod(1, 'float()'),
    'INDEX': SpecialMethod(1, 'index()'),
    'ROUND': SpecialMethod(2, 'round()'),
    'TRUNC': SpecialMethod(1, 'trunc()'),
    'FLOOR': SpecialMethod(1, 'floor()'),
    'CEIL': SpecialMethod(1, 'ceil()'),
    'ENTER': SpecialMethod(1, '__enter__'),
    'EXIT': SpecialMethod(4, '__exit__'),
}
