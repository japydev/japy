INDENT_SIZE = 2

class CodeWriter:
    def __init__(self, out, indent=1):
        self.out = out
        self.indent_size = INDENT_SIZE
        self.indent_string = ' ' * (self.indent_size * indent)

    def write_line(self, text=None):
        if text is not None:
            self.out.write(self.indent_string)
            self.out.write(text)
        self.out.write('\n')

    def indent(self, howmany=1):
        self.indent_size += self.indent_size * howmany
        self.indent_string = ' ' * self.indent_size

    def unindent(self, howmany=1):
        self.indent_size -= self.indent_size * howmany
        self.indent_string = ' ' * self.indent_size


class MethodBuilder:
    def __init__(self, flags, name, ret_type, args, annotations=()):
        self.indent_size = INDENT_SIZE
        self.indent_string = ' ' * INDENT_SIZE
        self.text = ''
        self.in_if = 0
        for ann in annotations:
            self.write_line(ann)
        self.write_line(f'{flags} {ret_type} {name}({args}) {{')
        self.indent()

    def annotation(self, annotation):
        self.text = ' ' * INDENT_SIZE + annotation + '\n' + self.text

    def end(self):
        self.unindent()
        self.write_line('}')

    def begin_switch(self, on_what):
        self.write_line(f'switch ({on_what}) {{')
        self.indent()

    def end_switch(self):
        self.unindent()
        self.write_line('}')

    def begin_case(self, value):
        self.write_line(f'case {value}:')
        self.indent()

    def begin_default(self):
        self.write_line(f'default:')
        self.indent()

    def end_case(self):
        self.unindent()

    def begin_try(self):
        self.write_line('try {')
        self.indent()

    def begin_catch(self, what):
        self.unindent()
        self.write_line(f'}} catch ({what}) {{')
        self.indent()

    def end_try(self):
        self.unindent()
        self.write_line('}')

    def begin_if(self, what):
        self.write_line(f"if ({what}) {{")
        self.in_if += 1
        self.indent()

    def begin_elif(self, what):
        assert self.in_if > 0
        self.unindent()
        self.write_line(f"}} else if ({what}) {{")
        self.indent()

    def begin_else(self):
        assert self.in_if > 0
        self.unindent()
        self.write_line(f"}} else {{")
        self.indent()

    def end_if(self):
        assert self.in_if > 0
        self.unindent()
        self.write_line('}')
        self.in_if -= 1

    def write_line(self, text=None):
        if text is not None:
            self.write(self.indent_string)
            self.write(text)
            self.write('\n')

    def write(self, text):
        self.text += text

    def indent(self):
        self.indent_size += INDENT_SIZE
        self.indent_string = ' ' * self.indent_size

    def unindent(self):
        self.indent_size -= INDENT_SIZE
        self.indent_string = ' ' * self.indent_size
