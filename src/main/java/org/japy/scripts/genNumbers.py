from typing import Dict, List, Optional

import regen
from common import *
from codewriter import MethodBuilder


class PyTypeData:
    def __init__(self, name, suffix, java_type):
        self.name = name
        self.java_type = java_type
        self.suffix = suffix


PY_INT = 0
PY_FLOAT = 1
PY_COMPLEX = 2
N_PY_TYPES = 3

PY_TYPE_DATA = [PyTypeData('int', 'I', 'PyInt'),
                PyTypeData('float', 'F', 'PyFloat'),
                PyTypeData('complex', 'C', 'PyComplex')]

CAST_OBJECT = ['(PyInt)%s', '(PyFloat)%s', '(PyComplex)%s']
WIDEN_SELF_TO_DOUBLE = ['IntLib.toDouble(self, ctx)', 'self.value', 'self']
WIDEN_TO_DOUBLE = ['IntLib.toDouble((PyInt)%s, ctx)', '((PyFloat)%s).value', '(PyComplex)%s']


NEED_CONTEXT = {'pow', 'truediv', 'floordiv', 'mod', 'divmod', 'lshift', 'rshift'}
INT_OPS = {'lshift', 'rshift', 'and', 'or', 'xor', 'invert'}
REAL_OPS = {'mod', 'divmod', 'floordiv'}


class UnaryOpData:
    def __init__(self, name):
        self.need_context = name in NEED_CONTEXT

class BinaryOpData:
    def __init__(self, name):
        self.need_context = name in NEED_CONTEXT


UNARY_OP_DATA: Dict[str, Optional[List[Optional[UnaryOpData]]]] = dict()
BINARY_OP_DATA: Dict[str, Optional[List[Optional[List[Optional[BinaryOpData]]]]]] = dict()


def unary_op_data(name, py_type):
    return UnaryOpData(name) if name != 'pos' and (name not in INT_OPS or py_type == PY_INT) else None

def binary_op_data(name, py_type_lhs, py_type_rhs):
    if name in INT_OPS:
        return BinaryOpData(name) if py_type_lhs == PY_INT and py_type_rhs == PY_INT else None
    if name in REAL_OPS:
        return BinaryOpData(name) if py_type_lhs != PY_COMPLEX and py_type_rhs != PY_COMPLEX else None
    return BinaryOpData(name)

def setup_data():
    for name, op in UNARY_OPS.items():
        UNARY_OP_DATA[name] = [unary_op_data(name, py_type) for py_type in range(N_PY_TYPES)]
    for name, op in BINARY_OPS.items():
        BINARY_OP_DATA[name] = [[binary_op_data(name, py_type_lhs, py_type_rhs) for py_type_rhs in range(N_PY_TYPES)] for py_type_lhs in range(N_PY_TYPES)]
    for name, op in TERNARY_OPS.items():
        BINARY_OP_DATA[name] = [[binary_op_data(name, py_type_lhs, py_type_rhs) for py_type_rhs in range(N_PY_TYPES)] for py_type_lhs in range(N_PY_TYPES)]
    for name, op in COMP_OPS.items():
        BINARY_OP_DATA[name] = [[BinaryOpData(name) for _ in range(N_PY_TYPES)] for _ in range(N_PY_TYPES)]
    for name in BINARY_OPS:
        for py_type in range(N_PY_TYPES):
            if all(x is None for x in BINARY_OP_DATA[name][py_type]):
                BINARY_OP_DATA[name][py_type] = None

    for op in COMP_OPS:
        BINARY_OP_DATA[op][PY_COMPLEX] = None
        for py_type in range(N_PY_TYPES):
            if BINARY_OP_DATA[op][py_type] is not None:
                BINARY_OP_DATA[op][py_type][PY_COMPLEX] = None
    BINARY_OP_DATA['ne'] = None
    BINARY_OP_DATA['matmul'] = None


setup_data()


def get_suffix(i):
    return PY_TYPE_DATA[i].suffix


def cast_object(arg, i):
    return CAST_OBJECT[i] % arg

def widen_to_double(arg, i):
    return WIDEN_TO_DOUBLE[i] % arg

def widen_self_to_double(i):
    return WIDEN_SELF_TO_DOUBLE[i]


def get_java_class(py_type):
    return f'Py{PY_TYPE_DATA[py_type].name.capitalize()}'


def get_ops_class(py_type):
    return f'{PY_TYPE_DATA[py_type].name.capitalize()}Ops'


def switch_on_right(m, op_name, left_value, extra_arg):
    ctx = ', ctx' if op_name in NEED_CONTEXT else ''
    m.begin_if('r.obj == null')
    m.write_line(f'return {op_name}({left_value}, r.data{extra_arg}{ctx});')
    m.begin_elif('r.isLongMask()')
    m.write_line(f'return {op_name}({left_value}, (long)r.obj{extra_arg}{ctx});')
    m.begin_else()
    m.write_line(f'return {op_name}({left_value}, (BigInteger)r.obj{extra_arg}{ctx});')
    m.end_if()


def generate_concrete_int(op_name, ret_type, extra_param, extra_arg):
    ctx = ', PyContext ctx' if op_name in NEED_CONTEXT else ''
    m = MethodBuilder('private static', f'{op_name}', ret_type, f'PyInt l, PyInt r{extra_param}{ctx}')
    m.begin_if('l.obj == null')
    switch_on_right(m, op_name, 'l.data', extra_arg)
    m.begin_elif('l.isLongMask()')
    switch_on_right(m, op_name, '(long)l.obj', extra_arg)
    m.begin_else()
    switch_on_right(m, op_name, '(BigInteger)l.obj', extra_arg)
    m.end_if()
    m.end()
    return m


def generate_concrete(methods, name, i, j, ret_type, extra_param, extra_arg):
    if i == PY_INT and j == PY_INT and name not in COMP_OPS and name not in ('truediv',):
        methods.append(generate_concrete_int(name, ret_type, extra_param, extra_arg))


def generate_binary_methods(py_type, ops, op_data, extra_param='', extra_arg=''):
    methods = []

    for name, op in ops.items():
        if op_data[name] is None:
            continue
        if op_data[name][py_type] is None:
            continue
        if extra_param and py_type in (PY_FLOAT, PY_COMPLEX):
            continue
        self_type = PY_TYPE_DATA[py_type].java_type
        ctx_param = ', PyContext ctx'
        m = MethodBuilder('static', name, 'IPyObj',
                          f'{self_type} self, IPyObj obj{extra_param}{ctx_param}')
        started_if = False
        for py_type_other in range(N_PY_TYPES):
            d = op_data[name][py_type][py_type_other]
            if d is None:
                continue
            if extra_param and py_type_other in (PY_FLOAT, PY_COMPLEX):
                continue
            if started_if:
                m.begin_elif(f'obj instanceof {PY_TYPE_DATA[py_type_other].java_type}')
            else:
                m.begin_if(f'obj instanceof {PY_TYPE_DATA[py_type_other].java_type}')
                started_if = True
            ctx_arg = ', ctx' if name in NEED_CONTEXT else ''
            if py_type in (PY_FLOAT, PY_COMPLEX) or py_type_other in (PY_FLOAT, PY_COMPLEX):
                self = widen_self_to_double(py_type)
                arg = widen_to_double('obj', py_type_other)
            else:
                self = 'self'
                arg = cast_object('obj', py_type_other)
            m.write_line(f'return {name}({self}, {arg}{extra_arg}{ctx_arg});')
            generate_concrete(methods, name, py_type, py_type_other, 'IPyObj', extra_param, extra_arg)
        assert started_if
        if started_if:
            m.end_if()
        m.write_line()
        m.write_line(f'return NotImplemented;')
        m.end()
        methods.append(m)

    return methods


def generate_unary_methods(py_type):
    methods = []

    for name, op in UNARY_OPS.items():
        if UNARY_OP_DATA[name] is None:
            continue
        if UNARY_OP_DATA[name][py_type] is None:
            continue
        # m = MethodBuilder('public static', f'__{name}__', 'IPyObj', f'IPyObj self, PyContext ctx')
        # m.annotation('@InstanceMethod')
        # if name == 'pos':
        #     m.write_line(f'return self;')
        # else:
        #     m.write_line(f'return {name}({get_object("self", py_type)}, ctx);')
        # m.end()
        # methods.append(m)
        if name in NEED_CONTEXT:
            ctx_param = ', PyContext ctx'
            ctx_arg = ', arg'
        else:
            ctx_param = ''
            ctx_arg = ''
        if name != 'pos':
            m = MethodBuilder('public static', f'{name}', 'IPyObj', f'IPyObj self{ctx_param}')
            m.write_line(f'return {name}({cast_object("self", py_type)}{ctx_arg});')
            m.end()
            methods.append(m)

    return methods


def write_methods(f, methods):
    for m in methods:
        f.write(m.text)
        f.write('\n')


def write_concrete_type(f, py_type):
    # write_methods(f, generate_unary_methods(py_type))
    # write_methods(f, generate_binary_methods(py_type, COMP_OPS, BINARY_OP_DATA))
    write_methods(f, generate_binary_methods(py_type, BINARY_OPS, BINARY_OP_DATA))
    write_methods(f, generate_binary_methods(py_type, TERNARY_OPS, BINARY_OP_DATA))
    write_methods(f, generate_binary_methods(py_type, TERNARY_OPS, BINARY_OP_DATA, ', IPyObj modulo',
                                             ', modulo'))


def generate_unary_methods_impl(py_type):
    methods = []

    for name, op in UNARY_OPS.items():
        if UNARY_OP_DATA[name] is None or UNARY_OP_DATA[name][py_type] is None:
            continue
        m = MethodBuilder('public', f'__{name}__', 'IPyObj', f'PyContext ctx')
        m.annotation('@Override')
        if name == 'pos':
            m.write_line(f'return this;')
        else:
            m.write_line(f'return {get_ops_class(py_type)}.{name}(this);')
        m.end()
        methods.append(m)

    return methods


def generate_binary_methods_impl(py_type, ops, op_data, extra_param='', extra_arg=''):
    methods = []

    for name, op in ops.items():
        if name in ('matmul', 'pow') or op_data[name] is None or op_data[name][py_type] is None:
            continue
        if extra_param and py_type in (PY_FLOAT, PY_COMPLEX):
            continue
        flags = 'public'
        m = MethodBuilder(flags, f'__{name}__', 'IPyObj', f'IPyObj obj{extra_param}, PyContext ctx')
        m.annotation('@Override')
        ctx_arg = ', ctx'
        m.write_line(f'return {get_ops_class(py_type)}.{name}(this, obj{extra_arg}{ctx_arg});')
        m.end()
        methods.append(m)

    return methods


def write_interface_impl(f, py_type):
    write_methods(f, generate_unary_methods_impl(py_type))
    write_methods(f, generate_binary_methods_impl(py_type, BINARY_OPS, BINARY_OP_DATA))
    write_methods(f, generate_binary_methods_impl(py_type, TERNARY_OPS, BINARY_OP_DATA))
    write_methods(f, generate_binary_methods_impl(py_type, TERNARY_OPS, BINARY_OP_DATA, ', IPyObj arg',
                                                  ', arg'))
    # write_methods(f, generate_binary_methods_impl(py_type, COMP_OPS, BINARY_OP_DATA))


def main():
    for py_type in range(N_PY_TYPES):
        regen.regen(f"../kernel/types/num/{get_ops_class(py_type)}.java",
                    lambda f: write_concrete_type(f, py_type))
        regen.regen(f"../kernel/types/num/{get_java_class(py_type)}.java",
                    lambda f: write_interface_impl(f, py_type))


main()
