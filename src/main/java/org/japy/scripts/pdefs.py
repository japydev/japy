EXPR_GROUPS = [
    ['CONST', 'STRING'],
    ['SLICE', 'SEQUENCE', 'DICT', 'MAKE_SEQUENCE', 'MAKE_DICT', 'COMP'],
    ['UNARY_OP', 'BIN_OP', 'COMPARISON', 'NOT', 'LOGICAL_OP', 'COND'],
    ['TEMP', 'IDENTIFIER'],
    ['WALRUS'],
    ['ATTR_REF', 'CLASS_ATTR_REF', 'SUBSCRIPTION', 'CALL', 'METH_CALL'],
    ['MAKE_FUNC', 'MAKE_CLASS'],
    ['YIELD', 'YIELD_FROM', 'AWAIT'],
    ['EXC_INFO'],
    ['OOPS'],
]

EXPRS = [e for grp in EXPR_GROUPS for e in grp]

INSTRS = [
    'EXPR',
    'RETURN',
    'RAISE',
    'ASSIGN',
    'DEL',
    'AUG_ASSIGN',
    'ASSERT',
    'IMPORT_FROM',
    'IMPORT',
    'ITER',
    'NEXT_ITER',
    'JUMP',
    'JUMP_IF',
    'PUSH_SCOPE',
    'POP_SCOPE',
    'FREE_TEMP',
    'CREATE_LIST',
    'ADD_LIST',
    'APPEND_LIST',
    'APPEND_DICT',
    'BEGIN_TRY',
    'EXCEPT',
    'EXCEPT_ELSE',
    'JUMP_AFTER_FINALLY',
    'FINALLY',
    'END_TRY',
    'SET_LOC',
    'OOPS',
]


def expr_name(e: str):
    return ''.join(p.capitalize() for p in e.split('_'))


def expr_java_class(e: str):
    return 'P' + expr_name(e)


def instr_name(instr: str):
    if instr == 'EXPR':
        return 'ExprInstr'
    return ''.join(p.capitalize() for p in instr.split('_'))


def instr_java_class(instr: str):
    return 'P' + instr_name(instr)
