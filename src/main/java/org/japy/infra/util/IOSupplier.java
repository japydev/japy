package org.japy.infra.util;

import java.io.IOException;

public interface IOSupplier<T> {
  T get() throws IOException;
}
