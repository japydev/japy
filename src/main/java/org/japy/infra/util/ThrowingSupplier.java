package org.japy.infra.util;

public interface ThrowingSupplier<T> {
  T get() throws Throwable;
}
