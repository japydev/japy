package org.japy.infra.util;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang3.concurrent.ConcurrentException;

import org.japy.infra.exc.InternalErrorException;

public class ExcUtil {
  public static void wrapIO(IORunnable func) {
    try {
      func.run();
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    }
  }

  public static <T> T wrapIO(IOSupplier<T> func) {
    try {
      return func.get();
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    }
  }

  public static void wrapVoid(ThrowingRunnable func) {
    wrap(func);
  }

  public static void wrap(ThrowingRunnable func) {
    try {
      func.run();
    } catch (Throwable ex) {
      throw rethrow(ex);
    }
  }

  public static <T> T wrap(ThrowingSupplier<T> func) {
    try {
      return func.get();
    } catch (Throwable ex) {
      throw rethrow(ex);
    }
  }

  public static RuntimeException rethrow(Throwable ex) {
    try {
      throw ex;
    } catch (RuntimeException | Error typedEx) {
      throw typedEx;
    } catch (InvocationTargetException | ConcurrentException invocEx) {
      throw rethrow(invocEx.getCause());
    } catch (Throwable t) {
      throw new InternalErrorException(t);
    }
  }
}
