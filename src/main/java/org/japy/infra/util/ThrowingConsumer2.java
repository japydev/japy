package org.japy.infra.util;

public interface ThrowingConsumer2<T1, T2> {
  @SuppressWarnings("RedundantThrows")
  void accept(T1 t1, T2 t2) throws Throwable;
}
