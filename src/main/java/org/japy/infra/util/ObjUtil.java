package org.japy.infra.util;

import java.util.function.Supplier;

import org.checkerframework.checker.nullness.qual.Nullable;

public final class ObjUtil {
  private ObjUtil() {}

  public static <T> T or(@Nullable T obj1, T obj2) {
    return obj1 != null ? obj1 : obj2;
  }

  public static <T> T or(@Nullable T obj1, @Nullable T obj2, T obj3) {
    return obj1 != null ? obj1 : or(obj2, obj3);
  }

  public static <T> T or(@Nullable T obj1, Supplier<T> obj2) {
    return obj1 != null ? obj1 : obj2.get();
  }
}
