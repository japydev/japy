package org.japy.infra.util;

import java.io.IOException;

public interface IORunnable {
  void run() throws IOException;
}
