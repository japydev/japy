package org.japy.infra.util;

public interface ThrowingConsumer1<T> {
  @SuppressWarnings("RedundantThrows")
  void accept(T t) throws Throwable;
}
