package org.japy.infra.util;

public interface ThrowingRunnable {
  void run() throws Throwable;
}
