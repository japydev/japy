package org.japy.infra.coll.ctx;

import com.google.common.primitives.Ints;
import com.google.errorprone.annotations.CanIgnoreReturnValue;

public interface CtxAwareSet<E extends CtxAwareObject<C>, C> extends Iterable<E> {
  boolean isEmpty();

  int size();

  void clear();

  boolean contains(E o, C ctx);

  @CanIgnoreReturnValue
  boolean remove(E o, C ctx);

  @CanIgnoreReturnValue
  boolean add(E o, C ctx);

  int hashCode(C ctx);

  boolean equals(Object obj, C ctx);

  static int capacity(int expectedSize) {
    if (expectedSize < 3) {
      return expectedSize + 1;
    }
    if (expectedSize < Ints.MAX_POWER_OF_TWO) {
      // This is the calculation used in JDK8 to resize when a putAll
      // happens; it seems to be the most conservative calculation we
      // can make.  0.75 is the default load factor.
      return (int) (expectedSize / 0.75F + 1.0F);
    }
    return Integer.MAX_VALUE;
  }

  default boolean isSubSet(CtxAwareSet<E, C> set, C ctx) {
    if (size() > set.size()) {
      return false;
    }
    for (E e : this) {
      if (!set.contains(e, ctx)) {
        return false;
      }
    }
    return true;
  }

  default boolean isDisjoint(CtxAwareSet<E, C> set, C ctx) {
    for (E e : this) {
      if (set.contains(e, ctx)) {
        return false;
      }
    }
    for (E e : set) {
      if (contains(e, ctx)) {
        return false;
      }
    }
    return true;
  }
}
