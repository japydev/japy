package org.japy.infra.coll.ctx;

import java.util.Collection;

@SuppressWarnings("unused")
public class CtxAwareHashSet<E extends CtxAwareObject<C>, C> extends HashedSet<E, C> {
  public CtxAwareHashSet() {}

  public CtxAwareHashSet(int initialCapacity) {
    super(initialCapacity);
  }

  public CtxAwareHashSet(Collection<E> content, C ctx) {
    super(CtxAwareSet.capacity(content.size()));
    for (E o : content) {
      add(o, ctx);
    }
  }

  public CtxAwareHashSet(CtxAwareSet<E, C> content, C ctx) {
    super(CtxAwareSet.capacity(content.size()));
    for (E o : content) {
      add(o, ctx);
    }
  }

  public static <E extends CtxAwareObject<C>, C> CtxAwareHashSet<E, C> withExpectedSize(
      int expectedSize) {
    return new CtxAwareHashSet<>(CtxAwareSet.capacity(expectedSize));
  }
}
