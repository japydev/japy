package org.japy.infra.coll.ctx;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;

import com.google.errorprone.annotations.Var;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.collections4.iterators.EmptyIterator;

import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Debug;

public class CtxAwareCollections {
  public static <E extends CtxAwareObject<C>, C> boolean equalElements(E lhs, E rhs, C ctx) {
    // This is how python collections compare elements. Note that it's different from
    // just isEqual() for objects like NAN.
    return lhs == rhs || lhs.isEqual(rhs, ctx);
  }

  public static <E extends CtxAwareObject<C>, C> boolean equals(List<E> lhs, List<E> rhs, C ctx) {
    if (lhs.size() != rhs.size()) {
      return false;
    }
    for (int i = 0, c = lhs.size(); i != c; ++i) {
      if (!equalElements(lhs.get(i), rhs.get(i), ctx)) {
        return false;
      }
    }
    return true;
  }

  public static <E extends CtxAwareObject<C>, C> boolean equals(E[] lhs, E[] rhs, C ctx) {
    if (lhs.length != rhs.length) {
      return false;
    }
    for (int i = 0, c = lhs.length; i != c; ++i) {
      if (!equalElements(lhs[i], rhs[i], ctx)) {
        return false;
      }
    }
    return true;
  }

  @SafeVarargs
  public static <E extends CtxAwareObject<C>, C> int hash(C ctx, E... a) {
    @Var int result = 1;

    for (E element : a) {
      result = 31 * result + element.hashCode(ctx);
    }

    return result;
  }

  public static <E extends CtxAwareObject<C>, C> int hash(C ctx, List<E> list) {
    @Var int result = 1;

    for (E element : list) {
      result = 31 * result + element.hashCode(ctx);
    }

    return result;
  }

  public static <E extends CtxAwareObject<C>, C> int hash(C ctx, E e1) {
    @Var int result = 1;
    result = 31 * result + e1.hashCode(ctx);
    return result;
  }

  public static <E extends CtxAwareObject<C>, C> int hash(C ctx, E e1, E e2) {
    @Var int result = 1;
    result = 31 * result + e1.hashCode(ctx);
    result = 31 * result + e2.hashCode(ctx);
    return result;
  }

  public static <E extends CtxAwareObject<C>, C> int hash(C ctx, E e1, E e2, E e3) {
    @Var int result = 1;
    result = 31 * result + e1.hashCode(ctx);
    result = 31 * result + e2.hashCode(ctx);
    result = 31 * result + e3.hashCode(ctx);
    return result;
  }

  public static <E extends CtxAwareObject<C>, C> CtxAwareSet<E, C> immutableSet() {
    return new ImmEmptySet<>();
  }

  public static <E extends CtxAwareObject<C>, C> CtxAwareSet<E, C> immutableSet(E e1) {
    return new ImmSet1<>(e1);
  }

  public static <E extends CtxAwareObject<C>, C> CtxAwareSet<E, C> immutableSet(C ctx, E e1, E e2) {
    if (e1.isEqual(e2, ctx)) {
      return new ImmSet1<>(e1);
    } else {
      return new ImmSet2<>(e1, e2, ctx);
    }
  }

  @SafeVarargs
  @SuppressWarnings("varargs")
  public static <E extends CtxAwareObject<C>, C> CtxAwareSet<E, C> immutableSet(
      C ctx, E... elements) {
    switch (elements.length) {
      case 0:
        return immutableSet();
      case 1:
        return immutableSet(elements[0]);
      case 2:
        return immutableSet(ctx, elements[0], elements[1]);
      case 3:
      case 4:
      case 5:
      case 6:
        return new ImmSetList<>(elements, ctx);
      default:
        return new ImmBigSet<>(elements, ctx);
    }
  }

  private abstract static class ImmSet<E extends CtxAwareObject<C>, C>
      implements CtxAwareSet<E, C> {
    @Override
    public void clear() {
      throw new IllegalArgumentException();
    }

    @Override
    public boolean remove(E o, C ctx) {
      throw new IllegalArgumentException();
    }

    @Override
    public boolean add(E o, C ctx) {
      throw new IllegalArgumentException();
    }
  }

  private static class ImmEmptySet<E extends CtxAwareObject<C>, C> extends ImmSet<E, C> {
    @Override
    public boolean isEmpty() {
      return true;
    }

    @Override
    public int size() {
      return 0;
    }

    @Override
    public boolean contains(E o, C ctx) {
      return false;
    }

    @Override
    public int hashCode(C ctx) {
      return 0;
    }

    @Override
    public boolean equals(Object obj, C ctx) {
      return (obj instanceof CtxAwareSet && ((CtxAwareSet<?, ?>) obj).isEmpty());
    }

    @Nonnull
    @Override
    public Iterator<E> iterator() {
      return EmptyIterator.emptyIterator();
    }
  }

  private static class ImmSet1<E extends CtxAwareObject<C>, C> extends ImmSet<E, C> {
    private final E e1;

    private ImmSet1(E e1) {
      this.e1 = e1;
    }

    @Override
    public boolean isEmpty() {
      return false;
    }

    @Override
    public int size() {
      return 1;
    }

    @Override
    public boolean contains(E o, C ctx) {
      return o == e1 || o.isEqual(e1, ctx);
    }

    @Override
    public int hashCode(C ctx) {
      return hash(ctx, e1);
    }

    @Override
    public boolean equals(Object obj, C ctx) {
      throw new NotImplementedException("equals");
    }

    @Nonnull
    @Override
    public Iterator<E> iterator() {
      return IteratorUtils.singletonIterator(e1);
    }
  }

  private static class ImmSet2<E extends CtxAwareObject<C>, C> extends ImmSet<E, C> {
    private final E e1;
    private final E e2;

    private ImmSet2(E e1, E e2, C ctx) {
      if (Debug.ENABLED) {
        dcheck(!e1.isEqual(e2, ctx));
      }
      this.e1 = e1;
      this.e2 = e2;
    }

    @Override
    public boolean isEmpty() {
      return false;
    }

    @Override
    public int size() {
      return 2;
    }

    @Override
    public boolean contains(E o, C ctx) {
      return o == e1 || o == e2 || o.isEqual(e1, ctx) || o.isEqual(e2, ctx);
    }

    @Override
    public int hashCode(C ctx) {
      // zzz: hash depends on the order
      return hash(ctx, e1, e2);
    }

    @Override
    public boolean equals(Object obj, C ctx) {
      throw new NotImplementedException("equals");
    }

    @Nonnull
    @Override
    public Iterator<E> iterator() {
      throw new NotImplementedException("iterator");
    }
  }

  private static class ImmSetList<E extends CtxAwareObject<C>, C> extends ImmSet<E, C> {
    private final CtxAwareObject<C>[] elements;
    private final int size;

    @SuppressWarnings({"unchecked", "rawtypes", "RedundantSuppression"})
    private ImmSetList(E[] elements, C ctx) {
      this.elements = new CtxAwareObject[elements.length];
      @SuppressWarnings("LocalVariableHidesMemberVariable")
      @Var
      int size = 0;
      for (E e : elements) {
        if (!contains(this.elements, size, e, ctx)) {
          this.elements[size++] = e;
          ++size;
        }
      }
      this.size = size;
    }

    @Override
    public boolean isEmpty() {
      return size == 0;
    }

    @Override
    public int size() {
      return size;
    }

    @Override
    public boolean contains(E o, C ctx) {
      return contains(elements, size, o, ctx);
    }

    private static <E extends CtxAwareObject<C>, C> boolean contains(
        Object[] elements, int size, E o, C ctx) {
      for (int i = 0; i != size; ++i) {
        if (o == elements[i] || o.isEqual(elements[i], ctx)) {
          return true;
        }
      }
      return false;
    }

    @Override
    public int hashCode(C ctx) {
      return CtxAwareCollections.hash(ctx, elements);
    }

    @Override
    public boolean equals(Object obj, C ctx) {
      throw new NotImplementedException("equals");
    }

    @Nonnull
    @Override
    public Iterator<E> iterator() {
      throw new NotImplementedException("iterator");
    }
  }

  private static class ImmBigSet<E extends CtxAwareObject<C>, C> extends CtxAwareHashSet<E, C> {
    private ImmBigSet(E[] elements, C ctx) {
      super(CtxAwareSet.capacity(elements.length));
      for (E e : elements) {
        super.add(e, ctx);
      }
    }

    @Override
    public boolean add(E key, C ctx) {
      throw new IllegalArgumentException();
    }

    @Override
    public boolean remove(E key, C ctx) {
      throw new IllegalArgumentException();
    }

    @Override
    public void clear() {
      throw new IllegalArgumentException();
    }

    @Override
    void addAll(Set<? extends E> set, C ctx) {
      throw new IllegalArgumentException();
    }
  }
}
