package org.japy.infra.coll.ctx;

@SuppressWarnings("unused")
public class CtxAwareHashMap<K extends CtxAwareObject<C>, V extends CtxAwareObject<C>, C>
    extends HashedMap<K, V, C> {
  public CtxAwareHashMap() {}

  public CtxAwareHashMap(int initialCapacity) {
    super(initialCapacity);
  }

  public static <K extends CtxAwareObject<C>, V extends CtxAwareObject<C>, C>
      CtxAwareHashMap<K, V, C> withExpectedSize(int expectedSize) {
    return new CtxAwareHashMap<>(CtxAwareSet.capacity(expectedSize));
  }
}
