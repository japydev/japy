/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.japy.infra.coll.ctx;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import org.apache.commons.collections4.iterators.EmptyIterator;
import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * An abstract implementation of a hash-based map which provides numerous points for subclasses to
 * override.
 *
 * <p>This class implements all the features necessary for a subclass hash-based map. Key-value
 * entries are stored in instances of the <code>HashEntry</code> class, which can be overridden and
 * replaced. The iterators can similarly be replaced, without the need to replace the KeySet,
 * EntrySet and Values view classes.
 *
 * <p>Overridable methods are provided to change the default hashing behaviour, and to change how
 * entries are added to and removed from the map. Hopefully, all you need for unusual subclasses is
 * here.
 *
 * <p>NOTE: From Commons Collections 3.1 this class extends AbstractMap. This is to provide
 * backwards compatibility for ReferenceMap between v3.0 and v3.1. This extends clause will be
 * removed in v5.0.
 *
 * @param <E, C> the type of the keys in this map
 * @param <V, C> the type of the values in this map
 * @since 3.0
 */
@SuppressWarnings({
  "JavaDoc",
  "HtmlTagCanBeJavadocTag",
  "JavadocReference",
  "NotNullFieldNotInitialized",
  "LocalVariableHidesMemberVariable",
  "ConstantConditions",
  "InstanceVariableUsedBeforeInitialized",
  "unused",
  "DuplicatedCode",
  "CStyleArrayDeclaration",
  "RedundantTypeArguments",
  "unchecked",
  "PointlessBooleanExpression",
  "TryWithIdenticalCatches",
  "WhileLoopReplaceableByForEach",
  "ObjectEquality",
  "EqualsBetweenInconvertibleTypes",
  "rawtypes",
  "RedundantSuppression",
  "EmptyMethod"
})
class AbstractHashedSet<E extends CtxAwareObject<C>, C> implements CtxAwareSet<E, C> {

  protected static final String NO_NEXT_ENTRY = "No next() entry in the iteration";
  protected static final String NO_PREVIOUS_ENTRY = "No previous() entry in the iteration";
  protected static final String REMOVE_INVALID = "remove() can only be called once after next()";
  protected static final String GETKEY_INVALID =
      "getKey() can only be called after next() and before remove()";
  protected static final String GETVALUE_INVALID =
      "getValue() can only be called after next() and before remove()";
  protected static final String SETVALUE_INVALID =
      "setValue() can only be called after next() and before remove()";

  /** The default capacity to use */
  protected static final int DEFAULT_CAPACITY = 16;
  /** The default threshold to use */
  protected static final int DEFAULT_THRESHOLD = 12;
  /** The default load factor to use */
  protected static final float DEFAULT_LOAD_FACTOR = 0.75f;
  /** The maximum capacity allowed */
  protected static final int MAXIMUM_CAPACITY = 1 << 30;
  /** An object for masking null */
  protected static final Object NULL = new Object();

  /** Load factor, normally 0.75 */
  transient float loadFactor;
  /** The size of the map */
  transient int size;
  /** Map entries */
  transient HashEntry<E, C>[] data;
  /** Size at which to rehash */
  transient int threshold;
  /** Modification count for iterators */
  transient int modCount;

  /** Constructor only used in deserialization, do not use otherwise. */
  protected AbstractHashedSet() {
    super();
  }

  /**
   * Constructor which performs no validation on the passed in parameters.
   *
   * @param initialCapacity the initial capacity, must be a power of two
   * @param loadFactor the load factor, must be &gt; 0.0f and generally &lt; 1.0f
   * @param threshold the threshold, must be sensible
   */
  @SuppressWarnings("unchecked")
  protected AbstractHashedSet(
      final int initialCapacity, final float loadFactor, final int threshold) {
    super();
    this.loadFactor = loadFactor;
    this.data = new HashEntry[initialCapacity];
    this.threshold = threshold;
    init();
  }

  /**
   * Constructs a new, empty map with the specified initial capacity and default load factor.
   *
   * @param initialCapacity the initial capacity
   * @throws IllegalArgumentException if the initial capacity is negative
   */
  protected AbstractHashedSet(final int initialCapacity) {
    this(initialCapacity, DEFAULT_LOAD_FACTOR);
  }

  /**
   * Constructs a new, empty map with the specified initial capacity and load factor.
   *
   * @param initialCapacity the initial capacity
   * @param loadFactor the load factor
   * @throws IllegalArgumentException if the initial capacity is negative
   * @throws IllegalArgumentException if the load factor is less than or equal to zero
   */
  @SuppressWarnings("unchecked")
  protected AbstractHashedSet(int initialCapacity, final float loadFactor) {
    super();
    if (initialCapacity < 0) {
      throw new IllegalArgumentException("Initial capacity must be a non negative number");
    }
    if (loadFactor <= 0.0f || Float.isNaN(loadFactor)) {
      throw new IllegalArgumentException("Load factor must be greater than 0");
    }
    this.loadFactor = loadFactor;
    initialCapacity = calculateNewCapacity(initialCapacity);
    this.threshold = calculateThreshold(initialCapacity, loadFactor);
    this.data = new HashEntry[initialCapacity];
    init();
  }

  protected AbstractHashedSet(final Set<? extends E> set, C ctx) {
    this(Math.max(2 * set.size(), DEFAULT_CAPACITY), DEFAULT_LOAD_FACTOR);
    _addAll(set, ctx);
  }

  protected AbstractHashedSet(final CtxAwareSet<? extends E, ? extends C> set, C ctx) {
    this(Math.max(2 * set.size(), DEFAULT_CAPACITY), DEFAULT_LOAD_FACTOR);
    _addAll(set, ctx);
  }

  /** Initialise subclasses during construction, cloning or deserialization. */
  protected void init() {}

  @Override
  public boolean contains(E key, C ctx) {
    final int hashCode = hash(key, ctx);
    HashEntry<E, C> entry = data[hashIndex(hashCode, data.length)]; // no local for hash index
    while (entry != null) {
      if (entry.hashCode == hashCode && isEqualKey(key, entry.key, ctx)) {
        return true;
      }
      entry = entry.next;
    }
    return false;
  }

  /**
   * Gets the size of the map.
   *
   * @return the size
   */
  @Override
  public int size() {
    return size;
  }

  /**
   * Checks whether the map is currently empty.
   *
   * @return true if the map is currently size zero
   */
  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  @CanIgnoreReturnValue
  public boolean add(final E key, C ctx) {
    final int hashCode = hash(key, ctx);
    final int index = hashIndex(hashCode, data.length);
    HashEntry<E, C> entry = data[index];
    while (entry != null) {
      if (entry.hashCode == hashCode && isEqualKey(key, entry.key, ctx)) {
        return false;
      }
      entry = entry.next;
    }

    addMapping(index, hashCode, key);
    return true;
  }

  void addAll(final Set<? extends E> set, C ctx) {
    _addAll(set, ctx);
  }

  private void _addAll(int setSize, final Iterable<? extends E> set, C ctx) {
    if (setSize == 0) {
      return;
    }
    final int newSize = (int) ((size + setSize) / loadFactor + 1);
    ensureCapacity(calculateNewCapacity(newSize));
    for (E entry : set) {
      add(entry, ctx);
    }
  }

  private void _addAll(final Set<? extends E> set, C ctx) {
    _addAll(set.size(), set, ctx);
  }

  private void _addAll(final CtxAwareSet<? extends E, ? extends C> set, C ctx) {
    _addAll(set.size(), set, ctx);
  }

  @Override
  public boolean remove(E key, C ctx) {
    final int hashCode = hash(key, ctx);
    final int index = hashIndex(hashCode, data.length);
    HashEntry<E, C> entry = data[index];
    HashEntry<E, C> previous = null;
    while (entry != null) {
      if (entry.hashCode == hashCode && isEqualKey(key, entry.key, ctx)) {
        removeMapping(entry, index, previous);
        return true;
      }
      previous = entry;
      entry = entry.next;
    }
    return false;
  }

  /**
   * Clears the map, resetting the size to zero and nullifying references to avoid garbage
   * collection issues.
   */
  @Override
  public void clear() {
    modCount++;
    final HashEntry<E, C>[] data = this.data;
    for (int i = data.length - 1; i >= 0; i--) {
      data[i] = null;
    }
    size = 0;
  }

  // -----------------------------------------------------------------------
  /**
   * Gets the hash code for the key specified. This implementation uses the additional hashing
   * routine from JDK1.4. Subclasses can override this to return alternate hash codes.
   *
   * @param key the key to get a hash code for
   * @return the hash code
   */
  protected int hash(final E key, C ctx) {
    // same as JDK 1.4
    int h = key.hashCode(ctx);
    h += ~(h << 9);
    h ^= h >>> 14;
    h += h << 4;
    h ^= h >>> 10;
    return h;
  }

  /**
   * Compares two keys, in internal converted form, to see if they are equal. This implementation
   * uses the equals method and assumes neither key is null. Subclasses can override this to match
   * differently.
   *
   * @param key1 the first key to compare passed in from outside
   * @param key2 the second key extracted from the entry via <code>entry.key</code>
   * @return true if equal
   */
  protected boolean isEqualKey(final E key1, final E key2, C ctx) {
    return key1 == key2 || key1.isEqual(key2, ctx);
  }

  /**
   * Gets the index into the data storage for the hashCode specified. This implementation uses the
   * least significant bits of the hashCode. Subclasses can override this to return alternate
   * bucketing.
   *
   * @param hashCode the hash code to use
   * @param dataSize the size of the data to pick a bucket from
   * @return the bucket index
   */
  protected int hashIndex(final int hashCode, final int dataSize) {
    return hashCode & dataSize - 1;
  }

  // -----------------------------------------------------------------------
  protected @Nullable HashEntry<E, C> getEntry(E key, C ctx) {
    final int hashCode = hash(key, ctx);
    HashEntry<E, C> entry = data[hashIndex(hashCode, data.length)]; // no local for hash index
    while (entry != null) {
      if (entry.hashCode == hashCode && isEqualKey(key, entry.key, ctx)) {
        return entry;
      }
      entry = entry.next;
    }
    return null;
  }

  protected void reuseEntry(
      final HashEntry<E, C> entry, final int hashIndex, final int hashCode, final E key) {
    entry.next = data[hashIndex];
    entry.hashCode = hashCode;
    entry.key = key;
  }

  protected void addMapping(final int hashIndex, final int hashCode, final E key) {
    modCount++;
    final HashEntry<E, C> entry = createEntry(data[hashIndex], hashCode, key);
    addEntry(entry, hashIndex);
    size++;
    checkCapacity();
  }

  protected HashEntry<E, C> createEntry(
      final HashEntry<E, C> next, final int hashCode, final E key) {
    return new HashEntry<>(next, hashCode, key);
  }

  protected void addEntry(final HashEntry<E, C> entry, final int hashIndex) {
    data[hashIndex] = entry;
  }

  protected void removeMapping(
      final HashEntry<E, C> entry, final int hashIndex, final HashEntry<E, C> previous) {
    modCount++;
    removeEntry(entry, hashIndex, previous);
    size--;
    destroyEntry(entry);
  }

  protected void removeEntry(
      final HashEntry<E, C> entry, final int hashIndex, final HashEntry<E, C> previous) {
    if (previous == null) {
      data[hashIndex] = entry.next;
    } else {
      previous.next = entry.next;
    }
  }

  protected void destroyEntry(final HashEntry<E, C> entry) {
    entry.next = null;
    entry.key = null;
  }

  protected void checkCapacity() {
    if (size >= threshold) {
      final int newCapacity = data.length * 2;
      if (newCapacity <= MAXIMUM_CAPACITY) {
        ensureCapacity(newCapacity);
      }
    }
  }

  @SuppressWarnings("unchecked")
  protected void ensureCapacity(final int newCapacity) {
    final int oldCapacity = data.length;
    if (newCapacity <= oldCapacity) {
      return;
    }
    if (size == 0) {
      threshold = calculateThreshold(newCapacity, loadFactor);
      data = new HashEntry[newCapacity];
    } else {
      final HashEntry<E, C> oldEntries[] = data;
      final HashEntry<E, C> newEntries[] = new HashEntry[newCapacity];

      modCount++;
      for (int i = oldCapacity - 1; i >= 0; i--) {
        HashEntry<E, C> entry = oldEntries[i];
        if (entry != null) {
          oldEntries[i] = null; // gc
          do {
            final HashEntry<E, C> next = entry.next;
            final int index = hashIndex(entry.hashCode, newCapacity);
            entry.next = newEntries[index];
            newEntries[index] = entry;
            entry = next;
          } while (entry != null);
        }
      }
      threshold = calculateThreshold(newCapacity, loadFactor);
      data = newEntries;
    }
  }

  protected int calculateNewCapacity(final int proposedCapacity) {
    int newCapacity = 1;
    if (proposedCapacity > MAXIMUM_CAPACITY) {
      newCapacity = MAXIMUM_CAPACITY;
    } else {
      while (newCapacity < proposedCapacity) {
        newCapacity <<= 1; // multiply by two
      }
      if (newCapacity > MAXIMUM_CAPACITY) {
        newCapacity = MAXIMUM_CAPACITY;
      }
    }
    return newCapacity;
  }

  protected int calculateThreshold(final int newCapacity, final float factor) {
    return (int) (newCapacity * factor);
  }

  @Override
  public Iterator<E> iterator() {
    if (size == 0) {
      return EmptyIterator.<E>emptyIterator();
    }
    return new HashSetIterator<>(this);
  }

  /** MapIterator implementation. */
  protected static class HashSetIterator<E extends CtxAwareObject<C>, C> extends HashIterator<E, C>
      implements Iterator<E> {

    protected HashSetIterator(final AbstractHashedSet<E, C> parent) {
      super(parent);
    }

    @Override
    public E next() {
      return super.nextEntry().key;
    }
  }

  /**
   * HashEntry used to store the data.
   *
   * <p>If you subclass <code>AbstractHashedMap</code> but not <code>HashEntry</code> then you will
   * not be able to access the protected fields. The <code>entryXxx()</code> methods on <code>
   * AbstractHashedMap</code> exist to provide the necessary access.
   */
  @SuppressWarnings("PointlessBooleanExpression")
  protected static class HashEntry<E extends CtxAwareObject<C>, C> {
    /** The next entry in the hash chain */
    protected HashEntry<E, C> next;
    /** The hash code of the key */
    protected int hashCode;
    /** The key */
    protected E key;

    protected HashEntry(final HashEntry<E, C> next, final int hashCode, final E key) {
      super();
      this.next = next;
      this.hashCode = hashCode;
      this.key = key;
    }

    public boolean equals(final Object obj) {
      throw new IllegalArgumentException();
    }

    public boolean equals(final Object obj, C ctx) {
      if (obj == this) {
        return true;
      }
      if (obj instanceof Map.Entry == false) {
        return false;
      }
      final HashEntry<?, ?> other = (HashEntry<?, ?>) obj;
      return key.isEqual(other.key, ctx);
    }

    public int hashCode() {
      throw new IllegalArgumentException();
    }

    public int hashCode(C ctx) {
      return key.hashCode(ctx);
    }

    @Override
    public String toString() {
      return key.toString();
    }
  }

  /** Base Iterator */
  protected abstract static class HashIterator<E extends CtxAwareObject<C>, C> {

    /** The parent map */
    private final AbstractHashedSet<E, C> parent;
    /** The current index into the array of buckets */
    private int hashIndex;
    /** The last returned entry */
    private HashEntry<E, C> last;
    /** The next entry */
    private HashEntry<E, C> next;
    /** The modification count expected */
    private int expectedModCount;

    protected HashIterator(final AbstractHashedSet<E, C> parent) {
      super();
      this.parent = parent;
      final HashEntry<E, C>[] data = parent.data;
      int i = data.length;
      HashEntry<E, C> next = null;
      while (i > 0 && next == null) {
        next = data[--i];
      }
      this.next = next;
      this.hashIndex = i;
      this.expectedModCount = parent.modCount;
    }

    public boolean hasNext() {
      return next != null;
    }

    protected HashEntry<E, C> nextEntry() {
      if (parent.modCount != expectedModCount) {
        throw new ConcurrentModificationException();
      }
      final HashEntry<E, C> newCurrent = next;
      if (newCurrent == null) {
        throw new NoSuchElementException(AbstractHashedSet.NO_NEXT_ENTRY);
      }
      final HashEntry<E, C>[] data = parent.data;
      int i = hashIndex;
      HashEntry<E, C> n = newCurrent.next;
      while (n == null && i > 0) {
        n = data[--i];
      }
      next = n;
      hashIndex = i;
      last = newCurrent;
      return newCurrent;
    }

    protected HashEntry<E, C> currentEntry() {
      return last;
    }

    public void remove() {
      throw new IllegalArgumentException();
    }

    public void remove(C ctx) {
      if (last == null) {
        throw new IllegalStateException(AbstractHashedSet.REMOVE_INVALID);
      }
      if (parent.modCount != expectedModCount) {
        throw new ConcurrentModificationException();
      }
      parent.remove(last.key, ctx);
      last = null;
      expectedModCount = parent.modCount;
    }

    @Override
    public String toString() {
      if (last != null) {
        return "Iterator[" + last.key + "]";
      }
      return "Iterator[]";
    }
  }

  public boolean equals(final Object obj) {
    throw new IllegalArgumentException();
  }

  @Override
  public boolean equals(final Object obj, C ctx) {
    if (obj == this) {
      return true;
    }
    if (obj instanceof CtxAwareSet == false) {
      return false;
    }
    final CtxAwareSet<E, C> set = (CtxAwareSet<E, C>) obj;
    if (set.size() != size()) {
      return false;
    }
    final Iterator<E> it = iterator();
    try {
      while (it.hasNext()) {
        final E key = it.next();
        if (!set.contains(key, ctx)) {
          return false;
        }
      }
    } catch (final ClassCastException ignored) {
      return false;
    } catch (final NullPointerException ignored) {
      return false;
    }
    return true;
  }

  public int hashCode() {
    throw new IllegalArgumentException();
  }

  /**
   * Gets the standard Map hashCode.
   *
   * @return the hash code defined in the Map interface
   */
  @Override
  public int hashCode(C ctx) {
    int total = 0;
    final Iterator<E> it = iterator();
    while (it.hasNext()) {
      total += it.next().hashCode(ctx);
    }
    return total;
  }

  /**
   * Gets the map as a String.
   *
   * @return a string version of the map
   */
  @Override
  public String toString() {
    if (size() == 0) {
      return "{}";
    }
    final StringBuilder buf = new StringBuilder(32 * size());
    buf.append('{');

    final Iterator<E> it = iterator();
    boolean hasNext = it.hasNext();
    while (hasNext) {
      final E key = it.next();
      buf.append(key == this ? "(this Set)" : key);

      hasNext = it.hasNext();
      if (hasNext) {
        buf.append(',').append(' ');
      }
    }

    buf.append('}');
    return buf.toString();
  }
}
