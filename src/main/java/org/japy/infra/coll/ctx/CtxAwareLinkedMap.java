package org.japy.infra.coll.ctx;

@SuppressWarnings("unused")
public class CtxAwareLinkedMap<K extends CtxAwareObject<C>, V extends CtxAwareObject<C>, C>
    extends LinkedMap<K, V, C> {
  public CtxAwareLinkedMap() {}

  public CtxAwareLinkedMap(int initialCapacity) {
    super(initialCapacity);
  }

  public static <K extends CtxAwareObject<C>, V extends CtxAwareObject<C>, C>
      CtxAwareLinkedMap<K, V, C> withExpectedSize(int expectedSize) {
    return new CtxAwareLinkedMap<>(CtxAwareSet.capacity(expectedSize));
  }
}
