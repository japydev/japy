/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.japy.infra.coll.ctx;

import java.util.Map;
import java.util.Set;

/**
 * A <code>Map</code> implementation that is a general purpose alternative to <code>HashMap</code>.
 *
 * <p>This implementation improves on the JDK1.4 HashMap by adding the {@link
 * org.apache.commons.collections4.MapIterator MapIterator} functionality and many methods for
 * subclassing.
 *
 * <p><strong>Note that HashedMap is not synchronized and is not thread-safe.</strong> If you wish
 * to use this map from multiple threads concurrently, you must use appropriate synchronization. The
 * simplest approach is to wrap this map using {@link java.util.Collections#synchronizedMap(Map)}.
 * This class may throw exceptions when accessed by concurrent threads without synchronization.
 *
 * @param <E> the type of the keys in this map
 * @param <V> the type of the values in this map
 * @since 3.0
 */
@SuppressWarnings({"JavadocReference", "HtmlTagCanBeJavadocTag", "unused"})
class HashedSet<E extends CtxAwareObject<C>, C> extends AbstractHashedSet<E, C> {

  /** Constructs a new empty map with default size and load factor. */
  public HashedSet() {
    super(DEFAULT_CAPACITY, DEFAULT_LOAD_FACTOR, DEFAULT_THRESHOLD);
  }

  /**
   * Constructs a new, empty map with the specified initial capacity.
   *
   * @param initialCapacity the initial capacity
   * @throws IllegalArgumentException if the initial capacity is negative
   */
  public HashedSet(final int initialCapacity) {
    super(initialCapacity);
  }

  /**
   * Constructs a new, empty map with the specified initial capacity and load factor.
   *
   * @param initialCapacity the initial capacity
   * @param loadFactor the load factor
   * @throws IllegalArgumentException if the initial capacity is negative
   * @throws IllegalArgumentException if the load factor is less than zero
   */
  public HashedSet(final int initialCapacity, final float loadFactor) {
    super(initialCapacity, loadFactor);
  }

  /**
   * Constructor copying elements from another map.
   *
   * @param map the map to copy
   * @throws NullPointerException if the map is null
   */
  public HashedSet(final Set<? extends E> set, C ctx) {
    super(set, ctx);
  }

  /**
   * Constructor copying elements from another map.
   *
   * @param map the map to copy
   * @throws NullPointerException if the map is null
   */
  public HashedSet(final CtxAwareSet<? extends E, ? extends C> set, C ctx) {
    super(set, ctx);
  }
}
