/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.japy.infra.coll.ctx;

import java.util.Map;

/**
 * A <code>Map</code> implementation that maintains the order of the entries. In this implementation
 * order is maintained by original insertion.
 *
 * <p>This implementation improves on the JDK1.4 LinkedHashMap by adding the {@link
 * org.apache.commons.collections4.MapIterator MapIterator} functionality, additional convenience
 * methods and allowing bidirectional iteration. It also implements <code>OrderedMap</code>. In
 * addition, non-interface methods are provided to access the map by index.
 *
 * <p>The <code>orderedMapIterator()</code> method provides direct access to a bidirectional
 * iterator. The iterators from the other views can also be cast to <code>OrderedIterator</code> if
 * required.
 *
 * <p>All the available iterators can be reset back to the start by casting to <code>
 * ResettableIterator</code> and calling <code>reset()</code>.
 *
 * <p>The implementation is also designed to be subclassed, with lots of useful methods exposed.
 *
 * <p><strong>Note that LinkedMap is not synchronized and is not thread-safe.</strong> If you wish
 * to use this map from multiple threads concurrently, you must use appropriate synchronization. The
 * simplest approach is to wrap this map using {@link java.util.Collections#synchronizedMap(Map)}.
 * This class may throw exceptions when accessed by concurrent threads without synchronization.
 *
 * @param <K> the type of the keys in this map
 * @param <V> the type of the values in this map
 * @since 3.0
 */
@SuppressWarnings({"HtmlTagCanBeJavadocTag", "unused"})
class LinkedMap<K extends CtxAwareObject<C>, V extends CtxAwareObject<C>, C>
    extends AbstractLinkedMap<K, V, C> {

  /** Constructs a new empty map with default size and load factor. */
  public LinkedMap() {
    super(DEFAULT_CAPACITY, DEFAULT_LOAD_FACTOR, DEFAULT_THRESHOLD);
  }

  /**
   * Constructs a new, empty map with the specified initial capacity.
   *
   * @param initialCapacity the initial capacity
   * @throws IllegalArgumentException if the initial capacity is negative
   */
  public LinkedMap(final int initialCapacity) {
    super(initialCapacity);
  }

  /**
   * Constructs a new, empty map with the specified initial capacity and load factor.
   *
   * @param initialCapacity the initial capacity
   * @param loadFactor the load factor
   * @throws IllegalArgumentException if the initial capacity is negative
   * @throws IllegalArgumentException if the load factor is less than zero
   */
  public LinkedMap(final int initialCapacity, final float loadFactor) {
    super(initialCapacity, loadFactor);
  }

  /**
   * Constructor copying elements from another map.
   *
   * @param map the map to copy
   * @throws NullPointerException if the map is null
   */
  public LinkedMap(final Map<? extends K, ? extends V> map, C ctx) {
    super(map, ctx);
  }
}
