package org.japy.infra.coll.ctx;

public interface CtxAwareObject<C> {
  int hashCode(C ctx);

  boolean isEqual(Object other, C ctx);
}
