package org.japy.infra.coll.ctx;

import java.util.Iterator;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import org.apache.commons.collections4.MapIterator;
import org.checkerframework.checker.nullness.qual.Nullable;

@SuppressWarnings("unused")
public interface CtxAwareMap<K extends CtxAwareObject<C>, V extends CtxAwareObject<C>, C> {
  interface Entry<K extends CtxAwareObject<C>, V extends CtxAwareObject<C>, C>
      extends Map.Entry<K, V> {
    int hashCode(C ctx);

    boolean equals(Object o, C ctx);
  }

  int size();

  boolean isEmpty();

  boolean equals(Object other, C ctx);

  int hashCode(C ctx);

  @Nullable
  V get(K key, C ctx);

  boolean containsKey(K key, C ctx);

  boolean containsValue(V value, C ctx);

  @CanIgnoreReturnValue
  @Nullable
  V put(K key, V value, C ctx);

  void putAll(Map<? extends K, ? extends V> map, C ctx);

  @CanIgnoreReturnValue
  @Nullable
  V remove(K key, C ctx);

  void clear();

  MapIterator<K, V> mapIterator();

  Iterator<Map.Entry<K, V>> entryIterator();

  default void forEach(BiConsumer<? super K, ? super V> func) {
    for (MapIterator<K, V> iter = mapIterator(); iter.hasNext(); ) {
      iter.next();
      func.accept(iter.getKey(), iter.getValue());
    }
  }

  default void forEachKey(Consumer<? super K> func) {
    for (MapIterator<K, V> iter = mapIterator(); iter.hasNext(); ) {
      iter.next();
      func.accept(iter.getKey());
    }
  }
}
