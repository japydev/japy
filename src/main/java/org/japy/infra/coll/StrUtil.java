package org.japy.infra.coll;

import static org.japy.infra.validation.Debug.dcheck;

import org.japy.infra.validation.Debug;

public class StrUtil {
  public static final String[] EMPTY_STRING_ARRAY = new String[0];

  public static String join(String[] array, String sep, int from, int to) {
    if (Debug.ENABLED) {
      dcheck(from < to && from >= 0 && to <= array.length);
    }

    if (to - from == 1) {
      return array[from];
    }

    StringBuilder sb = new StringBuilder(array[from]);
    for (int i = from + 1; i != to; ++i) {
      sb.append(sep);
      sb.append(array[i]);
    }
    return sb.toString();
  }

  public static String join(String[] array, String sep, int howmany) {
    return join(array, sep, 0, howmany);
  }

  public static String join(String[] array, String sep) {
    return join(array, sep, 0, array.length);
  }
}
