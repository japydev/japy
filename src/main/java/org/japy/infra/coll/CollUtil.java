package org.japy.infra.coll;

import static org.japy.infra.validation.Debug.dcheck;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class CollUtil {
  public static final int[] EMPTY_INT_ARRAY = new int[0];
  public static final String[] EMPTY_STRING_ARRAY = new String[0];

  public static <T> T[] prepend(T element, T[] array) {
    @SuppressWarnings("unchecked")
    T[] newArray = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length + 1);
    newArray[0] = element;
    System.arraycopy(array, 0, newArray, 1, array.length);
    return newArray;
  }

  public static <T> T[] append(T[] array, T obj) {
    @SuppressWarnings("unchecked")
    T[] newArray = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length + 1);
    System.arraycopy(array, 0, newArray, 0, array.length);
    newArray[array.length] = obj;
    return newArray;
  }

  public static <T> T[] tail(T[] array) {
    dcheck(array.length != 0);
    return Arrays.copyOfRange(array, 1, array.length);
  }

  public static <T1, T2> T2[] map(T1[] array, Class<T2> to, Function<T1, T2> func) {
    int len = array.length;
    @SuppressWarnings("unchecked")
    T2[] result = (T2[]) Array.newInstance(to, array.length);
    for (int i = 0; i < len; ++i) {
      result[i] = func.apply(array[i]);
    }
    return result;
  }

  public static <T> void fill(T[] array, Supplier<T> sup) {
    for (int i = 0; i < array.length; ++i) {
      array[i] = sup.get();
    }
  }

  public static <T> List<T> toList(T[] array) {
    List<T> list = new ArrayList<>(array.length);
    Collections.addAll(list, array);
    return list;
  }

  public static <T> T first(List<T> list) {
    return list.get(0);
  }

  public static <T> T first(T[] array) {
    return array[0];
  }

  public static <T> T last(List<T> list) {
    return list.get(list.size() - 1);
  }

  public static <T> T last(T[] array) {
    return array[array.length - 1];
  }

  public static <T> void push(List<T> list, T obj) {
    list.add(obj);
  }

  public static <T> T pop(List<T> list) {
    return list.remove(list.size() - 1);
  }

  public static <T> void forEachBackwards(List<T> list, Consumer<T> func) {
    for (int i = list.size() - 1; i >= 0; --i) {
      func.accept(list.get(i));
    }
  }

  public static <T> void forEach(T[] array, Consumer<? super T> func) {
    for (T elm : array) {
      func.accept(elm);
    }
  }

  public static class StopIteration extends RuntimeException {
    public StopIteration() {}

    @Override
    public synchronized Throwable fillInStackTrace() {
      return this;
    }
  }

  public static void doWithStop(Runnable func) {
    try {
      func.run();
    } catch (StopIteration ignored) {
      //
    }
  }
}
