package org.japy.infra.loc;

import com.google.errorprone.annotations.Immutable;

@Immutable
public class Location implements IHasLocation, Comparable<Location> {
  public final int line;
  public final int col;

  public static final int UNKNOWN_LINE = 0;
  public static final int UNKNOWN_COL = 0;

  public static final Location UNKNOWN = new Location(UNKNOWN_LINE, UNKNOWN_COL);

  public Location(int line, int col) {
    this.line = line;
    this.col = col;
  }

  @Override
  public Location location() {
    return this;
  }

  public boolean isUnknown() {
    return line == UNKNOWN_LINE;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Location)) {
      return false;
    }
    Location location = (Location) o;
    return line == location.line && col == location.col;
  }

  @Override
  public int hashCode() {
    return 31 * (31 + line) + col;
  }

  @Override
  public int compareTo(Location o) {
    int c = Integer.compare(line, o.line);
    if (c != 0) {
      return c;
    }
    return Integer.compare(col, o.col);
  }

  @Override
  public String toString() {
    return "{" + "line=" + line + ", col=" + col + '}';
  }
}
