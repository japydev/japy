package org.japy.infra.loc;

public interface IHasSourceFile {
  String UNKNOWN_FILE = "";

  String sourceFile();

  default boolean isUnknownSourceFile() {
    return sourceFile().equals(UNKNOWN_FILE);
  }
}
