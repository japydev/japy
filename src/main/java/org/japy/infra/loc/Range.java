package org.japy.infra.loc;

import static org.japy.infra.coll.CollUtil.last;
import static org.japy.infra.validation.Debug.dcheck;

import java.util.List;

import com.google.errorprone.annotations.Immutable;

import org.japy.infra.validation.Debug;

@Immutable
public class Range implements IHasRange {
  public static final Range UNKNOWN = new Range(Location.UNKNOWN, Location.UNKNOWN);

  public final Location start;
  public final Location end;

  public Range(Location start, Location end) {
    if (start.isUnknown() || end.isUnknown()) {
      this.start = Location.UNKNOWN;
      this.end = Location.UNKNOWN;
    } else {
      this.start = start;
      this.end = end;
    }
    if (Debug.ENABLED) {
      dcheck(start.compareTo(end) <= 0);
    }
  }

  public Range(IHasRange start, IHasRange end) {
    this(start.start(), end.end());
  }

  public boolean isUnknown() {
    return start.isUnknown();
  }

  @Override
  public Range range() {
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Range)) {
      return false;
    }
    Range range = (Range) o;
    return start.equals(range.start) && end.equals(range.end);
  }

  @Override
  public int hashCode() {
    return 31 * (31 + start.hashCode()) + end.hashCode();
  }

  public static Range span(IHasRange[] objs) {
    dcheck(objs.length != 0);
    return new Range(objs[0].start(), last(objs).end());
  }

  public static Range span(List<? extends IHasRange> objs) {
    dcheck(!objs.isEmpty());
    return new Range(objs.get(0).start(), last(objs).end());
  }
}
