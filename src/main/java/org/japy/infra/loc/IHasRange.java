package org.japy.infra.loc;

public interface IHasRange extends IHasLocation {
  @Override
  default Location location() {
    return start();
  }

  default Location start() {
    return range().start;
  }

  default Location end() {
    return range().end;
  }

  Range range();
}
