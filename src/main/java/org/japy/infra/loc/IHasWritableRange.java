package org.japy.infra.loc;

public interface IHasWritableRange extends IHasRange, IHasWritableLocation {
  void setRange(Range location);

  @Override
  default void setLocation(Location location) {
    throw new IllegalArgumentException();
  }
}
