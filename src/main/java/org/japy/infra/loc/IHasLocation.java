package org.japy.infra.loc;

public interface IHasLocation {
  Location location();
}
