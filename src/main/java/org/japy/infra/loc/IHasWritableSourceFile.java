package org.japy.infra.loc;

public interface IHasWritableSourceFile extends IHasSourceFile {
  void setSourceFile(String sourceFile);

  default void setSourceFileIfNotSet(String file) {
    if (isUnknownSourceFile()) {
      setSourceFile(file);
    }
  }
}
