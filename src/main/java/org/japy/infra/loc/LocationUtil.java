package org.japy.infra.loc;

import com.google.errorprone.annotations.Var;

public class LocationUtil {
  public static void setFileIfNotSet(@Var Throwable ex, String file) {
    while (ex != null) {
      if (ex instanceof IHasWritableSourceFile) {
        ((IHasWritableSourceFile) ex).setSourceFileIfNotSet(file);
      }
      ex = ex.getCause();
    }
  }

  public static void setLocationIfNotSet(@Var Throwable ex, IHasLocation location) {
    while (ex != null) {
      if (ex instanceof IHasWritableLocation) {
        ((IHasWritableLocation) ex).setLocationIfNotSet(location.location());
      }
      ex = ex.getCause();
    }
  }
}
