package org.japy.infra.loc;

public interface IHasWritableLocation extends IHasLocation {
  void setLocation(Location location);

  default void setLocationIfNotSet(Location location) {
    if (location().isUnknown()) {
      setLocation(location);
    }
  }
}
