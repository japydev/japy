package org.japy.infra.validation;

import java.util.IdentityHashMap;
import java.util.function.Supplier;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.DoNotCall;
import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.dataflow.qual.Pure;
import org.checkerframework.dataflow.qual.TerminatesExecution;
import org.jetbrains.annotations.Contract;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.LocationUtil;

public class Validator {
  private final boolean checkRecursion;
  private final IdentityHashMap<ISupportsValidation, ISupportsValidation> checked =
      new IdentityHashMap<>();

  public Validator() {
    this(true);
  }

  public Validator(boolean checkRecursion) {
    this.checkRecursion = checkRecursion;
  }

  public void validate(ISupportsValidation obj) {
    checkNotNull(obj, "null object");

    if (checkRecursion) {
      if (checked.containsKey(obj)) {
        return;
      }
      checked.put(obj, obj);
    }

    doValidate(obj);
  }

  protected void doValidate(ISupportsValidation obj) {
    try {
      obj.validate(this);
    } catch (Throwable ex) {
      if (!(obj instanceof IHasLocation) || ((IHasLocation) obj).location().isUnknown()) {
        throw ex;
      }

      LocationUtil.setLocationIfNotSet(ex, (IHasLocation) obj);

      if (ex instanceof IHasLocation) {
        throw ex;
      }

      throw new InternalErrorException((IHasLocation) obj, ex);
    }
  }

  @Contract("false, _ -> fail")
  @Pure
  public static void check(boolean condition, String message) {
    if (!condition) {
      fail(message);
    }
  }

  @Contract("null, _ -> fail")
  @EnsuresNonNull("#1")
  @Pure
  @CanIgnoreReturnValue
  public static <T> T checkNotNull(@Nullable T obj, String message) {
    if (obj == null) {
      fail(message);
    }
    return obj;
  }

  @Contract("null, _ -> fail")
  @EnsuresNonNull("#1")
  @Pure
  public static void checkNotNull(@Nullable Object obj, Supplier<RuntimeException> ex) {
    if (obj == null) {
      throw ex.get();
    }
  }

  @DoNotCall
  @SuppressWarnings("unused")
  public static void checkNotNull(boolean value, String message) {
    throw new InternalErrorException("should not be called");
  }

  @Contract("false, _, _ -> fail")
  @Pure
  public static void check(boolean condition, String message, IHasLocation location) {
    if (!condition) {
      fail(message, location);
    }
  }

  @Contract("false, _ -> fail")
  @Pure
  public static void check(boolean condition, Supplier<? extends RuntimeException> ex) {
    if (!condition) {
      throw ex.get();
    }
  }

  @Contract("_ -> fail")
  @TerminatesExecution
  @CanIgnoreReturnValue
  public static ValidationError fail(String message) {
    throw new ValidationError(message);
  }

  @Contract("_, _ -> fail")
  @TerminatesExecution
  @CanIgnoreReturnValue
  public static ValidationError fail(String message, IHasLocation location) {
    throw new ValidationError(message, location);
  }

  public void validateIfNotNull(@Nullable ISupportsValidation o) {
    if (o != null) {
      validate(o);
    }
  }

  public void validateIfNotNull(@Nullable Iterable<? extends @Nullable ISupportsValidation> list) {
    if (list != null) {
      list.forEach(this::validateIfNotNull);
    }
  }

  public void validateIfNotNull(@Nullable ISupportsValidation @Nullable ... objects) {
    if (objects != null) {
      for (@Nullable ISupportsValidation obj : objects) {
        validateIfNotNull(obj);
      }
    }
  }

  public void validate(Iterable<? extends ISupportsValidation> list) {
    list.forEach(this::validate);
  }

  public void validate(ISupportsValidation... objects) {
    for (ISupportsValidation obj : objects) {
      validate(obj);
    }
  }
}
