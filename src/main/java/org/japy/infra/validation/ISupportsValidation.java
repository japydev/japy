package org.japy.infra.validation;

public interface ISupportsValidation {
  void validate(Validator v);
}
