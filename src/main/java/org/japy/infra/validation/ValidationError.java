package org.japy.infra.validation;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.IHasLocation;

public class ValidationError extends InternalErrorException {
  public ValidationError(String message) {
    super(message);
  }

  public ValidationError(String message, Throwable ex) {
    super(message, ex);
  }

  public ValidationError(String message, IHasLocation location) {
    super(message, location);
  }
}
