package org.japy.infra.validation;

import java.util.function.Supplier;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.DoNotCall;
import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.checkerframework.dataflow.qual.Pure;
import org.jetbrains.annotations.Contract;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.IHasLocation;

@SuppressWarnings({"PointlessBooleanExpression", "Contract", "RedundantSuppression"})
public class Debug {
  public static final boolean PROFILING_ENABLED = false;
  public static final boolean ENABLED = !PROFILING_ENABLED && true;

  @Contract("false, _ -> fail")
  @Pure
  public static void dcheck(boolean condition, String message) {
    if (ENABLED && !condition) {
      throw new InternalErrorException(message);
    }
  }

  @Contract("false, _, _ -> fail")
  @Pure
  public static void dcheck(boolean condition, String message, IHasLocation location) {
    if (ENABLED && !condition) {
      throw new InternalErrorException(message, location);
    }
  }

  @Contract("false, _ -> fail")
  @Pure
  public static void dcheck(boolean condition, IHasLocation location) {
    if (ENABLED && !condition) {
      throw new InternalErrorException("condition failed", location);
    }
  }

  @Contract("false -> fail")
  @Pure
  public static void dcheck(boolean condition) {
    if (ENABLED && !condition) {
      throw new InternalErrorException("condition failed");
    }
  }

  @Contract("false -> fail")
  @Pure
  public static void dcheck(boolean condition, Supplier<InternalErrorException> makeExc) {
    if (ENABLED && !condition) {
      throw makeExc.get();
    }
  }

  @Contract("null, _ -> fail")
  @EnsuresNonNull("#1")
  @SuppressWarnings("contracts.postcondition")
  @Pure
  public static void dcheckNotNull(@Nullable Object obj, String message) {
    if (ENABLED && obj == null) {
      throw new InternalErrorException(message);
    }
  }

  @Contract("null -> fail")
  @EnsuresNonNull("#1")
  @SuppressWarnings({"contracts.postcondition", "return"})
  @CanIgnoreReturnValue
  @Pure
  public static <T> T dcheckNotNull(@Nullable T obj) {
    if (ENABLED && obj == null) {
      throw new InternalErrorException("null object");
    }
    //noinspection ConstantConditions,Contract
    return obj;
  }

  @DoNotCall
  @SuppressWarnings("unused")
  @Deprecated
  public static void dcheckNotNull(boolean unused) {
    throw new InternalErrorException("should not be called");
  }

  @DoNotCall
  @SuppressWarnings("unused")
  @Deprecated
  public static void dcheckNotNull(boolean unused, String message) {
    throw new InternalErrorException("should not be called");
  }
}
