package org.japy.infra.exc;

public class UserError extends RuntimeException {
  public UserError(String message) {
    super(message);
  }

  public UserError(String message, Throwable cause) {
    super(message, cause);
  }
}
