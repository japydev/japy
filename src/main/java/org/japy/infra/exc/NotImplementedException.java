package org.japy.infra.exc;

import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.IHasSourceFile;
import org.japy.infra.loc.IHasWritableLocation;
import org.japy.infra.loc.IHasWritableSourceFile;
import org.japy.infra.loc.Location;

public class NotImplementedException extends RuntimeException
    implements IHasWritableLocation, IHasWritableSourceFile {
  @SuppressWarnings("NonFinalFieldOfException")
  private Location location;

  @SuppressWarnings("NonFinalFieldOfException")
  private String sourceFile = IHasSourceFile.UNKNOWN_FILE;

  public NotImplementedException(String what) {
    this(what, Location.UNKNOWN);
  }

  public NotImplementedException(String what, IHasLocation location) {
    super("not implemented: " + what);
    this.location = location.location();
  }

  @Override
  public Location location() {
    return location;
  }

  @Override
  public void setLocation(Location location) {
    this.location = location;
  }

  @Override
  public String sourceFile() {
    return sourceFile;
  }

  @Override
  public void setSourceFile(String sourceFile) {
    this.sourceFile = sourceFile;
  }
}
