package org.japy.infra.exc;

import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.IHasSourceFile;
import org.japy.infra.loc.IHasWritableLocation;
import org.japy.infra.loc.IHasWritableSourceFile;
import org.japy.infra.loc.Location;

public class InternalErrorException extends RuntimeException
    implements IHasWritableLocation, IHasWritableSourceFile {
  @SuppressWarnings("NonFinalFieldOfException")
  private Location location = Location.UNKNOWN;

  @SuppressWarnings("NonFinalFieldOfException")
  private String sourceFile = IHasSourceFile.UNKNOWN_FILE;

  public InternalErrorException(String message) {
    super(message);
  }

  public InternalErrorException(String message, Throwable cause) {
    super(message, cause);
  }

  public InternalErrorException(Throwable cause) {
    super(cause);
  }

  public InternalErrorException(String message, IHasLocation location) {
    super(message);
    this.location = location.location();
  }

  public InternalErrorException(IHasLocation location, Throwable cause) {
    super(cause);
    this.location = location.location();
  }

  public InternalErrorException(String message, IHasLocation location, Throwable cause) {
    super(message, cause);
    this.location = location.location();
  }

  public static InternalErrorException notReached() {
    return new InternalErrorException("should not be reached");
  }

  public static InternalErrorException notReached(IHasLocation location) {
    return new InternalErrorException("should not be reached", location);
  }

  public static InternalErrorException notReached(Throwable cause) {
    return new InternalErrorException("should not be reached", cause);
  }

  public static InternalErrorException notReached(String what) {
    return new InternalErrorException("should not be reached: " + what);
  }

  public static InternalErrorException notReached(String what, Throwable cause) {
    return new InternalErrorException("should not be reached: " + what, cause);
  }

  public static InternalErrorException notReached(String what, IHasLocation location) {
    return new InternalErrorException("should not be reached: " + what, location);
  }

  @Override
  public Location location() {
    return location;
  }

  @Override
  public void setLocation(Location location) {
    this.location = location;
  }

  @Override
  public String sourceFile() {
    return sourceFile;
  }

  @Override
  public void setSourceFile(String sourceFile) {
    this.sourceFile = sourceFile;
  }
}
