@DefaultQualifier(
    value = NonNull.class,
    locations = {
      TypeUseLocation.FIELD,
      TypeUseLocation.PARAMETER,
      TypeUseLocation.RETURN,
      TypeUseLocation.LOCAL_VARIABLE
    })
@CheckReturnValue
package org.japy;

import com.google.errorprone.annotations.CheckReturnValue;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.framework.qual.DefaultQualifier;
import org.checkerframework.framework.qual.TypeUseLocation;
