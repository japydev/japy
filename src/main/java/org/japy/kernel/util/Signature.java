package org.japy.kernel.util;

import static org.japy.infra.coll.CollUtil.EMPTY_INT_ARRAY;
import static org.japy.infra.coll.StrUtil.EMPTY_STRING_ARRAY;
import static org.japy.infra.validation.Debug.dcheck;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;
import com.google.errorprone.annotations.Var;

import org.japy.base.ParamKind;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.ISupportsValidation;
import org.japy.infra.validation.Validator;

public class Signature implements ISupportsValidation {
  public static final Signature NO_PARAMS = new Signature();
  public static final Signature VARARGS;

  public static final int INVALID_INDEX = -1;

  private static final int FLAG_HAS_VAR_POS = 1;
  private static final int FLAG_HAS_VAR_KW = 2;
  private static final int FLAG_ALL_POS = 4;

  public final String[] paramNames;
  public final int[] paramKinds;
  public final int posOnly;
  public final int posOrKw;
  public final int flags;

  public boolean allPos() {
    return (flags & FLAG_ALL_POS) != 0;
  }

  public boolean hasKw() {
    return posOnly + ((flags & FLAG_HAS_VAR_POS) != 0 ? 1 : 0) < paramNames.length;
  }

  public int idxVarPos() {
    return (flags & FLAG_HAS_VAR_POS) != 0 ? posOnly + posOrKw : INVALID_INDEX;
  }

  public int idxVarKw() {
    return (flags & FLAG_HAS_VAR_KW) != 0 ? paramNames.length - 1 : INVALID_INDEX;
  }

  public int paramCount() {
    return paramNames.length;
  }

  public int paramIndex(String name) {
    for (int i = 0; i < paramNames.length; ++i) {
      if (name.equals(paramNames[i])) {
        return i;
      }
    }
    return INVALID_INDEX;
  }

  protected Signature() {
    this(EMPTY_STRING_ARRAY, EMPTY_INT_ARRAY, 0, 0, 0);
  }

  private Signature(String[] paramNames, int[] paramKinds, int posOnly, int posOrKw, int flags) {
    this.paramNames = paramNames;
    this.paramKinds = paramKinds;
    this.posOnly = posOnly;
    this.posOrKw = posOrKw;
    this.flags = flags;

    if (Debug.ENABLED) {
      validate();
    }
  }

  public static Builder builder(int paramCount) {
    return new Builder(paramCount);
  }

  public static Signature make(String[] paramNames, int[] paramKinds) {
    @Var int posOnly = 0;
    @Var int posOrKw = 0;
    @Var boolean seenVarPos = false;
    @Var int kwOnly = 0;
    @Var boolean seenVarKw = false;
    @Var boolean allPos = true;
    dcheck(paramNames.length == paramKinds.length);
    for (int i = 0, count = paramNames.length; i < count; ++i) {
      switch (paramKinds[i]) {
        case ParamKind.IPOS_ONLY:
          dcheck(posOrKw == 0 && !seenVarPos && kwOnly == 0 && !seenVarKw);
          ++posOnly;
          break;
        case ParamKind.IPOS_OR_KW:
          dcheck(!seenVarPos && kwOnly == 0 && !seenVarKw);
          ++posOrKw;
          break;
        case ParamKind.IVAR_POS:
          dcheck(!seenVarPos && kwOnly == 0 && !seenVarKw);
          seenVarPos = true;
          allPos = false;
          break;
        case ParamKind.IKW_ONLY:
          dcheck(!seenVarKw);
          ++kwOnly;
          allPos = false;
          break;
        case ParamKind.IVAR_KW:
          dcheck(!seenVarKw);
          seenVarKw = true;
          allPos = false;
          break;
        default:
          throw new InternalErrorException("invalid param kind " + paramKinds[i]);
      }
    }
    return new Signature(
        paramNames,
        paramKinds,
        posOnly,
        posOrKw,
        (seenVarPos ? FLAG_HAS_VAR_POS : 0)
            | (seenVarKw ? FLAG_HAS_VAR_KW : 0)
            | (allPos ? FLAG_ALL_POS : 0));
  }

  @CanIgnoreReturnValue
  public static class Builder {
    private final String[] names;
    private final int[] kinds;
    private int cur;
    private int posOnly;
    private int posOrKw;
    private boolean varPos;
    private int kwOnly;
    private boolean varKw;
    private boolean allPos = true;

    private Builder(int paramCount) {
      names = paramCount == 0 ? EMPTY_STRING_ARRAY : new String[paramCount];
      kinds = paramCount == 0 ? EMPTY_INT_ARRAY : new int[paramCount];
    }

    public Builder add(String name) {
      add(name, ParamKind.POS_OR_KW);
      return this;
    }

    public Builder add(String name, ParamKind kind) {
      if (Debug.ENABLED) {
        for (int i = 0; i != cur; ++i) {
          dcheck(!names[i].equals(name));
        }
      }

      names[cur] = name;
      kinds[cur] = kind.ordinal();
      ++cur;
      switch (kind) {
        case POS_ONLY:
          dcheck(posOrKw == 0 && !varPos && kwOnly == 0 && !varKw);
          ++posOnly;
          break;
        case POS_OR_KW:
          dcheck(!varPos && kwOnly == 0 && !varKw);
          ++posOrKw;
          break;
        case VAR_POS:
          dcheck(!varPos && kwOnly == 0 && !varKw);
          varPos = true;
          allPos = false;
          break;
        case KW_ONLY:
          dcheck(!varKw);
          ++kwOnly;
          allPos = false;
          break;
        case VAR_KW:
          dcheck(!varKw);
          varKw = true;
          allPos = false;
          break;
      }
      return this;
    }

    @CheckReturnValue
    public Signature build() {
      if (cur != names.length) {
        throw new InternalErrorException("not enough parameters");
      }
      return new Signature(
          names,
          kinds,
          posOnly,
          posOrKw,
          (varPos ? FLAG_HAS_VAR_POS : 0)
              | (varKw ? FLAG_HAS_VAR_KW : 0)
              | (allPos ? FLAG_ALL_POS : 0));
    }
  }

  static {
    Builder b = builder(2);
    b.add("args", ParamKind.VAR_POS);
    b.add("kwargs", ParamKind.VAR_KW);
    VARARGS = b.build();
  }

  @Override
  public void validate(Validator v) {
    validate();
  }

  private void validate() {
    dcheck(posOnly >= 0, "bad signature");
    dcheck(posOrKw >= 0, "bad signature");
    dcheck(paramKinds.length == paramNames.length, "bad signature");
  }
}
