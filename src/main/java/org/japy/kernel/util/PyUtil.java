package org.japy.kernel.util;

import java.math.BigInteger;

import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.num.PyInt;

public class PyUtil {
  public static PyBool wrap(boolean value) {
    return PyBool.of(value);
  }

  public static PyInt wrap(int value) {
    return PyInt.get(value);
  }

  public static PyInt wrap(long value) {
    return PyInt.get(value);
  }

  public static PyInt wrap(BigInteger value) {
    return PyInt.get(value);
  }

  public static PyStr wrapUcs2(String value) {
    return PyStr.ucs2(value);
  }
}
