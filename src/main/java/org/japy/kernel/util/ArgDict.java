package org.japy.kernel.util;

import java.util.LinkedHashMap;

import org.japy.infra.coll.ctx.CtxAwareSet;
import org.japy.kernel.types.IPyObj;

public class ArgDict extends LinkedHashMap<String, IPyObj> {
  public static final ArgDict EMPTY = new ArgDict();

  public ArgDict() {}

  public ArgDict(int expectedSize) {
    super(CtxAwareSet.capacity(expectedSize));
  }

  public ArgDict(ArgDict dict) {
    super(dict);
  }
}
