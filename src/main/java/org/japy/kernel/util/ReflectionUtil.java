package org.japy.kernel.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.checkerframework.checker.nullness.qual.Nullable;

public class ReflectionUtil {
  public static <T extends Annotation> @Nullable Constructor<?> findAnnotatedConstructor(
      Class<?> clazz, Class<T> annotationClass) {
    for (Constructor<?> constructor : clazz.getDeclaredConstructors()) {
      if (constructor.getAnnotation(annotationClass) != null) {
        return constructor;
      }
    }
    return null;
  }

  public static <T extends Annotation> @Nullable Method findAnnotatedMethod(
      Class<?> clazz, Class<T> annotationClass) {
    for (Method method : clazz.getDeclaredMethods()) {
      if (method.getAnnotation(annotationClass) != null) {
        return method;
      }
    }
    return null;
  }
}
