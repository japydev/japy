package org.japy.kernel.util;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.coll.seq.PyTuple.EMPTY_TUPLE;
import static org.japy.kernel.types.coll.str.PyStr.EMPTY_STRING;
import static org.japy.kernel.types.exc.px.PxException.typeError;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;
import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.infra.validation.ISupportsValidation;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.Cell;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.ParamDefault;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.num.PyInt;

public class ArgParser implements ISupportsValidation {
  public static final ArgParser NO_PARAMS = new ArgParser(Signature.NO_PARAMS, null);

  private final Signature sig;
  private final @Nullable IPyObj @Nullable [] defaults;

  public ArgParser(Signature sig, @Nullable IPyObj @Nullable [] defaults) {
    this.sig = sig;
    this.defaults = defaults;
    dcheck(defaults == null || sig.paramCount() == defaults.length);
  }

  public ArgParser(String paramName, @Nullable IPyObj defaultValue) {
    this.sig = Signature.builder(1).add(paramName).build();
    this.defaults = new IPyObj[] {defaultValue};
  }

  public ArgParser(String paramName, ParamKind paramKind, @Nullable IPyObj defaultValue) {
    this.sig = Signature.builder(1).add(paramName, paramKind).build();
    this.defaults = new IPyObj[] {defaultValue};
  }

  public static ArgParser make(@Nullable IPyObj @Nullable [] defaults, Signature sig) {
    return new ArgParser(sig, defaults);
  }

  public IPyObj[] parse(Args args, String name, PyContext ctx) {
    if (sig.paramCount() == 0) {
      verifyNoArgs(args, name, ctx);
    }

    if (canForwardPos(args, sig)) {
      return args.getPosArray();
    }

    return ArgArrayCollector.parse(args, name, sig, defaults, ctx);
  }

  public IPyObj parse1(Args args, String name, PyContext ctx) {
    dcheck(sig.paramCount() == 1);

    if (canForwardPos(args, sig)) {
      return args.getPos(0);
    }

    return ArgCollector1.parse(args, name, sig, defaults, ctx);
  }

  public static Builder builder(int paramCount) {
    return new Builder(paramCount);
  }

  @Override
  public void validate(Validator v) {
    v.validate(sig);
    v.validateIfNotNull(defaults);
  }

  @CanIgnoreReturnValue
  public static class Builder {
    private final Signature.Builder sigBuilder;
    private final int paramCount;
    private IPyObj @Nullable [] defaults;
    private int pos;

    private Builder(int paramCount) {
      this.paramCount = paramCount;
      sigBuilder = Signature.builder(paramCount);
    }

    public Builder add(String name) {
      return add(name, ParamKind.POS_OR_KW);
    }

    public Builder add(String name, ParamKind kind) {
      return add(name, kind, null);
    }

    public Builder add(String name, @Nullable IPyObj dflt) {
      return add(name, ParamKind.POS_OR_KW, dflt);
    }

    public Builder add(Param annotation) {
      String name = annotation.value();
      if (name.isEmpty()) {
        throw new IllegalArgumentException("parameter name is missing");
      }
      @Var
      @Nullable
      IPyObj dflt = null;
      if (!annotation.defaultStr().isEmpty()) {
        dflt = PyStr.get(annotation.defaultStr());
        dcheck(annotation.dflt() == ParamDefault.NO_DEFAULT);
      } else {
        switch (annotation.dflt()) {
          case DEFAULT_ZERO:
            dflt = PyInt.ZERO;
            break;
          case DEFAULT_ONE:
            dflt = PyInt.ONE;
            break;
          case DEFAULT_NEG_ONE:
            dflt = PyInt.NEGATIVE_ONE;
            break;
          case DEFAULT_NONE:
            dflt = None;
            break;
          case DEFAULT_FALSE:
            dflt = False;
            break;
          case DEFAULT_TRUE:
            dflt = True;
            break;
          case DEFAULT_SENTINEL:
            dflt = Sentinel;
            break;
          case DEFAULT_EMPTY_TUPLE:
            dflt = EMPTY_TUPLE;
            break;
          case DEFAULT_EMPTY_STRING:
            dflt = EMPTY_STRING;
            break;
          case NO_DEFAULT:
            break;
        }
      }
      return add(name, annotation.kind(), dflt);
    }

    public Builder addPosOnly(String name) {
      return addPosOnly(name, null);
    }

    public Builder addPosOnly(String name, @Nullable IPyObj dflt) {
      return add(name, ParamKind.POS_ONLY, dflt);
    }

    public Builder add(String name, ParamKind kind, @Nullable IPyObj dflt) {
      sigBuilder.add(name, kind);
      if (dflt != null) {
        if (defaults == null) {
          defaults = new IPyObj[paramCount];
        }
        defaults[pos] = dflt;
      }
      ++pos;
      return this;
    }

    @CheckReturnValue
    public ArgParser build() {
      return new ArgParser(sigBuilder.build(), defaults);
    }
  }

  public static void verifyNoArgs(Args args, String name, PyContext ctx) {
    if (!args.isEmpty()) {
      throw typeError(
          String.format("function %s() takes no parameters (%s given)", name, args.size()), ctx);
    }
  }

  public static void verifyNoKwArgs(Args args, String name, PyContext ctx) {
    if (args.kwCount() != 0) {
      throw typeError(
          String.format(
              "function %s() takes no keyword parameters (%s given)", name, args.kwCount()),
          ctx);
    }
  }

  private interface IArgCollector {
    void set(int param, @Nullable IPyObj value);

    boolean isSet(int param);
  }

  private static boolean canForwardPos(Args args, Signature sig) {
    return sig.allPos() && args.posCount() == sig.paramCount() && args.kwCount() == 0;
  }

  private static void parse(
      IArgCollector consumer,
      Args args,
      String funcName,
      Signature sig,
      @Nullable IPyObj @Nullable [] paramDefaults,
      PyContext ctx) {
    dcheck(paramDefaults == null || sig.paramCount() == paramDefaults.length);

    int paramCount = sig.paramCount();

    for (int i = 0, argCount = args.posCount(); i < argCount; ++i) {
      if (i == paramCount) {
        throw typeError(
            String.format(
                "%s positional arguments provided for function %s(), expected at most %s",
                args.posCount(), funcName, i),
            ctx);
      }
      int kind = sig.paramKinds[i];
      if (kind == ParamKind.IPOS_ONLY || kind == ParamKind.IPOS_OR_KW) {
        consumer.set(i, args.getPos(i));
      } else if (kind == ParamKind.IVAR_POS) {
        consumer.set(i, args.posTuple(i));
        break;
      } else {
        throw typeError(
            String.format(
                "%s positional arguments provided for function %s(), expected at most %s",
                args.posCount(), funcName, i),
            ctx);
      }
    }
    int varPos = sig.idxVarPos();
    if (varPos != Signature.INVALID_INDEX && !consumer.isSet(varPos)) {
      consumer.set(varPos, EMPTY_TUPLE);
    }

    int varKw = sig.idxVarKw();
    @Nullable PyDict varKwArg = varKw != Signature.INVALID_INDEX ? new PyDict() : null;
    if (varKwArg != null) {
      consumer.set(varKw, varKwArg);
    }
    if (args.kwCount() != 0) {
      if (!sig.hasKw()) {
        throw typeError(String.format("%s() takes no keyword arguments", funcName), ctx);
      }

      args.forEachKw(
          (argName, argValue) -> {
            int idx = sig.paramIndex(argName);
            if (idx != Signature.INVALID_INDEX
                && sig.paramKinds[idx] != ParamKind.IPOS_ONLY
                && sig.paramKinds[idx] != ParamKind.IVAR_POS) {
              int kind = sig.paramKinds[idx];
              if (kind != ParamKind.IPOS_OR_KW && kind != ParamKind.IKW_ONLY) {
                throw typeError(
                    String.format(
                        "'%s' is an invalid keyword argument for %s()", argName, funcName),
                    ctx);
              }
              if (consumer.isSet(idx)) {
                throw typeError(
                    String.format(
                        "parameter '%s' is specified twice for function %s", argName, funcName),
                    ctx);
              }
              consumer.set(idx, argValue);
            } else if (varKwArg != null) {
              varKwArg.setItem(argName, argValue, ctx);
            } else {
              throw typeError(
                  String.format("'%s' is an invalid keyword argument for %s()", argName, funcName),
                  ctx);
            }
          });
    }

    for (int i = 0; i < paramCount; ++i) {
      if (paramDefaults != null && !consumer.isSet(i)) {
        consumer.set(i, paramDefaults[i]);
      }
      if (!consumer.isSet(i)) {
        throw typeError(
            String.format("in %s(): missing parameter '%s'", funcName, sig.paramNames[i]), ctx);
      }
    }
  }

  private static class ArgArrayCollector implements IArgCollector {
    private final IPyObj[] args;

    private ArgArrayCollector(int paramCount) {
      this.args = new IPyObj[paramCount];
    }

    @Override
    public void set(int param, @Nullable IPyObj value) {
      args[param] = value;
    }

    @Override
    public boolean isSet(int param) {
      return args[param] != null;
    }

    public static IPyObj[] parse(
        Args args,
        String name,
        Signature sig,
        @Nullable IPyObj @Nullable [] paramDefaults,
        PyContext ctx) {
      ArgArrayCollector c = new ArgArrayCollector(sig.paramCount());
      ArgParser.parse(c, args, name, sig, paramDefaults, ctx);
      return c.args;
    }
  }

  private static class ArgCollector1 implements IArgCollector {
    public @Nullable IPyObj arg;

    @Override
    public void set(int param, @Nullable IPyObj value) {
      dcheck(param == 0);
      arg = value;
    }

    @Override
    public boolean isSet(int param) {
      dcheck(param == 0);
      return arg != null;
    }

    public static IPyObj parse(
        Args args,
        String name,
        Signature sig,
        @Nullable IPyObj @Nullable [] paramDefaults,
        PyContext ctx) {
      ArgCollector1 c = new ArgCollector1();
      ArgParser.parse(c, args, name, sig, paramDefaults, ctx);
      dcheckNotNull(c.arg);
      return c.arg;
    }
  }

  private static class LocalCollector implements IArgCollector {
    private final Cell[] locals;

    private LocalCollector(Cell[] locals) {
      this.locals = locals;
    }

    public static void parse(
        Args args,
        String name,
        Signature sig,
        @Nullable IPyObj @Nullable [] paramDefaults,
        Cell[] locals,
        PyContext ctx) {
      ArgParser.parse(new LocalCollector(locals), args, name, sig, paramDefaults, ctx);
    }

    @Override
    public void set(int param, @Nullable IPyObj value) {
      locals[param].value = value;
    }

    @Override
    public boolean isSet(int param) {
      return locals[param].value != null;
    }
  }

  public void parse(Args args, String name, Cell[] locals, PyContext ctx) {
    if (sig.paramCount() == 0) {
      verifyNoArgs(args, name, ctx);
    }

    if (canForwardPos(args, sig)) {
      args.copyPos(locals, sig.paramCount());
    }

    LocalCollector.parse(args, name, sig, defaults, locals, ctx);
  }
}
