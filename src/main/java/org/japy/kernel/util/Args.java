package org.japy.kernel.util;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.exc.px.PxException.typeError;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.coll.CollUtil;
import org.japy.kernel.exec.Cell;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxKeyError;

public class Args {
  public static final Args EMPTY = new Args(IPyObj.EMPTY_OBJECT_ARRAY, ArgDict.EMPTY);

  private final IPyObj[] pos;
  private final ArgDict kw;

  public Args(IPyObj[] pos, ArgDict kw) {
    this.pos = pos;
    this.kw = kw;
  }

  public Args(IPyObj[] pos) {
    this(pos, ArgDict.EMPTY);
  }

  public Args(int posCount, int kwCount) {
    this(new IPyObj[posCount], new ArgDict(kwCount));
  }

  public boolean isEmpty() {
    return posCount() == 0 && kw.isEmpty();
  }

  public int size() {
    return posCount() + kw.size();
  }

  public int posCount() {
    return pos.length;
  }

  public IPyObj getPos(int i) {
    return pos[i];
  }

  public IPyObj[] getPosArray() {
    return pos;
  }

  public void copyPos(Cell[] locals, int paramCount) {
    dcheck(paramCount == posCount());
    for (int i = 0; i < paramCount; ++i) {
      locals[i].value = pos[i];
    }
  }

  public PyTuple posTuple() {
    return posTuple(0);
  }

  public PyTuple posTuple(int start) {
    return start == 0 ? new PyTuple(pos) : new PyTuple(pos, start, pos.length);
  }

  public @Nullable IPyObj getKwOrNull(String name) {
    return kw.get(name);
  }

  public IPyObj getKw(String name, PyContext ctx) {
    @Nullable IPyObj v = kw.get(name);
    if (v == null) {
      throw new PxKeyError(name, ctx);
    }
    return v;
  }

  public int kwCount() {
    return kw.size();
  }

  public void forEachKw(BiConsumer<String, IPyObj> func) {
    kw.forEach(func);
  }

  public Args prepend(IPyObj obj) {
    return new Args(CollUtil.prepend(obj, pos), kw);
  }

  public Args tail() {
    return new Args(CollUtil.tail(pos), kw);
  }

  public Args posOnly() {
    return new Args(pos, ArgDict.EMPTY);
  }

  public Args withPos(IPyObj... newPos) {
    return new Args(newPos, kw);
  }

  public static Args of() {
    return EMPTY;
  }

  public static Args of(IPyObj arg) {
    return of(new IPyObj[] {arg});
  }

  public static Args of(IPyObj arg1, IPyObj arg2) {
    return of(new IPyObj[] {arg1, arg2});
  }

  public static Args of(IPyObj... args) {
    return new Args(args, ArgDict.EMPTY);
  }

  public static Args of(ArgDict kw, IPyObj... args) {
    return new Args(args, kw);
  }

  public static Args of(IPyObj[] args, ArgDict kw) {
    return new Args(args, kw);
  }

  public static Args star(IPyObj iterable, PyContext ctx) {
    return builder(ctx).unpackAndAddPos(iterable, ctx).build();
  }

  public static Builder builder(PyContext ctx) {
    return new Builder(0, 0, ctx);
  }

  public static Builder builder(int expectedPos, int expectedKw, PyContext ctx) {
    return new Builder(expectedPos, expectedKw, ctx);
  }

  public Args withoutKw(String name) {
    ArgDict d = new ArgDict(kw);
    d.remove(name);
    return new Args(pos, d);
  }

  public Args withoutPos() {
    return new Args(IPyObj.EMPTY_OBJECT_ARRAY, kw);
  }

  @CanIgnoreReturnValue
  public static class Builder {
    private final PyContext ctx;
    private @Nullable List<IPyObj> pos;
    private @Nullable ArgDict kw;

    private Builder(int expectedPos, int expectedKw, PyContext ctx) {
      this.ctx = ctx;
      if (expectedPos > 0) {
        pos = new ArrayList<>(expectedPos);
      }
      if (expectedKw > 0) {
        kw = new ArgDict(expectedKw);
      }
    }

    public Builder add(IPyObj arg) {
      if (pos == null) {
        pos = new ArrayList<>();
      }
      pos.add(arg);
      return this;
    }

    public Builder add(String name, IPyObj value) {
      if (kw == null) {
        kw = new ArgDict();
      }
      if (kw.containsKey(name)) {
        throw typeError(String.format("got multiple values for keyword argument '%s'", name), ctx);
      }
      kw.put(name, value);
      return this;
    }

    public Builder unpackAndAddPos(IPyObj list, PyContext ctx) {
      CollLib.forEach(list, this::add, ctx);
      return this;
    }

    public Builder unpackAndAddKw(IPyObj dict, PyContext ctx) {
      CollLib.dictForEach(
          dict,
          (k, v) -> {
            if (!(k instanceof PyStr)) {
              throw typeError("keywords must be strings", ctx);
            }
            add(k.toString(), v);
          },
          ctx);
      return this;
    }

    @CheckReturnValue
    public Args build() {
      return new Args(
          pos != null && !pos.isEmpty() ? pos.toArray(IPyObj[]::new) : IPyObj.EMPTY_OBJECT_ARRAY,
          kw != null && !kw.isEmpty() ? kw : ArgDict.EMPTY);
    }
  }
}
