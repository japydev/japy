package org.japy.kernel.util;

import org.japy.infra.validation.ISupportsValidation;
import org.japy.infra.validation.Validator;
import org.japy.kernel.types.IPyObj;

public class PyValidator extends Validator {
  @Override
  protected void doValidate(ISupportsValidation obj) {
    if (obj instanceof IPyObj) {
      validate(((IPyObj) obj).type());
    }
    super.doValidate(obj);
  }
}
