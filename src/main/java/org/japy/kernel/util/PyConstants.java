package org.japy.kernel.util;

import org.japy.kernel.types.coll.str.PyStr;

public class PyConstants {
  public static final PyStr __ANNOTATIONS__ = PyStr.ucs2(Constants.__ANNOTATIONS__);
  public static final PyStr __BUILTINS__ = PyStr.ucs2(Constants.__BUILTINS__);
  public static final PyStr UTF_8 = PyStr.ucs2(Constants.UTF_8);
  public static final PyStr STRICT = PyStr.ucs2(Constants.STRICT);
  public static final PyStr IGNORE = PyStr.ucs2(Constants.IGNORE);
  public static final PyStr REPLACE = PyStr.ucs2(Constants.REPLACE);
  public static final PyStr XMLCHARREFREPLACE = PyStr.ucs2(Constants.XMLCHARREFREPLACE);
  public static final PyStr BACKSLASHREPLACE = PyStr.ucs2(Constants.BACKSLASHREPLACE);
  public static final PyStr NAMEREPLACE = PyStr.ucs2(Constants.NAMEREPLACE);
  public static final PyStr SURROGATEESCAPE = PyStr.ucs2(Constants.SURROGATEESCAPE);
  public static final PyStr SURROGATEPASS = PyStr.ucs2(Constants.SURROGATEPASS);
  public static final PyStr NONE = PyStr.ucs2(Constants.NONE);
  public static final PyStr TRUE = PyStr.ucs2(Constants.TRUE);
  public static final PyStr FALSE = PyStr.ucs2(Constants.FALSE);
  public static final PyStr __NAME__ = PyStr.ucs2(Constants.__NAME__);
  public static final PyStr __MAIN__ = PyStr.ucs2(Constants.__MAIN__);
  public static final PyStr TILDE = PyStr.ucs2("~");
  public static final PyStr SPACE = PyStr.ucs2(" ");
  public static final PyStr NL = PyStr.ucs2("\n");
  public static final PyStr EQ = PyStr.ucs2("=");
  public static final PyStr COLONSPACE = PyStr.ucs2(": ");
  public static final PyStr UNKNOWN_MODULE = PyStr.ucs2("<unknown>");
  public static final PyStr BUILTINS = PyStr.ucs2(Constants.BUILTINS);
  public static final PyStr IMPL_NAME_CAP = PyStr.ucs2(Constants.IMPL_NAME_CAP);
  public static final PyStr __EQ__ = PyStr.ucs2(Constants.__EQ__);
  public static final PyStr __HASH__ = PyStr.ucs2(Constants.__HASH__);
}
