package org.japy.kernel.types.cls.builtin;

import static org.japy.infra.util.ObjUtil.or;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.ClassLib;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.PyAbstractClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.util.PyConstants;

public abstract class PyAbstractBuiltinClass extends PyAbstractClass implements IPyBuiltinClass {
  // Must keep it null by default because this may be PyStr itself or its dependency.
  // Null means "builtins".
  protected @Nullable PyStr module;
  // Initalized on demand
  private volatile @Nullable IPyObj doc;

  protected PyAbstractBuiltinClass(String name, IPyClass parent) {
    super(getName(name), parent);
    module = getModule(name);
  }

  protected PyAbstractBuiltinClass(String name, @Nullable Void root) {
    super(getName(name), root);
    module = getModule(name);
  }

  protected PyAbstractBuiltinClass(String name, IPyClass[] parents) {
    super(getName(name), parents, ClassLib.getBaseMro(parents, null));
    module = getModule(name);
  }

  @Override
  public void setAttr(String attr, IPyObj value, PyContext ctx) {
    throw PxAttributeError.readOnlyAttribute(attr, this, ctx);
  }

  @Override
  public void delAttr(String attr, PyContext ctx) {
    throw PxAttributeError.nonDeletableAttribute(attr, this, ctx);
  }

  protected abstract IPyObj getDoc();

  @Override
  public IPyObj doc() {
    if (doc == null) {
      synchronized (this) {
        if (doc == null) {
          doc = getDoc();
        }
      }
    }
    return dcheckNotNull(doc);
  }

  private static String getName(String fullName) {
    int lastDot = fullName.lastIndexOf('.');
    if (lastDot != -1) {
      return fullName.substring(lastDot + 1);
    } else {
      return fullName;
    }
  }

  private static @Nullable PyStr getModule(String fullName) {
    int lastDot = fullName.lastIndexOf('.');
    if (lastDot != -1) {
      return PyStr.get(fullName.substring(0, lastDot));
    } else {
      // must keep it null in case this is PyStr itself or some dependency of PyStr
      return null;
    }
  }

  private PxException throwReadOnlyAttributes(PyContext ctx) {
    throw new PxAttributeError(
        String.format("cannot set attributes of built-in class '%s'", name()), ctx);
  }

  @Override
  public boolean isBuiltinClass() {
    return true;
  }

  @Override
  public PyStr module() {
    return or(module, PyConstants.BUILTINS);
  }

  @Override
  public void setModule(PyStr module, PyContext ctx) {
    this.module = module;
  }

  @Override
  public void setName(PyStr name, PyContext ctx) {
    throw throwReadOnlyAttributes(ctx);
  }

  @Override
  public void setQualName(PyStr qualName, PyContext ctx) {
    throw throwReadOnlyAttributes(ctx);
  }

  @Override
  public void setDoc(IPyObj doc, PyContext ctx) {
    throw throwReadOnlyAttributes(ctx);
  }

  @Override
  public IPyObj annotations() {
    return None;
  }

  @Override
  public void setAnnotations(IPyObj annotations, PyContext ctx) {
    throw throwReadOnlyAttributes(ctx);
  }
}
