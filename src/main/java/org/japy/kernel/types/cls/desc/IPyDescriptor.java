package org.japy.kernel.types.cls.desc;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

public interface IPyDescriptor extends IPyTrueAtomObj {
  @InstanceMethod
  IPyObj __set_name__(@Param("cls") IPyObj cls, @Param("name") IPyObj name, PyContext ctx);

  @InstanceAttribute
  IPyObj __objclass__(PyContext ctx);

  @InstanceAttribute
  IPyObj __name__(PyContext ctx);
}
