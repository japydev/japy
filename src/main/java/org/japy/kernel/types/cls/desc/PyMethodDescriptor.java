package org.japy.kernel.types.cls.desc;

import static org.japy.kernel.util.PyUtil.wrap;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.func.IPyFunc;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

public abstract class PyMethodDescriptor extends PyAbstractDescriptor
    implements IPyFunc, IPyMethodDescriptor {
  public final IPyObj func;

  protected PyMethodDescriptor(IPyObj func) {
    this.func = func;
  }

  @Override
  public IPyObj __isabstractmethod__(PyContext ctx) {
    return wrap(ObjLib.isAbstractMethod(func, ctx));
  }

  @InstanceAttribute
  public IPyObj __func__(PyContext ctx) {
    return func;
  }

  @Override
  public IPyObj __name__(PyContext ctx) {
    return ObjLib.getAttr(func, Constants.__NAME__, ctx);
  }

  @Override
  public IPyObj call(Args args, PyContext ctx) {
    return FuncLib.call(func, args, ctx);
  }

  @SpecialInstanceMethod
  public IPyObj __getattr__(@Param("attr") IPyObj attr, PyContext ctx) {
    String attrName = ObjLib.attrName(attr, ctx);
    switch (attrName) {
      case Constants.__MODULE__:
      case Constants.__NAME__:
      case Constants.__QUALNAME__:
      case Constants.__DOC__:
      case Constants.__ANNOTATIONS__:
        return ObjLib.getAttr(func, attrName, ctx);
      case Constants.__FUNC__:
      case Constants.__WRAPPED__:
        return func;
      default:
        throw new PxAttributeError(attrName, this, false, ctx);
    }
  }
}
