package org.japy.kernel.types.cls.ext;

import static org.japy.infra.validation.Debug.dcheck;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyMetaClass;
import org.japy.kernel.types.cls.PyTypeClass;
import org.japy.kernel.types.cls.builtin.IPyBuiltinClass;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.util.Args;

public class PyExtMetaClass extends PyExtClass implements IPyMetaClass {
  public PyExtMetaClass(ExtClassData data, IPyMetaClass meta) {
    super(data, meta);

    if (Debug.ENABLED) {
      dcheck(data.builtinAncestor == PyTypeClass.INSTANCE);
      dcheck(data.builtinInstanceClass == null);
    }
  }

  @Override
  public IPyObj instantiate(Args args, PyContext ctx) {
    throw PxException.typeError("object.__new__(type) is not safe, use type.__new__()", ctx);
  }

  @Override
  public @Nullable IPyBuiltinClass builtinAncestor() {
    return PyTypeClass.INSTANCE;
  }

  @Override
  public @Nullable Class<? extends IPyObj> builtinInstanceClass() {
    throw InternalErrorException.notReached();
  }
}
