package org.japy.kernel.types.cls.desc;

import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.func.ArgLib;

public abstract class PyAbstractDescriptor implements IPyDescriptorGet {
  protected @Nullable IPyClass objClass;
  protected @Nullable PyStr name;

  @Override
  public IPyObj __set_name__(@Param("cls") IPyObj cls, @Param("name") IPyObj name, PyContext ctx) {
    if (this.objClass == null) {
      this.objClass = ArgLib.checkArgClass("__set_name__", "cls", cls, ctx);
      this.name = ArgLib.checkArgStr("__set_name__", "name", name, ctx);
    }
    return None;
  }

  protected IPyClass objClass() {
    return dcheckNotNull(objClass);
  }

  @Override
  public IPyObj __objclass__(PyContext ctx) {
    return objClass();
  }

  @Override
  public IPyObj __name__(PyContext ctx) {
    return dcheckNotNull(name);
  }

  @Override
  public void validate(Validator v) {
    v.validateIfNotNull(objClass);
  }
}
