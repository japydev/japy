package org.japy.kernel.types.cls.ext.adapters;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Adapter;
import org.japy.kernel.types.annotations.AdapterMethod;
import org.japy.kernel.types.annotations.ProtocolSuperMethod;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.ext.IPyBuiltinSubClassInstance;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.proto.IPyObjWithLen;
import org.japy.kernel.util.Constants;

@Adapter(IPyObjWithLen.class)
public interface IPyObjWithLenAdapter extends IPyBuiltinSubClassInstance {
  int baseLen(PyContext ctx);

  @AdapterMethod
  default int thisLen(PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.LEN);
    if (h == DELETED_METHOD) {
      throw CollLib.throwLenUnsupported(this, ctx);
    }
    if (h == null) {
      return baseLen(ctx);
    }
    try {
      return (int) h.handle.invokeExact((IPyObj) this, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @ProtocolSuperMethod(Constants.__LEN__)
  default IPyObj superLen(PyContext ctx) {
    return PyInt.get(baseLen(ctx));
  }
}
