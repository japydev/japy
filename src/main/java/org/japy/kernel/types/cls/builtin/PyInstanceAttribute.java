package org.japy.kernel.types.cls.builtin;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodType;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.desc.IPyDescriptorDel;
import org.japy.kernel.types.cls.desc.IPyDescriptorGet;
import org.japy.kernel.types.cls.desc.IPyDescriptorSet;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxAttributeError;

public class PyInstanceAttribute implements IPyDescriptorGet, IPyDescriptorDel, IPyDescriptorSet {
  public static final MethodType METHOD_TYPE_GETTER =
      MethodType.methodType(IPyObj.class, IPyObj.class, PyContext.class);
  public static final MethodType METHOD_TYPE_SETTER =
      MethodType.methodType(void.class, IPyObj.class, IPyObj.class, PyContext.class);
  public static final MethodType METHOD_TYPE_DELETER =
      MethodType.methodType(void.class, IPyObj.class, PyContext.class);

  private final IPyClass objClass;
  private final String name;
  private @Nullable FuncHandle getter;
  private @Nullable FuncHandle setter;
  private @Nullable FuncHandle deleter;

  public PyInstanceAttribute(IPyClass objClass, String name) {
    this.objClass = objClass;
    this.name = name;
  }

  public PyInstanceAttribute(
      IPyClass objClass,
      String name,
      @Nullable FuncHandle getter,
      @Nullable FuncHandle setter,
      @Nullable FuncHandle deleter) {
    this(objClass, name);
    setGetter(getter);
    setSetter(setter);
    setDeleter(deleter);
  }

  public @Nullable FuncHandle getter() {
    return getter;
  }

  public @Nullable FuncHandle setter() {
    return setter;
  }

  public @Nullable FuncHandle deleter() {
    return deleter;
  }

  public void setGetter(@Nullable FuncHandle getter) {
    this.getter = getter;
    if (Debug.ENABLED) {
      if (getter != null) {
        dcheck(getter.type().equals(METHOD_TYPE_GETTER), getter.type().toString());
      }
    }
  }

  public void setSetter(@Nullable FuncHandle setter) {
    this.setter = setter;
    if (Debug.ENABLED) {
      if (setter != null) {
        dcheck(setter.type().equals(METHOD_TYPE_SETTER), setter.type().toString());
      }
    }
  }

  public void setDeleter(@Nullable FuncHandle deleter) {
    this.deleter = deleter;
    if (Debug.ENABLED) {
      if (deleter != null) {
        dcheck(deleter.type().equals(METHOD_TYPE_DELETER), deleter.type().toString());
      }
    }
  }

  @Override
  public IPyObj __set_name__(IPyObj cls, IPyObj name, PyContext ctx) {
    return None;
  }

  @Override
  public IPyObj __name__(PyContext ctx) {
    return PyStr.get(name);
  }

  @Override
  public IPyObj __objclass__(PyContext ctx) {
    return objClass;
  }

  @Override
  public IPyObj get(IPyObj instance, IPyObj owner, PyContext ctx) {
    if (instance == None) {
      return this;
    }

    if (getter == null) {
      throw new PxAttributeError(
          String.format("'%s' object attribute '%s' is write-only", objClass.name(), name), ctx);
    }

    try {
      return (IPyObj) getter.handle.invokeExact(instance, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public void set(IPyObj instance, IPyObj value, PyContext ctx) {
    if (setter == null) {
      throw new PxAttributeError(
          String.format("'%s' object attribute '%s' is read-only", objClass.name(), name), ctx);
    }

    try {
      setter.handle.invokeExact(instance, value, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public void delete(IPyObj instance, PyContext ctx) {
    if (deleter == null) {
      if (setter == null) {
        throw new PxAttributeError(
            String.format("'%s' object attribute '%s' is read-only", objClass.name(), name), ctx);
      } else {
        throw new PxAttributeError(
            String.format("'%s' object attribute '%s' cannot be deleted", objClass.name(), name),
            ctx);
      }
    }

    try {
      deleter.handle.invokeExact(instance, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  private static class TypeHolder {
    static final IPyClass TYPE =
        new PyBuiltinClass("instanceattribute", PyInstanceAttribute.class, PyBuiltinClass.INTERNAL);
  }

  @Override
  public IPyClass type() {
    return TypeHolder.TYPE;
  }

  @Override
  public void validate(Validator v) {
    v.validate(objClass);
  }
}
