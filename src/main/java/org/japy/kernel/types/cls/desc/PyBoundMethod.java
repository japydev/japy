package org.japy.kernel.types.cls.desc;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.util.PyUtil.wrap;

import java.lang.invoke.MethodHandles;

import org.japy.infra.validation.Debug;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.func.IPyFunc;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.proto.IPyObjWithEq;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

class PyBoundMethod implements IPyFunc, IPyObjWithEq {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "boundmethod", PyBoundMethod.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private final IPyObj self;
  private final IPyObj method;
  private final String name;

  PyBoundMethod(IPyObj self, IPyObj method, String name) {
    if (Debug.ENABLED) {
      dcheck(self != None);
      dcheck(self != None.type());
    }
    this.self = self;
    this.method = method;
    this.name = name;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj call(Args args, PyContext ctx) {
    return FuncLib.call(method, args.prepend(self), ctx);
  }

  @InstanceAttribute
  public IPyObj __name__(PyContext ctx) {
    return PyStr.get(name);
  }

  @Override
  public void validate(Validator v) {
    v.validate(self);
    v.validate(method);
  }

  @SpecialInstanceMethod
  private IPyObj __getattr__(@Param("attr") IPyObj attr, PyContext ctx) {
    String attrName = ObjLib.attrName(attr, ctx);
    switch (attrName) {
      case Constants.__SELF__:
        return self;
      case Constants.__FUNC__:
        return method;
      default:
        return ObjLib.getAttr(method, attrName, ctx);
    }
  }

  @Override
  public IPyObj __eq__(IPyObj other, PyContext ctx) {
    if (!(other instanceof PyBoundMethod)) {
      return NotImplemented;
    }
    return wrap(self == ((PyBoundMethod) other).self && method == ((PyBoundMethod) other).method);
  }

  @Override
  public int intHashCode(PyContext ctx) {
    return method.hashCode(ctx);
  }
}
