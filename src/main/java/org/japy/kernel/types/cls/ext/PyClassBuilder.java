package org.japy.kernel.types.cls.ext;

import static org.japy.infra.util.ObjUtil.or;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.util.ArrayList;
import java.util.List;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.Cell;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.IPyMetaClass;
import org.japy.kernel.types.cls.PyTypeClass;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

public class PyClassBuilder {
  private @Nullable PyStr name;
  private @Nullable PyStr qualName;
  private Args args = Args.EMPTY;
  private @Nullable IPyClassExec exec;
  private @Nullable IPyRODict builtins;
  private @Nullable IPyDict globals;

  private static class Bases {
    final PyTuple effective;
    final @Nullable PyTuple orig;

    private Bases(PyTuple effective, @Nullable PyTuple orig) {
      this.effective = effective;
      this.orig = orig;
    }
  }

  public PyClassBuilder() {}

  public PyClassBuilder name(PyStr name) {
    this.name = name;
    return this;
  }

  public PyClassBuilder qualName(PyStr qualName) {
    this.qualName = qualName;
    return this;
  }

  public PyClassBuilder args(@Nullable Args args) {
    this.args = or(args, Args.EMPTY);
    return this;
  }

  public PyClassBuilder exec(IPyClassExec exec) {
    this.exec = exec;
    return this;
  }

  public PyClassBuilder globals(IPyDict globals) {
    this.globals = globals;
    return this;
  }

  public PyClassBuilder builtins(IPyRODict builtins) {
    this.builtins = builtins;
    return this;
  }

  public IPyObj build(PyContext ctx) {
    dcheckNotNull(exec);
    dcheckNotNull(globals);
    dcheckNotNull(builtins);

    @Nullable IPyObj explicitMeta = args.getKwOrNull(Constants.METACLASS);
    Args argsNoMeta = explicitMeta != null ? args.withoutKw(Constants.METACLASS) : args;

    Bases bases = resolveBases(ctx);
    IPyObj meta = determineMetaClass(explicitMeta, bases.effective, ctx);
    IPyDict namespace = prepareNamespace(meta, bases, argsNoMeta, ctx);

    Cell classCell = new Cell();
    exec.exec(namespace, globals, builtins, classCell, ctx);

    IPyObj cls =
        FuncLib.call(
            meta, argsNoMeta.withPos(dcheckNotNull(name), bases.effective, namespace), ctx);
    // https://github.com/python/cpython/blob/main/Lib/test/test_subclassinit.py#L115
    // https://discuss.python.org/t/metaclass-new-return-value/13376/4
    if (cls instanceof PyExtClass) {
      if (((PyExtClass) cls).qualName().equals(name.toString())) {
        ((PyExtClass) cls).setQualName(dcheckNotNull(qualName), ctx);
      }
    }

    classCell.value = cls;
    return cls;
  }

  /** https://docs.python.org/3/reference/datamodel.html#resolving-mro-entries */
  private Bases resolveBases(PyContext ctx) {
    List<IPyObj> bases = new ArrayList<>();
    int posCount = args.posCount();
    @Var boolean changedBases = false;
    for (int i = 0; i != posCount; ++i) {
      IPyObj obj = args.getPos(i);
      if (obj instanceof IPyClass) {
        bases.add(obj);
        continue;
      }

      @Nullable IPyObj mroEntries = ObjLib.getAttrOrNull(obj, Constants.__MRO_ENTRIES__, ctx);
      if (mroEntries == null) {
        throw PxException.typeError(
            String.format(
                "base %s is not a class and it does not have __mro_entries__ method",
                PrintLib.repr(obj, ctx)),
            ctx);
      }

      IPyObj classes = FuncLib.call(mroEntries, args.posTuple(), ctx);
      if (!(classes instanceof PyTuple)) {
        throw PxException.typeError(
            String.format("__mro_entries__ returned '%s', expected a tuple", classes.typeName()),
            ctx);
      }
      ((PyTuple) classes)
          .forEachNoCtx(
              b -> {
                if (!(b instanceof IPyClass)) {
                  throw PxException.typeError(
                      String.format(
                          "tuple returned by __mro_entries__ contains an object of type %s, expected a class",
                          b.typeName()),
                      ctx);
                }
                bases.add(b);
              });
      changedBases = true;
    }

    return new Bases(new PyTuple(bases), changedBases ? args.posTuple() : null);
  }

  /** https://docs.python.org/3/reference/datamodel.html#determining-the-appropriate-metaclass */
  private static IPyObj determineMetaClass(
      @Nullable IPyObj explicitMeta, PyTuple bases, PyContext ctx) {
    int baseCount = bases.len();

    if (baseCount == 0 && explicitMeta == null) {
      return PyTypeClass.INSTANCE;
    }

    if (explicitMeta != null && !(explicitMeta instanceof IPyMetaClass)) {
      return explicitMeta;
    }

    @Var
    @Nullable
    IPyMetaClass meta = (IPyMetaClass) explicitMeta;
    for (int i = 0; i != baseCount; ++i) {
      IPyObj base = bases.get(i);
      if (!(base instanceof IPyClass)) {
        throw PxException.typeError(
            String.format("base %s is not a class", PrintLib.repr(base, ctx)), ctx);
      }

      IPyMetaClass thisMeta = ((IPyClass) base).type();
      if (meta == null) {
        meta = thisMeta;
      } else {
        meta = mostDerived(meta, thisMeta);
      }
      if (meta == null) {
        throw PxException.typeError(
            String.format("cannot determine appropriate metaclass for bases %s", bases), ctx);
      }
    }

    return meta;
  }

  private static @Nullable IPyMetaClass mostDerived(IPyMetaClass meta1, IPyMetaClass meta2) {
    if (meta1.isSubClassRaw(meta2)) {
      return meta1;
    } else if (meta2.isSubClassRaw(meta1)) {
      return meta2;
    } else {
      return null;
    }
  }

  private IPyDict prepareNamespace(IPyObj meta, Bases bases, Args argsNoMeta, PyContext ctx) {
    @Nullable IPyObj prepare = ObjLib.getAttrOrNull(meta, Constants.__PREPARE__, ctx);
    if (prepare == null) {
      return new PyDict();
    }
    IPyObj ns =
        FuncLib.call(prepare, argsNoMeta.withPos(dcheckNotNull(name), bases.effective), ctx);
    if (!(ns instanceof IPyDict)) {
      throw PxException.typeError(
          String.format(
              "__prepare__ returned an object of type %s, expected a dictionary", ns.typeName()),
          ctx);
    }
    if (bases.orig != null) {
      ((IPyDict) ns).setItem(Constants.__ORIG_BASES__, bases.orig, ctx);
    }
    return (IPyDict) ns;
  }
}
