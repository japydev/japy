package org.japy.kernel.types.cls.builtin;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.IPyMetaClass;
import org.japy.kernel.types.cls.PyTypeClass;
import org.japy.kernel.util.Args;

public interface IPyBuiltinClass extends IPyClass {
  IPyObj instantiate(Args args, PyContext ctx);

  Class<? extends IPyObj> instanceClass();

  boolean isSubclassable();

  @Override
  default IPyMetaClass type() {
    return PyTypeClass.INSTANCE;
  }
}
