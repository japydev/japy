package org.japy.kernel.types.cls.ext.adapters;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Adapter;
import org.japy.kernel.types.annotations.AdapterMethod;
import org.japy.kernel.types.annotations.ProtocolSuperMethod;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.ext.IPyBuiltinSubClassInstance;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.proto.IPyObjWithTruth;
import org.japy.kernel.util.Constants;

@Adapter(IPyObjWithTruth.class)
public interface IPyObjWithTruthAdapter extends IPyBuiltinSubClassInstance {
  boolean baseIsTrue(PyContext ctx);

  @AdapterMethod
  default boolean thisIsTrue(PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.BOOL);
    if (h == DELETED_METHOD) {
      throw PxException.typeError(
          String.format("objects of type '%s' do not support truth testing", typeName()), ctx);
    }
    if (h == null) {
      return baseIsTrue(ctx);
    }
    try {
      return (boolean) h.handle.invokeExact((IPyObj) this, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @ProtocolSuperMethod(Constants.__BOOL__)
  default IPyObj superIsTrue(PyContext ctx) {
    return PyBool.of(baseIsTrue(ctx));
  }
}
