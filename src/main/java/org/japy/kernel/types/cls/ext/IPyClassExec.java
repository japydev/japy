package org.japy.kernel.types.cls.ext;

import org.japy.kernel.exec.Cell;
import org.japy.kernel.exec.IFrame;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;

public interface IPyClassExec extends IFrame {
  void exec(IPyDict dict, IPyDict globals, IPyRODict builtins, Cell classCell, PyContext ctx);
}
