package org.japy.kernel.types.cls.ext.adapters;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Adapter;
import org.japy.kernel.types.annotations.AdapterMethod;
import org.japy.kernel.types.annotations.ProtocolSuperMethod;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.ext.IPyBuiltinSubClassInstance;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.util.Constants;

@Adapter(IPyIntHashableObj.class)
public interface IPyIntHashableObjAdapter extends IPyBuiltinSubClassInstance {
  int baseIntHashCode(PyContext ctx);

  @AdapterMethod
  default int thisIntHashCode(PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.HASH);
    if (h == DELETED_METHOD) {
      throw ObjLib.throwUnhashable(this, ctx);
    }
    if (h == null) {
      return baseIntHashCode(ctx);
    }
    try {
      return (int) h.handle.invokeExact((IPyObj) this, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @ProtocolSuperMethod(Constants.__HASH__)
  default IPyObj superHash(PyContext ctx) {
    return PyInt.get(baseIntHashCode(ctx));
  }
}
