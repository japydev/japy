package org.japy.kernel.types.cls;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.coll.CollUtil;
import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.cls.builtin.IPyBuiltinClass;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

public class ClassLib {
  public static void finishClassSetup(IPyClass cls, Args classKwArg, PyContext ctx) {
    for (IPyClass base : cls.bases()) {
      base.addSubclass(cls);
    }

    IPyClass[] mro = cls.mro();
    if (mro.length != 1 && !(mro[1] instanceof IPyBuiltinClass)) {
      FuncLib.callMeth(new PySuper(cls, cls, ctx), Constants.__INIT_SUBCLASS__, classKwArg, ctx);
    }
  }

  public static IPyClass[] getBaseMro(IPyClass[] bases, @Nullable PyContext ctx) {
    if (bases.length == 0) {
      return new IPyClass[] {PyObjectClass.INSTANCE};
    } else if (bases.length == 1) {
      return bases[0].mro();
    } else {
      return new C3MroResolver(bases, ctx).resolve();
    }
  }

  public static SpecialMethodTable addMethodsFromMro(
      SpecialMethodTable methodTable, IPyClass[] bases, IPyClass[] baseMro) {
    SpecialMethodTable.Builder builder = methodTable.copy();
    if (bases.length == 0) {
      copyMethodsFromBase(builder, baseMro[0]);
    } else if (bases.length == 1) {
      copyMethodsFromBase(builder, bases[0]);
    } else {
      if (baseMro.length == 1 || Arrays.equals(baseMro[0].mro(), baseMro)) {
        copyMethodsFromBase(builder, baseMro[0]);
      } else {
        copyMethodsFromMro(builder, baseMro);
      }
    }
    return builder.build();
  }

  private static void copyMethodsFromBase(SpecialMethodTable.Builder builder, IPyClass base) {
    SpecialMethodTable parent = base.methodTable();
    for (SpecialMethod m : SpecialMethod.values()) {
      if (builder.get(m) == null) {
        builder.set(m, parent.getRaw(m));
      }
    }
  }

  private static @Nullable FuncHandle getMethodFromMro(SpecialMethod m, IPyClass[] baseMro) {
    for (IPyClass cls : baseMro) {
      @Nullable FuncHandle h = cls.originalMethodTable().getRaw(m);
      if (h != null) {
        return h;
      }
    }
    return null;
  }

  private static void copyMethodsFromMro(SpecialMethodTable.Builder builder, IPyClass[] baseMro) {
    for (SpecialMethod m : SpecialMethod.values()) {
      if (builder.get(m) == null) {
        builder.set(m, getMethodFromMro(m, baseMro));
      }
    }
  }

  /** https://www.python.org/download/releases/2.3/mro/ */
  private static class C3MroResolver {
    private final IPyClass[] bases;
    private final @Nullable PyContext ctx;

    public C3MroResolver(IPyClass[] bases, @Nullable PyContext ctx) {
      this.bases = bases;
      this.ctx = ctx;
    }

    public IPyClass[] resolve() {
      // L[C(B1 ... BN)] = C + merge(L[B1] ... L[BN], B1 ... BN)
      IPyClass[][] lists = new IPyClass[bases.length + 1][];
      for (int i = 0; i != bases.length; ++i) {
        lists[i] = bases[i].mro();
      }
      lists[bases.length] = bases;
      return merge(lists);
    }

    private IPyClass[] merge(@Var IPyClass[][] lists) {
      // -- take the head of the first list, i.e L[B1][0]; if this head is not in the tail of any
      // of
      // -- the other lists, then add it to the linearization of C and remove it from the lists in
      // -- the merge, otherwise look at the head of the next list and take it, if it is a good
      // -- head. Then repeat the operation until all the class are removed or it is impossible to
      // -- find good heads. In this case, it is impossible to construct the merge, Python 2.3
      // will
      // -- refuse to create the class C and will raise an exception.
      List<IPyClass> merged = new ArrayList<>();
      while (lists.length != 0) {
        if (lists.length == 1) {
          merged.addAll(Arrays.asList(lists[0]));
          break;
        }

        IPyClass head = findGoodHead(lists);
        lists = removeHead(head, lists);
        merged.add(head);
      }
      return merged.toArray(IPyClass[]::new);
    }

    private static IPyClass[][] removeHead(IPyClass head, IPyClass[][] lists) {
      List<IPyClass[]> newLists = new ArrayList<>(lists.length);
      for (IPyClass[] list : lists) {
        if (list[0] != head) {
          newLists.add(list);
        } else if (list.length != 1) {
          newLists.add(CollUtil.tail(list));
        }
      }
      return newLists.toArray(IPyClass[][]::new);
    }

    private IPyClass findGoodHead(IPyClass[][] lists) {
      for (int i = 0; i != lists.length; ++i) {
        IPyClass head = lists[i][0];
        if (isGoodHead(head, lists, i)) {
          return head;
        }
      }

      if (ctx != null) {
        throw PxException.typeError(
            "cannot determine method resolution order for bases "
                + Arrays.stream(bases).map(IPyClass::name).collect(Collectors.joining(", ")),
            ctx);
      } else {
        throw new InternalErrorException(
            "cannot determine method resolution order for bases "
                + Arrays.stream(bases).map(IPyClass::name).collect(Collectors.joining(", ")));
      }
    }

    private static boolean isGoodHead(IPyClass cls, IPyClass[][] lists, int skip) {
      for (int i = 0; i != lists.length; ++i) {
        if (i != skip && isInTail(cls, lists[i])) {
          return false;
        }
      }
      return true;
    }

    private static boolean isInTail(IPyClass cls, IPyClass[] list) {
      for (int i = 1; i < list.length; ++i) {
        if (cls == list[i]) {
          return true;
        }
      }
      return false;
    }
  }
}
