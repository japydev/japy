package org.japy.kernel.types.cls;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Validator.check;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.ISupportsValidation;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.util.Args;

public class SpecialMethodTable implements ISupportsValidation {
  public static final FuncHandle DELETED_METHOD;

  public static final MethodType TYPE_GENERIC_INSTANCE_METHOD =
      MethodType.methodType(IPyObj.class, IPyObj.class, Args.class, PyContext.class);
  public static final MethodType TYPE_GENERIC_FUNC =
      MethodType.methodType(IPyObj.class, Args.class, PyContext.class);
  public static final MethodType TYPE_OBJ_OBJ1_CTX =
      MethodType.methodType(IPyObj.class, IPyObj.class, PyContext.class);
  public static final MethodType TYPE_OBJ_OBJ2_CTX =
      MethodType.methodType(IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class);
  public static final MethodType TYPE_OBJ_OBJ3_CTX =
      MethodType.methodType(
          IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class);
  public static final MethodType TYPE_VOID_OBJ2_CTX =
      MethodType.methodType(void.class, IPyObj.class, IPyObj.class, PyContext.class);
  public static final MethodType TYPE_VOID_OBJ3_CTX =
      MethodType.methodType(void.class, IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class);
  public static final MethodType TYPE_BOOL_OBJ1_CTX =
      MethodType.methodType(boolean.class, IPyObj.class, PyContext.class);
  public static final MethodType TYPE_BOOL_OBJ2_CTX =
      MethodType.methodType(boolean.class, IPyObj.class, IPyObj.class, PyContext.class);
  public static final MethodType TYPE_INT_OBJ1_CTX =
      MethodType.methodType(int.class, IPyObj.class, PyContext.class);

  private final @Nullable FuncHandle[] methods;

  private SpecialMethodTable(FuncHandle[] methods) {
    this.methods = methods;
  }

  public @Nullable FuncHandle getIfPresent(SpecialMethod m) {
    FuncHandle h = methods[m.ordinal()];
    return h == DELETED_METHOD ? null : h;
  }

  public @Nullable FuncHandle getRaw(SpecialMethod m) {
    return methods[m.ordinal()];
  }

  public Builder copy() {
    return new Builder(this);
  }

  public static Builder builder() {
    return new Builder();
  }

  public SpecialMethodTable set(SpecialMethod specialMethod, @Nullable FuncHandle methodHandle) {
    return copy().set(specialMethod, methodHandle).build();
  }

  public static class Builder {
    private final @Nullable FuncHandle[] methods = new FuncHandle[SpecialMethod.values().length];

    private Builder() {}

    private Builder(SpecialMethodTable table) {
      System.arraycopy(table.methods, 0, methods, 0, methods.length);
    }

    public SpecialMethodTable build() {
      return new SpecialMethodTable(methods);
    }

    public @Nullable FuncHandle get(SpecialMethod m) {
      return methods[m.ordinal()];
    }

    @CanIgnoreReturnValue
    public Builder set(SpecialMethod m, @Nullable FuncHandle h) {
      if (Debug.ENABLED) {
        dcheck(h == null || h == DELETED_METHOD || h.type().equals(m.type));
      }
      methods[m.ordinal()] = h;
      return this;
    }
  }

  @Override
  public void validate(Validator v) {
    for (SpecialMethod m : SpecialMethod.values()) {
      @Nullable FuncHandle h = getRaw(m);
      check(h == null || h == DELETED_METHOD || h.type().equals(m.type), "bad method handle type");
    }
  }

  static {
    try {
      DELETED_METHOD =
          new FuncHandle(
              MethodHandles.lookup()
                  .findStatic(SpecialMethodTable.class, "dummy", MethodType.methodType(void.class)),
              SpecialMethodTable.class);
    } catch (NoSuchMethodException | IllegalAccessException ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @SuppressWarnings("unused")
  private static void dummy() {}
}
