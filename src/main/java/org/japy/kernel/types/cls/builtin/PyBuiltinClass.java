package org.japy.kernel.types.cls.builtin;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.PyClassDict;
import org.japy.kernel.types.cls.PyObjectClass;
import org.japy.kernel.types.cls.SpecialMethodTable;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.FuncAdapterLib;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.ReflectionUtil;

public class PyBuiltinClass extends PyAbstractBuiltinClass {
  public static final int SUBCLASSABLE = 1;
  public static final int CALLABLE = 2;
  public static final int INTERNAL = 0;
  public static final int REGULAR = SUBCLASSABLE | CALLABLE;

  private final PyClassDict dict = new PyClassDict();
  private final SpecialMethodTable originalMethodTable;
  private final SpecialMethodTable methodTable;
  private final @Nullable FuncHandle constructor;
  private final boolean constructorTakesClass;
  private final Class<? extends IPyObj> instanceClass;
  private final boolean isSubclassable;
  private final @Nullable String doc;

  public PyBuiltinClass(String name, Class<? extends IPyObj> instanceClass, int flags) {
    this(name, PyObjectClass.INSTANCE, instanceClass, MethodHandles.lookup(), true, flags, null);
  }

  public PyBuiltinClass(
      String name, Class<? extends IPyObj> instanceClass, int flags, @Nullable String doc) {
    this(name, PyObjectClass.INSTANCE, instanceClass, MethodHandles.lookup(), true, flags, doc);
  }

  public PyBuiltinClass(
      String name, Class<? extends IPyObj> instanceClass, MethodHandles.Lookup lookup, int flags) {
    this(name, PyObjectClass.INSTANCE, instanceClass, lookup, true, flags, null);
  }

  public PyBuiltinClass(
      String name,
      Class<? extends IPyObj> instanceClass,
      MethodHandles.Lookup lookup,
      int flags,
      @Nullable String doc) {
    this(name, PyObjectClass.INSTANCE, instanceClass, lookup, true, flags, doc);
  }

  public PyBuiltinClass(
      String name, PyBuiltinClass parent, Class<? extends IPyObj> instanceClass, int flags) {
    this(name, parent, instanceClass, MethodHandles.lookup(), false, flags, null);
  }

  public PyBuiltinClass(
      String name,
      PyBuiltinClass parent,
      Class<? extends IPyObj> instanceClass,
      int flags,
      @Nullable String doc) {
    this(name, parent, instanceClass, MethodHandles.lookup(), false, flags, doc);
  }

  public PyBuiltinClass(
      String name,
      PyBuiltinClass parent,
      Class<? extends IPyObj> instanceClass,
      MethodHandles.Lookup lookup,
      int flags) {
    this(name, parent, instanceClass, lookup, false, flags, null);
  }

  public PyBuiltinClass(
      String name,
      PyBuiltinClass parent,
      Class<? extends IPyObj> instanceClass,
      MethodHandles.Lookup lookup,
      int flags,
      @Nullable String doc) {
    this(name, parent, instanceClass, lookup, false, flags, doc);
  }

  private PyBuiltinClass(
      String name,
      IPyClass parent,
      Class<? extends IPyObj> instanceClass,
      MethodHandles.Lookup lookup,
      boolean inspectSupers,
      int flags) {
    this(name, parent, instanceClass, lookup, inspectSupers, flags, null);
  }

  private PyBuiltinClass(
      String name,
      IPyClass parent,
      Class<? extends IPyObj> instanceClass,
      MethodHandles.Lookup lookup,
      boolean inspectSupers,
      int flags,
      @Nullable String doc) {
    super(name, parent);

    if (Debug.ENABLED) {
      //noinspection ConstantConditions
      dcheck(parent != null);
      dcheck(parent == PyObjectClass.INSTANCE || parent instanceof PyBuiltinClass);
    }

    this.doc = doc;
    this.instanceClass = instanceClass;

    if ((flags & CALLABLE) != 0) {
      ConstructorInfo ci = getConstructor(instanceClass, lookup, name);
      constructor = ci.funcHandle;
      constructorTakesClass = ci.takesClass;
    } else {
      constructor = null;
      constructorTakesClass = false;
    }

    isSubclassable = (flags & SUBCLASSABLE) != 0;
    if (isSubclassable && Debug.ENABLED) {
      dcheckNotNull(getSubClassConstructor(instanceClass));
    }

    BuiltinClassLib.ClassData classData =
        BuiltinClassLib.initializeBuiltin(
            this,
            instanceClass,
            lookup,
            (inspectSupers ? BuiltinClassLib.INSPECT_SUPERS : 0)
                | (isSubclassable ? BuiltinClassLib.ADD_NOOP_INIT : 0));
    originalMethodTable = classData.original;
    methodTable = classData.complete;
    BuiltinClassLib.finishClassSetup(this);
  }

  @Override
  protected IPyObj getDoc() {
    return doc != null ? PyStr.get(doc) : None;
  }

  private static class ConstructorInfo {
    final FuncHandle funcHandle;
    final boolean takesClass;

    private ConstructorInfo(FuncHandle funcHandle, boolean takesClass) {
      this.funcHandle = funcHandle;
      this.takesClass = takesClass;
    }
  }

  private static ConstructorInfo getConstructor(
      Class<?> instanceClass, MethodHandles.Lookup lookup, String className) {
    @Nullable
    Constructor<?> constructor =
        ReflectionUtil.findAnnotatedConstructor(
            instanceClass, org.japy.kernel.types.annotations.Constructor.class);
    @Nullable
    Method constructorMethod =
        constructor == null
            ? ReflectionUtil.findAnnotatedMethod(
                instanceClass, org.japy.kernel.types.annotations.Constructor.class)
            : null;

    if (constructor == null && constructorMethod == null) {
      throw new InternalErrorException(
          String.format(
              "constructable type '%s' (%s), but no @Constructor-annotated constructor or method is present",
              className, instanceClass));
    }

    if (constructorMethod != null && !Modifier.isStatic(constructorMethod.getModifiers())) {
      throw new InternalErrorException(
          String.format(
              "type '%s' (%s): @Constructor-annotated method is not static",
              className, instanceClass));
    }

    boolean takesClass =
        constructorMethod != null
            && IPyClass.class.isAssignableFrom(constructorMethod.getParameterTypes()[0]);
    if (takesClass && Debug.ENABLED) {
      Class<?>[] ptypes = constructorMethod.getParameterTypes();
      if (ptypes.length != 3
          || ptypes[0] != IPyClass.class
          || ptypes[1] != Args.class
          || ptypes[2] != PyContext.class) {
        throw new InternalErrorException(
            String.format(
                "%s.%s: must be IPyObj(IPyClass,Args,PyContext)",
                constructorMethod.getDeclaringClass(), constructorMethod.getName()));
      }
    }

    FuncHandle h =
        constructor != null
            ? FuncHandle.constructor(constructor, lookup)
            : FuncHandle.method(constructorMethod, lookup);

    return new ConstructorInfo(
        takesClass
            ? h
            : FuncAdapterLib.makeBuiltinFunc(
                h, constructor != null ? constructor : constructorMethod, className),
        takesClass);
  }

  private static final MethodType SUBCLASS_CONSTRUCTOR_TYPE1 =
      MethodType.methodType(IPyObj.class, Args.class, PyContext.class);
  private static final MethodType SUBCLASS_CONSTRUCTOR_TYPE2 =
      MethodType.methodType(IPyObj.class, IPyClass.class, Args.class, PyContext.class);

  public static Constructor<?> getSubClassConstructor(Class<?> instanceClass) {
    @Nullable
    Constructor<?> constructor =
        ReflectionUtil.findAnnotatedConstructor(instanceClass, SubClassConstructor.class);

    if (constructor == null) {
      throw new InternalErrorException(
          String.format("no @SubClassConstructor-annotated constructor in %s", instanceClass));
    }

    int mod = constructor.getModifiers();
    if (!Modifier.isPublic(mod) && !Modifier.isProtected(mod)) {
      throw new InternalErrorException(
          String.format(
              "@SubClassConstructor-annotated constructor is not public or protected in %s",
              instanceClass));
    }

    MethodType methodType = MethodType.methodType(IPyObj.class, constructor.getParameterTypes());
    if (!methodType.equals(SUBCLASS_CONSTRUCTOR_TYPE1)
        && !methodType.equals(SUBCLASS_CONSTRUCTOR_TYPE2)) {
      throw new InternalErrorException(
          String.format(
              "bad constructor type %s in class %s, expected %s or %s",
              methodType, instanceClass, SUBCLASS_CONSTRUCTOR_TYPE1, SUBCLASS_CONSTRUCTOR_TYPE2));
    }

    return constructor;
  }

  @Override
  public SpecialMethodTable methodTable() {
    return methodTable;
  }

  @Override
  public SpecialMethodTable originalMethodTable() {
    return originalMethodTable;
  }

  @Override
  public IPyObj instantiate(Args args, PyContext ctx) {
    if (constructor == null) {
      throw PxException.typeError(
          String.format("cannot create instances of class '%s'", name()), ctx);
    }

    try {
      if (constructorTakesClass) {
        return (IPyObj) constructor.handle.invokeExact((IPyClass) this, args, ctx);
      } else {
        return (IPyObj) constructor.handle.invokeExact(args, ctx);
      }
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public Class<? extends IPyObj> instanceClass() {
    return instanceClass;
  }

  @Override
  public boolean isSubclassable() {
    return isSubclassable;
  }

  @Override
  public void validate(Validator v) {
    v.validate(dict);
  }
}
