package org.japy.kernel.types.cls.desc;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;

public interface IPyMethodDescriptor extends IPyDescriptor {
  @InstanceAttribute
  IPyObj __isabstractmethod__(PyContext ctx);
}
