package org.japy.kernel.types.cls.ext;

import org.japy.infra.validation.Validator;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.obj.IPyInstanceDict;
import org.japy.kernel.types.obj.PyInstanceDict;
import org.japy.kernel.types.obj.proto.IPyDefaultHashableObj;

final class PyPureExtObj implements IPyDefaultHashableObj {
  private final PyPureExtClass type;
  private final PyInstanceDict instanceDict = new PyInstanceDict();

  public PyPureExtObj(PyPureExtClass type) {
    this.type = type;
  }

  @Override
  public IPyClass type() {
    return type;
  }

  @Override
  public IPyInstanceDict instanceDict() {
    return instanceDict;
  }

  @Override
  public void validate(Validator v) {
    v.validate(instanceDict);
  }
}
