package org.japy.kernel.types.cls.ext.adapters;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Adapter;
import org.japy.kernel.types.annotations.AdapterMethod;
import org.japy.kernel.types.annotations.ProtocolSuperMethod;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.ext.IPyBuiltinSubClassInstance;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.iter.IPyIterable;
import org.japy.kernel.types.coll.iter.IterLib;
import org.japy.kernel.util.Constants;

@Adapter(IPyIterable.class)
public interface IPyIterableAdapter extends IPyBuiltinSubClassInstance {
  @ProtocolSuperMethod(Constants.__ITER__)
  IPyIter baseIter(PyContext ctx);

  @AdapterMethod
  default IPyIter thisIter(PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.ITER);
    if (h == DELETED_METHOD) {
      throw IterLib.throwIterUnsupported(this, ctx);
    }
    if (h == null) {
      return baseIter(ctx);
    }
    IPyObj iter;
    try {
      iter = (IPyObj) h.handle.invokeExact((IPyObj) this, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
    return IterLib.ensureIter(iter);
  }
}
