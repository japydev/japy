package org.japy.kernel.types.cls.ext.adapters;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Adapter;
import org.japy.kernel.types.annotations.AdapterMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.ProtocolSuperMethod;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.ext.IPyBuiltinSubClassInstance;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.IPyContains;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.util.Constants;

@Adapter(IPyContains.class)
public interface IPyContainsAdapter extends IPyBuiltinSubClassInstance {
  boolean baseContains(IPyObj obj, PyContext ctx);

  @AdapterMethod
  default boolean thisContains(IPyObj obj, PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.CONTAINS);
    if (h == DELETED_METHOD) {
      throw CollLib.throwContainsUnsupported(this, ctx);
    }
    if (h == null) {
      return baseContains(obj, ctx);
    }
    try {
      return (boolean) h.handle.invokeExact((IPyObj) this, obj, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @ProtocolSuperMethod(Constants.__CONTAINS__)
  default IPyObj superContains(@Param("object") IPyObj obj, PyContext ctx) {
    return PyBool.of(baseContains(obj, ctx));
  }
}
