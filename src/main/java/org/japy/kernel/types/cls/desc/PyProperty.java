package org.japy.kernel.types.cls.desc;

import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.util.PyUtil.wrap;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyProperty extends PyAbstractDescriptor
    implements IPyTrueAtomObj, IPyDescriptorGet, IPyDescriptorDel, IPyDescriptorSet {
  public static final IPyClass TYPE =
      new PyBuiltinClass("property", PyProperty.class, PyBuiltinClass.REGULAR);

  @InstanceAttribute public final IPyObj fget;
  @InstanceAttribute public final IPyObj fset;
  @InstanceAttribute public final IPyObj fdel;

  @InstanceAttribute(value = "__doc__", writable = true)
  public IPyObj doc;

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(4)
          .add("fget", None)
          .add("fset", None)
          .add("fdel", None)
          .add("doc", None)
          .build();

  public PyProperty(IPyObj fget, IPyObj fset, IPyObj fdel, IPyObj doc) {
    this.fget = fget;
    this.fset = fset;
    this.fdel = fdel;
    this.doc = doc;
  }

  @Constructor
  @SubClassConstructor
  public PyProperty(Args args, PyContext ctx) {
    IPyObj[] argVals = ARG_PARSER.parse(args, "property", ctx);
    this.fget = argVals[0];
    this.fset = argVals[1];
    this.fdel = argVals[2];
    this.doc = argVals[3];
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @InstanceMethod
  public IPyObj setter(@Param("fset") IPyObj fset, PyContext ctx) {
    return new PyProperty(fget, fset, fdel, doc);
  }

  @InstanceMethod
  public IPyObj getter(@Param("fget") IPyObj fget, PyContext ctx) {
    return new PyProperty(fget, fset, fdel, doc);
  }

  @InstanceMethod
  public IPyObj deleter(@Param("fdel") IPyObj fdel, PyContext ctx) {
    return new PyProperty(fget, fset, fdel, doc);
  }

  @InstanceAttribute
  public IPyObj __isabstractmethod__(PyContext ctx) {
    return wrap(
        (fget != None && ObjLib.isAbstractMethod(fget, ctx))
            || (fset != None && ObjLib.isAbstractMethod(fset, ctx))
            || (fdel != None && ObjLib.isAbstractMethod(fdel, ctx)));
  }

  @Override
  public void delete(IPyObj instance, PyContext ctx) {
    if (fdel == None) {
      if (fset == None) {
        throw new PxAttributeError(
            String.format("'%s' object attribute '%s' is read-only", objClass().name(), "<fixme>"),
            ctx);
      } else {
        throw new PxAttributeError(
            String.format(
                "'%s' object attribute '%s' cannot be deleted", objClass().name(), "<fixme>"),
            ctx);
      }
    }

    FuncLib.call(fdel, instance, ctx);
  }

  @Override
  public IPyObj get(IPyObj instance, IPyObj owner, PyContext ctx) {
    if (instance == None) {
      return this;
    }

    if (fget == None) {
      throw new PxAttributeError(
          String.format("'%s' object attribute '%s' is write-only", objClass().name(), "<fixme>"),
          ctx);
    }

    return FuncLib.call(fget, instance, ctx);
  }

  @Override
  public void set(IPyObj instance, IPyObj value, PyContext ctx) {
    if (fset == None) {
      throw new PxAttributeError(
          String.format("'%s' object attribute '%s' is read-only", objClass().name(), "<fixme>"),
          ctx);
    }

    FuncLib.call(fset, instance, value, ctx);
  }

  @Override
  public void validate(Validator v) {
    super.validate(v);
    v.validate(fget);
    v.validate(fset);
    v.validate(fdel);
    v.validate(doc);
  }
}
