package org.japy.kernel.types.cls.desc;

import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;

public class PyInstanceMethod extends PyMethodDescriptor {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "instancemethod",
          PyInstanceMethod.class,
          MethodHandles.lookup(),
          PyBuiltinClass.INTERNAL);

  public PyInstanceMethod(IPyObj func) {
    super(func);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj get(IPyObj instance, IPyObj owner, PyContext ctx) {
    if (instance != None) {
      return new PyBoundMethod(instance, func, dcheckNotNull(name).toString());
    }
    return this;
  }
}
