package org.japy.kernel.types.cls;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.obj.IPyInstanceDict;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

public interface IPyClass extends IPyTrueAtomObj {
  IPyClass[] EMPTY_CLASS_ARRAY = new IPyClass[0];

  String name();

  String qualName();

  PyStr module();

  IPyObj doc();

  IPyObj annotations();

  void setName(PyStr name, PyContext ctx);

  void setQualName(PyStr qualName, PyContext ctx);

  void setModule(PyStr module, PyContext ctx);

  void setDoc(IPyObj doc, PyContext ctx);

  void setAnnotations(IPyObj annotations, PyContext ctx);

  void setAttr(String attr, IPyObj value, PyContext ctx);

  void delAttr(String attr, PyContext ctx);

  @Override
  IPyMetaClass type();

  boolean isBuiltinClass();

  /**
   * Table of methods set (or deleted) explicitly in this class, without taking MRO into account.
   */
  SpecialMethodTable originalMethodTable();

  /** Effective method table, populated from the original table and classes from MRO. */
  SpecialMethodTable methodTable();

  IPyClassDict classDict();

  IPyRODict classDictWrapper();

  default IPyClassDict superDict(IPyClass base) {
    return base.classDict();
  }

  @Override
  default IPyInstanceDict instanceDict() {
    return classDict();
  }

  /**
   * List of the method resolution order (MRO) classes. The first element of the list is this class.
   */
  IPyClass[] mro();

  IPyClass[] bases();

  default boolean isSubClassRaw(IPyClass parent) {
    if (this == parent) {
      return true;
    }
    for (IPyClass base : mro()) {
      if (base == parent) {
        return true;
      }
    }
    return false;
  }

  default boolean isStrictSubClassRaw(IPyClass parent) {
    if (this == parent) {
      return false;
    }
    for (IPyClass base : mro()) {
      if (base == parent) {
        return true;
      }
    }
    return false;
  }

  PyList subclasses(PyContext ctx);

  void addSubclass(IPyClass subclass);
}
