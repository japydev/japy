package org.japy.kernel.types.cls;

import static org.japy.infra.validation.Debug.dcheck;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

public class PySuper implements IPyTrueAtomObj {
  public static final IPyClass TYPE =
      new PyBuiltinClass("super", PySuper.class, PyBuiltinClass.CALLABLE);

  private final IPyClass targetType;
  private final @Nullable IPyObj targetObj;
  private final IPyClass[] mro;
  private final int start;

  @Constructor
  public PySuper(
      @Param("type") IPyObj typeObj, @Param(value = "obj-or-type") IPyObj obj, PyContext ctx) {
    IPyClass type = ArgLib.checkArgClass("super", "type", typeObj, ctx);
    if (type == PyObjectClass.INSTANCE) {
      throw PxException.typeError("type may not be 'object'", ctx);
    }

    if (obj.isInstanceRaw(type)) {
      mro = obj.type().mro();
      targetObj = obj;
      targetType = obj.type();
    } else if ((obj instanceof IPyClass) && ((IPyClass) obj).isSubClassRaw(type)) {
      mro = ((IPyClass) obj).mro();
      targetType = (IPyClass) obj;
      targetObj = null;
    } else {
      throw PxException.typeError("obj-or-type must be an instance or a subclass of type", ctx);
    }

    if (mro.length == 1) {
      throw PxException.typeError("obj-or-type may not be 'object'", ctx);
    }

    @Var int startFrom = mro.length;
    for (int i = 0; i != mro.length; ++i) {
      if (mro[i] == type) {
        startFrom = i + 1;
        break;
      }
    }
    dcheck(startFrom < mro.length);
    start = startFrom;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @SpecialInstanceMethod
  public IPyObj __getattribute__(@Param("attr") IPyObj attr, PyContext ctx) {
    for (int i = start; i != mro.length; ++i) {
      @Nullable IPyObj value = targetType.superDict(mro[i]).getOrNull(attr, ctx);
      if (value != null) {
        return ObjLib.getAttributeValue(value, targetType, targetObj, ctx);
      }
    }
    throw new PxAttributeError((PyStr) attr, ctx);
  }

  @SpecialInstanceMethod
  public IPyObj __getattr__(@Param("attr") IPyObj attr, PyContext ctx) {
    throw new PxAttributeError((PyStr) attr, ctx);
  }

  @Override
  public void validate(Validator v) {
    v.validate(targetType);
    v.validateIfNotNull(targetObj);
    v.validate(mro);
  }
}
