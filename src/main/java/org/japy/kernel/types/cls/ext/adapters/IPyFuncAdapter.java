package org.japy.kernel.types.cls.ext.adapters;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Adapter;
import org.japy.kernel.types.annotations.AdapterMethod;
import org.japy.kernel.types.annotations.ProtocolSuperMethod;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.ext.IPyBuiltinSubClassInstance;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.func.IPyFunc;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

@Adapter(IPyFunc.class)
public interface IPyFuncAdapter extends IPyBuiltinSubClassInstance {
  @ProtocolSuperMethod(Constants.__CALL__)
  IPyObj baseCall(Args args, PyContext ctx);

  @AdapterMethod
  default IPyObj thisCall(Args args, PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.CALL);
    if (h == DELETED_METHOD) {
      throw CollLib.throwLenUnsupported(this, ctx);
    }
    if (h == null) {
      return baseCall(args, ctx);
    }
    try {
      return (IPyObj) h.handle.invokeExact((IPyObj) this, args, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }
}
