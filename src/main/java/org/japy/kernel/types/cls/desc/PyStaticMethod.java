package org.japy.kernel.types.cls.desc;

import org.japy.base.ParamKind;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyStaticMethod extends PyMethodDescriptor {
  public PyStaticMethod(IPyObj func) {
    super(func);
  }

  @Constructor
  public PyStaticMethod(@Param("func") IPyObj func, PyContext ctx) {
    this(func);
  }

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(1).add("func", ParamKind.POS_ONLY).build();

  @Constructor
  @SubClassConstructor
  public PyStaticMethod(Args args, PyContext ctx) {
    this(ARG_PARSER.parse1(args, "staticmethod", ctx));
  }

  private static class TypeHolder {
    public static final IPyClass TYPE =
        new PyBuiltinClass("staticmethod", PyStaticMethod.class, PyBuiltinClass.REGULAR);
  }

  public static IPyClass TYPE() {
    return TypeHolder.TYPE;
  }

  @Override
  public IPyClass type() {
    return TypeHolder.TYPE;
  }

  @Override
  public void validate(Validator v) {
    v.validate(func);
  }

  @Override
  public IPyObj get(IPyObj instance, IPyObj owner, PyContext ctx) {
    return func;
  }
}
