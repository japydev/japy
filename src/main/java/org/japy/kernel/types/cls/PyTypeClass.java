package org.japy.kernel.types.cls;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.ClassMethod;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.annotations.SpecialStaticMethod;
import org.japy.kernel.types.cls.builtin.BuiltinClassLib;
import org.japy.kernel.types.cls.builtin.IPyBuiltinClass;
import org.japy.kernel.types.cls.builtin.PyAbstractBuiltinClass;
import org.japy.kernel.types.cls.ext.ExtClassData;
import org.japy.kernel.types.cls.ext.ExtClassLib;
import org.japy.kernel.types.cls.ext.IPyExtClass;
import org.japy.kernel.types.cls.ext.PyExtBuiltinSubClass;
import org.japy.kernel.types.cls.ext.PyExtMetaClass;
import org.japy.kernel.types.cls.ext.PyPureExtClass;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.PyObject;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

/** {@code type} class, the root metaclass. */
public final class PyTypeClass extends PyAbstractBuiltinClass implements IPyMetaClass {
  public static final PyTypeClass INSTANCE = new PyTypeClass();

  private final SpecialMethodTable originalMethodTable;
  private final SpecialMethodTable methodTable;

  private PyTypeClass() {
    super("type", PyObjectClass.INSTANCE);
    BuiltinClassLib.ClassData classData =
        BuiltinClassLib.initializeBuiltin(this, MethodHandles.lookup());
    originalMethodTable = classData.original;
    methodTable = classData.complete;
    BuiltinClassLib.finishClassSetup(this);
  }

  @Override
  public SpecialMethodTable methodTable() {
    return methodTable;
  }

  @Override
  public void setAttr(String attr, IPyObj value, PyContext ctx) {
    throw PxAttributeError.readOnlyAttribute(attr, this, ctx);
  }

  @Override
  public void delAttr(String attr, PyContext ctx) {
    throw PxAttributeError.nonDeletableAttribute(attr, this, ctx);
  }

  @Override
  public SpecialMethodTable originalMethodTable() {
    return originalMethodTable;
  }

  @Override
  protected IPyObj getDoc() {
    return PyStr.ucs2(
        "type(object) -> the object's type\n" + "type(name, bases, dict, **kwds) -> a new type");
  }

  @Override
  public IPyObj instantiate(Args args, PyContext ctx) {
    throw PxException.typeError("object.__new__(type) is not safe, use type.__new__()", ctx);
  }

  @Override
  public Class<? extends IPyObj> instanceClass() {
    throw InternalErrorException.notReached();
  }

  @Override
  public boolean isSubclassable() {
    throw InternalErrorException.notReached();
  }

  @InstanceAttribute
  public static IPyObj __mro__(IPyObj self, PyContext ctx) {
    return new PyTuple(((IPyClass) self).mro());
  }

  @InstanceMethod
  public static IPyObj mro(IPyObj self, PyContext ctx) {
    return new PyList(((IPyClass) self).mro());
  }

  @InstanceMethod
  public static IPyObj __subclasses__(IPyObj self, PyContext ctx) {
    return ((IPyClass) self).subclasses(ctx);
  }

  @ClassMethod
  public static IPyObj __prepare__(
      IPyClass cls,
      @Param("name") IPyObj name,
      @Param("bases") IPyObj bases,
      @Param(value = "kwarg", kind = ParamKind.VAR_KW) IPyObj kwarg,
      PyContext ctx) {
    return new PyClassDict();
  }

  private static IPyObj newInstance(IPyClass cls, Args args, PyContext ctx) {
    @Nullable FuncHandle hNew = cls.methodTable().getIfPresent(SpecialMethod.NEW);
    if (hNew == null) {
      throw PxException.typeError(String.format("cannot instantiate class '%s'", cls.name()), ctx);
    }

    try {
      return (IPyObj) hNew.handle.invokeExact(args.prepend(cls), ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  private static void initInstance(IPyObj instance, Args args, PyContext ctx) {
    @Nullable FuncHandle hInit = instance.specialMethod(SpecialMethod.INIT);
    if (hInit == null) {
      throw PxException.typeError(
          String.format("cannot initialize objects of type '%s'", instance.typeName()), ctx);
    }
    IPyObj initResult;
    try {
      initResult = (IPyObj) hInit.handle.invokeExact(instance, args, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
    if (initResult != None) {
      throw PxException.typeError(
          String.format(
              "%s.__init__ returned %s, expected None",
              instance.typeName(), PrintLib.repr(initResult, ctx)),
          ctx);
    }
  }

  private static IPyObj createInstanceWithInit(IPyClass cls, Args args, PyContext ctx) {
    IPyObj instance = newInstance(cls, args, ctx);

    if (instance.type() == cls) {
      initInstance(instance, args, ctx);
    }

    return instance;
  }

  @SpecialInstanceMethod
  public static IPyObj __call__(IPyObj self, Args args, PyContext ctx) {
    IPyClass cls = ArgLib.checkArgClass("type.__call__", "self", self, ctx);

    if (cls == INSTANCE) {
      // type(obj) -> object type
      if (args.posCount() == 1 && args.kwCount() == 0) {
        return args.getPos(0).type();
      }
    }

    if (cls instanceof IPyMetaClass) {
      return createInstanceWithInit(cls, args, ctx);
    } else if (cls instanceof IPyBuiltinClass) {
      return newInstance(cls, args, ctx);
    } else {
      return createInstanceWithInit(cls, args, ctx);
    }
  }

  private static final ArgParser NEW_PARSER =
      ArgParser.builder(5)
          .add("cls", ParamKind.POS_ONLY)
          .add("name", ParamKind.POS_ONLY)
          .add("bases", ParamKind.POS_ONLY)
          .add("namespace", ParamKind.POS_ONLY)
          .add("kwarg", ParamKind.VAR_KW)
          .build();

  @SpecialStaticMethod
  public static IPyObj __new__(Args args, PyContext ctx) {
    IPyObj[] argVals = NEW_PARSER.parse(args, "type", ctx);

    if (!(argVals[0] instanceof IPyMetaClass)) {
      throw PxException.typeError(
          "expected a metaclass, got " + PrintLib.repr(args.getPos(0), ctx), ctx);
    }
    IPyMetaClass meta = (IPyMetaClass) argVals[0];

    // type(name, bases, namespace)
    PyStr name = ArgLib.checkArgStr("type.__new__", "name", argVals[1], ctx);
    PyTuple basesTup = ArgLib.checkArgTuple("type.__new__", "bases", argVals[2], ctx);
    IPyRODict namespace = ArgLib.checkArgRODict("type.__new__", "namespace", argVals[3], ctx);

    List<IPyClass> bases = new ArrayList<>(basesTup.len());
    basesTup.forEachNoCtx(
        b -> {
          if (!(b instanceof IPyClass)) {
            throw PxException.typeError(
                String.format("class bases must be classes, got %s", b.typeName()), ctx);
          }
          bases.add((IPyClass) b);
        });

    ExtClassData.Builder dataBuilder = ExtClassData.builder().name(name.toString());
    Consumer<IPyClass> postProcess =
        ExtClassLib.prepareClass(dataBuilder, bases.toArray(IPyClass[]::new), namespace, ctx);
    ExtClassData classData = dataBuilder.build();

    IPyExtClass cls;
    if (classData.builtinAncestor == null) {
      cls = new PyPureExtClass(classData, meta);
    } else if (classData.builtinAncestor == INSTANCE) {
      cls = new PyExtMetaClass(classData, meta);
    } else if (!classData.builtinAncestor.isSubclassable()) {
      throw PxException.typeError("cannot subclass class " + classData.builtinAncestor.name(), ctx);
    } else {
      cls = new PyExtBuiltinSubClass(classData, meta, ctx);
    }

    postProcess.accept(cls);

    ExtClassLib.finishClassSetup(cls, args.withoutPos(), ctx);

    return cls;
  }

  @SpecialInstanceMethod
  public static IPyObj __init__(IPyObj self, Args args, PyContext ctx) {
    return None;
  }

  @SpecialInstanceMethod
  public static IPyObj __getattribute__(IPyObj self, @Param("attr") IPyObj attr, PyContext ctx) {
    IPyClass cls = ArgLib.checkArgClass("type.__getattribute__", "self", self, ctx);
    String attrName = ObjLib.attrName(attr, ctx);
    switch (attrName) {
      case Constants.__NAME__:
        return PyStr.get(cls.name());
      case Constants.__QUALNAME__:
        return PyStr.get(cls.qualName());
      case Constants.__DOC__:
        return cls.doc();
      case Constants.__MODULE__:
        return cls.module();
      case Constants.__DICT__:
        return cls.classDictWrapper();
      case Constants.__CLASS__:
        return cls.type();
      case Constants.__BASES__:
        return new PyTuple(cls.bases());
      case Constants.__TEXT_SIGNATURE__:
        throw new NotImplementedException("__text_signature__");
      case Constants.__ANNOTATIONS__:
        return cls.annotations();
      case Constants.__ABSTRACTMETHODS__:
        {
          if (cls instanceof IPyExtClass) {
            @Nullable IPyObj result = ((IPyExtClass) cls).getAbstractMethods(ctx);
            if (result != null) {
              return result;
            }
          }
          throw new PxAttributeError(attrName, ctx);
        }
      default:
        break;
    }

    @Nullable IPyObj value = ObjLib.getClassAttributeOrNull((IPyClass) self, null, attrName, ctx);
    if (value != null) {
      return value;
    }
    return ObjLib.getClassAttribute(self.type(), self, attrName, ctx);
  }

  @SpecialInstanceMethod
  public static IPyObj __setattr__(
      IPyObj self, @Param("attr") IPyObj attr, @Param("value") IPyObj value, PyContext ctx) {
    IPyClass cls = ArgLib.checkArgClass("type.__setattr__", "self", self, ctx);

    String attrName = ObjLib.attrName(attr, ctx);
    switch (attrName) {
      case Constants.__DICT__:
      case Constants.__CLASS__:
        return PyObject.__setattr__(self, attr, value, ctx);
      case Constants.__NAME__:
        cls.setName(ArgLib.checkAttrStr(attrName, value, ctx), ctx);
        break;
      case Constants.__QUALNAME__:
        cls.setQualName(ArgLib.checkAttrStr(attrName, value, ctx), ctx);
        break;
      case Constants.__MODULE__:
        cls.setModule(ArgLib.checkAttrStr(attrName, value, ctx), ctx);
        break;
      case Constants.__DOC__:
        cls.setDoc(value, ctx);
        break;
      case Constants.__ANNOTATIONS__:
        cls.setAnnotations(value, ctx);
        break;
      case Constants.__TEXT_SIGNATURE__:
      case Constants.__BASES__:
        throw PxAttributeError.readOnlyAttribute(attrName, self, ctx);
      case Constants.__ABSTRACTMETHODS__:
        if (cls instanceof IPyExtClass) {
          ((IPyExtClass) cls).setAbstractMethods(value, ctx);
        } else {
          throw PxAttributeError.readOnlyAttribute(attrName, self, ctx);
        }
        break;
      default:
        cls.setAttr(attrName, value, ctx);
        break;
    }

    return None;
  }

  @SpecialInstanceMethod
  public static IPyObj __delattr__(IPyObj self, @Param("attr") IPyObj attr, PyContext ctx) {
    IPyClass cls = ArgLib.checkArgClass("type.__delattr__", "self", self, ctx);

    String attrName = ObjLib.attrName(attr, ctx);
    switch (attrName) {
      case Constants.__DICT__:
      case Constants.__CLASS__:
        return PyObject.__delattr__(self, attr, ctx);
      case Constants.__NAME__:
      case Constants.__QUALNAME__:
      case Constants.__MODULE__:
      case Constants.__DOC__:
      case Constants.__TEXT_SIGNATURE__:
      case Constants.__ANNOTATIONS__:
        throw PxAttributeError.nonDeletableAttribute(attrName, self, ctx);
      case Constants.__BASES__:
        throw PxAttributeError.readOnlyAttribute(attrName, self, ctx);
      case Constants.__ABSTRACTMETHODS__:
        if (cls instanceof IPyExtClass) {
          ((IPyExtClass) cls).setAbstractMethods(null, ctx);
        } else {
          throw PxAttributeError.readOnlyAttribute(attrName, self, ctx);
        }
        break;
      default:
        cls.delAttr(attrName, ctx);
        break;
    }

    return None;
  }

  @SpecialInstanceMethod
  public static IPyObj __repr__(IPyObj self, PyContext ctx) {
    return PyStr.get("<class '" + ((IPyClass) self).name() + "'>");
  }
}
