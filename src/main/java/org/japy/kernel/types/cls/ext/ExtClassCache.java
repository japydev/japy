package org.japy.kernel.types.cls.ext;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.jexec.codegen.SubClassCodegen;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.PyClassDict;
import org.japy.kernel.types.cls.builtin.IPyBuiltinClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.Args;

public class ExtClassCache implements IExtClassCache {
  private static final String CLASS_NAME_SUFFIX = "$Ext";

  private static class Key {
    public final IPyBuiltinClass builtinClass;
    public final Class<? extends IPyObj> instanceClass;

    public Key(IPyBuiltinClass builtinClass, Class<? extends IPyObj> instanceClass) {
      this.builtinClass = builtinClass;
      this.instanceClass = instanceClass;

      if (Debug.ENABLED) {
        dcheck(builtinClass.instanceClass().isAssignableFrom(instanceClass));
      }
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof Key)) return false;
      Key that = (Key) o;
      return builtinClass == that.builtinClass && instanceClass == that.instanceClass;
    }

    @Override
    public int hashCode() {
      return Objects.hash(builtinClass, instanceClass);
    }
  }

  public static class Entry {
    final Class<? extends IPyObj> instanceClass;
    final PyClassDict superDict;

    public Entry(Class<? extends IPyObj> instanceClass, PyClassDict superDict) {
      this.instanceClass = instanceClass;
      this.superDict = superDict;
    }
  }

  private final Loader loader = new Loader(getClass().getClassLoader());
  private final Map<Key, Entry> cache = new HashMap<>();
  private final SubClassCodegen codegen = new SubClassCodegen();

  public synchronized Entry get(
      IPyBuiltinClass builtinClass, Class<? extends IPyObj> instanceClass) {
    return cache.computeIfAbsent(new Key(builtinClass, instanceClass), this::compute);
  }

  @SuppressWarnings("unchecked")
  private Entry compute(Key bsci) {
    Constructor<?> constructor = PyBuiltinClass.getSubClassConstructor(bsci.instanceClass);
    Class<?>[] ptypes = constructor.getParameterTypes();
    boolean needClassParameter = ptypes.length == 3;

    if (Debug.ENABLED) {
      dcheck(ptypes.length == 2 || ptypes.length == 3);
      if (needClassParameter) {
        dcheck(ptypes[0] == IPyClass.class);
        dcheck(ptypes[1] == Args.class);
        dcheck(ptypes[2] == PyContext.class);
      } else {
        dcheck(ptypes[0] == Args.class);
        dcheck(ptypes[1] == PyContext.class);
      }
    }

    String className = bsci.instanceClass.getName() + CLASS_NAME_SUFFIX;

    // TODO what class loader should we use here?
    byte[] classFile = codegen.generate(bsci.instanceClass, className, needClassParameter);
    loader.setPending(className, classFile);
    Class<? extends IPyObj> clazz;
    try {
      clazz = (Class<? extends IPyObj>) loader.loadClass(className);
    } catch (ClassNotFoundException e) {
      throw ExcUtil.rethrow(e);
    }

    return new Entry(clazz, ExtClassLib.makeSuperDict(bsci.builtinClass, clazz));
  }

  private static class Loader extends ClassLoader {
    private @Nullable String name;
    private byte @Nullable [] code;

    public Loader(ClassLoader parent) {
      super(parent);
    }

    public void setPending(String name, byte[] code) {
      this.name = name;
      this.code = code;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
      if (this.name == null || !this.name.equals(name)) {
        throw new ClassNotFoundException("unknown class " + name);
      }

      dcheckNotNull(this.code);

      Class<?> klass = defineClass(this.name, this.code, 0, code.length);

      this.name = null;
      this.code = null;

      return klass;
    }
  }
}
