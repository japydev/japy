package org.japy.kernel.types.cls;

import org.japy.kernel.types.obj.IPyInstanceDict;

public interface IPyClassDict extends IPyInstanceDict {}
