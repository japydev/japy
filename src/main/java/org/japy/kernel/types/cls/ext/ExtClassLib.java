package org.japy.kernel.types.cls.ext;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolSuperMethod;
import org.japy.kernel.types.cls.ClassLib;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.PyClassDict;
import org.japy.kernel.types.cls.PyObjectClass;
import org.japy.kernel.types.cls.PyTypeClass;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.SpecialMethodTable;
import org.japy.kernel.types.cls.builtin.BuiltinClassLib;
import org.japy.kernel.types.cls.builtin.IPyBuiltinClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinInstanceMethod;
import org.japy.kernel.types.cls.desc.IPyDescriptor;
import org.japy.kernel.types.cls.desc.PyClassMethod;
import org.japy.kernel.types.cls.desc.PyInstanceMethod;
import org.japy.kernel.types.cls.desc.PyMethodDescriptor;
import org.japy.kernel.types.cls.desc.PyStaticMethod;
import org.japy.kernel.types.coll.IPyContains;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyMappingProtocol;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.dict.IPyROMappingProtocol;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.FuncAdapterLib;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.func.IPyCallable;
import org.japy.kernel.types.func.IPyFunc;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.types.obj.proto.IPyObjWithAscii;
import org.japy.kernel.types.obj.proto.IPyObjWithDeepRepr;
import org.japy.kernel.types.obj.proto.IPyObjWithLen;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.types.obj.proto.IPyObjWithStr;
import org.japy.kernel.types.obj.proto.IPyObjWithTruth;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;
import org.japy.kernel.util.PyConstants;

public class ExtClassLib {
  public static final List<Class<?>> PROTOCOLS =
      List.of(
          IPyObjWithTruth.class,
          IPyObjWithLen.class,
          IPyIntHashableObj.class,
          IPyObjWithStr.class,
          IPyObjWithRepr.class,
          IPyObjWithDeepRepr.class,
          IPyObjWithAscii.class,
          IPyFunc.class,
          IPyContains.class,
          IPyROMappingProtocol.class,
          IPyMappingProtocol.class);

  public static Consumer<IPyClass> prepareClass(
      ExtClassData.Builder dataBuilder, IPyClass[] bases, IPyRODict namespace, PyContext ctx) {
    return new Preparer(dataBuilder, bases, namespace, ctx).prepare();
  }

  public static void finishClassSetup(IPyExtClass cls, Args classKwarg, PyContext ctx) {
    setClassModule(cls, ctx);
    ClassLib.finishClassSetup(cls, classKwarg, ctx);
  }

  private static void setClassModule(IPyExtClass cls, PyContext ctx) {
    IPyDict globals = ctx.thread.frame().globals();
    @Nullable IPyObj module = globals.getOrNull(Constants.__NAME__, ctx);
    if (module instanceof PyStr) {
      cls.setModule((PyStr) module, ctx);
    }
  }

  public static PyClassDict makeSuperDict(
      IPyBuiltinClass cls, Class<? extends IPyObj> instanceClass) {
    PyClassDict dict = new PyClassDict(cls.classDict());
    for (Class<?> protoClass : PROTOCOLS) {
      if (protoClass.isAssignableFrom(instanceClass)) {
        addSuperMethods(cls, protoClass, dict);
      }
    }
    return dict;
  }

  private static void addSuperMethods(IPyBuiltinClass cls, Class<?> protoClass, PyClassDict dict) {
    Protocol pAnn = protoClass.getAnnotation(Protocol.class);
    Class<?> adapterClass = pAnn.value();
    MethodHandles.Lookup lookup = MethodHandles.lookup();
    for (Method method : adapterClass.getDeclaredMethods()) {
      @Nullable ProtocolSuperMethod psmAnn = method.getAnnotation(ProtocolSuperMethod.class);
      if (psmAnn != null) {
        dict.set(
            psmAnn.value(),
            BuiltinClassLib.wrapInstanceMethod(cls, psmAnn.value(), method, lookup));
      }
    }
  }

  private static class ClassAttrValue {
    final IPyObj value;
    final @Nullable SpecialMethod specialMethod;
    final @Nullable FuncHandle specialMethodHandle;

    private ClassAttrValue(IPyObj value) {
      this(value, null, null);
    }

    private ClassAttrValue(
        IPyObj value,
        @Nullable SpecialMethod specialMethod,
        @Nullable FuncHandle specialMethodHandle) {
      this.value = value;
      this.specialMethod = specialMethod;
      this.specialMethodHandle = specialMethodHandle;
    }
  }

  static void setAttrInDict(PyExtClass cls, String attr, IPyObj value, PyContext ctx) {
    ClassAttrValue attrValue = computeAttrValue(attr, value);
    cls.classDict().set(attr, attrValue.value);

    if (attrValue.specialMethod != null) {
      updateMethodTables(cls, attrValue.specialMethod, attrValue.specialMethodHandle, ctx);
    }
  }

  static void delAttrFromDict(PyExtClass cls, String attr, PyContext ctx) {
    if (cls.classDict().popOrNull(attr) == null) {
      throw new PxAttributeError(attr, cls, true, ctx);
    }

    @Nullable SpecialMethod specialMethod = SpecialMethod.find(attr);
    if (specialMethod != null) {
      updateMethodTables(cls, specialMethod, null, ctx);
    }
  }

  private static void addToQueue(
      PyExtClass base, Set<PyExtClass> queue, Set<PyExtClass> done, PyContext ctx) {
    PyList subclasses = base.subclasses(ctx);
    for (IPyObj subclass : subclasses.content) {
      if (!done.contains((PyExtClass) subclass)) {
        queue.add((PyExtClass) subclass);
      }
    }
  }

  private static void updateAllMethodTablesFromBases(
      PyExtClass changedClass, SpecialMethod specialMethod, PyContext ctx) {
    Set<PyExtClass> queue = new LinkedHashSet<>();
    Set<PyExtClass> done = new HashSet<>();
    addToQueue(changedClass, queue, done, ctx);
    while (!queue.isEmpty()) {
      PyExtClass cls = queue.iterator().next();
      queue.remove(cls);
      if (!done.contains(cls)) {
        done.add(cls);
        if (updateMethodTablesFromBases(cls, specialMethod)) {
          addToQueue(cls, queue, done, ctx);
        }
      }
    }
  }

  private static boolean updateMethodTablesFromBases(PyExtClass cls, SpecialMethod specialMethod) {
    @Var boolean changed = false;

    @Nullable FuncHandle myMethod = cls.originalMethodTable().getRaw(specialMethod);
    if (myMethod == null) {
      @Nullable FuncHandle oldMethod = cls.methodTable().getRaw(specialMethod);
      @Nullable FuncHandle newMethod = cls.mro()[1].specialMethodOrNone(specialMethod);
      if (newMethod != oldMethod) {
        cls.setMethod(specialMethod, newMethod);
        changed = true;
      }
    }

    if (cls instanceof PyExtBuiltinSubClass) {
      PyExtBuiltinSubClass bsc = (PyExtBuiltinSubClass) cls;
      @Nullable FuncHandle oldExtMethod = bsc.getExtMethod(specialMethod);
      @Nullable FuncHandle newExtMethod = getMethodFromBaseMroNoBuiltin(specialMethod, cls);
      if (oldExtMethod != newExtMethod) {
        bsc.setExtMethod(specialMethod, newExtMethod);
        changed = true;
      }
    }

    return changed;
  }

  private static void updateMethodTables(
      PyExtClass cls,
      SpecialMethod specialMethod,
      @Nullable FuncHandle methodHandle,
      PyContext ctx) {

    cls.setOriginalMethod(specialMethod, methodHandle);

    @Nullable
    FuncHandle newMethod =
        methodHandle != null ? methodHandle : cls.mro()[1].specialMethodOrNone(specialMethod);
    cls.setMethod(specialMethod, newMethod);

    if (cls instanceof PyExtBuiltinSubClass) {
      PyExtBuiltinSubClass bsc = (PyExtBuiltinSubClass) cls;
      bsc.setExtMethod(specialMethod, getMethodFromBaseMroNoBuiltin(specialMethod, cls));
    }

    updateAllMethodTablesFromBases(cls, specialMethod, ctx);
  }

  private static final Set<String> CLASS_METHODS = Set.of(Constants.__INIT_SUBCLASS__);

  private static ClassAttrValue computeAttrValue(String attr, IPyObj value) {
    @Nullable SpecialMethod specialMethod = SpecialMethod.find(attr);

    if (specialMethod == null) {
      IPyObj realValue;
      if (!(value instanceof IPyFunc) || value instanceof IPyDescriptor) {
        realValue = value;
      } else if (CLASS_METHODS.contains(attr)) {
        realValue = new PyClassMethod(value);
      } else {
        realValue = new PyInstanceMethod(value);
      }
      return new ClassAttrValue(realValue);
    }

    if (!(value instanceof IPyFunc)) {
      return new ClassAttrValue(value, specialMethod, SpecialMethodTable.DELETED_METHOD);
    }

    IPyFunc func = (IPyFunc) value;

    IPyObj realValue;
    FuncHandle funcHandle;
    if (value instanceof IPyDescriptor) {
      realValue = value;
      funcHandle = getMethodHandleFromDescriptor((IPyDescriptor) value, specialMethod);
    } else {
      switch (specialMethod.kind) {
        case STATIC:
          realValue = new PyStaticMethod(value);
          break;
        case CLASS:
          realValue = new PyClassMethod(func);
          break;
        case INSTANCE:
          realValue = new PyInstanceMethod(func);
          break;
        default:
          throw InternalErrorException.notReached();
      }
      funcHandle = FuncAdapterLib.makeSpecialMethod(func, specialMethod);
    }

    return new ClassAttrValue(realValue, specialMethod, funcHandle);
  }

  private static IPyCallable getCallable(IPyObj func) {
    return func instanceof IPyCallable
        ? (IPyCallable) func
        : (args, ctx) -> FuncLib.call(func, args, ctx);
  }

  private static FuncHandle getMethodHandleFromDescriptor(
      IPyDescriptor descriptor, SpecialMethod specialMethod) {
    if (descriptor instanceof PyMethodDescriptor) {
      return FuncAdapterLib.makeSpecialMethod(
          getCallable(((PyMethodDescriptor) descriptor).func), specialMethod);
    }

    if (descriptor instanceof PyBuiltinInstanceMethod) {
      return FuncAdapterLib.makeSpecialMethod((PyBuiltinInstanceMethod) descriptor, specialMethod);
    }

    throw new InternalErrorException("unhandled descriptor type: " + descriptor);
  }

  private static @Nullable FuncHandle getMethodFromMroNoBuiltin(
      SpecialMethod m, IPyClass[] baseMro) {
    for (IPyClass cls : baseMro) {
      if (cls instanceof IPyBuiltinClass) {
        break;
      }
      @Nullable FuncHandle h = cls.originalMethodTable().getRaw(m);
      if (h != null) {
        return h;
      }
    }
    return null;
  }

  private static @Nullable FuncHandle getMethodFromBaseMroNoBuiltin(
      SpecialMethod m, IPyClass thisClass) {
    IPyClass[] mro = thisClass.mro();
    for (int i = 1; i != mro.length; ++i) {
      IPyClass cls = mro[i];
      if (cls instanceof IPyBuiltinClass) {
        break;
      }
      @Nullable FuncHandle h = cls.originalMethodTable().getRaw(m);
      if (h != null) {
        return h;
      }
    }
    return null;
  }

  private static class Preparer {
    private final ExtClassData.Builder dataBuilder;
    private final IPyClass[] bases;
    private final IPyRODict namespace;
    private final PyContext ctx;

    private final PyClassDict classDict;
    private final SpecialMethodTable.Builder methodTableBuilder = SpecialMethodTable.builder();
    private final List<PyStr> names = new ArrayList<>();
    private final List<IPyObj> nameSetters = new ArrayList<>();

    private Preparer(
        ExtClassData.Builder dataBuilder, IPyClass[] bases, IPyRODict namespace, PyContext ctx) {
      this.dataBuilder = dataBuilder;
      this.classDict = new PyClassDict(namespace.len(ctx));
      this.bases = bases;
      this.namespace = namespace;
      this.ctx = ctx;
    }

    Consumer<IPyClass> prepare() {
      namespace.forEach(this::addClassAttribute, ctx);
      adjustClassDict();

      dataBuilder.bases(bases).classDict(classDict);
      computeBuiltinAncestor();

      SpecialMethodTable methodTable = methodTableBuilder.build();

      IPyClass[] baseMro = ClassLib.getBaseMro(bases, ctx);
      SpecialMethodTable complete = ClassLib.addMethodsFromMro(methodTable, bases, baseMro);

      SpecialMethodTable.Builder extBuilder = methodTable.copy();
      copyMethodsFromMroNoBuiltin(extBuilder, baseMro);

      dataBuilder
          .baseMro(baseMro)
          .originalMethodTable(methodTable)
          .completeMethodTable(complete)
          .extMethodTable(extBuilder.build());

      return this::setNames;
    }

    private static void copyMethodsFromMroNoBuiltin(
        SpecialMethodTable.Builder builder, IPyClass[] baseMro) {
      for (SpecialMethod m : SpecialMethod.values()) {
        if (builder.get(m) == null) {
          builder.set(m, getMethodFromMroNoBuiltin(m, baseMro));
        }
      }
    }

    private static PxException throwCannotSubclass(IPyClass base1, IPyClass base2, PyContext ctx) {
      throw PxException.typeError(
          String.format("cannot subclass built-in classes %s and %s", base1.name(), base2.name()),
          ctx);
    }

    private static class GetCommonBuiltinBase {
      private final IPyBuiltinClass base1;
      private final IPyBuiltinClass base2;

      final IPyBuiltinClass commonBase;
      final Class<? extends IPyObj> instanceClass;

      GetCommonBuiltinBase(
          IPyBuiltinClass base1,
          Class<? extends IPyObj> instanceClass1,
          IPyBuiltinClass base2,
          Class<? extends IPyObj> instanceClass2,
          PyContext ctx) {
        this.base1 = base1;
        this.base2 = base2;

        if (Debug.ENABLED) {
          dcheck(base1 != PyObjectClass.INSTANCE && base2 != PyObjectClass.INSTANCE);
          dcheck(base1 != PyTypeClass.INSTANCE && base2 != PyTypeClass.INSTANCE);
        }

        if (instanceClass1 == instanceClass2) {
          instanceClass = instanceClass1;
        } else if (instanceClass1.isAssignableFrom(instanceClass2)) {
          instanceClass = instanceClass2;
          checkNoProtocolsImplemented(instanceClass2, instanceClass1, ctx);
        } else if (instanceClass2.isAssignableFrom(instanceClass1)) {
          instanceClass = instanceClass1;
          checkNoProtocolsImplemented(instanceClass1, instanceClass2, ctx);
        } else {
          throw throwCannotSubclass(ctx);
        }

        if (base1 == base2) {
          commonBase = base1;
          return;
        }

        commonBase = getCommonBase(base1, base2);
        if (commonBase == PyObjectClass.INSTANCE) {
          throw throwCannotSubclass(ctx);
        }

        if (!commonBase.instanceClass().isAssignableFrom(instanceClass)) {
          throw throwCannotSubclass(ctx);
        }
      }

      private void checkNoProtocolsImplemented(Class<?> child, Class<?> parent, PyContext ctx) {
        for (Class<?> clazz = child; clazz != parent; clazz = clazz.getSuperclass()) {
          for (Class<?> iface : clazz.getInterfaces()) {
            if (PROTOCOLS.contains(iface)) {
              throw throwCannotSubclass(ctx);
            }
          }
        }
      }

      private PxException throwCannotSubclass(PyContext ctx) {
        throw Preparer.throwCannotSubclass(base1, base2, ctx);
      }

      private static int indexOf(IPyClass cls, IPyClass[] mro, int start) {
        for (int i = start; i != mro.length; ++i) {
          if (mro[i] == cls) {
            return i;
          }
        }
        return -1;
      }

      private static IPyBuiltinClass getCommonBase(IPyBuiltinClass cls1, IPyBuiltinClass cls2) {
        IPyClass[] mro1 = cls1.mro();
        IPyClass[] mro2 = cls2.mro();
        for (int i = 0; i != mro1.length && i != mro2.length; ++i) {
          @Var int idx = indexOf(mro1[i], mro2, i);
          if (idx != -1) {
            return (IPyBuiltinClass) mro1[i];
          }
          idx = indexOf(mro2[i], mro1, i + 1);
          if (idx != -1) {
            return (IPyBuiltinClass) mro2[i];
          }
        }
        // we should have at least reached object
        throw InternalErrorException.notReached();
      }
    }

    private void computeBuiltinAncestor() {
      @Var
      @Nullable
      IPyBuiltinClass ancestor = null;
      @Var
      @Nullable
      Class<? extends IPyObj> instanceClass = null;
      for (IPyClass base : bases) {
        @Nullable IPyBuiltinClass ancestorHere;
        @Nullable Class<? extends IPyObj> instanceClassHere;
        if (base instanceof IPyBuiltinClass) {
          if (base == PyObjectClass.INSTANCE) {
            ancestorHere = null;
            instanceClassHere = null;
          } else if (base == PyTypeClass.INSTANCE) {
            ancestorHere = (IPyBuiltinClass) base;
            instanceClassHere = null;
          } else {
            ancestorHere = (IPyBuiltinClass) base;
            instanceClassHere = ((IPyBuiltinClass) base).instanceClass();
          }
        } else {
          ancestorHere = ((IPyExtClass) base).builtinAncestor();
          if (ancestorHere == PyTypeClass.INSTANCE) {
            instanceClassHere = null;
          } else {
            instanceClassHere = ((IPyExtClass) base).builtinInstanceClass();
          }
        }

        if (ancestorHere != null) {
          if (ancestor == null) {
            ancestor = ancestorHere;
            instanceClass = instanceClassHere;
          } else if (ancestor == PyTypeClass.INSTANCE || ancestorHere == PyTypeClass.INSTANCE) {
            if (ancestor != ancestorHere) {
              throw throwCannotSubclass(ancestor, ancestorHere, ctx);
            }
          } else {
            GetCommonBuiltinBase getCommon =
                new GetCommonBuiltinBase(
                    ancestor,
                    dcheckNotNull(instanceClass),
                    ancestorHere,
                    dcheckNotNull(instanceClassHere),
                    ctx);
            ancestor = getCommon.commonBase;
            instanceClass = getCommon.instanceClass;
          }
        }
      }

      dataBuilder.builtinAncestor(ancestor).builtinInstanceClass(instanceClass);
    }

    private void setNames(IPyClass klass) {
      for (int i = 0, c = names.size(); i != c; ++i) {
        FuncLib.call(nameSetters.get(i), klass, names.get(i), ctx);
      }
    }

    private void adjustClassDict() {
      if (classDict.contains(Constants.__EQ__) && !classDict.contains(Constants.__HASH__)) {
        addClassAttribute(PyConstants.__HASH__, None);
      }
    }

    private void addClassAttribute(IPyObj nameObj, IPyObj value) {
      String name = ObjLib.attrName(nameObj, ctx);

      ClassAttrValue attrValue = computeAttrValue(name, value);

      @Nullable
      IPyObj nameSetter =
          ObjLib.getClassAttributeOrNull(
              attrValue.value.type(), attrValue.value, Constants.__SET_NAME__, ctx);
      if (nameSetter != null) {
        nameSetters.add(nameSetter);
        names.add((PyStr) nameObj);
      }

      if (attrValue.specialMethod != null) {
        methodTableBuilder.set(attrValue.specialMethod, attrValue.specialMethodHandle);
      }

      classDict.set(name, attrValue.value);
    }
  }
}
