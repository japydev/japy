package org.japy.kernel.types.cls.ext.adapters;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Adapter;
import org.japy.kernel.types.annotations.AdapterMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.ProtocolSuperMethod;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.ext.IPyBuiltinSubClassInstance;
import org.japy.kernel.types.coll.dict.IPyROMappingProtocol;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxKeyError;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.util.Constants;

@Adapter(IPyROMappingProtocol.class)
public interface IPyROMappingProtocolAdapter extends IPyBuiltinSubClassInstance {
  @ProtocolSuperMethod(Constants.__GETITEM__)
  IPyObj baseGetItem(@Param("key") IPyObj key, PyContext ctx);

  @Nullable
  IPyObj baseGetOrNull(IPyObj key, PyContext ctx);

  @ProtocolSuperMethod(Constants.ITEMS)
  IPyObj baseItems(PyContext ctx);

  @ProtocolSuperMethod(Constants.KEYS)
  IPyObj baseKeys(PyContext ctx);

  @ProtocolSuperMethod(Constants.VALUES)
  IPyObj baseValues(PyContext ctx);

  @AdapterMethod
  default IPyObj thisGetItem(IPyObj key, PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.GETITEM);
    if (h == DELETED_METHOD) {
      throw PxException.typeError("__getitem__ is deleted", ctx);
    }
    if (h == null) {
      return baseGetItem(key, ctx);
    }
    try {
      return (IPyObj) h.handle.invokeExact((IPyObj) this, key, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @AdapterMethod
  default @Nullable IPyObj thisGetOrNull(IPyObj key, PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.GETITEM);
    if (h == DELETED_METHOD) {
      throw PxException.typeError("__getitem__ is deleted", ctx);
    }
    if (h == null) {
      return baseGetOrNull(key, ctx);
    }
    try {
      return (IPyObj) h.handle.invokeExact((IPyObj) this, key, ctx);
    } catch (PxKeyError ignored) {
      return null;
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @AdapterMethod
  default IPyObj thisItems(PyContext ctx) {
    @Nullable IPyObj items = getExtMethod(Constants.ITEMS);
    if (items == null) {
      return baseItems(ctx);
    }
    return FuncLib.call(items, ctx);
  }

  @AdapterMethod
  default IPyObj thisKeys(PyContext ctx) {
    @Nullable IPyObj keys = getExtMethod(Constants.KEYS);
    if (keys == null) {
      return baseKeys(ctx);
    }
    return FuncLib.call(keys, ctx);
  }

  @AdapterMethod
  default IPyObj thisValues(PyContext ctx) {
    @Nullable IPyObj values = getExtMethod(Constants.VALUES);
    if (values == null) {
      return baseValues(ctx);
    }
    return FuncLib.call(values, ctx);
  }
}
