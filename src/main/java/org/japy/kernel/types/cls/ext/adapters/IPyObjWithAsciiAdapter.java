package org.japy.kernel.types.cls.ext.adapters;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Adapter;
import org.japy.kernel.types.annotations.AdapterMethod;
import org.japy.kernel.types.annotations.ProtocolSuperMethod;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.ext.IPyBuiltinSubClassInstance;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.obj.proto.IPyObjWithAscii;
import org.japy.kernel.util.Constants;

@Adapter(IPyObjWithAscii.class)
public interface IPyObjWithAsciiAdapter extends IPyBuiltinSubClassInstance {
  @ProtocolSuperMethod(Constants.__ASCII__)
  PyStr baseAscii(PyContext ctx);

  @AdapterMethod
  default PyStr thisAscii(PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.ASCII);
    if (h == DELETED_METHOD) {
      throw PrintLib.throwAsciiUnsupported(this, ctx);
    }
    if (h == null) {
      return baseAscii(ctx);
    }
    IPyObj s;
    try {
      s = (IPyObj) h.handle.invokeExact((IPyObj) this, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
    if (!(s instanceof PyStr)) {
      throw PxException.typeError(
          "__ascii__ returned an instance of class '" + s.typeName() + "', expected a string", ctx);
    }
    return (PyStr) s;
  }
}
