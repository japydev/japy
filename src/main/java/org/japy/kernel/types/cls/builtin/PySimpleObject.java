package org.japy.kernel.types.cls.builtin;

import org.japy.infra.validation.Validator;
import org.japy.kernel.types.obj.IPyInstanceDict;
import org.japy.kernel.types.obj.PyInstanceDict;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

public abstract class PySimpleObject implements IPyTrueAtomObj {
  protected final IPyInstanceDict dict = new PyInstanceDict();

  protected PySimpleObject() {}

  @Override
  public IPyInstanceDict instanceDict() {
    return dict;
  }

  @Override
  public void validate(Validator v) {
    v.validate(dict);
  }
}
