package org.japy.kernel.types.cls.ext;

import static org.japy.infra.util.ObjUtil.or;
import static org.japy.kernel.types.misc.PyNone.None;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyMetaClass;
import org.japy.kernel.types.cls.PyAbstractClass;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.SpecialMethodTable;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.Constants;
import org.japy.kernel.util.PyConstants;

abstract class PyExtClass extends PyAbstractClass implements IPyExtClass {
  private PyStr module = PyConstants.UNKNOWN_MODULE;
  private final IPyMetaClass meta;
  private SpecialMethodTable completeMethodTable;
  private SpecialMethodTable originalMethodTable;
  private boolean isAbstract;

  protected PyExtClass(ExtClassData data, IPyMetaClass meta) {
    super(data.name, data.bases, data.baseMro, data.classDict);
    this.meta = meta;
    this.completeMethodTable = data.completeMethodTable;
    this.originalMethodTable = data.originalMethodTable;
  }

  @Override
  public boolean isAbstract() {
    return isAbstract;
  }

  @Override
  public void setAbstractMethods(@Nullable IPyObj methods, PyContext ctx) {
    if (methods != null) {
      classDict().set(Constants.__ABSTRACTMETHODS__, methods);
    } else if (!classDict().contains(Constants.__ABSTRACTMETHODS__)) {
      throw new PxAttributeError(Constants.__ABSTRACTMETHODS__, ctx);
    } else {
      classDict().del(Constants.__ABSTRACTMETHODS__);
    }
    isAbstract = methods != null && ObjLib.isTrue(methods, ctx);
  }

  @Override
  public @Nullable IPyObj getAbstractMethods(PyContext ctx) {
    return classDict().get(Constants.__ABSTRACTMETHODS__);
  }

  @Override
  public boolean isBuiltinClass() {
    return false;
  }

  @Override
  public SpecialMethodTable methodTable() {
    return completeMethodTable;
  }

  @Override
  public SpecialMethodTable originalMethodTable() {
    return originalMethodTable;
  }

  void setMethod(SpecialMethod specialMethod, @Nullable FuncHandle methodHandle) {
    completeMethodTable = completeMethodTable.set(specialMethod, methodHandle);
  }

  void setOriginalMethod(SpecialMethod specialMethod, @Nullable FuncHandle methodHandle) {
    originalMethodTable = originalMethodTable.set(specialMethod, methodHandle);
  }

  @Override
  public IPyMetaClass type() {
    return meta;
  }

  @Override
  public void setName(PyStr name, PyContext ctx) {
    this.name = name.toString();
  }

  @Override
  public void setQualName(PyStr qualName, PyContext ctx) {
    this.qualName = qualName.toString();
  }

  @Override
  public PyStr module() {
    return module;
  }

  @Override
  public void setModule(PyStr module, PyContext ctx) {
    this.module = module;
  }

  @Override
  public IPyObj doc() {
    @Nullable IPyObj doc = classDict().get(Constants.__DOC__);
    return or(doc, None);
  }

  @Override
  public void setDoc(IPyObj doc, PyContext ctx) {
    classDict().set(Constants.__DOC__, doc);
  }

  @Override
  public synchronized IPyObj annotations() {
    @Var
    @Nullable
    IPyObj value = classDict().get(Constants.__ANNOTATIONS__);
    if (value == null) {
      value = new PyDict();
      classDict().set(Constants.__ANNOTATIONS__, value);
    }
    return value;
  }

  @Override
  public void setAnnotations(IPyObj annotations, PyContext ctx) {
    if (annotations != None && !(annotations instanceof PyDict)) {
      throw PxException.typeError("__annotations__ may only be set to None or a dict", ctx);
    }
    if (annotations == None) {
      classDict().del(Constants.__ANNOTATIONS__);
    } else {
      classDict().set(Constants.__ANNOTATIONS__, annotations);
    }
  }

  @Override
  public void setAttr(String attr, IPyObj value, PyContext ctx) {
    ExtClassLib.setAttrInDict(this, attr, value, ctx);
  }

  @Override
  public void delAttr(String attr, PyContext ctx) {
    ExtClassLib.delAttrFromDict(this, attr, ctx);
  }
}
