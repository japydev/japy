package org.japy.kernel.types.cls;

import java.lang.invoke.MethodType;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.util.Args;

public enum MethodKind {
  STATIC(MethodType.methodType(IPyObj.class, Args.class, PyContext.class)),
  CLASS(MethodType.methodType(IPyObj.class, IPyClass.class, Args.class, PyContext.class)),
  INSTANCE(MethodType.methodType(IPyObj.class, IPyObj.class, Args.class, PyContext.class)),
  ;

  @SuppressWarnings("ImmutableEnumChecker")
  public final MethodType methodType;

  MethodKind(MethodType methodType) {
    this.methodType = methodType;
  }
}
