package org.japy.kernel.types.cls;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.DeepRepr;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.obj.IPyAttrDict;
import org.japy.kernel.types.obj.PyAttrDict;

public class PyClassDict extends PyAttrDict implements IPyClassDict {
  public PyClassDict() {}

  public PyClassDict(int expectedSize) {
    super(expectedSize);
  }

  public PyClassDict(IPyAttrDict dict) {
    super(dict);
  }

  @Override
  public void print(DeepRepr dr, PyContext ctx) {
    PrintLib.printDict(this, dr, ctx);
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    return new PyClassDict(this);
  }

  private static class TYPE_HOLDER {
    private static final IPyClass TYPE =
        new PyBuiltinClass("classdict", PyClassDict.class, PyBuiltinClass.INTERNAL, null);
  }

  @Override
  public IPyClass type() {
    return TYPE_HOLDER.TYPE;
  }
}
