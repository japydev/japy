package org.japy.kernel.types.cls.desc;

import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;

public interface IPyDescriptorDel extends IPyDescriptor {
  void delete(IPyObj instance, PyContext ctx);

  @SpecialInstanceMethod
  default IPyObj __delete__(@Param("instance") IPyObj instance, PyContext ctx) {
    delete(instance, ctx);
    return None;
  }
}
