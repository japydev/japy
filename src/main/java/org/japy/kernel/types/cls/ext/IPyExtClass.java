package org.japy.kernel.types.cls.ext;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.IPyBuiltinClass;
import org.japy.kernel.util.Args;

public interface IPyExtClass extends IPyClass {
  IPyObj instantiate(Args args, PyContext ctx);

  @Nullable
  IPyBuiltinClass builtinAncestor();

  @Nullable
  Class<? extends IPyObj> builtinInstanceClass();

  boolean isAbstract();

  @Nullable
  IPyObj getAbstractMethods(PyContext ctx);

  void setAbstractMethods(@Nullable IPyObj methods, PyContext ctx);
}
