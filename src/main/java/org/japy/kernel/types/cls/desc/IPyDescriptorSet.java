package org.japy.kernel.types.cls.desc;

import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;

public interface IPyDescriptorSet extends IPyDescriptor {
  void set(IPyObj instance, IPyObj value, PyContext ctx);

  @SpecialInstanceMethod
  default IPyObj __set__(
      @Param("instance") IPyObj instance, @Param("value") IPyObj value, PyContext ctx) {
    set(instance, value, ctx);
    return None;
  }
}
