package org.japy.kernel.types.cls.ext.adapters;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.Adapter;
import org.japy.kernel.types.annotations.AdapterMethod;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.ext.IPyBuiltinSubClassInstance;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.obj.proto.IPyObjWithDeepRepr;

@Adapter(IPyObjWithDeepRepr.class)
public interface IPyObjWithDeepReprAdapter extends IPyBuiltinSubClassInstance {
  PyStr baseDeepRepr(PyContext ctx);

  @AdapterMethod
  default PyStr thisDeepRepr(PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.REPR);
    if (h == DELETED_METHOD) {
      throw PrintLib.throwReprUnsupported(this, ctx);
    }
    if (h == null) {
      return baseDeepRepr(ctx);
    }
    return IPyObjWithReprAdapter.__callExtRepr(h, this, ctx);
  }
}
