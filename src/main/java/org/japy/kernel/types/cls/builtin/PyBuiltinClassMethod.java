package org.japy.kernel.types.cls.builtin;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.util.Args;

class PyBuiltinClassMethod extends PyBuiltinMethodDescriptor {
  // IPyObj foo(IPyClass cls, Args args, PyContext ctx)
  private static final MethodType METHOD_TYPE =
      MethodType.methodType(IPyObj.class, IPyClass.class, Args.class, PyContext.class);

  PyBuiltinClassMethod(FuncHandle fh, IPyClass objClass, String name) {
    super(fh, objClass, name);

    if (Debug.ENABLED) {
      dcheck(fh.type().parameterCount() != 0);
      dcheck(IPyClass.class.isAssignableFrom(fh.type().parameterType(0)));
      dcheck(fh.type().changeParameterType(0, IPyClass.class).equals(METHOD_TYPE));
    }
  }

  private static class TYPE_HOLDER {
    private static final IPyClass TYPE =
        new PyBuiltinClass(
            "builtinclassmethod",
            PyBuiltinClassMethod.class,
            MethodHandles.lookup(),
            PyBuiltinClass.INTERNAL);
  }

  @Override
  public IPyClass type() {
    return TYPE_HOLDER.TYPE;
  }

  @Override
  public IPyObj get(IPyObj instance, IPyObj owner, PyContext ctx) {
    return new PyBoundBuiltinMethod(this, instance == None ? (IPyClass) owner : instance.type());
  }
}
