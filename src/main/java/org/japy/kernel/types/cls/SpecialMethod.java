package org.japy.kernel.types.cls;

import static org.japy.kernel.types.cls.SpecialMethodTable.*;

import java.lang.invoke.MethodType;
import java.util.HashMap;
import java.util.Map;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.util.Constants;

@SuppressWarnings("ImmutableEnumChecker")
public enum SpecialMethod {
  NEW(Constants.__NEW__, TYPE_GENERIC_FUNC, MethodKind.STATIC),
  INIT(Constants.__INIT__, TYPE_GENERIC_INSTANCE_METHOD),
  HASH(Constants.__HASH__, TYPE_INT_OBJ1_CTX),
  EQ(Constants.__EQ__, TYPE_OBJ_OBJ2_CTX),
  NE(Constants.__NE__, TYPE_OBJ_OBJ2_CTX),
  LT(Constants.__LT__, TYPE_OBJ_OBJ2_CTX),
  LE(Constants.__LE__, TYPE_OBJ_OBJ2_CTX),
  GT(Constants.__GT__, TYPE_OBJ_OBJ2_CTX),
  GE(Constants.__GE__, TYPE_OBJ_OBJ2_CTX),
  STR(Constants.__STR__, TYPE_OBJ_OBJ1_CTX),
  REPR(Constants.__REPR__, TYPE_OBJ_OBJ1_CTX),
  ASCII(Constants.__ASCII__, TYPE_OBJ_OBJ1_CTX),
  FORMAT(Constants.__FORMAT__, TYPE_OBJ_OBJ2_CTX),
  GETATTRIBUTE(Constants.__GETATTRIBUTE__, TYPE_OBJ_OBJ2_CTX),
  GETATTR(Constants.__GETATTR__, TYPE_OBJ_OBJ2_CTX),
  SETATTR(Constants.__SETATTR__, TYPE_VOID_OBJ3_CTX),
  SET(Constants.__SET__, TYPE_VOID_OBJ3_CTX),
  GET(Constants.__GET__, TYPE_OBJ_OBJ3_CTX),
  DELETE(Constants.__DELETE__, TYPE_VOID_OBJ2_CTX),
  DELATTR(Constants.__DELATTR__, TYPE_VOID_OBJ2_CTX),
  BOOL(Constants.__BOOL__, TYPE_BOOL_OBJ1_CTX),
  LEN(Constants.__LEN__, TYPE_INT_OBJ1_CTX),
  CALL(Constants.__CALL__, TYPE_GENERIC_INSTANCE_METHOD),
  ITER(Constants.__ITER__, TYPE_OBJ_OBJ1_CTX),
  NEXT(Constants.__NEXT__, TYPE_OBJ_OBJ1_CTX),
  REVERSED(Constants.__REVERSED__, TYPE_OBJ_OBJ1_CTX),
  GETITEM(Constants.__GETITEM__, TYPE_OBJ_OBJ2_CTX),
  SETITEM(Constants.__SETITEM__, TYPE_VOID_OBJ3_CTX),
  DELITEM(Constants.__DELITEM__, TYPE_VOID_OBJ2_CTX),
  CONTAINS(Constants.__CONTAINS__, TYPE_BOOL_OBJ2_CTX),
  CLASSGETITEM(Constants.__CLASS_GETITEM__, TYPE_OBJ_OBJ2_CTX, MethodKind.CLASS),
  INSTANCECHECK(Constants.__INSTANCECHECK__, TYPE_BOOL_OBJ2_CTX),
  SUBCLASSCHECK(Constants.__SUBCLASSCHECK__, TYPE_BOOL_OBJ2_CTX),

  ADD(Constants.__ADD__, TYPE_OBJ_OBJ2_CTX),
  SUB(Constants.__SUB__, TYPE_OBJ_OBJ2_CTX),
  MUL(Constants.__MUL__, TYPE_OBJ_OBJ2_CTX),
  TRUEDIV(Constants.__TRUEDIV__, TYPE_OBJ_OBJ2_CTX),
  FLOORDIV(Constants.__FLOORDIV__, TYPE_OBJ_OBJ2_CTX),
  MOD(Constants.__MOD__, TYPE_OBJ_OBJ2_CTX),
  DIVMOD(Constants.__DIVMOD__, TYPE_OBJ_OBJ2_CTX),
  POW(Constants.__POW__, TYPE_OBJ_OBJ2_CTX),
  MATMUL(Constants.__MATMUL__, TYPE_OBJ_OBJ2_CTX),
  AND(Constants.__AND__, TYPE_OBJ_OBJ2_CTX),
  OR(Constants.__OR__, TYPE_OBJ_OBJ2_CTX),
  XOR(Constants.__XOR__, TYPE_OBJ_OBJ2_CTX),
  LSHIFT(Constants.__LSHIFT__, TYPE_OBJ_OBJ2_CTX),
  RSHIFT(Constants.__RSHIFT__, TYPE_OBJ_OBJ2_CTX),
  RADD(Constants.__RADD__, TYPE_OBJ_OBJ2_CTX),
  RSUB(Constants.__RSUB__, TYPE_OBJ_OBJ2_CTX),
  RMUL(Constants.__RMUL__, TYPE_OBJ_OBJ2_CTX),
  RTRUEDIV(Constants.__RTRUEDIV__, TYPE_OBJ_OBJ2_CTX),
  RFLOORDIV(Constants.__RFLOORDIV__, TYPE_OBJ_OBJ2_CTX),
  RMOD(Constants.__RMOD__, TYPE_OBJ_OBJ2_CTX),
  RDIVMOD(Constants.__RDIVMOD__, TYPE_OBJ_OBJ2_CTX),
  RPOW(Constants.__RPOW__, TYPE_OBJ_OBJ2_CTX),
  RMATMUL(Constants.__RMATMUL__, TYPE_OBJ_OBJ2_CTX),
  RAND(Constants.__RAND__, TYPE_OBJ_OBJ2_CTX),
  ROR(Constants.__ROR__, TYPE_OBJ_OBJ2_CTX),
  RXOR(Constants.__RXOR__, TYPE_OBJ_OBJ2_CTX),
  RLSHIFT(Constants.__RLSHIFT__, TYPE_OBJ_OBJ2_CTX),
  RRSHIFT(Constants.__RRSHIFT__, TYPE_OBJ_OBJ2_CTX),
  IADD(Constants.__IADD__, TYPE_OBJ_OBJ2_CTX),
  ISUB(Constants.__ISUB__, TYPE_OBJ_OBJ2_CTX),
  IMUL(Constants.__IMUL__, TYPE_OBJ_OBJ2_CTX),
  ITRUEDIV(Constants.__ITRUEDIV__, TYPE_OBJ_OBJ2_CTX),
  IFLOORDIV(Constants.__IFLOORDIV__, TYPE_OBJ_OBJ2_CTX),
  IMOD(Constants.__IMOD__, TYPE_OBJ_OBJ2_CTX),
  IPOW(Constants.__IPOW__, TYPE_OBJ_OBJ2_CTX),
  IMATMUL(Constants.__IMATMUL__, TYPE_OBJ_OBJ2_CTX),
  IAND(Constants.__IAND__, TYPE_OBJ_OBJ2_CTX),
  IOR(Constants.__IOR__, TYPE_OBJ_OBJ2_CTX),
  IXOR(Constants.__IXOR__, TYPE_OBJ_OBJ2_CTX),
  ILSHIFT(Constants.__ILSHIFT__, TYPE_OBJ_OBJ2_CTX),
  IRSHIFT(Constants.__IRSHIFT__, TYPE_OBJ_OBJ2_CTX),

  NEG(Constants.__NEG__, TYPE_OBJ_OBJ1_CTX),
  POS(Constants.__POS__, TYPE_OBJ_OBJ1_CTX),
  ABS(Constants.__ABS__, TYPE_OBJ_OBJ1_CTX),
  INVERT(Constants.__INVERT__, TYPE_OBJ_OBJ1_CTX),
  ;

  public final String name;
  public final MethodType type;
  public final MethodKind kind;

  public static @Nullable SpecialMethod find(String name) {
    return nameMap.get(name);
  }

  SpecialMethod(String name, MethodType type) {
    this(name, type, MethodKind.INSTANCE);
  }

  SpecialMethod(String name, MethodType type, MethodKind kind) {
    this.name = name;
    this.type = type;
    this.kind = kind;
  }

  private static final Map<String, SpecialMethod> nameMap;

  static {
    nameMap = new HashMap<>();
    for (SpecialMethod m : values()) {
      nameMap.put(m.name, m);
    }
  }
}
