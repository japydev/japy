package org.japy.kernel.types.cls.ext;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.IPyClassDict;
import org.japy.kernel.types.cls.IPyMetaClass;
import org.japy.kernel.types.cls.PyClassDict;
import org.japy.kernel.types.cls.PyObjectClass;
import org.japy.kernel.types.cls.PyTypeClass;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.SpecialMethodTable;
import org.japy.kernel.types.cls.builtin.IPyBuiltinClass;
import org.japy.kernel.util.Args;

public class PyExtBuiltinSubClass extends PyExtClass {
  private final IPyBuiltinClass builtinAncestor;
  private final Class<? extends IPyObj> builtinInstanceClass;
  private final PyClassDict builtinSuperDict;
  private final MethodHandle constructor;
  private SpecialMethodTable extMethodTable;

  public PyExtBuiltinSubClass(ExtClassData data, IPyMetaClass meta, PyContext ctx) {
    super(data, meta);

    this.extMethodTable = data.extMethodTable;
    this.builtinAncestor = dcheckNotNull(data.builtinAncestor);
    this.builtinInstanceClass = dcheckNotNull(data.builtinInstanceClass);

    if (Debug.ENABLED) {
      dcheck(builtinAncestor != PyObjectClass.INSTANCE);
      dcheck(!builtinAncestor.isSubClassRaw(PyTypeClass.INSTANCE));
    }

    ExtClassCache.Entry e =
        ((ExtClassCache) ctx.kernel.classCache).get(builtinAncestor, builtinInstanceClass);
    Class<?> instanceClass = e.instanceClass;
    builtinSuperDict = e.superDict;

    try {
      constructor =
          MethodHandles.lookup()
              .unreflectConstructor(
                  instanceClass.getDeclaredConstructor(
                      PyExtBuiltinSubClass.class, Args.class, PyContext.class))
              .asType(
                  MethodType.methodType(
                      IPyObj.class, PyExtBuiltinSubClass.class, Args.class, PyContext.class));
    } catch (IllegalAccessException | NoSuchMethodException ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public IPyClassDict superDict(IPyClass base) {
    return base == builtinAncestor ? builtinSuperDict : base.classDict();
  }

  @Override
  public IPyObj instantiate(Args args, PyContext ctx) {
    try {
      return (IPyObj) constructor.invokeExact(this, args, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public IPyBuiltinClass builtinAncestor() {
    return builtinAncestor;
  }

  @Override
  public Class<? extends IPyObj> builtinInstanceClass() {
    return builtinInstanceClass;
  }

  @Override
  public void validate(Validator v) {
    v.validate(builtinAncestor);
    super.validate(v);
  }

  void setExtMethod(SpecialMethod specialMethod, @Nullable FuncHandle methodHandle) {
    extMethodTable = extMethodTable.set(specialMethod, methodHandle);
  }

  public @Nullable FuncHandle getExtMethod(SpecialMethod m) {
    return extMethodTable.getRaw(m);
  }

  public @Nullable IPyObj getExtMethod(String name) {
    for (IPyClass cls : mro()) {
      if (cls.isBuiltinClass()) {
        break;
      }
      @Nullable IPyObj value = cls.classDict().get(name);
      if (value != null) {
        return value;
      }
    }
    return null;
  }
}
