package org.japy.kernel.types.cls.builtin;

import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.num.PyBool.False;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.desc.IPyDescriptorDel;
import org.japy.kernel.types.cls.desc.IPyDescriptorGet;
import org.japy.kernel.types.cls.desc.IPyDescriptorSet;
import org.japy.kernel.types.cls.desc.IPyMethodDescriptor;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxAttributeError;

abstract class PyBuiltinMethodDescriptor
    implements IPyDescriptorGet, IPyDescriptorDel, IPyDescriptorSet, IPyMethodDescriptor {

  final IPyClass objClass;
  final String name;
  final FuncHandle fh;

  protected PyBuiltinMethodDescriptor(FuncHandle fh, IPyClass objClass, String name) {
    this.fh = fh;
    this.objClass = objClass;
    this.name = name;
  }

  @Override
  public IPyObj __name__(PyContext ctx) {
    return PyStr.get(name);
  }

  @Override
  public IPyObj __objclass__(PyContext ctx) {
    return objClass;
  }

  @Override
  public IPyObj __set_name__(IPyObj cls, IPyObj name, PyContext ctx) {
    return None;
  }

  @Override
  public IPyObj __isabstractmethod__(PyContext ctx) {
    return False;
  }

  @Override
  public void delete(IPyObj instance, PyContext ctx) {
    throw new PxAttributeError(
        String.format("'%s' object attribute '%s' is read-only", objClass.name(), name), ctx);
  }

  @Override
  public void set(IPyObj instance, IPyObj value, PyContext ctx) {
    throw new PxAttributeError(
        String.format("'%s' object attribute '%s' is read-only", objClass.name(), name), ctx);
  }

  @Override
  public void validate(Validator v) {
    v.validateIfNotNull(objClass);
  }
}
