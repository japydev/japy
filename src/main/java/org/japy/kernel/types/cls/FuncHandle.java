package org.japy.kernel.types.cls;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.util.ExcUtil;

public class FuncHandle {
  public final MethodHandle handle;
  public final Object origin;

  public FuncHandle(MethodHandle handle, Object origin) {
    this.handle = handle;
    this.origin = origin;
  }

  public static FuncHandle method(Method method, MethodHandles.Lookup lookup) {
    try {
      return new FuncHandle(lookup.unreflect(method), method);
    } catch (IllegalAccessException ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  public static FuncHandle constructor(Constructor<?> constructor, MethodHandles.Lookup lookup) {
    try {
      return new FuncHandle(lookup.unreflectConstructor(constructor), constructor);
    } catch (IllegalAccessException ex) {
      throw new InternalErrorException(
          String.format(
              "constructor %s in class %s is not public",
              constructor, constructor.getDeclaringClass()),
          ex);
    }
  }

  public MethodType type() {
    return handle.type();
  }

  public FuncHandle explicitCastArguments(MethodType type) {
    return new FuncHandle(MethodHandles.explicitCastArguments(handle, type), origin);
  }

  public FuncHandle asType(MethodType type) {
    return new FuncHandle(handle.asType(type), origin);
  }

  public FuncHandle bindTo(Object obj) {
    return new FuncHandle(handle.bindTo(obj), origin);
  }
}
