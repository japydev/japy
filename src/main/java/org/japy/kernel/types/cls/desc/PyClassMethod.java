package org.japy.kernel.types.cls.desc;

import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.base.ParamKind;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyClassMethod extends PyMethodDescriptor {
  public static final IPyClass TYPE =
      new PyBuiltinClass("classmethod", PyClassMethod.class, PyBuiltinClass.REGULAR);

  public PyClassMethod(IPyObj func) {
    super(func);
  }

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(1).add("func", ParamKind.POS_ONLY).build();

  @Constructor
  @SubClassConstructor
  public PyClassMethod(Args args, PyContext ctx) {
    this(ARG_PARSER.parse1(args, "classmethod", ctx));
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj get(IPyObj instance, IPyObj owner, PyContext ctx) {
    return new PyBoundMethod(
        instance != None ? instance.type() : owner, func, dcheckNotNull(name).toString());
  }
}
