package org.japy.kernel.types.cls;

import static org.japy.infra.validation.Validator.check;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.eclipse.collections.impl.list.mutable.primitive.IntArrayList;

import org.japy.infra.coll.CollUtil;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.dict.PyMappingProxy;
import org.japy.kernel.types.coll.seq.PyList;

public abstract class PyAbstractClass implements IPyClass {
  protected String name;
  protected String qualName;
  private final IPyClass[] bases;
  private final IPyClass[] mro;
  private final PyClassDict dict;
  private final IPyRODict dictWrapper;
  private final List<WeakReference<IPyClass>> subclasses = new ArrayList<>();

  protected PyAbstractClass(String name, IPyClass parent) {
    this.name = name;
    this.qualName = name;
    this.bases = new IPyClass[] {parent};
    this.mro = CollUtil.prepend(this, parent.mro());
    this.dict = new PyClassDict();
    this.dictWrapper = new PyMappingProxy(dict);
  }

  protected PyAbstractClass(String name, IPyClass[] bases, IPyClass[] baseMro) {
    this(name, bases, baseMro, new PyClassDict());
  }

  protected PyAbstractClass(String name, IPyClass[] bases, IPyClass[] baseMro, PyClassDict dict) {
    this.name = name;
    this.qualName = name;
    this.bases = bases;
    this.mro = CollUtil.prepend(this, baseMro);
    this.dict = dict;
    this.dictWrapper = new PyMappingProxy(dict);
  }

  protected PyAbstractClass(String name, @Nullable Void root) {
    this.name = name;
    this.qualName = name;
    this.bases = IPyClass.EMPTY_CLASS_ARRAY;
    this.mro = new IPyClass[] {this};
    this.dict = new PyClassDict();
    this.dictWrapper = new PyMappingProxy(dict);
  }

  @Override
  public final String name() {
    return name;
  }

  @Override
  public final String qualName() {
    return qualName;
  }

  @Override
  public final IPyClass[] bases() {
    return bases;
  }

  @Override
  public final IPyClass[] mro() {
    return mro;
  }

  @Override
  public final IPyClassDict classDict() {
    return dict;
  }

  @Override
  public final IPyRODict classDictWrapper() {
    return dictWrapper;
  }

  @Override
  public final synchronized void addSubclass(IPyClass subclass) {
    subclasses.add(new WeakReference<>(subclass));
  }

  @Override
  public synchronized PyList subclasses(PyContext ctx) {
    List<IPyObj> result = new ArrayList<>();
    @Var
    @Nullable
    IntArrayList nulls = null;
    for (int i = 0, c = subclasses.size(); i != c; ++i) {
      @Nullable IPyClass cls = subclasses.get(i).get();
      if (cls != null) {
        result.add(cls);
      } else {
        if (nulls == null) {
          nulls = new IntArrayList();
          nulls.add(i);
        }
      }
    }
    if (nulls != null) {
      for (int i = nulls.size() - 1; i >= 0; --i) {
        subclasses.remove(nulls.get(i));
      }
    }
    return PyList.take(result);
  }

  @Override
  public void validate(Validator v) {
    check(mro.length != 0, "empty mro");
    check(mro[0] == this, "bad mro");
    v.validate(mro);
    v.validate(dict);
  }

  @Override
  public String toString() {
    return "<class '" + name + "'>";
  }
}
