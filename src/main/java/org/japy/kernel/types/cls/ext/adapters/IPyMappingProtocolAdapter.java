package org.japy.kernel.types.cls.ext.adapters;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Adapter;
import org.japy.kernel.types.annotations.AdapterMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.ProtocolSuperMethod;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.ext.IPyBuiltinSubClassInstance;
import org.japy.kernel.types.coll.dict.IPyMappingProtocol;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.util.Constants;

@Adapter(IPyMappingProtocol.class)
public interface IPyMappingProtocolAdapter extends IPyBuiltinSubClassInstance {
  void baseSetItem(IPyObj key, IPyObj value, PyContext ctx);

  void baseDelItem(IPyObj key, PyContext ctx);

  IPyObj basePop(IPyObj key, PyContext ctx);

  IPyObj basePop(IPyObj key, IPyObj dflt, PyContext ctx);

  @ProtocolSuperMethod(Constants.SETDEFAULT)
  IPyObj baseSetdefault(
      @Param("key") IPyObj key,
      @Param(value = "default", dflt = DEFAULT_SENTINEL) IPyObj dflt,
      PyContext ctx);

  @AdapterMethod
  default void thisSetItem(IPyObj key, IPyObj value, PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.SETITEM);
    if (h == DELETED_METHOD) {
      throw PxException.typeError("__setitem__ is deleted", ctx);
    }
    if (h == null) {
      baseSetItem(key, value, ctx);
      return;
    }
    try {
      h.handle.invokeExact((IPyObj) this, key, value, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @AdapterMethod
  default void thisDelItem(IPyObj key, PyContext ctx) {
    @Nullable FuncHandle h = getExtMethod(SpecialMethod.DELITEM);
    if (h == DELETED_METHOD) {
      throw PxException.typeError("__delitem__ is deleted", ctx);
    }
    if (h == null) {
      baseDelItem(key, ctx);
      return;
    }
    try {
      h.handle.invokeExact((IPyObj) this, key, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @AdapterMethod
  default IPyObj thisPop(IPyObj key, PyContext ctx) {
    @Nullable IPyObj pop = getExtMethod(Constants.POP);
    if (pop == null) {
      return basePop(key, ctx);
    }
    return FuncLib.call(pop, this, key, ctx);
  }

  @AdapterMethod
  default IPyObj thisPop(IPyObj key, IPyObj dflt, PyContext ctx) {
    @Nullable IPyObj pop = getExtMethod(Constants.POP);
    if (pop == null) {
      return basePop(key, dflt, ctx);
    }
    return FuncLib.call(pop, this, key, dflt, ctx);
  }

  @AdapterMethod
  default IPyObj thisSetdefault(IPyObj key, IPyObj dflt, PyContext ctx) {
    @Nullable IPyObj setdefault = getExtMethod(Constants.SETDEFAULT);
    if (setdefault == null) {
      return baseSetdefault(key, dflt, ctx);
    }
    return FuncLib.call(setdefault, this, key, dflt, ctx);
  }

  @ProtocolSuperMethod(Constants.__SETITEM__)
  default IPyObj superSetItem(
      @Param("key") IPyObj key, @Param("value") IPyObj value, PyContext ctx) {
    baseSetItem(key, value, ctx);
    return None;
  }

  @ProtocolSuperMethod(Constants.__DELITEM__)
  default IPyObj superDelItem(@Param("key") IPyObj key, PyContext ctx) {
    baseDelItem(key, ctx);
    return None;
  }

  @ProtocolSuperMethod(Constants.POP)
  default IPyObj superPop(
      @Param("key") IPyObj key,
      @Param(value = "default", dflt = DEFAULT_SENTINEL) IPyObj dflt,
      PyContext ctx) {
    if (dflt != Sentinel) {
      return basePop(key, dflt, ctx);
    } else {
      return basePop(key, ctx);
    }
  }
}
