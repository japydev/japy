package org.japy.kernel.types.cls;

import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.ClassMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.annotations.SpecialStaticMethod;
import org.japy.kernel.types.cls.builtin.BuiltinClassLib;
import org.japy.kernel.types.cls.builtin.IPyBuiltinClass;
import org.japy.kernel.types.cls.builtin.PyAbstractBuiltinClass;
import org.japy.kernel.types.cls.ext.IPyExtClass;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.PyObject;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

public final class PyObjectClass extends PyAbstractBuiltinClass {
  public static final PyObjectClass INSTANCE = new PyObjectClass();

  private final SpecialMethodTable originalMethodTable;
  private final SpecialMethodTable methodTable;

  private PyObjectClass() {
    super("object", (Void) null);
    BuiltinClassLib.ClassData classData =
        BuiltinClassLib.initializeBuiltin(
            this,
            PyObject.class,
            MethodHandles.lookup(),
            BuiltinClassLib.INSPECT_CLASS | BuiltinClassLib.INSPECT_SUPERS);
    originalMethodTable = classData.original;
    methodTable = classData.complete;
    BuiltinClassLib.finishClassSetup(this);
  }

  @Override
  public SpecialMethodTable methodTable() {
    return methodTable;
  }

  @Override
  public SpecialMethodTable originalMethodTable() {
    return originalMethodTable;
  }

  @Override
  public IPyObj instantiate(Args args, PyContext ctx) {
    ArgParser.verifyNoArgs(args, "object", ctx);
    return new PyObject();
  }

  @Override
  protected IPyObj getDoc() {
    return PyStr.ucs2(
        "object()\n--\n\n"
            + "The base class of the class hierarchy.\n\n"
            + "When called, it accepts no arguments and returns a new featureless\n"
            + "instance that has no instance attributes and cannot be given any.\n");
  }

  @Override
  public Class<? extends IPyObj> instanceClass() {
    throw InternalErrorException.notReached();
  }

  @Override
  public boolean isSubclassable() {
    throw InternalErrorException.notReached();
  }

  @SpecialStaticMethod
  public static IPyObj __new__(Args args, PyContext ctx) {
    if (args.posCount() == 0) {
      throw PxException.typeError("expected a class argument", ctx);
    }

    IPyClass cls = ArgLib.checkArgClass("object.__new__", "cls", args.getPos(0), ctx);

    if (cls instanceof IPyExtClass && ((IPyExtClass) cls).isAbstract()) {
      throw throwAbstractClassError((IPyExtClass) cls, ctx);
    }

    if (cls instanceof IPyExtClass) {
      return ((IPyExtClass) cls).instantiate(args.tail(), ctx);
    } else {
      return ((IPyBuiltinClass) cls).instantiate(args.tail(), ctx);
    }
  }

  private static PxException throwAbstractClassError(IPyExtClass cls, PyContext ctx) {
    IPyObj abstractMethods = dcheckNotNull(cls.getAbstractMethods(ctx));
    List<String> names = new ArrayList<>();
    for (IPyObj m : new JavaIterable(abstractMethods, ctx)) {
      names.add(m.toString());
    }
    names.sort(String::compareTo);
    throw PxException.typeError(
        String.format(
            "Can't instantiate abstract class %s with abstract method%s %s",
            cls.name(), names.size() > 1 ? "s" : "", String.join(", ", names)),
        ctx);
  }

  @SpecialInstanceMethod
  public static IPyObj __init__(IPyObj self, Args args, PyContext ctx) {
    // https://github.com/python/cpython/blob/main/Objects/typeobject.c#L4460
    if (!args.isEmpty()) {
      if (self.specialMethod(SpecialMethod.INIT)
          != INSTANCE.methodTable.getRaw(SpecialMethod.INIT)) {
        throw PxException.typeError(
            "object.__init__() takes exactly one argument (the instance to initialize)", ctx);
      }
      if (self.specialMethod(SpecialMethod.NEW) == INSTANCE.methodTable.getRaw(SpecialMethod.NEW)) {
        throw PxException.typeError(
            self.typeName() + ".__init__() takes exactly one argument (the instance to initialize)",
            ctx);
      }
    }
    return None;
  }

  @ClassMethod
  public static IPyObj __init_subclass__(IPyClass cls, Args args, PyContext ctx) {
    if (!args.isEmpty()
        && ObjLib.findAttributeInClassHierarchy(cls, Constants.__INIT_SUBCLASS__)
            != INSTANCE.classDict().get(Constants.__INIT_SUBCLASS__)) {
      throw PxException.typeError("object.__init_subclass__() takes no arguments", ctx);
    }
    return None;
  }
}
