package org.japy.kernel.types.cls.ext;

import static org.japy.infra.validation.Debug.dcheck;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.IPyMetaClass;
import org.japy.kernel.types.cls.PyObjectClass;
import org.japy.kernel.types.cls.builtin.IPyBuiltinClass;
import org.japy.kernel.util.Args;

public class PyPureExtClass extends PyExtClass {
  public PyPureExtClass(ExtClassData data, IPyMetaClass meta) {
    super(data, meta);

    if (Debug.ENABLED) {
      dcheck(data.builtinAncestor == null);
      dcheck(data.builtinInstanceClass == null);
      for (IPyClass cls : data.bases) {
        dcheck(
            cls == PyObjectClass.INSTANCE
                || (cls instanceof IPyExtClass && ((IPyExtClass) cls).builtinAncestor() == null));
      }
    }
  }

  @Override
  public IPyObj instantiate(Args args, PyContext ctx) {
    return new PyPureExtObj(this);
  }

  @Override
  public @Nullable IPyBuiltinClass builtinAncestor() {
    return null;
  }

  @Override
  public @Nullable Class<? extends IPyObj> builtinInstanceClass() {
    return null;
  }
}
