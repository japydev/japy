package org.japy.kernel.types.cls.builtin;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;
import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.annotation.Annotation;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.coll.CollUtil;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.ConstOps;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.ClassMethod;
import org.japy.kernel.types.annotations.DeletedSpecialMethod;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceAttributeSetter;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.SpecialClassMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.annotations.SpecialStaticMethod;
import org.japy.kernel.types.annotations.StaticMethod;
import org.japy.kernel.types.cls.ClassLib;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.IPyClassDict;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.SpecialMethodTable;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.func.FuncAdapterLib;
import org.japy.kernel.types.func.PyBuiltinFunc;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

public final class BuiltinClassLib {
  private BuiltinClassLib() {}

  private static final MethodHandles.Lookup MY_LOOKUP = MethodHandles.lookup();

  public static final int INSPECT_SUPERS = 1;
  public static final int INSPECT_CLASS = 2;
  public static final int ADD_NOOP_INIT = 4;

  public static ClassData initializeBuiltin(
      IPyBuiltinClass cls,
      @Nullable Class<? extends IPyObj> instanceClass,
      MethodHandles.Lookup lookup,
      int flags) {
    MethodCollector collector = new MethodCollector(cls, instanceClass, lookup);
    try {
      collector.collect(flags);
    } catch (Throwable ex) {
      throw new InternalErrorException("builtin class " + cls + ": " + ex.getMessage(), ex);
    }
    return populateMethodTables(cls, collector);
  }

  public static ClassData initializeBuiltin(IPyBuiltinClass klass, MethodHandles.Lookup lookup) {
    MethodCollector collector = new MethodCollector(klass, null, lookup);
    try {
      collector.collect(INSPECT_CLASS);
    } catch (IllegalAccessException | NoSuchMethodException ex) {
      throw ExcUtil.rethrow(ex);
    }
    return populateMethodTables(klass, collector);
  }

  public static void finishClassSetup(IPyBuiltinClass cls) {
    ClassLib.finishClassSetup(cls, Args.EMPTY, ConstOps.CONTEXT);
  }

  public static IPyObj wrapInstanceMethod(
      IPyClass instanceClass, String name, Method method, MethodHandles.Lookup lookup) {
    return new PyBuiltinInstanceMethod(
        FuncAdapterLib.makeInstanceMethod(method, name, lookup), instanceClass, name);
  }

  public static class ClassData {
    public final SpecialMethodTable original;
    public final SpecialMethodTable complete;

    public ClassData(SpecialMethodTable original, SpecialMethodTable complete) {
      this.original = original;
      this.complete = complete;
    }
  }

  private static class MethodCollector {
    private final IPyBuiltinClass pyClass;
    private final @Nullable Class<? extends IPyObj> pyInstanceClass;
    private final MethodHandles.Lookup lookup;
    private final IPyClassDict classDict;
    private final Set<Class<?>> seenClasses = new HashSet<>();
    private final Map<String, FuncHandle> specialHandleMap = new HashMap<>();

    MethodCollector(
        IPyBuiltinClass pyClass,
        @Nullable Class<? extends IPyObj> instanceClass,
        MethodHandles.Lookup lookup) {
      this.pyClass = pyClass;
      this.pyInstanceClass = instanceClass;
      this.lookup = lookup;
      this.classDict = pyClass.classDict();
    }

    private void addClassAttr(String name, IPyObj meth) {
      if (classDict.contains(name)) {
        throw new IllegalArgumentException(
            String.format("duplicate method %s in class %s", name, pyInstanceClass));
      }
      classDict.set(name, meth);
    }

    private void addFieldMethods(Class<?> klass)
        throws IllegalAccessException, NoSuchMethodException {
      for (Field field : klass.getDeclaredFields()) {
        @Nullable InstanceAttribute ann = field.getAnnotation(InstanceAttribute.class);
        if (ann == null) {
          continue;
        }
        String name = ann.value().isEmpty() ? field.getName() : ann.value();
        addClassAttr(name, makeAttribute(field, name, ann));
      }
    }

    private interface IAttrGetter {
      IPyObj get(IPyObj obj, PyContext ctx);
    }

    private interface IAttrSetter {
      void set(IPyObj obj, IPyObj value, PyContext ctx);
    }

    private interface IAttrDeleter {
      void del(IPyObj obj, PyContext ctx);
    }

    private IPyObj makeAttribute(Field field, String name, InstanceAttribute ann)
        throws IllegalAccessException, NoSuchMethodException {
      if (Modifier.isStatic(field.getModifiers())) {
        throw new InternalErrorException(
            String.format("field %s.%s is static", field.getDeclaringClass(), field.getName()));
      }

      MethodHandle fieldGetter =
          lookup.unreflectGetter(field).asType(MethodType.methodType(IPyObj.class, IPyObj.class));
      @Nullable MethodHandle fieldSetter;
      if (ann.writable() || ann.deletable()) {
        fieldSetter =
            lookup
                .unreflectSetter(field)
                .asType(MethodType.methodType(void.class, IPyObj.class, IPyObj.class));
      } else {
        fieldSetter = null;
      }

      IAttrGetter getterLambda =
          (obj, ctx) -> {
            @Nullable IPyObj v;
            try {
              v = (IPyObj) fieldGetter.invokeExact(obj);
            } catch (Throwable ex) {
              throw ExcUtil.rethrow(ex);
            }
            if (v == null) {
              throw new PxAttributeError(name, ctx);
            }
            return v;
          };
      FuncHandle getter =
          new FuncHandle(
              lookup
                  .findVirtual(
                      getterLambda.getClass(), "get", PyInstanceAttribute.METHOD_TYPE_GETTER)
                  .bindTo(getterLambda),
              field);

      @Nullable FuncHandle setter;
      if (ann.writable()) {
        dcheckNotNull(fieldSetter);
        IAttrSetter setterLambda =
            (obj, value, ctx) -> {
              try {
                fieldSetter.invokeExact(obj, value);
              } catch (Throwable ex) {
                throw ExcUtil.rethrow(ex);
              }
            };
        setter =
            new FuncHandle(
                lookup
                    .findVirtual(
                        setterLambda.getClass(), "set", PyInstanceAttribute.METHOD_TYPE_SETTER)
                    .bindTo(setterLambda),
                field);
      } else {
        setter = null;
      }

      @Nullable FuncHandle deleter;
      if (ann.deletable()) {
        dcheckNotNull(fieldSetter);
        IAttrDeleter deleterLambda =
            (obj, ctx) -> {
              try {
                fieldSetter.invokeExact(obj, (IPyObj) null);
              } catch (Throwable ex) {
                throw ExcUtil.rethrow(ex);
              }
            };
        deleter =
            new FuncHandle(
                lookup
                    .findVirtual(
                        deleterLambda.getClass(), "del", PyInstanceAttribute.METHOD_TYPE_DELETER)
                    .bindTo(deleterLambda),
                field);
      } else {
        deleter = null;
      }

      return new PyInstanceAttribute(pyClass, name, getter, setter, deleter);
    }

    private void makeAttributeGetter(String name, FuncHandle fh) {
      @Nullable IPyObj attr = classDict.get(name);
      if (attr == null) {
        addClassAttr(name, new PyInstanceAttribute(pyClass, name, fh, null, null));
        return;
      }

      if (!(attr instanceof PyInstanceAttribute)) {
        throw new InternalErrorException("expected a PyInstanceAttribute, got " + attr);
      }

      dcheck(((PyInstanceAttribute) attr).getter() == null);
      ((PyInstanceAttribute) attr).setGetter(fh);
    }

    private void makeAttributeSetter(String name, FuncHandle fh) {
      @Nullable IPyObj attr = classDict.get(name);
      if (attr == null) {
        addClassAttr(name, new PyInstanceAttribute(pyClass, name, null, fh, null));
        return;
      }

      if (!(attr instanceof PyInstanceAttribute)) {
        throw new InternalErrorException("expected a PyInstanceAttribute, got " + attr);
      }

      dcheck(((PyInstanceAttribute) attr).setter() == null);
      ((PyInstanceAttribute) attr).setSetter(fh);
    }

    private static String getName(Annotation annotation) {
      if (annotation instanceof InstanceMethod) {
        return ((InstanceMethod) annotation).value();
      } else if (annotation instanceof SpecialInstanceMethod) {
        return ((SpecialInstanceMethod) annotation).value();
      } else if (annotation instanceof ClassMethod) {
        return ((ClassMethod) annotation).value();
      } else if (annotation instanceof SpecialClassMethod) {
        return ((SpecialClassMethod) annotation).value();
      } else if (annotation instanceof StaticMethod) {
        return ((StaticMethod) annotation).value();
      } else if (annotation instanceof SpecialStaticMethod) {
        return ((SpecialStaticMethod) annotation).value();
      } else if (annotation instanceof DeletedSpecialMethod) {
        return ((DeletedSpecialMethod) annotation).value();
      } else if (annotation instanceof InstanceAttribute) {
        return ((InstanceAttribute) annotation).value();
      } else if (annotation instanceof InstanceAttributeSetter) {
        return ((InstanceAttributeSetter) annotation).value();
      } else {
        throw InternalErrorException.notReached(annotation.toString());
      }
    }

    private static String getName(Method method, Annotation annotation) {
      String name = getName(annotation);
      return name.isEmpty() ? method.getName() : name;
    }

    private void addDeletedSpecialMethod(Method method, Annotation annotation) {
      String name = getName(method, annotation);
      addClassAttr(name, None);
      specialHandleMap.put(name, DELETED_METHOD);

      if (Debug.ENABLED) {
        if (SpecialMethod.find(name) == null) {
          throw new InternalErrorException(
              String.format(
                  "method annotated as special, but there isn't corresponding special method: %s.%s",
                  method.getDeclaringClass(), method.getName()));
        }
      }
    }

    private void addClassAttr(
        String name, Method method, boolean special, Function<String, IPyObj> makeMethodObj) {
      addClassAttr(name, makeMethodObj.apply(name));

      if (Debug.ENABLED) {
        if (special) {
          if (SpecialMethod.find(name) == null) {
            throw new InternalErrorException(
                String.format(
                    "method annotated as special, but there isn't corresponding special method: %s.%s",
                    method.getDeclaringClass(), method.getName()));
          }
        } else {
          if (SpecialMethod.find(name) != null) {
            throw new InternalErrorException(
                String.format(
                    "special method is not annotated as special: %s.%s",
                    method.getDeclaringClass(), method.getName()));
          }
        }
      }

      if (special) {
        specialHandleMap.put(name, FuncHandle.method(method, lookup));
      }
    }

    /**
     * __init__ method which doesn't do anything. It's defined for subclasses of built-in classes,
     * so that they can call super().__init__().
     */
    private static IPyObj noOpInit(
        @SuppressWarnings("unused") IPyObj self,
        @SuppressWarnings("unused") Args args,
        @SuppressWarnings("unused") PyContext ctx) {
      return None;
    }

    private void addNoOpInitMethod() {
      if (classDict.contains(Constants.__INIT__)) {
        return;
      }

      MethodHandle mh;
      try {
        mh =
            MY_LOOKUP.findStatic(
                MethodCollector.class, "noOpInit", PyBuiltinInstanceMethod.METHOD_TYPE);
      } catch (NoSuchMethodException | IllegalAccessException ex) {
        throw ExcUtil.rethrow(ex);
      }

      FuncHandle fh = new FuncHandle(mh, BuiltinClassLib.class);
      IPyObj init = new PyBuiltinInstanceMethod(fh, pyClass, Constants.__INIT__);
      addClassAttr(Constants.__INIT__, init);
      specialHandleMap.put(Constants.__INIT__, fh);
    }

    private void addMethodMethods(Class<?> klass) {
      for (Method method : klass.getDeclaredMethods()) {
        @Nullable InstanceMethod imAnn = method.getAnnotation(InstanceMethod.class);
        if (imAnn != null) {
          String pyName = getName(method, imAnn);
          addClassAttr(
              pyName,
              method,
              false,
              name ->
                  new PyBuiltinInstanceMethod(
                      FuncAdapterLib.makeInstanceMethod(method, name, lookup), pyClass, pyName));
          continue;
        }

        @Nullable SpecialInstanceMethod simAnn = method.getAnnotation(SpecialInstanceMethod.class);
        if (simAnn != null) {
          String pyName = getName(method, simAnn);
          addClassAttr(
              pyName,
              method,
              true,
              name ->
                  new PyBuiltinInstanceMethod(
                      FuncAdapterLib.makeInstanceMethod(method, name, lookup), pyClass, pyName));
          continue;
        }

        @Nullable ClassMethod cmAnn = method.getAnnotation(ClassMethod.class);
        if (cmAnn != null) {
          String pyName = getName(method, cmAnn);
          addClassAttr(
              pyName,
              method,
              false,
              name ->
                  new PyBuiltinClassMethod(
                      FuncAdapterLib.makeClassMethod(method, name, lookup), pyClass, pyName));
          continue;
        }

        @Nullable SpecialClassMethod scmAnn = method.getAnnotation(SpecialClassMethod.class);
        if (scmAnn != null) {
          String pyName = getName(method, scmAnn);
          addClassAttr(
              pyName,
              method,
              true,
              name ->
                  new PyBuiltinClassMethod(
                      FuncAdapterLib.makeClassMethod(method, name, lookup), pyClass, pyName));
          continue;
        }

        @Nullable StaticMethod smAnn = method.getAnnotation(StaticMethod.class);
        if (smAnn != null) {
          String pyName = getName(method, smAnn);
          addClassAttr(
              pyName,
              method,
              false,
              name ->
                  new PyBuiltinFunc(FuncAdapterLib.makeStaticMethod(method, name, lookup), pyName));
          continue;
        }

        @Nullable SpecialStaticMethod ssmAnn = method.getAnnotation(SpecialStaticMethod.class);
        if (ssmAnn != null) {
          String pyName = getName(method, ssmAnn);
          addClassAttr(
              pyName,
              method,
              true,
              name ->
                  new PyBuiltinFunc(FuncAdapterLib.makeStaticMethod(method, name, lookup), pyName));
          continue;
        }

        @Nullable InstanceAttribute iaAnn = method.getAnnotation(InstanceAttribute.class);
        if (iaAnn != null) {
          String name = getName(method, iaAnn);
          makeAttributeGetter(name, FuncAdapterLib.makeInstanceAttributeGetter(method, lookup));
          continue;
        }

        @Nullable
        InstanceAttributeSetter iasAnn = method.getAnnotation(InstanceAttributeSetter.class);
        if (iasAnn != null) {
          String name = getName(method, iasAnn);
          makeAttributeSetter(name, FuncAdapterLib.makeInstanceAttributeSetter(method, lookup));
          continue;
        }

        @Nullable DeletedSpecialMethod dsmAnn = method.getAnnotation(DeletedSpecialMethod.class);
        if (dsmAnn != null) {
          addDeletedSpecialMethod(method, dsmAnn);
          //noinspection UnnecessaryContinue
          continue;
        }
      }
    }

    private void inspectAnnotatedMembers(Class<?> klass, boolean isInstanceClass)
        throws IllegalAccessException, NoSuchMethodException {
      if (isInstanceClass) {
        addFieldMethods(klass);
      }
      addMethodMethods(klass);
    }

    private void addInstanceClassMethods(Class<? extends IPyObj> klass) {
      if (seenClasses.contains(klass)) {
        return;
      }
      seenClasses.add(klass);
      try {
        inspectAnnotatedMembers(klass, true);
      } catch (Throwable ex) {
        throw new InternalErrorException("instance class " + klass + ": " + ex.getMessage(), ex);
      }
    }

    @SuppressWarnings("unchecked")
    private void addInstanceClassMethods(Class<? extends IPyObj> klass, boolean inspectSupers) {
      addInstanceClassMethods(klass);

      if (inspectSupers) {
        @Nullable Class<?> superClass = klass.getSuperclass();
        if (superClass != null && IPyObj.class.isAssignableFrom(superClass)) {
          addInstanceClassMethods((Class<? extends IPyObj>) superClass, true);
        }

        for (Class<?> iface : klass.getInterfaces()) {
          if (IPyObj.class.isAssignableFrom(iface)) {
            addInstanceClassMethods((Class<? extends IPyObj>) iface, true);
          }
        }
      }
    }

    private void addClassMethods() throws IllegalAccessException, NoSuchMethodException {
      inspectAnnotatedMembers(pyClass.getClass(), false);
    }

    private void collect(int flags) throws IllegalAccessException, NoSuchMethodException {
      dcheck((flags & INSPECT_SUPERS) == 0 || pyInstanceClass != null);
      if (pyInstanceClass != null) {
        addInstanceClassMethods(pyInstanceClass, (flags & INSPECT_SUPERS) != 0);
      }

      if ((flags & INSPECT_CLASS) != 0) {
        addClassMethods();
      }

      if ((flags & ADD_NOOP_INIT) != 0) {
        addNoOpInitMethod();
      }
    }
  }

  private static void copyFromParent(SpecialMethodTable.Builder self, SpecialMethodTable parent) {
    for (SpecialMethod m : SpecialMethod.values()) {
      if (self.get(m) == null) {
        self.set(m, parent.getRaw(m));
      }
    }
  }

  private static void addSpecialMethod(
      SpecialMethod m,
      IPyClass klass,
      MethodCollector collector,
      SpecialMethodTable.Builder builder) {
    @Var
    @Nullable
    FuncHandle fh = collector.specialHandleMap.get(m.name);
    if (fh == null) {
      return;
    }
    if (fh == DELETED_METHOD) {
      builder.set(m, fh);
      return;
    }

    @Var MethodType t = fh.type();

    Class<?> firstParam = t.parameterType(0);
    if (m.type.parameterType(0) == IPyObj.class) {
      if (firstParam != IPyObj.class && IPyObj.class.isAssignableFrom(firstParam)) {
        t = fh.type().changeParameterType(0, IPyObj.class);
        fh = fh.explicitCastArguments(t);
      }
    } else if (m.type.parameterType(0) == IPyClass.class) {
      if (firstParam != IPyClass.class && IPyClass.class.isAssignableFrom(firstParam)) {
        t = fh.type().changeParameterType(0, IPyClass.class);
        fh = fh.explicitCastArguments(t);
      }
    }

    if (t.returnType() != m.type.returnType()) {
      t = t.changeReturnType(m.type.returnType());
      fh = FuncAdapterLib.changeReturnType(fh, m);
    }

    if (!t.equals(m.type) && m == SpecialMethod.POW) {
      fh = FuncAdapterLib.adaptTernaryPow(fh);
      t = fh.type();
    }

    if (!t.equals(m.type)) {
      throw new InternalErrorException(
          String.format(
              "bad type of method %s in class '%s'. Expected %s, got %s",
              m.name, klass.name(), m.type, fh.type()));
    }

    builder.set(m, fh);
  }

  private static ClassData populateMethodTables(IPyClass klass, MethodCollector collector) {
    SpecialMethodTable.Builder origBuilder = SpecialMethodTable.builder();

    for (SpecialMethod m : SpecialMethod.values()) {
      addSpecialMethod(m, klass, collector, origBuilder);
    }

    SpecialMethodTable original = origBuilder.build();

    IPyClass[] mro = klass.mro();
    if (mro.length != 1) {
      SpecialMethodTable complete;
      if (klass.bases().length != 1) {
        complete = ClassLib.addMethodsFromMro(original, klass.bases(), CollUtil.tail(mro));
      } else {
        SpecialMethodTable.Builder builder = original.copy();
        copyFromParent(builder, mro[1].methodTable());
        complete = builder.build();
      }
      return new ClassData(original, complete);
    } else {
      return new ClassData(original, original);
    }
  }
}
