package org.japy.kernel.types.cls.builtin;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.IPyFunc;
import org.japy.kernel.util.Args;

public class PyBuiltinInstanceMethod extends PyBuiltinMethodDescriptor implements IPyFunc {
  // IPyObj foo(IPyObj self, Args args, PyContext ctx)
  static final MethodType METHOD_TYPE =
      MethodType.methodType(IPyObj.class, IPyObj.class, Args.class, PyContext.class);

  PyBuiltinInstanceMethod(FuncHandle fh, IPyClass objClass, String name) {
    super(fh, objClass, name);

    if (Debug.ENABLED) {
      dcheck(fh.type().parameterCount() != 0);
      dcheck(IPyObj.class.isAssignableFrom(fh.type().parameterType(0)));
      dcheck(fh.type().changeParameterType(0, IPyObj.class).equals(METHOD_TYPE));
    }
  }

  private static class TYPE_HOLDER {
    private static final IPyClass TYPE =
        new PyBuiltinClass(
            "builtininstancemethod",
            PyBuiltinInstanceMethod.class,
            MethodHandles.lookup(),
            PyBuiltinClass.INTERNAL);
  }

  @Override
  public IPyClass type() {
    return TYPE_HOLDER.TYPE;
  }

  @Override
  public IPyObj get(IPyObj instance, IPyObj owner, PyContext ctx) {
    return instance == None ? this : new PyBoundBuiltinMethod(this, instance);
  }

  @Override
  public IPyObj call(IPyObj arg1, PyContext ctx) {
    try {
      return (IPyObj) fh.handle.invokeExact(arg1, Args.of(), ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public IPyObj call(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    try {
      return (IPyObj) fh.handle.invokeExact(arg1, Args.of(arg2), ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public IPyObj call(IPyObj arg1, IPyObj arg2, IPyObj arg3, PyContext ctx) {
    try {
      return (IPyObj) fh.handle.invokeExact(arg1, Args.of(arg2, arg3), ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public IPyObj call(Args args, PyContext ctx) {
    if (args.posCount() == 0) {
      throw PxException.typeError("missing self argument", ctx);
    }
    try {
      return (IPyObj) fh.handle.invokeExact(args.getPos(0), args.tail(), ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }
}
