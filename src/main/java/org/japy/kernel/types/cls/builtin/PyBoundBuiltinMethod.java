package org.japy.kernel.types.cls.builtin;

import static org.japy.infra.validation.Debug.dcheck;

import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.func.IPyFunc;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.util.Args;

class PyBoundBuiltinMethod implements IPyFunc, IPyNoValidationObj, IPyObjWithRepr {
  private static final IPyClass TYPE =
      new PyBuiltinClass("builtinmethod", PyBoundBuiltinMethod.class, PyBuiltinClass.INTERNAL);

  private final FuncHandle fh;
  private final PyBuiltinMethodDescriptor desc;
  private final IPyObj self;
  private final boolean isClassMethod;

  private PyBoundBuiltinMethod(PyBuiltinMethodDescriptor desc, IPyObj self, boolean isClassMethod) {
    this.fh = desc.fh.bindTo(self);
    this.desc = desc;
    this.self = self;
    this.isClassMethod = isClassMethod;

    if (Debug.ENABLED) {
      dcheck(fh.type().equals(IPyFunc.METHOD_TYPE));
    }
  }

  PyBoundBuiltinMethod(PyBuiltinClassMethod method, IPyClass cls) {
    this(method, cls, true);
  }

  PyBoundBuiltinMethod(PyBuiltinInstanceMethod method, IPyObj obj) {
    this(method, obj, false);
  }

  @InstanceAttribute
  IPyObj __objclass__(PyContext ctx) {
    return desc.objClass;
  }

  @InstanceAttribute
  IPyObj __self__(PyContext ctx) {
    return self;
  }

  @InstanceAttribute
  public IPyObj __name__(PyContext ctx) {
    return PyStr.get(desc.name);
  }

  @Override
  public IPyObj call(Args args, PyContext ctx) {
    try {
      return (IPyObj) fh.handle.invokeExact(args, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public PyStr repr(PyContext ctx) {
    return PyStr.get(
        String.format(
            "<bound %s method %s.%s>",
            isClassMethod ? "class" : "instance", desc.objClass.name(), desc.name));
  }
}
