package org.japy.kernel.types.cls.ext;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;

public interface IPyBuiltinSubClassInstance extends IPyObj {
  default @Nullable FuncHandle getExtMethod(SpecialMethod m) {
    return ((PyExtBuiltinSubClass) type()).getExtMethod(m);
  }

  default @Nullable IPyObj getExtMethod(String name) {
    return ((PyExtBuiltinSubClass) type()).getExtMethod(name);
  }
}
