package org.japy.kernel.types.cls.desc;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;

public interface IPyDescriptorGet extends IPyDescriptor {
  IPyObj get(IPyObj instance, IPyObj owner, PyContext ctx);

  @SpecialInstanceMethod
  default IPyObj __get__(
      @Param("instance") IPyObj instance,
      @Param(value = "owner", dflt = DEFAULT_NONE) IPyObj owner,
      PyContext ctx) {
    return get(instance, owner, ctx);
  }
}
