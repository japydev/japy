package org.japy.kernel.types.cls.ext;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.PyClassDict;
import org.japy.kernel.types.cls.SpecialMethodTable;
import org.japy.kernel.types.cls.builtin.IPyBuiltinClass;

public class ExtClassData {
  public final String name;
  public final PyClassDict classDict;
  public final @Nullable IPyBuiltinClass builtinAncestor;
  public final @Nullable Class<? extends IPyObj> builtinInstanceClass;
  public final IPyClass[] baseMro;
  public final IPyClass[] bases;
  public final SpecialMethodTable completeMethodTable;
  public final SpecialMethodTable originalMethodTable;
  public final SpecialMethodTable extMethodTable;

  private ExtClassData(Builder builder) {
    name = dcheckNotNull(builder.name);
    classDict = dcheckNotNull(builder.classDict);
    builtinAncestor = builder.builtinAncestor;
    builtinInstanceClass = builder.builtinInstanceClass;
    baseMro = dcheckNotNull(builder.baseMro);
    bases = dcheckNotNull(builder.bases);
    completeMethodTable = dcheckNotNull(builder.completeMethodTable);
    originalMethodTable = dcheckNotNull(builder.originalMethodTable);
    extMethodTable = dcheckNotNull(builder.extMethodTable);
  }

  public static Builder builder() {
    return new Builder();
  }

  @CanIgnoreReturnValue
  public static class Builder {
    private @Nullable String name;
    private @Nullable PyClassDict classDict;
    private @Nullable IPyBuiltinClass builtinAncestor;
    private @Nullable Class<? extends IPyObj> builtinInstanceClass;
    private IPyClass @Nullable [] baseMro;
    private IPyClass @Nullable [] bases;
    private @Nullable SpecialMethodTable completeMethodTable;
    private @Nullable SpecialMethodTable originalMethodTable;
    private @Nullable SpecialMethodTable extMethodTable;

    @CheckReturnValue
    public ExtClassData build() {
      return new ExtClassData(this);
    }

    public Builder name(String name) {
      this.name = name;
      return this;
    }

    public Builder classDict(PyClassDict classDict) {
      this.classDict = classDict;
      return this;
    }

    public Builder builtinAncestor(@Nullable IPyBuiltinClass builtinAncestor) {
      this.builtinAncestor = builtinAncestor;
      return this;
    }

    public Builder builtinInstanceClass(@Nullable Class<? extends IPyObj> builtinInstanceClass) {
      this.builtinInstanceClass = builtinInstanceClass;
      return this;
    }

    public Builder baseMro(IPyClass[] baseMro) {
      this.baseMro = baseMro;
      return this;
    }

    public Builder bases(IPyClass[] bases) {
      this.bases = bases;
      return this;
    }

    public Builder completeMethodTable(SpecialMethodTable completeMethodTable) {
      this.completeMethodTable = completeMethodTable;
      return this;
    }

    public Builder originalMethodTable(SpecialMethodTable originalMethodTable) {
      this.originalMethodTable = originalMethodTable;
      return this;
    }

    public Builder extMethodTable(SpecialMethodTable extMethodTable) {
      this.extMethodTable = extMethodTable;
      return this;
    }
  }
}
