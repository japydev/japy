package org.japy.kernel.types.java;

import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyUnhashableObj;

public class PyOpaqueJavaObject implements IPyObj, IPyUnhashableObj, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass("opaquejavaobject", PyOpaqueJavaObject.class, PyBuiltinClass.INTERNAL);

  public final Object obj;

  public PyOpaqueJavaObject(Object obj) {
    this.obj = obj;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
