package org.japy.kernel.types.func;

import com.google.errorprone.annotations.CanIgnoreReturnValue;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.ext.adapters.IPyFuncAdapter;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.Args;

@Protocol(IPyFuncAdapter.class)
public interface IPyFunc extends IPyCallable, IPyTrueAtomObj {
  @SpecialInstanceMethod("__call__")
  @CanIgnoreReturnValue
  @ProtocolMethod
  @Override
  IPyObj call(Args args, PyContext ctx);
}
