package org.japy.kernel.types.func;

import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.infra.validation.Validator;
import org.japy.jexec.obj.JFunc;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceAttributeSetter;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.obj.IPyInstanceDict;
import org.japy.kernel.types.obj.PyInstanceDict;
import org.japy.kernel.util.Constants;

public abstract class PyFunc implements IPyFunc {
  private static final IPyClass TYPE =
      new PyBuiltinClass("function", JFunc.class, PyBuiltinClass.INTERNAL, null);

  protected final PyFuncHeader header;
  protected final IPyInstanceDict instanceDict = new PyInstanceDict();

  protected PyFunc(PyFuncHeader header) {
    this.header = header;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public void validate(Validator v) {
    v.validate(header);
  }

  @Override
  public IPyInstanceDict instanceDict() {
    return instanceDict;
  }

  @InstanceAttribute
  public IPyObj __doc__(PyContext ctx) {
    return header.docString;
  }

  @InstanceAttributeSetter(Constants.__DOC__)
  public void setDoc(IPyObj doc, PyContext ctx) {
    header.docString = doc;
  }

  @InstanceAttribute
  public IPyObj __name__(PyContext ctx) {
    return header.name;
  }

  @InstanceAttributeSetter(Constants.__NAME__)
  public void setName(IPyObj name, PyContext ctx) {
    header.name = ArgLib.checkAttrStr("__name__", name, ctx);
  }

  @InstanceAttribute
  public IPyObj __qualname__(PyContext ctx) {
    return header.qualName;
  }

  @InstanceAttributeSetter(Constants.__QUALNAME__)
  public void setQualName(IPyObj qualName, PyContext ctx) {
    header.qualName = ArgLib.checkAttrStr("__qualname__", qualName, ctx);
  }

  @InstanceAttribute
  public IPyObj __module__(PyContext ctx) {
    return header.module;
  }

  @InstanceAttributeSetter(Constants.__MODULE__)
  public void setModule(IPyObj module, PyContext ctx) {
    header.module = ArgLib.checkAttrStr("__module__", module, ctx);
  }

  @InstanceAttribute
  public IPyObj __annotations__(PyContext ctx) {
    return header.annotations;
  }

  @InstanceAttributeSetter(Constants.__ANNOTATIONS__)
  public void setAnnotations(IPyObj annotations, PyContext ctx) {
    if (annotations != None && !(annotations instanceof IPyDict)) {
      throw PxException.typeError("__annotations__ must be set to a dict object", ctx);
    }
    header.annotations = annotations != None ? annotations : new PyDict();
  }
}
