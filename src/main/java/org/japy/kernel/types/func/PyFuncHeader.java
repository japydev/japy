package org.japy.kernel.types.func;

import static org.japy.infra.util.ObjUtil.or;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.ISupportsValidation;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.Cell;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.util.ArgParser;

public class PyFuncHeader implements ISupportsValidation {
  public PyStr name;
  public PyStr qualName;
  public PyStr module;
  // TODO having a dictionary for all functions is a waste. Lazy init?
  public IPyObj annotations;
  public IPyObj docString;

  public final ArgParser argParser;
  public final Cell[] upvalues;
  public final IPyDict globals;
  public final IPyRODict builtins;
  public final boolean isAsync;
  public final boolean isGenerator;

  private PyFuncHeader(Builder builder) {
    this.name = dcheckNotNull(builder.name);
    this.qualName = dcheckNotNull(builder.qualName);
    this.module = dcheckNotNull(builder.module);
    this.argParser = dcheckNotNull(builder.argParser);
    this.upvalues = dcheckNotNull(builder.upvalues);
    this.globals = dcheckNotNull(builder.globals);
    this.builtins = dcheckNotNull(builder.builtins);
    this.annotations = builder.annotations != null ? builder.annotations : new PyDict();
    this.docString = dcheckNotNull(builder.docString);
    this.isAsync = builder.isAsync;
    this.isGenerator = builder.isGenerator;
  }

  public static Builder builder() {
    return new Builder();
  }

  @Override
  public void validate(Validator v) {
    v.validate(upvalues);
    v.validate(globals);
    v.validate(builtins);
    v.validate(annotations);
    v.validate(docString);
  }

  public static class Builder {
    private @Nullable PyStr name;
    private @Nullable PyStr qualName;
    private @Nullable PyStr module;
    private @Nullable ArgParser argParser;
    private Cell[] upvalues = Cell.EMPTY_ARRAY;
    private @Nullable IPyDict globals;
    private @Nullable IPyRODict builtins;
    private @Nullable IPyObj annotations;
    private @Nullable IPyObj docString;
    private boolean isAsync;
    private boolean isGenerator;

    public PyFuncHeader build() {
      return new PyFuncHeader(this);
    }

    private Builder() {}

    public Builder name(@Nullable PyStr name) {
      this.name = name;
      return this;
    }

    public Builder qualName(@Nullable PyStr qualName) {
      this.qualName = qualName;
      return this;
    }

    public Builder module(@Nullable PyStr module) {
      this.module = module;
      return this;
    }

    public Builder argParser(ArgParser argParser) {
      this.argParser = argParser;
      return this;
    }

    public Builder upvalues(Cell @Nullable [] upvalues) {
      this.upvalues = upvalues != null ? upvalues : Cell.EMPTY_ARRAY;
      return this;
    }

    public Builder globals(IPyDict globals) {
      this.globals = globals;
      return this;
    }

    public Builder builtins(IPyRODict builtins) {
      this.builtins = builtins;
      return this;
    }

    public Builder annotations(@Nullable IPyObj annotations) {
      this.annotations = annotations;
      return this;
    }

    public Builder docString(@Nullable IPyObj docString) {
      this.docString = or(docString, None);
      return this;
    }

    public Builder isAsync(boolean isAsync) {
      this.isAsync = isAsync;
      return this;
    }

    public Builder isGenerator(boolean isGenerator) {
      this.isGenerator = isGenerator;
      return this;
    }
  }
}
