package org.japy.kernel.types.func;

import static org.japy.kernel.types.num.PyBool.True;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

public class FuncLib {
  @CanIgnoreReturnValue
  public static IPyObj call(IPyObj func, Args args, PyContext ctx) {
    try {
      if (func instanceof PyBuiltinFunc) {
        return (IPyObj) ((PyBuiltinFunc) func).fh.handle.invokeExact(args, ctx);
      }

      if (func instanceof IPyCallable) {
        return ((IPyCallable) func).call(args, ctx);
      }

      FuncHandle fh = getMeta(func, ctx);
      return (IPyObj) fh.handle.invokeExact(func, args, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @CanIgnoreReturnValue
  public static IPyObj call(IPyObj func, PyContext ctx) {
    try {
      if (func instanceof PyBuiltinFunc) {
        return (IPyObj) ((PyBuiltinFunc) func).fh.handle.invokeExact(Args.EMPTY, ctx);
      }

      if (func instanceof IPyCallable) {
        return ((IPyCallable) func).call(Args.EMPTY, ctx);
      }

      FuncHandle fh = getMeta(func, ctx);
      return (IPyObj) fh.handle.invokeExact(func, Args.EMPTY, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @CanIgnoreReturnValue
  public static IPyObj call(IPyObj func, IPyObj arg1, PyContext ctx) {
    try {
      if (func instanceof PyBuiltinFunc) {
        return (IPyObj) ((PyBuiltinFunc) func).fh.handle.invokeExact(Args.of(arg1), ctx);
      }

      if (func instanceof IPyCallable) {
        return ((IPyCallable) func).call(Args.of(arg1), ctx);
      }

      FuncHandle fh = getMeta(func, ctx);
      return (IPyObj) fh.handle.invokeExact(func, Args.of(arg1), ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @CanIgnoreReturnValue
  public static IPyObj call(IPyObj func, IPyObj arg1, IPyObj arg2, PyContext ctx) {
    try {
      if (func instanceof PyBuiltinFunc) {
        return (IPyObj) ((PyBuiltinFunc) func).fh.handle.invokeExact(Args.of(arg1, arg2), ctx);
      }

      if (func instanceof IPyCallable) {
        return ((IPyCallable) func).call(Args.of(arg1, arg2), ctx);
      }

      FuncHandle fh = getMeta(func, ctx);
      return (IPyObj) fh.handle.invokeExact(func, Args.of(arg1, arg2), ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @CanIgnoreReturnValue
  public static IPyObj call(IPyObj func, IPyObj arg1, IPyObj arg2, IPyObj arg3, PyContext ctx) {
    try {
      if (func instanceof PyBuiltinFunc) {
        return (IPyObj)
            ((PyBuiltinFunc) func).fh.handle.invokeExact(Args.of(arg1, arg2, arg3), ctx);
      }

      if (func instanceof IPyCallable) {
        return ((IPyCallable) func).call(Args.of(arg1, arg2, arg3), ctx);
      }

      FuncHandle fh = getMeta(func, ctx);
      return (IPyObj) fh.handle.invokeExact(func, Args.of(arg1, arg2, arg3), ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  private static FuncHandle getMeta(IPyObj obj, PyContext ctx) {
    @Nullable FuncHandle call = obj.specialMethod(SpecialMethod.CALL);
    if (call == null) {
      throw PxException.typeError(
          String.format("'%s' object is not callable", obj.typeName()), ctx);
    }
    return call;
  }

  public static IPyObj callable(IPyObj obj) {
    if (obj instanceof IPyCallable || obj instanceof IPyClass) {
      return True;
    }
    return PyBool.of(ObjLib.findAttributeInClassHierarchy(obj.type(), Constants.__CALL__) != null);
  }

  @CanIgnoreReturnValue
  @SuppressWarnings("unused")
  public static IPyObj callMeth(IPyObj obj, String method, Args args, PyContext ctx) {
    return FuncLib.call(ObjLib.getAttr(obj, method, ctx), args, ctx);
  }

  @CanIgnoreReturnValue
  @SuppressWarnings("unused")
  public static IPyObj callMeth(IPyObj obj, String method, PyContext ctx) {
    return FuncLib.call(ObjLib.getAttr(obj, method, ctx), Args.of(), ctx);
  }

  @CanIgnoreReturnValue
  @SuppressWarnings("unused")
  public static IPyObj callMeth(IPyObj obj, String method, IPyObj arg1, PyContext ctx) {
    return FuncLib.call(ObjLib.getAttr(obj, method, ctx), Args.of(arg1), ctx);
  }

  @CanIgnoreReturnValue
  @SuppressWarnings("unused")
  public static IPyObj callMeth(
      IPyObj obj, String method, IPyObj arg1, IPyObj arg2, PyContext ctx) {
    return FuncLib.call(ObjLib.getAttr(obj, method, ctx), Args.of(arg1, arg2), ctx);
  }

  @CanIgnoreReturnValue
  @SuppressWarnings("unused")
  public static IPyObj callMeth(
      IPyObj obj, String method, IPyObj arg1, IPyObj arg2, IPyObj arg3, PyContext ctx) {
    return FuncLib.call(ObjLib.getAttr(obj, method, ctx), Args.of(arg1, arg2, arg3), ctx);
  }
}
