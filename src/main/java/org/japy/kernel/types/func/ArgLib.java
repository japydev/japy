package org.japy.kernel.types.func;

import static org.japy.kernel.types.num.PyBool.True;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.dict.IPyROMapping;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.num.IntLib;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.num.PyInt;

public class ArgLib {
  public static IPyDict checkArgDict(String func, String param, IPyObj arg, PyContext ctx) {
    if (!(arg instanceof IPyDict)) {
      throw PxException.typeError(
          String.format(
              "in function %s: %s must be a dictionary, got '%s'", func, param, arg.typeName()),
          ctx);
    }
    return (IPyDict) arg;
  }

  public static IPyROMapping checkArgROMapping(
      String func, String param, IPyObj arg, PyContext ctx) {
    if (!(arg instanceof IPyROMapping)) {
      throw PxException.typeError(
          String.format(
              "in function %s: %s must be a dictionary, got '%s'", func, param, arg.typeName()),
          ctx);
    }
    return (IPyROMapping) arg;
  }

  public static IPyRODict checkArgRODict(String func, String param, IPyObj arg, PyContext ctx) {
    if (!(arg instanceof IPyRODict)) {
      throw PxException.typeError(
          String.format(
              "in function %s: %s must be a dictionary, got '%s'", func, param, arg.typeName()),
          ctx);
    }
    return (IPyRODict) arg;
  }

  public static IPyClass checkArgClass(String func, String param, IPyObj arg, PyContext ctx) {
    if (!(arg instanceof IPyClass)) {
      throw PxException.typeError(
          String.format(
              "in function %s: %s must be a class, got '%s'", func, param, arg.typeName()),
          ctx);
    }
    return (IPyClass) arg;
  }

  public static IPyClass checkArgClass(
      String func, String param, IPyClass base, IPyObj arg, PyContext ctx) {
    IPyClass cls = checkArgClass(func, param, arg, ctx);
    if (!cls.isSubClassRaw(base)) {
      throw PxException.typeError(
          String.format(
              "in function %s: %s must be a subclass of %s, got '%s'",
              func, param, base.name(), cls.name()),
          ctx);
    }
    return cls;
  }

  public static void checkArgType(
      String func, String param, IPyClass cls, IPyObj arg, PyContext ctx) {
    if (!arg.isInstanceRaw(cls)) {
      throw PxException.typeError(
          String.format(
              "in function %s: %s must be an instance of '%s', got '%s'",
              func, param, cls.name(), arg.typeName()),
          ctx);
    }
  }

  public static PyTuple checkArgTuple(String func, String param, IPyObj arg, PyContext ctx) {
    if (!(arg instanceof PyTuple)) {
      throw PxException.typeError(
          String.format(
              "in function %s: %s must be a tuple, got '%s'", func, param, arg.typeName()),
          ctx);
    }
    return (PyTuple) arg;
  }

  public static PyStr checkAttrStr(String attr, IPyObj value, PyContext ctx) {
    if (!(value instanceof PyStr)) {
      throw PxException.typeError(
          String.format("%s must be set to a string, got '%s'", attr, value.typeName()), ctx);
    }
    return (PyStr) value;
  }

  public static PyInt checkArgInt(String func, String param, IPyObj value, PyContext ctx) {
    return IntLib.asInt(value, ctx);
  }

  public static int checkArgSmallInt(String func, String param, IPyObj value, PyContext ctx) {
    PyInt intValue = checkArgInt(func, param, value, ctx);
    if (!intValue.isSmall()) {
      throw PxException.valueError(
          String.format("in function %s: %s must be a small int, got %s", func, param, value), ctx);
    }
    return intValue.intValue();
  }

  public static int checkArgSmallPosInt(String func, String param, IPyObj value, PyContext ctx) {
    int intValue = checkArgSmallInt(func, param, value, ctx);
    if (intValue <= 0) {
      throw PxException.valueError(
          String.format("in function %s: %s must be positive, got %s", func, param, value), ctx);
    }
    return intValue;
  }

  public static int checkArgSmallNonNegInt(String func, String param, IPyObj value, PyContext ctx) {
    int intValue = checkArgSmallInt(func, param, value, ctx);
    if (intValue < 0) {
      throw PxException.valueError(
          String.format("in function %s: %s must be non-negative, got %s", func, param, value),
          ctx);
    }
    return intValue;
  }

  public static PyStr checkArgStr(String func, String param, IPyObj arg, PyContext ctx) {
    if (!(arg instanceof PyStr)) {
      throw PxException.typeError(
          String.format(
              "in function %s: %s must be a string, got '%s'", func, param, arg.typeName()),
          ctx);
    }
    return (PyStr) arg;
  }

  public static boolean checkArgBool(String func, String param, IPyObj arg, PyContext ctx) {
    if (!(arg instanceof PyBool)) {
      throw PxException.typeError(
          String.format(
              "in function %s: %s must be a boolean, got '%s'", func, param, arg.typeName()),
          ctx);
    }
    return arg == True;
  }
}
