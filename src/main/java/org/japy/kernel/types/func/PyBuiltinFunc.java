package org.japy.kernel.types.func;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.util.Args;

public class PyBuiltinFunc implements IPyFunc, IPyNoValidationObj {
  public final FuncHandle fh;
  private final String name;

  public PyBuiltinFunc(FuncHandle fh, String name) {
    this.fh = fh;
    this.name = name;
    if (Debug.ENABLED) {
      dcheck(fh.type().equals(IPyFunc.METHOD_TYPE));
    }
  }

  @Override
  public IPyObj call(Args args, PyContext ctx) {
    try {
      return (IPyObj) fh.handle.invokeExact(args, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @InstanceAttribute
  public IPyObj __name__(PyContext ctx) {
    return PyStr.get(name);
  }

  // TODO
  @InstanceAttribute
  public IPyObj __doc__(PyContext ctx) {
    return None;
  }

  private static class TYPE_HOLDER {
    public static final IPyClass TYPE =
        new PyBuiltinClass("builtinfunction", PyBuiltinFunc.class, PyBuiltinClass.INTERNAL);
  }

  @Override
  public IPyClass type() {
    return TYPE_HOLDER.TYPE;
  }
}
