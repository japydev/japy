package org.japy.kernel.types.func;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.cls.SpecialMethodTable.TYPE_BOOL_OBJ1_CTX;
import static org.japy.kernel.types.cls.SpecialMethodTable.TYPE_BOOL_OBJ2_CTX;
import static org.japy.kernel.types.cls.SpecialMethodTable.TYPE_INT_OBJ1_CTX;
import static org.japy.kernel.types.cls.SpecialMethodTable.TYPE_OBJ_OBJ1_CTX;
import static org.japy.kernel.types.cls.SpecialMethodTable.TYPE_OBJ_OBJ2_CTX;
import static org.japy.kernel.types.cls.SpecialMethodTable.TYPE_OBJ_OBJ3_CTX;
import static org.japy.kernel.types.cls.SpecialMethodTable.TYPE_VOID_OBJ2_CTX;
import static org.japy.kernel.types.cls.SpecialMethodTable.TYPE_VOID_OBJ3_CTX;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;

import java.lang.annotation.Annotation;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Executable;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.MethodKind;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.HashLib;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

@SuppressWarnings("DuplicatedCode")
public class FuncAdapterLib {
  private static final MethodHandles.Lookup MY_LOOKUP = MethodHandles.lookup();

  private interface InstanceMethod {
    IPyObj call(IPyObj self, Args args, PyContext ctx);
  }

  private interface ClassMethod {
    IPyObj call(IPyClass cls, Args args, PyContext ctx);
  }

  private interface StaticMethod {
    IPyObj call(Args args, PyContext ctx);
  }

  private enum CallKind {
    CTX(false),
    ARGS_CTX(true),
    OBJS_CTX(false),
    ;

    public final boolean hasArgs;

    CallKind(boolean hasArgs) {
      this.hasArgs = hasArgs;
    }
  }

  private static class InstanceOrClassMethodInfo {
    public final CallKind kind;
    public final int objParamCount;
    public final int firstNonSelfParam;
    public final FuncHandle fh;

    public InstanceOrClassMethodInfo(
        Method method, MethodKind methodKind, boolean objRetType, MethodHandles.Lookup lookup) {
      boolean needSelfParam = Modifier.isStatic(method.getModifiers());
      if (!needSelfParam) {
        if (methodKind == MethodKind.CLASS) {
          throw new IllegalArgumentException(
              String.format(
                  "non-static class method %s in class %s",
                  method.getName(), method.getDeclaringClass()));
        } else if (!isPyObj(method.getDeclaringClass())) {
          throw new IllegalArgumentException(
              String.format(
                  "non-static method %s in a non-IPyObj-derived class %s",
                  method.getName(), method.getDeclaringClass()));
        }
      }

      if (objRetType && !isPyObj(method.getReturnType())) {
        throw new IllegalArgumentException(
            String.format(
                "method %s.%s: invalid return type %s, expected IPyObj or a subclass",
                method.getDeclaringClass(), method.getName(), method.getReturnType()));
      }

      Class<?>[] ptypes = method.getParameterTypes();
      int formalParamCount = ptypes.length;

      if (needSelfParam) {
        if (formalParamCount == 0) {
          throw new IllegalArgumentException(
              String.format(
                  "method %s.%s: missing self parameter",
                  method.getDeclaringClass(), method.getName()));
        }

        if (methodKind == MethodKind.INSTANCE) {
          if (!isPyObj(ptypes[0])) {
            throw new IllegalArgumentException(
                String.format(
                    "method %s.%s: first parameter must be an IPyObj",
                    method.getDeclaringClass(), method.getName()));
          }
        } else {
          if (!isPyClass(ptypes[0])) {
            throw new IllegalArgumentException(
                String.format(
                    "method %s.%s: first parameter must be an IPyClass",
                    method.getDeclaringClass(), method.getName()));
          }
        }
      }

      fh = FuncHandle.method(method, lookup);

      firstNonSelfParam = needSelfParam ? 1 : 0;
      int nonSelfParamCount = formalParamCount - firstNonSelfParam;

      if (nonSelfParamCount == 0 || ptypes[ptypes.length - 1] != PyContext.class) {
        throw new IllegalArgumentException(
            String.format(
                "method %s.%s: last parameter must be PyContext",
                method.getDeclaringClass(), method.getName()));
      }

      switch (nonSelfParamCount) {
        case 1:
          kind = CallKind.CTX;
          objParamCount = 0;
          return;
        case 2:
          if (ptypes[firstNonSelfParam] == Args.class) {
            kind = CallKind.ARGS_CTX;
            objParamCount = 0;
            return;
          }
          break;
        default:
          break;
      }

      objParamCount = nonSelfParamCount - 1;
      kind = CallKind.OBJS_CTX;
      //noinspection ConstantConditions
      dcheck(objParamCount != 0);

      for (int i = firstNonSelfParam; i < firstNonSelfParam + objParamCount; ++i) {
        if (!isPyObj(ptypes[i])) {
          throw new IllegalArgumentException(
              String.format(
                  "method %s.%s: invalid parameter type %s, expected an IPyObj",
                  method.getDeclaringClass(), method.getName(), ptypes[i]));
        }
      }
    }
  }

  private static class StaticMethodInfo {
    public final CallKind kind;
    public final int objParamCount;
    public final FuncHandle fh;

    private static FuncHandle getHandleFromMethod(Method method, MethodHandles.Lookup lookup) {
      if (!Modifier.isStatic(method.getModifiers())) {
        throw new IllegalArgumentException(
            String.format(
                "non-static static method %s in class %s",
                method.getName(), method.getDeclaringClass()));
      }

      return FuncHandle.method(method, lookup);
    }

    public StaticMethodInfo(Method method, MethodHandles.Lookup lookup) {
      this(getHandleFromMethod(method, lookup), method);
    }

    public StaticMethodInfo(FuncHandle fh, Executable method) {
      this.fh = fh;

      MethodType methodType = fh.type();

      if (!isPyObj(methodType.returnType())) {
        throw new IllegalArgumentException(
            String.format(
                "method %s.%s: invalid return type %s, expected IPyObj or a subclass",
                method.getDeclaringClass(), method.getName(), methodType.returnType()));
      }

      Class<?>[] ptypes = methodType.parameterArray();
      int paramCount = ptypes.length;

      if (paramCount == 0 || ptypes[ptypes.length - 1] != PyContext.class) {
        throw new IllegalArgumentException(
            String.format(
                "method %s.%s: last parameter must be a PyContext",
                method.getDeclaringClass(), method.getName()));
      }

      switch (paramCount) {
        case 1:
          kind = CallKind.CTX;
          objParamCount = 0;
          return;
        case 2:
          if (ptypes[0] == Args.class) {
            kind = CallKind.ARGS_CTX;
            objParamCount = 0;
            return;
          }
          break;
        default:
          break;
      }

      objParamCount = paramCount - 1;
      kind = CallKind.OBJS_CTX;
      //noinspection ConstantConditions
      dcheck(objParamCount != 0);

      for (int i = 0; i < objParamCount; ++i) {
        if (!isPyObj(ptypes[i])) {
          throw new IllegalArgumentException(
              String.format(
                  "method %s.%s: invalid parameter type %s, expected an IPyObj",
                  method.getDeclaringClass(), method.getName(), ptypes[i]));
        }
      }
    }
  }

  public static FuncHandle makeInstanceMethod(
      Method method, String name, MethodHandles.Lookup lookup) {
    return makeInstanceOrClassMethod(method, name, MethodKind.INSTANCE, lookup);
  }

  public static FuncHandle makeClassMethod(
      Method method, String name, MethodHandles.Lookup lookup) {
    return makeInstanceOrClassMethod(method, name, MethodKind.CLASS, lookup);
  }

  private static FuncHandle fixSelfParamType(MethodKind methodKind, FuncHandle h) {
    Class<?> selfType = methodKind == MethodKind.INSTANCE ? IPyObj.class : IPyClass.class;
    if (h.type().parameterType(0) == selfType) {
      return h;
    }
    return h.asType(h.type().changeParameterType(0, selfType));
  }

  private static FuncHandle makeInstanceOrClassMethod(
      Method method, String name, MethodKind methodKind, MethodHandles.Lookup lookup) {
    InstanceOrClassMethodInfo methInfo =
        new InstanceOrClassMethodInfo(method, methodKind, true, lookup);

    try {
      if (methInfo.kind == CallKind.ARGS_CTX) {
        return fixSelfParamType(methodKind, methInfo.fh);
      }

      if (methInfo.objParamCount == 0) {
        return fixSelfParamType(methodKind, adaptWithCtx0(methInfo.fh, name, methodKind));
      }

      ArgParser p = makeArgParser(method, methInfo.objParamCount, methInfo.firstNonSelfParam);

      switch (methInfo.objParamCount) {
        case 1:
          return fixSelfParamType(methodKind, adaptWithCtx1(methInfo.fh, name, p, methodKind));
        case 2:
          return fixSelfParamType(methodKind, adaptWithCtx2(methInfo.fh, name, p, methodKind));
        case 3:
          return fixSelfParamType(methodKind, adaptWithCtx3(methInfo.fh, name, p, methodKind));
        case 4:
          return fixSelfParamType(methodKind, adaptWithCtx4(methInfo.fh, name, p, methodKind));
        case 5:
          return fixSelfParamType(methodKind, adaptWithCtx5(methInfo.fh, name, p, methodKind));
        case 6:
          return fixSelfParamType(methodKind, adaptWithCtx6(methInfo.fh, name, p, methodKind));
        default:
          throw new IllegalArgumentException(
              String.format(
                  "method %s.%s: too many parameters",
                  method.getDeclaringClass(), method.getName()));
      }
    } catch (Throwable ex) {
      throw new IllegalArgumentException(
          String.format(
              "method %s.%s: %s", method.getDeclaringClass(), method.getName(), ex.getMessage()),
          ex);
    }
  }

  public static FuncHandle makeStaticMethod(
      Method method, String name, MethodHandles.Lookup lookup) {
    StaticMethodInfo methInfo = new StaticMethodInfo(method, lookup);
    return adaptStaticMethod(methInfo, method, name);
  }

  public static FuncHandle makeBuiltinFunc(FuncHandle mh, Executable method, String name) {
    StaticMethodInfo methInfo = new StaticMethodInfo(mh, method);
    FuncHandle adapted = adaptStaticMethod(methInfo, method, name);
    Class<?> rtype = adapted.type().returnType();
    if (rtype != IPyObj.class) {
      if (!isPyObj(rtype)) {
        throw new InternalErrorException(
            String.format(
                "invalid return type of %s.%s: %s, expected IPyObj",
                method.getDeclaringClass(), method.getName(), rtype));
      }
      return adapted.asType(adapted.type().changeReturnType(IPyObj.class));
    }
    return adapted;
  }

  private static FuncHandle adaptStaticMethod(
      StaticMethodInfo methInfo, Executable method, String name) {
    try {
      if (methInfo.kind == CallKind.ARGS_CTX) {
        return methInfo.fh;
      }

      if (methInfo.objParamCount == 0) {
        return adaptWithCtx0(methInfo.fh, name, MethodKind.STATIC);
      }

      ArgParser p = makeArgParser(method, methInfo.objParamCount, 0);

      switch (methInfo.objParamCount) {
        case 1:
          return adaptWithCtx1(methInfo.fh, name, p, MethodKind.STATIC);
        case 2:
          return adaptWithCtx2(methInfo.fh, name, p, MethodKind.STATIC);
        case 3:
          return adaptWithCtx3(methInfo.fh, name, p, MethodKind.STATIC);
        case 4:
          return adaptWithCtx4(methInfo.fh, name, p, MethodKind.STATIC);
        case 5:
          return adaptWithCtx5(methInfo.fh, name, p, MethodKind.STATIC);
        case 6:
          return adaptWithCtx6(methInfo.fh, name, p, MethodKind.STATIC);
        default:
          throw new IllegalArgumentException(
              String.format(
                  "method %s.%s: too many parameters",
                  method.getDeclaringClass(), method.getName()));
      }
    } catch (Throwable ex) {
      throw new IllegalArgumentException(
          String.format(
              "method %s.%s: %s", method.getDeclaringClass(), method.getName(), ex.getMessage()),
          ex);
    }
  }

  public static FuncHandle makeInstanceAttributeGetter(Method method, MethodHandles.Lookup lookup) {
    InstanceOrClassMethodInfo mi =
        new InstanceOrClassMethodInfo(method, MethodKind.INSTANCE, true, lookup);
    if (mi.kind != CallKind.CTX) {
      throw new IllegalArgumentException(
          String.format(
              "method %s.%s: unexpected parameters, it must take a single PyContext parameter",
              method.getDeclaringClass(), method.getName()));
    }

    return mi.fh.asType(mi.fh.type().changeParameterType(0, IPyObj.class));
  }

  public static FuncHandle makeInstanceAttributeSetter(Method method, MethodHandles.Lookup lookup) {
    InstanceOrClassMethodInfo mi =
        new InstanceOrClassMethodInfo(method, MethodKind.INSTANCE, false, lookup);
    if (mi.kind != CallKind.OBJS_CTX || mi.objParamCount != 1) {
      throw new IllegalArgumentException(
          String.format(
              "method %s.%s: unexpected parameters, it must take an IPyObj and PyContext parameters",
              method.getDeclaringClass(), method.getName()));
    }

    return mi.fh.asType(mi.fh.type().changeParameterType(0, IPyObj.class));
  }

  private static void addParam(ArgParser.Builder builder, Annotation[] annotations) {
    for (Annotation ann : annotations) {
      if (ann instanceof Param) {
        builder.add((Param) ann);
      }
      return;
    }

    throw new IllegalArgumentException(String.format("%s annotation is missing", Param.class));
  }

  private static ArgParser makeArgParser(Executable method, int paramCount, int firstParam) {
    ArgParser.Builder b = ArgParser.builder(paramCount);
    for (int i = 0; i != paramCount; ++i) {
      addParam(b, method.getParameterAnnotations()[firstParam + i]);
    }
    return b.build();
  }

  private static FuncHandle bindCall(Object meth, MethodType type, Object origin)
      throws NoSuchMethodException, IllegalAccessException {
    return new FuncHandle(
        MY_LOOKUP.findVirtual(meth.getClass(), "call", type).bindTo(meth), origin);
  }

  private static FuncHandle adaptWithCtx0(FuncHandle fh, String methodName, MethodKind methodKind)
      throws NoSuchMethodException, IllegalAccessException {
    Object meth;
    switch (methodKind) {
      case INSTANCE:
        meth =
            (InstanceMethod)
                (self, args, ctx) -> {
                  ArgParser.verifyNoArgs(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(self, ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case CLASS:
        meth =
            (ClassMethod)
                (cls, args, ctx) -> {
                  ArgParser.verifyNoArgs(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(cls, ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case STATIC:
        meth =
            (StaticMethod)
                (args, ctx) -> {
                  ArgParser.verifyNoArgs(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      default:
        throw InternalErrorException.notReached();
    }
    return bindCall(meth, methodKind.methodType, fh.origin);
  }

  private static FuncHandle adaptWithCtx1(
      FuncHandle fh, String methodName, ArgParser argParser, MethodKind methodKind)
      throws NoSuchMethodException, IllegalAccessException {
    Object meth;
    switch (methodKind) {
      case INSTANCE:
        meth =
            (InstanceMethod)
                (self, args, ctx) -> {
                  IPyObj arg = argParser.parse1(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(self, arg, ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case CLASS:
        meth =
            (ClassMethod)
                (cls, args, ctx) -> {
                  IPyObj arg = argParser.parse1(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(cls, arg, ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case STATIC:
        meth =
            (StaticMethod)
                (args, ctx) -> {
                  IPyObj arg = argParser.parse1(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(arg, ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      default:
        throw InternalErrorException.notReached();
    }
    return bindCall(meth, methodKind.methodType, fh.origin);
  }

  private static FuncHandle adaptWithCtx2(
      FuncHandle fh, String methodName, ArgParser argParser, MethodKind methodKind)
      throws NoSuchMethodException, IllegalAccessException {
    Object meth;
    switch (methodKind) {
      case INSTANCE:
        meth =
            (InstanceMethod)
                (self, args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(self, arg[0], arg[1], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case CLASS:
        meth =
            (ClassMethod)
                (cls, args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(cls, arg[0], arg[1], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case STATIC:
        meth =
            (StaticMethod)
                (args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(arg[0], arg[1], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      default:
        throw InternalErrorException.notReached();
    }
    return bindCall(meth, methodKind.methodType, fh.origin);
  }

  private static FuncHandle adaptWithCtx3(
      FuncHandle fh, String methodName, ArgParser argParser, MethodKind methodKind)
      throws NoSuchMethodException, IllegalAccessException {
    Object meth;
    switch (methodKind) {
      case INSTANCE:
        meth =
            (InstanceMethod)
                (self, args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(self, arg[0], arg[1], arg[2], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case CLASS:
        meth =
            (ClassMethod)
                (cls, args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(cls, arg[0], arg[1], arg[2], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case STATIC:
        meth =
            (StaticMethod)
                (args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(arg[0], arg[1], arg[2], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      default:
        throw InternalErrorException.notReached();
    }
    return bindCall(meth, methodKind.methodType, fh.origin);
  }

  private static FuncHandle adaptWithCtx4(
      FuncHandle fh, String methodName, ArgParser argParser, MethodKind methodKind)
      throws NoSuchMethodException, IllegalAccessException {
    Object meth;
    switch (methodKind) {
      case INSTANCE:
        meth =
            (InstanceMethod)
                (self, args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(self, arg[0], arg[1], arg[2], arg[3], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case CLASS:
        meth =
            (ClassMethod)
                (cls, args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(cls, arg[0], arg[1], arg[2], arg[3], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case STATIC:
        meth =
            (StaticMethod)
                (args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(arg[0], arg[1], arg[2], arg[3], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      default:
        throw InternalErrorException.notReached();
    }
    return bindCall(meth, methodKind.methodType, fh.origin);
  }

  private static FuncHandle adaptWithCtx5(
      FuncHandle fh, String methodName, ArgParser argParser, MethodKind methodKind)
      throws NoSuchMethodException, IllegalAccessException {
    Object meth;
    switch (methodKind) {
      case INSTANCE:
        meth =
            (InstanceMethod)
                (self, args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj)
                        fh.handle.invoke(self, arg[0], arg[1], arg[2], arg[3], arg[4], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case CLASS:
        meth =
            (ClassMethod)
                (cls, args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj)
                        fh.handle.invoke(cls, arg[0], arg[1], arg[2], arg[3], arg[4], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case STATIC:
        meth =
            (StaticMethod)
                (args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj) fh.handle.invoke(arg[0], arg[1], arg[2], arg[3], arg[4], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      default:
        throw InternalErrorException.notReached();
    }
    return bindCall(meth, methodKind.methodType, fh.origin);
  }

  private static FuncHandle adaptWithCtx6(
      FuncHandle fh, String methodName, ArgParser argParser, MethodKind methodKind)
      throws NoSuchMethodException, IllegalAccessException {
    Object meth;
    switch (methodKind) {
      case INSTANCE:
        meth =
            (InstanceMethod)
                (self, args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj)
                        fh.handle.invoke(self, arg[0], arg[1], arg[2], arg[3], arg[4], arg[5], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case CLASS:
        meth =
            (ClassMethod)
                (cls, args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj)
                        fh.handle.invoke(cls, arg[0], arg[1], arg[2], arg[3], arg[4], arg[5], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      case STATIC:
        meth =
            (StaticMethod)
                (args, ctx) -> {
                  IPyObj[] arg = argParser.parse(args, methodName, ctx);
                  try {
                    return (IPyObj)
                        fh.handle.invoke(arg[0], arg[1], arg[2], arg[3], arg[4], arg[5], ctx);
                  } catch (Throwable ex) {
                    throw ExcUtil.rethrow(ex);
                  }
                };
        break;
      default:
        throw InternalErrorException.notReached();
    }
    return bindCall(meth, methodKind.methodType, fh.origin);
  }

  private static boolean isPyObj(Class<?> paramType) {
    return IPyObj.class.isAssignableFrom(paramType);
  }

  private static boolean isPyClass(Class<?> paramType) {
    return IPyClass.class.isAssignableFrom(paramType);
  }

  private static final MethodType METH_TYPE_IPYOBJ_IPYOBJ1_CTX =
      MethodType.methodType(IPyObj.class, IPyObj.class, PyContext.class);
  private static final MethodType METH_TYPE_IPYOBJ_IPYOBJ2_CTX =
      MethodType.methodType(IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class);

  private interface FuncIntObj1Ctx {
    int call(IPyObj arg1, PyContext ctx);
  }

  private interface FuncBoolObj1Ctx {
    boolean call(IPyObj arg1, PyContext ctx);
  }

  private interface FuncBoolObj2Ctx {
    boolean call(IPyObj arg1, IPyObj arg2, PyContext ctx);
  }

  private static FuncHandle changeReturnTypeToInt(FuncHandle fh, SpecialMethod m)
      throws NoSuchMethodException, IllegalAccessException {
    MethodType srcType = fh.type();
    MethodType tgtType = srcType.changeReturnType(int.class);
    Object meth;
    if (srcType.equals(METH_TYPE_IPYOBJ_IPYOBJ1_CTX)) {
      switch (m) {
        case LEN:
          meth =
              (FuncIntObj1Ctx)
                  (arg1, ctx) -> {
                    IPyObj v;
                    try {
                      v = (IPyObj) fh.handle.invokeExact(arg1, ctx);
                    } catch (Throwable ex) {
                      throw ExcUtil.rethrow(ex);
                    }
                    return CollLib.toLenRaw(v, ctx);
                  };
          break;
        case HASH:
          meth =
              (FuncIntObj1Ctx)
                  (arg1, ctx) -> {
                    IPyObj v;
                    try {
                      v = (IPyObj) fh.handle.invokeExact(arg1, ctx);
                    } catch (Throwable ex) {
                      throw ExcUtil.rethrow(ex);
                    }
                    return HashLib.hashToInt(v, ctx);
                  };
          break;
        default:
          throw new InternalErrorException(
              String.format(
                  "don't know how to convert %s to %s for method %s", srcType, tgtType, m));
      }
    } else {
      throw new InternalErrorException(
          String.format("don't know how to convert %s to %s for method %s", srcType, tgtType, m));
    }
    return bindCall(meth, tgtType, fh.origin);
  }

  private static FuncHandle changeReturnTypeToBool(FuncHandle fh, SpecialMethod m)
      throws NoSuchMethodException, IllegalAccessException {
    MethodType srcType = fh.type();
    MethodType tgtType = srcType.changeReturnType(boolean.class);
    Object meth;
    if (srcType.equals(METH_TYPE_IPYOBJ_IPYOBJ1_CTX) && m == SpecialMethod.BOOL) {
      meth =
          (FuncBoolObj1Ctx)
              (arg1, ctx) -> {
                IPyObj v;
                try {
                  v = (IPyObj) fh.handle.invokeExact(arg1, ctx);
                } catch (Throwable ex) {
                  throw ExcUtil.rethrow(ex);
                }
                if (v == True) {
                  return true;
                } else if (v == False) {
                  return false;
                } else {
                  throw PxException.typeError(
                      String.format(
                          "__bool__ returned a '%s' value, expected a bool", v.typeName()),
                      ctx);
                }
              };
    } else if (srcType.equals(METH_TYPE_IPYOBJ_IPYOBJ2_CTX)) {
      meth =
          (FuncBoolObj2Ctx)
              (arg1, arg2, ctx) -> {
                IPyObj v;
                try {
                  v = (IPyObj) fh.handle.invokeExact(arg1, arg2, ctx);
                } catch (Throwable ex) {
                  throw ExcUtil.rethrow(ex);
                }
                return ObjLib.isTrue(v, ctx);
              };
    } else {
      throw new InternalErrorException(
          String.format("don't know how to convert %s to %s for method %s", srcType, tgtType, m));
    }
    return bindCall(meth, tgtType, fh.origin);
  }

  public static FuncHandle changeReturnType(FuncHandle h, SpecialMethod m) {
    try {
      if (m.type.returnType() == int.class) {
        return changeReturnTypeToInt(h, m);
      } else if (m.type.returnType() == boolean.class) {
        return changeReturnTypeToBool(h, m);
      } else if (m.type.returnType() == void.class) {
        return h.asType(h.type().changeReturnType(void.class));
      } else {
        throw new InternalErrorException("unhandled return type " + m.type.returnType());
      }
    } catch (NoSuchMethodException | IllegalAccessException ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  private interface IAdapter {
    MethodHandle adapt(IPyCallable func);
  }

  private static final @Nullable IAdapter[] METHOD_ADAPTERS =
      new IAdapter[SpecialMethod.values().length];

  static {
    makeAdapters();
  }

  public static FuncHandle makeSpecialMethod(IPyCallable func, SpecialMethod specialMethod) {
    @Nullable IAdapter adapter = METHOD_ADAPTERS[specialMethod.ordinal()];
    if (adapter == null) {
      throw new InternalErrorException("don't know how to adapt method " + specialMethod);
    }
    return new FuncHandle(adapter.adapt(func), func);
  }

  private interface FuncObjObj1Ctx {
    IPyObj call(IPyObj arg1, PyContext ctx);
  }

  private interface FuncObjObj2Ctx {
    IPyObj call(IPyObj arg1, IPyObj arg2, PyContext ctx);
  }

  private interface FuncObjObj3Ctx {
    IPyObj call(IPyObj arg1, IPyObj arg2, IPyObj arg3, PyContext ctx);
  }

  private interface FuncVoidObj2Ctx {
    void call(IPyObj arg1, IPyObj arg2, PyContext ctx);
  }

  private interface FuncVoidObj3Ctx {
    void call(IPyObj arg1, IPyObj arg2, IPyObj arg3, PyContext ctx);
  }

  private static void addAdapter(MethodType methodType, IAdapter adapter) {
    for (SpecialMethod m : SpecialMethod.values()) {
      if (m.type.equals(methodType)) {
        METHOD_ADAPTERS[m.ordinal()] = adapter;
      }
    }
  }

  private static void makeAdapters() {
    addAdapter(
        SpecialMethod.INIT.type,
        func -> {
          Object meth = (InstanceMethod) (self, args, ctx) -> func.call(args.prepend(self), ctx);
          try {
            return MY_LOOKUP
                .findVirtual(InstanceMethod.class, "call", SpecialMethod.INIT.type)
                .bindTo(meth);
          } catch (NoSuchMethodException | IllegalAccessException ex) {
            throw ExcUtil.rethrow(ex);
          }
        });

    METHOD_ADAPTERS[SpecialMethod.BOOL.ordinal()] =
        func -> {
          Object meth =
              (FuncBoolObj1Ctx)
                  (obj, ctx) -> {
                    IPyObj v = func.call(Args.of(obj), ctx);
                    if (!(v instanceof PyBool)) {
                      throw PxException.typeError(
                          String.format(
                              "__bool__ returned an instance of type '%s', expected bool",
                              v.typeName()),
                          ctx);
                    }
                    return v == True;
                  };
          try {
            return MY_LOOKUP
                .findVirtual(FuncBoolObj1Ctx.class, "call", TYPE_BOOL_OBJ1_CTX)
                .bindTo(meth);
          } catch (NoSuchMethodException | IllegalAccessException ex) {
            throw ExcUtil.rethrow(ex);
          }
        };

    addAdapter(
        TYPE_BOOL_OBJ2_CTX,
        func -> {
          Object meth =
              (FuncBoolObj2Ctx)
                  (arg1, arg2, ctx) -> ObjLib.isTrue(func.call(Args.of(arg1, arg2), ctx), ctx);
          try {
            return MY_LOOKUP
                .findVirtual(FuncBoolObj2Ctx.class, "call", TYPE_BOOL_OBJ2_CTX)
                .bindTo(meth);
          } catch (NoSuchMethodException | IllegalAccessException ex) {
            throw ExcUtil.rethrow(ex);
          }
        });

    METHOD_ADAPTERS[SpecialMethod.LEN.ordinal()] =
        func -> {
          Object meth =
              (FuncIntObj1Ctx) (obj, ctx) -> CollLib.toLenRaw(func.call(Args.of(obj), ctx), ctx);
          try {
            return MY_LOOKUP
                .findVirtual(FuncIntObj1Ctx.class, "call", TYPE_INT_OBJ1_CTX)
                .bindTo(meth);
          } catch (NoSuchMethodException | IllegalAccessException ex) {
            throw ExcUtil.rethrow(ex);
          }
        };

    METHOD_ADAPTERS[SpecialMethod.HASH.ordinal()] =
        func -> {
          Object meth =
              (FuncIntObj1Ctx) (obj, ctx) -> HashLib.hashToInt(func.call(Args.of(obj), ctx), ctx);
          try {
            return MY_LOOKUP
                .findVirtual(FuncIntObj1Ctx.class, "call", TYPE_INT_OBJ1_CTX)
                .bindTo(meth);
          } catch (NoSuchMethodException | IllegalAccessException ex) {
            throw ExcUtil.rethrow(ex);
          }
        };

    addAdapter(
        TYPE_OBJ_OBJ1_CTX,
        func -> {
          Object meth = (FuncObjObj1Ctx) (arg1, ctx) -> func.call(Args.of(arg1), ctx);
          try {
            return MY_LOOKUP
                .findVirtual(FuncObjObj1Ctx.class, "call", TYPE_OBJ_OBJ1_CTX)
                .bindTo(meth);
          } catch (NoSuchMethodException | IllegalAccessException ex) {
            throw ExcUtil.rethrow(ex);
          }
        });

    addAdapter(
        TYPE_OBJ_OBJ2_CTX,
        func -> {
          Object meth = (FuncObjObj2Ctx) (arg1, arg2, ctx) -> func.call(Args.of(arg1, arg2), ctx);
          try {
            return MY_LOOKUP
                .findVirtual(FuncObjObj2Ctx.class, "call", TYPE_OBJ_OBJ2_CTX)
                .bindTo(meth);
          } catch (NoSuchMethodException | IllegalAccessException ex) {
            throw ExcUtil.rethrow(ex);
          }
        });

    addAdapter(
        TYPE_OBJ_OBJ3_CTX,
        func -> {
          Object meth =
              (FuncObjObj3Ctx) (arg1, arg2, arg3, ctx) -> func.call(Args.of(arg1, arg2, arg3), ctx);
          try {
            return MY_LOOKUP
                .findVirtual(FuncObjObj3Ctx.class, "call", TYPE_OBJ_OBJ3_CTX)
                .bindTo(meth);
          } catch (NoSuchMethodException | IllegalAccessException ex) {
            throw ExcUtil.rethrow(ex);
          }
        });

    addAdapter(
        TYPE_VOID_OBJ2_CTX,
        func -> {
          Object meth = (FuncVoidObj2Ctx) (arg1, arg2, ctx) -> func.call(Args.of(arg1, arg2), ctx);
          try {
            return MY_LOOKUP
                .findVirtual(FuncVoidObj2Ctx.class, "call", TYPE_VOID_OBJ2_CTX)
                .bindTo(meth);
          } catch (NoSuchMethodException | IllegalAccessException ex) {
            throw ExcUtil.rethrow(ex);
          }
        });

    addAdapter(
        TYPE_VOID_OBJ3_CTX,
        func -> {
          Object meth =
              (FuncVoidObj3Ctx)
                  (arg1, arg2, arg3, ctx) -> func.call(Args.of(arg1, arg2, arg3), ctx);
          try {
            return MY_LOOKUP
                .findVirtual(FuncVoidObj3Ctx.class, "call", TYPE_VOID_OBJ3_CTX)
                .bindTo(meth);
          } catch (NoSuchMethodException | IllegalAccessException ex) {
            throw ExcUtil.rethrow(ex);
          }
        });

    addAdapter(
        SpecialMethod.NEW.type,
        func -> {
          try {
            return MY_LOOKUP
                .findVirtual(IPyFunc.class, "call", SpecialMethod.NEW.type)
                .bindTo(func);
          } catch (NoSuchMethodException | IllegalAccessException ex) {
            throw ExcUtil.rethrow(ex);
          }
        });
  }

  private interface BinaryOp {
    IPyObj call(IPyObj arg1, IPyObj arg2, PyContext ctx);
  }

  public static FuncHandle adaptTernaryPow(FuncHandle fh) {
    if (Debug.ENABLED) {
      dcheck(fh.type().equals(TYPE_OBJ_OBJ3_CTX));
    }

    BinaryOp op =
        (arg1, arg2, ctx) -> {
          try {
            return (IPyObj) fh.handle.invokeExact(arg1, arg2, (IPyObj) None, ctx);
          } catch (Throwable ex) {
            throw ExcUtil.rethrow(ex);
          }
        };

    try {
      return new FuncHandle(
          MY_LOOKUP.findVirtual(op.getClass(), "call", TYPE_OBJ_OBJ2_CTX).bindTo(op), fh.origin);
    } catch (NoSuchMethodException | IllegalAccessException ex) {
      throw ExcUtil.rethrow(ex);
    }
  }
}
