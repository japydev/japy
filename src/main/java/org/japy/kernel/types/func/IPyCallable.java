package org.japy.kernel.types.func;

import java.lang.invoke.MethodType;

import com.google.errorprone.annotations.CanIgnoreReturnValue;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.util.Args;

public interface IPyCallable {
  MethodType METHOD_TYPE = MethodType.methodType(IPyObj.class, Args.class, PyContext.class);

  @CanIgnoreReturnValue
  IPyObj call(Args args, PyContext ctx);

  default IPyObj call(PyContext ctx) {
    return call(Args.of(), ctx);
  }

  default IPyObj call(IPyObj arg1, PyContext ctx) {
    return call(Args.of(arg1), ctx);
  }

  default IPyObj call(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    return call(Args.of(arg1, arg2), ctx);
  }

  default IPyObj call(IPyObj arg1, IPyObj arg2, IPyObj arg3, PyContext ctx) {
    return call(Args.of(arg1, arg2, arg3), ctx);
  }
}
