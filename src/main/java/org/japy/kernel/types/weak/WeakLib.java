package org.japy.kernel.types.weak;

import org.japy.kernel.types.cls.IPyClass;

public class WeakLib {
  // TODO zzz

  public static IPyClass weakSetType() {
    return PyWeakSet.TYPE;
  }

  public static IPyClass proxyType() {
    return PyWeakProxy.TYPE;
  }

  public static IPyClass callableProxyType() {
    return PyCallableWeakProxy.TYPE;
  }

  public static IPyClass referenceType() {
    return PyWeakReference.TYPE;
  }
}
