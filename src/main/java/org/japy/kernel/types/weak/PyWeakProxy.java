package org.japy.kernel.types.weak;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.Args;

class PyWeakProxy implements IPyTrueAtomObj {
  static final IPyClass TYPE =
      new PyBuiltinClass(
          "weakproxy", PyWeakProxy.class, MethodHandles.lookup(), PyBuiltinClass.CALLABLE);

  @Constructor
  PyWeakProxy(Args args, PyContext ctx) {
    throw new NotImplementedException("PyWeakProxy");
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public void validate(Validator v) {}
}
