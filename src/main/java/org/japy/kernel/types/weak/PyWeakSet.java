package org.japy.kernel.types.weak;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;
import java.util.WeakHashMap;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.IPyContains;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.iter.IPyIterable;
import org.japy.kernel.types.coll.iter.PyJavaIter;
import org.japy.kernel.types.obj.proto.IPyUnhashableObj;

class PyWeakSet implements IPyUnhashableObj, IPyContains, IPyIterable {
  static final IPyClass TYPE =
      new PyBuiltinClass(
          "WeakSet", PyWeakSet.class, MethodHandles.lookup(), PyBuiltinClass.CALLABLE);

  private final WeakHashMap<IPyObj, Boolean> map = new WeakHashMap<>();

  @Constructor
  private PyWeakSet(PyContext ctx) {}

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public void validate(Validator v) {}

  @Override
  public boolean contains(IPyObj item, PyContext ctx) {
    return map.containsKey(item);
  }

  @Override
  public IPyIter iter(PyContext ctx) {
    return new PyJavaIter(map.keySet().iterator());
  }

  @InstanceMethod
  public IPyObj add(@Param("obj") IPyObj obj, PyContext ctx) {
    map.put(obj, true);
    return None;
  }
}
