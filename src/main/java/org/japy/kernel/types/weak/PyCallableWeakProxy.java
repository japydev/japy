package org.japy.kernel.types.weak;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.Args;

class PyCallableWeakProxy implements IPyTrueAtomObj {
  static final IPyClass TYPE =
      new PyBuiltinClass(
          "callableweakproxy",
          PyCallableWeakProxy.class,
          MethodHandles.lookup(),
          PyBuiltinClass.CALLABLE);

  @Constructor
  PyCallableWeakProxy(Args args, PyContext ctx) {
    throw new NotImplementedException("PyCallableWeakProxy");
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public void validate(Validator v) {}
}
