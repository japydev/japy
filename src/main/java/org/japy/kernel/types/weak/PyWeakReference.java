package org.japy.kernel.types.weak;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.japy.base.ParamKind;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyWeakReference implements IPyTrueAtomObj, IPyNoValidationObj {
  static final IPyClass TYPE =
      new PyBuiltinClass(
          "weakreference", PyWeakReference.class, MethodHandles.lookup(), PyBuiltinClass.REGULAR);

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(2)
          .add("object", ParamKind.POS_ONLY)
          .add("callback", ParamKind.POS_ONLY, None)
          .build();

  @Constructor
  @SubClassConstructor
  protected PyWeakReference(Args args, PyContext ctx) {
    IPyObj[] argVals = ARG_PARSER.parse(args, "weakreference", ctx);
    // TODO zzz
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
