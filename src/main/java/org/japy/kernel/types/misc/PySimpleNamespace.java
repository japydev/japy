package org.japy.kernel.types.misc;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.coll.str.PyStrBuilder;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.PyInstanceDict;
import org.japy.kernel.types.obj.proto.IPyObjWithEq;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.PyConstants;

public class PySimpleNamespace implements IPyTrueAtomObj, IPyObjWithRepr, IPyObjWithEq {
  public static final IPyClass TYPE =
      new PyBuiltinClass("types.SimpleNamespace", PySimpleNamespace.class, PyBuiltinClass.REGULAR);

  public final PyInstanceDict dict = new PyInstanceDict();

  public PySimpleNamespace() {}

  @SubClassConstructor
  @Constructor
  public PySimpleNamespace(Args args, PyContext ctx) {
    if (args.posCount() != 0) {
      throw PxException.typeError("SimpleNamespace() does not have positional parameters", ctx);
    }
    args.forEachKw(dict::set);
  }

  @Override
  public void validate(Validator v) {
    v.validate(dict);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public PyStr repr(PyContext ctx) {
    PyStrBuilder sb = new PyStrBuilder();
    sb.appendUcs2("namespace(");
    PrintLib.printDictContent(dict, PyConstants.EQ, false, sb, ctx);
    sb.appendUcs2(")");
    return sb.build();
  }

  @Override
  public IPyObj __eq__(IPyObj other, PyContext ctx) {
    if (!(other instanceof PySimpleNamespace)) {
      return NotImplemented;
    }
    return PyBool.of(dict.isEqual(((PySimpleNamespace) other).dict, ctx));
  }
}
