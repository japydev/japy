package org.japy.kernel.types.misc;

import static org.japy.infra.validation.Validator.check;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.validation.Validator;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.obj.proto.IPyAtomObj;

public class PySentinel implements IPyAtomObj {
  public static final PySentinel Sentinel = new PySentinel();

  private PySentinel() {}

  @Override
  public IPyClass type() {
    throw InternalErrorException.notReached();
  }

  @Override
  public void validate(Validator v) {
    check(this == Sentinel, "more than one Sentinel instance");
  }

  public static @Nullable IPyObj nullIfSentinel(IPyObj obj) {
    return obj == Sentinel ? null : obj;
  }

  public static IPyObj ifSentinel(IPyObj obj, IPyObj ifSentinel) {
    return obj == Sentinel ? ifSentinel : obj;
  }
}
