package org.japy.kernel.types.misc;

import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.num.PyBool.True;
import static org.japy.kernel.util.PyUtil.wrap;

import org.japy.infra.coll.ctx.CtxAwareCollections;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.seq.IntSlice;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.seq.SliceIndices;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.coll.str.PyStrBuilder;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.types.obj.proto.IPyObjWithEq;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.types.obj.proto.IPyTrueObj;
import org.japy.kernel.util.Args;

public class PySlice implements IPyIntHashableObj, IPyTrueObj, IPyObjWithRepr, IPyObjWithEq {
  public static final IPyClass TYPE =
      new PyBuiltinClass("slice", PySlice.class, PyBuiltinClass.CALLABLE);

  public final IPyObj start;
  public final IPyObj stop;
  public final IPyObj step;

  public PySlice(IPyObj start, IPyObj stop, IPyObj step) {
    this.start = start;
    this.stop = stop;
    this.step = step;
  }

  @Constructor
  public PySlice(Args args, PyContext ctx) {
    if (args.kwCount() != 0) {
      throw PxException.typeError("slice() does not take keyword parameters", ctx);
    }
    switch (args.posCount()) {
      case 1:
        start = None;
        stop = args.getPos(0);
        step = None;
        break;
      case 2:
        start = args.getPos(0);
        stop = args.getPos(1);
        step = None;
        break;
      case 3:
        start = args.getPos(0);
        stop = args.getPos(1);
        step = args.getPos(2);
        break;
      default:
        throw PxException.typeError("slice() takes 1, 2, or 3 arguments", ctx);
    }
  }

  public static PySlice of(IPyObj start, IPyObj end, IPyObj step) {
    return new PySlice(start, end, step);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj __eq__(IPyObj other, PyContext ctx) {
    if (this == other) {
      return True;
    }
    if (!(other instanceof PySlice)) {
      return NotImplemented;
    }
    return PyBool.of(
        start.isEqual(((PySlice) other).start, ctx)
            && stop.isEqual(((PySlice) other).stop, ctx)
            && step.isEqual(((PySlice) other).step, ctx));
  }

  @Override
  public int intHashCode(PyContext ctx) {
    return CtxAwareCollections.hash(ctx, start, stop, step);
  }

  @Override
  public String toString() {
    String s =
        (start != None ? start.toString() : "") + ":" + (stop != None ? stop.toString() : "");
    if (step != None) {
      return s + ":" + step;
    } else {
      return s;
    }
  }

  @Override
  public PyStr repr(PyContext ctx) {
    return new PyStrBuilder()
        .appendUcs2("slice(")
        .appendRepr(start, ctx)
        .appendUcs2(", ")
        .appendRepr(stop, ctx)
        .appendUcs2(", ")
        .appendRepr(step, ctx)
        .appendUcs2(")")
        .build();
  }

  @Override
  public void validate(Validator v) {
    v.validate(start);
    v.validate(stop);
    v.validate(step);
  }

  @InstanceMethod
  public IPyObj indices(@Param("len") IPyObj len, PyContext ctx) {
    IntSlice intSlice =
        new IntSlice(
            CollLib.sliceIndex(start, ctx),
            CollLib.sliceIndex(stop, ctx),
            CollLib.sliceIndex(step, ctx));
    SliceIndices indices =
        intSlice.indices(ArgLib.checkArgSmallNonNegInt("slice.indices", "len", len, ctx), ctx);
    return PyTuple.of(wrap(indices.start), wrap(indices.stop), wrap(indices.step));
  }
}
