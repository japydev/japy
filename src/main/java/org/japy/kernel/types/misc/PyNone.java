package org.japy.kernel.types.misc;

import static org.japy.infra.validation.Validator.check;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.obj.proto.IPyAtomObj;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.types.obj.proto.IPyObjWithTruth;
import org.japy.kernel.util.PyConstants;

public class PyNone implements IPyAtomObj, IPyObjWithTruth, IPyObjWithRepr {
  private static final IPyClass TYPE =
      new PyBuiltinClass("NoneType", PyNone.class, PyBuiltinClass.CALLABLE, null);

  public static final PyNone None = new PyNone();

  private PyNone() {}

  @Constructor
  public static IPyObj construct(PyContext ctx) {
    return None;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public String toString() {
    return "None";
  }

  @Override
  public boolean isTrue(PyContext ctx) {
    return false;
  }

  @Override
  public PyStr repr(PyContext ctx) {
    return PyConstants.NONE;
  }

  @Override
  public void validate(Validator v) {
    check(this == None, "more than one None instance");
  }
}
