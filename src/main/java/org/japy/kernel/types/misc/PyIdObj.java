package org.japy.kernel.types.misc;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.types.obj.proto.IPyObjWithEq;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.types.obj.proto.IPyTrueObj;

public class PyIdObj
    implements IPyIntHashableObj, IPyObjWithEq, IPyTrueObj, IPyNoValidationObj, IPyObjWithRepr {
  private static final IPyClass TYPE =
      new PyBuiltinClass("id", PyIdObj.class, PyBuiltinClass.INTERNAL);

  private final IPyObj obj;

  public PyIdObj(IPyObj obj) {
    this.obj = obj;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public int intHashCode(PyContext ctx) {
    return System.identityHashCode(obj);
  }

  @Override
  public IPyObj __eq__(IPyObj other, PyContext ctx) {
    if (!(other instanceof PyIdObj)) {
      return NotImplemented;
    }
    return PyBool.of(obj == ((PyIdObj) other).obj);
  }

  @Override
  public PyStr repr(PyContext ctx) {
    return PyStr.ucs2(
        String.format(
            "<id of <%s at %s>>", obj.getClass().getName(), System.identityHashCode(obj)));
  }
}
