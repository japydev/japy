package org.japy.kernel.types.misc;

import static org.japy.infra.validation.Validator.check;
import static org.japy.kernel.types.num.PyBool.True;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Warnings;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.obj.proto.IPyAtomObj;

public class PyNotImplemented implements IPyAtomObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "NotImplementedType", PyNotImplemented.class, PyBuiltinClass.CALLABLE, null);

  public static final PyNotImplemented NotImplemented = new PyNotImplemented();

  private PyNotImplemented() {}

  @Constructor
  public static IPyObj construct(PyContext ctx) {
    return NotImplemented;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @SpecialInstanceMethod
  public IPyObj __bool__(PyContext ctx) {
    if (ctx.kernel.config.allowNotImplementedBool) {
      Warnings.warn("bool(NotImplemented)", ctx);
      return True;
    } else {
      throw PxException.typeError("bool(NotImplemented)", ctx);
    }
  }

  @Override
  public String toString() {
    return "NotImplemented";
  }

  @Override
  public void validate(Validator v) {
    check(this == NotImplemented, "more than one NotImplemented instance");
  }
}
