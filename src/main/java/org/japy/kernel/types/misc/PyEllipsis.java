package org.japy.kernel.types.misc;

import static org.japy.infra.validation.Validator.check;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

public class PyEllipsis implements IPyTrueAtomObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass("ellipsis", PyEllipsis.class, PyBuiltinClass.CALLABLE, null);

  public static final PyEllipsis Ellipsis = new PyEllipsis();

  private PyEllipsis() {}

  @Constructor
  public static IPyObj construct(PyContext ctx) {
    return Ellipsis;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public String toString() {
    return "Ellipsis";
  }

  @Override
  public void validate(Validator v) {
    check(this == Ellipsis, "more than one Ellipsis instance");
  }
}
