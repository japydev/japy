package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;

public interface IPyObjWithComp extends IPyObjWithEq {
  @SpecialInstanceMethod
  IPyObj __lt__(@Param("other") IPyObj other, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __le__(@Param("other") IPyObj other, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __gt__(@Param("other") IPyObj other, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __ge__(@Param("other") IPyObj other, PyContext ctx);
}
