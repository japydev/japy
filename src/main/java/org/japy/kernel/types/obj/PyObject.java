package org.japy.kernel.types.obj;

import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.num.PyBool.True;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Comparisons;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.ClassMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.PyObjectClass;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.util.Constants;

public class PyObject implements IPyNoValidationObj {
  @Override
  public IPyClass type() {
    return PyObjectClass.INSTANCE;
  }

  @SpecialInstanceMethod
  public static IPyObj __eq__(IPyObj self, @Param("other") IPyObj other, PyContext ctx) {
    return self == other ? True : NotImplemented;
  }

  @SpecialInstanceMethod
  public static IPyObj __ne__(IPyObj self, @Param("other") IPyObj other, PyContext ctx) {
    IPyObj v = Comparisons.callEq(self, other, ctx);
    return v == NotImplemented ? NotImplemented : PyBool.of(!ObjLib.isTrue(v, ctx));
  }

  @SpecialInstanceMethod
  public static IPyObj __hash__(IPyObj self, PyContext ctx) {
    return PyInt.get(System.identityHashCode(self));
  }

  @Override
  public int __hash_implemented__() {
    throw InternalErrorException.notReached();
  }

  @SpecialInstanceMethod
  public static IPyObj __getattribute__(IPyObj self, @Param("attr") IPyObj attrObj, PyContext ctx) {
    String attr = ObjLib.attrName(attrObj, ctx);

    switch (attr) {
      case Constants.__DICT__:
        {
          @Nullable IPyInstanceDict instanceDict = self.instanceDict();
          if (instanceDict != null) {
            return instanceDict;
          }
          throw new PxAttributeError(attr, self, false, ctx);
        }

      case Constants.__CLASS__:
        return self.type();

      default:
        break;
    }

    @Nullable IPyInstanceDict instanceDict = self.instanceDict();
    if (instanceDict != null) {
      @Nullable IPyObj v = instanceDict.get(attr);
      if (v != null) {
        return v;
      }
    }

    return ObjLib.getClassAttribute(self.type(), self, attr, ctx);
  }

  @SpecialInstanceMethod
  public static IPyObj __setattr__(
      IPyObj self, @Param("attr") IPyObj attrNameObj, @Param("value") IPyObj value, PyContext ctx) {
    String attr = ObjLib.attrName(attrNameObj, ctx);
    switch (attr) {
      case Constants.__DICT__:
      case Constants.__CLASS__:
        throw PxAttributeError.readOnlyAttribute(attr, self, ctx);
      default:
        ObjLib.setAttrDefault(self, attr, value, ctx);
        return None;
    }
  }

  @SpecialInstanceMethod
  public static IPyObj __delattr__(IPyObj self, @Param("attr") IPyObj attrNameObj, PyContext ctx) {
    String attr = ObjLib.attrName(attrNameObj, ctx);
    switch (attr) {
      case Constants.__DICT__:
      case Constants.__CLASS__:
        throw PxAttributeError.readOnlyAttribute(attr, self, ctx);
      default:
        ObjLib.delAttrDefault(self, attr, ctx);
        return None;
    }
  }

  @SpecialInstanceMethod
  public static IPyObj __repr__(IPyObj self, PyContext ctx) {
    return PyStr.get(String.format("<%s@%d>", self.typeName(), System.identityHashCode(self)));
  }

  @SpecialInstanceMethod
  public static IPyObj __str__(IPyObj self, PyContext ctx) {
    return PrintLib.repr(self, ctx);
  }

  @SpecialInstanceMethod
  public static IPyObj __format__(
      IPyObj self, @Param("format_spec") IPyObj formatSpecArg, PyContext ctx) {
    PyStr formatSpec = ArgLib.checkArgStr("__format__", "format_spec", formatSpecArg, ctx);
    if (!formatSpec.isEmpty()) {
      throw PxException.valueError("object.__format__ does not allow non-empty format spec", ctx);
    }
    return PrintLib.str(self, ctx);
  }

  @ClassMethod
  public static IPyObj __subclasshook__(
      IPyClass cls, @Param("subclass") IPyObj subclass, PyContext ctx) {
    // TODO is this correct? Should it always be implemented?
    return NotImplemented;
  }
}
