package org.japy.kernel.types.obj;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.coll.ctx.CtxAwareSet;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.iter.PyJavaIter;
import org.japy.kernel.types.coll.iter.PyJavaTransformingIter;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.DeepRepr;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.obj.proto.IPyObjWithEq;

public abstract class PyAttrDict implements IPyAttrDict, IPyObjWithEq {
  private final Map<String, IPyObj> content;

  public PyAttrDict() {
    content = new HashMap<>();
  }

  public PyAttrDict(int expectedSize) {
    content = new HashMap<>(CtxAwareSet.capacity(expectedSize));
  }

  public PyAttrDict(IPyAttrDict dict) {
    content = new HashMap<>(CtxAwareSet.capacity(dict.len()));
    dict.forEachS(this::set);
  }

  @Override
  public boolean contains(String key) {
    return content.containsKey(key);
  }

  @Override
  public @Nullable IPyObj get(String key) {
    return content.get(key);
  }

  @Override
  public void set(String key, IPyObj value) {
    content.put(key, value);
  }

  @Override
  @CanIgnoreReturnValue
  public @Nullable IPyObj del(String key) {
    return content.remove(key);
  }

  @Override
  public IPyObj items(PyContext ctx) {
    return new PyJavaTransformingIter<>(
        content.entrySet().iterator(), e -> PyTuple.of(PyStr.get(e.getKey()), e.getValue()));
  }

  @Override
  public IPyObj keys(PyContext ctx) {
    return new PyJavaTransformingIter<>(content.keySet().iterator(), PyStr::get);
  }

  @Override
  public IPyObj values(PyContext ctx) {
    return new PyJavaIter(content.values().iterator());
  }

  @Override
  public void forEachS(BiConsumer<? super String, ? super IPyObj> func) {
    content.forEach(func);
  }

  @Override
  public Set<Map.Entry<String, IPyObj>> entrySet() {
    return content.entrySet();
  }

  @Override
  public IPyIter iter(PyContext ctx) {
    return new PyJavaTransformingIter<>(content.keySet(), PyStr::get);
  }

  @Override
  public int len() {
    return content.size();
  }

  @Override
  public int len(PyContext ctx) {
    return content.size();
  }

  @Override
  public IPyObj __eq__(IPyObj other, PyContext ctx) {
    if (!(other instanceof PyAttrDict)) {
      return NotImplemented;
    }
    Map<String, IPyObj> otherContent = ((PyAttrDict) other).content;
    if (content.size() != otherContent.size()) {
      return False;
    }
    for (Map.Entry<String, IPyObj> e : content.entrySet()) {
      @Nullable IPyObj v = otherContent.get(e.getKey());
      if (v == null || !e.getValue().isEqual(v, ctx)) {
        return False;
      }
    }
    return True;
  }

  @Override
  public void validate(Validator v) {
    forEachValue(v::validate);
  }

  @Override
  public void print(DeepRepr dr, PyContext ctx) {
    PrintLib.printDict(this, dr, ctx);
  }
}
