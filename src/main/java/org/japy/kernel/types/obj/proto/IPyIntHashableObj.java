package org.japy.kernel.types.obj.proto;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.ext.adapters.IPyIntHashableObjAdapter;
import org.japy.kernel.types.num.PyInt;

@Protocol(IPyIntHashableObjAdapter.class)
public interface IPyIntHashableObj extends IPyObj {
  @ProtocolMethod
  int intHashCode(PyContext ctx);

  @SpecialInstanceMethod
  default IPyObj __hash__(PyContext ctx) {
    return PyInt.get(intHashCode(ctx));
  }

  @Override
  default int __hash_implemented__() {
    throw InternalErrorException.notReached();
  }
}
