package org.japy.kernel.types.obj;

import org.japy.infra.validation.Validator;
import org.japy.kernel.types.IPyObj;

public interface IPyNoValidationObj extends IPyObj {
  @Override
  default void validate(Validator v) {}
}
