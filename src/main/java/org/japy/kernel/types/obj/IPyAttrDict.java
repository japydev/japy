package org.japy.kernel.types.obj;

import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyStrKeyMapping;

public interface IPyAttrDict extends IPyStrKeyMapping, IPyDict {
  int len();
}
