package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.ext.adapters.IPyObjWithReprAdapter;
import org.japy.kernel.types.coll.str.PyStr;

@Protocol(IPyObjWithReprAdapter.class)
public interface IPyObjWithRepr extends IPyObj {
  @SpecialInstanceMethod
  default IPyObj __repr__(PyContext ctx) {
    return repr(ctx);
  }

  @ProtocolMethod
  PyStr repr(PyContext ctx);
}
