package org.japy.kernel.types.obj.proto;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.DeletedSpecialMethod;

public interface IPyUnhashableObj extends IPyObj {
  @DeletedSpecialMethod
  default IPyObj __hash__(PyContext ctx) {
    throw InternalErrorException.notReached();
  }

  @Override
  default int __hash_implemented__() {
    throw InternalErrorException.notReached();
  }
}
