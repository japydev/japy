package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;

public interface IPyAtomObj extends IPyIntHashableObj {
  @Override
  default int intHashCode(PyContext ctx) {
    return System.identityHashCode(this);
  }
}
