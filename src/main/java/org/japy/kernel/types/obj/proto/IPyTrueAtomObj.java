package org.japy.kernel.types.obj.proto;

public interface IPyTrueAtomObj extends IPyAtomObj, IPyTrueObj {}
