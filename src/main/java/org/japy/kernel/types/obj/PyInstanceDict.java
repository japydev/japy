package org.japy.kernel.types.obj;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;

public class PyInstanceDict extends PyAttrDict implements IPyInstanceDict {
  private static final IPyClass TYPE =
      new PyBuiltinClass("instancedict", PyInstanceDict.class, PyBuiltinClass.INTERNAL);

  public PyInstanceDict() {}

  private PyInstanceDict(PyInstanceDict dict) {
    super(dict);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    return new PyInstanceDict(this);
  }
}
