package org.japy.kernel.types.obj.proto;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.types.IPyObj;

public interface IPyDefaultHashableObj extends IPyObj {
  @Override
  default int __hash_implemented__() {
    throw InternalErrorException.notReached();
  }
}
