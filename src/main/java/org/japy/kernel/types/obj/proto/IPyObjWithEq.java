package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;

public interface IPyObjWithEq extends IPyObj {
  @SpecialInstanceMethod
  IPyObj __eq__(@Param("other") IPyObj other, PyContext ctx);
}
