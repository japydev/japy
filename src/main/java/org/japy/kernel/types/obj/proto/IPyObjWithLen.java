package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.ext.adapters.IPyObjWithLenAdapter;
import org.japy.kernel.types.num.PyInt;

@Protocol(IPyObjWithLenAdapter.class)
public interface IPyObjWithLen extends IPyObjWithTruth {
  @ProtocolMethod
  int len(PyContext ctx);

  default boolean isEmpty(PyContext ctx) {
    return len(ctx) == 0;
  }

  @Override
  default boolean isTrue(PyContext ctx) {
    return len(ctx) != 0;
  }

  @SpecialInstanceMethod
  default IPyObj __len__(PyContext ctx) {
    return PyInt.get(len(ctx));
  }
}
