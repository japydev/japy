package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.coll.str.ObjFormatLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.func.ArgLib;

// TODO adapter
public interface IPyObjWithFormat extends IPyObj {
  PyStr format(ObjFormatLib.Spec spec, PyContext ctx);

  @SpecialInstanceMethod
  default IPyObj __format__(@Param("format_spec") IPyObj formatSpecArg, PyContext ctx) {
    String formatSpec =
        ArgLib.checkArgStr("__format__", "format_spec", formatSpecArg, ctx).toString();
    ObjFormatLib.Spec spec = ObjFormatLib.parseFormatSpec(formatSpec, this, ctx);
    return format(spec, ctx);
  }
}
