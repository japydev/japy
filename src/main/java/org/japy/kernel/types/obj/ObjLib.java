package org.japy.kernel.types.obj;

import static org.japy.infra.util.ObjUtil.or;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Comparisons;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.desc.IPyDescriptorDel;
import org.japy.kernel.types.cls.desc.IPyDescriptorGet;
import org.japy.kernel.types.cls.desc.IPyDescriptorSet;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.mod.PyModule;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.types.obj.proto.IPyObjWithTruth;
import org.japy.kernel.util.Constants;

public final class ObjLib {
  public static boolean hasAttr(IPyObj obj, String attr, PyContext ctx) {
    return getAttrOrNull(obj, attr, ctx) != null;
  }

  public static IPyObj getAttrOr(IPyObj obj, String attr, IPyObj dflt, PyContext ctx) {
    return or(getAttrOrNull(obj, attr, ctx), dflt);
  }

  public static @Nullable IPyObj getAttrOrNull(IPyObj obj, String attr, PyContext ctx) {
    try {
      return getAttr(obj, attr, ctx);
    } catch (PxAttributeError ignored) {
      return null;
    }
  }

  public static IPyObj getAttr(IPyObj obj, String attr, PyContext ctx) {
    @Nullable FuncHandle getattr;

    IPyObj attrObj = PyStr.get(attr);
    try {
      @Nullable FuncHandle getattribute = obj.specialMethod(SpecialMethod.GETATTRIBUTE);
      return (IPyObj) dcheckNotNull(getattribute).handle.invokeExact(obj, attrObj, ctx);
    } catch (PxAttributeError ex) {
      getattr = obj.specialMethod(SpecialMethod.GETATTR);
      if (getattr == null) {
        throw ex;
      }
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }

    try {
      return (IPyObj) getattr.handle.invokeExact(obj, attrObj, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  public static @Nullable IPyObj findAttributeInClassHierarchy(IPyClass cls, String attr) {
    for (IPyClass sup : cls.mro()) {
      @Nullable IPyObj v = sup.classDict().get(attr);
      if (v != null) {
        return v;
      }
    }

    return null;
  }

  public static IPyObj getAttributeValue(
      IPyObj value, IPyClass cls, @Nullable IPyObj self, PyContext ctx) {
    if (value instanceof IPyDescriptorGet) {
      return ((IPyDescriptorGet) value).get(or(self, None), cls, ctx);
    }
    @Nullable FuncHandle get = value.specialMethod(SpecialMethod.GET);
    if (get != null) {
      try {
        return (IPyObj) get.handle.invokeExact(value, or(self, None), (IPyObj) cls, ctx);
      } catch (Throwable ex) {
        throw ExcUtil.rethrow(ex);
      }
    }
    return value;
  }

  public static IPyObj getClassAttribute(
      IPyClass cls, @Nullable IPyObj self, String attr, PyContext ctx) {
    @Nullable IPyObj value = getClassAttributeOrNull(cls, self, attr, ctx);
    if (value != null) {
      return value;
    }

    throw new PxAttributeError(attr, or(self, cls), self == null || self instanceof IPyClass, ctx);
  }

  public static @Nullable IPyObj getClassAttributeOrNull(
      IPyClass cls, @Nullable IPyObj self, String attr, PyContext ctx) {
    @Nullable IPyObj value = findAttributeInClassHierarchy(cls, attr);
    if (value == null) {
      return null;
    }

    return getAttributeValue(value, cls, self, ctx);
  }

  public static void setAttrDefault(IPyObj self, String attr, IPyObj value, PyContext ctx) {
    @Nullable IPyObj maybeDescriptor = findAttributeInClassHierarchy(self.type(), attr);
    if (maybeDescriptor != null) {
      if (maybeDescriptor instanceof IPyDescriptorSet) {
        ((IPyDescriptorSet) maybeDescriptor).set(self, value, ctx);
        return;
      }

      @Nullable FuncHandle set = maybeDescriptor.specialMethodOrNone(SpecialMethod.SET);
      if (set == DELETED_METHOD) {
        throw new PxAttributeError(
            String.format("cannot set attribute '%s' of '%s' objects", attr, self.typeName()), ctx);
      }

      if (set != null) {
        try {
          set.handle.invokeExact(maybeDescriptor, self, value, ctx);
        } catch (Throwable ex) {
          throw ExcUtil.rethrow(ex);
        }
        return;
      }

      if (maybeDescriptor instanceof IPyDescriptorDel
          || maybeDescriptor.specialMethodOrNone(SpecialMethod.DELETE) != null) {
        throw new PxAttributeError(
            String.format("cannot set attribute '%s' of '%s' objects", attr, self.typeName()), ctx);
      }
    }

    @Nullable IPyInstanceDict instanceDict = self.instanceDict();
    if (instanceDict != null) {
      instanceDict.set(attr, value);
      return;
    }

    throw new PxAttributeError(
        String.format("cannot set attribute '%s' of '%s' objects", attr, self.typeName()), ctx);
  }

  public static void setAttr(IPyObj self, String attr, IPyObj value, PyContext ctx) {
    @Nullable FuncHandle setattr = self.specialMethod(SpecialMethod.SETATTR);
    if (setattr != null) {
      try {
        setattr.handle.invokeExact(self, (IPyObj) PyStr.get(attr), value, ctx);
        return;
      } catch (Throwable ex) {
        throw ExcUtil.rethrow(ex);
      }
    }

    setAttrDefault(self, attr, value, ctx);
  }

  public static void delAttrDefault(IPyObj self, String attr, PyContext ctx) {
    @Nullable IPyObj maybeDescriptor = findAttributeInClassHierarchy(self.type(), attr);
    if (maybeDescriptor != null) {
      if (maybeDescriptor instanceof IPyDescriptorDel) {
        ((IPyDescriptorDel) maybeDescriptor).delete(self, ctx);
        return;
      }

      @Nullable FuncHandle delete = maybeDescriptor.specialMethod(SpecialMethod.DELETE);
      if (delete != null) {
        try {
          delete.handle.invokeExact(maybeDescriptor, self, ctx);
          return;
        } catch (Throwable ex) {
          throw ExcUtil.rethrow(ex);
        }
      }

      if (maybeDescriptor instanceof IPyDescriptorSet
          || maybeDescriptor.specialMethodOrNone(SpecialMethod.SET) != null) {
        throw new PxAttributeError(
            String.format("'%s' object has no attribute '%s'", self.typeName(), attr), ctx);
      }
    }

    @Nullable IPyInstanceDict instanceDict = self.instanceDict();
    if (instanceDict != null) {
      if (instanceDict.popOrNull(attr) == null) {
        throw new PxAttributeError(
            String.format("'%s' object has no attribute '%s'", self.typeName(), attr), ctx);
      }
      return;
    }

    throw new PxAttributeError(
        String.format("'%s' object has no attribute '%s'", self.typeName(), attr), ctx);
  }

  public static void delAttr(IPyObj self, String attr, PyContext ctx) {
    @Nullable FuncHandle delattr = self.specialMethod(SpecialMethod.DELATTR);
    if (delattr != null) {
      try {
        delattr.handle.invokeExact(self, (IPyObj) PyStr.get(attr), ctx);
        return;
      } catch (Throwable ex) {
        throw ExcUtil.rethrow(ex);
      }
    }

    delAttrDefault(self, attr, ctx);
  }

  public static boolean isTrue(IPyObj obj, PyContext ctx) {
    if (obj == True) {
      return true;
    } else if (obj == False) {
      return false;
    }
    if (obj instanceof IPyObjWithTruth) {
      return ((IPyObjWithTruth) obj).isTrue(ctx);
    }
    return isTrueMeta(obj, ctx);
  }

  public static IPyObj not(IPyObj obj, PyContext ctx) {
    return isTrue(obj, ctx) ? False : True;
  }

  private static boolean isTrueMeta(IPyObj obj, PyContext ctx) {
    @Nullable FuncHandle bool = obj.specialMethodOrNone(SpecialMethod.BOOL);
    if (bool == DELETED_METHOD) {
      throw PxException.typeError(
          String.format("'%s' objects may not be evaluated in boolean context", obj.typeName()),
          ctx);
    }
    if (bool != null) {
      try {
        return (boolean) bool.handle.invokeExact(obj, ctx);
      } catch (Throwable ex) {
        throw ExcUtil.rethrow(ex);
      }
    }

    @Nullable FuncHandle len = obj.specialMethod(SpecialMethod.LEN);
    if (len != null) {
      try {
        return ((int) len.handle.invokeExact(obj, ctx)) != 0;
      } catch (Throwable ex) {
        throw ExcUtil.rethrow(ex);
      }
    }

    return true;
  }

  public static PxException throwUnhashable(IPyObj obj, PyContext ctx) {
    throw PxException.typeError(
        String.format("objects of type '%s' are not hashable", obj.typeName()), ctx);
  }

  public static boolean isEqual(IPyObj lhs, IPyObj rhs, PyContext ctx) {
    return isTrue(Comparisons.eq(lhs, rhs, ctx), ctx);
  }

  public static int hashCode(IPyObj obj, PyContext ctx) {
    if (obj instanceof IPyIntHashableObj) {
      return ((IPyIntHashableObj) obj).intHashCode(ctx);
    }

    @Nullable FuncHandle hash = obj.specialMethod(SpecialMethod.HASH);
    if (hash == null) {
      throw throwUnhashable(obj, ctx);
    }

    try {
      return (int) hash.handle.invokeExact(obj, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  public static boolean isInstance(IPyObj obj, IPyObj classInfo, PyContext ctx) {
    if (classInfo instanceof PyTuple) {
      PyTuple tup = (PyTuple) classInfo;
      for (int i = 0, c = tup.len(); i != c; ++i) {
        if (isInstance(obj, tup.get(i), ctx)) {
          return true;
        }
      }
      return false;
    }

    if (!(classInfo instanceof IPyClass)) {
      throw PxException.typeError("expected a class or a tuple of classes", ctx);
    }

    @Nullable FuncHandle instanceCheck = classInfo.specialMethod(SpecialMethod.INSTANCECHECK);
    if (instanceCheck != null) {
      try {
        return (boolean) instanceCheck.handle.invokeExact(classInfo, obj, ctx);
      } catch (Throwable ex) {
        throw ExcUtil.rethrow(ex);
      }
    }

    return obj.isInstanceRaw((IPyClass) classInfo);
  }

  public static boolean isSubclass(IPyObj cls, IPyObj classInfo, PyContext ctx) {
    if (!(cls instanceof IPyClass)) {
      throw PxException.typeError("class must be a class", ctx);
    }

    if (classInfo instanceof PyTuple) {
      PyTuple tup = (PyTuple) classInfo;
      for (int i = 0, c = tup.len(); i != c; ++i) {
        if (isSubclass(cls, tup.get(i), ctx)) {
          return true;
        }
      }
      return false;
    }

    if (!(classInfo instanceof IPyClass)) {
      throw PxException.typeError("classinfo must be a class or a tuple of classes", ctx);
    }

    @Nullable FuncHandle subclassCheck = classInfo.specialMethod(SpecialMethod.SUBCLASSCHECK);
    if (subclassCheck != null) {
      try {
        return (boolean) subclassCheck.handle.invokeExact(classInfo, cls, ctx);
      } catch (Throwable ex) {
        throw ExcUtil.rethrow(ex);
      }
    }

    return ((IPyClass) cls).isSubClassRaw((IPyClass) classInfo);
  }

  public static IPyObj dir(IPyObj obj, PyContext ctx) {
    IPyObj dir = getAttrOr(obj, Constants.__DIR__, None, ctx);
    if (dir != None) {
      return FuncLib.call(dir, ctx);
    }

    if (obj instanceof PyModule) {
      return CollLib.sortedKeys(((PyModule) obj).moduleDict(), ctx);
    }

    // TODO
    @Nullable IPyInstanceDict dict = obj.instanceDict();
    if (dict != null) {
      return CollLib.sortedKeys(dict, ctx);
    }

    return new PyList();
  }

  public static String attrName(IPyObj attr, PyContext ctx) {
    if (!(attr instanceof PyStr)) {
      throw PxException.typeError("attribute name must be a string", ctx);
    }
    return attr.toString();
  }

  @SuppressWarnings("UnusedException")
  public static IPyObj vars(IPyObj obj, PyContext ctx) {
    try {
      return getAttr(obj, Constants.__DICT__, ctx);
    } catch (PxAttributeError ignored) {
      throw PxException.typeError(
          String.format("objects of type '%s' do not have __dict__", obj.typeName()), ctx);
    }
  }

  public static boolean isAbstractMethod(IPyObj func, PyContext ctx) {
    return isTrue(ObjLib.getAttrOr(func, Constants.__ISABSTRACTMETHOD__, False, ctx), ctx);
  }
}
