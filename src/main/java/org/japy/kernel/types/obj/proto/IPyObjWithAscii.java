package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.ext.adapters.IPyObjWithAsciiAdapter;
import org.japy.kernel.types.coll.str.PyStr;

@Protocol(IPyObjWithAsciiAdapter.class)
public interface IPyObjWithAscii extends IPyObj {
  @SpecialInstanceMethod
  default IPyObj __ascii__(PyContext ctx) {
    return ascii(ctx);
  }

  @ProtocolMethod
  PyStr ascii(PyContext ctx);
}
