package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.ext.adapters.IPyObjWithStrAdapter;
import org.japy.kernel.types.coll.str.PyStr;

@Protocol(IPyObjWithStrAdapter.class)
public interface IPyObjWithStr extends IPyObj {
  @SpecialInstanceMethod
  default IPyObj __str__(PyContext ctx) {
    return str(ctx);
  }

  @ProtocolMethod
  PyStr str(PyContext ctx);
}
