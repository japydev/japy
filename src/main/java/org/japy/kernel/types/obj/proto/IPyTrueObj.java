package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;

public interface IPyTrueObj extends IPyObjWithTruth {
  @Override
  default boolean isTrue(PyContext ctx) {
    return true;
  }
}
