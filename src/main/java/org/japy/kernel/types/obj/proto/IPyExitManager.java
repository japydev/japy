package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;

public interface IPyExitManager extends IPyObj {
  @InstanceMethod
  IPyObj __enter__(PyContext ctx);

  @InstanceMethod
  IPyObj __exit__(
      @Param("type") IPyObj excType,
      @Param("value") IPyObj exc,
      @Param("tb") IPyObj tb,
      PyContext ctx);
}
