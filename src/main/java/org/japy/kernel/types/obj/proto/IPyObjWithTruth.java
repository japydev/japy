package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.ext.adapters.IPyObjWithTruthAdapter;
import org.japy.kernel.types.num.PyBool;

@Protocol(IPyObjWithTruthAdapter.class)
public interface IPyObjWithTruth extends IPyObj {
  @ProtocolMethod
  boolean isTrue(PyContext ctx);

  @SpecialInstanceMethod
  default IPyObj __bool__(PyContext ctx) {
    return PyBool.of(isTrue(ctx));
  }
}
