package org.japy.kernel.types.obj.proto;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.cls.ext.adapters.IPyObjWithDeepReprAdapter;
import org.japy.kernel.types.coll.str.DeepRepr;
import org.japy.kernel.types.coll.str.PyStr;

@Protocol(IPyObjWithDeepReprAdapter.class)
public interface IPyObjWithDeepRepr extends IPyObjWithRepr {
  @ProtocolMethod
  default PyStr deepRepr(PyContext ctx) {
    DeepRepr dr = new DeepRepr();
    dr.append(this, ctx);
    return dr.sb.build();
  }

  @Override
  default PyStr repr(PyContext ctx) {
    return deepRepr(ctx);
  }

  void print(DeepRepr dr, PyContext ctx);
}
