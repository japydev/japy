package org.japy.kernel.types.num;

import static org.japy.kernel.types.misc.PyNone.None;

import java.math.BigInteger;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Comparisons;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.Constants;

public class NumLib {
  static final BigInteger BIG_ONE = BigInteger.valueOf(1);
  private static final BigInteger BIG_MIN_LONG = BigInteger.valueOf(Long.MIN_VALUE);
  private static final BigInteger BIG_MAX_LONG = BigInteger.valueOf(Long.MAX_VALUE);

  // TODO
  static int hashCode(int value) {
    return Integer.hashCode(value);
  }

  // TODO
  static int hashCode(long value) {
    return ((int) value == value) ? hashCode((int) value) : Long.hashCode(value);
  }

  // TODO
  static int hashCode(BigInteger value) {
    switch (value.signum()) {
      case 0:
        return hashCode(0);
      case 1:
        return value.compareTo(BIG_MAX_LONG) <= 0 ? hashCode(value.longValue()) : value.hashCode();
      case -1:
        return value.compareTo(BIG_MIN_LONG) >= 0 ? hashCode(value.longValue()) : value.hashCode();
      default:
        throw InternalErrorException.notReached("bad signum");
    }
  }

  // TODO
  static int hashCode(double value) {
    if ((long) value == value) {
      return hashCode((long) value);
    }
    return Double.hashCode(value);
  }

  @SuppressWarnings("UnusedException")
  static IPyObj getMethod(IPyObj number, String name, PyContext ctx) {
    @Nullable IPyObj meth = getMethodIfExists(number, name, ctx);
    if (meth == null) {
      throw PxException.typeError(
          String.format("type %s doesn't define %s method", number.typeName(), name), ctx);
    }
    return meth;
  }

  static @Nullable IPyObj getMethodIfExists(IPyObj number, String name, PyContext ctx) {
    return ObjLib.getClassAttributeOrNull(number.type(), null, name, ctx);
  }

  public static IPyObj round(IPyObj number, IPyObj ndigits, PyContext ctx) {
    return FuncLib.call(getMethod(number, Constants.__ROUND__, ctx), number, ndigits, ctx);
  }

  public static IPyObj round(IPyObj number, PyContext ctx) {
    return FuncLib.call(getMethod(number, Constants.__ROUND__, ctx), number, ctx);
  }

  public static IPyObj trunc(IPyObj number, PyContext ctx) {
    return FuncLib.call(getMethod(number, Constants.__TRUNC__, ctx), number, ctx);
  }

  public static IPyObj floor(IPyObj number, PyContext ctx) {
    return FuncLib.call(getMethod(number, Constants.__FLOOR__, ctx), number, ctx);
  }

  public static IPyObj ceil(IPyObj number, PyContext ctx) {
    return FuncLib.call(getMethod(number, Constants.__CEIL__, ctx), number, ctx);
  }

  private static IPyObj minMaxValue(IPyObj value, IPyObj key, PyContext ctx) {
    return key != None ? FuncLib.call(key, value, ctx) : value;
  }

  private interface MinMaxCompare {
    IPyObj takeNext(IPyObj cur, IPyObj next, PyContext ctx);
  }

  private static IPyObj minMax(
      IPyObj iterable, IPyObj key, IPyObj dflt, MinMaxCompare compare, PyContext ctx) {
    @Var
    @Nullable
    IPyObj cur = null;
    for (IPyObj value : new JavaIterable(iterable, ctx)) {
      IPyObj next = minMaxValue(value, key, ctx);
      if (cur == null || ObjLib.isTrue(compare.takeNext(cur, next, ctx), ctx)) {
        cur = next;
      }
    }
    if (cur == null) {
      if (dflt == None) {
        throw PxException.valueError("iterable is empty but default value is not provided", ctx);
      }
      cur = dflt;
    }
    return cur;
  }

  public static IPyObj min(IPyObj iterable, IPyObj key, IPyObj dflt, PyContext ctx) {
    return minMax(
        iterable, key, dflt, (curMin, next, __ctx) -> Comparisons.lt(next, curMin, __ctx), ctx);
  }

  public static IPyObj max(IPyObj iterable, IPyObj key, IPyObj dflt, PyContext ctx) {
    return minMax(iterable, key, dflt, Comparisons::lt, ctx);
  }

  private static IPyObj minMax(PyTuple args, IPyObj key, MinMaxCompare compare, PyContext ctx) {
    @Var IPyObj cur = minMaxValue(args.get(0), key, ctx);
    for (int i = 1, c = args.len(); i != c; ++i) {
      IPyObj next = minMaxValue(args.get(i), key, ctx);
      if (ObjLib.isTrue(compare.takeNext(cur, next, ctx), ctx)) {
        cur = next;
      }
    }
    return cur;
  }

  public static IPyObj min(PyTuple args, IPyObj key, PyContext ctx) {
    if (args.len() == 2) {
      IPyObj lhs = args.get(0);
      IPyObj rhs = args.get(1);
      return ObjLib.isTrue(Comparisons.lt(lhs, rhs, ctx), ctx) ? lhs : rhs;
    }
    return minMax(args, key, (cur, next, __ctx) -> Comparisons.lt(next, cur, __ctx), ctx);
  }

  public static IPyObj max(PyTuple args, IPyObj key, PyContext ctx) {
    if (args.len() == 2) {
      IPyObj lhs = args.get(0);
      IPyObj rhs = args.get(1);
      return ObjLib.isTrue(Comparisons.lt(lhs, rhs, ctx), ctx) ? rhs : lhs;
    }
    return minMax(args, key, Comparisons::lt, ctx);
  }
}
