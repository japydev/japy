package org.japy.kernel.types.num;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Locale;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.ObjFormatLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxOverflowError;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.Constants;

public class FloatLib extends NumLib {
  public static PyFloat toFloat(IPyObj obj, PyContext ctx) {
    if (obj.type() == PyStr.TYPE) {
      return toFloat(obj.toString(), ctx);
    }

    return toFloatNoString(obj, ctx);
  }

  private static PyFloat toFloatNoString(IPyObj obj, PyContext ctx) {
    if (obj.type() == PyFloat.TYPE) {
      return (PyFloat) obj;
    }

    if (obj.type() == PyInt.TYPE) {
      return new PyFloat(IntLib.toDouble((PyInt) obj, ctx));
    }

    @Nullable
    IPyObj floatMeth = ObjLib.getClassAttributeOrNull(obj.type(), null, Constants.__FLOAT__, ctx);
    if (floatMeth != null) {
      IPyObj value = FuncLib.call(floatMeth, obj, ctx);
      if (!(value instanceof PyFloat)) {
        throw PxException.typeError(
            String.format(
                "__float__ returned an instance of type '%s', expected a float", value.typeName()),
            ctx);
      } else if (value.type() != PyFloat.TYPE) {
        // TODO deprecation warning
        // -- Foo3.__float__ returned non-float (type Foo3).  The ability to return an instance of a
        // strict subclass of float is deprecated, and may be removed in a future version of Python
        return new PyFloat(((PyFloat) value).value);
      }
      return (PyFloat) value;
    }

    @Nullable
    IPyObj index = ObjLib.getClassAttributeOrNull(obj.type(), null, Constants.__INDEX__, ctx);
    if (index != null) {
      IPyObj value = FuncLib.call(index, obj, ctx);
      if (value.type() != PyInt.TYPE) {
        throw PxException.typeError(
            String.format(
                "__index__ returned an instance of type '%s', expected an int", value.typeName()),
            ctx);
      }
      return toFloat(value, ctx);
    }

    throw PxException.typeError(String.format("can't convert %s to float", obj.typeName()), ctx);
  }

  private static @Nullable PyFloat floatConstant(String s) {
    switch (s) {
      case "inf":
      case "infinity":
      case "+inf":
      case "+infinity":
        return PyFloat.POSITIVE_INFINITY;
      case "-inf":
      case "-infinity":
        return PyFloat.NEGATIVE_INFINITY;
      case "nan":
      case "+nan":
      case "-nan":
        return PyFloat.NAN;
      default:
        return null;
    }
  }

  @SuppressWarnings("UnusedException")
  private static PyFloat toFloat(String string, PyContext ctx) {
    // TODO
    String normalized = string.toLowerCase(Locale.ROOT).strip();
    @Nullable PyFloat f = floatConstant(normalized);
    if (f != null) {
      return f;
    }
    try {
      return new PyFloat(Double.parseDouble(normalized));
    } catch (NumberFormatException ignored) {
      throw PxException.valueError(
          String.format("could not convert string to float: '%s'", string), ctx);
    }
  }

  @SuppressWarnings("UnusedException")
  static IPyObj fromHex(IPyClass cls, @Var String string, PyContext ctx) {
    string = string.strip().toLowerCase(Locale.ROOT);
    @Nullable PyFloat f = floatConstant(string);
    if (f != null) {
      return f;
    }
    int sign = string.startsWith("+") ? 1 : (string.startsWith("-") ? -1 : 0);
    if (sign != 0) {
      string = string.substring(1);
    }
    if (!string.startsWith("0x")) {
      string = "0x" + string;
    }
    if (string.indexOf('p') == -1) {
      string = string + "p0";
    }
    @Var double v;
    try {
      v = Double.parseDouble(string);
    } catch (NumberFormatException ignored) {
      throw PxException.valueError("invalid hexadecimal floating-point string", ctx);
    }
    if (!Double.isFinite(v)) {
      throw new PxOverflowError("hexadecimal value too large to represent as a float", ctx);
    }
    if (sign == -1) {
      v = -v;
    }
    return cls == PyFloat.TYPE ? new PyFloat(v) : FuncLib.call(cls, new PyFloat(v), ctx);
  }

  static PyStr hex(double value) {
    return PyStr.ucs2(Double.toHexString(value));
  }

  static IPyObj asIntegerRatio(double value, PyContext ctx) {
    if (Double.isNaN(value)) {
      throw PxException.valueError("cannot convert NaN to integer ratio", ctx);
    } else if (Double.isInfinite(value)) {
      throw new PxOverflowError("cannot convert Infinity to integer ratio", ctx);
    } else if (value == 0) {
      return PyTuple.of(PyInt.ZERO, PyInt.ONE);
    }

    long bits = Double.doubleToRawLongBits(value);
    boolean negative = (bits & 0x8000_0000_0000_0000L) != 0;
    @Var int exponent = (int) ((bits >> 52) & 0x7FFL);
    @Var long mantissa = bits & 0xF_FFFF_FFFF_FFFFL;

    if (exponent == 0) {
      // Subnormal numbers; exponent is effectively one higher,
      // but there's no extra normalisation bit in the mantissa
      exponent++;
    } else {
      // Normal numbers; leave exponent as it is but add extra
      // bit to the front of the mantissa
      mantissa = mantissa | (1L << 52);
    }

    // Bias the exponent. It's actually biased by 1023, but we're
    // treating the mantissa as m.0 rather than 0.m, so we need
    // to subtract another 52 from it.
    exponent -= 1075;
    @Var double float_part = negative ? (-1 * mantissa) : mantissa;

    // Normalize
    while ((mantissa & 1) == 0) {
      mantissa >>= 1;
      float_part /= 2;
      exponent++;
    }

    // TODO zzz I don't know what I am doing here

    for (int i = 0; i < 300 && float_part != Math.floor(float_part); i++) {
      float_part *= 2.0;
      exponent--;
    }

    long numerator = (long) float_part;

    PyInt numObj;
    PyInt denomObj;
    if (exponent > 0) {
      numObj = PyInt.getMinimal(BigInteger.valueOf(numerator).shiftLeft(exponent));
      denomObj = PyInt.ONE;
    } else {
      denomObj = PyInt.getMinimal(BIG_ONE.shiftLeft(-exponent));
      numObj = PyInt.get(numerator);
    }

    return PyTuple.of(numObj, denomObj);
  }

  static boolean isInteger(double value) {
    return Double.isFinite(value) && Math.floor(value) == value;
  }

  static IPyObj round(double value) {
    // TODO zzz
    return PyInt.get(Math.round(value));
  }

  static IPyObj round(double value, int ndigits) {
    // TODO zzz
    int log10 = (int) Math.floor(Math.log10(Math.abs(value)));
    if (ndigits + log10 < -1) {
      return PyFloat.ZERO;
    } else if (ndigits + log10 == -1) {
      return new PyFloat(
          new BigDecimal(value).round(new MathContext(1, RoundingMode.HALF_EVEN)).doubleValue());
    } else {
      return new PyFloat(
          new BigDecimal(value)
              .round(new MathContext(ndigits + log10 + 1, RoundingMode.HALF_EVEN))
              .doubleValue());
    }
  }

  static IPyObj trunc(double value) {
    throw new NotImplementedException("trunc");
  }

  static IPyObj floor(double value, PyContext ctx) {
    return IntLib.convertToInt(Math.floor(value), ctx);
  }

  static IPyObj ceil(double value, PyContext ctx) {
    return IntLib.convertToInt(Math.ceil(value), ctx);
  }

  public static PyStr format(PyFloat self, ObjFormatLib.NumSpec spec, PyContext ctx) {
    if (!spec.isEmpty()) {
      throw new NotImplementedException("format");
    }
    return PyStr.get(PyFloat.repr(self.value));
  }

  static double doubleForOps(IPyObj obj, PyContext ctx) {
    return toFloatNoString(obj, ctx).value;
  }

  public static IPyObj copysign(IPyObj magnitudeObj, IPyObj signObj, PyContext ctx) {
    double magnitude = doubleForOps(magnitudeObj, ctx);
    double sign = doubleForOps(signObj, ctx);
    double result = Math.copySign(magnitude, sign);
    return magnitude == result && magnitudeObj.type() == PyFloat.TYPE
        ? magnitudeObj
        : new PyFloat(result);
  }

  public static IPyObj ldexp(IPyObj x, IPyObj i, PyContext ctx) {
    return new PyFloat(
        Math.scalb(doubleForOps(x, ctx), ArgLib.checkArgSmallInt("ldexp", "i", i, ctx)));
  }

  public static IPyObj frexp(IPyObj x, PyContext ctx) {
    double value = doubleForOps(x, ctx);
    if (!Double.isFinite(value) || value == 0) {
      return PyTuple.of(x, PyInt.ZERO);
    }

    // https://stackoverflow.com/questions/389993/extracting-mantissa-and-exponent-from-double-in-c-sharp/390072#390072

    long bits = Double.doubleToRawLongBits(value);
    boolean negative = (bits & 0x8000_0000_0000_0000L) != 0;
    @Var int exponent = (int) ((bits >> 52) & 0x7FFL);
    @Var long mantissa = bits & 0xF_FFFF_FFFF_FFFFL;

    if (exponent == 0) {
      // Subnormal numbers; exponent is effectively one higher,
      // but there's no extra normalisation bit in the mantissa
      exponent++;
    } else {
      // Normal numbers; leave exponent as it is but add extra
      // bit to the front of the mantissa
      mantissa = mantissa | (1L << 52);
    }

    // Bias the exponent. It's actually biased by 1023, but we're
    // treating the mantissa as m.0 rather than 0.m, so we need
    // to subtract another 52 from it.
    exponent -= 1075;
    @Var double resultMantissa = negative ? (-1 * mantissa) : mantissa;

    // Normalize
    while ((mantissa & 1) == 0) {
      mantissa >>= 1;
      resultMantissa /= 2;
      exponent++;
    }

    return PyTuple.of(new PyFloat(resultMantissa), PyInt.get(exponent));
  }
}
