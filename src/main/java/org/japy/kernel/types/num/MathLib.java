package org.japy.kernel.types.num;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;

public class MathLib {
  public static IPyObj atan2(IPyObj yArg, IPyObj xArg, PyContext ctx) {
    throw new NotImplementedException("atan2");
  }

  public static IPyObj atan(IPyObj xArg, PyContext ctx) {
    throw new NotImplementedException("atan");
  }
}
