package org.japy.kernel.types.num;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.exc.px.PxZeroDivisionError;

class ComplexOps extends NumOps {
  static IPyObj abs(PyComplex a) {
    return new PyFloat(a.abs());
  }

  static IPyObj add(PyComplex lhs, double rhs) {
    return rhs == 0 ? lhs : lhs.add(rhs);
  }

  static IPyObj add(PyComplex lhs, PyComplex rhs) {
    return lhs.add(rhs);
  }

  static IPyObj mul(PyComplex lhs, double rhs) {
    return lhs.multiply(rhs);
  }

  static IPyObj mul(PyComplex lhs, PyComplex rhs) {
    return lhs.multiply(rhs);
  }

  static IPyObj sub(PyComplex lhs, double rhs) {
    return lhs.subtract(rhs);
  }

  static IPyObj sub(PyComplex lhs, PyComplex rhs) {
    return lhs.subtract(rhs);
  }

  static IPyObj truediv(PyComplex lhs, double rhs, PyContext ctx) {
    if (rhs == 0) {
      throw new PxZeroDivisionError(ctx);
    }
    return lhs.divide(rhs);
  }

  static IPyObj truediv(PyComplex lhs, PyComplex rhs, PyContext ctx) {
    if (rhs.isZero()) {
      throw new PxZeroDivisionError(ctx);
    }
    return lhs.divide(rhs);
  }

  static IPyObj floordiv(PyComplex lhs, PyComplex rhs, PyContext ctx) {
    throw new NotImplementedException("floordiv");
  }

  static IPyObj floordiv(PyComplex lhs, double rhs, PyContext ctx) {
    throw new NotImplementedException("floordiv");
  }

  static IPyObj pow(PyComplex lhs, double rhs, @SuppressWarnings("unused") PyContext ctx) {
    return lhs.pow(rhs);
  }

  static IPyObj pow(PyComplex lhs, PyComplex rhs, @SuppressWarnings("unused") PyContext ctx) {
    return lhs.pow(rhs);
  }

  static IPyObj neg(PyComplex self) {
    return self.negate();
  }

  // BEGIN GENERATED CODE

  static IPyObj add(PyComplex self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return add(self, IntLib.toDouble((PyInt) obj, ctx));
    } else if (obj instanceof PyFloat) {
      return add(self, ((PyFloat) obj).value);
    } else if (obj instanceof PyComplex) {
      return add(self, (PyComplex) obj);
    }
    return NotImplemented;
  }

  static IPyObj sub(PyComplex self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return sub(self, IntLib.toDouble((PyInt) obj, ctx));
    } else if (obj instanceof PyFloat) {
      return sub(self, ((PyFloat) obj).value);
    } else if (obj instanceof PyComplex) {
      return sub(self, (PyComplex) obj);
    }
    return NotImplemented;
  }

  static IPyObj mul(PyComplex self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return mul(self, IntLib.toDouble((PyInt) obj, ctx));
    } else if (obj instanceof PyFloat) {
      return mul(self, ((PyFloat) obj).value);
    } else if (obj instanceof PyComplex) {
      return mul(self, (PyComplex) obj);
    }
    return NotImplemented;
  }

  static IPyObj truediv(PyComplex self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return truediv(self, IntLib.toDouble((PyInt) obj, ctx), ctx);
    } else if (obj instanceof PyFloat) {
      return truediv(self, ((PyFloat) obj).value, ctx);
    } else if (obj instanceof PyComplex) {
      return truediv(self, (PyComplex) obj, ctx);
    }
    return NotImplemented;
  }

  static IPyObj pow(PyComplex self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return pow(self, IntLib.toDouble((PyInt) obj, ctx), ctx);
    } else if (obj instanceof PyFloat) {
      return pow(self, ((PyFloat) obj).value, ctx);
    } else if (obj instanceof PyComplex) {
      return pow(self, (PyComplex) obj, ctx);
    }
    return NotImplemented;
  }

  // END GENERATED CODE
}
