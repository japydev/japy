package org.japy.kernel.types.num;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.proto.IPyObjWithComp;

public interface IPyRealNumber extends IPyNumber, IPyObjWithComp {
  IPyObj zero();

  IPyObj one();

  @Override
  default IPyObj real(PyContext ctx) {
    return this;
  }

  @Override
  default IPyObj imag(PyContext ctx) {
    return zero();
  }

  @Override
  default IPyObj conjugate(PyContext ctx) {
    return this;
  }

  @SpecialInstanceMethod
  IPyObj __mod__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __divmod__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __floordiv__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @InstanceMethod
  IPyObj __int__(PyContext ctx);

  @InstanceMethod
  IPyObj __float__(PyContext ctx);

  IPyObj round(PyContext ctx);

  IPyObj round(int ndigits, PyContext ctx);

  @InstanceMethod
  default IPyObj __round__(
      @Param(value = "ndigits", dflt = DEFAULT_SENTINEL) IPyObj ndigitsArg, PyContext ctx) {
    if (ndigitsArg == Sentinel || ndigitsArg == None) {
      return round(ctx);
    } else {
      int ndigits = ArgLib.checkArgSmallNonNegInt("__round__", "ndigits", ndigitsArg, ctx);
      return round(ndigits, ctx);
    }
  }

  @InstanceMethod
  IPyObj __trunc__(PyContext ctx);

  @InstanceMethod
  IPyObj __floor__(PyContext ctx);

  @InstanceMethod
  IPyObj __ceil__(PyContext ctx);
}
