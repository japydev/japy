package org.japy.kernel.types.num;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Validator.check;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_FALSE;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;

import java.math.BigInteger;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.ClassMethod;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.IPyBuiltinClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.ObjFormatLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyInt implements IPyIntHashableObj, IPyObjWithRepr, IPyInteger {
  public static final PyBuiltinClass TYPE =
      new PyBuiltinClass(
          "int",
          PyInt.class,
          PyBuiltinClass.REGULAR,
          "int([x]) -> integer\n"
              + "int(x, base=10) -> integer\n"
              + "\n"
              + "Convert a number or string to an integer, or return 0 if no arguments\n"
              + "are given.  If x is a number, return x.__int__().  For floating point\n"
              + "numbers, this truncates towards zero.\n"
              + "\n"
              + "If x is not a number or if base is given, then x must be a string,\n"
              + "bytes, or bytearray instance representing an integer literal in the\n"
              + "given base.  The literal can be preceded by '+' or '-' and be surrounded\n"
              + "by whitespace.  The base defaults to 10.  Valid bases are 0 and 2-36.\n"
              + "Base 0 means to interpret the base from the string as an integer literal.\n"
              + ">>> int('0b100', base=0)\n"
              + "4");

  public static final PyInt ZERO = new PyInt(0);
  public static final PyInt ONE = new PyInt(1);
  public static final PyInt TWO = new PyInt(2);
  public static final PyInt NEGATIVE_ONE = new PyInt(-1);
  public static final PyInt NEGATIVE_TWO = new PyInt(-2);

  private static final int N_SMALL = 64;
  private static final PyInt[] SMALL_POS = new PyInt[N_SMALL];
  private static final PyInt[] SMALL_NEG = new PyInt[N_SMALL];

  static {
    SMALL_POS[0] = ZERO;
    SMALL_POS[1] = ONE;
    SMALL_POS[2] = TWO;
    SMALL_NEG[0] = ZERO;
    SMALL_NEG[1] = NEGATIVE_ONE;
    SMALL_NEG[2] = NEGATIVE_TWO;
    for (int i = 3; i < N_SMALL; ++i) {
      SMALL_POS[i] = new PyInt(i);
      SMALL_NEG[i] = new PyInt(-i);
    }
  }

  private static final int MASK_LONG = 0x1;
  private static final int MASK_BIG = 0x2;
  private static final int MASK_SIGN = 0x8000_0000;

  final int data;
  final @Nullable Object obj;

  private PyInt(int data, @Nullable Object obj) {
    this.data = data;
    this.obj = obj;
  }

  protected PyInt(int value) {
    data = value;
    obj = null;
  }

  PyInt(PyInt value) {
    data = value.data;
    obj = value.obj;
  }

  @SubClassConstructor
  protected PyInt(Args args, PyContext ctx) {
    IPyObj i = construct(args, ctx);
    dcheck(i instanceof PyInt);
    data = ((PyInt) i).data;
    obj = ((PyInt) i).obj;
  }

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(2).add("x", ParamKind.POS_ONLY).add("base", PyInt.get(10)).build();

  @Constructor
  public static IPyObj construct(Args args, PyContext ctx) {
    if (args.isEmpty()) {
      return PyInt.ZERO;
    }

    if (args.posCount() == 1 && args.kwCount() == 0) {
      return IntLib.convertToInt(args.getPos(0), ctx);
    }

    IPyObj[] argVals = ARG_PARSER.parse(args, "int", ctx);
    return IntLib.convertToInt(argVals[0], argVals[1], ctx);
  }

  public static PyInt get(int value) {
    if (value >= 0 && value < N_SMALL) {
      return SMALL_POS[value];
    } else if (value < 0 && value > -N_SMALL) {
      return SMALL_NEG[-value];
    } else {
      return new PyInt(value, null);
    }
  }

  public static PyInt get(long value) {
    int i = (int) value;
    if (i == value) {
      if (value >= 0 && value < N_SMALL) {
        return SMALL_POS[(int) value];
      } else if (value < 0 && value > -N_SMALL) {
        return SMALL_NEG[-(int) value];
      } else {
        return new PyInt((int) value, null);
      }
    }
    return getLongUnchecked(value);
  }

  public static PyInt getLongUnchecked(long value) {
    if (Debug.ENABLED) {
      dcheck((int) value != value);
    }
    return new PyInt((value < 0 ? MASK_SIGN : 0) | MASK_LONG, value);
  }

  public static PyInt get(BigInteger value) {
    if (value.signum() == 0) {
      return ZERO;
    } else {
      return getBigUnchecked(value);
    }
  }

  public static PyInt getBigUnchecked(BigInteger value) {
    int signum = value.signum();
    if (Debug.ENABLED) {
      dcheck(signum != 0);
    }
    return new PyInt((signum & MASK_SIGN) | MASK_BIG, value);
  }

  public static PyInt getMinimal(BigInteger value) {
    if (IntOps.isLong(value)) {
      return get(value.longValue());
    } else {
      return getBigUnchecked(value);
    }
  }

  public final boolean isZero() {
    return data == 0;
  }

  public final boolean isNegative() {
    return (data & MASK_SIGN) != 0;
  }

  @Override
  public boolean isTrue(PyContext ctx) {
    return data != 0;
  }

  final boolean isLongMask() {
    return (data & MASK_LONG) != 0;
  }

  public final boolean isLongRep() {
    return obj != null && (data & MASK_LONG) != 0;
  }

  public final boolean isBigRep() {
    return obj != null && (data & MASK_BIG) != 0;
  }

  public final boolean isSmallRep() {
    return obj == null;
  }

  @Override
  public int intHashCode(PyContext ctx) {
    if (obj == null) {
      return NumLib.hashCode(data);
    } else if ((data & MASK_LONG) != 0) {
      return NumLib.hashCode((long) obj);
    } else {
      return NumLib.hashCode((BigInteger) obj);
    }
  }

  public final int intUnchecked() {
    if (Debug.ENABLED) {
      dcheck(obj == null);
    }
    return data;
  }

  public final long longUnchecked() {
    if (Debug.ENABLED) {
      dcheck(isLongRep());
    }
    return (long) obj;
  }

  public final BigInteger bigIntUnchecked() {
    if (Debug.ENABLED) {
      dcheck(isBigRep());
    }
    return (BigInteger) obj;
  }

  public final boolean isSmall() {
    if (obj == null) {
      return true;
    } else if ((data & MASK_LONG) != 0) {
      long v = (long) obj;
      return (int) v == v;
    } else {
      return IntOps.isInt((BigInteger) obj);
    }
  }

  public final boolean isLong() {
    if (obj == null || (data & MASK_BIG) == 0) {
      return true;
    } else {
      return IntOps.isLong((BigInteger) obj);
    }
  }

  public final int signum() {
    return Integer.signum(data);
  }

  public final int intValue() {
    if (Debug.ENABLED) {
      dcheck(isSmall());
    }
    if (obj == null) {
      return data;
    } else if ((data & MASK_LONG) != 0) {
      return (int) (long) obj;
    } else {
      return ((BigInteger) obj).intValue();
    }
  }

  public final long longValue() {
    if (Debug.ENABLED) {
      dcheck(isLong());
    }
    if (obj == null) {
      return data;
    } else if ((data & MASK_LONG) != 0) {
      return (long) obj;
    } else {
      return ((BigInteger) obj).longValue();
    }
  }

  public final BigInteger bigIntValue() {
    if (obj == null) {
      return BigInteger.valueOf(data);
    } else if ((data & MASK_LONG) != 0) {
      return BigInteger.valueOf((long) obj);
    } else {
      return (BigInteger) obj;
    }
  }

  @Override
  public IPyObj __eq__(@Param("other") IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return PyBool.of(IntLib.isEqual(this, (PyInt) obj));
    } else if (obj instanceof PyFloat) {
      return PyBool.of(IntLib.isEqual(this, ((PyFloat) obj).value));
    } else if (obj instanceof PyComplex) {
      PyComplex c = (PyComplex) obj;
      return PyBool.of(c.getImaginary() == 0 && IntLib.isEqual(this, c.getReal()));
    } else {
      return NotImplemented;
    }
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  @SuppressWarnings({"ReferenceEquality", "ConstantConditions"})
  public void validate(Validator v) {
    v.validate(type());
    check(
        !isSmallRep()
            || intValue() != 0
            || this == ZERO
            || this == False
            || !(type() instanceof IPyBuiltinClass),
        "more than two zero values");
    check(
        !isSmallRep()
            || intValue() != 1
            || this == ONE
            || this == True
            || !(type() instanceof IPyBuiltinClass),
        "more than two unit values");
    if (obj == null) {
      check(isSmallRep(), "bad value");
      check(!isLongRep(), "bad value");
      check(!isBigRep(), "bad value");
      check(isSmall(), "bad value");
      check(isLong(), "bad value");
    } else if ((data & MASK_LONG) != 0) {
      check(!isSmallRep(), "bad value");
      check(isLongRep(), "bad value");
      check(!isBigRep(), "bad value");
      check(obj instanceof Long, "bad data");
      check(!isSmall(), "bad value");
      check(isLong(), "bad value");
    } else {
      check(!isSmallRep(), "bad value");
      check(!isLongRep(), "bad value");
      check(isBigRep(), "bad value");
      check(obj instanceof BigInteger, "bad data");
      check(((BigInteger) obj).signum() != 0, "zero big int");
    }
  }

  @Override
  public String toString() {
    return obj != null ? obj.toString() : Integer.toString(data);
  }

  @Override
  public boolean equals(Object obj) {
    throw new InternalErrorException("equals");
  }

  @Override
  public int hashCode() {
    throw new InternalErrorException("hashCode");
  }

  @Override
  public PyStr repr(PyContext ctx) {
    return PyStr.ucs2(toString());
  }

  @Override
  public IPyObj __lt__(@Param("other") IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return PyBool.of(IntLib.lt(this, (PyInt) obj));
    } else if (obj instanceof PyFloat) {
      return PyBool.of(IntLib.lt(this, ((PyFloat) obj).value));
    } else {
      return NotImplemented;
    }
  }

  @Override
  public IPyObj __le__(@Param("other") IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return PyBool.of(IntLib.le(this, (PyInt) obj));
    } else if (obj instanceof PyFloat) {
      return PyBool.of(IntLib.le(this, ((PyFloat) obj).value));
    } else {
      return NotImplemented;
    }
  }

  @Override
  public IPyObj __gt__(@Param("other") IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return PyBool.of(!IntLib.le(this, (PyInt) obj));
    } else if (obj instanceof PyFloat) {
      return PyBool.of(IntLib.gt(this, ((PyFloat) obj).value));
    } else {
      return NotImplemented;
    }
  }

  @Override
  public IPyObj __ge__(@Param("other") IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return PyBool.of(!IntLib.lt(this, (PyInt) obj));
    } else if (obj instanceof PyFloat) {
      return PyBool.of(IntLib.ge(this, ((PyFloat) obj).value));
    } else {
      return NotImplemented;
    }
  }

  @Override
  public IPyObj __complex__(PyContext ctx) {
    return new PyComplex(IntLib.toDouble(this, ctx), 0);
  }

  @Override
  public IPyObj __float__(PyContext ctx) {
    return new PyFloat(IntLib.toDouble(this, ctx));
  }

  @Override
  public IPyObj zero() {
    return ZERO;
  }

  @Override
  public IPyObj one() {
    return ONE;
  }

  @InstanceMethod
  public IPyObj bit_length(PyContext ctx) {
    throw new NotImplementedException("bit_length");
  }

  @Override
  public IPyObj __pow__(IPyObj rhs, IPyObj mod, PyContext ctx) {
    if (mod != None) {
      return IntOps.pow(this, rhs, mod, ctx);
    } else {
      return IntOps.pow(this, rhs, ctx);
    }
  }

  @Override
  public PyStr format(ObjFormatLib.Spec spec, PyContext ctx) {
    return IntLib.format(this, (ObjFormatLib.NumSpec) spec, ctx);
  }

  @ClassMethod
  public static IPyObj from_bytes(
      IPyClass cls,
      @Param("bytes") IPyObj bytes,
      @Param("byteorder") IPyObj byteOrder,
      @Param(value = "signed", dflt = DEFAULT_FALSE, kind = ParamKind.KW_ONLY) IPyObj signed,
      PyContext ctx) {
    throw new NotImplementedException("from_bytes");
  }

  // BEGIN GENERATED CODE

  @Override
  public IPyObj __neg__(PyContext ctx) {
    return IntOps.neg(this);
  }

  @Override
  public IPyObj __abs__(PyContext ctx) {
    return IntOps.abs(this);
  }

  @Override
  public IPyObj __invert__(PyContext ctx) {
    return IntOps.invert(this);
  }

  @Override
  public IPyObj __add__(IPyObj obj, PyContext ctx) {
    return IntOps.add(this, obj, ctx);
  }

  @Override
  public IPyObj __sub__(IPyObj obj, PyContext ctx) {
    return IntOps.sub(this, obj, ctx);
  }

  @Override
  public IPyObj __mul__(IPyObj obj, PyContext ctx) {
    return IntOps.mul(this, obj, ctx);
  }

  @Override
  public IPyObj __truediv__(IPyObj obj, PyContext ctx) {
    return IntOps.truediv(this, obj, ctx);
  }

  @Override
  public IPyObj __floordiv__(IPyObj obj, PyContext ctx) {
    return IntOps.floordiv(this, obj, ctx);
  }

  @Override
  public IPyObj __mod__(IPyObj obj, PyContext ctx) {
    return IntOps.mod(this, obj, ctx);
  }

  @Override
  public IPyObj __divmod__(IPyObj obj, PyContext ctx) {
    return IntOps.divmod(this, obj, ctx);
  }

  @Override
  public IPyObj __lshift__(IPyObj obj, PyContext ctx) {
    return IntOps.lshift(this, obj, ctx);
  }

  @Override
  public IPyObj __rshift__(IPyObj obj, PyContext ctx) {
    return IntOps.rshift(this, obj, ctx);
  }

  @Override
  public IPyObj __and__(IPyObj obj, PyContext ctx) {
    return IntOps.and(this, obj, ctx);
  }

  @Override
  public IPyObj __xor__(IPyObj obj, PyContext ctx) {
    return IntOps.xor(this, obj, ctx);
  }

  @Override
  public IPyObj __or__(IPyObj obj, PyContext ctx) {
    return IntOps.or(this, obj, ctx);
  }

  // END GENERATED CODE
}
