package org.japy.kernel.types.num;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;

class FloatOps extends NumOps {
  static IPyObj neg(PyFloat self) {
    return new PyFloat(-self.value);
  }

  static IPyObj abs(PyFloat self) {
    return (self.value <= 0.0D) ? new PyFloat(0.0D - self.value) : self;
  }

  // BEGIN GENERATED CODE

  static IPyObj add(PyFloat self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return add(self.value, IntLib.toDouble((PyInt) obj, ctx));
    } else if (obj instanceof PyFloat) {
      return add(self.value, ((PyFloat) obj).value);
    } else if (obj instanceof PyComplex) {
      return add(self.value, (PyComplex) obj);
    }
    return NotImplemented;
  }

  static IPyObj sub(PyFloat self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return sub(self.value, IntLib.toDouble((PyInt) obj, ctx));
    } else if (obj instanceof PyFloat) {
      return sub(self.value, ((PyFloat) obj).value);
    } else if (obj instanceof PyComplex) {
      return sub(self.value, (PyComplex) obj);
    }
    return NotImplemented;
  }

  static IPyObj mul(PyFloat self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return mul(self.value, IntLib.toDouble((PyInt) obj, ctx));
    } else if (obj instanceof PyFloat) {
      return mul(self.value, ((PyFloat) obj).value);
    } else if (obj instanceof PyComplex) {
      return mul(self.value, (PyComplex) obj);
    }
    return NotImplemented;
  }

  static IPyObj truediv(PyFloat self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return truediv(self.value, IntLib.toDouble((PyInt) obj, ctx), ctx);
    } else if (obj instanceof PyFloat) {
      return truediv(self.value, ((PyFloat) obj).value, ctx);
    } else if (obj instanceof PyComplex) {
      return truediv(self.value, (PyComplex) obj, ctx);
    }
    return NotImplemented;
  }

  static IPyObj floordiv(PyFloat self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return floordiv(self.value, IntLib.toDouble((PyInt) obj, ctx), ctx);
    } else if (obj instanceof PyFloat) {
      return floordiv(self.value, ((PyFloat) obj).value, ctx);
    }
    return NotImplemented;
  }

  static IPyObj mod(PyFloat self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return mod(self.value, IntLib.toDouble((PyInt) obj, ctx), ctx);
    } else if (obj instanceof PyFloat) {
      return mod(self.value, ((PyFloat) obj).value, ctx);
    }
    return NotImplemented;
  }

  static IPyObj divmod(PyFloat self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return divmod(self.value, IntLib.toDouble((PyInt) obj, ctx), ctx);
    } else if (obj instanceof PyFloat) {
      return divmod(self.value, ((PyFloat) obj).value, ctx);
    }
    return NotImplemented;
  }

  static IPyObj pow(PyFloat self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return pow(self.value, IntLib.toDouble((PyInt) obj, ctx), ctx);
    } else if (obj instanceof PyFloat) {
      return pow(self.value, ((PyFloat) obj).value, ctx);
    } else if (obj instanceof PyComplex) {
      return pow(self.value, (PyComplex) obj, ctx);
    }
    return NotImplemented;
  }

  // END GENERATED CODE
}
