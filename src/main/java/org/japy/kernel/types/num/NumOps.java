package org.japy.kernel.types.num;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.exc.px.PxZeroDivisionError;

class NumOps {
  static IPyObj add(double lhs, double rhs) {
    return new PyFloat(lhs + rhs);
  }

  static IPyObj add(double lhs, PyComplex rhs) {
    return lhs == 0 ? rhs : rhs.add(lhs);
  }

  static IPyObj sub(double lhs, double rhs) {
    return new PyFloat(lhs - rhs);
  }

  static IPyObj sub(double lhs, PyComplex rhs) {
    return new PyComplex(lhs - rhs.real, -rhs.imag);
  }

  static IPyObj mul(double lhs, double rhs) {
    return new PyFloat(lhs * rhs);
  }

  static IPyObj mul(double lhs, PyComplex rhs) {
    return rhs.multiply(lhs);
  }

  static IPyObj truediv(double lhs, double rhs, PyContext ctx) {
    if (rhs == 0) {
      throw new PxZeroDivisionError(ctx);
    }
    return new PyFloat(lhs / rhs);
  }

  static IPyObj truediv(double lhs, PyComplex rhs, PyContext ctx) {
    if (rhs.isZero()) {
      throw new PxZeroDivisionError(ctx);
    }
    return rhs.reciprocal().multiply(lhs);
  }

  static IPyObj floordiv(double lhs, double rhs, PyContext ctx) {
    if (rhs == 0) {
      throw new PxZeroDivisionError(ctx);
    }
    // TODO zzz
    return new PyFloat(Math.floor(lhs / rhs));
  }

  static IPyObj floordiv(double lhs, PyComplex rhs, PyContext ctx) {
    throw new NotImplementedException("floordiv");
  }

  static IPyObj mod(double lhs, double rhs, PyContext ctx) {
    if (rhs == 0) {
      throw new PxZeroDivisionError(ctx);
    }
    // TODO zzz
    return new PyFloat(lhs - Math.floor(lhs / rhs) * rhs);
  }

  static IPyObj divmod(double lhs, double rhs, PyContext ctx) {
    throw new NotImplementedException("divmod");
  }

  static IPyObj pow(double lhs, double rhs, PyContext ctx) {
    return new PyFloat(Math.pow(lhs, rhs));
  }

  static IPyObj pow(double lhs, PyComplex rhs, @SuppressWarnings("unused") PyContext ctx) {
    return new PyComplex(lhs, 0).pow(rhs);
  }
}
