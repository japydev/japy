package org.japy.kernel.types.num;

import static org.japy.infra.validation.Validator.check;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.ObjFormatLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.PyConstants;

@SuppressWarnings("ReferenceEquality")
public class PyBool extends PyInt implements IPyObjWithRepr {
  public static final PyBool True = new PyBool(true);
  public static final PyBool False = new PyBool(false);

  private static class TypeHolder {
    static final IPyClass TYPE =
        new PyBuiltinClass(
            "bool",
            PyInt.TYPE,
            PyBool.class,
            PyBuiltinClass.CALLABLE,
            "bool(x) -> bool\n"
                + "\n"
                + "Returns True when the argument x is true, False otherwise.\n"
                + "The builtins True and False are the only two instances of the class bool.\n"
                + "The class bool is a subclass of the class int, and cannot be subclassed.");
  }

  // override PyInt.TYPE
  private static final int TYPE = 8;

  public static IPyClass TYPE() {
    return TypeHolder.TYPE;
  }

  private PyBool(boolean value) {
    super(value ? 1 : 0);
  }

  @SuppressWarnings("MethodOverridesStaticMethodOfSuperclass")
  @Constructor
  public static IPyObj construct(Args args, PyContext ctx) {
    ArgParser.verifyNoKwArgs(args, "bool", ctx);
    if (args.posCount() > 1) {
      throw PxException.typeError("too many arguments for bool()", ctx);
    }
    if (args.posCount() == 0) {
      return False;
    }
    return bool(args.getPos(0), ctx);
  }

  public static PyBool bool(IPyObj obj, PyContext ctx) {
    if (obj == True || obj == False) {
      return (PyBool) obj;
    }
    return PyBool.of(ObjLib.isTrue(obj, ctx));
  }

  public static PyBool of(boolean value) {
    return value ? True : False;
  }

  public boolean value() {
    return data != 0;
  }

  @Override
  public IPyClass type() {
    return TYPE();
  }

  @Override
  public IPyObj __bool__(PyContext ctx) {
    return this;
  }

  @Override
  public String toString() {
    return this == True ? "True" : "False";
  }

  @Override
  public PyStr repr(PyContext ctx) {
    return this == True ? PyConstants.TRUE : PyConstants.FALSE;
  }

  @Override
  public PyStr format(ObjFormatLib.Spec spec, PyContext ctx) {
    if (!spec.isEmpty()) {
      return super.format(spec, ctx);
    }
    return repr(ctx);
  }

  @Override
  public void validate(Validator v) {
    super.validate(v);
    check(this == True || this == False, "too many boolean objects");
  }

  @Override
  public IPyObj __and__(IPyObj obj, PyContext ctx) {
    if (obj instanceof PyBool) {
      return of(this.value() && ((PyBool) obj).value());
    }
    return super.__and__(obj, ctx);
  }

  @Override
  public IPyObj __or__(IPyObj obj, PyContext ctx) {
    if (obj instanceof PyBool) {
      return of(this.value() || ((PyBool) obj).value());
    }
    return super.__or__(obj, ctx);
  }

  @Override
  public IPyObj __xor__(IPyObj obj, PyContext ctx) {
    if (obj instanceof PyBool) {
      return of(this.value() ^ ((PyBool) obj).value());
    }
    return super.__xor__(obj, ctx);
  }

  @Override
  public IPyObj __invert__(PyContext ctx) {
    return value() ? NEGATIVE_TWO : NEGATIVE_ONE;
  }

  @Override
  public IPyObj __pos__(PyContext ctx) {
    return value() ? ONE : ZERO;
  }

  @Override
  public IPyObj __abs__(PyContext ctx) {
    return __pos__(ctx);
  }

  @Override
  public IPyObj __neg__(PyContext ctx) {
    return value() ? NEGATIVE_ONE : ZERO;
  }

  @Override
  public IPyObj real(PyContext ctx) {
    return __pos__(ctx);
  }

  @Override
  public IPyObj conjugate(PyContext ctx) {
    return __pos__(ctx);
  }

  @Override
  public IPyObj numerator(PyContext ctx) {
    return __pos__(ctx);
  }
}
