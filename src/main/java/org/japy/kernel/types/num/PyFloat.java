package org.japy.kernel.types.num;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;
import static org.japy.kernel.util.PyUtil.wrap;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.ClassMethod;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.annotations.StaticMethod;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.ObjFormatLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyFloat
    implements IPyIntHashableObj, IPyNoValidationObj, IPyRealNumber, IPyObjWithRepr {
  public static final PyFloat ZERO = new PyFloat(0);
  public static final PyFloat ONE = new PyFloat(1);
  public static final PyFloat NEGATIVE_ONE = new PyFloat(-1);
  public static final PyFloat NAN = new PyFloat(Double.NaN);
  public static final PyFloat POSITIVE_INFINITY = new PyFloat(Double.POSITIVE_INFINITY);
  public static final PyFloat NEGATIVE_INFINITY = new PyFloat(Double.NEGATIVE_INFINITY);

  public static final IPyClass TYPE =
      new PyBuiltinClass("float", PyFloat.class, PyBuiltinClass.REGULAR);

  public final double value;

  public PyFloat(double value) {
    this.value = value;
  }

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(1).addPosOnly("x", Sentinel).build();

  @Constructor
  @SubClassConstructor
  public PyFloat(Args args, PyContext ctx) {
    IPyObj x = ARG_PARSER.parse1(args, "float", ctx);
    if (x == Sentinel) {
      value = 0;
    } else {
      value = FloatLib.toFloat(args.getPos(0), ctx).value;
    }
  }

  public static PyFloat of(double value) {
    return new PyFloat(value);
  }

  @Override
  public int intHashCode(PyContext ctx) {
    return NumLib.hashCode(value);
  }

  @Override
  public IPyObj __eq__(@Param("other") IPyObj obj, PyContext ctx) {
    if (obj instanceof PyFloat) {
      return PyBool.of(value == ((PyFloat) obj).value);
    } else if (obj instanceof PyInt) {
      return PyBool.of(IntLib.isEqual((PyInt) obj, value));
    } else if (obj instanceof PyComplex) {
      return PyBool.of(((PyComplex) obj).imag == 0 && ((PyComplex) obj).real == value);
    } else {
      return NotImplemented;
    }
  }

  @Override
  public String toString() {
    return Double.toString(value);
  }

  @Override
  public boolean equals(Object obj) {
    throw new InternalErrorException("equals");
  }

  @Override
  public int hashCode() {
    throw new InternalErrorException("hashCode");
  }

  static String repr(double value) {
    if (Double.isNaN(value)) {
      return "nan";
    }
    if (value == Double.POSITIVE_INFINITY) {
      return "inf";
    }
    if (value == Double.NEGATIVE_INFINITY) {
      return "-inf";
    }
    return Double.toString(value);
  }

  @Override
  public PyStr repr(PyContext ctx) {
    return PyStr.ucs2(repr(value));
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public boolean isTrue(PyContext ctx) {
    return value != 0;
  }

  @Override
  public IPyObj __lt__(@Param("other") IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return PyBool.of(IntLib.gt((PyInt) obj, value));
    } else if (obj instanceof PyFloat) {
      return PyBool.of(value < ((PyFloat) obj).value);
    } else {
      return NotImplemented;
    }
  }

  @Override
  public IPyObj __le__(@Param("other") IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return PyBool.of(IntLib.ge((PyInt) obj, value));
    } else if (obj instanceof PyFloat) {
      return PyBool.of(value <= ((PyFloat) obj).value);
    } else {
      return NotImplemented;
    }
  }

  @Override
  public IPyObj __gt__(@Param("other") IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return PyBool.of(IntLib.lt((PyInt) obj, value));
    } else if (obj instanceof PyFloat) {
      return PyBool.of(value > ((PyFloat) obj).value);
    } else {
      return NotImplemented;
    }
  }

  @Override
  public IPyObj __ge__(@Param("other") IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return PyBool.of(IntLib.le((PyInt) obj, value));
    } else if (obj instanceof PyFloat) {
      return PyBool.of(value >= ((PyFloat) obj).value);
    } else {
      return NotImplemented;
    }
  }

  @Override
  public IPyObj __complex__(PyContext ctx) {
    return new PyComplex(value, 0);
  }

  @Override
  public IPyObj __int__(PyContext ctx) {
    return IntLib.convertToInt(value, ctx);
  }

  @Override
  public IPyObj __float__(PyContext ctx) {
    return type() == TYPE ? this : new PyFloat(value);
  }

  @Override
  public IPyObj round(PyContext ctx) {
    return FloatLib.round(value);
  }

  @Override
  public IPyObj round(int ndigits, PyContext ctx) {
    return FloatLib.round(value, ndigits);
  }

  @Override
  public IPyObj __trunc__(PyContext ctx) {
    return FloatLib.trunc(value);
  }

  @Override
  public IPyObj __floor__(PyContext ctx) {
    return FloatLib.floor(value, ctx);
  }

  @Override
  public IPyObj __ceil__(PyContext ctx) {
    return FloatLib.ceil(value, ctx);
  }

  @Override
  public IPyObj zero() {
    return ZERO;
  }

  @Override
  public IPyObj one() {
    return ONE;
  }

  @SpecialInstanceMethod
  public IPyObj __pow__(@Param("rhs") IPyObj rhs, PyContext ctx) {
    return FloatOps.pow(this, rhs, ctx);
  }

  @ClassMethod
  public static IPyObj fromhex(IPyClass cls, @Param("string") IPyObj s, PyContext ctx) {
    return FloatLib.fromHex(
        cls, ArgLib.checkArgStr("float.fromhex", "string", s, ctx).toString(), ctx);
  }

  @InstanceMethod
  public IPyObj hex(PyContext ctx) {
    return FloatLib.hex(value);
  }

  @InstanceMethod
  public IPyObj as_integer_ratio(PyContext ctx) {
    return FloatLib.asIntegerRatio(value, ctx);
  }

  @InstanceMethod
  public IPyObj is_integer(PyContext ctx) {
    return wrap(FloatLib.isInteger(value));
  }

  @Override
  public PyStr format(ObjFormatLib.Spec spec, PyContext ctx) {
    return FloatLib.format(this, (ObjFormatLib.NumSpec) spec, ctx);
  }

  @StaticMethod
  public static IPyObj __getformat__(@Param("typestr") IPyObj typestr, PyContext ctx) {
    return PyStr.ucs2("IEEE, little-endian");
  }

  // BEGIN GENERATED CODE

  @Override
  public IPyObj __neg__(PyContext ctx) {
    return FloatOps.neg(this);
  }

  @Override
  public IPyObj __abs__(PyContext ctx) {
    return FloatOps.abs(this);
  }

  @Override
  public IPyObj __add__(IPyObj obj, PyContext ctx) {
    return FloatOps.add(this, obj, ctx);
  }

  @Override
  public IPyObj __sub__(IPyObj obj, PyContext ctx) {
    return FloatOps.sub(this, obj, ctx);
  }

  @Override
  public IPyObj __mul__(IPyObj obj, PyContext ctx) {
    return FloatOps.mul(this, obj, ctx);
  }

  @Override
  public IPyObj __truediv__(IPyObj obj, PyContext ctx) {
    return FloatOps.truediv(this, obj, ctx);
  }

  @Override
  public IPyObj __floordiv__(IPyObj obj, PyContext ctx) {
    return FloatOps.floordiv(this, obj, ctx);
  }

  @Override
  public IPyObj __mod__(IPyObj obj, PyContext ctx) {
    return FloatOps.mod(this, obj, ctx);
  }

  @Override
  public IPyObj __divmod__(IPyObj obj, PyContext ctx) {
    return FloatOps.divmod(this, obj, ctx);
  }

  // END GENERATED CODE
}
