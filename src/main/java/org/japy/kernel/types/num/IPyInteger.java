package org.japy.kernel.types.num;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;

public interface IPyInteger extends IPyRealNumber {
  @InstanceAttribute
  default IPyObj numerator(PyContext ctx) {
    return this;
  }

  @InstanceAttribute
  default IPyObj denominator(PyContext ctx) {
    return one();
  }

  @SpecialInstanceMethod
  IPyObj __lshift__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __rshift__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __and__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __xor__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __or__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __invert__(PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __pow__(
      @Param("rhs") IPyObj rhs,
      @Param(value = "mod", dflt = DEFAULT_NONE) IPyObj mod,
      PyContext ctx);

  @Override
  default IPyObj __int__(PyContext ctx) {
    return this;
  }

  @InstanceMethod
  default IPyObj __index__(PyContext ctx) {
    return this;
  }

  @Override
  default IPyObj round(PyContext ctx) {
    return this;
  }

  @Override
  default IPyObj round(int ndigits, PyContext ctx) {
    return this;
  }

  @Override
  default IPyObj __trunc__(PyContext ctx) {
    return this;
  }

  @Override
  default IPyObj __floor__(PyContext ctx) {
    return this;
  }

  @Override
  default IPyObj __ceil__(PyContext ctx) {
    return this;
  }
}
