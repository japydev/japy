package org.japy.kernel.types.num;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.util.PyUtil.wrap;

import java.math.BigInteger;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.exc.px.PxOverflowError;
import org.japy.kernel.types.exc.px.PxZeroDivisionError;
import org.japy.kernel.types.func.ArgLib;

@SuppressWarnings({"DuplicatedCode"})
class IntOps extends NumOps {
  private static final BigInteger BIG_MAX_INT = BigInteger.valueOf(Integer.MAX_VALUE);
  private static final BigInteger BIG_MIN_INT = BigInteger.valueOf(Integer.MIN_VALUE);
  private static final BigInteger BIG_MAX_LONG = BigInteger.valueOf(Long.MAX_VALUE);
  private static final BigInteger BIG_MIN_LONG = BigInteger.valueOf(Long.MIN_VALUE);
  private static final BigInteger BIG_ONE = BigInteger.valueOf(1);

  static boolean isInt(BigInteger v) {
    return v.compareTo(BIG_MAX_INT) <= 0 && v.compareTo(BIG_MIN_INT) >= 0;
  }

  private static boolean isInt(long v) {
    return v >= Integer.MIN_VALUE && v <= Integer.MAX_VALUE;
  }

  static boolean isLong(BigInteger value) {
    return value.compareTo(BIG_MIN_LONG) >= 0 && value.compareTo(BIG_MAX_LONG) <= 0;
  }

  static PyInt neg(PyInt v) {
    if (v.obj == null) {
      if (v.data == 0) {
        return v;
      } else if (v.data == Integer.MIN_VALUE) {
        // https://youtrack.jetbrains.com/issue/IDEA-286284
        //noinspection UnnecessaryExplicitNumericCast:
        return PyInt.getLongUnchecked(-(long) v.data);
      } else {
        return PyInt.get(-v.data);
      }
    }

    if (v.isLongRep()) {
      long lv = (long) v.obj;
      if (lv == Long.MIN_VALUE) {
        return PyInt.getBigUnchecked(BigInteger.valueOf(Long.MIN_VALUE).negate());
      } else //noinspection UnnecessaryExplicitNumericCast
      if (lv == -(long) Integer.MIN_VALUE) {
        return PyInt.get(Integer.MIN_VALUE);
      } else {
        return PyInt.getLongUnchecked(-lv);
      }
    }

    return PyInt.getBigUnchecked(((BigInteger) v.obj).negate());
  }

  static PyInt abs(PyInt self) {
    if (self.signum() == -1) {
      return neg(self);
    } else {
      return self;
    }
  }

  static IPyObj invert(PyInt self) {
    if (self.obj == null) {
      return PyInt.get(~self.data);
    } else if (self.isLongRep()) {
      return PyInt.get(~(long) self.obj);
    } else {
      return PyInt.get(self.bigIntUnchecked().not());
    }
  }

  private static IPyObj add(int lhs, int rhs) {
    try {
      return wrap(Math.addExact(lhs, rhs));
    } catch (ArithmeticException ignored) {
      return wrap((long) lhs + rhs);
    }
  }

  private static IPyObj add(long lhs, long rhs) {
    try {
      return wrap(Math.addExact(lhs, rhs));
    } catch (ArithmeticException ignored) {
      return PyInt.getBigUnchecked(BigInteger.valueOf(lhs).add(BigInteger.valueOf(rhs)));
    }
  }

  private static IPyObj add(long lhs, BigInteger rhs) {
    return add(rhs, lhs);
  }

  private static IPyObj add(BigInteger lhs, long rhs) {
    return wrap(lhs.add(BigInteger.valueOf(rhs)));
  }

  private static IPyObj add(BigInteger lhs, BigInteger rhs) {
    return wrap(lhs.add(rhs));
  }

  private static IPyObj sub(int lhs, int rhs) {
    try {
      return wrap(Math.subtractExact(lhs, rhs));
    } catch (ArithmeticException ex) {
      return wrap((long) lhs - rhs);
    }
  }

  private static IPyObj sub(long lhs, long rhs) {
    try {
      return wrap(Math.subtractExact(lhs, rhs));
    } catch (ArithmeticException ex) {
      return wrap(BigInteger.valueOf(lhs).subtract(BigInteger.valueOf(rhs)));
    }
  }

  private static IPyObj sub(long lhs, BigInteger rhs) {
    return wrap(BigInteger.valueOf(lhs).subtract(rhs));
  }

  private static IPyObj sub(BigInteger lhs, long rhs) {
    return wrap(lhs.subtract(BigInteger.valueOf(rhs)));
  }

  private static IPyObj sub(BigInteger lhs, BigInteger rhs) {
    return wrap(lhs.subtract(rhs));
  }

  private static IPyObj mul(long lhs, long rhs) {
    try {
      return wrap(Math.multiplyExact(lhs, rhs));
    } catch (ArithmeticException ignored) {
      return wrap(BigInteger.valueOf(lhs).multiply(BigInteger.valueOf(rhs)));
    }
  }

  private static IPyObj mul(long lhs, BigInteger rhs) {
    return wrap(rhs.multiply(BigInteger.valueOf(lhs)));
  }

  private static IPyObj mul(BigInteger lhs, long rhs) {
    return wrap(lhs.multiply(BigInteger.valueOf(rhs)));
  }

  private static IPyObj mul(BigInteger lhs, BigInteger rhs) {
    return wrap(lhs.multiply(rhs));
  }

  private static IPyObj truediv(PyInt lhs, PyInt rhs, PyContext ctx) {
    if (rhs.isZero()) {
      throw new PxZeroDivisionError(ctx);
    }
    // TODO zzz
    if (lhs.isSmall() && rhs.isSmall()) {
      int x = lhs.intValue();
      int y = rhs.intValue();
      if (y > 0) {
        return new PyFloat((double) x / y);
      }
    }
    throw new NotImplementedException("truediv");
  }

  private static IPyObj floordiv(int lhs, int rhs, PyContext ctx) {
    if (rhs == 0) {
      throw new PxZeroDivisionError(ctx);
    }
    if (lhs == Integer.MIN_VALUE && rhs == -1) {
      return wrap(Math.floorDiv((long) lhs, rhs));
    }
    return wrap(Math.floorDiv(lhs, rhs));
  }

  private static IPyObj floordiv(long lhs, long rhs, PyContext ctx) {
    if (rhs == 0) {
      throw new PxZeroDivisionError(ctx);
    }
    if (lhs == Long.MIN_VALUE && rhs == -1) {
      return floordiv(BigInteger.valueOf(lhs), BigInteger.valueOf(rhs), ctx);
    }
    return wrap(Math.floorDiv(lhs, rhs));
  }

  private static IPyObj floordiv(long lhs, BigInteger rhs, PyContext ctx) {
    return floordiv(BigInteger.valueOf(lhs), rhs, ctx);
  }

  private static IPyObj floordiv(BigInteger lhs, int rhs, PyContext ctx) {
    return floordiv(lhs, BigInteger.valueOf(rhs), ctx);
  }

  private static IPyObj floordiv(BigInteger lhs, long rhs, PyContext ctx) {
    return floordiv(lhs, BigInteger.valueOf(rhs), ctx);
  }

  private static IPyObj floordiv(BigInteger lhs, BigInteger rhs, PyContext ctx) {
    int lhsSignum = lhs.signum();
    int rhsSignum = rhs.signum();
    if (rhsSignum == 0) {
      throw new PxZeroDivisionError(ctx);
    }
    if (lhsSignum == 0) {
      return PyInt.ZERO;
    }

    BigInteger result = lhs.divide(rhs);
    // if the signs are different and the modulo is not zero, round down
    if (lhsSignum != rhsSignum && !result.multiply(rhs).equals(lhs)) {
      return wrap(result.subtract(BIG_ONE));
    } else {
      return wrap(result);
    }
  }

  private static IPyObj mod(int lhs, int rhs, PyContext ctx) {
    if (rhs == 0) {
      throw new PxZeroDivisionError(ctx);
    }
    return wrap(Math.floorMod(lhs, rhs));
  }

  private static IPyObj mod(long lhs, int rhs, PyContext ctx) {
    if (rhs == 0) {
      throw new PxZeroDivisionError(ctx);
    }
    return wrap(Math.floorMod(lhs, rhs));
  }

  private static IPyObj mod(long lhs, long rhs, PyContext ctx) {
    if (rhs == 0) {
      throw new PxZeroDivisionError(ctx);
    }
    return wrap(Math.floorMod(lhs, rhs));
  }

  private static IPyObj mod(long lhs, BigInteger rhs, PyContext ctx) {
    return mod(BigInteger.valueOf(lhs), rhs, ctx);
  }

  private static IPyObj mod(BigInteger lhs, long rhs, PyContext ctx) {
    return mod(lhs, BigInteger.valueOf(rhs), ctx);
  }

  private static IPyObj mod(BigInteger lhs, BigInteger rhs, PyContext ctx) {
    int lhsSignum = lhs.signum();
    int rhsSignum = rhs.signum();
    if (rhsSignum == 0) {
      throw new PxZeroDivisionError(ctx);
    }
    if (lhsSignum == 0) {
      return PyInt.ZERO;
    }
    if (rhsSignum == 1) {
      return PyInt.getMinimal(lhs.mod(rhs));
    } else {
      return PyInt.getMinimal(lhs.negate().mod(rhs.negate()).negate());
    }
  }

  private static IPyObj divmod(int lhs, int rhs, PyContext ctx) {
    return PyTuple.of(floordiv(lhs, rhs, ctx), mod(lhs, rhs, ctx));
  }

  private static IPyObj divmod(long lhs, int rhs, PyContext ctx) {
    return PyTuple.of(floordiv(lhs, rhs, ctx), mod(lhs, rhs, ctx));
  }

  private static IPyObj divmod(long lhs, long rhs, PyContext ctx) {
    return PyTuple.of(floordiv(lhs, rhs, ctx), mod(lhs, rhs, ctx));
  }

  private static IPyObj divmod(long lhs, BigInteger rhs, PyContext ctx) {
    BigInteger bLhs = BigInteger.valueOf(lhs);
    return PyTuple.of(floordiv(bLhs, rhs, ctx), mod(bLhs, rhs, ctx));
  }

  private static IPyObj divmod(BigInteger lhs, long rhs, PyContext ctx) {
    BigInteger bRhs = BigInteger.valueOf(rhs);
    return PyTuple.of(floordiv(lhs, bRhs, ctx), mod(lhs, bRhs, ctx));
  }

  private static IPyObj divmod(BigInteger lhs, BigInteger rhs, PyContext ctx) {
    return PyTuple.of(floordiv(lhs, rhs, ctx), mod(lhs, rhs, ctx));
  }

  private static IPyObj lshift(long lhs, long rhs, PyContext ctx) {
    if (!isInt(rhs)) {
      throw new PxOverflowError("shift is too large", ctx);
    }
    return PyInt.getMinimal(BigInteger.valueOf(lhs).shiftLeft((int) rhs));
  }

  private static IPyObj lshift(long lhs, BigInteger rhs, PyContext ctx) {
    if (!isInt(rhs)) {
      throw new PxOverflowError("shift is too large", ctx);
    }
    return PyInt.getMinimal(BigInteger.valueOf(lhs).shiftLeft(rhs.intValue()));
  }

  private static IPyObj lshift(BigInteger lhs, long rhs, PyContext ctx) {
    if (!isInt(rhs)) {
      throw new PxOverflowError("shift is too large", ctx);
    }
    return PyInt.getMinimal(lhs.shiftLeft((int) rhs));
  }

  private static IPyObj lshift(BigInteger lhs, BigInteger rhs, PyContext ctx) {
    if (!isInt(rhs)) {
      throw new PxOverflowError("shift is too large", ctx);
    }
    return PyInt.getMinimal(lhs.shiftLeft(rhs.intValue()));
  }

  private static IPyObj rshift(long lhs, long rhs, PyContext ctx) {
    if (!isInt(rhs)) {
      throw new PxOverflowError("shift is too large", ctx);
    }
    return PyInt.getMinimal(BigInteger.valueOf(lhs).shiftRight((int) rhs));
  }

  private static IPyObj rshift(long lhs, BigInteger rhs, PyContext ctx) {
    if (!isInt(rhs)) {
      throw new PxOverflowError("shift is too large", ctx);
    }
    return PyInt.getMinimal(BigInteger.valueOf(lhs).shiftRight(rhs.intValue()));
  }

  private static IPyObj rshift(BigInteger lhs, long rhs, PyContext ctx) {
    if (!isInt(rhs)) {
      throw new PxOverflowError("shift is too large", ctx);
    }
    return PyInt.getMinimal(lhs.shiftRight((int) rhs));
  }

  private static IPyObj rshift(BigInteger lhs, BigInteger rhs, PyContext ctx) {
    if (!isInt(rhs)) {
      throw new PxOverflowError("shift is too large", ctx);
    }
    return PyInt.getMinimal(lhs.shiftRight(rhs.intValue()));
  }

  private static IPyObj xor(int lhs, int rhs) {
    return wrap(lhs ^ rhs);
  }

  private static IPyObj xor(long lhs, long rhs) {
    return wrap(lhs ^ rhs);
  }

  private static IPyObj xor(long lhs, BigInteger rhs) {
    return PyInt.getMinimal(BigInteger.valueOf(lhs).xor(rhs));
  }

  private static IPyObj xor(BigInteger lhs, long rhs) {
    return PyInt.getMinimal(lhs.xor(BigInteger.valueOf(rhs)));
  }

  private static IPyObj xor(BigInteger lhs, BigInteger rhs) {
    return PyInt.getMinimal(lhs.xor(rhs));
  }

  private static IPyObj and(int lhs, int rhs) {
    return PyInt.get(lhs & rhs);
  }

  private static IPyObj and(long lhs, long rhs) {
    return PyInt.get(lhs & rhs);
  }

  private static IPyObj and(long lhs, BigInteger rhs) {
    return PyInt.get(BigInteger.valueOf(lhs).and(rhs));
  }

  private static IPyObj and(BigInteger lhs, long rhs) {
    return PyInt.get(lhs.and(BigInteger.valueOf(rhs)));
  }

  private static IPyObj and(BigInteger lhs, BigInteger rhs) {
    return PyInt.get(lhs.and(rhs));
  }

  private static IPyObj or(int lhs, int rhs) {
    return PyInt.get(lhs | rhs);
  }

  private static IPyObj or(long lhs, long rhs) {
    return PyInt.get(lhs | rhs);
  }

  private static IPyObj or(long lhs, BigInteger rhs) {
    return PyInt.getBigUnchecked(BigInteger.valueOf(lhs).or(rhs));
  }

  private static IPyObj or(BigInteger lhs, long rhs) {
    return PyInt.getBigUnchecked(lhs.or(BigInteger.valueOf(rhs)));
  }

  private static IPyObj or(BigInteger lhs, BigInteger rhs) {
    return PyInt.getBigUnchecked(lhs.or(rhs));
  }

  private static IPyObj pow(long lhs, int rhs, PyContext ctx) {
    if (rhs == 0) {
      return PyInt.ONE;
    } else if (rhs > 0) {
      return PyInt.getMinimal(BigInteger.valueOf(lhs).pow(rhs));
    } else {
      return new PyFloat(Math.pow(IntLib.toDouble(lhs, ctx), -(double) rhs));
    }
  }

  private static IPyObj pow(long lhs, long rhs, PyContext ctx) {
    if (rhs == 0) {
      return PyInt.ONE;
    } else if (rhs > 0) {
      if (!isInt(rhs)) {
        throw new PxOverflowError("exponent is too large", ctx);
      }
      return PyInt.getMinimal(BigInteger.valueOf(lhs).pow((int) rhs));
    } else {
      return new PyFloat(Math.pow(IntLib.toDouble(lhs, ctx), -IntLib.toDouble(rhs, ctx)));
    }
  }

  private static IPyObj pow(long lhs, BigInteger rhs, PyContext ctx) {
    int rhsSignum = rhs.signum();
    if (rhsSignum == -1) {
      throw new NotImplementedException("negative exponent");
    }
    if (rhsSignum == 0) {
      return PyInt.ONE;
    }
    if (!isInt(rhs)) {
      throw new PxOverflowError("exponent is too large", ctx);
    }
    return PyInt.get(BigInteger.valueOf(lhs).pow(rhs.intValue()));
  }

  private static IPyObj pow(BigInteger lhs, long rhs, PyContext ctx) {
    if (rhs < 0) {
      throw new NotImplementedException("negative exponent");
    }
    if (rhs == 0) {
      return PyInt.ONE;
    }
    if (!isInt(rhs)) {
      throw new PxOverflowError("exponent is too large", ctx);
    }
    return PyInt.get(lhs.pow((int) rhs));
  }

  private static IPyObj pow(BigInteger lhs, BigInteger rhs, PyContext ctx) {
    int rhsSignum = rhs.signum();
    if (rhsSignum == -1) {
      throw new NotImplementedException("negative exponent");
    }
    if (rhsSignum == 0) {
      return PyInt.ONE;
    }
    throw new NotImplementedException("pow");
  }

  private static IPyObj pow(long lhs, long rhs, IPyObj modulo, PyContext ctx) {
    return pow(BigInteger.valueOf(lhs), BigInteger.valueOf(rhs), modulo, ctx);
  }

  private static IPyObj pow(long lhs, BigInteger rhs, IPyObj modulo, PyContext ctx) {
    return pow(BigInteger.valueOf(lhs), rhs, modulo, ctx);
  }

  private static IPyObj pow(BigInteger lhs, long rhs, IPyObj modulo, PyContext ctx) {
    return pow(lhs, BigInteger.valueOf(rhs), modulo, ctx);
  }

  private static PyInt checkModulo(IPyObj modulo, PyContext ctx) {
    return ArgLib.checkArgInt("pow", "mod", modulo, ctx);
  }

  private static IPyObj pow(BigInteger lhs, BigInteger rhs, IPyObj modulo, PyContext ctx) {
    return PyInt.get(lhs.modPow(rhs, checkModulo(modulo, ctx).bigIntValue()));
  }

  // BEGIN GENERATED CODE

  private static IPyObj add(PyInt l, PyInt r) {
    if (l.obj == null) {
      if (r.obj == null) {
        return add(l.data, r.data);
      } else if (r.isLongMask()) {
        return add(l.data, (long) r.obj);
      } else {
        return add(l.data, (BigInteger) r.obj);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return add((long) l.obj, r.data);
      } else if (r.isLongMask()) {
        return add((long) l.obj, (long) r.obj);
      } else {
        return add((long) l.obj, (BigInteger) r.obj);
      }
    } else {
      if (r.obj == null) {
        return add((BigInteger) l.obj, r.data);
      } else if (r.isLongMask()) {
        return add((BigInteger) l.obj, (long) r.obj);
      } else {
        return add((BigInteger) l.obj, (BigInteger) r.obj);
      }
    }
  }

  static IPyObj add(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return add(self, (PyInt) obj);
    } else if (obj instanceof PyFloat) {
      return add(IntLib.toDouble(self, ctx), ((PyFloat) obj).value);
    } else if (obj instanceof PyComplex) {
      return add(IntLib.toDouble(self, ctx), (PyComplex) obj);
    }
    return NotImplemented;
  }

  private static IPyObj sub(PyInt l, PyInt r) {
    if (l.obj == null) {
      if (r.obj == null) {
        return sub(l.data, r.data);
      } else if (r.isLongMask()) {
        return sub(l.data, (long) r.obj);
      } else {
        return sub(l.data, (BigInteger) r.obj);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return sub((long) l.obj, r.data);
      } else if (r.isLongMask()) {
        return sub((long) l.obj, (long) r.obj);
      } else {
        return sub((long) l.obj, (BigInteger) r.obj);
      }
    } else {
      if (r.obj == null) {
        return sub((BigInteger) l.obj, r.data);
      } else if (r.isLongMask()) {
        return sub((BigInteger) l.obj, (long) r.obj);
      } else {
        return sub((BigInteger) l.obj, (BigInteger) r.obj);
      }
    }
  }

  static IPyObj sub(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return sub(self, (PyInt) obj);
    } else if (obj instanceof PyFloat) {
      return sub(IntLib.toDouble(self, ctx), ((PyFloat) obj).value);
    } else if (obj instanceof PyComplex) {
      return sub(IntLib.toDouble(self, ctx), (PyComplex) obj);
    }
    return NotImplemented;
  }

  private static IPyObj mul(PyInt l, PyInt r) {
    if (l.obj == null) {
      if (r.obj == null) {
        return mul(l.data, r.data);
      } else if (r.isLongMask()) {
        return mul(l.data, (long) r.obj);
      } else {
        return mul(l.data, (BigInteger) r.obj);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return mul((long) l.obj, r.data);
      } else if (r.isLongMask()) {
        return mul((long) l.obj, (long) r.obj);
      } else {
        return mul((long) l.obj, (BigInteger) r.obj);
      }
    } else {
      if (r.obj == null) {
        return mul((BigInteger) l.obj, r.data);
      } else if (r.isLongMask()) {
        return mul((BigInteger) l.obj, (long) r.obj);
      } else {
        return mul((BigInteger) l.obj, (BigInteger) r.obj);
      }
    }
  }

  static IPyObj mul(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return mul(self, (PyInt) obj);
    } else if (obj instanceof PyFloat) {
      return mul(IntLib.toDouble(self, ctx), ((PyFloat) obj).value);
    } else if (obj instanceof PyComplex) {
      return mul(IntLib.toDouble(self, ctx), (PyComplex) obj);
    }
    return NotImplemented;
  }

  static IPyObj truediv(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return truediv(self, (PyInt) obj, ctx);
    } else if (obj instanceof PyFloat) {
      return truediv(IntLib.toDouble(self, ctx), ((PyFloat) obj).value, ctx);
    } else if (obj instanceof PyComplex) {
      return truediv(IntLib.toDouble(self, ctx), (PyComplex) obj, ctx);
    }
    return NotImplemented;
  }

  private static IPyObj floordiv(PyInt l, PyInt r, PyContext ctx) {
    if (l.obj == null) {
      if (r.obj == null) {
        return floordiv(l.data, r.data, ctx);
      } else if (r.isLongMask()) {
        return floordiv(l.data, (long) r.obj, ctx);
      } else {
        return floordiv(l.data, (BigInteger) r.obj, ctx);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return floordiv((long) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return floordiv((long) l.obj, (long) r.obj, ctx);
      } else {
        return floordiv((long) l.obj, (BigInteger) r.obj, ctx);
      }
    } else {
      if (r.obj == null) {
        return floordiv((BigInteger) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return floordiv((BigInteger) l.obj, (long) r.obj, ctx);
      } else {
        return floordiv((BigInteger) l.obj, (BigInteger) r.obj, ctx);
      }
    }
  }

  static IPyObj floordiv(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return floordiv(self, (PyInt) obj, ctx);
    } else if (obj instanceof PyFloat) {
      return floordiv(IntLib.toDouble(self, ctx), ((PyFloat) obj).value, ctx);
    }
    return NotImplemented;
  }

  private static IPyObj mod(PyInt l, PyInt r, PyContext ctx) {
    if (l.obj == null) {
      if (r.obj == null) {
        return mod(l.data, r.data, ctx);
      } else if (r.isLongMask()) {
        return mod(l.data, (long) r.obj, ctx);
      } else {
        return mod(l.data, (BigInteger) r.obj, ctx);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return mod((long) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return mod((long) l.obj, (long) r.obj, ctx);
      } else {
        return mod((long) l.obj, (BigInteger) r.obj, ctx);
      }
    } else {
      if (r.obj == null) {
        return mod((BigInteger) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return mod((BigInteger) l.obj, (long) r.obj, ctx);
      } else {
        return mod((BigInteger) l.obj, (BigInteger) r.obj, ctx);
      }
    }
  }

  static IPyObj mod(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return mod(self, (PyInt) obj, ctx);
    } else if (obj instanceof PyFloat) {
      return mod(IntLib.toDouble(self, ctx), ((PyFloat) obj).value, ctx);
    }
    return NotImplemented;
  }

  private static IPyObj divmod(PyInt l, PyInt r, PyContext ctx) {
    if (l.obj == null) {
      if (r.obj == null) {
        return divmod(l.data, r.data, ctx);
      } else if (r.isLongMask()) {
        return divmod(l.data, (long) r.obj, ctx);
      } else {
        return divmod(l.data, (BigInteger) r.obj, ctx);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return divmod((long) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return divmod((long) l.obj, (long) r.obj, ctx);
      } else {
        return divmod((long) l.obj, (BigInteger) r.obj, ctx);
      }
    } else {
      if (r.obj == null) {
        return divmod((BigInteger) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return divmod((BigInteger) l.obj, (long) r.obj, ctx);
      } else {
        return divmod((BigInteger) l.obj, (BigInteger) r.obj, ctx);
      }
    }
  }

  static IPyObj divmod(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return divmod(self, (PyInt) obj, ctx);
    } else if (obj instanceof PyFloat) {
      return divmod(IntLib.toDouble(self, ctx), ((PyFloat) obj).value, ctx);
    }
    return NotImplemented;
  }

  private static IPyObj lshift(PyInt l, PyInt r, PyContext ctx) {
    if (l.obj == null) {
      if (r.obj == null) {
        return lshift(l.data, r.data, ctx);
      } else if (r.isLongMask()) {
        return lshift(l.data, (long) r.obj, ctx);
      } else {
        return lshift(l.data, (BigInteger) r.obj, ctx);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return lshift((long) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return lshift((long) l.obj, (long) r.obj, ctx);
      } else {
        return lshift((long) l.obj, (BigInteger) r.obj, ctx);
      }
    } else {
      if (r.obj == null) {
        return lshift((BigInteger) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return lshift((BigInteger) l.obj, (long) r.obj, ctx);
      } else {
        return lshift((BigInteger) l.obj, (BigInteger) r.obj, ctx);
      }
    }
  }

  static IPyObj lshift(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return lshift(self, (PyInt) obj, ctx);
    }
    return NotImplemented;
  }

  private static IPyObj rshift(PyInt l, PyInt r, PyContext ctx) {
    if (l.obj == null) {
      if (r.obj == null) {
        return rshift(l.data, r.data, ctx);
      } else if (r.isLongMask()) {
        return rshift(l.data, (long) r.obj, ctx);
      } else {
        return rshift(l.data, (BigInteger) r.obj, ctx);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return rshift((long) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return rshift((long) l.obj, (long) r.obj, ctx);
      } else {
        return rshift((long) l.obj, (BigInteger) r.obj, ctx);
      }
    } else {
      if (r.obj == null) {
        return rshift((BigInteger) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return rshift((BigInteger) l.obj, (long) r.obj, ctx);
      } else {
        return rshift((BigInteger) l.obj, (BigInteger) r.obj, ctx);
      }
    }
  }

  static IPyObj rshift(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return rshift(self, (PyInt) obj, ctx);
    }
    return NotImplemented;
  }

  private static IPyObj and(PyInt l, PyInt r) {
    if (l.obj == null) {
      if (r.obj == null) {
        return and(l.data, r.data);
      } else if (r.isLongMask()) {
        return and(l.data, (long) r.obj);
      } else {
        return and(l.data, (BigInteger) r.obj);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return and((long) l.obj, r.data);
      } else if (r.isLongMask()) {
        return and((long) l.obj, (long) r.obj);
      } else {
        return and((long) l.obj, (BigInteger) r.obj);
      }
    } else {
      if (r.obj == null) {
        return and((BigInteger) l.obj, r.data);
      } else if (r.isLongMask()) {
        return and((BigInteger) l.obj, (long) r.obj);
      } else {
        return and((BigInteger) l.obj, (BigInteger) r.obj);
      }
    }
  }

  static IPyObj and(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return and(self, (PyInt) obj);
    }
    return NotImplemented;
  }

  private static IPyObj xor(PyInt l, PyInt r) {
    if (l.obj == null) {
      if (r.obj == null) {
        return xor(l.data, r.data);
      } else if (r.isLongMask()) {
        return xor(l.data, (long) r.obj);
      } else {
        return xor(l.data, (BigInteger) r.obj);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return xor((long) l.obj, r.data);
      } else if (r.isLongMask()) {
        return xor((long) l.obj, (long) r.obj);
      } else {
        return xor((long) l.obj, (BigInteger) r.obj);
      }
    } else {
      if (r.obj == null) {
        return xor((BigInteger) l.obj, r.data);
      } else if (r.isLongMask()) {
        return xor((BigInteger) l.obj, (long) r.obj);
      } else {
        return xor((BigInteger) l.obj, (BigInteger) r.obj);
      }
    }
  }

  static IPyObj xor(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return xor(self, (PyInt) obj);
    }
    return NotImplemented;
  }

  private static IPyObj or(PyInt l, PyInt r) {
    if (l.obj == null) {
      if (r.obj == null) {
        return or(l.data, r.data);
      } else if (r.isLongMask()) {
        return or(l.data, (long) r.obj);
      } else {
        return or(l.data, (BigInteger) r.obj);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return or((long) l.obj, r.data);
      } else if (r.isLongMask()) {
        return or((long) l.obj, (long) r.obj);
      } else {
        return or((long) l.obj, (BigInteger) r.obj);
      }
    } else {
      if (r.obj == null) {
        return or((BigInteger) l.obj, r.data);
      } else if (r.isLongMask()) {
        return or((BigInteger) l.obj, (long) r.obj);
      } else {
        return or((BigInteger) l.obj, (BigInteger) r.obj);
      }
    }
  }

  static IPyObj or(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return or(self, (PyInt) obj);
    }
    return NotImplemented;
  }

  private static IPyObj pow(PyInt l, PyInt r, PyContext ctx) {
    if (l.obj == null) {
      if (r.obj == null) {
        return pow(l.data, r.data, ctx);
      } else if (r.isLongMask()) {
        return pow(l.data, (long) r.obj, ctx);
      } else {
        return pow(l.data, (BigInteger) r.obj, ctx);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return pow((long) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return pow((long) l.obj, (long) r.obj, ctx);
      } else {
        return pow((long) l.obj, (BigInteger) r.obj, ctx);
      }
    } else {
      if (r.obj == null) {
        return pow((BigInteger) l.obj, r.data, ctx);
      } else if (r.isLongMask()) {
        return pow((BigInteger) l.obj, (long) r.obj, ctx);
      } else {
        return pow((BigInteger) l.obj, (BigInteger) r.obj, ctx);
      }
    }
  }

  static IPyObj pow(PyInt self, IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return pow(self, (PyInt) obj, ctx);
    } else if (obj instanceof PyFloat) {
      return pow(IntLib.toDouble(self, ctx), ((PyFloat) obj).value, ctx);
    } else if (obj instanceof PyComplex) {
      return pow(IntLib.toDouble(self, ctx), (PyComplex) obj, ctx);
    }
    return NotImplemented;
  }

  private static IPyObj pow(PyInt l, PyInt r, IPyObj modulo, PyContext ctx) {
    if (l.obj == null) {
      if (r.obj == null) {
        return pow(l.data, r.data, modulo, ctx);
      } else if (r.isLongMask()) {
        return pow(l.data, (long) r.obj, modulo, ctx);
      } else {
        return pow(l.data, (BigInteger) r.obj, modulo, ctx);
      }
    } else if (l.isLongMask()) {
      if (r.obj == null) {
        return pow((long) l.obj, r.data, modulo, ctx);
      } else if (r.isLongMask()) {
        return pow((long) l.obj, (long) r.obj, modulo, ctx);
      } else {
        return pow((long) l.obj, (BigInteger) r.obj, modulo, ctx);
      }
    } else {
      if (r.obj == null) {
        return pow((BigInteger) l.obj, r.data, modulo, ctx);
      } else if (r.isLongMask()) {
        return pow((BigInteger) l.obj, (long) r.obj, modulo, ctx);
      } else {
        return pow((BigInteger) l.obj, (BigInteger) r.obj, modulo, ctx);
      }
    }
  }

  static IPyObj pow(PyInt self, IPyObj obj, IPyObj modulo, PyContext ctx) {
    if (obj instanceof PyInt) {
      return pow(self, (PyInt) obj, modulo, ctx);
    }
    return NotImplemented;
  }

  // END GENERATED CODE
}
