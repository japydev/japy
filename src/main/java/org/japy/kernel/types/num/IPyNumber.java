package org.japy.kernel.types.num;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.obj.proto.IPyObjWithEq;
import org.japy.kernel.types.obj.proto.IPyObjWithFormat;
import org.japy.kernel.types.obj.proto.IPyObjWithTruth;

public interface IPyNumber extends IPyObjWithTruth, IPyObjWithEq, IPyObjWithFormat {
  @InstanceAttribute
  IPyObj real(PyContext ctx);

  @InstanceAttribute
  IPyObj imag(PyContext ctx);

  @InstanceMethod
  IPyObj conjugate(PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __add__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __sub__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __mul__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __truediv__(@Param("rhs") IPyObj rhs, PyContext ctx);

  @SpecialInstanceMethod
  IPyObj __neg__(PyContext ctx);

  @SpecialInstanceMethod
  default IPyObj __pos__(PyContext ctx) {
    return this;
  }

  @SpecialInstanceMethod
  IPyObj __abs__(PyContext ctx);

  @InstanceMethod
  IPyObj __complex__(PyContext ctx);
}
