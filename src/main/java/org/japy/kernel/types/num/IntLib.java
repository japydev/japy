package org.japy.kernel.types.num;

import static org.japy.kernel.types.exc.px.PxException.typeError;
import static org.japy.parser.ParserUtil.isAsciiDigit;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.List;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.str.ObjFormatLib;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxOverflowError;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.util.Constants;
import org.japy.parser.ScannerError;
import org.japy.parser.python.Parser;
import org.japy.parser.python.impl.javacc.Token;
import org.japy.parser.python.impl.token.IntToken;

public class IntLib extends NumLib {
  private static final BigInteger BIG_MAX_DOUBLE = BigInteger.valueOf(2).pow(1024);
  private static final BigInteger BIG_MIN_DOUBLE = BIG_MAX_DOUBLE.negate();

  static double toDouble(long value, PyContext ctx) {
    return (double) value;
  }

  static double toDouble(PyInt self, PyContext ctx) {
    if (self.obj == null) {
      return self.data;
    }
    // TODO zzz
    if (self.isLongMask()) {
      return (double) self.longUnchecked();
    }
    BigInteger v = self.bigIntValue();
    if (v.compareTo(BIG_MAX_DOUBLE) < 0 && v.compareTo(BIG_MIN_DOUBLE) > 0) {
      return v.doubleValue();
    }
    throw new PxOverflowError("int too large to convert to float", ctx);
  }

  static boolean isEqual(PyInt lhs, PyInt rhs) {
    if (lhs.obj == null) {
      if (rhs.obj == null) {
        return lhs.data == rhs.data;
      } else if (rhs.isLongRep()) {
        return lhs.data == (long) rhs.obj;
      } else {
        return rhs.obj.equals(BigInteger.valueOf(lhs.data));
      }
    } else if (lhs.isLongRep()) {
      if (rhs.obj == null) {
        return (long) lhs.obj == rhs.data;
      } else if (rhs.isLongRep()) {
        return (long) lhs.obj == (long) rhs.obj;
      } else {
        return rhs.obj.equals(BigInteger.valueOf((long) lhs.obj));
      }
    } else {
      if (rhs.obj == null) {
        return lhs.obj.equals(BigInteger.valueOf(rhs.data));
      } else if (rhs.isLongRep()) {
        return lhs.obj.equals(BigInteger.valueOf((long) rhs.obj));
      } else {
        return lhs.obj.equals(rhs.obj);
      }
    }
  }

  private static boolean isEqual(PyInt lhs, long rhs) {
    if (lhs.obj == null) {
      return lhs.data == rhs;
    } else if (lhs.isLongRep()) {
      return (long) lhs.obj == rhs;
    } else {
      return lhs.obj.equals(BigInteger.valueOf(rhs));
    }
  }

  static boolean isEqual(PyInt lhs, double rhs) {
    if (DBL_MIN_LONG <= rhs && rhs < DBL_MAX_LONG_PLUS_ONE) {
      long whole = (long) rhs;
      return whole == rhs && isEqual(lhs, whole);
    }

    if (lhs.isSmallRep()) {
      return false;
    }

    double whole = Math.floor(rhs);
    if (whole != rhs) {
      return false;
    }

    return new BigDecimal(whole).toBigIntegerExact().equals(lhs.bigIntValue());
  }

  private static final double DBL_MIN_LONG = Long.MIN_VALUE;
  private static final double DBL_MAX_LONG_PLUS_ONE = -DBL_MIN_LONG;

  private static BigInteger hugeWholeDoubleToBigInt(double value) {
    return new BigDecimal(value).toBigIntegerExact();
  }

  static PyInt convertToInt(double value, PyContext ctx) {
    if (DBL_MIN_LONG <= value && value < DBL_MAX_LONG_PLUS_ONE) {
      return PyInt.get((long) value);
    }

    if (Double.isInfinite(value)) {
      throw new PxOverflowError("cannot convert float infinity to integer", ctx);
    } else if (Double.isNaN(value)) {
      throw PxException.valueError("cannot convert float NaN to integer", ctx);
    }

    if (value > 0) {
      return PyInt.getBigUnchecked(hugeWholeDoubleToBigInt(Math.floor(value)));
    } else {
      return PyInt.getBigUnchecked(hugeWholeDoubleToBigInt(Math.ceil(value)));
    }
  }

  static boolean lt(PyInt lhs, PyInt rhs) {
    if (!lhs.isBigRep()) {
      if (!rhs.isBigRep()) {
        return lhs.longValue() < rhs.longValue();
      } else {
        return lhs.bigIntValue().compareTo(rhs.bigIntUnchecked()) < 0;
      }
    } else {
      if (!rhs.isBigRep()) {
        return lhs.bigIntUnchecked().compareTo(rhs.bigIntValue()) < 0;
      } else {
        return lhs.bigIntUnchecked().compareTo(rhs.bigIntUnchecked()) < 0;
      }
    }
  }

  static boolean le(PyInt lhs, PyInt rhs) {
    if (!lhs.isBigRep()) {
      if (!rhs.isBigRep()) {
        return lhs.longValue() <= rhs.longValue();
      } else {
        return lhs.bigIntValue().compareTo(rhs.bigIntUnchecked()) <= 0;
      }
    } else {
      if (!rhs.isBigRep()) {
        return lhs.bigIntUnchecked().compareTo(rhs.bigIntValue()) <= 0;
      } else {
        return lhs.bigIntUnchecked().compareTo(rhs.bigIntUnchecked()) <= 0;
      }
    }
  }

  private static boolean lt(PyInt lhs, long rhs) {
    if (!lhs.isBigRep()) {
      return lhs.longValue() < rhs;
    } else {
      return lhs.bigIntUnchecked().compareTo(BigInteger.valueOf(rhs)) < 0;
    }
  }

  private static boolean le(PyInt lhs, long rhs) {
    if (!lhs.isBigRep()) {
      return lhs.longValue() <= rhs;
    } else {
      return lhs.bigIntUnchecked().compareTo(BigInteger.valueOf(rhs)) <= 0;
    }
  }

  private static boolean gt(PyInt lhs, long rhs) {
    if (!lhs.isBigRep()) {
      return lhs.longValue() > rhs;
    } else {
      return lhs.bigIntUnchecked().compareTo(BigInteger.valueOf(rhs)) > 0;
    }
  }

  private static boolean ge(PyInt lhs, long rhs) {
    if (!lhs.isBigRep()) {
      return lhs.longValue() >= rhs;
    } else {
      return lhs.bigIntUnchecked().compareTo(BigInteger.valueOf(rhs)) >= 0;
    }
  }

  private static boolean lt(PyInt lhs, BigInteger rhs) {
    return lhs.bigIntValue().compareTo(rhs) < 0;
  }

  private static boolean le(PyInt lhs, BigInteger rhs) {
    return lhs.bigIntValue().compareTo(rhs) <= 0;
  }

  private static boolean gt(PyInt lhs, BigInteger rhs) {
    return lhs.bigIntValue().compareTo(rhs) > 0;
  }

  private static boolean ge(PyInt lhs, BigInteger rhs) {
    return lhs.bigIntValue().compareTo(rhs) >= 0;
  }

  static boolean lt(PyInt lhs, double rhs) {
    double floor = Math.floor(rhs);
    if (DBL_MIN_LONG <= rhs && rhs < DBL_MAX_LONG_PLUS_ONE) {
      if (floor == rhs) {
        return lt(lhs, (long) floor);
      } else {
        return le(lhs, (long) floor);
      }
    }

    if (rhs == Double.POSITIVE_INFINITY) {
      return true;
    } else if (rhs == Double.NEGATIVE_INFINITY || Double.isNaN(rhs)) {
      return false;
    }

    if (floor == rhs) {
      return lt(lhs, hugeWholeDoubleToBigInt(floor));
    } else {
      return le(lhs, hugeWholeDoubleToBigInt(floor));
    }
  }

  static boolean le(PyInt lhs, double rhs) {
    double floor = Math.floor(rhs);
    if (DBL_MIN_LONG <= rhs && rhs < DBL_MAX_LONG_PLUS_ONE) {
      return le(lhs, (long) floor);
    }

    if (rhs == Double.POSITIVE_INFINITY) {
      return true;
    } else if (rhs == Double.NEGATIVE_INFINITY || Double.isNaN(rhs)) {
      return false;
    }

    return le(lhs, hugeWholeDoubleToBigInt(floor));
  }

  static boolean gt(PyInt lhs, double rhs) {
    double ceil = Math.ceil(rhs);
    if (DBL_MIN_LONG <= rhs && rhs < DBL_MAX_LONG_PLUS_ONE) {
      if (ceil == rhs) {
        return gt(lhs, (long) ceil);
      } else {
        return ge(lhs, (long) ceil);
      }
    }

    if (rhs == Double.POSITIVE_INFINITY || Double.isNaN(rhs)) {
      return false;
    } else if (rhs == Double.NEGATIVE_INFINITY) {
      return true;
    }

    if (ceil == rhs) {
      return gt(lhs, hugeWholeDoubleToBigInt(ceil));
    } else {
      return ge(lhs, hugeWholeDoubleToBigInt(ceil));
    }
  }

  static boolean ge(PyInt lhs, double rhs) {
    double ceil = Math.ceil(rhs);
    if (DBL_MIN_LONG <= rhs && rhs < DBL_MAX_LONG_PLUS_ONE) {
      return ge(lhs, (long) ceil);
    }

    if (rhs == Double.POSITIVE_INFINITY || Double.isNaN(rhs)) {
      return false;
    } else if (rhs == Double.NEGATIVE_INFINITY) {
      return true;
    }

    return ge(lhs, hugeWholeDoubleToBigInt(ceil));
  }

  static PyInt convertToInt(IPyObj obj, PyContext ctx) {
    if (obj.type() == PyInt.TYPE) {
      return (PyInt) obj;
    }
    if (obj.type() == PyFloat.TYPE) {
      return convertToInt(((PyFloat) obj).value, ctx);
    }
    if (obj instanceof PyStr) {
      return convertToInt(obj.toString(), obj, 10, ctx);
    }
    if (obj instanceof PyBytes) {
      // TODO
      return convertToInt(
          new String(((PyBytes) obj).internalArray(), StandardCharsets.UTF_8), obj, 10, ctx);
    }
    return toIntMeta(obj, ctx);
  }

  private static @Nullable PyInt tryCallIntMethod(String method, IPyObj obj, PyContext ctx) {
    @Nullable IPyObj meth = NumLib.getMethodIfExists(obj, method, ctx);
    if (meth == null) {
      return null;
    }
    IPyObj value = FuncLib.call(meth, obj, ctx);
    if (value.type() != PyInt.TYPE) {
      if (!(value instanceof PyInt)) {
        throw typeError(
            String.format(
                "%s returned an instance of type %s, expected an int", method, value.typeName()),
            ctx);
      } else {
        // TODO deprecation warning
        return new PyInt((PyInt) value);
      }
    } else {
      return (PyInt) value;
    }
  }

  private static @Nullable PyInt tryCallTrunc(IPyObj obj, PyContext ctx) {
    @Nullable IPyObj trunc = NumLib.getMethodIfExists(obj, Constants.__TRUNC__, ctx);
    if (trunc == null) {
      return null;
    }

    IPyObj value = FuncLib.call(trunc, obj, ctx);
    if (value.type() == PyInt.TYPE) {
      return (PyInt) value;
    } else if (value instanceof PyInt) {
      // TODO deprecation warning
      return new PyInt((PyInt) value);
    }

    @Var
    @Nullable
    PyInt intValue = tryCallIntMethod(Constants.__INT__, value, ctx);
    if (intValue != null) {
      return intValue;
    }

    intValue = tryCallIntMethod(Constants.__INDEX__, value, ctx);
    if (intValue != null) {
      return intValue;
    }

    throw typeError(
        String.format("__trunc__ returned non-Integral (type %s)", value.typeName()), ctx);
  }

  private static PyInt toIntMeta(IPyObj obj, PyContext ctx) {
    @Var
    @Nullable
    PyInt value = tryCallIntMethod(Constants.__INT__, obj, ctx);
    if (value != null) {
      return value;
    }

    value = tryCallIntMethod(Constants.__INDEX__, obj, ctx);
    if (value != null) {
      return value;
    }

    value = tryCallTrunc(obj, ctx);
    if (value != null) {
      return value;
    }

    throw typeError(String.format("can't convert %s to int", obj.typeName()), ctx);
  }

  public static PyInt asInt(IPyObj obj, PyContext ctx) {
    if (obj instanceof PyInt) {
      return (PyInt) obj;
    }

    @Nullable PyInt value = tryCallIntMethod(Constants.__INDEX__, obj, ctx);
    if (value != null) {
      return value;
    }

    throw typeError(
        String.format("'%s' object cannot be interpreted as an integer", obj.typeName()), ctx);
  }

  public static PyInt asIntExact(IPyObj obj, PyContext ctx) {
    PyInt v = asInt(obj, ctx);
    if (v.type() == PyInt.TYPE) {
      return v;
    }
    return new PyInt(v);
  }

  private static PxException throwInvalidIntLiteral(IPyObj str, int base, PyContext ctx) {
    throw PxException.valueError(
        String.format("invalid literal for int() with base %s: ", base) + PrintLib.repr(str, ctx),
        ctx);
  }

  @SuppressWarnings("UnusedException")
  private static PyInt toIntBase0(String text, IPyObj str, PyContext ctx) {
    List<Token> tokens;
    try {
      tokens = Parser.tokenize(text);
    } catch (ScannerError ex) {
      throw throwInvalidIntLiteral(str, 0, ctx);
    }

    @Var boolean minus = false;
    Token token;
    switch (tokens.size()) {
      case 1:
        token = tokens.get(0);
        break;
      case 2:
        {
          Token prefix = tokens.get(0);
          switch (prefix.kind) {
            case Token.ADD:
              break;
            case Token.SUB:
              minus = true;
              break;
            default:
              throw throwInvalidIntLiteral(str, 0, ctx);
          }
          token = tokens.get(1);
          break;
        }
      default:
        throw throwInvalidIntLiteral(str, 0, ctx);
    }

    if (token.kind == Token.INT) {
      IntToken t = (IntToken) token;
      return PyInt.get(minus ? t.value.negate() : t.value);
    }

    throw throwInvalidIntLiteral(str, 0, ctx);
  }

  private static PyInt maybeMinus(int value, boolean minus) {
    if (!minus) {
      return PyInt.get(value);
    } else if (value == Integer.MIN_VALUE) {
      // https://youtrack.jetbrains.com/issue/IDEA-286284
      //noinspection UnnecessaryExplicitNumericCast:
      return PyInt.get(-(long) value);
    } else {
      return PyInt.get(-value);
    }
  }

  private static PyInt maybeMinus(BigInteger value, boolean minus) {
    return PyInt.getMinimal(minus ? value.negate() : value);
  }

  private static boolean startsWithNumberLiteralPrefix(String s, char c1, char c2) {
    if (s.length() < 2 || s.charAt(0) != '0') {
      return false;
    }
    char c = s.charAt(1);
    return c == c1 || c == c2;
  }

  private static boolean startsWithNumberLiteralPrefix(String text, int base) {
    return (base == 2 && startsWithNumberLiteralPrefix(text, 'b', 'B'))
        || (base == 8 && startsWithNumberLiteralPrefix(text, 'o', 'O'))
        || (base == 16 && startsWithNumberLiteralPrefix(text, 'x', 'X'));
  }

  @SuppressWarnings("UnusedException")
  private static PyInt convertToInt(@Var String text, IPyObj str, int base, PyContext ctx) {
    if (base < 0) {
      throw PxException.valueError(String.format("invalid int base %s", base), ctx);
    } else if (base == 0) {
      return toIntBase0(text, str, ctx);
    }

    @Var boolean minus = false;
    text = removeUnderscores(text.strip());
    if (text.startsWith("+")) {
      text = text.substring(1).strip();
    } else if (text.startsWith("-") && text.length() > 1 && !isAsciiDigit(text.charAt(1))) {
      text = text.substring(1).strip();
      minus = true;
    }

    try {
      int value = Integer.parseInt(text, base);
      return maybeMinus(value, minus);
    } catch (NumberFormatException ignored) {
      //
    }

    try {
      BigInteger value = new BigInteger(text, base);
      return maybeMinus(value, minus);
    } catch (NumberFormatException ignored) {
      //
    }

    if (startsWithNumberLiteralPrefix(text, base)) {
      text = text.substring(2);
    } else {
      throw throwInvalidIntLiteral(str, base, ctx);
    }

    try {
      BigInteger value = new BigInteger(text, base);
      return maybeMinus(value, minus);
    } catch (NumberFormatException ignored) {
      //
    }

    throw throwInvalidIntLiteral(str, base, ctx);
  }

  private static String removeUnderscores(String s) {
    int first = s.indexOf('_');
    if (first == -1) {
      return s;
    }

    StringBuilder sb = new StringBuilder(s.length());
    @Var boolean lastDigit = false;
    int len = s.length();
    for (int i = 0; i != len; ++i) {
      char c = s.charAt(i);
      if (isAsciiDigit(c)) {
        lastDigit = true;
        sb.append(c);
      } else if (c == '+' || c == '-') {
        lastDigit = false;
        sb.append(c);
      } else if (c != '_' || !lastDigit || i == len - 1) {
        return s;
      } else {
        lastDigit = false;
      }
    }
    return sb.toString();
  }

  private static int getBase(IPyObj baseArg, PyContext ctx) {
    if (baseArg instanceof PyInt) {
      if (!((PyInt) baseArg).isSmall()) {
        throw PxException.valueError("invalid base", ctx);
      }
      int base = ((PyInt) baseArg).intValue();
      if (base < 0 || base == 1 || base > 36) {
        throw PxException.valueError("invalid base", ctx);
      }
      return base;
    }

    IPyObj index = NumLib.getMethod(baseArg, Constants.__INDEX__, ctx);
    IPyObj base = FuncLib.call(index, baseArg, ctx);
    if (!(base instanceof PyInt)) {
      throw typeError(
          String.format(
              "__index__ returned an object of type %s, expected an int", base.typeName()),
          ctx);
    }
    return getBase(base, ctx);
  }

  static PyInt convertToInt(IPyObj str, IPyObj baseArg, PyContext ctx) {
    int base = getBase(baseArg, ctx);
    if (str instanceof PyStr) {
      return convertToInt(str.toString(), str, base, ctx);
    }
    if (str instanceof PyBytes) {
      // TODO
      return convertToInt(
          new String(((PyBytes) str).internalArray(), StandardCharsets.UTF_8), str, base, ctx);
    }
    throw typeError(String.format("expected a string or bytes, got '%s'", str.typeName()), ctx);
  }

  static PyStr format(PyInt self, ObjFormatLib.NumSpec spec, PyContext ctx) {
    // TODO zzz
    if (self.obj == null) {
      return PyStr.ucs2(Integer.toString(self.data));
    } else {
      return PyStr.ucs2(self.obj.toString());
    }
  }

  public static PyInt gcd(PyInt a, PyInt b) {
    return PyInt.getMinimal(a.bigIntValue().gcd(b.bigIntValue()));
  }

  private static PyInt intForOps(IPyObj n, PyContext ctx) {
    if (n instanceof PyInt) {
      return (PyInt) n;
    }

    @Nullable PyInt intValue = tryCallIntMethod(Constants.__INDEX__, n, ctx);
    if (intValue != null) {
      return intValue;
    }

    throw typeError("an int or an object with __index__ is required", ctx);
  }

  private static String bin(PyInt n) {
    if (n.isSmallRep()) {
      return Integer.toBinaryString(n.intUnchecked());
    } else if (n.isLongMask()) {
      return Long.toBinaryString(n.longUnchecked());
    } else {
      return n.bigIntUnchecked().toString(2);
    }
  }

  private static String oct(PyInt n) {
    if (n.isSmallRep()) {
      return Integer.toOctalString(n.intUnchecked());
    } else if (n.isLongMask()) {
      return Long.toOctalString(n.longUnchecked());
    } else {
      return n.bigIntUnchecked().toString(8);
    }
  }

  private static String hex(PyInt n) {
    if (n.isSmallRep()) {
      return Integer.toHexString(n.intUnchecked());
    } else if (n.isLongMask()) {
      return Long.toHexString(n.longUnchecked());
    } else {
      return n.bigIntUnchecked().toString(16);
    }
  }

  private static IPyObj toStringWithPrefix(IPyObj n, int base, PyContext ctx) {
    @Var PyInt i = intForOps(n, ctx);
    int sign = i.signum();
    @Var String signPrefix = "";
    if (sign == -1) {
      i = IntOps.neg(i);
      signPrefix = "-";
    } else if (sign == 0) {
      return PyStr.ucs2("0");
    }

    switch (base) {
      case 2:
        return PyStr.ucs2(signPrefix + 'b' + bin(i));
      case 8:
        return PyStr.ucs2(signPrefix + 'o' + oct(i));
      case 16:
        return PyStr.ucs2(signPrefix + 'x' + hex(i));
      default:
        throw InternalErrorException.notReached();
    }
  }

  public static IPyObj bin(IPyObj n, PyContext ctx) {
    return toStringWithPrefix(n, 2, ctx);
  }

  public static IPyObj oct(IPyObj n, PyContext ctx) {
    return toStringWithPrefix(n, 8, ctx);
  }

  public static IPyObj hex(IPyObj n, PyContext ctx) {
    return toStringWithPrefix(n, 16, ctx);
  }
}
