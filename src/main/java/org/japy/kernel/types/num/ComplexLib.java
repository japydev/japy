package org.japy.kernel.types.num;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Warnings;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.str.ObjFormatLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.Constants;

public class ComplexLib extends ComplexOps {
  public static PyComplex toComplex(IPyObj obj, PyContext ctx) {
    if (obj.type() == PyComplex.TYPE) {
      return (PyComplex) obj;
    }
    if (obj.type() == PyFloat.TYPE) {
      return new PyComplex(((PyFloat) obj).value, 0);
    }
    if (obj.type() == PyInt.TYPE) {
      return new PyComplex(IntLib.toDouble((PyInt) obj, ctx), 0);
    }

    if (obj instanceof PyStr) {
      return toComplex(obj.toString(), ctx);
    }

    @Nullable
    IPyObj func = ObjLib.getClassAttributeOrNull(obj.type(), null, Constants.__COMPLEX__, ctx);
    if (func != null) {
      IPyObj value = FuncLib.call(func, obj, ctx);
      if (value.type() == PyComplex.TYPE) {
        return (PyComplex) value;
      }
      if (value instanceof PyComplex) {
        Warnings.warn(
            String.format(
                "__complex__ returned non-complex (type %s). "
                    + "The ability to return an instance of a strict subclass of complex is deprecated, "
                    + "and may be removed in a future version of Python.",
                value.typeName()),
            ctx);
        return (PyComplex) value;
      }
      throw PxException.typeError(
          String.format(
              "__complex__ returned an instance of type '%s', expected a complex",
              value.typeName()),
          ctx);
    }

    return new PyComplex(FloatLib.toFloat(obj, ctx).value, 0);
  }

  private static PyComplex toComplex(String str, PyContext ctx) {
    throw new NotImplementedException("toComplex(String)");
  }

  public static PyComplex toComplex(IPyObj real, IPyObj imag, PyContext ctx) {
    if (real instanceof PyStr) {
      throw PxException.typeError("complex() can't take second arg if first is a string", ctx);
    } else if (imag instanceof PyStr) {
      throw PxException.typeError("complex() second arg can't be a string", ctx);
    }

    if (real.type() == PyFloat.TYPE && imag.type() == PyFloat.TYPE) {
      // this preserves negative zeros and non-finite values
      return new PyComplex(((PyFloat) real).value, ((PyFloat) imag).value);
    }

    PyComplex creal = toComplex(real, ctx);
    PyComplex cimag = toComplex(imag, ctx);
    return creal.add(cimag.multiply(PyComplex.I));
  }

  public static PyStr format(PyComplex self, ObjFormatLib.NumSpec spec, PyContext ctx) {
    throw new NotImplementedException("format");
  }
}
