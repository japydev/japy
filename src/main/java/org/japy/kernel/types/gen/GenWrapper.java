package org.japy.kernel.types.gen;

import static org.japy.kernel.types.exc.px.PxException.typeError;
import static org.japy.kernel.types.misc.PyNone.None;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.exc.py.ExcLib;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

class GenWrapper {
  private final IPyObj gen;
  private final @Nullable FuncHandle next;
  private final @Nullable IPyObj send;
  private final @Nullable IPyObj throw_;
  private final @Nullable IPyObj close;

  GenWrapper(IPyObj gen, PyContext ctx) {
    this.gen = gen;

    if (!(gen instanceof IPyIter)) {
      @Nullable FuncHandle next = gen.specialMethod(SpecialMethod.NEXT);
      if (next == null) {
        throw typeError(
            String.format("object of type '%s' does not have __next__ method", gen.typeName()),
            ctx);
      }
      this.next = next;
    } else {
      next = null;
    }

    send = ObjLib.getClassAttributeOrNull(gen.type(), null, Constants.SEND, ctx);
    throw_ = ObjLib.getClassAttributeOrNull(gen.type(), null, Constants.THROW, ctx);
    close = ObjLib.getClassAttributeOrNull(gen.type(), null, Constants.CLOSE, ctx);
  }

  void close(PyContext ctx) {
    if (close != null) {
      FuncLib.call(close, gen, ctx);
    }
  }

  IPyObj resume(IPyObj value, @Nullable PyBaseException ex, PyContext ctx) {
    if (ex != null) {
      if (throw_ == null) {
        throw ex.pxExc();
      }

      @Nullable PyBaseException savedEx = ExcLib.setCurrentExc(ex, ctx);
      try {
        // TODO traceback
        return FuncLib.call(throw_, Args.of(gen, ex.type(), ex, None), ctx);
      } finally {
        ExcLib.setCurrentExc(savedEx, ctx);
      }
    }

    if (value != None) {
      if (send == null) {
        throw typeError(
            String.format("object of type '%s' does not have 'send' method", gen.typeName()), ctx);
      }
      return FuncLib.call(send, gen, value, ctx);
    }

    if (next == null) {
      return ((IPyIter) gen).next(ctx);
    }

    try {
      return (IPyObj) next.handle.invokeExact(gen, ctx);
    } catch (Throwable nextEx) {
      throw ExcUtil.rethrow(nextEx);
    }
  }
}
