package org.japy.kernel.types.gen;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;

/** Interface used by the generator/async generator/coroutine implementations */
public interface IPyGeneratorSupport extends IPyObj {
  class Continue {
    public final IPyObj value;

    public Continue(IPyObj value) {
      this.value = value;
    }
  }

  /**
   * Generator implementation invokes this in the `yield from` expression. This either returns a
   * value yielded by the subgenerator, in which case the generator goes to sleep, or an instance of
   * Continue, which becomes the value of the yield expression and the generator continues
   * execution.
   */
  Object yieldFrom(IPyObj list, PyContext ctx);

  /** `await` expression */
  Object await(IPyObj awaitable, PyContext ctx);

  /**
   * This is called to handle exceptions thrown from the body of the generator. For example,
   * generators turn StopIteration into a RuntimeError using this method.
   */
  Throwable rethrow(Throwable ex, PyContext ctx);

  /**
   * This is called when the generator executes a `return` expression, it throws the result
   * (generators don't ever return normally).
   */
  Throwable returnValue(IPyObj value, PyContext ctx);
}
