package org.japy.kernel.types.gen;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.exc.px.PxException.valueError;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PySentinel.nullIfSentinel;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.iter.IterLib;
import org.japy.kernel.types.exc.px.PxBaseException;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxGeneratorExit;
import org.japy.kernel.types.exc.px.PxRuntimeError;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.exc.py.ExcLib;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyGeneratorExit;
import org.japy.kernel.types.obj.IPyNoValidationObj;

public final class PyGenerator implements IPyGenerator, IPyGeneratorSupport, IPyNoValidationObj {
  public static final IPyClass TYPE =
      new PyBuiltinClass("generator", PyGenerator.class, PyBuiltinClass.INTERNAL);

  private static final int BORN = 0;
  private static final int SUSPENDED = 1;
  private static final int RUNNING = 2;
  private static final int FINISHED = 3;

  private int state;
  private final GeneratorImpl impl;
  private @Nullable GenWrapper subgen;
  private @Nullable PyBaseException curExc;

  public PyGenerator(GeneratorImpl impl) {
    this.impl = impl;
    impl.setSupport(this);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj send(IPyObj value, PyContext ctx) {
    return resume(value, null, ctx);
  }

  @Override
  public IPyObj throw_(IPyObj typ, IPyObj val, IPyObj tb, PyContext ctx) {
    @Var PyBaseException ex;
    try {
      ex = PyBaseException.parse("throw", typ, nullIfSentinel(val), nullIfSentinel(tb), ctx);
    } catch (Throwable parseEx) {
      // This exception needs to be thrown into the generator as if the generator
      // did `resume(typ, val, tb)`.
      ex = ExcLib.captureExc(parseEx, ctx);
    }
    return resume(None, ex, ctx);
  }

  @Override
  public IPyObj close(PyContext ctx) {
    if (state == RUNNING) {
      throw valueError("generator is running", ctx);
    }

    if (state == FINISHED) {
      return None;
    }

    try {
      @SuppressWarnings("unused")
      IPyObj v = resume(None, new PyGeneratorExit(ctx), ctx);
    } catch (PxGeneratorExit ignored) {
      //
    }
    // TODO: what if it eats the exception and doesn't terminate?
    //    dcheck(stage == FINISHED);
    return None;
  }

  private IPyObj resume(IPyObj value, @Nullable PyBaseException ex, PyContext ctx) {
    if (Debug.ENABLED) {
      dcheck(subgen == null || state == SUSPENDED || state == RUNNING);
    }

    switch (state) {
      case BORN:
        if (value != None) {
          throw PxException.typeError("can't send non-None value to a just-started generator", ctx);
        }
        if (ex != null) {
          state = FINISHED;
          throw ex.pxExc();
        }
        state = SUSPENDED;
        break;

      case RUNNING:
        throw valueError("generator is already running", ctx);

      case SUSPENDED:
        break;

      case FINISHED:
        if (ex != null) {
          throw ex.pxExc();
        } else {
          throw new PxStopIteration(ctx);
        }

      default:
        throw InternalErrorException.notReached();
    }

    if (subgen != null) {
      return resumeSubgen(value, ex, ctx);
    }

    try {
      state = RUNNING;
      IPyObj retVal = impl.resume(value, ex, ctx);
      state = SUSPENDED;
      return retVal;
    } catch (Throwable genEx) {
      state = FINISHED;
      throw genEx;
    }
  }

  private IPyObj resumeSubgen(IPyObj value, @Nullable PyBaseException ex, PyContext ctx) {
    dcheckNotNull(subgen);

    @Var IPyObj myValue = None;
    @Var
    @Nullable
    PyBaseException myEx = null;

    if (ex != null && ex.isInstanceRaw(PyBuiltinExc.GeneratorExit)) {
      try {
        subgen.close(ctx);
      } catch (Throwable closeEx) {
        // TODO warn
      }
      myEx = ex;
    } else {
      try {
        return subgen.resume(value, ex, ctx);
      } catch (PxStopIteration siex) {
        myValue = siex.value();
      } catch (Throwable subEx) {
        myEx = ExcLib.captureExc(subEx, ctx);
      }
    }

    subgen = null;
    return resume(myValue, myEx, ctx);
  }

  @Override
  public Object yieldFrom(IPyObj list, PyContext ctx) {
    IPyObj iter = IterLib.iter(list, ctx);
    try {
      IPyObj value = IterLib.next(iter, ctx);
      subgen = new GenWrapper(iter, ctx);
      return value;
    } catch (PxStopIteration ex) {
      return new Continue(ex.value());
    }
  }

  @Override
  public Object await(IPyObj awaitable, PyContext ctx) {
    throw InternalErrorException.notReached();
  }

  @Override
  public Throwable rethrow(Throwable ex, PyContext ctx) {
    if (ex instanceof PxBaseException) {
      PyBaseException pex = ((PxBaseException) ex).pyExc;
      if (pex.isInstanceRaw(PyBuiltinExc.StopIteration)) {
        throw new PxRuntimeError("generator raised StopIteration", ctx, ex);
      }
    }

    throw ExcUtil.rethrow(ex);
  }

  @Override
  public Throwable returnValue(IPyObj value, PyContext ctx) {
    throw new PxStopIteration(value, ctx);
  }
}
