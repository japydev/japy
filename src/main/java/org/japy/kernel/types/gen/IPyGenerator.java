package org.japy.kernel.types.gen;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.iter.IPyIter;

public interface IPyGenerator extends IPyIter {
  @Override
  default IPyObj next(PyContext ctx) {
    return send(None, ctx);
  }

  @InstanceMethod
  IPyObj send(@Param("value") IPyObj value, PyContext ctx);

  @InstanceMethod("throw")
  IPyObj throw_(
      @Param("typ") IPyObj typ,
      @Param(value = "val", dflt = DEFAULT_SENTINEL) IPyObj val,
      @Param(value = "tb", dflt = DEFAULT_SENTINEL) IPyObj tb,
      PyContext ctx);

  @InstanceMethod
  IPyObj close(PyContext ctx);
}
