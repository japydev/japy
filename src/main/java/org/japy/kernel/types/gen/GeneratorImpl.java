package org.japy.kernel.types.gen;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.func.PyFuncHeader;

/**
 * Body of the generator/coroutine - the object which actually executes the generator/coroutine's
 * code. It is not a python object, it's a helper object for
 * PyGenerator/PyAsyncGenerator/PyCoroutine.
 */
public abstract class GeneratorImpl {
  protected @Nullable IPyGeneratorSupport support;
  protected final PyFuncHeader header;

  protected GeneratorImpl(PyFuncHeader header) {
    this.header = header;
  }

  protected abstract IPyObj resume(IPyObj value, @Nullable PyBaseException ex, PyContext ctx);

  public void setSupport(IPyGeneratorSupport support) {
    this.support = support;
  }
}
