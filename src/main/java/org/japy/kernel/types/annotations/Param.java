package org.japy.kernel.types.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.japy.base.ParamKind;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface Param {
  String value();

  ParamKind kind() default ParamKind.POS_ONLY;

  ParamDefault dflt() default ParamDefault.NO_DEFAULT;

  String defaultStr() default "";
}
