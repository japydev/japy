package org.japy.kernel.types.mod;

import org.japy.kernel.types.obj.IPyInstanceDict;

public interface IPyModuleDict extends IPyInstanceDict {}
