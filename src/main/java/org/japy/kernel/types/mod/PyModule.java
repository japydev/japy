package org.japy.kernel.types.mod;

import static org.japy.kernel.types.misc.PyNone.None;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.obj.IPyInstanceDict;
import org.japy.kernel.types.obj.PyObject;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyModule implements IPyModule {
  public static final IPyClass TYPE =
      new PyBuiltinClass("module", PyModule.class, PyBuiltinClass.REGULAR);

  private final PyModuleDict dict = new PyModuleDict();
  private final String name;

  public PyModule(String name) {
    this.name = name;
  }

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(2).addPosOnly("name").addPosOnly("doc", None).build();

  @Constructor
  @SubClassConstructor
  public PyModule(Args args, PyContext ctx) {
    IPyObj[] argVals = ARG_PARSER.parse(args, "module", ctx);
    throw new NotImplementedException("PyModule");
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyInstanceDict instanceDict() {
    return dict;
  }

  @Override
  public IPyModuleDict moduleDict() {
    return dict;
  }

  @Override
  public void validate(Validator v) {
    v.validate(dict);
  }

  public String name() {
    return name;
  }

  @Override
  public @Nullable IPyObj get(String name) {
    return dict.get(name);
  }

  public boolean contains(String name) {
    return dict.contains(name);
  }

  @Override
  public void set(String name, IPyObj value) {
    dict.set(name, value);
  }

  @SpecialInstanceMethod
  @SuppressWarnings("UnusedException")
  public IPyObj __getattribute__(@Param("attr") IPyObj attr, PyContext ctx) {
    try {
      return PyObject.__getattribute__(this, attr, ctx);
    } catch (PxAttributeError ignored) {
      throw new PxAttributeError(
          String.format("module '%s' has no attribute '%s'", name, attr), ctx);
    }
  }
}
