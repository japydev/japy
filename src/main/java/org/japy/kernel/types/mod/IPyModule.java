package org.japy.kernel.types.mod;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

public interface IPyModule extends IPyTrueAtomObj {
  IPyModuleDict moduleDict();

  @Nullable
  IPyObj get(String name);

  void set(String name, IPyObj obj);
}
