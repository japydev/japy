package org.japy.kernel.types.mod;

import org.japy.kernel.exec.IFrame;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;

public interface IPyExtModuleExec extends IFrame {
  void exec(IPyDict namespace, IPyRODict builtins, PyContext ctx);
}
