package org.japy.kernel.types.mod;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.PyAttrDict;

class PyModuleDict extends PyAttrDict implements IPyModuleDict {
  private static final IPyClass TYPE =
      new PyBuiltinClass("moduledict", PyModuleDict.class, PyBuiltinClass.INTERNAL);

  public PyModuleDict() {}

  private PyModuleDict(PyModuleDict dict) {
    super(dict);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    return new PyModuleDict(this);
  }
}
