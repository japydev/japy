package org.japy.kernel.types.typing;

import java.lang.invoke.MethodHandles;

import org.japy.base.ParamKind;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyGenericAlias implements IPyTrueAtomObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "GenericAlias", PyGenericAlias.class, MethodHandles.lookup(), PyBuiltinClass.REGULAR);

  private final IPyClass origin;
  private final PyTuple args;

  public PyGenericAlias(IPyClass origin, PyTuple args) {
    this.origin = origin;
    this.args = args;
  }

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(2)
          .add("origin", ParamKind.POS_ONLY)
          .add("args", ParamKind.POS_ONLY)
          .build();

  @Constructor
  @SubClassConstructor
  public PyGenericAlias(Args args, PyContext ctx) {
    IPyObj[] argVals = ARG_PARSER.parse(args, "GenericAlias", ctx);
    this.origin = ArgLib.checkArgClass("GenericAlias", "origin", argVals[0], ctx);
    this.args = ArgLib.checkArgTuple("GenericAlias", "args", argVals[1], ctx);
  }

  @InstanceAttribute
  private IPyObj __origin__(PyContext ctx) {
    return origin;
  }

  @InstanceAttribute
  private IPyObj __args__(PyContext ctx) {
    return args;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public void validate(Validator v) {}
}
