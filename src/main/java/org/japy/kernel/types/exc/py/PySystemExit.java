package org.japy.kernel.types.exc.py;

import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.util.Args;

public class PySystemExit extends PyExc {
  @Constructor
  @SubClassConstructor
  public PySystemExit(IPyClass cls, Args args, PyContext ctx) {
    super(cls, args, ctx);
  }

  @InstanceAttribute
  public IPyObj code(PyContext ctx) {
    return args.isEmpty() ? None : args.get(0);
  }
}
