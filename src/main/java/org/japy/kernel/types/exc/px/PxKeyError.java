package org.japy.kernel.types.exc.px;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxKeyError extends PxLookupError {
  public PxKeyError(String key, PyContext ctx) {
    super(key, PyBuiltinExc.KeyError, new PyTuple(PyStr.get(key)), ctx, null);
  }

  public PxKeyError(IPyObj key, PyContext ctx) {
    super(key.toString(), PyBuiltinExc.KeyError, new PyTuple(key), ctx, null);
  }

  protected PxKeyError(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxKeyError(PyBaseException pyExc) {
    super(pyExc);
  }
}
