package org.japy.kernel.types.exc.px;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxNameError extends PxException {
  public PxNameError(String message, PyContext ctx) {
    super(message, PyBuiltinExc.NameError, ctx);
  }

  protected PxNameError(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxNameError(PyBaseException pyExc) {
    super(pyExc);
  }

  public static PxNameError nameNotDefined(PyStr name, PyContext ctx) {
    return new PxNameError(String.format("name '%s' is not defined", name), ctx);
  }

  public static PxNameError nameNotDefined(String name, PyContext ctx) {
    return new PxNameError(String.format("name '%s' is not defined", name), ctx);
  }

  public static PxNameError upvalueNotDefined(String name, PyContext ctx) {
    return new PxNameError(
        String.format("free variable '%s' referenced before assignment in enclosing scope", name),
        ctx);
  }
}
