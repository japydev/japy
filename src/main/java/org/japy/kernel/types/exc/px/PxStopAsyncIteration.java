package org.japy.kernel.types.exc.px;

import static org.japy.kernel.types.coll.seq.PyTuple.EMPTY_TUPLE;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxStopAsyncIteration extends PxException {
  public PxStopAsyncIteration(PyContext ctx) {
    super(null, PyBuiltinExc.StopAsyncIteration, EMPTY_TUPLE, ctx, null);
  }

  public PxStopAsyncIteration(IPyObj value, PyContext ctx) {
    super(null, PyBuiltinExc.StopAsyncIteration, new PyTuple(value), ctx, null);
  }

  protected PxStopAsyncIteration(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxStopAsyncIteration(PyBaseException pyExc) {
    super(pyExc);
  }
}
