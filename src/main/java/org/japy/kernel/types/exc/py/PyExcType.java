package org.japy.kernel.types.exc.py;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.util.Arrays;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.PyObjectClass;
import org.japy.kernel.types.cls.SpecialMethodTable;
import org.japy.kernel.types.cls.builtin.BuiltinClassLib;
import org.japy.kernel.types.cls.builtin.PyAbstractBuiltinClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.cls.ext.PyExtBuiltinSubClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.*;
import org.japy.kernel.util.Args;

public class PyExcType extends PyAbstractBuiltinClass {
  private final SpecialMethodTable originalMethodTable;
  private final SpecialMethodTable methodTable;
  private final Class<? extends PyBaseException> pyClass;
  private final Class<? extends PxBaseException> pxClass;
  private final FuncHandle pyConstructor;
  private final FuncHandle pxConstructor;
  private final @Nullable String doc;

  PyExcType() {
    super("BaseException", PyObjectClass.INSTANCE);
    doc = "Common base class for all exceptions";
    pyClass = PyBaseException.class;
    pxClass = PxBaseException.class;
    pyConstructor = getPyConstructor(pyClass);
    pxConstructor = getPxConstructor(pxClass);
    BuiltinClassLib.ClassData classData =
        BuiltinClassLib.initializeBuiltin(
            this,
            PyBaseException.class,
            MethodHandles.lookup(),
            BuiltinClassLib.INSPECT_CLASS
                | BuiltinClassLib.INSPECT_SUPERS
                | BuiltinClassLib.ADD_NOOP_INIT);
    originalMethodTable = classData.original;
    methodTable = classData.complete;
    BuiltinClassLib.finishClassSetup(this);
  }

  public PyExcType(
      String name,
      PyExcType[] parents,
      Class<? extends PyExc> pyClass,
      Class<? extends PxBaseException> pxClass) {
    this(name, parents, pyClass, pxClass, null);
  }

  public PyExcType(
      String name,
      PyExcType[] parents,
      Class<? extends PyExc> pyClass,
      Class<? extends PxBaseException> pxClass,
      @Nullable String doc) {
    super(name, parents);

    this.doc = doc;
    this.pyClass = pyClass;
    this.pxClass = pxClass;

    if (Debug.ENABLED) {
      for (PyExcType parent : parents) {
        dcheck(parent.pyClass.isAssignableFrom(pyClass));
        dcheck(parent.pxClass.isAssignableFrom(pxClass));
      }
    }

    boolean newPyClass = Arrays.stream(parents).noneMatch(p -> p.pyClass == pyClass);
    if (newPyClass) {
      pyConstructor = getPyConstructor(pyClass);
      dcheckNotNull(PyBuiltinClass.getSubClassConstructor(pyClass));
    } else {
      pyConstructor = parents[0].pyConstructor;
    }

    if (parents.length != 1 || pxClass != parents[0].pxClass) {
      pxConstructor = getPxConstructor(pxClass);
    } else {
      pxConstructor = parents[0].pxConstructor;
    }

    BuiltinClassLib.ClassData classData =
        BuiltinClassLib.initializeBuiltin(
            this,
            pyClass,
            MethodHandles.lookup(),
            BuiltinClassLib.ADD_NOOP_INIT | (newPyClass ? BuiltinClassLib.INSPECT_CLASS : 0));
    originalMethodTable = classData.original;
    methodTable = classData.complete;
    BuiltinClassLib.finishClassSetup(this);
  }

  public PyExcType(
      String name,
      PyExcType parent,
      Class<? extends PyExc> pyClass,
      Class<? extends PxBaseException> pxClass) {
    this(name, parent, pyClass, pxClass, null);
  }

  public PyExcType(
      String name,
      PyExcType parent,
      Class<? extends PyExc> pyClass,
      Class<? extends PxBaseException> pxClass,
      @Nullable String doc) {
    this(name, new PyExcType[] {parent}, pyClass, pxClass, doc);
  }

  PyExcType(String name, PyExcType parent) {
    this(name, parent, parent.pxClass, null);
  }

  PyExcType(String name, PyExcType parent, Class<? extends PxBaseException> pxClass) {
    this(name, parent, pxClass, null);
  }

  @SuppressWarnings("unchecked")
  PyExcType(
      String name,
      PyExcType parent,
      Class<? extends PxBaseException> pxClass,
      @Nullable String doc) {
    this(
        name,
        parent,
        parent.pyClass == PyBaseException.class
            ? PyExc.class
            : (Class<? extends PyExc>) parent.pyClass,
        pxClass,
        doc);
  }

  private static FuncHandle getPyConstructor(Class<? extends PyBaseException> pyClass) {
    try {
      Constructor<?> constructor =
          pyClass.getConstructor(IPyClass.class, Args.class, PyContext.class);
      FuncHandle fh = FuncHandle.constructor(constructor, MethodHandles.lookup());
      return fh.asType(fh.type().changeReturnType(PyBaseException.class));
    } catch (NoSuchMethodException ex) {
      throw new InternalErrorException(
          String.format("class %s does not have public generic constructor", pyClass), ex);
    }
  }

  private static FuncHandle getPxConstructor(Class<? extends PxBaseException> pxClass) {
    try {
      Constructor<?> constructor = pxClass.getConstructor(PyBaseException.class);
      FuncHandle h = FuncHandle.constructor(constructor, MethodHandles.lookup());
      return h.asType(h.type().changeReturnType(PxBaseException.class));
    } catch (NoSuchMethodException ex) {
      throw new InternalErrorException(
          String.format("class %s does not have public generic constructor", pxClass), ex);
    }
  }

  public static PxBaseException makeJavaException(PyBaseException pex) {
    IPyClass cls = pex.type();
    FuncHandle pxConstructor;
    if (cls instanceof PyExcType) {
      pxConstructor = ((PyExcType) cls).pxConstructor;
    } else {
      pxConstructor = ((PyExcType) ((PyExtBuiltinSubClass) cls).builtinAncestor()).pxConstructor;
    }

    try {
      return (PxBaseException) pxConstructor.handle.invokeExact(pex);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public SpecialMethodTable methodTable() {
    return methodTable;
  }

  @Override
  public SpecialMethodTable originalMethodTable() {
    return originalMethodTable;
  }

  @Override
  public PyBaseException instantiate(Args args, PyContext ctx) {
    try {
      return (PyBaseException) pyConstructor.handle.invokeExact((IPyClass) this, args, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public Class<? extends IPyObj> instanceClass() {
    return pyClass;
  }

  @Override
  public boolean isSubclassable() {
    return true;
  }

  public PyBaseException instantiate(PxBaseException pxExc, PyTuple args, PyContext ctx) {
    PyBaseException pyExc;
    try {
      pyExc =
          (PyBaseException)
              pyConstructor.handle.invokeExact((IPyClass) this, Args.of(args.__array()), ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }

    pyExc.setPxExc(pxExc);

    if (pxExc.getCause() instanceof PxBaseException) {
      pyExc.setCause(((PxBaseException) pxExc.getCause()).pyExc, ctx);
    }

    return pyExc;
  }

  @Override
  protected IPyObj getDoc() {
    return doc != null ? PyStr.get(doc) : None;
  }
}
