package org.japy.kernel.types.exc.px;

import static org.japy.kernel.types.coll.seq.PyTuple.EMPTY_TUPLE;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;

public class PxGeneratorExit extends PxBaseException {
  public PxGeneratorExit(PyContext ctx) {
    super(null, PyBuiltinExc.GeneratorExit, EMPTY_TUPLE, ctx, null);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxGeneratorExit(PyBaseException pyExc) {
    super(pyExc);
  }

  @Override
  public synchronized Throwable fillInStackTrace() {
    return this;
  }
}
