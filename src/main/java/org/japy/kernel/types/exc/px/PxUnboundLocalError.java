package org.japy.kernel.types.exc.px;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;

public class PxUnboundLocalError extends PxNameError {
  public PxUnboundLocalError(String message, PyContext ctx) {
    super(message, PyBuiltinExc.UnboundLocalError, new PyTuple(PyStr.get(message)), ctx, null);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxUnboundLocalError(PyBaseException pyExc) {
    super(pyExc);
  }

  public static PxUnboundLocalError localNotDefined(String name, PyContext ctx) {
    throw new PxUnboundLocalError(
        String.format("local variable '%s' referenced before assignment", name), ctx);
  }
}
