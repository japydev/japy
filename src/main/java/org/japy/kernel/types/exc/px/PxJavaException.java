package org.japy.kernel.types.exc.px;

import static org.japy.kernel.types.coll.seq.PyTuple.EMPTY_TUPLE;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxJavaException extends PxException {
  public PxJavaException(Throwable cause, PyContext ctx) {
    super(
        cause.getMessage(),
        PyBuiltinExc.JavaException,
        cause.getMessage() != null ? new PyTuple(PyStr.get(cause.getMessage())) : EMPTY_TUPLE,
        ctx,
        cause);
  }

  protected PxJavaException(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxJavaException(PyBaseException pyExc) {
    super(pyExc);
  }
}
