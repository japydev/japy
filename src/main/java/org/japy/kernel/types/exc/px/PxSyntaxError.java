package org.japy.kernel.types.exc.px;

import static org.japy.kernel.types.coll.seq.PyTuple.EMPTY_TUPLE;

import java.util.function.Supplier;

import org.japy.compiler.SyntaxError;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.IHasSourceFile;
import org.japy.infra.loc.Location;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PySyntaxError;
import org.japy.kernel.types.num.PyInt;

public class PxSyntaxError extends PxException implements IHasSourceFile, IHasLocation {
  public PxSyntaxError(SyntaxError ex, PyContext ctx) {
    this((Throwable) ex, ctx);
    ((PySyntaxError) pyExc).setDetails(ex);
  }

  public PxSyntaxError(Throwable ex, PyContext ctx) {
    super(
        ex.getMessage(),
        PyBuiltinExc.SyntaxError,
        ex.getMessage() != null ? new PyTuple(PyStr.get(ex.getMessage())) : EMPTY_TUPLE,
        ctx,
        ex);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxSyntaxError(PyBaseException pyExc) {
    super(pyExc);
  }

  public static <T> T wrap(Supplier<T> func, PyContext ctx) {
    try {
      return func.get();
    } catch (SyntaxError ex) {
      throw new PxSyntaxError(ex, ctx);
    }
  }

  @Override
  public Location location() {
    IPyObj line = ((PySyntaxError) pyExc).lineno();
    IPyObj col = ((PySyntaxError) pyExc).offset();
    if (line instanceof PyInt && col instanceof PyInt) {
      return new Location(((PyInt) line).intValue(), ((PyInt) col).intValue());
    }
    return Location.UNKNOWN;
  }

  @Override
  public String sourceFile() {
    return ((PySyntaxError) pyExc).filename().toString();
  }
}
