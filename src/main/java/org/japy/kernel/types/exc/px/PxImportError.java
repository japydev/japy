package org.japy.kernel.types.exc.px;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxImportError extends PxException {
  public PxImportError(String message, PyContext ctx) {
    super(message, PyBuiltinExc.ImportError, ctx);
  }

  public PxImportError(PxBaseException ex, PyContext ctx) {
    super(ex.getMessage(), PyBuiltinExc.ImportError, ex.pyExc.args, ctx, ex);
  }

  protected PxImportError(String message, PyExcType type, PyContext ctx) {
    this(message, type, new PyTuple(PyStr.get(message)), ctx, null);
  }

  protected PxImportError(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxImportError(PyBaseException pyExc) {
    super(pyExc);
  }
}
