package org.japy.kernel.types.exc.px;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxAssertionError extends PxException {
  public PxAssertionError(@Nullable PyStr message, PyContext ctx) {
    super(
        message != null ? message.toString() : null,
        PyBuiltinExc.AssertionError,
        message != null ? new PyTuple(message) : PyTuple.EMPTY_TUPLE,
        ctx,
        null);
  }

  protected PxAssertionError(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxAssertionError(PyBaseException pyExc) {
    super(pyExc);
  }
}
