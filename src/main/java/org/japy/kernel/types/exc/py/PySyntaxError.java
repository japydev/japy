package org.japy.kernel.types.exc.py;

import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.util.PyUtil.wrap;

import org.japy.compiler.SyntaxError;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.util.Args;

public class PySyntaxError extends PyExc {
  private static final PyTuple NO_DETAILS = PyTuple.of(None, None, None, None, None, None);

  private PyTuple details = NO_DETAILS;

  @Constructor
  @SubClassConstructor
  public PySyntaxError(IPyClass cls, Args args, PyContext ctx) {
    super(cls, args, ctx);
  }

  public void setDetails(SyntaxError ex) {
    // TODO full location, text, message
    details =
        PyTuple.of(
            PyStr.get(ex.sourceFile()),
            wrap(ex.location().line),
            wrap(ex.location().col),
            None,
            wrap(ex.location().line),
            wrap(ex.location().col));
  }

  public IPyObj filename() {
    return details.get(0);
  }

  public IPyObj lineno() {
    return details.get(1);
  }

  public IPyObj offset() {
    return details.get(2);
  }

  @InstanceAttribute
  public IPyObj filename(PyContext ctx) {
    return filename();
  }

  @InstanceAttribute
  public IPyObj lineno(PyContext ctx) {
    return lineno();
  }

  @InstanceAttribute
  public IPyObj offset(PyContext ctx) {
    return offset();
  }

  @InstanceAttribute
  public IPyObj text(PyContext ctx) {
    return details.get(3);
  }

  @InstanceAttribute
  public IPyObj end_lineno(PyContext ctx) {
    return details.get(4);
  }

  @InstanceAttribute
  public IPyObj end_offset(PyContext ctx) {
    return details.get(5);
  }
}
