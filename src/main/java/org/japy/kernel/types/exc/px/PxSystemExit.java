package org.japy.kernel.types.exc.px;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;

public class PxSystemExit extends PxBaseException {
  public PxSystemExit(IPyObj status, PyContext ctx) {
    super(null, PyBuiltinExc.SystemExit, new PyTuple(status), ctx, null);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxSystemExit(PyBaseException pyExc) {
    super(pyExc);
  }
}
