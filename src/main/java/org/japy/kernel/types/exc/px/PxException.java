package org.japy.kernel.types.exc.px;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxException extends PxBaseException {
  public PxException(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  protected PxException(String message, PyExcType type, PyContext ctx) {
    this(message, type, new PyTuple(PyStr.get(message)), ctx, null);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxException(PyBaseException pyExc) {
    super(pyExc);
  }

  public static PxException valueError(String message, PyContext ctx) {
    return new PxException(message, PyBuiltinExc.ValueError, ctx);
  }

  public static PxException typeError(String message, PyContext ctx) {
    return new PxException(message, PyBuiltinExc.TypeError, ctx);
  }
}
