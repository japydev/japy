package org.japy.kernel.types.exc.px;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxWarning extends PxException {
  public PxWarning(String message, PyExcType type, PyContext ctx) {
    this(message, type, new PyTuple(PyStr.get(message)), ctx, null);
  }

  protected PxWarning(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxWarning(PyBaseException pyExc) {
    super(pyExc);
  }
}
