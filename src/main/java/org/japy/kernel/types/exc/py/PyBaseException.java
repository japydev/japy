package org.japy.kernel.types.exc.py;

import static org.japy.infra.util.ObjUtil.or;
import static org.japy.kernel.types.exc.px.PxException.typeError;
import static org.japy.kernel.types.misc.PyNone.None;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceAttributeSetter;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.coll.str.PyStrBuilder;
import org.japy.kernel.types.exc.px.PxBaseException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.types.obj.proto.IPyObjWithStr;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

public class PyBaseException implements IPyTrueAtomObj, IPyObjWithStr, IPyObjWithRepr {
  private final IPyClass type;
  public final PyTuple args;
  private @Nullable PxBaseException pxExc;

  private @Nullable PyBaseException cause;
  private @Nullable PyBaseException context;
  private boolean suppressContext;

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(1).add("args", ParamKind.VAR_POS).build();

  @Constructor
  @SubClassConstructor
  public PyBaseException(IPyClass cls, Args args, PyContext ctx) {
    this.type = cls;
    this.args = (PyTuple) ARG_PARSER.parse1(args, cls.name(), ctx);
  }

  public static PyBaseException parse(
      String func, IPyObj typ, @Nullable IPyObj val, @Nullable IPyObj tb, PyContext ctx) {
    // TODO traceback
    if (val == null) {
      if (typ instanceof PyBaseException) {
        return (PyBaseException) typ;
      }
      IPyClass cls = ArgLib.checkArgClass(func, "typ", PyBuiltinExc.BaseException, typ, ctx);
      IPyObj ex = FuncLib.call(cls, ctx);
      if (ex instanceof PyBaseException) {
        return (PyBaseException) ex;
      }
      throw typeError(
          String.format(
              "%s() should have returned an instance of BaseException, not '%s'",
              cls.name(), ex.typeName()),
          ctx);
    }

    IPyClass cls = ArgLib.checkArgClass(func, "typ", PyBuiltinExc.BaseException, typ, ctx);
    ArgLib.checkArgType(func, "val", cls, val, ctx);
    return (PyBaseException) val;
  }

  @Override
  public IPyClass type() {
    return type;
  }

  @InstanceAttribute(Constants.__CAUSE__)
  IPyObj getCause(PyContext ctx) {
    return or(cause, None);
  }

  private static @Nullable PyBaseException checkAttrExcOrNone(
      String attr, IPyObj value, PyContext ctx) {
    if (value != None && !(value instanceof PyBaseException)) {
      throw typeError(
          String.format("%s must be set to None or an exception, got '%s'", attr, value.typeName()),
          ctx);
    }
    return value != None ? (PyBaseException) value : null;
  }

  @InstanceAttributeSetter(Constants.__CAUSE__)
  void setCause(IPyObj cause, PyContext ctx) {
    this.cause = checkAttrExcOrNone("__cause__", cause, ctx);
    suppressContext = true;

    if (pxExc != null && cause instanceof PyBaseException) {
      pxExc.addSuppressed(((PyBaseException) cause).pxExc());
    }
  }

  @InstanceAttribute(Constants.__CONTEXT__)
  IPyObj getContext(PyContext ctx) {
    return or(context, None);
  }

  @InstanceAttributeSetter(Constants.__CONTEXT__)
  void setContext(IPyObj context, PyContext ctx) {
    this.context = checkAttrExcOrNone("__context__", context, ctx);

    if (pxExc != null && context instanceof PyBaseException) {
      pxExc.addSuppressed(((PyBaseException) context).pxExc());
    }
  }

  @InstanceAttribute(Constants.__SUPPRESS_CONTEXT__)
  IPyObj getSuppressContext(PyContext ctx) {
    return PyBool.of(suppressContext);
  }

  @InstanceAttributeSetter(Constants.__SUPPRESS_CONTEXT__)
  void setSuppressContext(IPyObj suppressContext, PyContext ctx) {
    this.suppressContext = ObjLib.isTrue(suppressContext, ctx);
  }

  public @Nullable PyBaseException cause() {
    return cause;
  }

  public @Nullable PxBaseException pxExcRaw() {
    return pxExc;
  }

  public PxBaseException pxExc() {
    if (pxExc == null) {
      pxExc = PyExcType.makeJavaException(this);
    }
    return pxExc;
  }

  public void setPxExc(PxBaseException pxExc) {
    this.pxExc = pxExc;
  }

  @Override
  public PyStr str(PyContext ctx) {
    if (args.len() == 1) {
      return PrintLib.str(args.get(0), ctx);
    } else {
      return PrintLib.str(args, ctx);
    }
  }

  @Override
  public PyStr repr(PyContext ctx) {
    PyStrBuilder sb = new PyStrBuilder();
    sb.appendUnknown(type.name());
    sb.appendRepr(args, ctx);
    return sb.build();
  }

  @InstanceAttribute
  public IPyObj args(PyContext ctx) {
    return args;
  }

  @InstanceMethod
  public IPyObj with_traceback(@Param("tb") IPyObj tb, PyContext ctx) {
    // TODO
    return this;
  }

  @Override
  public void validate(Validator v) {
    v.validate(args);
    v.validateIfNotNull(cause);
  }
}
