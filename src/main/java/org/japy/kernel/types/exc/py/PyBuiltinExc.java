package org.japy.kernel.types.exc.py;

import org.japy.kernel.types.exc.px.*;

@SuppressWarnings("unused")
public class PyBuiltinExc {
  // spotless:off
  public static final PyExcType BaseException = new PyExcType();
  public static final PyExcType GeneratorExit = new PyExcType("GeneratorExit", BaseException, PyGeneratorExit.class, PxGeneratorExit.class);
  public static final PyExcType KeyboardInterrupt = new PyExcType("KeyboardInterrupt", BaseException, PxKeyboardInterrupt.class);
  public static final PyExcType SystemExit = new PyExcType("SystemExit", BaseException, PySystemExit.class, PxSystemExit.class);
  public static final PyExcType Exception = new PyExcType("Exception", BaseException, PxException.class);
  public static final PyExcType JavaException = new PyExcType("JavaException", Exception, PxJavaException.class);
  public static final PyExcType MemoryError = new PyExcType("MemoryError", Exception);
  public static final PyExcType TypeError = new PyExcType("TypeError", Exception);
  public static final PyExcType ValueError = new PyExcType("ValueError", Exception);
  public static final PyExcType AttributeError = new PyExcType("AttributeError", Exception, PxAttributeError.class);
  public static final PyExcType NameError = new PyExcType("NameError", Exception, PxNameError.class);
  public static final PyExcType UnboundLocalError = new PyExcType("UnboundLocalError", NameError, PxUnboundLocalError.class);
  public static final PyExcType StopIteration = new PyExcType("StopIteration", Exception, PyStopIteration.class, PxStopIteration.class);
  public static final PyExcType StopAsyncIteration = new PyExcType("StopAsyncIteration", Exception, PxStopAsyncIteration.class);
  public static final PyExcType ArithmeticError = new PyExcType("ArithmeticError", Exception, PxArithmeticError.class);
  public static final PyExcType OverflowError = new PyExcType("OverflowError", ArithmeticError, PxOverflowError.class);
  public static final PyExcType ZeroDivisionError = new PyExcType("ZeroDivisionError", ArithmeticError, PxZeroDivisionError.class);
  public static final PyExcType SyntaxError = new PyExcType("SyntaxError", Exception, PySyntaxError.class, PxSyntaxError.class);
  public static final PyExcType IndentationError = new PyExcType("IndentationError", SyntaxError);
  public static final PyExcType TabError = new PyExcType("TabError", IndentationError);
  public static final PyExcType LookupError = new PyExcType("LookupError", Exception, PxLookupError.class);
  public static final PyExcType IndexError = new PyExcType("IndexError", LookupError, PxIndexError.class);
  public static final PyExcType KeyError = new PyExcType("KeyError", LookupError, PxKeyError.class);
  public static final PyExcType AssertionError = new PyExcType("AssertionError", Exception, PxAssertionError.class);
  public static final PyExcType RuntimeError = new PyExcType("RuntimeError", Exception, PxRuntimeError.class);
  public static final PyExcType RecursionError = new PyExcType("RecursionError", RuntimeError);
  public static final PyExcType NotImplementedError = new PyExcType("NotImplementedError", RuntimeError);
  public static final PyExcType ImportError = new PyExcType("ImportError", Exception, PxImportError.class);
  public static final PyExcType ModuleNotFoundError = new PyExcType("ModuleNotFoundError", ImportError, PxModuleNotFoundError.class);
  public static final PyExcType OSError = new PyExcType("OSError", Exception, PyOSError.class, PxOSError.class);
  public static final PyExcType BlockingIOError = new PyExcType("BlockingIOError", OSError, PxBlockingIOError.class);
  public static final PyExcType FileNotFoundError = new PyExcType("FileNotFoundError", OSError);
  public static final PyExcType FileExistsError = new PyExcType("FileExistsError", OSError);
  public static final PyExcType InterruptedError = new PyExcType("InterruptedError", OSError);
  public static final PyExcType IsADirectoryError = new PyExcType("IsADirectoryError", OSError);
  public static final PyExcType NotADirectoryError = new PyExcType("NotADirectoryError", OSError);
  public static final PyExcType PermissionError = new PyExcType("PermissionError", OSError);
  public static final PyExcType ProcessLookupError = new PyExcType("ProcessLookupError", OSError);
  public static final PyExcType TimeoutError = new PyExcType("TimeoutError", OSError);
  public static final PyExcType ChildProcessError = new PyExcType("ChildProcessError", OSError);
  public static final PyExcType ConnectionError = new PyExcType("ConnectionError", OSError);
  public static final PyExcType BrokenPipeError = new PyExcType("BrokenPipeError", ConnectionError);
  public static final PyExcType ConnectionAbortedError = new PyExcType("ConnectionAbortedError", ConnectionError);
  public static final PyExcType ConnectionRefusedError = new PyExcType("ConnectionRefusedError", ConnectionError);
  public static final PyExcType ConnectionResetError = new PyExcType("ConnectionResetError", ConnectionError);
  public static final PyExcType SystemError = new PyExcType("SystemError", Exception);
  public static final PyExcType EOFError = new PyExcType("EOFError", Exception);
  // TODO zzz
  public static final PyExcType UnicodeError = new PyExcType("UnicodeError", ValueError);
  public static final PyExcType UnicodeEncodeError = new PyExcType("UnicodeEncodeError", UnicodeError);
  public static final PyExcType UnicodeDecodeError = new PyExcType("UnicodeDecodeError", UnicodeError);
  public static final PyExcType UnicodeTranslateError = new PyExcType("UnicodeTranslateError", UnicodeError);

  public static final PyExcType Warning = new PyExcType("Warning", Exception, PxWarning.class);
  public static final PyExcType UserWarning = new PyExcType("UserWarning", Warning);
  public static final PyExcType DeprecationWarning = new PyExcType("DeprecationWarning", Warning);
  public static final PyExcType PendingDeprecationWarning = new PyExcType("PendingDeprecationWarning", Warning);
  public static final PyExcType SyntaxWarning = new PyExcType("SyntaxWarning", Warning);
  public static final PyExcType RuntimeWarning = new PyExcType("RuntimeWarning", Warning);
  public static final PyExcType FutureWarning = new PyExcType("FutureWarning", Warning);
  public static final PyExcType ImportWarning = new PyExcType("ImportWarning", Warning);
  public static final PyExcType UnicodeWarning = new PyExcType("UnicodeWarning", Warning);
  public static final PyExcType BytesWarning = new PyExcType("BytesWarning", Warning);
  public static final PyExcType ResourceWarning = new PyExcType("ResourceWarning", Warning);
  // spotless:on
}
