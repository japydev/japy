package org.japy.kernel.types.exc.px;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxAttributeError extends PxException {
  public PxAttributeError(String message, PyContext ctx) {
    super(message, PyBuiltinExc.AttributeError, ctx);
  }

  public PxAttributeError(String attrName, IPyObj obj, boolean isClass, PyContext ctx) {
    super(
        isClass
            ? String.format(
                "class '%s' does not have '%s' attribute", ((IPyClass) obj).name(), attrName)
            : String.format(
                "instances of type '%s' do not have '%s' attribute",
                obj instanceof IPyClass ? ((IPyClass) obj).name() : obj.typeName(), attrName),
        PyBuiltinExc.AttributeError,
        ctx);
  }

  public PxAttributeError(PyStr message, PyContext ctx) {
    super(message.toString(), PyBuiltinExc.AttributeError, new PyTuple(message), ctx, null);
  }

  public static PxAttributeError readOnlyAttribute(String attr, IPyObj obj, PyContext ctx) {
    return new PxAttributeError(
        String.format("attribute '%s' of '%s' objects is not writable", attr, obj.typeName()), ctx);
  }

  public static PxAttributeError nonDeletableAttribute(String attr, IPyObj obj, PyContext ctx) {
    return new PxAttributeError(
        String.format("attribute '%s' of '%s' objects cannot be deleted", attr, obj.typeName()),
        ctx);
  }

  protected PxAttributeError(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxAttributeError(PyBaseException pyExc) {
    super(pyExc);
  }
}
