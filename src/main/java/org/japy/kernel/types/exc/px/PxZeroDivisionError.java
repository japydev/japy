package org.japy.kernel.types.exc.px;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxZeroDivisionError extends PxArithmeticError {
  public PxZeroDivisionError(PyContext ctx) {
    this("division by zero", ctx);
  }

  public PxZeroDivisionError(String message, PyContext ctx) {
    super(message, PyBuiltinExc.ZeroDivisionError, new PyTuple(PyStr.get(message)), ctx, null);
  }

  protected PxZeroDivisionError(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxZeroDivisionError(PyBaseException pyExc) {
    super(pyExc);
  }
}
