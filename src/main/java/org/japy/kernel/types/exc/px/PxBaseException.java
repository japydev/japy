package org.japy.kernel.types.exc.px;

import static org.japy.infra.validation.Debug.dcheck;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxBaseException extends RuntimeException {
  public final PyBaseException pyExc;

  /**
   * Constructor to be used by subclasses. Constructors to be used in regular code must not take
   * PyExcType as a parameter.
   */
  protected PxBaseException(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, cause);
    pyExc = pyExcType.instantiate(this, args, ctx);
  }

  /**
   * Constructor to be used by subclasses. Constructors to be used in regular code must not take
   * PyExcType as a parameter.
   */
  @Deprecated
  protected PxBaseException(PyExcType pyExcType, PyTuple args, PyContext ctx) {
    this(getMessage(args), pyExcType, args, ctx, null);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxBaseException(PyBaseException pyExc) {
    super(getMessage(pyExc.args), getCause(pyExc));
    dcheck(pyExc.pxExcRaw() == null);
    this.pyExc = pyExc;
  }

  protected static @Nullable String getMessage(PyTuple args) {
    if (args.isEmpty() || !(args.get(0) instanceof PyStr)) {
      return null;
    }
    return args.get(0).toString();
  }

  protected static @Nullable Throwable getCause(PyBaseException exc) {
    @Nullable PyBaseException cause = exc.cause();
    return cause != null ? cause.pxExc() : null;
  }

  public boolean is(PyExcType type) {
    return pyExc.isInstanceRaw(type);
  }
}
