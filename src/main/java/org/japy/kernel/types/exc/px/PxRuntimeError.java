package org.japy.kernel.types.exc.px;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;

public class PxRuntimeError extends PxException {
  public PxRuntimeError(String message, PyContext ctx) {
    super(message, PyBuiltinExc.RuntimeError, ctx);
  }

  public PxRuntimeError(String message, PyContext ctx, Throwable cause) {
    super(message, PyBuiltinExc.RuntimeError, new PyTuple(PyStr.get(message)), ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxRuntimeError(PyBaseException pyExc) {
    super(pyExc);
  }
}
