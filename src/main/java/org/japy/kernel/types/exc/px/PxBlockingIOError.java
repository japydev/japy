package org.japy.kernel.types.exc.px;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxBlockingIOError extends PxOSError {
  public PxBlockingIOError(PxBaseException ex, PyContext ctx) {
    super(ex.getMessage(), PyBuiltinExc.BlockingIOError, ex.pyExc.args, ctx, ex);
  }

  protected PxBlockingIOError(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxBlockingIOError(PyBaseException pyExc) {
    super(pyExc);
  }
}
