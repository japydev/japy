package org.japy.kernel.types.exc.px;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;

public class PxModuleNotFoundError extends PxImportError {
  public PxModuleNotFoundError(String moduleName, PyContext ctx) {
    super(
        String.format("module '%s' not found", moduleName), PyBuiltinExc.ModuleNotFoundError, ctx);
  }

  public PxModuleNotFoundError(PyStr moduleName, PyContext ctx) {
    super(
        String.format("module '%s' not found", moduleName), PyBuiltinExc.ModuleNotFoundError, ctx);
  }

  protected PxModuleNotFoundError(
      @Nullable String message,
      PyExcType pyExcType,
      PyTuple args,
      PyContext ctx,
      @Nullable Throwable cause) {
    super(message, pyExcType, args, ctx, cause);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxModuleNotFoundError(PyBaseException pyExc) {
    super(pyExc);
  }
}
