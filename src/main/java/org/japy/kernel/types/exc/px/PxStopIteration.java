package org.japy.kernel.types.exc.px;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.coll.seq.PyTuple.EMPTY_TUPLE;

import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyStopIteration;

public class PxStopIteration extends PxException {
  public PxStopIteration(PyContext ctx) {
    super(null, PyBuiltinExc.StopIteration, EMPTY_TUPLE, ctx, null);
    if (Debug.ENABLED) {
      dcheck(pyExc instanceof PyStopIteration);
    }
  }

  public PxStopIteration(IPyObj value, PyContext ctx) {
    super(null, PyBuiltinExc.StopIteration, new PyTuple(value), ctx, null);
    if (Debug.ENABLED) {
      dcheck(pyExc instanceof PyStopIteration);
    }
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxStopIteration(PyBaseException pyExc) {
    super(pyExc);
    if (Debug.ENABLED) {
      dcheck(pyExc instanceof PyStopIteration);
    }
  }

  public IPyObj value() {
    return ((PyStopIteration) pyExc).value();
  }

  @Override
  public synchronized Throwable fillInStackTrace() {
    return this;
  }
}
