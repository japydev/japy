package org.japy.kernel.types.exc.py;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.util.Args;

public class PyGeneratorExit extends PyExc {
  @Constructor
  @SubClassConstructor
  public PyGeneratorExit(IPyClass cls, Args args, PyContext ctx) {
    super(cls, args, ctx);
  }

  public PyGeneratorExit(PyContext ctx) {
    super(PyBuiltinExc.GeneratorExit, Args.EMPTY, ctx);
  }
}
