package org.japy.kernel.types.exc.py;

import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.util.Args;

public class PyStopIteration extends PyExc {
  @Constructor
  @SubClassConstructor
  public PyStopIteration(IPyClass cls, Args args, PyContext ctx) {
    super(cls, args, ctx);
  }

  public IPyObj value() {
    return args.isEmpty() ? None : args.get(0);
  }

  @InstanceAttribute
  public IPyObj value(PyContext ctx) {
    return value();
  }
}
