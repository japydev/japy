package org.japy.kernel.types.exc.py;

import static org.japy.kernel.types.misc.PyNone.None;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.exc.px.PxBaseException;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxJavaException;
import org.japy.kernel.types.exc.px.PxRuntimeError;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.util.Args;

public class ExcLib {
  public static PxBaseException raise(IPyObj ex, @Nullable IPyObj from, PyContext ctx) {
    if (from != null && from != None && !(from instanceof PyBaseException)) {
      throw PxException.typeError(
          String.format("'from' must be an exception or None, got '%s'", from.typeName()), ctx);
    }

    PyBaseException pex;
    if (ex instanceof IPyClass) {
      if (!((IPyClass) ex).isSubClassRaw(PyBuiltinExc.BaseException)) {
        throw PxException.typeError(
            "expected an exception class, got " + PrintLib.repr(ex, ctx), ctx);
      }
      pex = (PyBaseException) FuncLib.call(ex, Args.EMPTY, ctx);
    } else if (!(ex instanceof PyBaseException)) {
      throw PxException.typeError("expected an exception, got " + PrintLib.repr(ex, ctx), ctx);
    } else {
      pex = (PyBaseException) ex;
    }

    if (from != null) {
      pex.setCause(from, ctx);
    }

    throw pex.pxExc();
  }

  public static PxBaseException raise(IPyObj ex, PyContext ctx) {
    throw raise(ex, null, ctx);
  }

  public static PxBaseException raise(PyContext ctx) {
    @Nullable PyBaseException exc = ctx.thread.currentException();
    if (exc != null) {
      throw exc.pxExc();
    }

    throw new PxRuntimeError("no exception is being handled", ctx);
  }

  public static PyBaseException captureExc(Throwable ex, PyContext ctx) {
    if (ex instanceof PxBaseException) {
      return ((PxBaseException) ex).pyExc;
    }
    if (ex instanceof OutOfMemoryError) {
      return new PxException(
              ex.getMessage(), PyBuiltinExc.MemoryError, PyTuple.EMPTY_TUPLE, ctx, ex)
          .pyExc;
    }
    PxJavaException jex = new PxJavaException(ex, ctx);
    return jex.pyExc;
  }

  @CanIgnoreReturnValue
  public static @Nullable PyBaseException setCurrentExc(
      @Nullable PyBaseException ex, PyContext ctx) {
    return ctx.thread.setCurrentException(ex);
  }
}
