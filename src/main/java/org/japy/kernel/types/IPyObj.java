package org.japy.kernel.types;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.coll.ctx.CtxAwareObject;
import org.japy.infra.validation.ISupportsValidation;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.obj.IPyInstanceDict;
import org.japy.kernel.types.obj.ObjLib;

public interface IPyObj extends ISupportsValidation, CtxAwareObject<PyContext> {
  IPyObj[] EMPTY_OBJECT_ARRAY = new IPyObj[0];

  IPyClass type();

  default boolean isBuiltin() {
    return type().isBuiltinClass();
  }

  default @Nullable FuncHandle specialMethodOrNone(SpecialMethod m) {
    return type().methodTable().getRaw(m);
  }

  default @Nullable FuncHandle specialMethod(SpecialMethod m) {
    return type().methodTable().getIfPresent(m);
  }

  @Override
  default boolean isEqual(Object other, PyContext ctx) {
    return ObjLib.isEqual(this, (IPyObj) other, ctx);
  }

  @Override
  default int hashCode(PyContext ctx) {
    return ObjLib.hashCode(this, ctx);
  }

  // force implementors to decide on hashing
  @SuppressWarnings("unused")
  int __hash_implemented__();

  default String typeName() {
    return type().name();
  }

  default @Nullable IPyInstanceDict instanceDict() {
    return null;
  }

  default boolean isInstanceRaw(IPyClass cls) {
    return type().isSubClassRaw(cls);
  }
}
