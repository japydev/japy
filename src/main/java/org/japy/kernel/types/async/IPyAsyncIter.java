package org.japy.kernel.types.async;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

/** Asynchronous iterator - object with an async __anext__ method */
public interface IPyAsyncIter extends IPyTrueAtomObj, IPyAsyncIterable {
  @SpecialInstanceMethod("__anext__")
  IPyObj anext(PyContext ctx);

  @Override
  default IPyAsyncIter aiter(PyContext ctx) {
    return this;
  }
}
