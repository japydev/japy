package org.japy.kernel.types.async;

import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.IPyNoValidationObj;

public abstract class PyAsyncGenerator implements IPyAsyncGenerator, IPyNoValidationObj {
  public static final IPyClass TYPE =
      new PyBuiltinClass("asyncgenerator", PyAsyncGenerator.class, PyBuiltinClass.INTERNAL);

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
