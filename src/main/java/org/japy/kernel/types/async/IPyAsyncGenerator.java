package org.japy.kernel.types.async;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;

public interface IPyAsyncGenerator extends IPyAsyncIter {
  @InstanceMethod
  default IPyObj __anext__(PyContext ctx) {
    return asend(None, ctx);
  }

  @InstanceMethod
  IPyObj asend(IPyObj value, PyContext ctx);

  @InstanceMethod
  IPyObj athrow(
      @Param("typ") IPyObj typ,
      @Param(value = "val", dflt = DEFAULT_SENTINEL) IPyObj val,
      @Param(value = "tb", dflt = DEFAULT_SENTINEL) IPyObj tb,
      PyContext ctx);

  @InstanceMethod
  IPyObj aclose(PyContext ctx);
}
