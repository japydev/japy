package org.japy.kernel.types.async;

import org.japy.kernel.types.IPyObj;

public interface IPyAwaitable extends IPyObj {}
