package org.japy.kernel.types.async;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;

/** Asynchronous iterable - object with __aiter__ method which returns an asyncronous iterator */
public interface IPyAsyncIterable extends IPyObj {
  IPyAsyncIter aiter(PyContext ctx);

  @SpecialInstanceMethod
  default IPyObj __aiter__(PyContext ctx) {
    return aiter(ctx);
  }
}
