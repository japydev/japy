package org.japy.kernel.types.coll.set;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.util.PyUtil.wrap;

import org.japy.base.ParamKind;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.coll.IPyContainerObj;
import org.japy.kernel.types.coll.iter.IPyIterable;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.obj.proto.IPyObjWithEq;
import org.japy.kernel.types.obj.proto.IPyObjWithLen;

public interface IPyROSet extends IPyObjWithLen, IPyIterable, IPyContainerObj, IPyObjWithEq {
  boolean isSubSet(IPyObj coll, PyContext ctx);

  boolean isSuperSet(IPyObj coll, PyContext ctx);

  boolean isDisjoint(IPyObj coll, PyContext ctx);

  IPyObj union(IPyObj[] others, PyContext ctx);

  IPyObj intersection(IPyObj[] others, PyContext ctx);

  IPyObj difference(IPyObj[] others, PyContext ctx);

  IPyObj symmetricDifference(IPyObj other, PyContext ctx);

  @ProtocolMethod
  @InstanceMethod
  IPyObj copy(PyContext ctx);

  @ProtocolMethod
  @SpecialInstanceMethod
  default IPyObj __le__(@Param("other") IPyObj other, PyContext ctx) {
    if (other instanceof IPyROSet) {
      return wrap(isSubSet(other, ctx));
    }
    return NotImplemented;
  }

  @ProtocolMethod
  @SpecialInstanceMethod
  default IPyObj __lt__(@Param("other") IPyObj other, PyContext ctx) {
    if (!(other instanceof IPyROSet)) {
      return NotImplemented;
    }
    return wrap(isSubSet(other, ctx) && len(ctx) != ((IPyROSet) other).len(ctx));
  }

  @ProtocolMethod
  @SpecialInstanceMethod
  default IPyObj __ge__(@Param("other") IPyObj other, PyContext ctx) {
    if (other instanceof IPyROSet) {
      return wrap(isSuperSet(other, ctx));
    }
    return NotImplemented;
  }

  @ProtocolMethod
  @SpecialInstanceMethod
  default IPyObj __gt__(@Param("other") IPyObj other, PyContext ctx) {
    if (!(other instanceof IPyROSet)) {
      return NotImplemented;
    }
    return wrap(isSuperSet(other, ctx) && len(ctx) != ((IPyROSet) other).len(ctx));
  }

  @ProtocolMethod
  @SpecialInstanceMethod
  default IPyObj __and__(@Param("other") IPyObj other, PyContext ctx) {
    return intersection(new IPyObj[] {other}, ctx);
  }

  @ProtocolMethod
  @SpecialInstanceMethod
  default IPyObj __or__(@Param("other") IPyObj other, PyContext ctx) {
    return union(new IPyObj[] {other}, ctx);
  }

  @ProtocolMethod
  @SpecialInstanceMethod
  default IPyObj __xor__(@Param("other") IPyObj other, PyContext ctx) {
    return symmetricDifference(other, ctx);
  }

  @ProtocolMethod
  @SpecialInstanceMethod
  default IPyObj __sub__(@Param("other") IPyObj other, PyContext ctx) {
    return difference(new IPyObj[] {other}, ctx);
  }

  @ProtocolMethod
  @InstanceMethod("issubset")
  default IPyObj __issubset(@Param("other") IPyObj other, PyContext ctx) {
    return wrap(isSubSet(other, ctx));
  }

  @ProtocolMethod
  @InstanceMethod("issuperset")
  default IPyObj __issuperset(@Param("other") IPyObj other, PyContext ctx) {
    return wrap(isSuperSet(other, ctx));
  }

  @ProtocolMethod
  @InstanceMethod("isdisjoint")
  default IPyObj __isdisjoint(@Param("other") IPyObj other, PyContext ctx) {
    return wrap(isDisjoint(other, ctx));
  }

  @ProtocolMethod
  @InstanceMethod("union")
  default IPyObj __union(
      @Param(value = "others", kind = ParamKind.VAR_POS) IPyObj others, PyContext ctx) {
    return union(((PyTuple) others).__array(), ctx);
  }

  @ProtocolMethod
  @InstanceMethod("intersection")
  default IPyObj __intersection(
      @Param(value = "others", kind = ParamKind.VAR_POS) IPyObj others, PyContext ctx) {
    return intersection(((PyTuple) others).__array(), ctx);
  }

  @ProtocolMethod
  @InstanceMethod("difference")
  default IPyObj __difference(
      @Param(value = "others", kind = ParamKind.VAR_POS) IPyObj others, PyContext ctx) {
    return difference(((PyTuple) others).__array(), ctx);
  }

  @ProtocolMethod
  @InstanceMethod("symmetric_difference")
  default IPyObj __symmetric_difference(@Param(value = "other") IPyObj other, PyContext ctx) {
    return symmetricDifference(other, ctx);
  }
}
