package org.japy.kernel.types.coll.set;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.coll.ctx.CtxAwareHashSet;
import org.japy.infra.coll.ctx.CtxAwareSet;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.str.DeepRepr;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyFrozenSet extends PyAbstractSet implements IPyROSet, IPyIntHashableObj {
  public static final IPyClass TYPE =
      new PyBuiltinClass("frozenset", PyFrozenSet.class, PyBuiltinClass.REGULAR);

  public static final PyFrozenSet EMPTY_SET = new PyFrozenSet();

  public PyFrozenSet() {}

  private PyFrozenSet(CtxAwareSet<IPyObj, PyContext> content) {
    super(content);
  }

  @Override
  protected PyAbstractSet instantiate(CtxAwareSet<IPyObj, PyContext> content) {
    return new PyFrozenSet(content);
  }

  public PyFrozenSet(IPyObj iterable, PyContext ctx) {
    this(setFromIterable(iterable, ctx));
  }

  @SubClassConstructor
  public PyFrozenSet(Args args, PyContext ctx) {
    ArgParser.verifyNoKwArgs(args, "frozenset", ctx);
    if (args.posCount() > 1) {
      throw PxException.typeError("frozenset() takes zero or one parameters", ctx);
    }
    if (args.posCount() == 0) {
      return;
    }

    for (IPyObj obj : new JavaIterable(args.getPos(0), ctx)) {
      content.add(obj, ctx);
    }
  }

  @Constructor
  public static IPyObj construct(Args args, PyContext ctx) {
    ArgParser.verifyNoKwArgs(args, "frozenset", ctx);
    if (args.posCount() > 1) {
      throw PxException.typeError("frozenset() takes zero or one parameters", ctx);
    }
    if (args.posCount() == 0) {
      return EMPTY_SET;
    }

    IPyObj arg = args.getPos(0);
    if (arg.type() == TYPE) {
      return arg;
    }

    @Var
    @Nullable
    CtxAwareSet<IPyObj, PyContext> content = null;

    for (IPyObj obj : new JavaIterable(arg, ctx)) {
      if (content == null) {
        content = new CtxAwareHashSet<>();
      }
      content.add(obj, ctx);
    }

    return content != null ? new PyFrozenSet(content) : EMPTY_SET;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public int intHashCode(PyContext ctx) {
    return content.hashCode(ctx);
  }

  @Override
  public void print(DeepRepr dr, PyContext ctx) {
    PrintLib.printFrozenSet(this, dr, ctx);
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    if (type() == TYPE) {
      return this;
    } else {
      return new PyFrozenSet(content);
    }
  }
}
