package org.japy.kernel.types.coll.str;

public interface CharSource {
  enum Kind {
    BYTES,
    UCS2,
    UTF32,
  }

  Kind kind();

  int len();

  int getChar(int index);
}
