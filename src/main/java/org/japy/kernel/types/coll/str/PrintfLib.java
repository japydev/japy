package org.japy.kernel.types.coll.str;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.num.PyBool.True;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.eclipse.collections.impl.list.mutable.primitive.ByteArrayList;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.num.PyInt;

class PrintfLib {
  static IPyObj printf(PyStr s, IPyObj values, PyContext ctx) {
    if (s.isUcs2()) {
      return printfUcs2(s.s(), values, ctx);
    } else {
      return printfUtf32(s.u(), values, ctx);
    }
  }

  static IPyObj printf(PyBytes s, IPyObj values, PyContext ctx) {
    Parser p = new Parser(new BytesCharSource(s.content), ctx);
    p.parse();
    return new BytesFormatter(p, values, ctx).format();
  }

  static IPyObj printf(PyByteArray s, IPyObj values, PyContext ctx) {
    Parser p = new Parser(new ByteArrayCharSource(s.content), ctx);
    p.parse();
    return new BytesFormatter(p, values, ctx).format();
  }

  private static IPyObj printfUtf32(Utf32String s, IPyObj values, PyContext ctx) {
    Parser p = new Parser(new Utf32CharSource(s.content), ctx);
    p.parse();
    return new Utf32Formatter(p, values, ctx).format();
  }

  private static IPyObj printfUcs2(String s, IPyObj values, PyContext ctx) {
    Parser p = new Parser(new Ucs2CharSource(s), ctx);
    p.parse();
    return new Ucs2Formatter(p, values, ctx).format();
  }

  private static IPyObj getArg(IPyObj values, int index, int positionalCount) {
    if (positionalCount == 1) {
      dcheck(index == 0);
      return values;
    } else {
      return ((PyTuple) values).get(index);
    }
  }

  private static IPyObj getArg(IPyObj values, String name, PyContext ctx) {
    return CollLib.getItem(values, PyStr.get(name), ctx);
  }

  private abstract static class Formatter {
    protected final List<Fragment> fragments;
    protected final int positionalCount;
    protected final IPyObj values;
    protected final PyContext ctx;

    protected Formatter(Parser parser, IPyObj values, PyContext ctx) {
      this.fragments = parser.fragments;
      this.positionalCount = parser.positionalCount;
      this.values = values;
      this.ctx = ctx;
      validate();
    }

    final IPyObj format() {
      fragments.forEach(this::formatFragment);
      return getResult();
    }

    protected abstract IPyObj getResult();

    protected abstract void formatLiteral(LiteralFragment fragment);

    protected abstract void formatObj(IPyObj obj, Conversion conversion);

    private void formatFragment(Fragment fragment) {
      if (fragment instanceof LiteralFragment) {
        formatLiteral((LiteralFragment) fragment);
        return;
      }

      if (fragment instanceof ConversionFragment) {
        IPyObj obj;
        if (fragment instanceof PositionalFragment) {
          obj = getArg(values, ((PositionalFragment) fragment).pos, positionalCount);
        } else if (fragment instanceof NamedFragment) {
          obj = getArg(values, ((NamedFragment) fragment).name, ctx);
        } else {
          throw InternalErrorException.notReached();
        }

        formatObj(obj, ((ConversionFragment) fragment).conversion);
        return;
      }

      throw InternalErrorException.notReached();
    }

    private void validate() {
      if (fragments.stream().anyMatch(f -> f instanceof NamedFragment)) {
        if (positionalCount != 0) {
          throw PxException.valueError("named and positional arguments mixed", ctx);
        }
      } else if (positionalCount > 1) {
        if (!(values instanceof PyTuple)) {
          throw PxException.typeError(
              "expected a tuple of arguments, got " + PrintLib.repr(values, ctx), ctx);
        }
        PyTuple tup = (PyTuple) values;
        if (tup.len() != positionalCount) {
          throw PxException.valueError(
              String.format(
                  "expected a tuple of exactly %s elements, got %s", positionalCount, tup.len()),
              ctx);
        }
      } else if (positionalCount == 0) {
        if (!(values instanceof PyTuple)) {
          throw PxException.typeError(
              "expected a tuple of arguments, got " + PrintLib.repr(values, ctx), ctx);
        }
        PyTuple tup = (PyTuple) values;
        if (tup.len() != 0) {
          throw PxException.valueError(
              String.format("expected an empty tuple, got tuple of %s elements", tup.len()), ctx);
        }
      }
    }
  }

  private abstract static class StrFormatter extends Formatter {
    protected final PyStrBuilder sb = new PyStrBuilder();

    private StrFormatter(Parser parser, IPyObj values, PyContext ctx) {
      super(parser, values, ctx);
    }

    @Override
    protected void formatObj(IPyObj obj, Conversion conversion) {
      // TODO zzz

      if (conversion.type == ConversionType.F) {
        sb.appendUnknown(obj.toString());
        return;
      }

      if (conversion.flags == 0
          && conversion.minWidth == -1
          && conversion.minWidthArg == -1
          && conversion.precision == -1
          && conversion.precisionArg == -1) {
        switch (conversion.type) {
          case S:
            sb.appendStr(obj, ctx);
            break;
          case R:
            sb.appendRepr(obj, ctx);
            break;
          case D:
            if (obj instanceof PyBool) {
              sb.appendUcs2(obj == True ? "1" : "0");
            } else if (obj instanceof PyInt) {
              sb.appendUcs2(obj.toString());
            } else {
              throw new NotImplementedException("format");
            }
            break;
          default:
            throw new NotImplementedException("format");
        }
        return;
      }

      throw new NotImplementedException("format");
    }

    @Override
    protected IPyObj getResult() {
      return sb.build();
    }
  }

  private static class Ucs2Formatter extends StrFormatter {
    private Ucs2Formatter(Parser parser, IPyObj values, PyContext ctx) {
      super(parser, values, ctx);
    }

    @Override
    protected void formatLiteral(LiteralFragment fragment) {
      sb.appendUcs2((String) fragment.data);
    }
  }

  private static class Utf32Formatter extends StrFormatter {
    private Utf32Formatter(Parser parser, IPyObj values, PyContext ctx) {
      super(parser, values, ctx);
    }

    @Override
    protected void formatLiteral(LiteralFragment fragment) {
      sb.appendUtf32((int[]) fragment.data);
    }
  }

  private static class BytesFormatter extends Formatter {
    BytesFormatter(Parser parser, IPyObj values, PyContext ctx) {
      super(parser, values, ctx);
    }

    @Override
    protected IPyObj getResult() {
      throw new NotImplementedException("makeResult");
    }

    @Override
    protected void formatLiteral(LiteralFragment fragment) {
      throw new NotImplementedException("formatLiteral");
    }

    @Override
    protected void formatObj(IPyObj obj, Conversion conversion) {
      throw new NotImplementedException("format");
    }
  }

  private interface CharSource {
    int size();

    int get(int index);

    Object subString(int start, int end);

    String toString(int ch);
  }

  private static class Ucs2CharSource implements CharSource {
    private final String str;

    private Ucs2CharSource(String str) {
      this.str = str;
    }

    @Override
    public int size() {
      return str.length();
    }

    @Override
    public int get(int index) {
      return str.charAt(index);
    }

    @Override
    public Object subString(int start, int end) {
      return str.substring(start, end);
    }

    @Override
    public String toString(int ch) {
      if (ch != 0 && !Character.isISOControl((char) ch)) {
        return String.valueOf((char) ch);
      } else {
        return String.format("0x%4x", ch);
      }
    }
  }

  private static class Utf32CharSource implements CharSource {
    private final int[] str;

    private Utf32CharSource(int[] str) {
      this.str = str;
    }

    @Override
    public int size() {
      return str.length;
    }

    @Override
    public int get(int index) {
      return str[index];
    }

    @Override
    public Object subString(int start, int end) {
      return Arrays.copyOfRange(str, start, end);
    }

    @Override
    public String toString(int ch) {
      if (ch != 0 && !Character.isISOControl(ch)) {
        return Character.toString(ch);
      } else {
        return String.format("0x%8x", ch);
      }
    }
  }

  private static class BytesCharSource implements CharSource {
    private final byte[] bytes;

    private BytesCharSource(byte[] bytes) {
      this.bytes = bytes;
    }

    @Override
    public int size() {
      return bytes.length;
    }

    @Override
    public int get(int index) {
      return bytes[index];
    }

    @Override
    public Object subString(int start, int end) {
      return Arrays.copyOfRange(bytes, start, end);
    }

    @Override
    public String toString(int ch) {
      byte b = (byte) ch;
      if (b > 0 && !Character.isISOControl(b)) {
        return String.valueOf((char) b);
      } else {
        return String.format("0x%2x", b & 0xFF);
      }
    }
  }

  private static class ByteArrayCharSource implements CharSource {
    private final ByteArrayList bytes;

    private ByteArrayCharSource(ByteArrayList bytes) {
      this.bytes = bytes;
    }

    @Override
    public int size() {
      return bytes.size();
    }

    @Override
    public int get(int index) {
      return bytes.get(index);
    }

    @Override
    public Object subString(int start, int end) {
      return bytes.subList(start, end);
    }

    @Override
    public String toString(int ch) {
      byte b = (byte) ch;
      if (b > 0 && !Character.isISOControl(b)) {
        return String.valueOf((char) b);
      } else {
        return String.format("0x%2x", b & 0xFF);
      }
    }
  }

  private enum ConversionType {
    S,
    R,
    A,
    D,
    O,
    X_LOWER,
    X_UPPER,
    E_LOWER,
    E_UPPER,
    F,
    G_LOWER,
    G_UPPER,
    C,
  }

  private static final int ALTERNATE_FORM = 1;
  private static final int ZERO_PADDED = 2;
  private static final int LEFT_ADJUSTED = 4;
  private static final int BLANK_PLUS = 8;
  private static final int PRINT_SIGN = 16;

  private static class Conversion {
    ConversionType type = ConversionType.S;
    int flags;
    int minWidth = -1;
    int minWidthArg = -1;
    int precision = -1;
    int precisionArg = -1;
  }

  private abstract static class Fragment {}

  private static class LiteralFragment extends Fragment {
    final Object data;

    private LiteralFragment(Object data) {
      this.data = data;
    }
  }

  private static class ConversionFragment extends Fragment {
    final Conversion conversion;

    private ConversionFragment(Conversion conversion) {
      this.conversion = conversion;
    }
  }

  private static class PositionalFragment extends ConversionFragment {
    final int pos;

    private PositionalFragment(int pos, Conversion conversion) {
      super(conversion);
      this.pos = pos;
    }
  }

  private static class NamedFragment extends ConversionFragment {
    final String name;

    private NamedFragment(String name, Conversion conversion) {
      super(conversion);
      this.name = name;
    }
  }

  private static class Parser {
    private final PyContext ctx;
    private final CharSource chars;
    private final int size;
    private final List<Fragment> fragments = new ArrayList<>();

    private int pos;
    private int positionalCount;

    private Parser(CharSource chars, PyContext ctx) {
      this.chars = chars;
      this.size = chars.size();
      this.ctx = ctx;
    }

    void parse() {
      @Var int literalStart = 0;
      while (pos < size) {
        int ch = chars.get(pos);
        if (ch != '%') {
          ++pos;
          continue;
        }

        if (literalStart != pos) {
          fragments.add(new LiteralFragment(chars.subString(literalStart, pos)));
        }

        if (pos + 1 == size) {
          throw PxException.valueError("unterminated conversion specifier", ctx);
        }

        if (chars.get(pos + 1) == '%') {
          fragments.add(new LiteralFragment(chars.subString(pos, pos + 1)));
          pos += 2;
          continue;
        }

        fragments.add(readConversionSpecifier());
        literalStart = pos;
      }

      dcheck(pos == size);

      if (literalStart != size) {
        fragments.add(new LiteralFragment(chars.subString(literalStart, size)));
      }
    }

    private int curChar() {
      return getChar(pos);
    }

    private int getChar(int index) {
      if (index >= size) {
        throw PxException.valueError("invalid format string", ctx);
      }
      return chars.get(index);
    }

    private Fragment readConversionSpecifier() {
      if (Debug.ENABLED) {
        dcheck(curChar() == '%');
      }

      ++pos;

      @Nullable String name = readName();

      Conversion conversion = new Conversion();
      readConversionFlags(conversion);
      readMinWidth(conversion);
      readPrecision(conversion);
      conversion.type = readConversionType();

      if (name == null) {
        return new PositionalFragment(positionalCount++, conversion);
      } else {
        return new NamedFragment(name, conversion);
      }
    }

    private void readMinWidth(Conversion conversion) {
      int ch = curChar();
      if (ch == '*') {
        conversion.minWidthArg = positionalCount++;
        ++pos;
      } else if (isDigit(ch)) {
        conversion.minWidth = readInt();
      }
    }

    private void readPrecision(Conversion conversion) {
      if (curChar() != '.') {
        return;
      }
      ++pos;
      int ch = curChar();
      if (ch == '*') {
        conversion.precisionArg = positionalCount++;
        ++pos;
      } else if (isDigit(ch)) {
        conversion.precision = readInt();
      } else {
        throw PxException.valueError("missing precision", ctx);
      }
    }

    private static boolean isDigit(int ch) {
      return ch >= '0' && ch <= '9';
    }

    private static boolean isLetter(int ch) {
      return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
    }

    @SuppressWarnings("UnusedException")
    private int readInt() {
      if (Debug.ENABLED) {
        dcheck(isDigit(curChar()));
      }

      @Var int value = 0;
      while (pos < size) {
        int ch = curChar();
        if (!isDigit(ch)) {
          break;
        }
        try {
          value = Math.multiplyExact(value, 10);
        } catch (ArithmeticException ignored) {
          throw PxException.valueError("min width is too large", ctx);
        }
        value += ch - '0';
        ++pos;
      }
      return value;
    }

    private @Nullable String readName() {
      if (curChar() != '(') {
        return null;
      }

      @Var boolean isEmpty = true;
      StringBuilder sb = new StringBuilder(10);
      ++pos;

      while (pos < size) {
        int ch = chars.get(pos++);
        if (isLetter(ch) || ch == '_' || (isDigit(ch) && !isEmpty)) {
          sb.append((char) ch);
          isEmpty = false;
        } else if (ch == ')') {
          if (isEmpty) {
            throw PxException.valueError("missing name", ctx);
          }
          ++pos;
          return sb.toString();
        } else {
          throw PxException.valueError("invalid name character", ctx);
        }
      }

      throw PxException.valueError("unterminated name", ctx);
    }

    private ConversionType readConversionType() {
      @Var int ch = getChar(pos++);
      switch (ch) {
        case 'l':
        case 'L':
        case 'h':
          ch = getChar(pos++);
          break;
        default:
          break;
      }

      switch (ch) {
        case 'd':
        case 'i':
        case 'u':
          return ConversionType.D;
        case 'o':
          return ConversionType.O;
        case 'x':
          return ConversionType.X_LOWER;
        case 'X':
          return ConversionType.X_UPPER;
        case 'e':
          return ConversionType.E_LOWER;
        case 'E':
          return ConversionType.E_UPPER;
        case 'f':
        case 'F':
          return ConversionType.F;
        case 'g':
          return ConversionType.G_LOWER;
        case 'G':
          return ConversionType.G_UPPER;
        case 'c':
          return ConversionType.C;
        case 'r':
          return ConversionType.R;
        case 's':
          return ConversionType.S;
        case 'a':
          return ConversionType.A;

        default:
          throw PxException.valueError("invalid conversion type " + chars.toString(ch), ctx);
      }
    }

    private void readConversionFlags(Conversion conversion) {
      while (pos < size) {
        int ch = chars.get(pos);

        switch (ch) {
          case '#':
            conversion.flags |= ALTERNATE_FORM;
            break;
          case '0':
            conversion.flags |= ZERO_PADDED;
            break;
          case '-':
            conversion.flags |= LEFT_ADJUSTED;
            break;
          case ' ':
            conversion.flags |= BLANK_PLUS;
            break;
          case '+':
            conversion.flags |= PRINT_SIGN;
            break;

          default:
            return;
        }

        ++pos;
      }
    }
  }
}
