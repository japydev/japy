package org.japy.kernel.types.coll;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;
import static org.japy.kernel.types.misc.PyNone.None;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyMapping;
import org.japy.kernel.types.coll.dict.IPyROMapping;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.iter.IPyIterable;
import org.japy.kernel.types.coll.iter.IterLib;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.seq.IPyROSequence;
import org.japy.kernel.types.coll.seq.IntSlice;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxIndexError;
import org.japy.kernel.types.exc.px.PxOverflowError;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.misc.PySlice;
import org.japy.kernel.types.num.IntLib;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.proto.IPyObjWithLen;

public final class CollLib {
  private CollLib() {}

  public static final BigInteger BI_MAX_INT = BigInteger.valueOf(Integer.MAX_VALUE);

  public static int checkIndex(@Var int index, int length, PyContext ctx) {
    if (index < 0) {
      index += length;
    }
    if (index < 0 || index >= length) {
      throw new PxIndexError("list index out of range", ctx);
    }
    return index;
  }

  public static int checkIndexForInsert(@Var int index, int length, PyContext ctx) {
    if (index < 0) {
      index += length;
    }
    if (index < 0 || index > length) {
      throw new PxIndexError("list index out of range", ctx);
    }
    return index;
  }

  public static int checkIndexForInsert(IPyObj index, int length, PyContext ctx) {
    Object indexObj = grokListIndex(index, ctx);
    if (indexObj instanceof IntSlice) {
      throw PxException.typeError(
          String.format("index must be an int, got '%s'", index.typeName()), ctx);
    }
    return checkIndexForInsert((int) indexObj, length, ctx);
  }

  public static int len(IPyObj obj, PyContext ctx) {
    if (obj instanceof IPyObjWithLen) {
      return ((IPyObjWithLen) obj).len(ctx);
    }
    return lenMeta(obj, ctx);
  }

  public static PxException throwLenUnsupported(IPyObj obj, PyContext ctx) {
    throw PxException.typeError(
        String.format("object of type '%s' has no len()", obj.typeName()), ctx);
  }

  private static int lenMeta(IPyObj obj, PyContext ctx) {
    @Nullable FuncHandle len = obj.specialMethod(SpecialMethod.LEN);
    if (len == null) {
      throw throwLenUnsupported(obj, ctx);
    }

    try {
      return (int) len.handle.invokeExact(obj, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  public static PxException throwContainsUnsupported(IPyObj coll, PyContext ctx) {
    throw PxException.typeError(
        String.format("objects of type '%s' do not support containment check", coll.typeName()),
        ctx);
  }

  public static boolean contains(IPyObj coll, IPyObj elm, PyContext ctx) {
    if (coll instanceof IPyContains) {
      return ((IPyContains) coll).contains(elm, ctx);
    }

    @Nullable FuncHandle contains = coll.specialMethodOrNone(SpecialMethod.CONTAINS);
    if (contains == DELETED_METHOD) {
      throw throwContainsUnsupported(coll, ctx);
    }
    if (contains != null) {
      try {
        return (boolean) contains.handle.invokeExact(coll, elm, ctx);
      } catch (Throwable ex) {
        throw ExcUtil.rethrow(ex);
      }
    }

    for (IPyObj obj : new JavaIterable(coll, ctx)) {
      if (obj == elm || obj.isEqual(elm, ctx)) {
        return true;
      }
    }

    return false;
  }

  public static IPyObj in(IPyObj elm, IPyObj coll, PyContext ctx) {
    return PyBool.of(contains(coll, elm, ctx));
  }

  public static IPyObj notIn(IPyObj elm, IPyObj coll, PyContext ctx) {
    return PyBool.of(!contains(coll, elm, ctx));
  }

  public static void setItem(IPyObj coll, IPyObj index, IPyObj value, PyContext ctx) {
    @Nullable FuncHandle setItem = coll.specialMethod(SpecialMethod.SETITEM);
    if (setItem == null) {
      throw PxException.typeError(
          String.format("'%s' object does not support item assignment", coll.typeName()), ctx);
    }
    try {
      setItem.handle.invokeExact(coll, index, value, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  public static IPyObj getItem(IPyObj coll, IPyObj index, PyContext ctx) {
    @Nullable FuncHandle getItem = coll.specialMethod(SpecialMethod.GETITEM);
    if (getItem != null) {
      try {
        return (IPyObj) getItem.handle.invokeExact(coll, index, ctx);
      } catch (Throwable ex) {
        throw ExcUtil.rethrow(ex);
      }
    }

    if (coll instanceof IPyClass) {
      @Nullable
      FuncHandle classGetItem =
          ((IPyClass) coll).methodTable().getIfPresent(SpecialMethod.CLASSGETITEM);
      if (classGetItem != null) {
        try {
          return (IPyObj) classGetItem.handle.invokeExact(coll, index, ctx);
        } catch (Throwable ex) {
          throw ExcUtil.rethrow(ex);
        }
      }
    }

    throw PxException.typeError(
        String.format("'%s' object is not subscriptable", coll.typeName()), ctx);
  }

  public static void delItem(IPyObj coll, IPyObj index, PyContext ctx) {
    @Nullable FuncHandle delItem = coll.specialMethod(SpecialMethod.DELITEM);
    if (delItem == null) {
      throw PxException.typeError(
          String.format("'%s' object does not support item deletion", coll.typeName()), ctx);
    }
    try {
      delItem.handle.invokeExact(coll, index, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  public static int toLenRaw(IPyObj obj, PyContext ctx) {
    if (!(obj instanceof PyInt)) {
      throw PxException.typeError(
          "len() returned an instance of type " + obj.typeName() + ", expected int", ctx);
    }
    if (!((PyInt) obj).isLong()) {
      throw PxException.valueError("len() returned a value too large", ctx);
    }
    long l = ((PyInt) obj).longValue();
    if (l < 0) {
      throw PxException.valueError("len() returned a negative value", ctx);
    }
    if (l > Integer.MAX_VALUE) {
      throw PxException.valueError("len() returned a value too large", ctx);
    }
    return (int) l;
  }

  private static PyTuple unpackIterable(IPyObj coll, PyContext ctx) {
    List<IPyObj> elements =
        (coll instanceof IPyObjWithLen)
            ? new ArrayList<>(((IPyObjWithLen) coll).len(ctx))
            : new ArrayList<>();

    forEach(coll, elements::add, ctx);

    return new PyTuple(elements);
  }

  public static IPyROSequence unpackToSequence(IPyObj coll, PyContext ctx) {
    if (coll instanceof IPyROSequence) {
      return (IPyROSequence) coll;
    }

    return unpackIterable(coll, ctx);
  }

  public static PyTuple unpack(IPyObj coll, PyContext ctx) {
    if (coll instanceof PyTuple) {
      return (PyTuple) coll;
    }

    if (coll instanceof IPyROSequence) {
      IPyROSequence seq = (IPyROSequence) coll;
      int count = seq.len(ctx);
      IPyObj[] elements = new IPyObj[count];
      for (int i = 0; i != count; ++i) {
        elements[i] = seq.get(i, ctx);
      }
      return new PyTuple(elements);
    }

    return unpackIterable(coll, ctx);
  }

  //  public static int dispatchLen(IPyObj obj, PyContext ctx) {
  //    IPyObj len = BuiltinClassLib.getClassAttrOrNull(obj.pyClass(), Constants.__LEN__, ctx);
  //    if (len != null) {
  //      IPyObj v = ((IPyFunc) len).call(obj, ctx);
  //      if (!(v instanceof PyInt)) {
  //        throw PxException.typeError(
  //            String.format("len() returned a value of type %s instead of int", v.pyClassName()));
  //      }
  //      if (!PyInt.c(v).isSmall()) {
  //        throw new PxOverflowError(String.format("value returned from len() is too large: %s",
  // v));
  //      }
  //      return PyInt.c(v).intValue();
  //    }
  //
  //    // TODO
  //    throw new NotImplementedException("len");
  //  }

  @SuppressWarnings("UnusedException")
  public static int sumLen(int len1, int len2, PyContext ctx) {
    try {
      return Math.addExact(len1, len2);
    } catch (ArithmeticException ignored) {
      throw new PxOverflowError("result is too long", ctx);
    }
  }

  @SuppressWarnings("ResultOfMethodCallIgnored")
  public static boolean checkMulOverflows(int x, int y) {
    try {
      Math.multiplyExact(x, y);
      return false;
    } catch (ArithmeticException ignored) {
      return true;
    }
  }

  //  public static IPyObj getLenMulBy(int len, IPyObj n, PyContext ctx) {
  //    PyInt in;
  //    if (n instanceof PyInt) {
  //      in = (PyInt) n;
  //    } else {
  //      IPyCallable index = PyClassUtil.getSpecialMethodOrNull(n, Constants.__INDEX__, ctx);
  //      if (index == null) {
  //        return NotImplemented;
  //      }
  //      IPyObj v = index.call(n, ctx);
  //      if (!(v instanceof PyInt)) {
  //        throw PxException.typeError("__index__() must return an int");
  //      }
  //      in = (PyInt) v;
  //    }
  //
  //    int iMulBy = PyNumOps.getInt(in);
  //    if (iMulBy <= 0) {
  //      return PyInt.ZERO;
  //    }
  //
  //    try {
  //      //noinspection ResultOfMethodCallIgnored
  //      Math.multiplyExact(len, iMulBy);
  //    } catch (ArithmeticException ex) {
  //      throw new PxOverflowError("result is too long", ex);
  //    }
  //
  //    return PyInt.get(iMulBy);
  //  }

  public static @Nullable Integer sliceIndex(IPyObj idxObj, PyContext ctx) {
    if (idxObj == None) {
      return null;
    }

    PyInt idx = IntLib.asInt(idxObj, ctx);
    if (!idx.isSmall()) {
      throw new PxOverflowError("index is too large", ctx);
    }
    return idx.intValue();
  }

  public static Object grokListIndex(IPyObj index, PyContext ctx) {
    if (index instanceof PyInt) {
      PyInt i = (PyInt) index;
      if (!i.isSmall()) {
        throw new PxOverflowError("index is too large", ctx);
      }
      return i.intValue();
    }

    if (index instanceof PySlice) {
      PySlice slice = (PySlice) index;
      return new IntSlice(
          sliceIndex(slice.start, ctx), sliceIndex(slice.stop, ctx), sliceIndex(slice.step, ctx));
    }

    PyInt i = IntLib.asInt(index, ctx);
    if (!i.isSmall()) {
      throw new PxOverflowError("index is too large", ctx);
    }
    return i.intValue();
  }

  //  public static PyList makeList(IPyObj obj, PyContext ctx) {
  //    if (obj instanceof PyList) {
  //      return (PyList) obj;
  //    }
  //
  //    ICtxAwareIterator iter = PyCollObjUtil.getJavaIterator(obj, ctx);
  //    List<IPyObj> list = new ArrayList<>();
  //    while (iter.hasNext(ctx)) {
  //      list.add(iter.next(ctx));
  //    }
  //    return new PyList(list);
  //  }
  //
  //  public static PySet makeSet(IPyObj iterable, PyContext ctx) {
  //    ICtxAwareIterator iter = PyCollObjUtil.getJavaIterator(iterable, ctx);
  //    PySet set = new PySet();
  //    while (iter.hasNext(ctx)) {
  //      set.content.add(iter.next(ctx), ctx);
  //    }
  //    return set;
  //  }
  //
  //  public static PyDict makeDict(IPyObj iterable, PyContext ctx) {
  //    ICtxAwareIterator iter = PyCollObjUtil.getJavaIterator(iterable, ctx);
  //    PyDict dict = new PyDict();
  //    while (iter.hasNext(ctx)) {
  //      ICtxAwareIterator prIter = PyCollObjUtil.getJavaIterator(iter.next(ctx), ctx);
  //      IPyObj key = prIter.next(ctx);
  //      IPyObj value = prIter.next(ctx);
  //      if (prIter.hasNext(ctx)) {
  //        throw PxException.typeError("too many values to unpack");
  //      }
  //      dict.setItem(key, value, ctx);
  //    }
  //    return dict;
  //  }
  //
  //  private static class NotEqual extends RuntimeException {}
  //
  //  public static boolean isEqual(IPyRODict d1, IPyRODict d2, PyContext ctx) {
  //    if (d1 == d2) {
  //      return true;
  //    }
  //
  //    if (d1.len() != d2.len()) {
  //      return false;
  //    }
  //
  //    try {
  //      d1.forEach(
  //          (k1, v1) -> {
  //            IPyObj v2 = d2.findItem(k1, ctx);
  //            if (v2 == null || !v1.isEqual(v2, ctx)) {
  //              throw new NotEqual();
  //            }
  //          });
  //    } catch (NotEqual ignored) {
  //      return false;
  //    }
  //
  //    return true;
  //  }

  public static PyList listOf(String... content) {
    List<IPyObj> list = new ArrayList<>(content.length);
    for (String s : content) {
      list.add(PyStr.get(s));
    }
    return PyList.take(list);
  }

  //  public static PyList listOf(Stream<IPyObj> content) {
  //    return PyList.take(content.collect(Collectors.toList()));
  //  }

  //  public static PyTuple tupleOfStrings(Collection<String> content) {
  //    return PyTuple.of(content.stream().map(PyStr::get).toArray(IPyObj[]::new));
  //  }

  public static PyTuple tupleOfStrings(Stream<String> content) {
    return PyTuple.of(content.map(PyStr::get).toArray(IPyObj[]::new));
  }

  //  public static PyTuple tupleOf(Collection<IPyObj> content) {
  //    if (content.isEmpty()) {
  //      return PyTuple.EMPTY;
  //    }
  //    IPyObj[] array = new IPyObj[content.size()];
  //    @Var int i = 0;
  //    for (IPyObj o : content) {
  //      array[i++] = o;
  //    }
  //    return new PyTuple(array);
  //  }

  //  public static PyTuple tupleOfStrings(String... content) {
  //    if (content.length == 0) {
  //      return PyTuple.EMPTY;
  //    }
  //    IPyObj[] array = new IPyObj[content.length];
  //    @Var int i = 0;
  //    for (String s : content) {
  //      array[i++] = PyStr.get(s);
  //    }
  //    return new PyTuple(array);
  //  }

  public static void forEach(IPyObj coll, Consumer<IPyObj> func, PyContext ctx) {
    if (coll instanceof IPyIterable) {
      ((IPyIterable) coll).forEach(func, ctx);
      return;
    }

    IPyObj iter = IterLib.iter(coll, ctx);
    while (true) {
      try {
        func.accept(IterLib.next(iter, ctx));
      } catch (PxStopIteration ignored) {
        break;
      }
    }
  }

  public static IPyObj sortedKeys(IPyDict dict, PyContext ctx) {
    List<IPyObj> list = new ArrayList<>(dict.len(ctx));
    dict.forEach(k -> list.add(k), ctx);
    list.sort(Comparator.comparing(n -> (PyStr) n));
    return PyList.take(list);
  }

  public static void dictForEach(IPyObj dict, BiConsumer<IPyObj, IPyObj> func, PyContext ctx) {
    if (dict instanceof IPyROMapping) {
      ((IPyROMapping) dict).forEach(func, ctx);
      return;
    }

    for (IPyObj key : new JavaIterable(dict, ctx)) {
      IPyObj value = getItem(dict, key, ctx);
      func.accept(key, value);
    }
  }

  private static boolean updateWithKeys(IPyMapping dict, IPyObj obj, PyContext ctx) {
    @Nullable IPyObj keys = ObjLib.getAttrOrNull(obj, "keys", ctx);
    if (keys == null) {
      return false;
    }

    for (IPyObj key : new JavaIterable(FuncLib.call(keys, ctx), ctx)) {
      dict.setItem(key, getItem(obj, key, ctx), ctx);
    }

    return true;
  }

  private static void update(IPyMapping dict, IPyObj obj, PyContext ctx) {
    if (obj instanceof IPyROMapping) {
      ((IPyROMapping) obj).forEach((k, v) -> dict.setItem(k, v, ctx), ctx);
      return;
    }

    if (updateWithKeys(dict, obj, ctx)) {
      return;
    }

    dictForEach(obj, (k, v) -> dict.setItem(k, v, ctx), ctx);
  }

  public static void update(
      IPyMapping dict, @Nullable IPyObj dictOrIterable, PyDict kwargs, PyContext ctx) {
    if (dictOrIterable != null) {
      update(dict, dictOrIterable, ctx);
    }
    update(dict, kwargs, ctx);
  }

  public static IPyObj sorted(IPyObj iterable, IPyObj key, boolean reverse, PyContext ctx) {
    PyList list = PyList.ofIterable(iterable, ctx);
    list.sort(key, reverse, ctx);
    return list;
  }
}
