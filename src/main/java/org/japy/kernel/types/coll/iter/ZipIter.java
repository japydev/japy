package org.japy.kernel.types.coll.iter;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;
import java.util.Arrays;

import com.google.errorprone.annotations.Var;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.exc.px.PxRuntimeError;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.obj.IPyNoValidationObj;

class ZipIter implements IPyIter, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass("zip", ZipIter.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private final IPyObj[] iters;
  private final boolean strict;
  private boolean done;

  ZipIter(IPyObj[] iters, boolean strict) {
    this.iters = iters;
    this.strict = strict;
    this.done = iters.length == 0;
  }

  private void setDone() {
    done = true;
    Arrays.fill(iters, None);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj next(PyContext ctx) {
    if (done) {
      throw new PxStopIteration(ctx);
    }

    @Var boolean seenValue = false;
    @Var boolean seenEnd = false;
    IPyObj[] values = new IPyObj[iters.length];
    for (int i = 0; i != values.length; ++i) {
      try {
        values[i] = IterLib.next(iters[i], ctx);
        if (strict && seenEnd) {
          setDone();
          throw new PxRuntimeError("not all iterables exhausted in zip()", ctx);
        }
        seenValue = true;
      } catch (PxStopIteration ex) {
        if (!strict) {
          setDone();
          throw ex;
        }
        if (seenValue) {
          setDone();
          throw new PxRuntimeError("not all iterables exhausted in zip()", ctx);
        }
        seenEnd = true;
      }
    }

    if (seenEnd) {
      setDone();
      throw new PxStopIteration(ctx);
    }

    return PyTuple.of(values);
  }
}
