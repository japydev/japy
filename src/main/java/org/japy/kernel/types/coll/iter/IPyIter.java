package org.japy.kernel.types.coll.iter;

import java.util.function.Consumer;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

public interface IPyIter extends IPyTrueAtomObj, IPyIterable {
  @Override
  default IPyIter iter(PyContext ctx) {
    return this;
  }

  @SpecialInstanceMethod("__next__")
  IPyObj next(PyContext ctx);

  @Override
  default void forEach(Consumer<? super IPyObj> func, PyContext ctx) {
    while (true) {
      try {
        func.accept(next(ctx));
      } catch (PxStopIteration ignored) {
        break;
      }
    }
  }
}
