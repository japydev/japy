package org.japy.kernel.types.coll.seq;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;

import java.lang.invoke.MethodHandles;
import java.util.ArrayDeque;
import java.util.Deque;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.Var;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.iter.IPyIterable;
import org.japy.kernel.types.coll.iter.PyJavaIter;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.proto.IPyObjWithLen;
import org.japy.kernel.types.obj.proto.IPyUnhashableObj;

public class PyDeque implements IPyIterable, IPyObjWithLen, IPyUnhashableObj {
  public static final IPyClass TYPE =
      new PyBuiltinClass("deque", PyDeque.class, MethodHandles.lookup(), PyBuiltinClass.CALLABLE);

  // TODO zzz thread safety
  private final Deque<IPyObj> content;
  private final int maxLen;

  @Constructor
  public PyDeque(
      @Param(value = "iterable", dflt = DEFAULT_SENTINEL) IPyObj iterable,
      @Param(value = "maxlen", dflt = DEFAULT_SENTINEL) IPyObj maxLenObj,
      PyContext ctx) {
    if (maxLenObj != Sentinel) {
      maxLen = ArgLib.checkArgSmallPosInt("deque", "maxlen", maxLenObj, ctx);
    } else {
      maxLen = Integer.MAX_VALUE;
    }

    if (iterable == Sentinel) {
      content = new ArrayDeque<>();
    } else {
      @Var int estimatedSize = 0;
      if (iterable instanceof IPyObjWithLen) {
        estimatedSize = Math.min(maxLen, ((IPyObjWithLen) iterable).len(ctx));
      }
      content = estimatedSize != 0 ? new ArrayDeque<>(estimatedSize) : new ArrayDeque<>();
      CollLib.forEach(iterable, o -> append(o, ctx), ctx);
    }
  }

  @Override
  public void validate(Validator v) {
    content.forEach(v::validate);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyIter iter(PyContext ctx) {
    return new PyJavaIter(content.iterator());
  }

  @Override
  public int len(PyContext ctx) {
    return content.size();
  }

  @InstanceMethod
  @CanIgnoreReturnValue
  private IPyObj append(@Param("object") IPyObj obj, PyContext ctx) {
    content.add(obj);
    if (content.size() > maxLen) {
      content.pop();
    }
    return None;
  }
}
