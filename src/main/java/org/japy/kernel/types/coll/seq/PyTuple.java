package org.japy.kernel.types.coll.seq;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;
import static org.japy.kernel.types.num.PyBool.True;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;

import org.japy.base.ParamKind;
import org.japy.infra.coll.ctx.CtxAwareCollections;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.iter.PyJavaIter;
import org.japy.kernel.types.coll.str.DeepRepr;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.types.obj.proto.IPyObjWithComp;
import org.japy.kernel.types.obj.proto.IPyObjWithDeepRepr;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyTuple
    implements IPyIntHashableObj, IPyImmSequence, IPyObjWithDeepRepr, IPyObjWithComp {
  public static final PyTuple EMPTY_TUPLE = new PyTuple();

  public static final PyBuiltinClass TYPE =
      new PyBuiltinClass(
          "tuple",
          PyTuple.class,
          PyBuiltinClass.REGULAR,
          "tuple(iterable=(), /)\n"
              + "--\n"
              + "\n"
              + "Built-in immutable sequence.\n"
              + "\n"
              + "If no argument is given, the constructor returns an empty tuple.\n"
              + "If iterable is specified the tuple is initialized from iterable's items.\n"
              + "\n"
              + "If the argument is a tuple, the return value is the same object.");

  protected final IPyObj[] content;

  private PyTuple() {
    content = EMPTY_OBJECT_ARRAY;
  }

  public PyTuple(IPyObj... elements) {
    content = elements;
  }

  public PyTuple(List<IPyObj> elements) {
    content = elements.toArray(IPyObj[]::new);
  }

  public PyTuple(IPyObj[] elements, int from, int to) {
    content = Arrays.copyOfRange(elements, from, to);
  }

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(1).add("iterable", ParamKind.POS_ONLY, Sentinel).build();

  @SubClassConstructor
  protected PyTuple(Args args, PyContext ctx) {
    IPyObj iterable = ARG_PARSER.parse1(args, "tuple", ctx);
    if (iterable != Sentinel) {
      content = arrayFromIterable(iterable, ctx);
    } else {
      content = EMPTY_OBJECT_ARRAY;
    }
  }

  @Constructor
  public static IPyObj construct(Args args, PyContext ctx) {
    IPyObj iterable = ARG_PARSER.parse1(args, "tuple", ctx);
    if (iterable == Sentinel) {
      return EMPTY_TUPLE;
    }
    if (iterable instanceof PyTuple) {
      return iterable;
    }
    return new PyTuple(arrayFromIterable(iterable, ctx));
  }

  private static IPyObj[] arrayFromIterable(IPyObj iterable, PyContext ctx) {
    if (iterable instanceof IPyROSequence) {
      IPyROSequence seq = (IPyROSequence) iterable;
      IPyObj[] elements = new IPyObj[seq.len(ctx)];
      for (int i = 0; i != elements.length; ++i) {
        elements[i] = seq.get(i, ctx);
      }
      return elements;
    }
    List<IPyObj> elements = new ArrayList<>();
    for (IPyObj obj : new JavaIterable(iterable, ctx)) {
      elements.add(obj);
    }
    return elements.toArray(IPyObj[]::new);
  }

  public IPyObj[] __array() {
    return content;
  }

  public static PyTuple of(IPyObj... content) {
    if (content.length == 0) {
      return EMPTY_TUPLE;
    }
    return new PyTuple(content);
  }

  public static PyTuple of(IPyObj obj1) {
    return new PyTuple(obj1);
  }

  public static PyTuple of(IPyObj obj1, IPyObj obj2) {
    return new PyTuple(obj1, obj2);
  }

  public static PyTuple of(IPyObj obj1, IPyObj obj2, IPyObj obj3) {
    return new PyTuple(obj1, obj2, obj3);
  }

  public static PyTuple of(IPyObj obj1, IPyObj obj2, IPyObj obj3, IPyObj obj4) {
    return new PyTuple(obj1, obj2, obj3, obj4);
  }

  public static Builder builder(int expectedSize) {
    return new Builder(expectedSize);
  }

  public boolean isEmpty() {
    return content.length == 0;
  }

  @Override
  public IPyIter iter(PyContext ctx) {
    return new PyJavaIter(Arrays.asList(content));
  }

  @Override
  public IPyObj get(int index, PyContext ctx) {
    return content[CollLib.checkIndex(index, content.length, ctx)];
  }

  @Override
  public IPyObj slice(IntSlice slice, PyContext ctx) {
    SliceIndices indices = slice.indices(content.length, ctx);
    if (indices.isEmpty()) {
      return EMPTY_TUPLE;
    }
    if (indices.step != 1) {
      throw new NotImplementedException("slice");
    }
    if (indices.start == 0 && indices.stop == content.length) {
      return this;
    }
    return new PyTuple(Arrays.copyOfRange(content, indices.start, indices.stop));
  }

  @Override
  public boolean _canConcat(IPyObj other) {
    return other instanceof PyTuple;
  }

  @Override
  public IPyObj _concat(IPyROSequence seq, PyContext ctx) {
    PyTuple tup = (PyTuple) seq;
    IPyObj[] sum = new IPyObj[content.length + tup.content.length];
    System.arraycopy(content, 0, sum, 0, content.length);
    System.arraycopy(tup.content, 0, sum, content.length, tup.content.length);
    return new PyTuple(sum);
  }

  @Override
  public IPyObj _repeat(int times, PyContext ctx) {
    IPyObj[] newContent = new IPyObj[times * content.length];
    for (int i = 0; i != times; ++i) {
      System.arraycopy(content, 0, newContent, i * content.length, content.length);
    }
    return new PyTuple(newContent);
  }

  @Override
  public IPyObj makeEmpty(PyContext ctx) {
    return EMPTY_TUPLE;
  }

  @Override
  public void validate(Validator v) {
    v.validate(content);
  }

  @Override
  public void print(DeepRepr dr, PyContext ctx) {
    PrintLib.printTuple(this, dr, ctx);
  }

  @Override
  public IPyObj __lt__(IPyObj other, PyContext ctx) {
    if (!(other instanceof PyTuple)) {
      return NotImplemented;
    }
    return PyBool.of(SeqLib.compare(this, (PyTuple) other, ctx) < 0);
  }

  @Override
  public IPyObj __le__(IPyObj other, PyContext ctx) {
    if (!(other instanceof PyTuple)) {
      return NotImplemented;
    }
    return PyBool.of(SeqLib.compare(this, (PyTuple) other, ctx) <= 0);
  }

  @Override
  public IPyObj __gt__(IPyObj other, PyContext ctx) {
    if (!(other instanceof PyTuple)) {
      return NotImplemented;
    }
    return PyBool.of(SeqLib.compare(this, (PyTuple) other, ctx) > 0);
  }

  @Override
  public IPyObj __ge__(IPyObj other, PyContext ctx) {
    if (!(other instanceof PyTuple)) {
      return NotImplemented;
    }
    return PyBool.of(SeqLib.compare(this, (PyTuple) other, ctx) >= 0);
  }

  @CanIgnoreReturnValue
  public static class Builder {
    private final List<IPyObj> content;

    private Builder(int expectedSize) {
      content = new ArrayList<>(Math.max(expectedSize, 0));
    }

    public Builder add(IPyObj obj) {
      content.add(obj);
      return this;
    }

    public Builder extend(IPyObj coll, PyContext ctx) {
      CollLib.forEach(coll, this::add, ctx);
      return this;
    }

    @CheckReturnValue
    public PyTuple build() {
      return new PyTuple(content.toArray(IPyObj[]::new));
    }
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj __eq__(IPyObj other, PyContext ctx) {
    if (this == other) {
      return True;
    }
    if (!(other instanceof PyTuple)) {
      return NotImplemented;
    }
    return PyBool.of(CtxAwareCollections.equals(content, ((PyTuple) other).content, ctx));
  }

  @Override
  public int intHashCode(PyContext ctx) {
    return CtxAwareCollections.hash(ctx, content);
  }

  public int len() {
    return content.length;
  }

  @Override
  public int len(PyContext ctx) {
    return content.length;
  }

  public IPyObj get(int index) {
    return content[index];
  }

  public void forEachNoCtx(Consumer<? super IPyObj> func) {
    for (IPyObj obj : content) {
      func.accept(obj);
    }
  }

  @Override
  public String toString() {
    return "("
        + Arrays.stream(content).map(Object::toString).collect(Collectors.joining(", "))
        + ")";
  }
}
