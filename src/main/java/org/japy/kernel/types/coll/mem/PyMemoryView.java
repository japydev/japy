package org.japy.kernel.types.coll.mem;

import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.Args;

public class PyMemoryView implements IPyTrueAtomObj {
  public static final IPyClass TYPE =
      new PyBuiltinClass("memoryview", PyMemoryView.class, PyBuiltinClass.CALLABLE);

  @Constructor
  public PyMemoryView(Args args, PyContext ctx) {
    throw new NotImplementedException("memoryview");
  }

  @Override
  public void validate(Validator v) {}

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
