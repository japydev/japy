package org.japy.kernel.types.coll.dict;

import java.util.function.BiConsumer;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.str.DeepRepr;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.proto.IPyUnhashableObj;

public class PyMappingProxy implements IPyRODict, IPyUnhashableObj {
  private final IPyROMapping dict;

  public PyMappingProxy(IPyROMapping dict) {
    this.dict = dict;
  }

  @Constructor
  public PyMappingProxy(@Param("dict") IPyObj dict, PyContext ctx) {
    this.dict = ArgLib.checkArgROMapping("mappingproxy", "dict", dict, ctx);
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    return new PyDict(dict, ctx);
  }

  private static class TYPE_HOLDER {
    private static final IPyClass TYPE =
        new PyBuiltinClass("mappingproxy", PyMappingProxy.class, PyBuiltinClass.CALLABLE);
  }

  @Override
  public IPyClass type() {
    return TYPE_HOLDER.TYPE;
  }

  @Override
  public void validate(Validator v) {
    v.validate(dict);
  }

  @Override
  public void print(DeepRepr dr, PyContext ctx) {
    dr.sb.appendUcs2("mappingproxy(");
    dr.append(dict, ctx);
    dr.sb.appendUcs2(")");
  }

  @Override
  public boolean contains(IPyObj item, PyContext ctx) {
    return dict.contains(item, ctx);
  }

  @Override
  public void _fastForEach(BiConsumer<? super IPyObj, ? super IPyObj> func) {
    dict._fastForEach(func);
  }

  @Override
  public IPyObj getItem(IPyObj key, PyContext ctx) {
    return dict.getItem(key, ctx);
  }

  @Override
  public IPyObj items(PyContext ctx) {
    return dict.items(ctx);
  }

  @Override
  public IPyObj values(PyContext ctx) {
    return dict.values(ctx);
  }

  @Override
  public IPyIter iter(PyContext ctx) {
    return dict.iter(ctx);
  }

  @Override
  public int len(PyContext ctx) {
    return dict.len(ctx);
  }
}
