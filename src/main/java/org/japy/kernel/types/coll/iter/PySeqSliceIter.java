package org.japy.kernel.types.coll.iter;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.seq.IPyROSequence;
import org.japy.kernel.types.coll.seq.IntSlice;
import org.japy.kernel.types.coll.seq.SliceIndices;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.obj.IPyNoValidationObj;

class PySeqSliceIter implements IPyIter, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "seqsliceiter", PySeqSliceIter.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private final IPyROSequence seq;
  private final SliceIndices indices;
  private int next;

  PySeqSliceIter(IPyROSequence seq, IntSlice slice, PyContext ctx) {
    this.seq = seq;
    this.indices = slice.indices(seq.len(ctx), ctx);
    this.next = indices.start;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj next(PyContext ctx) {
    if (indices.step > 0) {
      if (next >= indices.stop) {
        throw new PxStopIteration(ctx);
      }
    } else {
      if (next <= indices.stop) {
        throw new PxStopIteration(ctx);
      }
    }

    IPyObj value = seq.get(next, ctx);
    next += indices.step;
    return value;
  }
}
