package org.japy.kernel.types.coll.seq;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;

public interface IPyImmSequence extends IPyROSequence {
  @Override
  default IPyObj makeCopy(PyContext ctx) {
    return this;
  }
}
