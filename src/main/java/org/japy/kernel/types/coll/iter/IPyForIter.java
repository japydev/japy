package org.japy.kernel.types.coll.iter;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;

public interface IPyForIter extends IPyObj {
  @Nullable
  IPyObj next(PyContext ctx);
}
