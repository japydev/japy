package org.japy.kernel.types.coll.dict;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.ext.adapters.IPyROMappingProtocolAdapter;
import org.japy.kernel.types.coll.IPyContainerObj;
import org.japy.kernel.types.coll.iter.IPyIterable;
import org.japy.kernel.types.coll.iter.IterLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxKeyError;
import org.japy.kernel.types.obj.proto.IPyObjWithLen;

@Protocol(IPyROMappingProtocolAdapter.class)
public interface IPyROMappingProtocol extends IPyObjWithLen, IPyIterable, IPyContainerObj {
  default boolean contains(String key, PyContext ctx) {
    return contains(PyStr.get(key), ctx);
  }

  /** Gets the item by key, throws a PxKeyError if it's not there */
  @ProtocolMethod
  IPyObj getItem(IPyObj key, PyContext ctx);

  /** Returns an iterator over (key, value) tuples */
  @InstanceMethod
  @ProtocolMethod
  IPyObj items(PyContext ctx);

  /** Returns an iterator over keys */
  @InstanceMethod
  @ProtocolMethod
  default IPyObj keys(PyContext ctx) {
    return IterLib.iter(this, ctx);
  }

  /** Returns an iterator over values */
  @InstanceMethod
  @ProtocolMethod
  IPyObj values(PyContext ctx);

  /** Gets the item by key, returns null if it's not there */
  @ProtocolMethod
  default @Nullable IPyObj getOrNull(IPyObj key, PyContext ctx) {
    try {
      return getItem(key, ctx);
    } catch (PxKeyError ignored) {
      return null;
    }
  }

  @SpecialInstanceMethod
  default IPyObj __getitem__(@Param("key") IPyObj key, PyContext ctx) {
    return getItem(key, ctx);
  }
}
