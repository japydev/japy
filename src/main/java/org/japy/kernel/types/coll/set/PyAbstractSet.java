package org.japy.kernel.types.coll.set;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.num.PyBool.True;

import java.util.function.Consumer;

import com.google.errorprone.annotations.Var;

import org.japy.infra.coll.ctx.CtxAwareHashSet;
import org.japy.infra.coll.ctx.CtxAwareSet;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.IPyContains;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.iter.PyJavaIter;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.proto.IPyObjWithLen;

public abstract class PyAbstractSet implements IPyROSet {
  protected CtxAwareSet<IPyObj, PyContext> content;

  public PyAbstractSet() {
    content = new CtxAwareHashSet<>();
  }

  protected PyAbstractSet(CtxAwareSet<IPyObj, PyContext> content) {
    this.content = content;
  }

  public PyAbstractSet(int expectedSize) {
    content = CtxAwareHashSet.withExpectedSize(expectedSize);
  }

  public PyAbstractSet(IPyObj[] elements, PyContext ctx) {
    content = CtxAwareHashSet.withExpectedSize(elements.length);
    for (IPyObj v : elements) {
      content.add(v, ctx);
    }
  }

  protected static CtxAwareSet<IPyObj, PyContext> setFromIterable(IPyObj iterable, PyContext ctx) {
    // TODO len, len hint
    CtxAwareSet<IPyObj, PyContext> set = new CtxAwareHashSet<>();
    CollLib.forEach(iterable, o -> set.add(o, ctx), ctx);
    return set;
  }

  public void forEachNoCtx(Consumer<? super IPyObj> func) {
    content.forEach(func);
  }

  public int len() {
    return content.size();
  }

  @Override
  public int len(PyContext ctx) {
    return content.size();
  }

  @Override
  public IPyObj __eq__(IPyObj other, PyContext ctx) {
    if (this == other) {
      return True;
    }
    if (!(other instanceof PyAbstractSet)) {
      return NotImplemented;
    }
    return PyBool.of(content.equals(((PyAbstractSet) other).content, ctx));
  }

  private boolean isSubSet(IPyContains coll, PyContext ctx) {
    if (coll instanceof IPyObjWithLen && ((IPyObjWithLen) coll).len(ctx) < len()) {
      return false;
    }
    for (IPyObj obj : content) {
      if (!coll.contains(obj, ctx)) {
        return false;
      }
    }
    return true;
  }

  private boolean isSubsetOfIterable(IPyObj iterable, PyContext ctx) {
    for (IPyObj obj : content) {
      if (!CollLib.contains(iterable, obj, ctx)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isSubSet(IPyObj coll, PyContext ctx) {
    if (coll instanceof PyAbstractSet) {
      return content.isSubSet(((PyAbstractSet) coll).content, ctx);
    } else if (coll instanceof IPyContains) {
      return isSubSet((IPyContains) coll, ctx);
    } else {
      return isSubsetOfIterable(coll, ctx);
    }
  }

  private boolean isSuperSetOfIterable(IPyObj coll, PyContext ctx) {
    for (IPyObj elem : new JavaIterable(coll, ctx)) {
      if (!contains(elem, ctx)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isSuperSet(IPyObj coll, PyContext ctx) {
    if (coll instanceof PyAbstractSet) {
      return ((PyAbstractSet) coll).content.isSubSet(content, ctx);
    } else if (coll instanceof IPyObjWithLen && ((IPyObjWithLen) coll).len(ctx) > len()) {
      return false;
    } else {
      return isSuperSetOfIterable(coll, ctx);
    }
  }

  @Override
  public boolean isDisjoint(IPyObj coll, PyContext ctx) {
    if (coll instanceof PyAbstractSet) {
      return content.isDisjoint(((PyAbstractSet) coll).content, ctx);
    }
    for (IPyObj elem : new JavaIterable(coll, ctx)) {
      if (contains(elem, ctx)) {
        return false;
      }
    }
    for (IPyObj elem : content) {
      if (CollLib.contains(coll, elem, ctx)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public IPyIter iter(PyContext ctx) {
    return new PyJavaIter(content);
  }

  @Override
  public boolean contains(IPyObj item, PyContext ctx) {
    try {
      return content.contains(item, ctx);
    } catch (PxException ex) {
      if (!(item instanceof PySet) || !ex.is(PyBuiltinExc.TypeError)) {
        throw ex;
      }

      // TODO WHAT???
      return contains(new PyFrozenSet(item, ctx), ctx);
    }
  }

  @Override
  public void validate(Validator v) {
    content.forEach(v::validate);
  }

  protected abstract PyAbstractSet instantiate(CtxAwareSet<IPyObj, PyContext> content);

  @Override
  public IPyObj union(IPyObj[] others, PyContext ctx) {
    PyAbstractSet union = (PyAbstractSet) copy(ctx);
    for (IPyObj other : others) {
      for (IPyObj elem : new JavaIterable(other, ctx)) {
        union.content.add(elem, ctx);
      }
    }
    return union;
  }

  private boolean isInAny(IPyObj obj, IPyObj[] collections, PyContext ctx) {
    for (IPyObj coll : collections) {
      if (CollLib.contains(coll, obj, ctx)) {
        return true;
      }
    }
    return false;
  }

  private CtxAwareSet<IPyObj, PyContext> intersection(
      @Var CtxAwareSet<IPyObj, PyContext> set1,
      @Var CtxAwareSet<IPyObj, PyContext> set2,
      PyContext ctx) {
    CtxAwareSet<IPyObj, PyContext> intersection = new CtxAwareHashSet<>();
    if (set1.size() > set2.size()) {
      CtxAwareSet<IPyObj, PyContext> tmp = set1;
      set1 = set2;
      set2 = tmp;
    }
    for (IPyObj obj : set1) {
      if (set2.contains(obj, ctx)) {
        intersection.add(obj, ctx);
      }
    }
    return intersection;
  }

  protected CtxAwareSet<IPyObj, PyContext> intersection(
      CtxAwareSet<IPyObj, PyContext> set, IPyObj other, PyContext ctx) {
    if (other instanceof PyAbstractSet) {
      return intersection(set, ((PyAbstractSet) other).content, ctx);
    }

    CtxAwareSet<IPyObj, PyContext> intersection = new CtxAwareHashSet<>();
    CollLib.forEach(
        other,
        obj -> {
          if (contains(obj, ctx)) {
            intersection.add(obj, ctx);
          }
        },
        ctx);

    return intersection;
  }

  @Override
  public IPyObj intersection(IPyObj[] others, PyContext ctx) {
    @Var CtxAwareSet<IPyObj, PyContext> intersection = content;
    for (IPyObj other : others) {
      intersection = intersection(intersection, other, ctx);
    }
    return instantiate(intersection);
  }

  protected static void differenceUpdate(
      CtxAwareSet<IPyObj, PyContext> set, IPyObj other, PyContext ctx) {
    CollLib.forEach(other, o -> set.remove(o, ctx), ctx);
  }

  protected static CtxAwareSet<IPyObj, PyContext> difference(
      CtxAwareSet<IPyObj, PyContext> set, IPyObj other, PyContext ctx) {
    CtxAwareSet<IPyObj, PyContext> diff = new CtxAwareHashSet<>(set, ctx);
    differenceUpdate(diff, other, ctx);
    return diff;
  }

  @Override
  public IPyObj difference(IPyObj[] others, PyContext ctx) {
    CtxAwareSet<IPyObj, PyContext> diff = new CtxAwareHashSet<>(content, ctx);
    for (IPyObj other : others) {
      differenceUpdate(diff, other, ctx);
    }
    return instantiate(diff);
  }

  protected static CtxAwareSet<IPyObj, PyContext> symmetricDifference(
      CtxAwareSet<IPyObj, PyContext> set, CtxAwareSet<IPyObj, PyContext> other, PyContext ctx) {
    CtxAwareSet<IPyObj, PyContext> diff = new CtxAwareHashSet<>(other, ctx);
    for (IPyObj obj : set) {
      if (!diff.remove(obj, ctx)) {
        diff.add(obj, ctx);
      }
    }
    return diff;
  }

  protected static CtxAwareSet<IPyObj, PyContext> symmetricDifference(
      CtxAwareSet<IPyObj, PyContext> set, IPyRODict other, PyContext ctx) {
    CtxAwareSet<IPyObj, PyContext> diff = new CtxAwareHashSet<>(set, ctx);
    other.forEach(
        (k, v) -> {
          if (!diff.remove(k, ctx)) {
            diff.add(k, ctx);
          }
        },
        ctx);
    return diff;
  }

  protected static void symmetricDifferenceUpdate(
      CtxAwareSet<IPyObj, PyContext> set, CtxAwareSet<IPyObj, PyContext> other, PyContext ctx) {
    for (IPyObj obj : other) {
      if (!set.remove(obj, ctx)) {
        set.add(obj, ctx);
      }
    }
  }

  protected static CtxAwareSet<IPyObj, PyContext> symmetricDifference(
      CtxAwareSet<IPyObj, PyContext> set, IPyObj other, PyContext ctx) {
    if (other instanceof IPyRODict) {
      return symmetricDifference(set, (IPyRODict) other, ctx);
    } else if (other instanceof PyAbstractSet) {
      return symmetricDifference(set, ((PyAbstractSet) other).content, ctx);
    }

    CtxAwareSet<IPyObj, PyContext> diff = setFromIterable(other, ctx);
    symmetricDifferenceUpdate(diff, set, ctx);
    return diff;
  }

  @Override
  public IPyObj symmetricDifference(IPyObj other, PyContext ctx) {
    return instantiate(symmetricDifference(content, other, ctx));
  }
}
