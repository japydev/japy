package org.japy.kernel.types.coll.str;

import org.eclipse.collections.impl.list.mutable.primitive.ByteArrayList;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.iter.IPyIterable;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxOverflowError;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.proto.IPyObjWithLen;

class BytesLib {
  static IPyObj toObj(byte b) {
    return PyInt.get(b & 0xFF);
  }

  static byte[] toBytes(IPyObj obj, PyContext ctx) {
    if (obj.type() == PyBytes.TYPE) {
      return ((PyBytes) obj).content;
    }
    if (obj.type() == PyByteArray.TYPE) {
      return ((PyByteArray) obj).content.toArray();
    }
    if (obj.type() == PyInt.TYPE) {
      PyInt len = (PyInt) obj;
      if (!len.isSmall()) {
        throw new PxOverflowError("byte array size is too large", ctx);
      }
      int intLen = len.intValue();
      if (intLen < 0) {
        throw PxException.valueError("negative byte array size", ctx);
      }
      return new byte[intLen];
    }

    if (obj instanceof IPyIterable) {
      return bytesFromIterable((IPyIterable) obj, ctx);
    }

    try {
      ByteArrayList array = new ByteArrayList();
      CollLib.forEach(obj, (elem) -> array.add(byteFromObj(elem, ctx)), ctx);
      return array.toArray();
    } catch (PxException ex) {
      if (ex.is(PyBuiltinExc.TypeError)) {
        throw PxException.typeError(
            String.format("cannot convert '%s' to bytes", obj.typeName()), ctx);
      }
      throw ex;
    }
  }

  private static byte byteFromObj(IPyObj obj, PyContext ctx) {
    if (!(obj instanceof PyInt)) {
      throw PxException.typeError(String.format("expected an int, got %s", obj.typeName()), ctx);
    }
    if (!((PyInt) obj).isSmall()) {
      throw PxException.typeError(
          String.format("expected an int in range [0..255], got %s", obj), ctx);
    }
    int v = ((PyInt) obj).intValue();
    if (v < 0 || v >= 256) {
      throw PxException.typeError(
          String.format("expected an int in range [0..255], got %s", obj), ctx);
    }
    return (byte) v;
  }

  private static byte[] bytesFromIterable(IPyIterable obj, PyContext ctx) {
    ByteArrayList array;
    if (obj instanceof IPyObjWithLen) {
      array = new ByteArrayList(((IPyObjWithLen) obj).len(ctx));
    } else {
      array = new ByteArrayList();
    }
    obj.forEach(elem -> array.add(byteFromObj(elem, ctx)), ctx);
    return array.toArray();
  }

  private static class ByteArrayCharSource implements CharSource {
    private final PyByteArray bytes;

    private ByteArrayCharSource(PyByteArray bytes) {
      this.bytes = bytes;
    }

    @Override
    public Kind kind() {
      return Kind.BYTES;
    }

    @Override
    public int len() {
      return bytes.len();
    }

    @Override
    public int getChar(int index) {
      return bytes.getByte(index) & 0xFF;
    }
  }

  private static class BytesCharSource implements CharSource {
    private final PyBytes bytes;

    private BytesCharSource(PyBytes bytes) {
      this.bytes = bytes;
    }

    @Override
    public Kind kind() {
      return Kind.BYTES;
    }

    @Override
    public int len() {
      return bytes.len();
    }

    @Override
    public int getChar(int index) {
      return bytes.getByte(index) & 0xFF;
    }
  }

  static CharSource charSource(PyByteArray bytes) {
    return new ByteArrayCharSource(bytes);
  }

  static CharSource charSource(PyBytes bytes) {
    return new BytesCharSource(bytes);
  }
}
