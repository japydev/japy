package org.japy.kernel.types.coll.dict;

import org.japy.kernel.types.obj.proto.IPyUnhashableObj;

public interface IPyDict extends IPyRODict, IPyMapping, IPyUnhashableObj {}
