package org.japy.kernel.types.coll.iter;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.util.Args;

class PyMapIter implements IPyIter, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "mapiter", PyMapIter.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private final IPyObj func;
  private final IPyObj[] iters;

  PyMapIter(IPyObj func, PyTuple iterables, PyContext ctx) {
    this.func = func;
    iters = new IPyObj[iterables.len()];
    for (int i = 0; i != iters.length; ++i) {
      iters[i] = IterLib.iter(iterables.get(i), ctx);
    }
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj next(PyContext ctx) {
    Args.Builder b = Args.builder(iters.length, 0, ctx);
    for (IPyObj iter : iters) {
      b.add(IterLib.next(iter, ctx));
    }
    return FuncLib.call(func, b.build(), ctx);
  }
}
