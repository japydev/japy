package org.japy.kernel.types.coll.str;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.iter.IPyIterable;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.set.PyFrozenSet;
import org.japy.kernel.types.coll.set.PySet;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.proto.IPyObjWithAscii;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.types.obj.proto.IPyObjWithStr;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;
import org.japy.kernel.util.PyConstants;

public class PrintLib {
  public static PxException throwReprUnsupported(IPyObj obj, PyContext ctx) {
    throw PxException.typeError(String.format("'%s' objects have no repr()", obj.typeName()), ctx);
  }

  public static PxException throwStrUnsupported(IPyObj obj, PyContext ctx) {
    throw PxException.typeError(String.format("'%s' objects have no str()", obj.typeName()), ctx);
  }

  public static PxException throwAsciiUnsupported(IPyObj obj, PyContext ctx) {
    throw PxException.typeError(String.format("'%s' objects have no ascii()", obj.typeName()), ctx);
  }

  private static PyStr checkString(
      IPyObj value, IPyObj convertedObj, String method, PyContext ctx) {
    if (value.type() == PyStr.TYPE) {
      return (PyStr) value;
    }

    if (value instanceof PyStr) {
      // TODO warning
      return new PyStr((PyStr) value);
    }

    throw PxException.typeError(
        String.format(
            "%s.%s() returned an object of type '%s', expected a string",
            convertedObj.typeName(), method, value.typeName()),
        ctx);
  }

  public static PyStr str(IPyObj obj, PyContext ctx) {
    if (obj.type() == PyStr.TYPE) {
      return (PyStr) obj;
    }

    if (obj instanceof PyStr) {
      return new PyStr((PyStr) obj);
    }

    if (obj instanceof IPyObjWithStr) {
      return ((IPyObjWithStr) obj).str(ctx);
    }

    @Nullable FuncHandle str = obj.specialMethod(SpecialMethod.STR);
    if (str == null) {
      throw throwStrUnsupported(obj, ctx);
    }

    try {
      return checkString((IPyObj) str.handle.invokeExact(obj, ctx), obj, Constants.__STR__, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  public static PyStr repr(IPyObj obj, PyContext ctx) {
    if (obj instanceof IPyObjWithRepr) {
      return ((IPyObjWithRepr) obj).repr(ctx);
    }

    @Nullable FuncHandle repr = obj.specialMethod(SpecialMethod.REPR);
    if (repr == null) {
      throw throwReprUnsupported(obj, ctx);
    }

    try {
      return checkString((IPyObj) repr.handle.invokeExact(obj, ctx), obj, Constants.__REPR__, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  public static PyStr ascii(IPyObj obj, PyContext ctx) {
    if (obj instanceof IPyObjWithAscii) {
      return ((IPyObjWithAscii) obj).ascii(ctx);
    }

    @Nullable FuncHandle ascii = obj.specialMethod(SpecialMethod.ASCII);
    if (ascii == null) {
      throw throwAsciiUnsupported(obj, ctx);
    }

    try {
      return checkString(
          (IPyObj) ascii.handle.invokeExact(obj, ctx), obj, Constants.__ASCII__, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  private static final ArgParser ARG_PARSER_STR_THREE_ARGS =
      ArgParser.builder(3)
          .add("object", PyBytes.EMPTY)
          .add("encoding", PyConstants.UTF_8)
          .add("errors", PyConstants.STRICT)
          .build();

  public static PyStr str(Args args, PyContext ctx) {
    if (args.isEmpty()) {
      return PyStr.EMPTY_STRING;
    }
    if (args.posCount() == 1 && args.kwCount() == 0) {
      return str(args.getPos(0), ctx);
    }
    if (args.posCount() == 0 && args.kwCount() == 1) {
      @Nullable IPyObj obj = args.getKwOrNull("object");
      if (obj != null) {
        return str(obj, ctx);
      }
    }

    IPyObj[] argVals = ARG_PARSER_STR_THREE_ARGS.parse(args, "str", ctx);
    return EncodeLib.decode(
        argVals[0],
        ArgLib.checkArgStr("str", "encoding", argVals[1], ctx),
        ArgLib.checkArgStr("str", "errors", argVals[2], ctx),
        ctx);
  }

  private static void prependComma(PyStrBuilder sb, MutableBoolean first) {
    if (!first.booleanValue()) {
      sb.appendUcs2(", ");
    } else {
      first.setFalse();
    }
  }

  private static void printDictContent(
      IPyDict dict, PyStr sep, boolean keyRepr, DeepRepr dr, PyContext ctx) {
    MutableBoolean first = new MutableBoolean(true);
    dict.forEach(
        (k, v) -> {
          prependComma(dr.sb, first);
          if (keyRepr) {
            dr.append(k, ctx);
          } else {
            dr.sb.append((PyStr) k);
          }
          dr.sb.append(sep);
          dr.append(v, ctx);
        },
        ctx);
  }

  public static void printDictContent(
      IPyDict dict, PyStr sep, boolean keyRepr, PyStrBuilder sb, PyContext ctx) {
    printDictContent(dict, sep, keyRepr, new DeepRepr(sb), ctx);
  }

  public static void printDict(IPyDict dict, DeepRepr dr, PyContext ctx) {
    dr.sb.appendUcs2("{");
    printDictContent(dict, PyConstants.COLONSPACE, true, dr, ctx);
    dr.sb.appendUcs2("}");
  }

  public static PyStr strRepr(PyStr str) {
    // TODO zzz
    return PyStr.get("'" + str + "'");
  }

  private enum SeqType {
    LIST("[", "]"),
    TUPLE("(", ")"),
    SET("{", "}"),
    FROZENSET("frozenset(", ")"),
    ;

    public final String open;
    public final String close;

    SeqType(String open, String close) {
      this.open = open;
      this.close = close;
    }
  }

  private static void printSeq(IPyIterable seq, SeqType seqType, DeepRepr dr, PyContext ctx) {
    dr.sb.appendUcs2(seqType.open);

    MutableInt count = new MutableInt(0);
    seq.forEach(
        v -> {
          if (count.intValue() != 0) {
            dr.sb.appendUcs2(", ");
          }
          dr.append(v, ctx);
          count.increment();
        },
        ctx);

    if (seqType == SeqType.TUPLE && count.intValue() == 1) {
      dr.sb.appendUcs2(",");
    }

    dr.sb.appendUcs2(seqType.close);
  }

  public static void printList(PyList list, DeepRepr dr, PyContext ctx) {
    printSeq(list, SeqType.LIST, dr, ctx);
  }

  public static void printTuple(PyTuple tuple, DeepRepr dr, PyContext ctx) {
    printSeq(tuple, SeqType.TUPLE, dr, ctx);
  }

  public static void printSet(PySet set, DeepRepr dr, PyContext ctx) {
    printSeq(set, SeqType.SET, dr, ctx);
  }

  public static void printFrozenSet(PyFrozenSet set, DeepRepr dr, PyContext ctx) {
    printSeq(set, SeqType.FROZENSET, dr, ctx);
  }
}
