package org.japy.kernel.types.coll.dict;

import static org.japy.infra.util.ObjUtil.or;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;

import java.util.Map;
import java.util.function.BiConsumer;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.coll.ctx.CtxAwareLinkedMap;
import org.japy.infra.coll.ctx.CtxAwareMap;
import org.japy.infra.coll.ctx.CtxAwareSet;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.ClassMethod;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.iter.PyJavaTransformingIter;
import org.japy.kernel.types.coll.seq.IPyROSequence;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.DeepRepr;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxKeyError;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.proto.IPyObjWithEq;
import org.japy.kernel.types.obj.proto.IPyUnhashableObj;
import org.japy.kernel.util.Args;

public class PyDict implements IPyDict, IPyUnhashableObj, IPyObjWithEq {
  public static final PyBuiltinClass TYPE =
      new PyBuiltinClass("dict", PyDict.class, PyBuiltinClass.REGULAR);

  private final CtxAwareMap<IPyObj, IPyObj, PyContext> content;

  public PyDict() {
    content = new CtxAwareLinkedMap<>();
  }

  public PyDict(int expectedSize) {
    content = CtxAwareLinkedMap.withExpectedSize(expectedSize);
  }

  public PyDict(IPyROMapping dict, PyContext ctx) {
    this(dict.len(ctx));
    dict.forEach((k, v) -> content.put(k, v, ctx), ctx);
  }

  @Constructor
  @SubClassConstructor
  public PyDict(Args args, PyContext ctx) {
    if (args.isEmpty()) {
      content = new CtxAwareLinkedMap<>();
      return;
    }

    if (args.posCount() == 0) {
      content = CtxAwareLinkedMap.withExpectedSize(CtxAwareSet.capacity(args.kwCount()));
    } else if (args.posCount() == 1) {
      content = makeMap(args.getPos(0), args.kwCount(), ctx);
    } else {
      throw PxException.typeError("too many positional arguments, zero or one expected", ctx);
    }

    args.forEachKw((k, v) -> content.put(PyStr.get(k), v, ctx));
  }

  private static CtxAwareMap<IPyObj, IPyObj, PyContext> makeMap(
      IPyObj obj, int extra, PyContext ctx) {
    CtxAwareMap<IPyObj, IPyObj, PyContext> map;
    if (obj instanceof IPyROMapping) {
      map = CtxAwareLinkedMap.withExpectedSize(extra + ((IPyROMapping) obj).len(ctx));
      ((IPyROMapping) obj).forEach((k, v) -> map.put(k, v, ctx), ctx);
    } else if (obj instanceof IPyROSequence) {
      map = CtxAwareLinkedMap.withExpectedSize(extra + ((IPyROSequence) obj).len(ctx));
      CollLib.dictForEach(obj, (k, v) -> map.put(k, v, ctx), ctx);
    } else {
      map = CtxAwareLinkedMap.withExpectedSize(extra);
      CollLib.dictForEach(obj, (k, v) -> map.put(k, v, ctx), ctx);
    }
    return map;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @ClassMethod
  public static IPyObj fromkeys(
      IPyClass cls,
      @Param("iterable") IPyObj iterable,
      @Param(value = "value", dflt = DEFAULT_NONE) IPyObj value,
      PyContext ctx) {
    PyDict dict = (PyDict) FuncLib.call(cls, ctx);
    for (IPyObj key : new JavaIterable(iterable, ctx)) {
      dict.setItem(key, value, ctx);
    }
    return dict;
  }

  @Override
  public void print(DeepRepr dr, PyContext ctx) {
    PrintLib.printDict(this, dr, ctx);
  }

  @Override
  public void validate(Validator v) {
    content.forEach(
        (key, value) -> {
          v.validate(key);
          v.validate(value);
        });
  }

  @Override
  public boolean contains(IPyObj item, PyContext ctx) {
    return content.containsKey(item, ctx);
  }

  @Override
  public void setItem(IPyObj key, IPyObj value, PyContext ctx) {
    content.put(key, value, ctx);
  }

  @Override
  public void delItem(IPyObj key, PyContext ctx) {
    if (content.remove(key, ctx) == null) {
      throw new PxKeyError(key, ctx);
    }
  }

  @Override
  public IPyObj pop(IPyObj key, PyContext ctx) {
    @Nullable IPyObj value = content.remove(key, ctx);
    if (value == null) {
      throw new PxKeyError(key, ctx);
    }
    return value;
  }

  @Override
  public IPyObj pop(IPyObj key, IPyObj dflt, PyContext ctx) {
    return or(content.remove(key, ctx), dflt);
  }

  @Override
  public void _fastForEach(BiConsumer<? super IPyObj, ? super IPyObj> func) {
    content.forEach(func);
  }

  public void forEach(BiConsumer<? super IPyObj, ? super IPyObj> func) {
    content.forEach(func);
  }

  @Override
  public IPyObj getItem(IPyObj key, PyContext ctx) {
    @Nullable IPyObj value = content.get(key, ctx);
    if (value == null) {
      throw new PxKeyError(key, ctx);
    }
    return value;
  }

  @Override
  public @Nullable IPyObj getOrNull(IPyObj key, PyContext ctx) {
    return content.get(key, ctx);
  }

  @Override
  public IPyObj items(PyContext ctx) {
    return new PyJavaTransformingIter<>(
        content.entryIterator(), e -> PyTuple.of(e.getKey(), e.getValue()));
  }

  @Override
  public IPyObj values(PyContext ctx) {
    return new PyJavaTransformingIter<>(content.entryIterator(), Map.Entry::getValue);
  }

  @Override
  public IPyIter iter(PyContext ctx) {
    return new PyJavaTransformingIter<>(content.entryIterator(), Map.Entry::getKey);
  }

  @Override
  public int len(PyContext ctx) {
    return content.size();
  }

  public int len() {
    return content.size();
  }

  public void extend(IPyObj dict, PyContext ctx) {
    CollLib.dictForEach(dict, (k, v) -> setItem(k, v, ctx), ctx);
  }

  @Override
  public IPyObj __eq__(IPyObj other, PyContext ctx) {
    if (!(other instanceof PyDict)) {
      return NotImplemented;
    }
    return PyBool.of(content.equals(((PyDict) other).content, ctx));
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    return new PyDict(this, ctx);
  }
}
