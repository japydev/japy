package org.japy.kernel.types.coll.iter;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.exc.px.PxStopIteration;

public class JavaIterator implements Iterator<IPyObj> {
  private final IPyObj iter;
  private final PyContext ctx;
  private @Nullable IPyObj next;

  public JavaIterator(IPyObj iter, PyContext ctx) {
    this.iter = iter;
    this.ctx = ctx;
  }

  @Override
  public boolean hasNext() {
    if (next == null) {
      try {
        next = IterLib.next(iter, ctx);
      } catch (PxStopIteration ignored) {
        //
      }
    }
    return next != null;
  }

  @Override
  @SuppressWarnings("UnusedException")
  public IPyObj next() {
    if (next == null) {
      try {
        next = IterLib.next(iter, ctx);
      } catch (PxStopIteration ignored) {
        throw new NoSuchElementException();
      }
    }
    IPyObj t = next;
    next = null;
    return t;
  }
}
