package org.japy.kernel.types.coll.seq;

import static org.japy.infra.validation.Debug.dcheck;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;

import com.google.errorprone.annotations.Var;

import org.japy.infra.validation.Debug;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;

public class PyNamedTupleClass extends PyBuiltinClass {
  protected final Map<String, Integer> fieldMap;

  public PyNamedTupleClass(String name, String... fields) {
    super(name, PyTuple.TYPE, PyNamedTuple.class, MethodHandles.lookup(), CALLABLE);
    this.fieldMap = new HashMap<>();
    @Var int index = 0;
    for (String field : fields) {
      fieldMap.put(field, index++);
      if (Debug.ENABLED) {
        dcheck(!classDict().contains(field));
      }
    }
  }

  public int len() {
    return fieldMap.size();
  }
}
