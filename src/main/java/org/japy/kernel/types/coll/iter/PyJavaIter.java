package org.japy.kernel.types.coll.iter;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.obj.IPyNoValidationObj;

public class PyJavaIter implements IPyIter, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass("jiter", PyJavaIter.class, PyBuiltinClass.INTERNAL);

  private final Iterator<IPyObj> iter;

  public PyJavaIter(Iterable<IPyObj> coll) {
    this(coll.iterator());
  }

  public PyJavaIter(Iterator<IPyObj> iter) {
    this.iter = iter;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  @SuppressWarnings("UnusedException")
  public IPyObj next(PyContext ctx) {
    try {
      return iter.next();
    } catch (NoSuchElementException ignored) {
      throw new PxStopIteration(ctx);
    }
  }
}
