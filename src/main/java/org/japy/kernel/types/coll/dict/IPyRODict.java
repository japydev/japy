package org.japy.kernel.types.coll.dict;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;

public interface IPyRODict extends IPyROMapping {
  @InstanceMethod
  IPyObj copy(PyContext ctx);
}
