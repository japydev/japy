package org.japy.kernel.types.coll.seq;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;

import java.util.function.Consumer;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.iter.IPyIterable;
import org.japy.kernel.types.coll.iter.IterLib;
import org.japy.kernel.types.coll.iter.PySeqIter;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxOverflowError;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.num.IntLib;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.proto.IPyObjWithLen;

public interface IPyROSequence extends IPyObjWithLen, IPyIterable {
  IPyObj get(int index, PyContext ctx);

  IPyObj slice(IntSlice slice, PyContext ctx);

  default IPyIter sliceIter(IntSlice slice, PyContext ctx) {
    return IterLib.seqSliceIter(this, slice, ctx);
  }

  boolean _canConcat(IPyObj other);

  IPyObj _concat(IPyROSequence seq, PyContext ctx);

  IPyObj _repeat(int times, PyContext ctx);

  IPyObj makeCopy(PyContext ctx);

  IPyObj makeEmpty(PyContext ctx);

  default int index(IPyObj elem, int from, int to, PyContext ctx) {
    for (int i = from; i != to; ++i) {
      IPyObj obj = get(i, ctx);
      if (obj == elem || obj.isEqual(elem, ctx)) {
        return i;
      }
    }
    throw PxException.valueError(
        String.format("%s not in the sequence", PrintLib.repr(elem, ctx)), ctx);
  }

  @SpecialInstanceMethod
  default IPyObj __getitem__(@Param("index") IPyObj index, PyContext ctx) {
    Object idx = CollLib.grokListIndex(index, ctx);
    if (idx instanceof Integer) {
      return get((int) idx, ctx);
    } else {
      return slice((IntSlice) idx, ctx);
    }
  }

  @InstanceMethod("index")
  default IPyObj __index(
      @Param("elem") IPyObj elem,
      @Param(value = "from", dflt = DEFAULT_SENTINEL) IPyObj fromArg,
      @Param(value = "to", dflt = DEFAULT_SENTINEL) IPyObj toArg,
      PyContext ctx) {
    int len = len(ctx);
    int from =
        fromArg == Sentinel
            ? 0
            : CollLib.checkIndexForInsert(
                ArgLib.checkArgSmallInt("index", "from", fromArg, ctx), len, ctx);
    int to =
        toArg == Sentinel
            ? len
            : CollLib.checkIndexForInsert(
                ArgLib.checkArgSmallInt("index", "to", toArg, ctx), len, ctx);
    return PyInt.get(index(elem, from, to, ctx));
  }

  @Override
  default void forEach(Consumer<? super IPyObj> func, PyContext ctx) {
    for (int i = 0, c = len(ctx); i != c; ++i) {
      func.accept(get(i, ctx));
    }
  }

  @Override
  default IPyIter iter(PyContext ctx) {
    return new PySeqIter(this, false, ctx);
  }

  @SpecialInstanceMethod
  default IPyObj __reversed__(PyContext ctx) {
    return new PySeqIter(this, true, ctx);
  }

  default IPyObj sum(IPyObj other, PyContext ctx) {
    if (!(other instanceof IPyROSequence) || !_canConcat(other)) {
      return NotImplemented;
    }
    IPyROSequence seq = (IPyROSequence) other;
    if (isEmpty(ctx)) {
      return seq.makeCopy(ctx);
    }
    if (seq.isEmpty(ctx)) {
      return makeCopy(ctx);
    }
    return _concat(seq, ctx);
  }

  default IPyObj mul(IPyObj other, PyContext ctx) {
    PyInt num;
    try {
      num = IntLib.asInt(other, ctx);
    } catch (PxException ex) {
      if (ex.is(PyBuiltinExc.TypeError)) {
        return NotImplemented;
      }
      throw ex;
    }

    if (!num.isSmall()) {
      throw new PxOverflowError("multiplier is too large", ctx);
    }

    int multiplier = num.intValue();
    if (multiplier < 0) {
      throw PxException.valueError("cannot repeat negative number of times", ctx);
    } else if (multiplier == 0) {
      return makeEmpty(ctx);
    } else if (multiplier == 1) {
      return this;
    }

    if (isEmpty(ctx)) {
      return this;
    }

    if (CollLib.checkMulOverflows(multiplier, len(ctx))) {
      throw new PxOverflowError("multiplier is too large", ctx);
    }

    return _repeat(multiplier, ctx);
  }

  @SpecialInstanceMethod
  default IPyObj __add__(@Param("other") IPyObj other, PyContext ctx) {
    return sum(other, ctx);
  }

  @SpecialInstanceMethod
  default IPyObj __mul__(@Param("other") IPyObj other, PyContext ctx) {
    return mul(other, ctx);
  }

  @SpecialInstanceMethod
  default IPyObj __rmul__(@Param("other") IPyObj other, PyContext ctx) {
    return mul(other, ctx);
  }
}
