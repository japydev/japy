package org.japy.kernel.types.coll.iter;

import java.util.function.Consumer;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;

public interface IPyIterable extends IPyObj {
  @SpecialInstanceMethod
  default IPyObj __iter__(PyContext ctx) {
    return iter(ctx);
  }

  IPyIter iter(PyContext ctx);

  default void forEach(Consumer<? super IPyObj> func, PyContext ctx) {
    iter(ctx).forEach(func, ctx);
  }
}
