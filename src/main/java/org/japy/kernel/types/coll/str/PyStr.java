package org.japy.kernel.types.coll.str;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_FALSE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NEG_ONE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.exc.px.PxException.typeError;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;
import static org.japy.kernel.util.PyUtil.wrap;

import java.util.Locale;
import java.util.function.Consumer;

import com.google.errorprone.annotations.Var;

import org.japy.base.ParamKind;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.IPyContains;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.seq.IPyImmSequence;
import org.japy.kernel.types.coll.seq.IPyROSequence;
import org.japy.kernel.types.coll.seq.IntSlice;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.seq.SliceIndices;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxLookupError;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.types.obj.proto.IPyObjWithComp;
import org.japy.kernel.types.obj.proto.IPyObjWithFormat;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.types.obj.proto.IPyObjWithStr;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;

public class PyStr
    implements IPyImmSequence,
        IPyIntHashableObj,
        IPyObjWithStr,
        IPyObjWithRepr,
        Comparable<PyStr>,
        IPyCharSource,
        IPyContains,
        IPyObjWithFormat,
        IPyObjWithComp,
        IPyNoValidationObj {
  public static final PyStr EMPTY_STRING = new PyStr();

  public static final IPyClass TYPE =
      new PyBuiltinClass(
          "str",
          PyStr.class,
          PyBuiltinClass.REGULAR,
          "str(object='') -> str\n"
              + "str(bytes_or_buffer[, encoding[, errors]]) -> str\n"
              + "\n"
              + "Create a new string object from the given object. If encoding or\n"
              + "errors is specified, then the object must expose a data buffer\n"
              + "that will be decoded using the given encoding and error handler.\n"
              + "Otherwise, returns the result of object.__str__() (if defined)\n"
              + "or repr(object).\n"
              + "encoding defaults to sys.getdefaultencoding().\n"
              + "errors defaults to 'strict'.");

  private static final int MASK_UTF32 = 0x8000_0000;

  private final Object data;
  private final int lenAndFlags;

  private PyStr() {
    this("");
  }

  protected PyStr(String s) {
    this.data = s;
    this.lenAndFlags = s.length();
  }

  protected PyStr(Utf32String u) {
    this.data = u;
    this.lenAndFlags = u.content.length | MASK_UTF32;
  }

  PyStr(PyStr s) {
    this.data = s.data;
    this.lenAndFlags = s.lenAndFlags;
  }

  @SubClassConstructor
  protected PyStr(Args args, PyContext ctx) {
    this(PrintLib.str(args, ctx));
  }

  @Constructor
  public static PyStr construct(Args args, PyContext ctx) {
    return PrintLib.str(args, ctx);
  }

  @Override
  public PyStr str(PyContext ctx) {
    return this;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  protected String s() {
    return (String) data;
  }

  protected Utf32String u() {
    return (Utf32String) data;
  }

  public static PyStr get(String text) {
    return UnicodeLib.isUCS2(text) ? new PyStr(text) : new PyStr(UnicodeLib.getUtf32String(text));
  }

  public static PyStr utf32(String text) {
    if (Debug.ENABLED) {
      dcheck(!UnicodeLib.isUCS2(text));
    }
    return new PyStr(UnicodeLib.getUtf32String(text));
  }

  public static PyStr ucs2(String text) {
    if (Debug.ENABLED) {
      dcheck(UnicodeLib.isUCS2(text));
    }
    return new PyStr(text);
  }

  public static PyStr ucs2(char[] text) {
    return ucs2(text, 0, text.length);
  }

  public static PyStr ucs2(char[] text, int start, int len) {
    if (Debug.ENABLED) {
      dcheck(UnicodeLib.isUCS2(text, start, len));
    }
    return len == 0 ? EMPTY_STRING : new PyStr(new String(text, start, len));
  }

  public boolean isUcs2() {
    return (lenAndFlags & MASK_UTF32) == 0;
  }

  @Override
  public UniCharSource asCharSource() {
    return StrData.charSource(this);
  }

  public boolean isEmpty() {
    return len() == 0;
  }

  public int len() {
    return lenAndFlags & ~MASK_UTF32;
  }

  @Override
  public int len(PyContext ctx) {
    return lenAndFlags & ~MASK_UTF32;
  }

  @Override
  public int intHashCode(PyContext ctx) {
    return data.hashCode();
  }

  @Override
  public IPyObj __eq__(IPyObj obj, PyContext ctx) {
    if (!(obj instanceof PyStr)) {
      return NotImplemented;
    }

    return PyBool.of(this.data.equals(((PyStr) obj).data));
  }

  @Override
  public String toString() {
    return data.toString();
  }

  public String getString() {
    return data.toString();
  }

  @Override
  public boolean equals(Object obj) {
    throw InternalErrorException.notReached("equals");
  }

  public boolean isEqual(PyStr str) {
    return this.data.equals(str.data);
  }

  @Override
  public int hashCode() {
    throw InternalErrorException.notReached("hashCode");
  }

  @SpecialInstanceMethod
  public IPyObj __mod__(@Param("obj") IPyObj obj, PyContext ctx) {
    return PrintfLib.printf(this, obj, ctx);
  }

  @InstanceMethod
  public IPyObj partition(@Param("sep") IPyObj sepObj, PyContext ctx) {
    PyStr pysep = ArgLib.checkArgStr("str.partition", "sep", sepObj, ctx);
    if (!isUcs2()) {
      throw new NotImplementedException("partition utf32");
    }
    String s = s();
    String sep = pysep.toString();
    int idx = s.indexOf(sep);
    if (idx >= 0) {
      return PyTuple.of(
          PyStr.ucs2(s.substring(0, idx)), sepObj, PyStr.ucs2(s.substring(idx + sep.length())));
    } else {
      return PyTuple.of(this, EMPTY_STRING, EMPTY_STRING);
    }
  }

  @InstanceMethod
  public IPyObj rpartition(@Param("sep") IPyObj sepObj, PyContext ctx) {
    PyStr pysep = ArgLib.checkArgStr("str.rpartition", "sep", sepObj, ctx);
    if (!isUcs2()) {
      throw new NotImplementedException("rpartition utf32");
    }
    String s = s();
    String sep = pysep.toString();
    int idx = s.lastIndexOf(sep);
    if (idx >= 0) {
      return PyTuple.of(
          PyStr.ucs2(s.substring(0, idx)), sepObj, PyStr.ucs2(s.substring(idx + sep.length())));
    } else {
      return PyTuple.of(EMPTY_STRING, EMPTY_STRING, this);
    }
  }

  @InstanceMethod
  public IPyObj format(Args args, PyContext ctx) {
    return StrFormatLib.format(this, args, ctx);
  }

  @InstanceMethod
  public IPyObj split(
      @Param(value = "sep", dflt = DEFAULT_NONE) IPyObj sep,
      @Param(value = "maxsplit", dflt = DEFAULT_NEG_ONE) IPyObj maxSplitObj,
      PyContext ctx) {
    int maxSplit = ArgLib.checkArgSmallInt("str.split", "maxsplit", maxSplitObj, ctx);
    if (sep != None) {
      return StrData.split(this, ArgLib.checkArgStr("str.split", "sep", sep, ctx), maxSplit, ctx);
    } else {
      return StrData.split(this, maxSplit);
    }
  }

  @InstanceMethod
  public IPyObj rsplit(
      @Param(value = "sep", dflt = DEFAULT_NONE) IPyObj sep,
      @Param(value = "maxsplit", dflt = DEFAULT_NEG_ONE) IPyObj maxSplitObj,
      PyContext ctx) {
    int maxSplit = ArgLib.checkArgSmallInt("str.rsplit", "maxsplit", maxSplitObj, ctx);
    if (sep != None) {
      return StrData.rsplit(this, ArgLib.checkArgStr("str.rsplit", "sep", sep, ctx), maxSplit);
    } else {
      return StrData.rsplit(this, maxSplit);
    }
  }

  @InstanceMethod
  public IPyObj join(@Param("iterable") IPyObj iterable, PyContext ctx) {
    PyStrBuilder sb = new PyStrBuilder();
    @Var boolean first = true;
    for (IPyObj obj : new JavaIterable(iterable, ctx)) {
      if (!(obj instanceof PyStr)) {
        throw typeError(
            String.format("in str.join: expected a string, got '%s'", obj.typeName()), ctx);
      }
      if (!first) {
        sb.append(this);
      }
      first = false;
      sb.append((PyStr) obj);
    }
    return sb.build();
  }

  @InstanceMethod
  public IPyObj startswith(@Param("prefix") IPyObj prefix, PyContext ctx) {
    if (prefix instanceof PyStr) {
      return PyBool.of(StrData.startsWith(this, (PyStr) prefix));
    }
    for (IPyObj obj : new JavaIterable(prefix, ctx)) {
      if (!(obj instanceof PyStr)) {
        throw typeError(
            String.format("in str.startswith: expected a string, got '%s'", obj.typeName()), ctx);
      }
      if (StrData.startsWith(this, (PyStr) obj)) {
        return True;
      }
    }
    return False;
  }

  @InstanceMethod
  public IPyObj endswith(@Param("suffix") IPyObj suffix, PyContext ctx) {
    if (suffix instanceof PyStr) {
      return PyBool.of(StrData.endsWith(this, (PyStr) suffix));
    }
    for (IPyObj obj : new JavaIterable(suffix, ctx)) {
      if (!(obj instanceof PyStr)) {
        throw typeError(
            String.format("in str.endswith: expected a string, got '%s'", obj.typeName()), ctx);
      }
      if (StrData.endsWith(this, (PyStr) obj)) {
        return True;
      }
    }
    return False;
  }

  @InstanceMethod
  public IPyObj lower(PyContext ctx) {
    // TODO zzz
    return get(toString().toLowerCase(Locale.ROOT));
  }

  @InstanceMethod
  public IPyObj upper(PyContext ctx) {
    // TODO zzz
    return get(toString().toUpperCase(Locale.ROOT));
  }

  @InstanceMethod
  public IPyObj islower(PyContext ctx) {
    // TODO zzz
    String s = toString();
    return wrap(s.toLowerCase(Locale.ROOT).equals(s));
  }

  @InstanceMethod
  public IPyObj isupper(PyContext ctx) {
    // TODO zzz
    String s = toString();
    return wrap(s.toUpperCase(Locale.ROOT).equals(s));
  }

  @InstanceMethod
  public IPyObj capitalize(PyContext ctx) {
    // TODO zzz
    String s = toString();
    if (s.isEmpty()) {
      return this;
    }
    return get(s.substring(0, 1).toUpperCase() + s.substring(1));
  }

  @Override
  public IPyObj get(int index, PyContext ctx) {
    int realIndex = CollLib.checkIndex(index, len(), ctx);
    if (isUcs2()) {
      return ucs2(s().substring(realIndex, realIndex + 1));
    }
    int codepoint = u().content[realIndex];
    if (Character.isBmpCodePoint(codepoint)) {
      return new PyStr(new Utf32String(new int[] {codepoint}));
    }
    return ucs2(Character.toString(codepoint));
  }

  @Override
  public IPyObj slice(IntSlice slice, PyContext ctx) {
    SliceIndices indices = slice.indices(len(), ctx);
    if (indices.isEmpty()) {
      return EMPTY_STRING;
    }
    if (indices.step == 1) {
      return StrData.slice(this, indices.start, indices.stop);
    }
    throw new NotImplementedException("slice");
  }

  @Override
  public boolean _canConcat(IPyObj other) {
    return other instanceof PyStr;
  }

  @Override
  public IPyObj _concat(IPyROSequence seq, PyContext ctx) {
    return StrData.concat(this, (PyStr) seq, ctx);
  }

  @Override
  public IPyObj _repeat(int times, PyContext ctx) {
    return StrData.repeat(this, times);
  }

  @Override
  public IPyObj makeEmpty(PyContext ctx) {
    return EMPTY_STRING;
  }

  @Override
  public void forEach(Consumer<? super IPyObj> func, PyContext ctx) {
    if (isUcs2()) {
      String s = s();
      for (int i = 0, c = len(); i != c; ++i) {
        func.accept(PyStr.ucs2(s.substring(i, i + 1)));
      }
    } else {
      for (int ch : u().content) {
        if (Character.isBmpCodePoint(ch)) {
          func.accept(PyStr.ucs2(Character.toString(ch)));
        } else {
          func.accept(new PyStr(new Utf32String(new int[] {ch})));
        }
      }
    }
  }

  @Override
  public int compareTo(PyStr other) {
    return StrData.compare(this, other);
  }

  @Override
  public PyStr repr(PyContext ctx) {
    return PrintLib.strRepr(this);
  }

  @InstanceMethod
  public IPyObj strip(@Param(value = "chars", dflt = DEFAULT_NONE) IPyObj chars, PyContext ctx) {
    if (chars != None) {
      PyStr charsStr = ArgLib.checkArgStr("str.strip", "chars", chars, ctx);
      return StrData.strip(this, charsStr);
    } else {
      return StrData.strip(this);
    }
  }

  @InstanceMethod
  public IPyObj rstrip(@Param(value = "chars", dflt = DEFAULT_NONE) IPyObj chars, PyContext ctx) {
    if (chars != None) {
      PyStr charsStr = ArgLib.checkArgStr("str.rstrip", "chars", chars, ctx);
      return StrData.rstrip(this, charsStr);
    } else {
      return StrData.rstrip(this);
    }
  }

  @InstanceMethod
  public IPyObj lstrip(@Param(value = "chars", dflt = DEFAULT_NONE) IPyObj chars, PyContext ctx) {
    if (chars != None) {
      PyStr charsStr = ArgLib.checkArgStr("str.lstrip", "chars", chars, ctx);
      return StrData.lstrip(this, charsStr);
    } else {
      return StrData.lstrip(this);
    }
  }

  @Override
  public boolean contains(IPyObj obj, PyContext ctx) {
    if (!(obj instanceof PyStr)) {
      throw typeError(
          String.format("'in <string>' requires string as left operand, not %s", obj.typeName()),
          ctx);
    }
    return StrData.contains(this, (PyStr) obj);
  }

  @InstanceMethod
  public IPyObj maketrans(Args args, PyContext ctx) {
    throw new NotImplementedException("maketrans");
  }

  @InstanceMethod
  public IPyObj isidentifier(PyContext ctx) {
    // TODO
    if (!isUcs2()) {
      throw new NotImplementedException("isidentifier");
    }
    String s = s();
    for (int i = 0, c = s.length(); i != c; ++i) {
      char ch = s.charAt(i);
      if (ch != '_'
          && !('a' <= ch && ch <= 'z')
          && !('A' <= ch && ch <= 'Z')
          && !(i != 0 && '0' <= ch && ch <= '9')) {
        return False;
      }
    }
    return True;
  }

  private IPyObj find(
      boolean first,
      String funcName,
      IPyObj subObj,
      IPyObj startObj,
      IPyObj endObj,
      PyContext ctx) {
    PyStr sub = ArgLib.checkArgStr(funcName, "sub", subObj, ctx);
    @Var
    int start =
        startObj != Sentinel ? ArgLib.checkArgSmallInt(funcName, "start", startObj, ctx) : 0;
    @Var
    int end = endObj != Sentinel ? ArgLib.checkArgSmallInt(funcName, "end", endObj, ctx) : len();
    if (start < 0) {
      start += len();
    }
    if (end < 0) {
      end += len();
    }
    start = Math.max(start, 0);
    end = Math.min(end, len());
    return wrap(StrData.find(first, this, sub, start, end));
  }

  @InstanceMethod
  public IPyObj find(
      @Param("sub") IPyObj subObj,
      @Param(value = "start", dflt = DEFAULT_SENTINEL) IPyObj startObj,
      @Param(value = "end", dflt = DEFAULT_SENTINEL) IPyObj endObj,
      PyContext ctx) {
    return find(true, "str.find", subObj, startObj, endObj, ctx);
  }

  @InstanceMethod
  public IPyObj rfind(
      @Param("sub") IPyObj subObj,
      @Param(value = "start", dflt = DEFAULT_SENTINEL) IPyObj startObj,
      @Param(value = "end", dflt = DEFAULT_SENTINEL) IPyObj endObj,
      PyContext ctx) {
    return find(false, "str.rfind", subObj, startObj, endObj, ctx);
  }

  @InstanceMethod
  public IPyObj replace(
      @Param("old") IPyObj oldObj,
      @Param("new") IPyObj replObj,
      @Param(value = "count", dflt = DEFAULT_SENTINEL) IPyObj countObj,
      PyContext ctx) {
    PyStr old = ArgLib.checkArgStr("str.replace", "old", oldObj, ctx);
    PyStr repl = ArgLib.checkArgStr("str.replace", "repl", replObj, ctx);
    int count =
        countObj != Sentinel
            ? ArgLib.checkArgSmallNonNegInt("str.replace", "count", countObj, ctx)
            : -1;
    return StrData.replace(this, old, repl, count, ctx);
  }

  @Override
  public IPyObj __lt__(@Param("other") IPyObj other, PyContext ctx) {
    if (other instanceof PyStr) {
      return PyBool.of(compareTo((PyStr) other) < 0);
    }
    return NotImplemented;
  }

  @Override
  public IPyObj __le__(@Param("other") IPyObj other, PyContext ctx) {
    if (other instanceof PyStr) {
      return PyBool.of(compareTo((PyStr) other) <= 0);
    }
    return NotImplemented;
  }

  @Override
  public IPyObj __gt__(@Param("other") IPyObj other, PyContext ctx) {
    if (other instanceof PyStr) {
      return PyBool.of(compareTo((PyStr) other) > 0);
    }
    return NotImplemented;
  }

  @Override
  public IPyObj __ge__(@Param("other") IPyObj other, PyContext ctx) {
    if (other instanceof PyStr) {
      return PyBool.of(compareTo((PyStr) other) >= 0);
    }
    return NotImplemented;
  }

  @InstanceMethod
  public IPyObj translate(@Param("table") IPyObj table, PyContext ctx) {
    int length = len();
    StringBuilder sb = new StringBuilder(length);
    for (int i = 0; i != length; ++i) {
      int cp = isUcs2() ? s().charAt(i) : u().content[i];
      try {
        IPyObj v = CollLib.getItem(table, PyInt.get(cp), ctx);
        if (v != None) {
          sb.append(v);
        }
      } catch (PxLookupError ignored) {
        sb.appendCodePoint(cp);
      }
    }
    return get(sb.toString());
  }

  @Override
  public PyStr format(ObjFormatLib.Spec spec, PyContext ctx) {
    if (spec.isEmpty()) {
      return this;
    }
    throw new NotImplementedException("format");
  }

  public IPyObj ord(PyContext ctx) {
    if (len() != 1) {
      throw PxException.valueError("char must be a string of length 1", ctx);
    }
    return PyInt.get(isUcs2() ? s().charAt(0) : u().content[0]);
  }

  @InstanceMethod
  public IPyObj encode(
      @Param(value = "encoding", defaultStr = Constants.UTF_8, kind = ParamKind.POS_OR_KW)
          IPyObj encodingArg,
      @Param(value = "errors", defaultStr = Constants.STRICT, kind = ParamKind.POS_OR_KW)
          IPyObj errorsArg,
      PyContext ctx) {
    PyStr encoding = ArgLib.checkArgStr("str.encode", "encoding", encodingArg, ctx);
    PyStr errors = ArgLib.checkArgStr("str.encode", "errors", errorsArg, ctx);
    return new PyBytes(EncodeLib.encode(this, encoding, errors, ctx));
  }

  @InstanceMethod
  public IPyObj splitlines(
      @Param(value = "keepends", kind = ParamKind.POS_OR_KW, dflt = DEFAULT_FALSE)
          IPyObj keepEndsArg,
      PyContext ctx) {
    boolean keepEnds = ArgLib.checkArgBool("str.splitlines", "keepends", keepEndsArg, ctx);
    return StrData.splitlines(this, keepEnds);
  }
}
