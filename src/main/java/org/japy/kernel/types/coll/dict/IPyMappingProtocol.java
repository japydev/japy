package org.japy.kernel.types.coll.dict;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.ext.adapters.IPyMappingProtocolAdapter;
import org.japy.kernel.util.Constants;

@Protocol(IPyMappingProtocolAdapter.class)
public interface IPyMappingProtocol extends IPyROMappingProtocol {
  @ProtocolMethod
  void setItem(IPyObj key, IPyObj value, PyContext ctx);

  @ProtocolMethod
  void delItem(IPyObj key, PyContext ctx);

  @ProtocolMethod
  IPyObj pop(IPyObj key, PyContext ctx);

  @ProtocolMethod
  IPyObj pop(IPyObj key, IPyObj dflt, PyContext ctx);

  @InstanceMethod
  default IPyObj setdefault(
      @Param("key") IPyObj key, @Param("default") IPyObj dflt, PyContext ctx) {
    @Nullable IPyObj value = getOrNull(key, ctx);
    if (value != null) {
      return value;
    }
    setItem(key, dflt, ctx);
    return dflt;
  }

  @InstanceMethod(Constants.POP)
  default IPyObj _pop(
      @Param("key") IPyObj key,
      @Param(value = "default", dflt = DEFAULT_SENTINEL) IPyObj dflt,
      PyContext ctx) {
    if (dflt != Sentinel) {
      return pop(key, dflt, ctx);
    } else {
      return pop(key, ctx);
    }
  }

  @SpecialInstanceMethod
  default IPyObj __setitem__(
      @Param("key") IPyObj key, @Param("value") IPyObj value, PyContext ctx) {
    setItem(key, value, ctx);
    return None;
  }

  @SpecialInstanceMethod
  default IPyObj __delitem__(@Param("key") IPyObj key, PyContext ctx) {
    delItem(key, ctx);
    return None;
  }
}
