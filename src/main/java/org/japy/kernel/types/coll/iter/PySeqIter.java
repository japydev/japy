package org.japy.kernel.types.coll.iter;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.seq.IPyROSequence;
import org.japy.kernel.types.exc.px.PxStopIteration;

public class PySeqIter implements IPyIter {
  private static final IPyClass TYPE =
      new PyBuiltinClass("seqiter", PySeqIter.class, PyBuiltinClass.INTERNAL);

  private final IPyROSequence seq;
  private final boolean reverse;
  private int next;

  public PySeqIter(IPyROSequence seq, boolean reverse, PyContext ctx) {
    this.seq = seq;
    this.reverse = reverse;
    this.next = reverse ? seq.len(ctx) - 1 : 0;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj next(PyContext ctx) {
    if (reverse) {
      if (next < 0) {
        throw new PxStopIteration(ctx);
      }
      return seq.get(next--, ctx);
    } else {
      if (next >= seq.len(ctx)) {
        throw new PxStopIteration(ctx);
      }
      return seq.get(next++, ctx);
    }
  }

  @Override
  public void validate(Validator v) {
    v.validate(seq);
  }
}
