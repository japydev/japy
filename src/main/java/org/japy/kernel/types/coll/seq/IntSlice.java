package org.japy.kernel.types.coll.seq;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.exc.px.PxException;

public class IntSlice {
  public static final IntSlice EMPTY = new IntSlice(0, 0, 1);

  public final @Nullable Integer start;
  public final @Nullable Integer end;
  public final @Nullable Integer step;

  public IntSlice(@Nullable Integer start, @Nullable Integer end, @Nullable Integer step) {
    this.start = start;
    this.end = end;
    this.step = step;
  }

  public SliceIndices indices(int length, PyContext ctx) {
    int istep = step != null ? step : 1;
    if (istep == 0) {
      throw PxException.valueError("slice step cannot be zero", ctx);
    }
    if (istep < 0) {
      throw new NotImplementedException("negative step");
    }
    @Var int istart = start != null ? start : 0;
    if (istart < 0) {
      istart += length;
    }
    if (istart < 0) {
      istart = 0;
    }
    if (istart >= length) {
      return SliceIndices.EMPTY;
    }
    @Var int iend = end != null ? end : length;
    if (iend < 0) {
      iend += length;
    }
    if (iend < 0) {
      iend = 0;
    } else if (iend > length) {
      iend = length;
    }
    if (istart >= iend) {
      return SliceIndices.EMPTY;
    }
    if (istep != 1) {
      throw new NotImplementedException("non-unit step");
    }
    return new SliceIndices(istart, iend, 1);
  }
}
