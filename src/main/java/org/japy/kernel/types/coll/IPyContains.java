package org.japy.kernel.types.coll;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.ext.adapters.IPyContainsAdapter;
import org.japy.kernel.types.num.PyBool;

@Protocol(IPyContainsAdapter.class)
public interface IPyContains extends IPyObj {
  @ProtocolMethod
  boolean contains(IPyObj item, PyContext ctx);

  @SpecialInstanceMethod
  default IPyObj __contains__(@Param("item") IPyObj item, PyContext ctx) {
    return PyBool.of(contains(item, ctx));
  }
}
