package org.japy.kernel.types.coll.str;

import static org.japy.infra.validation.Debug.dcheck;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.util.Constants;
import org.japy.kernel.util.PyConstants;
import org.japy.lib.misc._Codecs;

class EncodeLib {
  enum Errors {
    STRICT,
    IGNORE,
    REPLACE,
    XMLCHARREFREPLACE,
    BACKSLASHREPLACE,
    NAMEREPLACE,
    SURROGATEESCAPE,
    SURROGATEPASS,
    ;

    static @Nullable Errors get(PyStr name) {
      switch (name.toString()) {
        case Constants.STRICT:
          return STRICT;
        case Constants.IGNORE:
          return IGNORE;
        case Constants.REPLACE:
          return REPLACE;
        case Constants.XMLCHARREFREPLACE:
          return XMLCHARREFREPLACE;
        case Constants.BACKSLASHREPLACE:
          return BACKSLASHREPLACE;
        case Constants.NAMEREPLACE:
          return NAMEREPLACE;
        case Constants.SURROGATEESCAPE:
          return SURROGATEESCAPE;
        case Constants.SURROGATEPASS:
          return SURROGATEPASS;
        default:
          return null;
      }
    }
  }

  static PyStr decode(IPyObj obj, PyStr encoding, PyStr errors, PyContext ctx) {
    if (obj instanceof PyBytes) {
      return decode(((PyBytes) obj).content, encoding, errors, ctx);
    }
    throw new NotImplementedException("decode");
  }

  static PyStr decode(byte[] bytes, PyStr encoding, PyStr errorsStr, PyContext ctx) {
    if (encoding.isEqual(PyConstants.UTF_8)) {
      @Nullable Errors errors = Errors.get(errorsStr);
      if (errors != null) {
        return new Utf8Decoder(bytes, errors, ctx).decode();
      }
    }

    return decodeWithModule(bytes, encoding, errorsStr, ctx);
  }

  private static class Utf8Decoder {
    private final byte[] inputBytes;
    private final Errors errors;
    private final PyContext ctx;

    private Utf8Decoder(byte[] inputBytes, Errors errors, PyContext ctx) {
      this.inputBytes = inputBytes;
      this.errors = errors;
      this.ctx = ctx;
    }

    PyStr decode() {
      ByteBuffer input = ByteBuffer.wrap(inputBytes);
      CharsetDecoder decoder = makeDecoder();
      @Var CharBuffer output = CharBuffer.allocate(inputBytes.length);
      while (true) {
        CoderResult result = decoder.decode(input, output, true);
        if (result.isUnderflow()) {
          output.flip();
          return PyStr.ucs2(output.array(), 0, output.length());
        } else if (result.isOverflow()) {
          output = resizeOutput(output);
        } else if (result.isMalformed()) {
          throw new NotImplementedException("malformed input");
        } else if (result.isUnmappable()) {
          throw new NotImplementedException("unmappable input");
        }
      }
    }

    private CharBuffer resizeOutput(CharBuffer output) {
      int newCapacity = Math.min(output.capacity() + 64, (int) (output.capacity() * 1.7));
      CharBuffer newOutput = CharBuffer.allocate(newCapacity);
      output.flip();
      newOutput.put(output);
      return newOutput;
    }

    private CharsetDecoder makeDecoder() {
      CharsetDecoder decoder = StandardCharsets.UTF_8.newDecoder();
      switch (errors) {
        case STRICT:
        case XMLCHARREFREPLACE:
        case BACKSLASHREPLACE:
        case SURROGATEESCAPE:
        case SURROGATEPASS:
        case NAMEREPLACE:
          return decoder
              .onUnmappableCharacter(CodingErrorAction.REPORT)
              .onMalformedInput(CodingErrorAction.REPORT);
        case IGNORE:
          return decoder
              .onUnmappableCharacter(CodingErrorAction.IGNORE)
              .onMalformedInput(CodingErrorAction.IGNORE);
        case REPLACE:
          return decoder
              .onUnmappableCharacter(CodingErrorAction.REPLACE)
              .onMalformedInput(CodingErrorAction.REPLACE);
      }
      throw InternalErrorException.notReached();
    }
  }

  static byte[] encode(PyStr str, PyStr encoding, PyStr errorsStr, PyContext ctx) {
    if (encoding.isEqual(PyConstants.UTF_8)) {
      @Nullable Errors errors = Errors.get(errorsStr);
      if (errors != null) {
        return new Utf8Encoder(str, errors, ctx).encode();
      }
    }

    return encodeWithModule(str, encoding, errorsStr, ctx);
  }

  private static class Utf8Encoder {
    private final PyStr str;
    private final Errors errors;
    private final PyContext ctx;

    Utf8Encoder(PyStr str, Errors errors, PyContext ctx) {
      this.str = str;
      this.errors = errors;
      this.ctx = ctx;
    }

    byte[] encode() {
      CharsetEncoder encoder = makeEncoder();
      String inputString = str.toString();
      int inputLen = inputString.length();
      CharBuffer input = CharBuffer.wrap(inputString);
      // Optimistically assume ascii first
      @Var ByteBuffer output = ByteBuffer.allocate(inputLen);
      @Var boolean first = true;
      while (true) {
        CoderResult result = encoder.encode(input, output, true);
        if (result.isUnderflow()) {
          if (first) {
            dcheck(output.position() == inputLen);
            return output.array();
          } else {
            return bytesFromBuffer(output);
          }
        } else if (result.isOverflow()) {
          output = resizeOutput(output, inputLen);
          first = false;
        } else if (result.isUnmappable()) {
          throw new NotImplementedException("unmappable");
        } else {
          throw InternalErrorException.notReached();
        }
      }
    }

    private static ByteBuffer resizeOutput(ByteBuffer output, int inputLen) {
      int newCapacity =
          Math.min(Math.max((int) (output.capacity() * 1.7), output.capacity() + 64), inputLen * 4);
      dcheck(newCapacity > output.capacity());
      ByteBuffer newOutput = ByteBuffer.allocateDirect(newCapacity);
      output.flip();
      newOutput.put(output);
      return newOutput;
    }

    private static byte[] bytesFromBuffer(ByteBuffer buffer) {
      byte[] bytes = new byte[buffer.position()];
      buffer.flip();
      buffer.get(bytes);
      return bytes;
    }

    private CharsetEncoder makeEncoder() {
      CharsetEncoder encoder = StandardCharsets.UTF_8.newEncoder();
      switch (errors) {
        case IGNORE:
          return encoder.onUnmappableCharacter(CodingErrorAction.IGNORE);
        case REPLACE:
          return encoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
        case STRICT:
        case NAMEREPLACE:
        case SURROGATEPASS:
        case SURROGATEESCAPE:
        case BACKSLASHREPLACE:
        case XMLCHARREFREPLACE:
          return encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
      }
      throw InternalErrorException.notReached();
    }
  }

  private static _Codecs getModule(PyContext ctx) {
    return (_Codecs) ctx.kernel.moduleRegistry.ensureBuiltin(Constants._CODECS, ctx);
  }

  private static byte[] encodeWithModule(PyStr str, PyStr encoding, PyStr errors, PyContext ctx) {
    throw new NotImplementedException("encodeWithModule");
  }

  private static PyStr decodeWithModule(byte[] bytes, PyStr encoding, PyStr errors, PyContext ctx) {
    throw new NotImplementedException("decodeWithModule");
  }
}
