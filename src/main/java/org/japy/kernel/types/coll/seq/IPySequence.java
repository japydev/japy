package org.japy.kernel.types.coll.seq;

import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxOverflowError;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.num.IntLib;
import org.japy.kernel.types.num.PyInt;

public interface IPySequence extends IPyROSequence {
  void set(int index, IPyObj value, PyContext ctx);

  void setSlice(IntSlice slice, IPyObj value, PyContext ctx);

  void del(int index, PyContext ctx);

  void delSlice(IntSlice slice, PyContext ctx);

  void append(IPyObj obj, PyContext ctx);

  void insert(int pos, IPyObj obj, PyContext ctx);

  void clear(PyContext ctx);

  @InstanceMethod
  IPyObj copy(PyContext ctx);

  void remove(IPyObj obj, PyContext ctx);

  default void extend(IPyObj iterable, PyContext ctx) {
    CollLib.forEach(iterable, o -> append(o, ctx), ctx);
  }

  void _repeatInPlace(int times, PyContext ctx);

  default IPyObj imul(IPyObj other, PyContext ctx) {
    PyInt num;
    try {
      num = IntLib.asInt(other, ctx);
    } catch (PxException ex) {
      if (ex.is(PyBuiltinExc.TypeError)) {
        return NotImplemented;
      }
      throw ex;
    }

    if (!num.isSmall()) {
      throw new PxOverflowError("multiplier is too large", ctx);
    }

    int multiplier = num.intValue();
    if (multiplier < 0) {
      throw PxException.valueError("cannot repeat negative number of times", ctx);
    } else if (multiplier == 0) {
      clear(ctx);
      return this;
    } else if (multiplier == 1) {
      return this;
    }

    if (isEmpty(ctx)) {
      return this;
    }

    if (CollLib.checkMulOverflows(multiplier, len(ctx))) {
      throw new PxOverflowError("multiplier is too large", ctx);
    }

    _repeatInPlace(multiplier, ctx);
    return this;
  }

  @SpecialInstanceMethod
  default IPyObj __setitem__(
      @Param("index") IPyObj index, @Param("value") IPyObj value, PyContext ctx) {
    Object idx = CollLib.grokListIndex(index, ctx);
    if (idx instanceof Integer) {
      set((int) idx, value, ctx);
    } else {
      setSlice((IntSlice) idx, value, ctx);
    }
    return None;
  }

  @SpecialInstanceMethod
  default IPyObj __delitem__(@Param("index") IPyObj index, PyContext ctx) {
    Object idx = CollLib.grokListIndex(index, ctx);
    if (idx instanceof Integer) {
      del((int) idx, ctx);
    } else {
      delSlice((IntSlice) idx, ctx);
    }
    return None;
  }

  @SpecialInstanceMethod
  default IPyObj __imul__(@Param("num") IPyObj num, PyContext ctx) {
    return imul(num, ctx);
  }

  @InstanceMethod("append")
  default IPyObj __append(@Param("obj") IPyObj obj, PyContext ctx) {
    append(obj, ctx);
    return None;
  }

  @InstanceMethod("extend")
  default IPyObj __extend(@Param("iterable") IPyObj iterable, PyContext ctx) {
    extend(iterable, ctx);
    return None;
  }

  @InstanceMethod("insert")
  default IPyObj __insert(@Param("pos") IPyObj pos, @Param("obj") IPyObj obj, PyContext ctx) {
    insert(CollLib.checkIndexForInsert(pos, len(ctx), ctx), obj, ctx);
    return None;
  }

  @InstanceMethod("clear")
  default IPyObj __clear(PyContext ctx) {
    clear(ctx);
    return None;
  }

  @InstanceMethod("remove")
  default IPyObj __remove(@Param("element") IPyObj obj, PyContext ctx) {
    remove(obj, ctx);
    return None;
  }
}
