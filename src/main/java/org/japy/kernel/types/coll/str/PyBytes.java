package org.japy.kernel.types.coll.str;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.num.PyBool.True;

import java.util.Arrays;

import org.japy.base.ParamKind;
import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.seq.IPyImmSequence;
import org.japy.kernel.types.coll.seq.IPyROSequence;
import org.japy.kernel.types.coll.seq.IntSlice;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.types.obj.proto.IPyObjWithEq;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;
import org.japy.kernel.util.PyConstants;

public class PyBytes
    implements IPyImmSequence, IPyIntHashableObj, IPyCharSource, IPyNoValidationObj, IPyObjWithEq {
  private static final byte[] EMPTY_BYTE_ARRAY = new byte[0];
  public static final PyBytes EMPTY = new PyBytes(EMPTY_BYTE_ARRAY);

  public static final IPyClass TYPE =
      new PyBuiltinClass("bytes", PyBytes.class, PyBuiltinClass.REGULAR);

  protected final byte[] content;

  public PyBytes(byte[] bytes) {
    content = bytes;
  }

  protected static final ArgParser ARG_PARSER =
      ArgParser.builder(3).add("source").add("encoding").add("errors", PyConstants.STRICT).build();

  @Constructor
  @SubClassConstructor
  public PyBytes(Args args, PyContext ctx) {
    if (args.isEmpty()) {
      content = EMPTY_BYTE_ARRAY;
      return;
    }
    if (args.posCount() == 1 && args.kwCount() == 0) {
      IPyObj arg = args.getPos(0);
      if (arg instanceof PyStr) {
        throw PxException.typeError("bytes(): encoding argument is missing", ctx);
      }
      content = BytesLib.toBytes(args.getPos(0), ctx);
      return;
    }

    IPyObj[] argVals = ARG_PARSER.parse(args, "bytes", ctx);
    content =
        EncodeLib.encode(
            ArgLib.checkArgStr("bytes", "source", argVals[0], ctx),
            ArgLib.checkArgStr("bytes", "encoding", argVals[1], ctx),
            ArgLib.checkArgStr("bytes", "errors", argVals[2], ctx),
            ctx);
  }

  public static PyBytes fromAscii(String s) {
    return new PyBytes(UnicodeLib.ascii(s));
  }

  public byte[] internalArray() {
    return content;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj __eq__(IPyObj other, PyContext ctx) {
    if (this == other) {
      return True;
    }
    if (!(other instanceof PyBytes)) {
      return NotImplemented;
    }
    return PyBool.of(Arrays.equals(content, ((PyBytes) other).content));
  }

  @Override
  public int intHashCode(PyContext ctx) {
    return Arrays.hashCode(content);
  }

  private boolean isEmpty() {
    return content.length == 0;
  }

  public int len() {
    return content.length;
  }

  @Override
  public int len(PyContext ctx) {
    return content.length;
  }

  public byte getByte(int index) {
    return content[index];
  }

  @Override
  public IPyObj get(int index, PyContext ctx) {
    return BytesLib.toObj(content[CollLib.checkIndex(index, content.length, ctx)]);
  }

  @Override
  public IPyObj slice(IntSlice slice, PyContext ctx) {
    throw new NotImplementedException("slice");
  }

  @Override
  public boolean _canConcat(IPyObj other) {
    return other instanceof PyBytes || other instanceof PyByteArray;
  }

  @Override
  public IPyObj _concat(IPyROSequence seq, PyContext ctx) {
    if (seq instanceof PyBytes) {
      PyBytes b = (PyBytes) seq;
      byte[] sum = new byte[content.length + b.content.length];
      System.arraycopy(content, 0, sum, 0, content.length);
      System.arraycopy(b.content, 0, sum, content.length, b.content.length);
      return new PyBytes(sum);
    } else {
      PyByteArray ba = (PyByteArray) seq;
      throw new NotImplementedException("concat");
    }
  }

  @Override
  public IPyObj _repeat(int times, PyContext ctx) {
    byte[] newContent = new byte[times * content.length];
    for (int i = 0; i != times; ++i) {
      System.arraycopy(content, 0, newContent, i * content.length, content.length);
    }
    return new PyBytes(newContent);
  }

  @Override
  public IPyObj makeEmpty(PyContext ctx) {
    return EMPTY;
  }

  @SpecialInstanceMethod
  public IPyObj __mod__(@Param("obj") IPyObj obj, PyContext ctx) {
    return PrintfLib.printf(this, obj, ctx);
  }

  public boolean isAscii() {
    return UnicodeLib.isAscii(content);
  }

  public String asAsciiString() {
    return UnicodeLib.ascii(content);
  }

  @Override
  public CharSource asCharSource() {
    return BytesLib.charSource(this);
  }

  @InstanceMethod
  public IPyObj decode(
      @Param(value = "encoding", defaultStr = Constants.UTF_8, kind = ParamKind.POS_OR_KW)
          IPyObj encodingArg,
      @Param(value = "errors", defaultStr = Constants.STRICT, kind = ParamKind.POS_OR_KW)
          IPyObj errorsArg,
      PyContext ctx) {
    return EncodeLib.decode(
        this,
        ArgLib.checkArgStr("bytes.decode", "encoding", encodingArg, ctx),
        ArgLib.checkArgStr("bytes.decode", "errors", errorsArg, ctx),
        ctx);
  }
}
