package org.japy.kernel.types.coll.iter;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.IPyNoValidationObj;

class PyIterWrapper implements IPyIter, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "iterwrapper", PyIterWrapper.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private final IPyObj iter;

  PyIterWrapper(IPyObj iter) {
    this.iter = iter;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj next(PyContext ctx) {
    return IterLib.next(iter, ctx);
  }
}
