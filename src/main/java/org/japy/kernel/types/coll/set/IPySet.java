package org.japy.kernel.types.coll.set;

import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.base.ParamKind;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.obj.proto.IPyUnhashableObj;

public interface IPySet extends IPyROSet, IPyUnhashableObj {
  void add(IPyObj obj, PyContext ctx);

  void remove(IPyObj obj, PyContext ctx);

  void discard(IPyObj obj, PyContext ctx);

  void clear(PyContext ctx);

  @InstanceMethod
  IPyObj pop(PyContext ctx);

  void intersectionUpdate(IPyObj other, PyContext ctx);

  @InstanceMethod
  default IPyObj intersection_update(
      @Param(value = "others", kind = ParamKind.VAR_POS) IPyObj others, PyContext ctx) {
    ((PyTuple) others).forEachNoCtx(other -> intersectionUpdate(other, ctx));
    return None;
  }

  @InstanceMethod
  default IPyObj update(
      @Param(value = "others", kind = ParamKind.VAR_POS) IPyObj others, PyContext ctx) {
    ((PyTuple) others).forEachNoCtx(other -> CollLib.forEach(other, o -> add(o, ctx), ctx));
    return None;
  }

  @InstanceMethod
  default IPyObj difference_update(
      @Param(value = "others", kind = ParamKind.VAR_POS) IPyObj others, PyContext ctx) {
    ((PyTuple) others).forEachNoCtx(other -> CollLib.forEach(other, o -> discard(o, ctx), ctx));
    return None;
  }

  @InstanceMethod("add")
  default IPyObj __add(@Param("elem") IPyObj elem, PyContext ctx) {
    add(elem, ctx);
    return None;
  }

  @InstanceMethod("discard")
  default IPyObj __discard(@Param("elem") IPyObj elem, PyContext ctx) {
    discard(elem, ctx);
    return None;
  }

  @InstanceMethod("remove")
  default IPyObj __remove(@Param("elem") IPyObj elem, PyContext ctx) {
    remove(elem, ctx);
    return None;
  }

  @InstanceMethod("clear")
  default IPyObj __clear(PyContext ctx) {
    clear(ctx);
    return None;
  }
}
