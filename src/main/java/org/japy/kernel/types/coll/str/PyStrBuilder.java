package org.japy.kernel.types.coll.str;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.eclipse.collections.impl.list.mutable.primitive.IntArrayList;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;

@CanIgnoreReturnValue
public class PyStrBuilder {
  private @Nullable IntArrayList cp;
  private @Nullable StringBuilder sb;

  public PyStrBuilder() {}

  public PyStrBuilder append(PyStr s) {
    if (s.len() == 0) {
      return this;
    }

    if (s.isUcs2()) {
      return appendUcs2(s.s());
    } else {
      return append(s.u());
    }
  }

  public PyStrBuilder appendUcs2(String s) {
    dcheck(UnicodeLib.isUCS2(s));

    if (s.isEmpty()) {
      return this;
    }

    if (cp != null) {
      UnicodeLib.getCodePointsUcs2(s, cp);
    } else if (sb == null) {
      sb = new StringBuilder(s);
    } else {
      sb.append(s);
    }

    return this;
  }

  public PyStrBuilder appendRepr(IPyObj obj, PyContext ctx) {
    return append(PrintLib.repr(obj, ctx));
  }

  public PyStrBuilder appendStr(IPyObj obj, PyContext ctx) {
    return append(PrintLib.str(obj, ctx));
  }

  public PyStrBuilder appendUnknown(String s) {
    if (s.isEmpty()) {
      return this;
    }

    if (UnicodeLib.isUCS2(s)) {
      return appendUcs2(s);
    }

    makeCodePoints(s.length());
    dcheckNotNull(cp);
    UnicodeLib.getCodePoints(s, cp);
    return this;
  }

  @CheckReturnValue
  public PyStr build() {
    if (cp != null) {
      return new PyStr(new Utf32String(cp.toArray()));
    } else if (sb != null) {
      return new PyStr(sb.toString());
    } else {
      return PyStr.EMPTY_STRING;
    }
  }

  private void makeCodePoints(int addLength) {
    if (cp == null) {
      if (sb != null) {
        cp = new IntArrayList(sb.length() + addLength);
        UnicodeLib.getCodePoints(sb.toString(), cp);
      } else {
        cp = new IntArrayList(addLength);
      }
    }
  }

  private PyStrBuilder append(Utf32String s) {
    return appendUtf32(s.content);
  }

  public PyStrBuilder appendUtf32(int[] data) {
    if (data.length == 0) {
      return this;
    }

    makeCodePoints(data.length);
    dcheckNotNull(cp);
    cp.addAll(data);
    return this;
  }
}
