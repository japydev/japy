package org.japy.kernel.types.coll.seq;

import static org.japy.infra.validation.Debug.dcheck;

import org.japy.infra.validation.Debug;

public class SliceIndices {
  public static final SliceIndices EMPTY = new SliceIndices(0, 0, 1);

  public final int start;
  public final int stop;
  public final int step;

  public SliceIndices(int start, int stop, int step) {
    if (Debug.ENABLED) {
      dcheck((step > 0 && start <= stop) || (step < 0 && start >= stop));
    }

    this.start = start;
    this.stop = stop;
    this.step = step;
  }

  public boolean isEmpty() {
    return step > 0 ? (start >= stop) : (start <= stop);
  }
}
