package org.japy.kernel.types.coll.iter;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;
import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.seq.IPyROSequence;
import org.japy.kernel.types.coll.seq.IntSlice;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.ObjLib;

public class IterLib {
  private static PxException throwIterOrReversedUnsupported(
      IPyObj obj, boolean reverse, PyContext ctx) {
    throw PxException.typeError(
        String.format(
            "object of type '%s' has no %s()", obj.typeName(), reverse ? "reversed" : "iter"),
        ctx);
  }

  public static PxException throwIterUnsupported(IPyObj obj, PyContext ctx) {
    return throwIterOrReversedUnsupported(obj, false, ctx);
  }

  private static IPyObj getIter(IPyObj obj, boolean reverse, PyContext ctx) {
    @Nullable
    FuncHandle iter =
        obj.specialMethodOrNone(reverse ? SpecialMethod.REVERSED : SpecialMethod.ITER);
    if (iter == DELETED_METHOD) {
      throw throwIterOrReversedUnsupported(obj, reverse, ctx);
    }

    if (iter != null) {
      try {
        return (IPyObj) iter.handle.invokeExact(obj, ctx);
      } catch (Throwable ex) {
        throw ExcUtil.rethrow(ex);
      }
    }

    @Nullable FuncHandle getItem = obj.specialMethod(SpecialMethod.GETITEM);
    if (getItem != null) {
      if (reverse) {
        @Nullable FuncHandle len = obj.specialMethod(SpecialMethod.LEN);
        if (len != null) {
          return new PyReverseGetItemIter(obj, len, getItem, ctx);
        }
      } else {
        return new PyGetItemIter(obj, getItem);
      }
    }

    throw PxException.typeError(String.format("'%s' object is not iterable", obj.typeName()), ctx);
  }

  public static IPyObj iter(IPyObj obj, PyContext ctx) {
    if (obj instanceof IPyIterable) {
      return ((IPyIterable) obj).iter(ctx);
    }

    return getIter(obj, false, ctx);
  }

  public static IPyObj reversed(IPyObj obj, PyContext ctx) {
    return getIter(obj, true, ctx);
  }

  public static IPyObj next(IPyObj obj, PyContext ctx) {
    if (obj instanceof IPyIter) {
      return ((IPyIter) obj).next(ctx);
    }

    @Nullable FuncHandle next = obj.specialMethod(SpecialMethod.NEXT);
    if (next == null) {
      throw PxException.typeError(
          String.format("'%s' object does not support iteration", obj.typeName()), ctx);
    }
    try {
      return (IPyObj) next.handle.invokeExact(obj, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  public static IPyForIter makeForIter(IPyObj coll, PyContext ctx) {
    if (coll instanceof IPyIterable) {
      return new PyForIter((IPyIterable) coll, ctx);
    }
    return new PyGenericForIter(coll, ctx);
  }

  // TODO zip should be a class
  public static IPyIter zip(PyTuple iterables, boolean strict, PyContext ctx) {
    IPyObj[] iters = new IPyObj[iterables.len()];
    for (int i = 0; i != iters.length; ++i) {
      iters[i] = iter(iterables.get(i), ctx);
    }
    return new ZipIter(iters, strict);
  }

  public static IPyObj sliceIter(IPyObj seq, IntSlice slice, PyContext ctx) {
    if (seq instanceof IPyROSequence) {
      return ((IPyROSequence) seq).sliceIter(slice, ctx);
    } else {
      throw new NotImplementedException("sliceIter");
    }
  }

  public static IPyIter seqSliceIter(IPyROSequence seq, IntSlice slice, PyContext ctx) {
    return new PySeqSliceIter(seq, slice, ctx);
  }

  public static IPyIter map(IPyObj func, PyTuple iterables, PyContext ctx) {
    if (iterables.isEmpty()) {
      throw PxException.typeError("at least one iterable must be provided", ctx);
    }
    return new PyMapIter(func, iterables, ctx);
  }

  public static IPyObj enumerate(IPyObj obj, int start, PyContext ctx) {
    return new PyEnumerateIter(iter(obj, ctx), start);
  }

  public static IPyIter ensureIter(IPyObj iter) {
    if (iter instanceof IPyIter) {
      return (IPyIter) iter;
    } else {
      return new PyIterWrapper(iter);
    }
  }

  private static class CallUntilSentinelIter implements IPyIter, IPyNoValidationObj {
    private static final IPyClass TYPE =
        new PyBuiltinClass(
            "calluntilsentineliter",
            CallUntilSentinelIter.class,
            MethodHandles.lookup(),
            PyBuiltinClass.INTERNAL);

    private final IPyObj func;
    private final IPyObj sentinel;
    private boolean done;

    private CallUntilSentinelIter(IPyObj func, IPyObj sentinel) {
      this.func = func;
      this.sentinel = sentinel;
    }

    @Override
    public IPyClass type() {
      return TYPE;
    }

    @Override
    public IPyObj next(PyContext ctx) {
      if (done) {
        throw new PxStopIteration(ctx);
      }

      IPyObj value = FuncLib.call(func, ctx);
      if (value.isEqual(sentinel, ctx)) {
        done = true;
        throw new PxStopIteration(ctx);
      }

      return value;
    }
  }

  public static IPyObj iter(IPyObj func, IPyObj sentinel) {
    return new CallUntilSentinelIter(func, sentinel);
  }

  private static class FilterIter implements IPyIter, IPyNoValidationObj {
    private static final IPyClass TYPE =
        new PyBuiltinClass(
            "filteriter", FilterIter.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

    private final IPyObj function;
    private final IPyObj iter;

    FilterIter(IPyObj function, IPyObj iterable, PyContext ctx) {
      this.function = function;
      this.iter = IterLib.iter(iterable, ctx);
    }

    @Override
    public IPyClass type() {
      return TYPE;
    }

    @Override
    public IPyObj next(PyContext ctx) {
      while (true) {
        IPyObj value = IterLib.next(iter, ctx);
        if (ObjLib.isTrue(function != None ? FuncLib.call(function, value, ctx) : value, ctx)) {
          return value;
        }
      }
    }
  }

  public static IPyObj filter(IPyObj function, IPyObj iterable, PyContext ctx) {
    return new FilterIter(function, iterable, ctx);
  }
}
