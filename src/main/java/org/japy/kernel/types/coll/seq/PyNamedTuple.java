package org.japy.kernel.types.coll.seq;

import static org.japy.infra.validation.Debug.dcheck;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.exc.px.PxAttributeError;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.PyObject;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyNamedTuple extends PyTuple {
  private final PyNamedTupleClass cls;

  public PyNamedTuple(PyNamedTupleClass cls, IPyObj... values) {
    super(values);
    this.cls = cls;
    if (Debug.ENABLED) {
      dcheck(cls.fieldMap.size() == values.length);
    }
  }

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(1).add("tuple", ParamKind.POS_ONLY).build();

  @Constructor
  static IPyObj construct(IPyClass cls, Args args, PyContext ctx) {
    if (!(cls instanceof PyNamedTupleClass)) {
      throw PxException.typeError("class is not a named tuple class", ctx);
    }
    PyNamedTupleClass tupleClass = (PyNamedTupleClass) cls;
    PyTuple tuple =
        ArgLib.checkArgTuple(
            "namedtuple", "tuple", ARG_PARSER.parse1(args, "namedtuple", ctx), ctx);
    if (tupleClass.fieldMap.size() != tuple.len()) {
      throw PxException.valueError(
          String.format(
              "expected %s values in the tuple, got %s", tupleClass.fieldMap.size(), tuple.len()),
          ctx);
    }
    return new PyNamedTuple(tupleClass, tuple.content);
  }

  @Override
  public IPyClass type() {
    return cls;
  }

  @SpecialInstanceMethod
  static IPyObj __getattribute__(IPyObj self, @Param("attr") IPyObj attr, PyContext ctx) {
    PyNamedTupleClass cls = (PyNamedTupleClass) self.type();
    String attrName = ObjLib.attrName(attr, ctx);
    @Nullable Integer index = cls.fieldMap.get(attrName);
    if (index != null) {
      return ((PyNamedTuple) self).get(index);
    }
    return PyObject.__getattribute__(self, attr, ctx);
  }

  @SpecialInstanceMethod
  static IPyObj __setattr__(
      IPyObj self, @Param("attr") IPyObj attr, @Param("value") IPyObj value, PyContext ctx) {
    PyNamedTupleClass cls = (PyNamedTupleClass) self.type();
    String attrName = ObjLib.attrName(attr, ctx);
    if (cls.fieldMap.containsKey(attrName)) {
      throw new PxAttributeError(
          String.format("cannot set attribute '%s' of class '%s'", attrName, self.typeName()), ctx);
    }
    return PyObject.__setattr__(self, attr, value, ctx);
  }

  @SpecialInstanceMethod
  static IPyObj __delattr__(IPyObj self, @Param("attr") IPyObj attr, PyContext ctx) {
    PyNamedTupleClass cls = (PyNamedTupleClass) self.type();
    String attrName = ObjLib.attrName(attr, ctx);
    if (cls.fieldMap.containsKey(attrName)) {
      throw new PxAttributeError(
          String.format("cannot delete attribute '%s' of class '%s'", attrName, self.typeName()),
          ctx);
    }
    return PyObject.__delattr__(self, attr, ctx);
  }
}
