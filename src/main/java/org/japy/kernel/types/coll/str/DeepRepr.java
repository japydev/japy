package org.japy.kernel.types.coll.str;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.obj.proto.IPyObjWithDeepRepr;
import org.japy.kernel.util.PyConstants;

public class DeepRepr {
  private final Set<IPyObj> seen = Collections.newSetFromMap(new IdentityHashMap<>());
  public final PyStrBuilder sb;

  public DeepRepr() {
    sb = new PyStrBuilder();
  }

  public DeepRepr(PyStrBuilder sb) {
    this.sb = sb;
  }

  public void append(IPyObj obj, PyContext ctx) {
    if (!(obj instanceof IPyObjWithDeepRepr)) {
      sb.appendRepr(obj, ctx);
      return;
    }

    if (seen.contains(obj)) {
      sb.append(PyConstants.TILDE);
      return;
    }

    seen.add(obj);
    ((IPyObjWithDeepRepr) obj).print(this, ctx);
  }
}
