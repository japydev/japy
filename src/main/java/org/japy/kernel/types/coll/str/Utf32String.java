package org.japy.kernel.types.coll.str;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.Arrays;

import com.google.errorprone.annotations.Var;

import org.japy.infra.validation.Debug;

class Utf32String {
  protected final int[] content;
  private int hash;
  private boolean hashIsZero;

  Utf32String(int[] content) {
    this.content = content;
    if (Debug.ENABLED) {
      dcheck(!UnicodeLib.isUCS2(content));
    }
  }

  @Override
  public String toString() {
    return UnicodeLib.codePointsToString(content);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Utf32String)) {
      return false;
    }
    Utf32String that = (Utf32String) o;
    return Arrays.equals(content, that.content);
  }

  @Override
  public int hashCode() {
    // same data race shenanigans as in String
    @Var int h = hash;
    if (h == 0 && !hashIsZero) {
      h = Arrays.hashCode(content);
      if (h == 0) {
        hashIsZero = true;
      } else {
        hash = h;
      }
    }
    return h;
  }

  static PyStr fromCodePoints(int[] codepoints) {
    return new PyStr(new Utf32String(codepoints));
  }

  PyStr substring(int start, int end) {
    return substring(content, start, end);
  }

  static PyStr substring(int[] content, int start, int end) {
    if (UnicodeLib.isUCS2(content, start, end)) {
      return Ucs2String.fromCodePoints(content, start, end);
    } else {
      return fromCodePoints(Arrays.copyOfRange(content, start, end));
    }
  }

  boolean contains(char c) {
    for (int cp : content) {
      if (cp == c) {
        return true;
      }
    }
    return false;
  }

  boolean contains(int c) {
    for (int cp : content) {
      if (cp == c) {
        return true;
      }
    }
    return false;
  }

  boolean startsWith(Utf32String prefix) {
    return prefix.content.length <= content.length
        && Arrays.equals(
            content, 0, prefix.content.length, prefix.content, 0, prefix.content.length);
  }

  boolean startsWith(String prefix) {
    if (content.length < prefix.length()) {
      return false;
    }

    for (int i = 0, len = prefix.length(); i < len; ++i) {
      if (content[i] != prefix.charAt(i)) {
        return false;
      }
    }

    return true;
  }

  boolean endsWith(String suffix) {
    if (content.length < suffix.length()) {
      return false;
    }
    for (int i = 0, len = suffix.length(); i != len; ++i) {
      if (content[content.length - i - 1] != suffix.charAt(len - i - 1)) {
        return false;
      }
    }
    return true;
  }

  boolean endsWith(Utf32String suffix) {
    return content.length >= suffix.content.length
        && Arrays.equals(
            content,
            content.length - suffix.content.length,
            content.length,
            suffix.content,
            0,
            suffix.content.length);
  }

  int rstripLen(Utf32String chars) {
    for (int i = content.length - 1; i >= 0; --i) {
      if (!chars.contains(content[i])) {
        return content.length - 1 - i;
      }
    }
    return content.length;
  }

  int rstripLen(String chars) {
    for (int i = content.length - 1; i >= 0; --i) {
      if (!Character.isBmpCodePoint(content[i]) || chars.indexOf(content[i]) == -1) {
        return content.length - 1 - i;
      }
    }
    return content.length;
  }

  int rstripLen() {
    for (int i = content.length - 1; i >= 0; --i) {
      if (!Character.isWhitespace(content[i])) {
        return content.length - 1 - i;
      }
    }
    return content.length;
  }

  int lstripLen(Utf32String chars) {
    for (int i = 0, c = content.length; i != c; ++i) {
      if (!chars.contains(content[i])) {
        return i;
      }
    }
    return content.length;
  }

  int lstripLen(String chars) {
    for (int i = 0, c = content.length; i != c; ++i) {
      if (!Ucs2String.contains(chars, content[i])) {
        return i;
      }
    }
    return content.length;
  }

  int lstripLen() {
    for (int i = 0, c = content.length; i != c; ++i) {
      if (!Character.isWhitespace(content[i])) {
        return i;
      }
    }
    return content.length;
  }

  public boolean contains(String needle) {
    return indexOf(needle) != -1;
  }

  public boolean contains(Utf32String needle) {
    return indexOf(needle) != -1;
  }

  int indexOf(String needle) {
    return indexOf(needle, 0);
  }

  int indexOf(String needle, int start) {
    int needleLen = needle.length();
    if (needleLen == 0) {
      return start;
    }
    if (start + needleLen > content.length) {
      return -1;
    }
    char firstChar = needle.charAt(0);
    for (int i = start, stop = content.length - needleLen; i != stop; ++i) {
      if (content[i] == firstChar) {
        @Var int j = 1;
        while (j != needleLen && content[i + j] == needle.charAt(j)) {
          ++j;
        }
        if (j == needleLen) {
          return i;
        }
      }
    }
    return -1;
  }

  int indexOf(Utf32String needle) {
    return indexOf(needle, 0);
  }

  int indexOf(Utf32String needle, int start) {
    int needleLen = needle.content.length;
    if (needleLen == 0) {
      return start;
    }
    if (start + needleLen > content.length) {
      return -1;
    }
    int firstChar = needle.content[0];
    for (int i = 0, stop = content.length - needleLen; i != stop; ++i) {
      if (content[i] == firstChar) {
        @Var int j = 1;
        while (j != needleLen && content[i + j] == needle.content[j]) {
          ++j;
        }
        if (j == needleLen) {
          return i;
        }
      }
    }
    return -1;
  }

  int compareTo(String s) {
    int len1 = content.length;
    int len2 = s.length();
    for (int i = 0, commonLen = Math.min(len1, len2); i != commonLen; ++i) {
      int c1 = content[i];
      char c2 = s.charAt(i);
      if (c1 != c2) {
        return c1 - c2;
      }
    }
    return len1 - len2;
  }

  int compareTo(Utf32String s) {
    return Arrays.compare(content, s.content);
  }

  private void checkFindPrereqs(int needleLen, int start, int end) {
    dcheck(needleLen != 0);
    dcheck(start < end);
    dcheck(start >= 0);
    dcheck(end <= content.length);
  }

  private boolean regionMatches(int start, String other, int otherStart, int len) {
    for (int i = 0; i != len; ++i) {
      if (content[start + i] != other.charAt(otherStart + i)) {
        return false;
      }
    }
    return true;
  }

  int find(String needle, int start, int end) {
    if (Debug.ENABLED) {
      checkFindPrereqs(needle.length(), start, end);
    }

    int needleLen = needle.length();
    char firstChar = needle.charAt(0);
    for (int i = start, stop = end - needleLen; i != stop + 1; ++i) {
      if (content[i] == firstChar && regionMatches(i + 1, needle, 1, needle.length() - 1)) {
        return i;
      }
    }
    return -1;
  }

  int rfind(String needle, int start, int end) {
    if (Debug.ENABLED) {
      checkFindPrereqs(needle.length(), start, end);
    }

    int needleLen = needle.length();
    char firstChar = needle.charAt(0);
    for (int i = end - needleLen; i != start - 1; --i) {
      if (content[i] == firstChar && regionMatches(i + 1, needle, 1, needle.length() - 1)) {
        return i;
      }
    }
    return -1;
  }

  int find(Utf32String needle, int start, int end) {
    if (Debug.ENABLED) {
      checkFindPrereqs(needle.content.length, start, end);
    }

    int needleLen = needle.content.length;
    int firstChar = needle.content[0];
    for (int i = start, stop = end - needleLen; i != stop + 1; ++i) {
      if (content[i] == firstChar
          && Arrays.equals(content, i + 1, i + needleLen, needle.content, 1, needleLen)) {
        return i;
      }
    }
    return -1;
  }

  int rfind(Utf32String needle, int start, int end) {
    if (Debug.ENABLED) {
      checkFindPrereqs(needle.content.length, start, end);
    }

    int needleLen = needle.content.length;
    int firstChar = needle.content[0];
    for (int i = end - needleLen; i != start - 1; --i) {
      if (content[i] == firstChar
          && Arrays.equals(content, i + 1, i + needleLen, needle.content, 1, needleLen)) {
        return i;
      }
    }
    return -1;
  }
}
