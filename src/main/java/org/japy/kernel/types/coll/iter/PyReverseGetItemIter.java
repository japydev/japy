package org.japy.kernel.types.coll.iter;

import static org.japy.kernel.util.PyUtil.wrap;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.obj.IPyNoValidationObj;

class PyReverseGetItemIter implements IPyIter, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass("genericreverseiter", PyReverseGetItemIter.class, PyBuiltinClass.INTERNAL);

  private final IPyObj obj;
  private final FuncHandle getItem;
  private int next;

  PyReverseGetItemIter(IPyObj obj, FuncHandle len, FuncHandle getItem, PyContext ctx) {
    this.obj = obj;
    this.getItem = getItem;
    int len1;
    try {
      len1 = (int) len.handle.invokeExact(obj, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
    this.next = len1 - 1;
  }

  @Override
  public IPyObj next(PyContext ctx) {
    if (next < 0) {
      throw new PxStopIteration(ctx);
    }
    try {
      return (IPyObj) getItem.handle.invokeExact(obj, (IPyObj) wrap(next--), ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
