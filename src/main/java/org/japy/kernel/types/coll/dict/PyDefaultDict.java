package org.japy.kernel.types.coll.dict;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.Args;

public class PyDefaultDict extends PyDict {
  public static final IPyClass TYPE =
      new PyBuiltinClass("defaultdict", PyDict.TYPE, PyDefaultDict.class, PyBuiltinClass.REGULAR);

  @Constructor
  @SubClassConstructor
  public PyDefaultDict(Args args, PyContext ctx) {
    throw new NotImplementedException("PyDefaultDict");
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
