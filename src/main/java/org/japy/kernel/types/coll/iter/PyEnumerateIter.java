package org.japy.kernel.types.coll.iter;

import static org.japy.kernel.util.PyUtil.wrap;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.obj.IPyNoValidationObj;

class PyEnumerateIter implements IPyIter, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "enumerateiter", PyEnumerateIter.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private final IPyObj iter;
  private int index;

  PyEnumerateIter(IPyObj iter, int start) {
    this.iter = iter;
    this.index = start;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj next(PyContext ctx) {
    IPyObj value = IterLib.next(iter, ctx);
    return PyTuple.of(wrap(index++), value);
  }
}
