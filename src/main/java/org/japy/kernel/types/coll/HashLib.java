package org.japy.kernel.types.coll;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.seq.PyNamedTuple;
import org.japy.kernel.types.coll.seq.PyNamedTupleClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.num.PyInt;

public class HashLib {
  public static int hashToInt(IPyObj v, PyContext ctx) {
    // TODO truncate
    if (!(v instanceof PyInt)) {
      throw PxException.typeError(
          "__hash__() returned an instance of type " + v.typeName() + ", expected int", ctx);
    }
    if (!((PyInt) v).isLong()) {
      throw PxException.typeError("__hash__() returned a value too large", ctx);
    }
    long l = ((PyInt) v).longValue();
    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
      throw PxException.typeError("__hash__() returned a value too large", ctx);
    }
    return (int) l;
  }

  private static final PyNamedTupleClass HASH_INFO_TYPE =
      new PyNamedTupleClass(
          "sys.hash_info",
          "width",
          "modulus",
          "inf",
          "nan",
          "imag",
          "algorithm",
          "hash_bits",
          "seed_bits");

  // TODO
  public static IPyObj hashInfo() {
    return new PyNamedTuple(
        HASH_INFO_TYPE,
        PyInt.get(32),
        PyInt.get(Integer.MAX_VALUE),
        PyInt.ZERO,
        PyInt.ZERO,
        PyInt.ONE,
        PyStr.ucs2("java"),
        PyInt.get(8),
        PyInt.get(8));
  }
}
