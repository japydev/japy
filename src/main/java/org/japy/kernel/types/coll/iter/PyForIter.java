package org.japy.kernel.types.coll.iter;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

class PyForIter implements IPyForIter, IPyTrueAtomObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass("foriter", PyForIter.class, PyBuiltinClass.INTERNAL);

  private final IPyIter iter;

  PyForIter(IPyIterable coll, PyContext ctx) {
    this.iter = coll.iter(ctx);
  }

  @Override
  public @Nullable IPyObj next(PyContext ctx) {
    try {
      return iter.next(ctx);
    } catch (PxStopIteration ignored) {
      return null;
    }
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public void validate(Validator v) {
    v.validate(iter);
  }
}
