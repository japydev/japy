package org.japy.kernel.types.coll.dict;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;

import java.util.function.BiConsumer;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxKeyError;

public interface IPyROMapping extends IPyROMappingProtocol {
  default IPyObj getItem(String key, PyContext ctx) {
    return getItem(PyStr.get(key), ctx);
  }

  @Nullable
  default IPyObj getOrNull(String key, PyContext ctx) {
    return getOrNull(PyStr.get(key), ctx);
  }

  @InstanceMethod("get")
  default IPyObj _get(
      @Param("key") IPyObj key,
      @Param(value = "default", dflt = DEFAULT_NONE) IPyObj dflt,
      PyContext ctx) {
    try {
      return getItem(key, ctx);
    } catch (PxKeyError ignored) {
      return dflt;
    }
  }

  void _fastForEach(BiConsumer<? super IPyObj, ? super IPyObj> func);

  default void forEach(BiConsumer<? super IPyObj, ? super IPyObj> func, PyContext ctx) {
    if (isBuiltin()) {
      _fastForEach(func);
    } else {
      forEach(k -> func.accept(k, getItem(k, ctx)), ctx);
    }
  }
}
