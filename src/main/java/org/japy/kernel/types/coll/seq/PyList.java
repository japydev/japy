package org.japy.kernel.types.coll.seq;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_FALSE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NEG_ONE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;
import static org.japy.kernel.types.num.PyBool.True;
import static org.japy.kernel.util.PyUtil.wrap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import com.google.errorprone.annotations.Var;

import org.japy.base.ParamKind;
import org.japy.infra.coll.ctx.CtxAwareCollections;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Comparisons;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialClassMethod;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.iter.IterLib;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.str.DeepRepr;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.types.obj.proto.IPyObjWithDeepRepr;
import org.japy.kernel.types.obj.proto.IPyObjWithEq;
import org.japy.kernel.types.obj.proto.IPyObjWithLen;
import org.japy.kernel.types.obj.proto.IPyUnhashableObj;
import org.japy.kernel.types.typing.PyGenericAlias;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyList implements IPyUnhashableObj, IPySequence, IPyObjWithDeepRepr, IPyObjWithEq {
  public static final IPyClass TYPE =
      new PyBuiltinClass("list", PyList.class, PyBuiltinClass.REGULAR);

  public final List<IPyObj> content;

  public PyList() {
    content = new ArrayList<>();
  }

  public PyList(int expectedSize) {
    content = expectedSize >= 0 ? new ArrayList<>(expectedSize) : new ArrayList<>();
  }

  private PyList(List<IPyObj> elements) {
    content = elements;
  }

  public PyList(IPyObj... elements) {
    this(elements, 0, elements.length);
  }

  public PyList(IPyObj[] array, int start, int end) {
    content = new ArrayList<>(end - start);
    for (int i = start; i != end; ++i) {
      content.add(array[i]);
    }
  }

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(1).add("iterable", ParamKind.POS_ONLY, Sentinel).build();

  @Constructor
  @SubClassConstructor
  public PyList(Args args, PyContext ctx) {
    IPyObj iterable = ARG_PARSER.parse1(args, "list", ctx);
    if (iterable == Sentinel) {
      content = new ArrayList<>();
      return;
    }

    content = contentOfIterable(iterable, ctx);
  }

  private static List<IPyObj> contentOfIterable(IPyObj iterable, PyContext ctx) {
    List<IPyObj> content =
        iterable instanceof IPyObjWithLen
            ? new ArrayList<>(((IPyObjWithLen) iterable).len(ctx))
            : new ArrayList<>();
    CollLib.forEach(iterable, content::add, ctx);
    return content;
  }

  public static PyList ofIterable(IPyObj iterable, PyContext ctx) {
    return new PyList(contentOfIterable(iterable, ctx));
  }

  public static PyList take(List<IPyObj> content) {
    return new PyList(content);
  }

  public static PyList of(IPyObj... elements) {
    return new PyList(elements);
  }

  public static PyList of() {
    return new PyList();
  }

  public static PyList of(IPyObj obj1) {
    PyList list = new PyList(1);
    list.append(obj1);
    return list;
  }

  public static PyList of(IPyObj obj1, IPyObj obj2) {
    PyList list = new PyList(2);
    list.append(obj1);
    list.append(obj2);
    return list;
  }

  public static PyList of(IPyObj obj1, IPyObj obj2, IPyObj obj3) {
    PyList list = new PyList(3);
    list.append(obj1);
    list.append(obj2);
    list.append(obj3);
    return list;
  }

  public static PyList of(IPyObj obj1, IPyObj obj2, IPyObj obj3, IPyObj obj4) {
    PyList list = new PyList(4);
    list.append(obj1);
    list.append(obj2);
    list.append(obj3);
    list.append(obj4);
    return list;
  }

  public void append(IPyObj obj) {
    content.add(obj);
  }

  @Override
  public void append(IPyObj obj, PyContext ctx) {
    content.add(obj);
  }

  @Override
  public void insert(int pos, IPyObj obj, PyContext ctx) {
    content.add(pos, obj);
  }

  @Override
  public void clear(PyContext ctx) {
    content.clear();
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    List<IPyObj> newContent = new ArrayList<>(content.size());
    newContent.addAll(content);
    return new PyList(newContent);
  }

  @Override
  public void remove(IPyObj obj, PyContext ctx) {
    for (int i = content.size() - 1; i >= 0; --i) {
      if (content.get(i).isEqual(obj, ctx)) {
        content.remove(i);
      }
    }
  }

  @InstanceMethod
  public IPyObj pop(
      @Param(value = "index", dflt = DEFAULT_NEG_ONE) IPyObj indexArg, PyContext ctx) {
    int index =
        CollLib.checkIndex(ArgLib.checkArgSmallInt("list.pop", "index", indexArg, ctx), len(), ctx);
    return content.remove(index);
  }

  public PyList appendNV(IPyObj obj) {
    content.add(obj);
    return this;
  }

  public void forEachNoCtx(Consumer<? super IPyObj> func) {
    content.forEach(func);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj __eq__(IPyObj other, PyContext ctx) {
    if (this == other) {
      return True;
    }
    if (!(other instanceof PyList)) {
      return NotImplemented;
    }
    return PyBool.of(CtxAwareCollections.equals(content, ((PyList) other).content, ctx));
  }

  public int len() {
    return content.size();
  }

  @Override
  public int len(PyContext ctx) {
    return content.size();
  }

  public boolean isEmpty() {
    return content.isEmpty();
  }

  public PyList copy() {
    return new PyList(new ArrayList<>(content));
  }

  @Override
  public IPyObj get(int index, PyContext ctx) {
    return content.get(CollLib.checkIndex(index, content.size(), ctx));
  }

  @Override
  public IPyObj slice(IntSlice slice, PyContext ctx) {
    SliceIndices indices = slice.indices(len(), ctx);
    if (indices.isEmpty()) {
      return new PyList();
    }
    if (indices.step != 1) {
      throw new NotImplementedException("slice");
    }
    List<IPyObj> newContent = new ArrayList<>(indices.stop - indices.start);
    for (int i = indices.start; i != indices.stop; ++i) {
      newContent.add(content.get(i));
    }
    return new PyList(newContent);
  }

  @Override
  public boolean _canConcat(IPyObj other) {
    return other instanceof PyList;
  }

  @Override
  public IPyObj makeCopy(PyContext ctx) {
    return new PyList(new ArrayList<>(content));
  }

  @Override
  public IPyObj makeEmpty(PyContext ctx) {
    return new PyList();
  }

  @Override
  public IPyObj _concat(IPyROSequence seq, PyContext ctx) {
    List<IPyObj> sum = new ArrayList<>(content);
    sum.addAll(((PyList) seq).content);
    return new PyList(sum);
  }

  @Override
  public IPyObj _repeat(int times, PyContext ctx) {
    List<IPyObj> newContent = new ArrayList<>(content.size() * times);
    for (int i = 0; i != times; ++i) {
      // TODO lots of needless copies here
      newContent.addAll(content);
    }
    return new PyList(newContent);
  }

  @Override
  public void _repeatInPlace(int times, PyContext ctx) {
    // TODO lots of needless copies here
    IPyObj[] copy = content.toArray(IPyObj[]::new);
    for (int i = 0; i < times - 1; ++i) {
      content.addAll(Arrays.asList(copy));
    }
  }

  @Override
  public void set(int index, IPyObj value, PyContext ctx) {
    content.set(CollLib.checkIndex(index, content.size(), ctx), value);
  }

  @Override
  public void setSlice(IntSlice slice, IPyObj value, PyContext ctx) {
    SliceIndices indices = slice.indices(len(), ctx);

    if (indices.start == 0 && indices.stop == len() && indices.step == 1) {
      IPyObj iter = IterLib.iter(value, ctx);
      content.clear();
      extend(iter, ctx);
      return;
    }

    if (indices.step == 1) {
      IPyObj iter = IterLib.iter(value, ctx);
      // TODO
      content.subList(indices.start, indices.stop).clear();
      @Var int idx = indices.start;
      for (IPyObj elem : new JavaIterable(iter, ctx)) {
        content.add(idx++, elem);
      }
      return;
    }

    throw new NotImplementedException("setSlice");
  }

  @Override
  public void del(int index, PyContext ctx) {
    content.remove(CollLib.checkIndex(index, content.size(), ctx));
  }

  @Override
  public void delSlice(IntSlice slice, PyContext ctx) {
    SliceIndices indices = slice.indices(len(), ctx);
    if (indices.isEmpty()) {
      return;
    }
    if (indices.step == 1) {
      content.subList(indices.start, indices.stop).clear();
      return;
    }
    throw new NotImplementedException("delSlice");
  }

  @InstanceMethod
  public IPyObj reverse(PyContext ctx) {
    for (int i = 0, c = len() / 2; i != c; ++i) {
      IPyObj tmp = content.get(i);
      content.set(i, content.get(len() - i - 1));
      content.set(len() - i - 1, tmp);
    }
    return None;
  }

  @Override
  public void validate(Validator v) {
    v.validate(content);
  }

  @SpecialClassMethod
  public static IPyObj __class_getitem__(IPyClass cls, @Param("item") IPyObj item, PyContext ctx) {
    if (item instanceof PyTuple) {
      return new PyGenericAlias(cls, (PyTuple) item);
    } else {
      return new PyGenericAlias(cls, PyTuple.of(item));
    }
  }

  @Override
  public void print(DeepRepr dr, PyContext ctx) {
    PrintLib.printList(this, dr, ctx);
  }

  private static boolean lt(IPyObj obj1, IPyObj obj2, PyContext ctx) {
    return ObjLib.isTrue(Comparisons.lt(obj1, obj2, ctx), ctx);
  }

  private static int cmp(IPyObj obj1, IPyObj obj2, boolean reverse, PyContext ctx) {
    if (obj1 == obj2) {
      return 0;
    } else if (lt(obj1, obj2, ctx)) {
      return reverse ? 1 : -1;
    } else if (lt(obj2, obj1, ctx)) {
      return reverse ? -1 : 1;
    } else {
      return 0;
    }
  }

  public void sort(IPyObj key, boolean reverse, PyContext ctx) {
    // TODO stable sort
    if (key != None) {
      content.sort(
          (o1, o2) -> cmp(FuncLib.call(key, o1, ctx), FuncLib.call(key, o2, ctx), reverse, ctx));
    } else {
      content.sort((o1, o2) -> cmp(o1, o2, reverse, ctx));
    }
  }

  @InstanceMethod
  public IPyObj sort(
      @Param(value = "key", kind = ParamKind.KW_ONLY, dflt = DEFAULT_NONE) IPyObj key,
      @Param(value = "reverse", kind = ParamKind.KW_ONLY, dflt = DEFAULT_FALSE) IPyObj reverse,
      PyContext ctx) {
    sort(key, ObjLib.isTrue(reverse, ctx), ctx);
    return None;
  }

  @InstanceMethod
  public IPyObj count(@Param("value") IPyObj value, PyContext ctx) {
    @Var int count = 0;
    for (IPyObj obj : content) {
      if (obj == value || ObjLib.isEqual(obj, value, ctx)) {
        ++count;
      }
    }
    return wrap(count);
  }
}
