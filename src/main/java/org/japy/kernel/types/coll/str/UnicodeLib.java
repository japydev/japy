package org.japy.kernel.types.coll.str;

import static org.japy.infra.validation.Debug.dcheck;

import org.eclipse.collections.impl.list.mutable.primitive.IntArrayList;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;

public class UnicodeLib {
  public static final int MAX_CODEPOINT = 0x10FFFF;

  public static boolean isUCS2(String text) {
    for (int i = 0, len = text.length(); i != len; ++i) {
      char c = text.charAt(i);
      if (Character.isHighSurrogate(c) || Character.isLowSurrogate(c)) {
        return false;
      }
    }
    return true;
  }

  static boolean isUCS2(char[] text, int start, int len) {
    for (int i = start; i != start + len; ++i) {
      char c = text[i];
      if (Character.isHighSurrogate(c) || Character.isLowSurrogate(c)) {
        return false;
      }
    }
    return true;
  }

  public static boolean isUCS2(int[] codepoints, int start, int end) {
    for (int i = start; i != end; ++i) {
      if (!Character.isBmpCodePoint(codepoints[i])) {
        return false;
      }
    }
    return true;
  }

  public static boolean isUCS2(int[] codepoints) {
    return isUCS2(codepoints, 0, codepoints.length);
  }

  static Utf32String getUtf32String(String text) {
    IntArrayList ar = new IntArrayList(text.length());
    getCodePoints(text, ar);
    return new Utf32String(ar.toArray());
  }

  static String codePointsToString(int[] codePoints) {
    StringBuilder sb = new StringBuilder((int) (codePoints.length * 1.2));
    for (int cp : codePoints) {
      if (Character.isBmpCodePoint(cp)) {
        sb.append((char) cp);
      } else {
        sb.append(Character.highSurrogate(cp));
        sb.append(Character.lowSurrogate(cp));
      }
    }
    return sb.toString();
  }

  static void getCodePoints(String text, IntArrayList ar) {
    for (int i = 0, len = text.length(); i < len; ++i) {
      char c = text.charAt(i);
      if (Character.isHighSurrogate(c)) {
        if (i + 1 == len) {
          throw new InternalErrorException("unmatched high surrogate character");
        }
        char c2 = text.charAt(i + 1);
        if (!Character.isLowSurrogate(c2)) {
          throw new InternalErrorException("unmatched high surrogate character");
        }
        ar.add(Character.toCodePoint(c, c2));
        ++i;
      } else if (Character.isLowSurrogate(c)) {
        throw new InternalErrorException("unmatched low surrogate character");
      } else {
        ar.add(c);
      }
    }
  }

  static void getCodePointsUcs2(String text, IntArrayList ar) {
    for (int i = 0, len = text.length(); i < len; ++i) {
      ar.add(text.charAt(i));
    }
  }

  public static boolean isAscii(byte[] value) {
    for (byte c : value) {
      if (c <= 0) {
        return false;
      }
    }
    return true;
  }

  public static String ascii(byte[] bytes) {
    if (Debug.ENABLED) {
      dcheck(isAscii(bytes));
    }

    char[] chars = new char[bytes.length];
    for (int i = 0; i != bytes.length; ++i) {
      chars[i] = (char) bytes[i];
    }
    return new String(chars);
  }

  public static byte[] ascii(String value) {
    int length = value.length();
    byte[] bytes = new byte[value.length()];
    for (int i = 0; i != length; ++i) {
      bytes[i] = (byte) value.charAt(i);
    }
    return bytes;
  }

  public static IPyObj chr(IPyObj ord, PyContext ctx) {
    int iord = ArgLib.checkArgSmallInt("chr", "ord", ord, ctx);
    if (iord < 0 || iord > MAX_CODEPOINT) {
      throw PxException.valueError("ordinal must be an int in the range of [0..0x10ffff]", ctx);
    }
    return PyStr.get(Character.toString(iord));
  }
}
