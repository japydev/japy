package org.japy.kernel.types.coll.dict;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;
import static org.japy.kernel.types.misc.PySentinel.nullIfSentinel;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.str.PyStr;

public interface IPyMapping extends IPyROMapping, IPyMappingProtocol {
  default void setItem(String key, IPyObj value, PyContext ctx) {
    setItem(PyStr.get(key), value, ctx);
  }

  default @Nullable IPyObj popOrNull(IPyObj key, PyContext ctx) {
    IPyObj value = pop(key, Sentinel, ctx);
    return value != Sentinel ? value : null;
  }

  default @Nullable IPyObj popOrNull(String key, PyContext ctx) {
    return popOrNull(PyStr.get(key), ctx);
  }

  default IPyObj pop(String key, PyContext ctx) {
    return pop(PyStr.get(key), ctx);
  }

  default IPyObj pop(String key, IPyObj dflt, PyContext ctx) {
    return pop(PyStr.get(key), dflt, ctx);
  }

  @InstanceMethod
  default IPyObj update(
      @Param(value = "dict-or-iterable", dflt = DEFAULT_SENTINEL) IPyObj dictOrIterable,
      @Param(value = "kwargs", kind = ParamKind.VAR_KW) IPyObj kwargs,
      PyContext ctx) {
    CollLib.update(this, nullIfSentinel(dictOrIterable), (PyDict) kwargs, ctx);
    return None;
  }
}
