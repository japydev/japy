package org.japy.kernel.types.coll.seq;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Comparisons;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.obj.ObjLib;

class SeqLib {
  private static int compare(IPyObj obj1, IPyObj obj2, PyContext ctx) {
    if (obj1 == obj2) {
      return 0;
    }
    if (ObjLib.isTrue(Comparisons.lt(obj1, obj2, ctx), ctx)) {
      return -1;
    }
    if (ObjLib.isTrue(Comparisons.eq(obj1, obj2, ctx), ctx)) {
      return 0;
    }
    if (ObjLib.isTrue(Comparisons.lt(obj2, obj1, ctx), ctx)) {
      return 1;
    }
    throw PxException.typeError(
        String.format("objects %s and %s are not comparable", obj1.typeName(), obj2.typeName()),
        ctx);
  }

  static int compare(PyTuple tup1, PyTuple tup2, PyContext ctx) {
    for (int i = 0, c = Math.min(tup1.len(), tup2.len()); i != c; ++i) {
      int cmp = compare(tup1.content[i], tup2.content[i], ctx);
      if (cmp != 0) {
        return cmp;
      }
    }
    return Integer.compare(tup1.len(), tup2.len());
  }
}
