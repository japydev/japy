package org.japy.kernel.types.coll.str;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.coll.str.PyStr.EMPTY_STRING;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import com.google.errorprone.annotations.Var;

import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.exc.px.PxException;

class StrData {
  private static final Pattern WHITESPACE =
      Pattern.compile("\\s+", Pattern.UNICODE_CHARACTER_CLASS);

  static int compare(PyStr s1, PyStr s2) {
    if (s1.isUcs2()) {
      if (s2.isUcs2()) {
        return s1.s().compareTo(s2.s());
      } else {
        return -s2.u().compareTo(s1.s());
      }
    } else {
      if (s2.isUcs2()) {
        return s1.u().compareTo(s2.s());
      } else {
        return s1.u().compareTo(s2.u());
      }
    }
  }

  static PyStr concat(PyStr ps1, PyStr ps2, PyContext ctx) {
    int len1 = ps1.len();
    int len2 = ps2.len();

    if (len1 == 0) {
      return ps2;
    } else if (len2 == 0) {
      return ps1;
    }

    int sumLen = CollLib.sumLen(len1, len2, ctx);
    if (ps1.isUcs2()) {
      if (ps2.isUcs2()) {
        return new PyStr(ps1.s() + ps2.s());
      } else {
        int[] codePoints = new int[sumLen];
        String s1 = ps1.s();
        for (int i = 0; i < len1; ++i) {
          codePoints[i] = s1.charAt(i);
        }
        System.arraycopy(ps2.u().content, 0, codePoints, len1, len2);
        return new PyStr(new Utf32String(codePoints));
      }
    } else if (ps2.isUcs2()) {
      int[] codePoints = new int[sumLen];
      System.arraycopy(ps1.u().content, 0, codePoints, 0, len1);
      String s2 = ps2.s();
      for (int i = 0; i < len2; ++i) {
        codePoints[i + len1] = s2.charAt(i);
      }
      return new PyStr(new Utf32String(codePoints));
    } else {
      int[] codePoints = new int[sumLen];
      System.arraycopy(ps1.u().content, 0, codePoints, 0, len1);
      System.arraycopy(ps2.u().content, 0, codePoints, len1, len2);
      return new PyStr(new Utf32String(codePoints));
    }
  }

  static PyStr repeat(PyStr s, int n) {
    int len = s.len();

    dcheck(n > 1);

    if (s.isUcs2()) {
      return PyStr.ucs2(s.s().repeat(n));
    }

    int[] cp = s.u().content;
    int[] newCodePoints = new int[n * len];
    for (int i = 0; i != n; ++i) {
      System.arraycopy(cp, 0, newCodePoints, i * len, len);
    }
    return new PyStr(new Utf32String(newCodePoints));
  }

  private static boolean isUcs2(int[] cp) {
    return isUcs2(cp, 0, cp.length);
  }

  private static boolean isUcs2(int[] cp, int start, int stop) {
    dcheck(start < stop);

    for (int i = start; i != stop; ++i) {
      if (!Character.isBmpCodePoint(cp[i])) {
        return false;
      }
    }

    return true;
  }

  private static String ucs2SubArray(int[] cp, int start, int stop) {
    StringBuilder sb = new StringBuilder(stop - start);
    for (int i = start; i != stop; ++i) {
      sb.append((char) cp[i]);
    }
    return sb.toString();
  }

  public static PyStr slice(PyStr ps, int start, int stop) {
    int len = ps.len();

    if (Debug.ENABLED) {
      dcheck(start >= 0 && start < len);
      dcheck(stop >= 0 && stop <= len);
      dcheck(start < stop);
    }

    if (start == 0 && stop == len) {
      return ps;
    }

    if (ps.isUcs2()) {
      return new PyStr(ps.s().substring(start, stop));
    } else if (!isUcs2(ps.u().content, start, stop)) {
      return new PyStr(new Utf32String(Arrays.copyOfRange(ps.u().content, start, stop)));
    } else {
      return new PyStr(ucs2SubArray(ps.u().content, start, stop));
    }
  }

  private static IPyObj split(String str, int maxSplit) {
    if (maxSplit > 0) {
      throw new NotImplementedException("split");
    }

    String[] pieces = WHITESPACE.split(str, 0);
    List<IPyObj> list = new ArrayList<>(pieces.length);
    for (String p : pieces) {
      if (!p.isEmpty()) {
        list.add(PyStr.get(p));
      }
    }
    return PyList.take(list);
  }

  private static IPyObj rsplit(String str, int maxSplit) {
    throw new NotImplementedException("rsplit");
  }

  private static IPyObj rsplit(String str, char sep, @Var int maxSplit) {
    ArrayList<IPyObj> list = new ArrayList<>(maxSplit > 0 ? maxSplit + 1 : 0);

    @Var int pos = str.length() - 1;
    @Var int end = pos;
    while ((pos >= 0) && (maxSplit-- > 0)) {
      for (; pos >= 0; pos--) {
        if (str.charAt(pos) == sep) {
          list.add(new PyStr(str.substring(pos + 1, end + 1)));
          pos = pos - 1;
          end = pos;
          break;
        }
      }
    }
    if (end >= -1) {
      list.add(new PyStr(str.substring(0, end + 1)));
    }

    Collections.reverse(list);
    return PyList.take(list);
  }

  private static IPyObj rsplit(String str, String sep, int maxSplit) {
    throw new NotImplementedException("rsplit");
  }

  static IPyObj split(PyStr ps, int maxSplit) {
    if (maxSplit == 0) {
      return PyList.of(ps);
    }

    if (ps.isUcs2()) {
      return split(ps.s(), maxSplit);
    } else {
      throw new NotImplementedException("split utf32");
    }
  }

  static IPyObj split(PyStr ps, PyStr sep, int maxSplit, PyContext ctx) {
    if (sep.len() == 0) {
      throw PxException.valueError("empty separator", ctx);
    }

    if (maxSplit == 0) {
      return PyList.of(ps);
    }

    if (maxSplit > 0) {
      throw new NotImplementedException("maxSplit");
    }

    int len = ps.len();
    if (sep.len() > len) {
      return PyList.of(ps);
    }

    List<IPyObj> pieces = new ArrayList<>();

    @Var int start = 0;
    while (true) {
      int match = indexOf(ps, sep, start);
      if (match == -1) {
        pieces.add(substring(ps, start, len));
        break;
      }
      pieces.add(substring(ps, start, match));
      start = match + sep.len();
      if (start == len) {
        pieces.add(EMPTY_STRING);
        break;
      }
    }

    return PyList.take(pieces);
  }

  static IPyObj rsplit(PyStr ps, int maxSplit) {
    if (maxSplit == 0) {
      return PyList.of(ps);
    }

    if (ps.isUcs2()) {
      return rsplit(ps.s(), maxSplit);
    } else {
      throw new NotImplementedException("rsplit utf32");
    }
  }

  static IPyObj rsplit(PyStr ps, PyStr sep, int maxSplit) {
    if (maxSplit == 0) {
      return PyList.of(ps);
    }

    if (ps.isUcs2()) {
      if (sep.isUcs2()) {
        if (sep.len() == 1) {
          return rsplit(ps.s(), sep.s().charAt(0), maxSplit);
        } else {
          return rsplit(ps.s(), sep.s(), maxSplit);
        }
      } else {
        return PyList.of(ps);
      }
    } else {
      throw new NotImplementedException("rsplit utf32");
    }
  }

  public static boolean startsWith(PyStr s, PyStr prefix) {
    if (s.isUcs2()) {
      if (prefix.isUcs2()) {
        return s.s().startsWith(prefix.s());
      } else {
        return false;
      }
    } else if (prefix.isUcs2()) {
      return s.u().startsWith(prefix.s());
    } else {
      return s.u().startsWith(prefix.u());
    }
  }

  public static boolean endsWith(PyStr s, PyStr suffix) {
    if (s.isUcs2()) {
      if (suffix.isUcs2()) {
        return s.s().endsWith(suffix.s());
      } else {
        return false;
      }
    } else if (suffix.isUcs2()) {
      return s.u().endsWith(suffix.s());
    } else {
      return s.u().endsWith(suffix.u());
    }
  }

  private static int rstripLen(PyStr ps, PyStr chars) {
    if (ps.isUcs2()) {
      if (chars.isUcs2()) {
        return Ucs2String.rstripLen(ps, chars.s());
      } else {
        return Ucs2String.rstripLen(ps, chars.u());
      }
    } else {
      if (chars.isUcs2()) {
        return ps.u().rstripLen(chars.s());
      } else {
        return ps.u().rstripLen(chars.u());
      }
    }
  }

  private static int lstripLen(PyStr ps, PyStr chars) {
    if (ps.isUcs2()) {
      if (chars.isUcs2()) {
        return Ucs2String.lstripLen(ps, chars.s());
      } else {
        return Ucs2String.lstripLen(ps, chars.u());
      }
    } else {
      if (chars.isUcs2()) {
        return ps.u().lstripLen(chars.s());
      } else {
        return ps.u().lstripLen(chars.u());
      }
    }
  }

  private static IPyObj substring(PyStr s, int start, int end) {
    if (start == 0 && end == s.len()) {
      return s;
    } else if (start == s.len()) {
      return EMPTY_STRING;
    }

    if (s.isUcs2()) {
      return PyStr.ucs2(s.s().substring(start, end));
    } else {
      return s.u().substring(start, end);
    }
  }

  public static IPyObj rstrip(PyStr ps, PyStr chars) {
    int rstripLen = rstripLen(ps, chars);
    return substring(ps, 0, ps.len() - rstripLen);
  }

  public static IPyObj lstrip(PyStr ps, PyStr chars) {
    int lstripLen = lstripLen(ps, chars);
    return substring(ps, lstripLen, ps.len());
  }

  public static IPyObj strip(PyStr ps, PyStr chars) {
    int lstripLen = lstripLen(ps, chars);
    if (lstripLen == ps.len()) {
      return EMPTY_STRING;
    }
    return substring(ps, lstripLen, ps.len() - rstripLen(ps, chars));
  }

  private static int rstripLen(PyStr s) {
    if (s.isUcs2()) {
      return Ucs2String.rstripLen(s);
    } else {
      return s.u().rstripLen();
    }
  }

  private static int lstripLen(PyStr s) {
    if (s.isUcs2()) {
      return Ucs2String.lstripLen(s);
    } else {
      return s.u().lstripLen();
    }
  }

  public static IPyObj rstrip(PyStr s) {
    int rstripLen = rstripLen(s);
    return substring(s, 0, s.len() - rstripLen);
  }

  public static IPyObj lstrip(PyStr s) {
    int lstripLen = lstripLen(s);
    return substring(s, lstripLen, s.len());
  }

  public static IPyObj strip(PyStr s) {
    int lstripLen = lstripLen(s);
    if (lstripLen == s.len()) {
      return EMPTY_STRING;
    }
    return substring(s, lstripLen, s.len() - rstripLen(s));
  }

  private static int indexOf(PyStr s, PyStr needle, int start) {
    if (s.isUcs2()) {
      if (needle.isUcs2()) {
        return s.s().indexOf(needle.s(), start);
      } else {
        return -1;
      }
    } else {
      if (needle.isUcs2()) {
        return s.u().indexOf(needle.s(), start);
      } else {
        return s.u().indexOf(needle.u(), start);
      }
    }
  }

  static boolean contains(PyStr s, PyStr needle) {
    if (s.isUcs2()) {
      if (needle.isUcs2()) {
        return s.s().contains(needle.s());
      } else {
        return false;
      }
    } else {
      if (needle.isUcs2()) {
        return s.u().contains(needle.s());
      } else {
        return s.u().contains(needle.u());
      }
    }
  }

  public static PyStr replace(PyStr str, PyStr old, PyStr repl, int count, PyContext ctx) {
    if (!str.isUcs2() || !old.isUcs2() || !repl.isUcs2()) {
      throw new NotImplementedException("replace");
    }
    if (count == 0) {
      return str;
    }
    if (count > 0) {
      throw new NotImplementedException("replace");
    }
    return PyStr.ucs2(str.s().replace(old.s(), repl.s()));
  }

  private static int findUnchecked(PyStr str, PyStr needle, int start, int end) {
    if (str.isUcs2()) {
      if (needle.isUcs2()) {
        return Ucs2String.find(str.s(), needle.s(), start, end);
      } else {
        return -1;
      }
    } else {
      if (needle.isUcs2()) {
        return str.u().find(needle.s(), start, end);
      } else {
        return str.u().find(needle.u(), start, end);
      }
    }
  }

  private static int rfindUnchecked(PyStr str, PyStr needle, int start, int end) {
    if (str.isUcs2()) {
      if (needle.isUcs2()) {
        return Ucs2String.rfind(str.s(), needle.s(), start, end);
      } else {
        return -1;
      }
    } else {
      if (needle.isUcs2()) {
        return str.u().rfind(needle.s(), start, end);
      } else {
        return str.u().rfind(needle.u(), start, end);
      }
    }
  }

  public static int find(boolean first, PyStr str, PyStr needle, int start, int end) {
    if (Debug.ENABLED) {
      dcheck(start >= 0);
      dcheck(end <= str.len());
    }

    if (end - start < needle.len()) {
      return -1;
    }
    if (needle.len() == 0) {
      return first ? start : end;
    }

    if (first) {
      return findUnchecked(str, needle, start, end);
    } else {
      return rfindUnchecked(str, needle, start, end);
    }
  }

  private static boolean isEol(int ch) {
    return ch == '\n'
        || ch == '\r'
        || ch == 0x0b
        || ch == 0x0c
        || ch == 0x1c
        || ch == 0x1d
        || ch == 0x85
        || ch == 0x2028
        || ch == 0x2029;
  }

  private static int findEol(CharSource src, int start) {
    for (int i = start, len = src.len(); i != len; ++i) {
      if (isEol(src.getChar(i))) {
        return i;
      }
    }
    return -1;
  }

  static IPyObj splitlines(PyStr str, boolean keepEnds) {
    UniCharSource src = str.asCharSource();
    int len = str.len();
    List<IPyObj> lines = new ArrayList<>();

    if (len == 0) {
      return new PyList();
    }

    @Var int start = 0;
    while (true) {
      if (start == len) {
        if (!keepEnds) {
          lines.add(EMPTY_STRING);
        }
        break;
      }

      int eol = findEol(src, start);
      if (eol == -1) {
        lines.add(src.subString(start, len));
        break;
      }

      int nextLine;
      if (src.getChar(eol) == '\r' && eol + 1 != len && src.getChar(eol + 1) == '\n') {
        nextLine = eol + 2;
      } else {
        nextLine = eol + 1;
      }

      if (keepEnds) {
        lines.add(src.subString(start, nextLine));
      } else {
        lines.add(src.subString(start, eol));
      }

      start = nextLine;
    }

    return PyList.take(lines);
  }

  private static class Ucs2CharSource implements UniCharSource {
    private final String str;

    private Ucs2CharSource(String str) {
      this.str = str;
    }

    @Override
    public Kind kind() {
      return Kind.UCS2;
    }

    @Override
    public int len() {
      return str.length();
    }

    @Override
    public int getChar(int index) {
      return str.charAt(index);
    }

    @Override
    public String toString() {
      return str;
    }

    @Override
    public PyStr subString(int start, int stop) {
      return PyStr.ucs2(str.substring(start, stop));
    }
  }

  private static class Utf32CharSource implements UniCharSource {
    private final int[] str;

    private Utf32CharSource(int[] str) {
      this.str = str;
    }

    @Override
    public Kind kind() {
      return Kind.UTF32;
    }

    @Override
    public int len() {
      return str.length;
    }

    @Override
    public int getChar(int index) {
      return str[index];
    }

    @Override
    public PyStr subString(int start, int stop) {
      return Utf32String.substring(str, start, stop);
    }
  }

  public static UniCharSource charSource(PyStr s) {
    if (s.isUcs2()) {
      return new Ucs2CharSource(s.s());
    } else {
      return new Utf32CharSource(s.u().content);
    }
  }
}
