package org.japy.kernel.types.coll.str;

public interface IPyCharSource {
  CharSource asCharSource();
}
