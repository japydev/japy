package org.japy.kernel.types.coll.str;

import static org.japy.kernel.types.coll.str.PyStr.EMPTY_STRING;

import java.util.List;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.RFConversion;
import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.exc.px.PxIndexError;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.Args;
import org.japy.parser.formatstring.FormatStringParser;

class StrFormatLib {
  static IPyObj format(PyStr str, Args args, PyContext ctx) {
    return new Formatter(str, args, ctx).format();
  }

  private static class Formatter {
    private final Args args;
    private final PyContext ctx;

    private final List<FormatStringParser.Chunk> chunks;

    private int curPos;

    Formatter(PyStr formatString, Args args, PyContext ctx) {
      this.args = args;
      this.ctx = ctx;
      this.chunks = new FormatStringParser(formatString.toString()).parse(ctx);
    }

    PyStr format() {
      PyStrBuilder sb = new PyStrBuilder();
      for (FormatStringParser.Chunk chunk : chunks) {
        printChunk(chunk, sb);
      }
      return sb.build();
    }

    private void printChunk(FormatStringParser.Chunk chunk, PyStrBuilder sb) {
      if (chunk instanceof FormatStringParser.Literal) {
        sb.appendUnknown(((FormatStringParser.Literal) chunk).text);
        return;
      }

      if (!(chunk instanceof FormatStringParser.ReplField)) {
        throw new InternalErrorException("unhandled chunk type " + chunk);
      }

      printReplField((FormatStringParser.ReplField) chunk, sb);
    }

    private IPyObj getArg(int index) {
      if (index >= args.posCount()) {
        throw new PxIndexError(
            String.format("Replacement index %s out of range for positional args tuple", index),
            ctx);
      }
      return args.getPos(index);
    }

    private IPyObj getArg(FormatStringParser.@Nullable Id id) {
      if (id == null) {
        return getArg(curPos++);
      } else if (id.isPos()) {
        return getArg(id.pos());
      } else {
        return args.getKw(id.name(), ctx);
      }
    }

    private IPyObj applyOps(@Var IPyObj obj, FormatStringParser.ReplField rf) {
      for (FormatStringParser.ArgOp op : rf.argOps) {
        if (op instanceof FormatStringParser.ArgAttr) {
          obj = ObjLib.getAttr(obj, ((FormatStringParser.ArgAttr) op).attr, ctx);
        } else if (op instanceof FormatStringParser.ArgSub) {
          obj = CollLib.getItem(obj, PyInt.get(((FormatStringParser.ArgSub) op).index), ctx);
        } else {
          throw InternalErrorException.notReached();
        }
      }
      return obj;
    }

    private PyStr getFormatSpec(List<FormatStringParser.Chunk> spec) {
      if (spec.size() == 0) {
        return EMPTY_STRING;
      }
      if (spec.size() == 1 && spec.get(0) instanceof FormatStringParser.Literal) {
        return PyStr.get(((FormatStringParser.Literal) spec.get(0)).text);
      }
      PyStrBuilder fssb = new PyStrBuilder();
      for (FormatStringParser.Chunk chunk : spec) {
        printChunk(chunk, fssb);
      }
      return fssb.build();
    }

    private static IPyObj convertObj(IPyObj obj, @Nullable RFConversion conversion, PyContext ctx) {
      if (conversion == null) {
        return obj;
      }
      switch (conversion) {
        case S:
          return PrintLib.str(obj, ctx);
        case A:
          return PrintLib.ascii(obj, ctx);
        case R:
          return PrintLib.repr(obj, ctx);
      }
      throw InternalErrorException.notReached();
    }

    private void printReplField(FormatStringParser.ReplField rf, PyStrBuilder sb) {
      IPyObj arg = applyOps(getArg(rf.id), rf);
      PyStr formatSpec = getFormatSpec(rf.formatSpec);
      IPyObj formatted = convertObj(arg, rf.conversion, ctx);
      sb.append(ObjFormatLib.format(formatted, formatSpec, ctx));
    }
  }
}
