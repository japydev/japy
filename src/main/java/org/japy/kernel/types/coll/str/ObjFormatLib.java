package org.japy.kernel.types.coll.str;

import static org.japy.infra.validation.Debug.dcheck;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.num.PyComplex;
import org.japy.kernel.types.num.PyFloat;
import org.japy.kernel.types.num.PyInt;
import org.japy.parser.ParserUtil;

/** https://docs.python.org/3/library/string.html#formatspec */
public class ObjFormatLib {
  public static PyStr format(IPyObj obj, PyStr formatSpec, PyContext ctx) {
    @Nullable FuncHandle h = obj.specialMethod(SpecialMethod.FORMAT);
    if (h == null) {
      throw PxException.typeError(
          String.format("object of type %s do not support format", obj.typeName()), ctx);
    }

    IPyObj v;
    try {
      v = (IPyObj) h.handle.invokeExact(obj, (IPyObj) formatSpec, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }

    if (!(v instanceof PyStr)) {
      throw PxException.typeError(
          String.format(
              "__format__ returned an instance of type %s, expected a string", v.typeName()),
          ctx);
    }

    return (PyStr) v;
  }

  public static Spec parseFormatSpec(String formatSpec, IPyObj obj, PyContext ctx) {
    ObjectType objectType;
    if (obj instanceof PyInt) {
      objectType = ObjectType.INTEGER;
    } else if (obj instanceof PyFloat) {
      objectType = ObjectType.FLOAT;
    } else if (obj instanceof PyComplex) {
      objectType = ObjectType.COMPLEX;
    } else {
      objectType = ObjectType.GENERAL;
    }
    return new SpecParser(objectType, formatSpec, ctx).parse();
  }

  private static final int TYPE_FLAG_NUMERIC = 1;
  private static final int TYPE_FLAG_INTEGER = TYPE_FLAG_NUMERIC | 2;
  private static final int TYPE_FLAG_FLOAT = TYPE_FLAG_NUMERIC | 4;
  private static final int TYPE_FLAT_COMPLEX = TYPE_FLAG_NUMERIC | 8;

  private enum ObjectType {
    INTEGER(TYPE_FLAG_INTEGER),
    FLOAT(TYPE_FLAG_FLOAT),
    COMPLEX(TYPE_FLAT_COMPLEX),
    GENERAL(0),
    ;

    public final int flags;

    ObjectType(int flags) {
      this.flags = flags;
    }

    public boolean isNumeric() {
      return (flags & TYPE_FLAG_NUMERIC) != 0;
    }

    public boolean isInteger() {
      return (flags & TYPE_FLAG_INTEGER) != 0;
    }

    public boolean isFloat() {
      return (flags & TYPE_FLAG_FLOAT) != 0;
    }
  }

  public enum PresType {
    /** `s` - String format. This is the default type for most objects. */
    STRING('s'),

    /** `b` - Binary format for integers. Outputs the number in base 2. */
    BINARY('b'),

    /**
     * `c` - Character format for integers. Converts the integer to the corresponding unicode
     * character before printing.
     */
    CHARACTER('c'),

    /** `d` - Decimal integer format. Outputs the number in base 10. Default for integers. */
    DECIMAL('d'),

    /** `o` - Octal format for integers. Outputs the number in base 8. */
    OCTAL('o'),

    /**
     * `x` - Hex format for integers. Outputs the number in base 16, using lower-case letters for
     * the digits above 9.
     */
    HEX('x'),

    /**
     * `X` - Hex format for integers. Outputs the number in base 16, using upper-case letters for
     * the digits above 9. In case '#' is specified, the prefix '0x' will be upper-cased to '0X' as
     * well.
     */
    HEX_UPPER('X'),

    /**
     * Used when presentation type is omitted for floats. This is the same as 'g', except that when
     * fixed-point notation is used to format the result, it always includes at least one digit past
     * the decimal point. The precision used is as large as needed to represent the given value
     * faithfully.
     */
    FLOAT((char) 0),

    /**
     * `n` - Number.
     *
     * <p>For integers, this is the same as 'd', except that it uses the current locale setting to
     * insert the appropriate number separator characters.
     *
     * <p>For floats, this is the same as 'g', except that it uses the current locale setting to
     * insert the appropriate number separator characters.
     */
    NUMBER('n'),

    /**
     * `e` - Scientific notation for floats. For a given precision p, formats the number in
     * scientific notation with the letter ‘e’ separating the coefficient from the exponent. The
     * coefficient has one digit before and p digits after the decimal point, for a total of p + 1
     * significant digits. With no precision given, uses a precision of 6 digits after the decimal
     * point for float, and shows all coefficient digits for Decimal. If no digits follow the
     * decimal point, the decimal point is also removed unless the # option is used.
     */
    SCIENTIFIC('e'),

    /**
     * `E` - Scientific notation for floats. Same as 'e' except it uses an upper case ‘E’ as the
     * separator character.
     */
    SCIENTIFIC_UPPER('E'),

    /**
     * `f` - Fixed-point notation for floats. For a given precision p, formats the number as a
     * decimal number with exactly p digits following the decimal point. With no precision given,
     * uses a precision of 6 digits after the decimal point for float, and uses a precision large
     * enough to show all coefficient digits for Decimal. If no digits follow the decimal point, the
     * decimal point is also removed unless the # option is used.
     */
    FIXED('f'),

    /**
     * `F` - Fixed-point notation for floats. Same as 'f', but converts nan to NAN and inf to INF.
     */
    FIXED_UPPER('F'),

    /**
     * `g` - General format for floats. For a given precision p >= 1, this rounds the number to p
     * significant digits and then formats the result in either fixed-point format or in scientific
     * notation, depending on its magnitude. A precision of 0 is treated as equivalent to a
     * precision of 1.
     *
     * <p>The precise rules are as follows: suppose that the result formatted with presentation type
     * 'e' and precision p-1 would have exponent exp. Then, if m <= exp < p, where m is -4 for
     * floats and -6 for Decimals, the number is formatted with presentation type 'f' and precision
     * p-1-exp. Otherwise, the number is formatted with presentation type 'e' and precision p-1. In
     * both cases insignificant trailing zeros are removed from the significand, and the decimal
     * point is also removed if there are no remaining digits following it, unless the '#' option is
     * used.
     *
     * <p>With no precision given, uses a precision of 6 significant digits for float. For Decimal,
     * the coefficient of the result is formed from the coefficient digits of the value; scientific
     * notation is used for values smaller than 1e-6 in absolute value and values where the place
     * value of the least significant digit is larger than 1, and fixed-point notation is used
     * otherwise.
     *
     * <p>Positive and negative infinity, positive and negative zero, and nans, are formatted as
     * inf, -inf, 0, -0 and nan respectively, regardless of the precision.
     */
    GENERAL('g'),

    /**
     * `G` - General format for floats. Same as 'g' except switches to 'E' if the number gets too
     * large. The representations of infinity and NaN are uppercased, too.
     */
    GENERAL_UPPER('G'),

    /**
     * `%` - Percentage for floats. Multiplies the number by 100 and displays in fixed ('f') format,
     * followed by a percent sign.
     */
    PERCENT('%'),
    ;

    public final char character;

    PresType(char character) {
      this.character = character;
    }
  }

  public enum Align {
    /**
     * `<` - Forces the field to be left-aligned within the available space (this is the default for
     * most objects).
     */
    LEFT,

    /**
     * `>` - Forces the field to be right-aligned within the available space (this is the default
     * for numbers).
     */
    RIGHT,

    /**
     * `=` - Forces the padding to be placed after the sign (if any) but before the digits. This is
     * used for printing fields in the form ‘+000000120’. This alignment option is only valid for
     * numeric types. It becomes the default for numbers when ‘0’ immediately precedes the field
     * width.
     */
    PAD_DIGITS,

    /** `^` - Forces the field to be centered within the available space. */
    CENTER,
  }

  public enum Sign {
    /** `+` - indicates that a sign should be used for both positive as well as negative numbers. */
    PLUS,
    /**
     * `-` - indicates that a sign should be used only for negative numbers (this is the default
     * behavior).
     */
    MINUS,
    /**
     * ` ` - indicates that a leading space should be used on positive numbers, and a minus sign on
     * negative numbers.
     */
    SPACE,
  }

  public static class Spec {
    public final @Nullable PresType presType;
    public final @Nullable String fill;
    public final @Nullable Align align;
    public final int width;

    private Spec(SpecParser p) {
      presType = p.presType;
      fill = p.fill;
      align = p.align;
      width = p.width;
    }

    public boolean isEmpty() {
      return presType == null && fill == null && align == null && width == -1;
    }
  }

  public static class NumSpec extends Spec {
    public final @Nullable Sign sign;
    public final boolean alternateForm;
    public final char decimalSeparator;
    public final int precision;

    private NumSpec(SpecParser p) {
      super(p);
      sign = p.sign;
      alternateForm = p.alternateForm;
      decimalSeparator = p.decimalSeparator;
      precision = p.precision;
    }

    @Override
    public boolean isEmpty() {
      return super.isEmpty()
          && sign == null
          && !alternateForm
          && decimalSeparator == 0
          && precision == -1;
    }
  }

  private static class SpecParser {
    private final ObjectType objectType;
    private final PyContext ctx;

    private final String formatSpec;
    private final int len;
    private int curPos;

    private @Nullable PresType presType;
    private @Nullable String fill;
    private @Nullable Align align;
    private @Nullable Sign sign;
    private int width = -1;
    private int precision = -1;
    private boolean alternateForm;
    private char decimalSeparator;

    SpecParser(ObjectType objectType, String formatSpec, PyContext ctx) {
      this.objectType = objectType;
      this.formatSpec = formatSpec;
      this.ctx = ctx;
      this.len = formatSpec.length();
    }

    Spec parse() {
      doParse();
      validate();
      return build();
    }

    private void validate() {
      if (align == Align.PAD_DIGITS && !objectType.isNumeric()) {
        throw throwInvalid("= is invalid for non-numeric types");
      }
      if (precision != -1 && !objectType.isNumeric()) {
        throw throwInvalid("precision may not be specified for non-numeric types");
      }
      if (alternateForm && !objectType.isNumeric()) {
        throw throwInvalid("alternate form may not be specified for non-numeric types");
      }
      if (decimalSeparator != 0 && !objectType.isNumeric()) {
        throw throwInvalid("decimal separator may not be specified for non-numeric types");
      }
      if (sign != null && !objectType.isNumeric()) {
        throw throwInvalid("sign option may not be specified for non-numeric types");
      }
      if (presType != null) {
        switch (presType) {
          case GENERAL:
          case GENERAL_UPPER:
          case FLOAT:
          case FIXED:
          case FIXED_UPPER:
          case PERCENT:
          case SCIENTIFIC:
          case SCIENTIFIC_UPPER:
            if (!objectType.isFloat()) {
              throw throwInvalid(
                  String.format(
                      "invalid presentation type %c for non-float type", presType.character));
            }
            break;
          case HEX:
          case HEX_UPPER:
          case OCTAL:
          case BINARY:
          case DECIMAL:
          case CHARACTER:
            if (!objectType.isInteger()) {
              throw throwInvalid(
                  String.format(
                      "invalid presentation type %c for non-integer type", presType.character));
            }
            break;
          case NUMBER:
            if (!objectType.isNumeric()) {
              throw throwInvalid(
                  String.format(
                      "invalid presentation type %c for non-numeric type", presType.character));
            }
            break;
          case STRING:
            break;
        }
      }
    }

    private char curChar() {
      return curPos == len ? 0 : formatSpec.charAt(curPos);
    }

    private void doParse() {
      if (len == 0) {
        return;
      }

      getAlign();

      switch (curChar()) {
        case ' ':
          sign = Sign.SPACE;
          ++curPos;
          break;
        case '+':
          sign = Sign.PLUS;
          ++curPos;
          break;
        case '-':
          sign = Sign.MINUS;
          ++curPos;
          break;
        default:
          break;
      }

      if (curChar() == '#') {
        alternateForm = true;
        if (!objectType.isNumeric()) {
          throw throwInvalid("# is valid only for numeric types");
        }
        ++curPos;
      }

      if (curChar() == '0') {
        if (!objectType.isNumeric()) {
          throw throwInvalid("0 is valid only for numeric types");
        }
        if (fill != null) {
          throw throwInvalid("0 conflicts with the explicit fill character");
        }
        if (align != null) {
          throw throwInvalid("0 conflicts with the explicit alignment specifier");
        }
        align = Align.PAD_DIGITS;
        fill = "0";
        ++curPos;
      }

      getWidth();

      switch (curChar()) {
        case '_':
          decimalSeparator = '_';
          ++curPos;
          break;
        case ',':
          decimalSeparator = ',';
          ++curPos;
          break;
        default:
          break;
      }

      if (curChar() == '.') {
        getPrecision();
      }

      if (curPos != len) {
        char c = curChar();
        for (PresType presType : PresType.values()) {
          if (presType.character == c) {
            this.presType = presType;
            ++curPos;
            break;
          }
        }
        if (this.presType == null) {
          throw throwInvalid(String.format("unrecognized format character '%c'", c));
        }
      }

      if (curPos != len) {
        throw throwInvalid(String.format("unrecognized format character '%c'", curChar()));
      }
    }

    private PxException throwInvalid(String message) {
      throw PxException.valueError(message, ctx);
    }

    @SuppressWarnings("UnusedException")
    private int readNumber() {
      @Var int n = -1;
      while (curPos != len) {
        char c = formatSpec.charAt(curPos);
        if (!ParserUtil.isAsciiDigit(c)) {
          return n;
        }
        ++curPos;
        if (n == -1) {
          n = 0;
        }
        try {
          n = Math.addExact(Math.multiplyExact(n, 10), c - '0');
        } catch (ArithmeticException ignored) {
          throw throwInvalid("number is too large");
        }
      }
      return n;
    }

    private void getWidth() {
      int n = readNumber();
      if (n != -1) {
        if (n == 0) {
          throw throwInvalid("negative width");
        }
        width = n;
      }
    }

    private void getPrecision() {
      if (Debug.ENABLED) {
        dcheck(curChar() == '.');
      }
      ++curPos;
      int n = readNumber();
      if (n == -1) {
        throw throwInvalid("missing precision");
      }
      precision = n;
    }

    private static @Nullable Align alignFromChar(char c) {
      switch (c) {
        case '<':
          return Align.LEFT;
        case '>':
          return Align.RIGHT;
        case '=':
          return Align.PAD_DIGITS;
        case '^':
          return Align.CENTER;
        default:
          return null;
      }
    }

    private void getAlign() {
      char c = formatSpec.charAt(0);
      int next = Character.isBmpCodePoint(c) ? 1 : 2;

      if (len > next) {
        char c2 = formatSpec.charAt(next);
        @Nullable Align align = alignFromChar(c2);
        if (align != null) {
          fill = formatSpec.substring(0, next);
          this.align = align;
          curPos = next + 1;
          return;
        }
      }

      @Nullable Align align = alignFromChar(c);
      if (align != null) {
        this.align = align;
        curPos = 1;
      }
    }

    private Spec build() {
      return objectType.isNumeric() ? new NumSpec(this) : new Spec(this);
    }
  }
}
