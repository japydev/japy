package org.japy.kernel.types.coll.dict;

import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxKeyError;

public interface IPyStrKeyMapping extends IPyMapping {
  boolean contains(String key);

  @Nullable
  IPyObj get(String key);

  void set(String key, IPyObj value);

  @CanIgnoreReturnValue
  @Nullable
  IPyObj del(String key);

  void forEachS(BiConsumer<? super String, ? super IPyObj> func);

  Set<Map.Entry<String, IPyObj>> entrySet();

  @Override
  default IPyObj getItem(IPyObj key, PyContext ctx) {
    if (key instanceof PyStr) {
      @Nullable IPyObj value = get(key.toString());
      if (value != null) {
        return value;
      }
    }
    throw new PxKeyError(key, ctx);
  }

  @Override
  default IPyObj pop(IPyObj key, PyContext ctx) {
    if (key instanceof PyStr) {
      @Nullable IPyObj value = del(key.toString());
      if (value != null) {
        return value;
      }
    }
    throw new PxKeyError(key, ctx);
  }

  @Override
  default IPyObj pop(IPyObj key, IPyObj dflt, PyContext ctx) {
    if (key instanceof PyStr) {
      @Nullable IPyObj value = del(key.toString());
      if (value != null) {
        return value;
      }
    }
    return dflt;
  }

  @Override
  default void delItem(IPyObj key, PyContext ctx) {
    if (key instanceof PyStr && del(key.toString()) != null) {
      return;
    }
    throw new PxKeyError(key, ctx);
  }

  @Override
  default void setItem(IPyObj key, IPyObj value, PyContext ctx) {
    if (!(key instanceof PyStr)) {
      throw PxException.typeError(
          String.format("key must be a string, got %s", key.typeName()), ctx);
    }
    set(key.toString(), value);
  }

  @Override
  default void setItem(String key, IPyObj value, PyContext ctx) {
    set(key, value);
  }

  @Override
  default IPyObj _get(IPyObj key, IPyObj dflt, PyContext ctx) {
    if (key instanceof PyStr) {
      @Nullable IPyObj value = get(key.toString());
      if (value != null) {
        return value;
      }
    }
    return dflt;
  }

  @Override
  default @Nullable IPyObj getOrNull(String key, PyContext ctx) {
    return get(key);
  }

  @Override
  @Nullable
  default IPyObj getOrNull(IPyObj key, PyContext ctx) {
    if (key instanceof PyStr) {
      return get(key.toString());
    }
    return null;
  }

  @Override
  default IPyObj getItem(String key, PyContext ctx) {
    @Nullable IPyObj value = get(key);
    if (value == null) {
      throw new PxKeyError(key, ctx);
    }
    return value;
  }

  @Override
  default boolean contains(IPyObj item, PyContext ctx) {
    if (item instanceof PyStr) {
      return contains(item.toString());
    }
    return false;
  }

  @Override
  default @Nullable IPyObj popOrNull(IPyObj key, PyContext ctx) {
    if (key instanceof PyStr) {
      return del(key.toString());
    }
    return null;
  }

  @Override
  default @Nullable IPyObj popOrNull(String key, PyContext ctx) {
    return del(key);
  }

  default @Nullable IPyObj popOrNull(String key) {
    return del(key);
  }

  @Override
  default IPyObj pop(String key, PyContext ctx) {
    @Nullable IPyObj value = del(key);
    if (value == null) {
      throw new PxKeyError(key, ctx);
    }
    return value;
  }

  @Override
  default IPyObj pop(String key, IPyObj dflt, PyContext ctx) {
    @Nullable IPyObj value = del(key);
    return value != null ? value : dflt;
  }

  default void forEachKeyS(Consumer<? super String> func) {
    forEachS((k, v) -> func.accept(k));
  }

  default void forEachValue(Consumer<? super IPyObj> func) {
    forEachS((k, v) -> func.accept(v));
  }

  @Override
  default void _fastForEach(BiConsumer<? super IPyObj, ? super IPyObj> func) {
    forEachS((k, v) -> func.accept(PyStr.get(k), v));
  }
}
