package org.japy.kernel.types.coll.iter;

import java.util.Iterator;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;

public class JavaIterable implements Iterable<IPyObj> {
  private final IPyObj iterable;
  private final PyContext ctx;

  public JavaIterable(IPyObj iterable, PyContext ctx) {
    this.iterable = iterable;
    this.ctx = ctx;
  }

  @Override
  public Iterator<IPyObj> iterator() {
    return new JavaIterator(IterLib.iter(iterable, ctx), ctx);
  }
}
