package org.japy.kernel.types.coll.str;

import java.lang.invoke.MethodHandles;

import org.eclipse.collections.impl.list.mutable.primitive.ByteArrayList;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SpecialInstanceMethod;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.seq.IPyROSequence;
import org.japy.kernel.types.coll.seq.IPySequence;
import org.japy.kernel.types.coll.seq.IntSlice;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyUnhashableObj;
import org.japy.kernel.util.Args;

public class PyByteArray
    implements IPySequence, IPyUnhashableObj, IPyCharSource, IPyNoValidationObj {
  public static final IPyClass TYPE =
      new PyBuiltinClass(
          "bytearray", PyByteArray.class, MethodHandles.lookup(), PyBuiltinClass.CALLABLE);

  protected final ByteArrayList content;

  public PyByteArray() {
    this(new ByteArrayList());
  }

  public PyByteArray(int initialCapacity) {
    this(new ByteArrayList(initialCapacity));
  }

  private PyByteArray(ByteArrayList content) {
    this.content = content;
  }

  @Constructor
  public PyByteArray(Args args, PyContext ctx) {
    if (args.isEmpty()) {
      content = new ByteArrayList();
      return;
    }
    if (args.posCount() == 1 && args.kwCount() == 0) {
      IPyObj arg = args.getPos(0);
      if (arg instanceof PyStr) {
        throw PxException.typeError("bytearray(): encoding argument is missing", ctx);
      }
      content = new ByteArrayList(BytesLib.toBytes(args.getPos(0), ctx));
      return;
    }

    IPyObj[] argVals = PyBytes.ARG_PARSER.parse(args, "bytearray", ctx);
    // TODO avoid extra copy
    content =
        new ByteArrayList(
            EncodeLib.encode(
                ArgLib.checkArgStr("bytes", "source", argVals[0], ctx),
                ArgLib.checkArgStr("bytes", "encoding", argVals[1], ctx),
                ArgLib.checkArgStr("bytes", "errors", argVals[2], ctx),
                ctx));
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  public boolean isEmpty() {
    return content.isEmpty();
  }

  public PyByteArray copy() {
    ByteArrayList contentCopy = new ByteArrayList(content.size());
    contentCopy.addAll(content);
    return new PyByteArray(contentCopy);
  }

  public byte getByte(int index) {
    return content.get(index);
  }

  @Override
  public IPyObj get(int index, PyContext ctx) {
    return BytesLib.toObj(content.get(CollLib.checkIndex(index, content.size(), ctx)));
  }

  @Override
  public IPyObj slice(IntSlice slice, PyContext ctx) {
    throw new NotImplementedException("slice");
  }

  @Override
  public boolean _canConcat(IPyObj other) {
    return other instanceof PyByteArray || other instanceof PyBytes;
  }

  @Override
  public IPyObj makeCopy(PyContext ctx) {
    throw new NotImplementedException("copy");
  }

  @Override
  public IPyObj makeEmpty(PyContext ctx) {
    return new PyByteArray();
  }

  @Override
  public IPyObj _concat(IPyROSequence seq, PyContext ctx) {
    throw new NotImplementedException("concat");
  }

  @Override
  public IPyObj _repeat(int times, PyContext ctx) {
    ByteArrayList newContent = new ByteArrayList(times * content.size());
    for (int i = 0; i != times; ++i) {
      newContent.addAll(content);
    }
    return new PyByteArray(newContent);
  }

  @Override
  public void _repeatInPlace(int times, PyContext ctx) {
    // TODO don't copy
    byte[] bytes = content.toArray();
    for (int i = 0; i < times - 1; ++i) {
      content.addAll(bytes);
    }
  }

  @Override
  public void set(int index, IPyObj value, PyContext ctx) {
    throw new NotImplementedException("set");
  }

  @Override
  public void setSlice(IntSlice slice, IPyObj value, PyContext ctx) {
    throw new NotImplementedException("setSlice");
  }

  @Override
  public void del(int index, PyContext ctx) {
    throw new NotImplementedException("del");
  }

  @Override
  public void delSlice(IntSlice slice, PyContext ctx) {
    throw new NotImplementedException("delSlice");
  }

  @Override
  public void append(IPyObj obj, PyContext ctx) {
    throw new NotImplementedException("append");
  }

  @Override
  public void insert(int pos, IPyObj obj, PyContext ctx) {
    throw new NotImplementedException("insert");
  }

  @Override
  public void clear(PyContext ctx) {
    content.clear();
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    ByteArrayList newContent = new ByteArrayList(content.size());
    newContent.addAll(content);
    return new PyByteArray(newContent);
  }

  @Override
  public void remove(IPyObj obj, PyContext ctx) {
    throw new NotImplementedException("remove");
  }

  public int len() {
    throw new NotImplementedException("len");
  }

  @Override
  public int len(PyContext ctx) {
    throw new NotImplementedException("len");
  }

  @SpecialInstanceMethod
  public IPyObj __mod__(@Param("obj") IPyObj obj, PyContext ctx) {
    return PrintfLib.printf(this, obj, ctx);
  }

  @Override
  public CharSource asCharSource() {
    return BytesLib.charSource(this);
  }
}
