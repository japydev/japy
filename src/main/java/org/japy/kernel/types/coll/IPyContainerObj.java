package org.japy.kernel.types.coll;

import org.japy.kernel.types.obj.proto.IPyObjWithDeepRepr;

public interface IPyContainerObj extends IPyContains, IPyObjWithDeepRepr {}
