package org.japy.kernel.types.coll.set;

import static org.japy.kernel.types.misc.PyNone.None;

import com.google.errorprone.annotations.CanIgnoreReturnValue;

import org.japy.infra.coll.ctx.CtxAwareHashSet;
import org.japy.infra.coll.ctx.CtxAwareSet;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.str.DeepRepr;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxKeyError;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PySet extends PyAbstractSet implements IPySet {
  public static final IPyClass TYPE =
      new PyBuiltinClass("set", PySet.class, PyBuiltinClass.REGULAR);

  public PySet() {}

  private PySet(CtxAwareSet<IPyObj, PyContext> content) {
    super(content);
  }

  public PySet(int expectedSize) {
    super(expectedSize);
  }

  public PySet(IPyObj[] elements, PyContext ctx) {
    super(elements, ctx);
  }

  @Override
  protected PyAbstractSet instantiate(CtxAwareSet<IPyObj, PyContext> content) {
    return new PySet(content);
  }

  @Constructor
  @SubClassConstructor
  public PySet(Args args, PyContext ctx) {
    ArgParser.verifyNoKwArgs(args, "set", ctx);
    if (args.posCount() > 1) {
      throw PxException.typeError("set() takes zero or one parameters", ctx);
    }
    if (args.posCount() == 0) {
      return;
    }

    for (IPyObj obj : new JavaIterable(args.getPos(0), ctx)) {
      add(obj, ctx);
    }
  }

  public static PySet of() {
    return new PySet();
  }

  public void extend(IPyObj coll, PyContext ctx) {
    CollLib.forEach(coll, o -> add(o, ctx), ctx);
  }

  public static PySet of(IPyObj obj1, PyContext ctx) {
    PySet set = new PySet(1);
    set.add(obj1, ctx);
    return set;
  }

  public static PySet of(IPyObj obj1, IPyObj obj2, PyContext ctx) {
    PySet set = new PySet(2);
    set.add(obj1, ctx);
    set.add(obj2, ctx);
    return set;
  }

  public static PySet of(IPyObj obj1, IPyObj obj2, IPyObj obj3, PyContext ctx) {
    PySet set = new PySet(3);
    set.add(obj1, ctx);
    set.add(obj2, ctx);
    set.add(obj3, ctx);
    return set;
  }

  public static PySet of(IPyObj obj1, IPyObj obj2, IPyObj obj3, IPyObj obj4, PyContext ctx) {
    PySet set = new PySet(4);
    set.add(obj1, ctx);
    set.add(obj2, ctx);
    set.add(obj3, ctx);
    set.add(obj4, ctx);
    return set;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public void print(DeepRepr dr, PyContext ctx) {
    PrintLib.printSet(this, dr, ctx);
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    return instantiate(new CtxAwareHashSet<>(content, ctx));
  }

  @Override
  public void add(IPyObj obj, PyContext ctx) {
    content.add(obj, ctx);
  }

  @CanIgnoreReturnValue
  private boolean removeImpl(IPyObj obj, PyContext ctx) {
    try {
      return content.remove(obj, ctx);
    } catch (PxException ex) {
      if (!(obj instanceof PySet) || !ex.is(PyBuiltinExc.TypeError)) {
        throw ex;
      }
      return removeImpl(new PyFrozenSet(obj, ctx), ctx);
    }
  }

  @Override
  public void remove(IPyObj obj, PyContext ctx) {
    if (!removeImpl(obj, ctx)) {
      throw new PxKeyError(obj, ctx);
    }
  }

  @Override
  public void discard(IPyObj obj, PyContext ctx) {
    removeImpl(obj, ctx);
  }

  @Override
  public void clear(PyContext ctx) {
    content.clear();
  }

  @Override
  public IPyObj pop(PyContext ctx) {
    if (content.isEmpty()) {
      throw new PxKeyError(None, ctx);
    }

    IPyObj obj = content.iterator().next();
    content.remove(obj, ctx);
    return obj;
  }

  @Override
  public void intersectionUpdate(IPyObj other, PyContext ctx) {
    content = intersection(content, other, ctx);
  }
}
