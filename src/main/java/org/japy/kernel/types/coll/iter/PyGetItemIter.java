package org.japy.kernel.types.coll.iter;

import static org.japy.kernel.util.PyUtil.wrap;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.exc.px.PxIndexError;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.obj.IPyNoValidationObj;

class PyGetItemIter implements IPyIter, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass("genericiter", PyGetItemIter.class, PyBuiltinClass.INTERNAL);

  private final IPyObj obj;
  private final FuncHandle getItem;
  private int next;

  PyGetItemIter(IPyObj obj, FuncHandle getItem) {
    this.obj = obj;
    this.getItem = getItem;
  }

  @Override
  @SuppressWarnings("UnusedException")
  public IPyObj next(PyContext ctx) {
    try {
      return (IPyObj) getItem.handle.invokeExact(obj, (IPyObj) wrap(next++), ctx);
    } catch (PxIndexError ignored) {
      throw new PxStopIteration(ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
