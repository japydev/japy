package org.japy.kernel.types.coll.str;

import static org.japy.infra.validation.Debug.dcheck;

import org.japy.infra.validation.Debug;

class Ucs2String {
  static PyStr fromCodePoints(int[] codepoints, int start, int end) {
    if (Debug.ENABLED) {
      for (int i = start; i != end; ++i) {
        dcheck(Character.isBmpCodePoint(codepoints[i]));
      }
    }
    char[] chars = new char[end - start];
    for (int i = 0; i != end - start; ++i) {
      chars[i] = (char) codepoints[start + i];
    }
    return PyStr.ucs2(String.valueOf(chars));
  }

  static boolean contains(String s, char c) {
    return s.indexOf(c) != -1;
  }

  static boolean contains(String s, int cp) {
    return Character.isBmpCodePoint(cp) && s.indexOf(cp) != -1;
  }

  static int rstripLen(PyStr ps, String chars) {
    String s = ps.s();
    for (int i = s.length() - 1; i >= 0; --i) {
      if (!contains(chars, s.charAt(i))) {
        return s.length() - 1 - i;
      }
    }
    return s.length();
  }

  static int rstripLen(PyStr ps, Utf32String chars) {
    String s = ps.s();
    for (int i = s.length() - 1; i >= 0; --i) {
      if (!chars.contains(s.charAt(i))) {
        return s.length() - 1 - i;
      }
    }
    return s.length();
  }

  static int rstripLen(PyStr ps) {
    String s = ps.s();
    for (int i = s.length() - 1; i >= 0; --i) {
      if (!Character.isWhitespace(s.charAt(i))) {
        return s.length() - 1 - i;
      }
    }
    return s.length();
  }

  static int lstripLen(PyStr ps, String chars) {
    String s = ps.s();
    for (int i = 0, c = s.length(); i != c; ++i) {
      if (!contains(chars, s.charAt(i))) {
        return i;
      }
    }
    return s.length();
  }

  static int lstripLen(PyStr ps, Utf32String chars) {
    String s = ps.s();
    for (int i = 0, c = s.length(); i != c; ++i) {
      if (!chars.contains(s.charAt(i))) {
        return i;
      }
    }
    return s.length();
  }

  static int lstripLen(PyStr ps) {
    String s = ps.s();
    for (int i = 0, c = s.length(); i != c; ++i) {
      if (!Character.isWhitespace(s.charAt(i))) {
        return i;
      }
    }
    return s.length();
  }

  private static void checkFindPrereqs(String s, String needle, int start, int end) {
    dcheck(needle.length() != 0);
    dcheck(start < end);
    dcheck(start >= 0);
    dcheck(end <= s.length());
  }

  public static int find(String s, String needle, int start, int end) {
    if (Debug.ENABLED) {
      checkFindPrereqs(s, needle, start, end);
    }

    int needleLen = needle.length();
    char firstChar = needle.charAt(0);
    for (int i = start, stop = end - needleLen; i != stop + 1; ++i) {
      if (s.charAt(i) == firstChar && s.regionMatches(i + 1, needle, 1, needle.length() - 1)) {
        return i;
      }
    }
    return -1;
  }

  public static int rfind(String s, String needle, int start, int end) {
    if (Debug.ENABLED) {
      checkFindPrereqs(s, needle, start, end);
    }

    int needleLen = needle.length();
    char firstChar = needle.charAt(0);
    for (int i = end - needleLen; i != start - 1; --i) {
      if (s.charAt(i) == firstChar && s.regionMatches(i + 1, needle, 1, needle.length() - 1)) {
        return i;
      }
    }
    return -1;
  }
}
