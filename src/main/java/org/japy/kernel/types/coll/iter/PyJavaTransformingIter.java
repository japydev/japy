package org.japy.kernel.types.coll.iter;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Function;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.obj.IPyNoValidationObj;

public class PyJavaTransformingIter<T> implements IPyIter, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass("jtiter", PyJavaTransformingIter.class, PyBuiltinClass.INTERNAL);

  private final Iterator<T> iter;
  private final Function<T, IPyObj> mapFunc;

  public PyJavaTransformingIter(Iterable<T> coll, Function<T, IPyObj> mapFunc) {
    this(coll.iterator(), mapFunc);
  }

  public PyJavaTransformingIter(Iterator<T> iter, Function<T, IPyObj> mapFunc) {
    this.iter = iter;
    this.mapFunc = mapFunc;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  @SuppressWarnings("UnusedException")
  public IPyObj next(PyContext ctx) {
    try {
      return mapFunc.apply(iter.next());
    } catch (NoSuchElementException ignored) {
      throw new PxStopIteration(ctx);
    }
  }
}
