package org.japy.kernel.types.coll.seq;

import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.util.PyUtil.wrap;

import java.lang.invoke.MethodHandles;
import java.math.BigInteger;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxIndexError;
import org.japy.kernel.types.exc.px.PxOverflowError;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyUnhashableObj;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

public class PyRange implements IPyImmSequence, IPyUnhashableObj, IPyNoValidationObj {
  public static final IPyClass TYPE =
      new PyBuiltinClass("range", PyRange.class, PyBuiltinClass.CALLABLE);

  private interface Range {
    IPyObj get(int index, PyContext ctx);

    int len(PyContext ctx);

    IPyIter iter(PyContext ctx);

    boolean isEmpty();
  }

  private static class SmallRange implements Range {
    private final int start;
    private final int stop;
    private final int step;

    public SmallRange(int start, int stop, int step, PyContext ctx) {
      this.start = start;
      this.stop = stop;
      this.step = step;
      if (step == 0) {
        throw PxException.valueError("step may not be zero", ctx);
      }
    }

    @Override
    public IPyObj get(int index, PyContext ctx) {
      int len = len(ctx);
      int realIndex = CollLib.checkIndex(index, len, ctx);
      return wrap(start + step * realIndex);
    }

    @Override
    public int len(PyContext ctx) {
      return isEmpty() ? 0 : (stop - start) / step;
    }

    @Override
    public IPyIter iter(PyContext ctx) {
      return new Iter(this);
    }

    @Override
    public boolean isEmpty() {
      return step > 0 ? start >= stop : start <= stop;
    }

    private static class Iter implements IPyIter, IPyNoValidationObj {
      private static final IPyClass ITER_TYPE =
          new PyBuiltinClass(
              "rangeiter", Iter.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

      private final SmallRange range;
      private int next;

      Iter(SmallRange range) {
        this.range = range;
        if (range.isEmpty()) {
          this.next = range.stop;
        } else {
          this.next = range.start;
        }
      }

      @Override
      public IPyObj next(PyContext ctx) {
        IPyObj v = wrap(next);
        if (range.step > 0) {
          if (next >= range.stop) {
            throw new PxStopIteration(ctx);
          }
        } else {
          if (next <= range.stop) {
            throw new PxStopIteration(ctx);
          }
        }
        try {
          next = Math.addExact(next, range.step);
        } catch (ArithmeticException ignored) {
          next = range.stop;
        }
        return v;
      }

      @Override
      public IPyClass type() {
        return ITER_TYPE;
      }
    }
  }

  private static class LargeRange implements Range {
    private final BigInteger start;
    private final BigInteger stop;
    private final BigInteger step;

    public LargeRange(BigInteger start, BigInteger stop, BigInteger step, PyContext ctx) {
      this.start = start;
      this.stop = stop;
      this.step = step;
      if (step.signum() == 0) {
        throw PxException.valueError("step may not be zero", ctx);
      }
    }

    @Override
    public IPyObj get(int index, PyContext ctx) {
      BigInteger len = realLen();
      BigInteger biIndex;
      if (len.compareTo(CollLib.BI_MAX_INT) <= 0) {
        int realIndex = CollLib.checkIndex(index, len.intValue(), ctx);
        biIndex = BigInteger.valueOf(realIndex);
      } else if (index < 0) {
        biIndex = BigInteger.valueOf(index).add(len);
        if (biIndex.signum() == -1 || biIndex.compareTo(len) >= 0) {
          throw new PxIndexError("list index out of range", ctx);
        }
      } else {
        biIndex = BigInteger.valueOf(index);
        if (biIndex.compareTo(len) >= 0) {
          throw new PxIndexError("list index out of range", ctx);
        }
      }
      return wrap(start.add(step.multiply(biIndex)));
    }

    private BigInteger realLen() {
      return isEmpty() ? BigInteger.ZERO : stop.subtract(start).divide(step);
    }

    @Override
    public int len(PyContext ctx) {
      BigInteger len = realLen();
      if (len.compareTo(CollLib.BI_MAX_INT) > 0) {
        throw new PxOverflowError("list is too long", ctx);
      }
      return len.intValue();
    }

    @Override
    public IPyIter iter(PyContext ctx) {
      return new Iter(this);
    }

    @Override
    public boolean isEmpty() {
      return step.signum() == 1 ? start.compareTo(stop) >= 0 : start.compareTo(stop) <= 0;
    }

    private static class Iter implements IPyIter, IPyNoValidationObj {
      private static final IPyClass ITER_TYPE =
          new PyBuiltinClass(
              "largerangeiter",
              SmallRange.Iter.class,
              MethodHandles.lookup(),
              PyBuiltinClass.INTERNAL);

      private final LargeRange range;
      private BigInteger next;

      Iter(LargeRange range) {
        this.range = range;
        if (range.isEmpty()) {
          this.next = range.stop;
        } else {
          this.next = range.start;
        }
      }

      @Override
      public IPyObj next(PyContext ctx) {
        IPyObj v = wrap(next);
        if (range.step.signum() == 1) {
          if (next.compareTo(range.stop) >= 0) {
            throw new PxStopIteration(ctx);
          }
        } else {
          if (next.compareTo(range.stop) <= 0) {
            throw new PxStopIteration(ctx);
          }
        }
        next = next.add(range.step);
        return v;
      }

      @Override
      public IPyClass type() {
        return ITER_TYPE;
      }
    }
  }

  private final Range impl;

  @Constructor
  public PyRange(Args args, PyContext ctx) {
    ArgParser.verifyNoKwArgs(args, "range", ctx);
    IPyObj argStart;
    IPyObj argStop;
    IPyObj argStep;
    switch (args.posCount()) {
      case 1:
        argStop = args.getPos(0);
        argStart = PyInt.ZERO;
        argStep = PyInt.ONE;
        break;
      case 2:
        argStart = args.getPos(0);
        argStop = args.getPos(1);
        argStep = PyInt.ONE;
        break;
      case 3:
        argStart = args.getPos(0);
        argStop = args.getPos(1);
        argStep = args.getPos(2);
        break;
      default:
        throw PxException.typeError("1, 2, or 3 arguments expected, got " + args.posCount(), ctx);
    }

    PyInt start = ArgLib.checkArgInt("range", "start", argStart, ctx);
    PyInt stop = ArgLib.checkArgInt("range", "stop", argStop, ctx);
    PyInt step = ArgLib.checkArgInt("range", "step", argStep, ctx);

    if (!start.isSmall() || !stop.isSmall() || !step.isSmall()) {
      impl = new LargeRange(start.bigIntValue(), stop.bigIntValue(), step.bigIntValue(), ctx);
    } else {
      impl = new SmallRange(start.intUnchecked(), stop.intUnchecked(), step.intUnchecked(), ctx);
    }
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj get(int index, PyContext ctx) {
    return impl.get(index, ctx);
  }

  @Override
  public IPyObj slice(IntSlice slice, PyContext ctx) {
    throw PxException.typeError("range does not support slicing", ctx);
  }

  @Override
  public int len(PyContext ctx) {
    return impl.len(ctx);
  }

  @Override
  public IPyIter iter(PyContext ctx) {
    return impl.iter(ctx);
  }

  @Override
  public boolean _canConcat(IPyObj other) {
    return false;
  }

  @Override
  public IPyObj _concat(IPyROSequence seq, PyContext ctx) {
    throw InternalErrorException.notReached();
  }

  @Override
  public IPyObj _repeat(int times, PyContext ctx) {
    throw InternalErrorException.notReached();
  }

  @Override
  public IPyObj makeEmpty(PyContext ctx) {
    throw InternalErrorException.notReached();
  }

  @Override
  public IPyObj mul(IPyObj other, PyContext ctx) {
    return NotImplemented;
  }
}
