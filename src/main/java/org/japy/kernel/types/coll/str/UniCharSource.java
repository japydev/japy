package org.japy.kernel.types.coll.str;

public interface UniCharSource extends CharSource {
  PyStr subString(int start, int stop);
}
