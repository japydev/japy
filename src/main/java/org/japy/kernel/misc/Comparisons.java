package org.japy.kernel.misc;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.CompOp;
import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.exc.px.PxException;

public final class Comparisons {
  public static PxException throwUnsupported(CompOp op, IPyObj lhs, IPyObj rhs, PyContext ctx) {
    throw PxException.typeError(
        String.format(
            "there is no %s for '%s' and '%s' objects", op.op, lhs.typeName(), rhs.typeName()),
        ctx);
  }

  private static IPyObj callCompFuncOneWay(
      IPyObj lhs, IPyObj rhs, SpecialMethod meth, CompOp op, PyContext ctx) {
    @Nullable FuncHandle h = lhs.specialMethodOrNone(meth);
    if (h == DELETED_METHOD) {
      throw throwUnsupported(op, lhs, rhs, ctx);
    }

    if (h == null) {
      return NotImplemented;
    }

    try {
      return (IPyObj) h.handle.invokeExact(lhs, rhs, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  private static IPyObj callCompFuncTwoWays(
      IPyObj lhs,
      IPyObj rhs,
      SpecialMethod meth,
      SpecialMethod reverseMeth,
      CompOp op,
      CompOp reverseOp,
      PyContext ctx) {
    @Var boolean triedReverse = false;
    if (rhs.type().isStrictSubClassRaw(lhs.type())) {
      triedReverse = true;
      IPyObj result = callCompFuncOneWay(rhs, lhs, reverseMeth, reverseOp, ctx);
      if (result != NotImplemented) {
        return result;
      }
    }

    IPyObj result = callCompFuncOneWay(lhs, rhs, meth, op, ctx);
    if (result != NotImplemented) {
      return result;
    }

    if (triedReverse) {
      return NotImplemented;
    }

    return callCompFuncOneWay(rhs, lhs, reverseMeth, reverseOp, ctx);
  }

  private static IPyObj callCompFuncTwoWays(
      IPyObj lhs, IPyObj rhs, SpecialMethod meth, CompOp op, PyContext ctx) {
    @Var boolean triedReverse = false;
    if (rhs.type().isStrictSubClassRaw(lhs.type())) {
      triedReverse = true;
      IPyObj result = callCompFuncOneWay(rhs, lhs, meth, op, ctx);
      if (result != NotImplemented) {
        return result;
      }
    }

    IPyObj result = callCompFuncOneWay(lhs, rhs, meth, op, ctx);
    if (result != NotImplemented) {
      return result;
    }

    if (triedReverse) {
      return NotImplemented;
    }

    return callCompFuncOneWay(rhs, lhs, meth, op, ctx);
  }

  public static IPyObj callEq(IPyObj lhs, IPyObj rhs, PyContext ctx) {
    return callCompFuncTwoWays(lhs, rhs, SpecialMethod.EQ, CompOp.EQ, ctx);
  }

  public static IPyObj eq(IPyObj lhs, IPyObj rhs, PyContext ctx) {
    IPyObj result = callCompFuncTwoWays(lhs, rhs, SpecialMethod.EQ, CompOp.EQ, ctx);
    return result == NotImplemented ? False : result;
  }

  public static IPyObj ne(IPyObj lhs, IPyObj rhs, PyContext ctx) {
    IPyObj result = callCompFuncTwoWays(lhs, rhs, SpecialMethod.NE, CompOp.NE, ctx);
    return result == NotImplemented ? True : result;
  }

  private static IPyObj compThrowOnNotImplemented(
      IPyObj lhs,
      IPyObj rhs,
      SpecialMethod meth,
      SpecialMethod reverseMeth,
      CompOp op,
      CompOp reverseOp,
      PyContext ctx) {
    IPyObj result = callCompFuncTwoWays(lhs, rhs, meth, reverseMeth, op, reverseOp, ctx);
    if (result == NotImplemented) {
      throw throwUnsupported(op, lhs, rhs, ctx);
    }
    return result;
  }

  public static IPyObj lt(IPyObj lhs, IPyObj rhs, PyContext ctx) {
    return compThrowOnNotImplemented(
        lhs, rhs, SpecialMethod.LT, SpecialMethod.GT, CompOp.LT, CompOp.GT, ctx);
  }

  public static IPyObj le(IPyObj lhs, IPyObj rhs, PyContext ctx) {
    return compThrowOnNotImplemented(
        lhs, rhs, SpecialMethod.LE, SpecialMethod.GE, CompOp.LE, CompOp.GE, ctx);
  }

  public static IPyObj gt(IPyObj lhs, IPyObj rhs, PyContext ctx) {
    return compThrowOnNotImplemented(
        lhs, rhs, SpecialMethod.GT, SpecialMethod.LT, CompOp.GT, CompOp.LT, ctx);
  }

  public static IPyObj ge(IPyObj lhs, IPyObj rhs, PyContext ctx) {
    return compThrowOnNotImplemented(
        lhs, rhs, SpecialMethod.GE, SpecialMethod.LE, CompOp.GE, CompOp.LE, ctx);
  }
}
