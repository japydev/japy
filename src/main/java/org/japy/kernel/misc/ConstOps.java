package org.japy.kernel.misc;

import org.japy.base.BinOp;
import org.japy.base.CompOp;
import org.japy.base.UnaryOp;
import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.obj.ObjLib;

public final class ConstOps {
  @SuppressWarnings("ConstantConditions")
  public static final PyContext CONTEXT = new PyContext(null, null);

  public static class NotSupportedException extends RuntimeException {
    @Override
    public synchronized Throwable fillInStackTrace() {
      return this;
    }
  }

  public static boolean isTrue(IPyObj obj) {
    return ObjLib.isTrue(obj, CONTEXT);
  }

  public static IPyObj binOp(BinOp op, IPyObj lhs, IPyObj rhs) {
    return Arithmetics.binOp(lhs, rhs, op, CONTEXT);
  }

  public static IPyObj unaryOp(UnaryOp op, IPyObj obj) {
    switch (op) {
      case POS:
        return Arithmetics.pos(obj, CONTEXT);
      case NEG:
        return Arithmetics.neg(obj, CONTEXT);
      case INVERT:
        return Arithmetics.invert(obj, CONTEXT);
      case ABS:
        return Arithmetics.abs(obj, CONTEXT);
    }

    throw InternalErrorException.notReached();
  }

  public static boolean compOp(CompOp op, IPyObj lhs, IPyObj rhs) {
    switch (op) {
      case EQ:
        return isTrue(Comparisons.eq(lhs, rhs, CONTEXT));
      case NE:
        return isTrue(Comparisons.ne(lhs, rhs, CONTEXT));
      case LT:
        return isTrue(Comparisons.lt(lhs, rhs, CONTEXT));
      case LE:
        return isTrue(Comparisons.le(lhs, rhs, CONTEXT));
      case GT:
        return isTrue(Comparisons.gt(lhs, rhs, CONTEXT));
      case GE:
        return isTrue(Comparisons.ge(lhs, rhs, CONTEXT));

      case IN:
      case NOT_IN:
      case IS:
      case IS_NOT:
        throw new NotSupportedException();
    }

    throw InternalErrorException.notReached();
  }
}
