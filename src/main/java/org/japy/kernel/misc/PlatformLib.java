package org.japy.kernel.misc;

import org.apache.commons.lang3.SystemUtils;

public class PlatformLib {
  public static boolean isWindows() {
    return SystemUtils.IS_OS_WINDOWS;
  }
}
