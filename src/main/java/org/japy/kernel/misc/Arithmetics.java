package org.japy.kernel.misc;

import static org.japy.kernel.types.cls.SpecialMethodTable.DELETED_METHOD;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.AugOp;
import org.japy.base.BinOp;
import org.japy.base.UnaryOp;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.cls.SpecialMethod;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.Constants;

public class Arithmetics {
  public static PxException throwUnsupported(BinOp op, IPyObj lhs, IPyObj rhs, PyContext ctx) {
    throw PxException.typeError(
        String.format(
            "unsupported operand type(s) for %s: '%s' and '%s'",
            op.op, lhs.typeName(), rhs.typeName()),
        ctx);
  }

  public static PxException throwUnsupported(AugOp op, IPyObj lhs, IPyObj rhs, PyContext ctx) {
    throw PxException.typeError(
        String.format(
            "unsupported operand type(s) for %s: '%s' and '%s'",
            op.op, lhs.typeName(), rhs.typeName()),
        ctx);
  }

  public static PxException throwUnsupported(UnaryOp op, IPyObj obj, PyContext ctx) {
    throw PxException.typeError(
        String.format("unsupported operand type for %s: '%s'", op.op, obj.typeName()), ctx);
  }

  public static IPyObj binOp(IPyObj lhs, IPyObj rhs, BinOp op, PyContext ctx) {
    switch (op) {
      case ADD:
        return add(lhs, rhs, ctx);
      case SUB:
        return sub(lhs, rhs, ctx);
      case MUL:
        return mul(lhs, rhs, ctx);
      case TRUEDIV:
        return truediv(lhs, rhs, ctx);
      case FLOORDIV:
        return floordiv(lhs, rhs, ctx);
      case MOD:
        return mod(lhs, rhs, ctx);
      case DIVMOD:
        return divmod(lhs, rhs, ctx);
      case POW:
        return pow(lhs, rhs, ctx);
      case MATMUL:
        return matmul(lhs, rhs, ctx);
      case AND:
        return and(lhs, rhs, ctx);
      case OR:
        return or(lhs, rhs, ctx);
      case XOR:
        return xor(lhs, rhs, ctx);
      case LSHIFT:
        return lshift(lhs, rhs, ctx);
      case RSHIFT:
        return rshift(lhs, rhs, ctx);
    }

    throw InternalErrorException.notReached();
  }

  private static IPyObj callBinOp(
      IPyObj lhs, IPyObj rhs, SpecialMethod m, BinOp op, PyContext ctx) {
    @Nullable FuncHandle h = lhs.specialMethodOrNone(m);
    if (h == DELETED_METHOD) {
      throw throwUnsupported(op, lhs, rhs, ctx);
    }

    if (h == null) {
      return NotImplemented;
    }

    try {
      return (IPyObj) h.handle.invokeExact(lhs, rhs, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  private static IPyObj binOp(
      IPyObj lhs,
      IPyObj rhs,
      BinOp op,
      SpecialMethod directMeth,
      SpecialMethod inverseMeth,
      PyContext ctx) {
    @Var IPyObj v = callBinOp(lhs, rhs, directMeth, op, ctx);
    if (v != NotImplemented) {
      return v;
    }

    if (lhs.type() == rhs.type()) {
      throw throwUnsupported(op, lhs, rhs, ctx);
    }

    v = callBinOp(rhs, lhs, inverseMeth, op, ctx);
    if (v != NotImplemented) {
      return v;
    }

    throw throwUnsupported(op, lhs, rhs, ctx);
  }

  public static IPyObj augOp(AugOp op, IPyObj lhs, IPyObj rhs, PyContext ctx) {
    switch (op) {
      case IADD:
        return iadd(lhs, rhs, ctx);
      case ISUB:
        return isub(lhs, rhs, ctx);
      case IMUL:
        return imul(lhs, rhs, ctx);
      case ITRUEDIV:
        return itruediv(lhs, rhs, ctx);
      case IFLOORDIV:
        return ifloordiv(lhs, rhs, ctx);
      case IMOD:
        return imod(lhs, rhs, ctx);
      case IPOW:
        return ipow(lhs, rhs, ctx);
      case IMATMUL:
        return imatmul(lhs, rhs, ctx);
      case IAND:
        return iand(lhs, rhs, ctx);
      case IOR:
        return ior(lhs, rhs, ctx);
      case IXOR:
        return ixor(lhs, rhs, ctx);
      case ILSHIFT:
        return ilshift(lhs, rhs, ctx);
      case IRSHIFT:
        return irshift(lhs, rhs, ctx);
    }

    throw InternalErrorException.notReached();
  }

  private static IPyObj augOp(
      IPyObj lhs, IPyObj rhs, AugOp op, SpecialMethod method, PyContext ctx) {
    @Nullable FuncHandle h = lhs.specialMethodOrNone(method);
    if (h == DELETED_METHOD) {
      throw throwUnsupported(op, lhs, rhs, ctx);
    }

    if (h != null) {
      IPyObj v;
      try {
        v = (IPyObj) h.handle.invokeExact(lhs, rhs, ctx);
      } catch (Throwable ex) {
        throw ExcUtil.rethrow(ex);
      }

      if (v != NotImplemented) {
        return v;
      }
    }

    return binOp(lhs, rhs, op.binOp, ctx);
  }

  public static IPyObj unaryOp(IPyObj obj, UnaryOp op, PyContext ctx) {
    switch (op) {
      case ABS:
        return unaryOp(obj, op, SpecialMethod.ABS, ctx);
      case INVERT:
        return unaryOp(obj, op, SpecialMethod.INVERT, ctx);
      case NEG:
        return unaryOp(obj, op, SpecialMethod.NEG, ctx);
      case POS:
        return unaryOp(obj, op, SpecialMethod.POS, ctx);
    }

    throw InternalErrorException.notReached();
  }

  private static IPyObj unaryOp(IPyObj obj, UnaryOp op, SpecialMethod method, PyContext ctx) {
    @Nullable FuncHandle h = obj.specialMethod(method);
    if (h == null) {
      throw throwUnsupported(op, obj, ctx);
    }

    IPyObj v;
    try {
      v = (IPyObj) h.handle.invokeExact(obj, ctx);
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }

    if (v != NotImplemented) {
      return v;
    }

    throw throwUnsupported(op, obj, ctx);
  }

  public static IPyObj pow(IPyObj base, IPyObj exp, IPyObj mod, PyContext ctx) {
    @Nullable
    IPyObj pow = ObjLib.getClassAttributeOrNull(base.type(), null, Constants.__POW__, ctx);

    if (pow != null && pow != None) {
      IPyObj v = FuncLib.call(pow, base, exp, mod, ctx);
      if (v != NotImplemented) {
        return v;
      }
    }

    throw throwUnsupported(BinOp.POW, base, exp, ctx);
  }

  // BEGIN GENERATED CODE

  public static IPyObj pow(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__pow__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RPOW, BinOp.POW, ctx);
    // }

    return binOp(arg1, arg2, BinOp.POW, SpecialMethod.POW, SpecialMethod.RPOW, ctx);
  }

  public static IPyObj ipow(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__pow__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.IPOW, SpecialMethod.IPOW, ctx);
  }

  public static IPyObj add(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__add__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RADD, BinOp.ADD, ctx);
    // }

    return binOp(arg1, arg2, BinOp.ADD, SpecialMethod.ADD, SpecialMethod.RADD, ctx);
  }

  public static IPyObj iadd(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__add__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.IADD, SpecialMethod.IADD, ctx);
  }

  public static IPyObj sub(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__sub__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RSUB, BinOp.SUB, ctx);
    // }

    return binOp(arg1, arg2, BinOp.SUB, SpecialMethod.SUB, SpecialMethod.RSUB, ctx);
  }

  public static IPyObj isub(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__sub__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.ISUB, SpecialMethod.ISUB, ctx);
  }

  public static IPyObj mul(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__mul__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RMUL, BinOp.MUL, ctx);
    // }

    return binOp(arg1, arg2, BinOp.MUL, SpecialMethod.MUL, SpecialMethod.RMUL, ctx);
  }

  public static IPyObj imul(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__mul__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.IMUL, SpecialMethod.IMUL, ctx);
  }

  public static IPyObj matmul(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    return binOp(arg1, arg2, BinOp.MATMUL, SpecialMethod.MATMUL, SpecialMethod.RMATMUL, ctx);
  }

  public static IPyObj imatmul(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    return augOp(arg1, arg2, AugOp.IMATMUL, SpecialMethod.IMATMUL, ctx);
  }

  public static IPyObj truediv(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__truediv__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RTRUEDIV, BinOp.TRUEDIV, ctx);
    // }

    return binOp(arg1, arg2, BinOp.TRUEDIV, SpecialMethod.TRUEDIV, SpecialMethod.RTRUEDIV, ctx);
  }

  public static IPyObj itruediv(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__truediv__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.ITRUEDIV, SpecialMethod.ITRUEDIV, ctx);
  }

  public static IPyObj floordiv(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__floordiv__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RFLOORDIV, BinOp.FLOORDIV, ctx);
    // }

    return binOp(arg1, arg2, BinOp.FLOORDIV, SpecialMethod.FLOORDIV, SpecialMethod.RFLOORDIV, ctx);
  }

  public static IPyObj ifloordiv(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__floordiv__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.IFLOORDIV, SpecialMethod.IFLOORDIV, ctx);
  }

  public static IPyObj mod(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__mod__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RMOD, BinOp.MOD, ctx);
    // }

    return binOp(arg1, arg2, BinOp.MOD, SpecialMethod.MOD, SpecialMethod.RMOD, ctx);
  }

  public static IPyObj imod(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__mod__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.IMOD, SpecialMethod.IMOD, ctx);
  }

  public static IPyObj divmod(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__divmod__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RDIVMOD, BinOp.DIVMOD, ctx);
    // }

    return binOp(arg1, arg2, BinOp.DIVMOD, SpecialMethod.DIVMOD, SpecialMethod.RDIVMOD, ctx);
  }

  public static IPyObj lshift(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__lshift__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RLSHIFT, BinOp.LSHIFT, ctx);
    // }

    return binOp(arg1, arg2, BinOp.LSHIFT, SpecialMethod.LSHIFT, SpecialMethod.RLSHIFT, ctx);
  }

  public static IPyObj ilshift(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__lshift__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.ILSHIFT, SpecialMethod.ILSHIFT, ctx);
  }

  public static IPyObj rshift(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__rshift__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RRSHIFT, BinOp.RSHIFT, ctx);
    // }

    return binOp(arg1, arg2, BinOp.RSHIFT, SpecialMethod.RSHIFT, SpecialMethod.RRSHIFT, ctx);
  }

  public static IPyObj irshift(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__rshift__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.IRSHIFT, SpecialMethod.IRSHIFT, ctx);
  }

  public static IPyObj and(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__and__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RAND, BinOp.AND, ctx);
    // }

    return binOp(arg1, arg2, BinOp.AND, SpecialMethod.AND, SpecialMethod.RAND, ctx);
  }

  public static IPyObj iand(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__and__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.IAND, SpecialMethod.IAND, ctx);
  }

  public static IPyObj xor(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__xor__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.RXOR, BinOp.XOR, ctx);
    // }

    return binOp(arg1, arg2, BinOp.XOR, SpecialMethod.XOR, SpecialMethod.RXOR, ctx);
  }

  public static IPyObj ixor(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__xor__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.IXOR, SpecialMethod.IXOR, ctx);
  }

  public static IPyObj or(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  IPyObj value = ((IPyNumber)arg1).__or__(arg2, ctx);
    //  if (value != NotImplemented) {
    //    return value;
    //  }
    //
    //  return callBinOp(arg2, arg1, SpecialMethod.ROR, BinOp.OR, ctx);
    // }

    return binOp(arg1, arg2, BinOp.OR, SpecialMethod.OR, SpecialMethod.ROR, ctx);
  }

  public static IPyObj ior(IPyObj arg1, IPyObj arg2, PyContext ctx) {
    // if (arg1 instanceof IPyNumber) {
    //  return ((IPyNumber)arg1).__or__(arg2, ctx);
    // }

    return augOp(arg1, arg2, AugOp.IOR, SpecialMethod.IOR, ctx);
  }

  public static IPyObj neg(IPyObj obj, PyContext ctx) {
    // if (obj instanceof IPyNumber) {
    //  return ((IPyNumber)obj).__neg__(ctx);
    // }
    return unaryOp(obj, UnaryOp.NEG, SpecialMethod.NEG, ctx);
  }

  public static IPyObj pos(IPyObj obj, PyContext ctx) {
    // if (obj instanceof IPyNumber) {
    //  return ((IPyNumber)obj).__pos__(ctx);
    // }
    return unaryOp(obj, UnaryOp.POS, SpecialMethod.POS, ctx);
  }

  public static IPyObj abs(IPyObj obj, PyContext ctx) {
    // if (obj instanceof IPyNumber) {
    //  return ((IPyNumber)obj).__abs__(ctx);
    // }
    return unaryOp(obj, UnaryOp.ABS, SpecialMethod.ABS, ctx);
  }

  public static IPyObj invert(IPyObj obj, PyContext ctx) {
    // if (obj instanceof IPyNumber) {
    //  return ((IPyNumber)obj).__invert__(ctx);
    // }
    return unaryOp(obj, UnaryOp.INVERT, SpecialMethod.INVERT, ctx);
  }

  // END GENERATED CODE
}
