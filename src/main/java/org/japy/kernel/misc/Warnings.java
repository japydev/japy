package org.japy.kernel.misc;

import org.japy.kernel.exec.PyContext;

public final class Warnings {
  // TODO zzz
  @SuppressWarnings({"UseOfSystemOutOrSystemErr", "SystemOut"})
  public static void warn(String warning, PyContext ctx) {
    System.err.printf("WARNING: %s%n", warning);
  }
}
