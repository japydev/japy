package org.japy.kernel.misc;

import static org.japy.kernel.types.misc.PyNone.None;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.misc.PySimpleNamespace;
import org.japy.kernel.util.Constants;
import org.japy.kernel.util.PyConstants;

public class SysLib {
  private static final PyTuple SYS_EXC_INFO_NO_EXC = PyTuple.of(None, None, None);

  public static IPyObj excInfo(PyContext ctx) {
    @Nullable PyBaseException exc = ctx.thread.currentException();
    if (exc == null) {
      return SYS_EXC_INFO_NO_EXC;
    }

    // TODO traceback
    return PyTuple.of(exc.type(), exc, None);
  }

  public static IPyObj getFilesystemEncoding(PyContext ctx) {
    // TODO
    return PyConstants.UTF_8;
  }

  public static IPyObj getFilesystemEncodeErrors(PyContext ctx) {
    return PyConstants.SURROGATEPASS;
  }

  public static IPyObj implementation(PyContext ctx) {
    PySimpleNamespace ns = new PySimpleNamespace();
    // TODO
    ns.dict.set("name", PyStr.ucs2(Constants.IMPL_NAME));
    return ns;
  }
}
