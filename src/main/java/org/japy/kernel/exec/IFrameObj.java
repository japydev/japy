package org.japy.kernel.exec;

import org.japy.infra.validation.ISupportsValidation;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;

public interface IFrameObj extends ISupportsValidation {
  int localCount();

  int cellCount();

  IPyDict globals();

  IPyRODict builtins();

  String[] localNames();

  String[] cellNames();
}
