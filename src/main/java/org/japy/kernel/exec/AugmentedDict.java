package org.japy.kernel.exec;

import static org.japy.infra.util.ObjUtil.or;

import java.lang.invoke.MethodHandles;
import java.util.function.BiConsumer;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.str.DeepRepr;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxKeyError;
import org.japy.kernel.types.obj.IPyNoValidationObj;

class AugmentedDict implements IPyDict, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "AugmentedDict", AugmentedDict.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private final IPyDict dict;
  private @Nullable IPyObj magicValue;

  AugmentedDict(IPyDict dict) {
    this.dict = dict;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public boolean contains(IPyObj item, PyContext ctx) {
    return item == magicValue || dict.contains(item, ctx);
  }

  private static boolean isMagicKey(IPyObj key) {
    return key instanceof PyStr && key.toString().equals(SourceCode.MAGIC_VAR);
  }

  @Override
  public void setItem(IPyObj key, IPyObj value, PyContext ctx) {
    if (isMagicKey(key)) {
      magicValue = value;
    } else {
      dict.setItem(key, value, ctx);
    }
  }

  @Override
  public void delItem(IPyObj key, PyContext ctx) {
    if (isMagicKey(key)) {
      if (magicValue == null) {
        throw new PxKeyError(key, ctx);
      }
      magicValue = null;
    } else {
      dict.delItem(key, ctx);
    }
  }

  @Override
  public IPyObj pop(IPyObj key, PyContext ctx) {
    if (isMagicKey(key)) {
      if (magicValue == null) {
        throw new PxKeyError(key, ctx);
      }
      IPyObj tmp = magicValue;
      magicValue = null;
      return tmp;
    } else {
      return dict.pop(key, ctx);
    }
  }

  @Override
  public IPyObj pop(IPyObj key, IPyObj dflt, PyContext ctx) {
    if (isMagicKey(key)) {
      return or(magicValue, dflt);
    } else {
      return dict.pop(key, dflt, ctx);
    }
  }

  @Override
  public void _fastForEach(BiConsumer<? super IPyObj, ? super IPyObj> func) {
    if (magicValue != null) {
      func.accept(magicValue, PyStr.get(SourceCode.MAGIC_VAR));
    }
    dict._fastForEach(func);
  }

  @Override
  public IPyObj getItem(IPyObj key, PyContext ctx) {
    if (isMagicKey(key)) {
      if (magicValue == null) {
        throw new PxKeyError(key, ctx);
      }
      return magicValue;
    }

    return dict.getItem(key, ctx);
  }

  @Override
  public IPyObj items(PyContext ctx) {
    return dict.items(ctx);
  }

  @Override
  public IPyObj values(PyContext ctx) {
    return dict.values(ctx);
  }

  @Override
  public IPyIter iter(PyContext ctx) {
    return dict.iter(ctx);
  }

  @Override
  public void print(DeepRepr dr, PyContext ctx) {
    dict.print(dr, ctx);
  }

  @Override
  public int len(PyContext ctx) {
    return (magicValue != null ? 1 : 0) + dict.len(ctx);
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    throw InternalErrorException.notReached();
  }
}
