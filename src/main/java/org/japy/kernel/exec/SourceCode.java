package org.japy.kernel.exec;

import static org.japy.infra.validation.Debug.dcheck;

import java.nio.charset.StandardCharsets;

import org.eclipse.collections.impl.list.mutable.primitive.ByteArrayList;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.parser.python.ScannerInput;
import org.japy.parser.python.impl.ScannerUtil;

class SourceCode implements ScannerInput {
  static final int EVAL = 1;
  static final int CLASS = 2;

  static final String MAGIC_VAR = "XN5cytJp7BGEWP30gfEyKQ";
  static final String CLASS_NAME = "XN5cytJp7BGEWP30gfEyKQQ";

  private static final String EVAL_PREFIX =
      String.format("global %s; %s = (", MAGIC_VAR, MAGIC_VAR);
  private static final String EVAL_SUFFIX = ")";
  private static final String CLASS_PREFIX = String.format("class %s:\n", CLASS_NAME);

  private final String filename;
  private final SourceText sourceText;

  SourceCode(IPyObj input, String filename, int flags) {
    this.filename = filename;

    if (input instanceof PyStr) {
      sourceText = new StrSourceText((PyStr) input);
    } else if (input instanceof PyBytes) {
      sourceText = new ByteSourceText((PyBytes) input);
    } else {
      throw InternalErrorException.notReached();
    }

    if ((flags & EVAL) != 0) {
      sourceText.makeEval();
    }

    if ((flags & CLASS) != 0) {
      sourceText.makeClass();
    }
  }

  @Override
  public String sourceFile() {
    return filename;
  }

  @Override
  public boolean isUtf8() {
    return sourceText instanceof StrSourceText;
  }

  @Override
  public byte[] getBytes() {
    if (sourceText instanceof StrSourceText) {
      return ((StrSourceText) sourceText).text.getBytes(StandardCharsets.UTF_8);
    } else {
      return ((ByteSourceText) sourceText).text;
    }
  }

  private abstract static class SourceText {
    abstract void makeEval();

    abstract void makeClass();
  }

  private static class StrSourceText extends SourceText {
    String text;

    StrSourceText(PyStr text) {
      this.text = text.toString();
    }

    @Override
    void makeEval() {
      text = EVAL_PREFIX + text + EVAL_SUFFIX;
    }

    @Override
    void makeClass() {
      StringBuilder sb = new StringBuilder(CLASS_PREFIX.length() + text.length() + 100);
      sb.append(CLASS_PREFIX);
      ScannerUtil.indent(text, sb);
      text = sb.toString();
    }
  }

  private static class ByteSourceText extends SourceText {
    byte[] text;

    ByteSourceText(PyBytes bytes) {
      text = bytes.internalArray();
    }

    @Override
    void makeEval() {
      int newSize = text.length + EVAL_PREFIX.length() + EVAL_SUFFIX.length();
      ByteArrayList newText = new ByteArrayList(newSize);
      newText.addAll(EVAL_PREFIX.getBytes(StandardCharsets.UTF_8));
      newText.addAll(text);
      newText.addAll(EVAL_SUFFIX.getBytes(StandardCharsets.UTF_8));
      dcheck(newText.size() == newSize);
      text = newText.toArray();
    }

    @Override
    void makeClass() {
      ScannerUtil.ByteWriter newText =
          new ScannerUtil.ByteWriter(CLASS_PREFIX.length() + text.length + 100);
      newText.write(CLASS_PREFIX.getBytes(StandardCharsets.UTF_8));
      ScannerUtil.indent(text, newText);
      text = newText.toByteArray();
    }
  }
}
