package org.japy.kernel.exec;

import static org.japy.infra.util.ObjUtil.or;
import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.misc.PyNone.None;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.file.Path;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.CompilerConfig;
import org.japy.compiler.PackageCompiler;
import org.japy.compiler.code.CPackage;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Debug;
import org.japy.jexec.obj.JClassExec;
import org.japy.jexec.obj.JModuleExec;
import org.japy.jexec.pkg.JPackage;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxRuntimeError;
import org.japy.kernel.types.exc.px.PxSyntaxError;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.mod.PyModule;
import org.japy.kernel.util.Constants;
import org.japy.kernel.util.PyConstants;
import org.japy.parser.python.Parser;
import org.japy.parser.python.ast.expr.IAstExpr;

public class ExecLib {
  private enum RunMode {
    EXEC,
    EVAL,
  }

  public enum CompileMode {
    EVAL,
    SINGLE,
    EXEC,
  }

  public static IPyObj compile(
      IPyObj source,
      String filename,
      CompileMode mode,
      int flags,
      boolean dontInherit,
      @Nullable OptimizationLevel optLevel,
      PyContext ctx) {

    CompilerConfig compilerConfig = makeCompilerConfig(flags, dontInherit, optLevel, ctx);

    switch (mode) {
      case EXEC:
        return compileExec(source, filename, compilerConfig, ctx);
      case EVAL:
        return compileEval(source, filename, compilerConfig, ctx);
      case SINGLE:
        throw new NotImplementedException("compile " + mode);
    }

    throw InternalErrorException.notReached();
  }

  private static PyCodeObject compileExec(
      IPyObj source, String filename, CompilerConfig config, PyContext ctx) {

    CPackage cpkg =
        PxSyntaxError.wrap(
            () -> {
              if (source instanceof PyStr) {
                return PackageCompiler.compile(source.toString(), filename, config, ctx);
              } else if (source instanceof PyBytes) {
                return PackageCompiler.compile(
                    ((PyBytes) source).internalArray(), filename, config, ctx);
              } else {
                throw InternalErrorException.notReached();
              }
            },
            ctx);

    JPackage jpkg = new JPackage(cpkg, ctx.kernel.config.codegen);
    return PyCodeObject.exec(jpkg, cpkg);
  }

  private static PyCodeObject compileEval(
      IPyObj source, String filename, CompilerConfig compilerConfig, PyContext ctx) {

    IAstExpr expr =
        PxSyntaxError.wrap(
            () -> {
              if (source instanceof PyStr) {
                return Parser.parseExpression(source.toString(), filename);
              } else if (source instanceof PyBytes) {
                return Parser.parseExpression(((PyBytes) source).internalArray(), filename);
              } else {
                throw InternalErrorException.notReached();
              }
            },
            ctx);

    return PyCodeObject.eval(compilerConfig, ctx.kernel.config.codegen, expr);
  }

  private static CompilerConfig makeCompilerConfig(
      int flags, boolean dontInherit, @Nullable OptimizationLevel optLevel, PyContext ctx) {
    if (flags != 0) {
      throw new NotImplementedException("compile with flags");
    }
    if (dontInherit) {
      throw new NotImplementedException("compile with dont_inherit");
    }
    return CompilerConfig.builder(ctx.kernel.config.compiler)
        .optimizationLevel(or(optLevel, ctx.kernel.config.optimizationLevel))
        .build();
  }

  private static void runCode(
      CPackage cpkg,
      boolean classMode,
      IPyDict globals,
      IPyObj locals,
      IPyRODict builtins,
      PyContext ctx) {

    JPackage jpkg = new JPackage(cpkg, ctx.kernel.config.codegen);
    JModuleExec moduleExec = JModuleExec.create(jpkg, cpkg.module.binName);

    if (!classMode) {
      moduleExec.exec(globals, builtins, ctx);
    } else {
      JClassExec classExec = JClassExec.make(moduleExec, SourceCode.CLASS_NAME, Cell.EMPTY_ARRAY);
      Cell classCell = new Cell();
      classExec.exec(
          locals instanceof IPyDict ? (IPyDict) locals : new DictObjProxy(locals),
          globals,
          builtins,
          classCell,
          ctx);
    }
  }

  private static IPyObj runCode(
      IPyObj code,
      RunMode mode,
      IPyDict globals,
      IPyObj locals,
      IPyRODict builtins,
      String filename,
      PyContext ctx) {

    if (!(code instanceof PyStr) && !(code instanceof PyBytes)) {
      throw new NotImplementedException("code objects");
    }

    boolean classMode = globals != locals;
    SourceCode sourceCode =
        new SourceCode(
            code,
            filename,
            (mode == RunMode.EVAL ? SourceCode.EVAL : 0) | (classMode ? SourceCode.CLASS : 0));

    IPyDict realGlobals = mode == RunMode.EVAL ? new AugmentedDict(globals) : globals;

    CPackage cpkg =
        PxSyntaxError.wrap(
            () -> PackageCompiler.compile(sourceCode, ctx.kernel.config.compiler, ctx), ctx);

    runCode(cpkg, classMode, realGlobals, locals, builtins, ctx);

    if (mode == RunMode.EXEC) {
      return None;
    } else {
      return realGlobals.getItem(SourceCode.MAGIC_VAR, ctx);
    }
  }

  @CanIgnoreReturnValue
  private static IPyObj runCode(
      String func,
      IPyObj code,
      RunMode mode,
      @Nullable IPyObj globalsObj,
      @Nullable IPyObj localsArg,
      @Nullable String filename,
      PyContext ctx) {
    @Nullable
    IPyDict globalsArg =
        globalsObj != null ? ArgLib.checkArgDict(func, "globals", globalsObj, ctx) : null;

    IPyDict globals;
    IPyObj locals;
    if (globalsArg == null) {
      dcheck(localsArg == null);
      IFrame frame = ctx.thread.frame();
      globals = frame.globals();
      if (frame.hasLocals()) {
        locals = frame.locals();
      } else {
        throw new PxRuntimeError(
            "locals are not available, cannot exec/eval in current context", ctx);
      }
    } else {
      globals = globalsArg;
      locals = or(localsArg, globals);
      if (!globals.contains(PyConstants.__NAME__, ctx)) {
        globals.setItem(PyConstants.__NAME__, PyConstants.__MAIN__, ctx);
      }
    }

    IPyRODict builtins;
    @Nullable IPyObj builtinsObj = globals.getOrNull(PyConstants.__BUILTINS__, ctx);
    if (builtinsObj == null) {
      builtins = ctx.kernel.builtins().moduleDict();
    } else if (!(builtinsObj instanceof IPyRODict)) {
      throw PxException.typeError("__builtins__ must be a dictionary", ctx);
    } else {
      builtins = (IPyRODict) builtinsObj;
    }

    return runCode(
        code,
        mode,
        globals,
        locals,
        builtins,
        or(filename, org.japy.compiler.impl.Constants.INPUT),
        ctx);
  }

  public static void exec(
      IPyObj code,
      @Nullable IPyObj globals,
      @Nullable IPyObj locals,
      @Nullable IPyObj filename,
      PyContext ctx) {
    runCode(
        "exec",
        code,
        RunMode.EXEC,
        globals,
        locals,
        filename != null ? filename.toString() : null,
        ctx);
  }

  public static IPyObj eval(
      IPyObj code, @Nullable IPyObj globalsObj, @Nullable IPyObj localsObj, PyContext ctx) {
    if (!(code instanceof PyStr)) {
      throw new NotImplementedException("code objects");
    }

    @Nullable IPyObj value = ExprInterpreter.tryEval((PyStr) code, ctx);
    if (value != null) {
      return value;
    }

    return runCode("eval", code, RunMode.EVAL, globalsObj, localsObj, null, ctx);
  }

  private static IPyRODict builtinsFromContext(PyContext ctx) {
    return ctx.thread.hasFrame()
        ? ctx.thread.frame().builtins()
        : ctx.kernel.builtins().moduleDict();
  }

  private static byte[] readEmbedded(String path, Class<?> moduleClass) {
    try (@Nullable InputStream is = moduleClass.getResourceAsStream(path)) {
      if (is == null) {
        throw new InternalErrorException("could not find resource file " + path);
      }
      return is.readAllBytes();
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    }
  }

  public static void execEmbedded(PyModule module, PyContext ctx) {
    String name = module.name();
    String path = Constants.EMBEDDED_MODULE_DIR + "/" + name + ".py";
    byte[] code = readEmbedded(path, module.getClass());
    String sourcePath = Debug.ENABLED ? "src/main/resources" + path : name + ".py";
    CPackage cpkg =
        PxSyntaxError.wrap(
            () -> PackageCompiler.compile(code, sourcePath, ctx.kernel.config.compiler, ctx), ctx);
    JPackage jpkg = new JPackage(cpkg, ctx.kernel.config.codegen);
    JModuleExec exec = JModuleExec.create(jpkg, cpkg.module.binName);
    exec.exec(module.moduleDict(), builtinsFromContext(ctx), ctx);
  }

  public static void execFile(PyModule module, Path path, PyContext ctx) {
    CPackage cpkg =
        PxSyntaxError.wrap(
            () -> PackageCompiler.compile(path, ctx.kernel.config.compiler, ctx), ctx);
    JPackage jpkg = new JPackage(cpkg, ctx.kernel.config.codegen);
    JModuleExec exec = JModuleExec.create(jpkg, cpkg.module.binName);
    exec.exec(module.moduleDict(), builtinsFromContext(ctx), ctx);
  }
}
