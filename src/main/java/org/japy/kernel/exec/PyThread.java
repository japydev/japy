package org.japy.kernel.exec;

import static org.japy.infra.validation.Debug.dcheck;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Debug;
import org.japy.infra.validation.ISupportsValidation;
import org.japy.infra.validation.Validator;
import org.japy.kernel.types.exc.py.PyBaseException;

public final class PyThread implements ISupportsValidation {
  private final ThreadStack stack = new ThreadStack();
  private final FrameCache frameCache = new FrameCache();
  private @Nullable PyBaseException currentException;

  public void push(IFrame frame) {
    stack.push(frame);
  }

  public Frame push(IFrameObj obj) {
    Frame frame = frameCache.push(obj);
    stack.push(frame);
    return frame;
  }

  public void pop(IFrame frame) {
    if (Debug.ENABLED) {
      dcheck(frame == frame());
    }
    stack.pop();
  }

  public void pop(Frame frame) {
    if (Debug.ENABLED) {
      dcheck(frame == frame());
    }
    stack.pop();
    frameCache.pop(frame);
  }

  public boolean hasFrame() {
    return !stack.isEmpty();
  }

  public IFrame frame() {
    return stack.frame();
  }

  public @Nullable PyBaseException currentException() {
    return currentException;
  }

  public @Nullable PyBaseException setCurrentException(@Nullable PyBaseException exc) {
    @Nullable PyBaseException old = currentException;
    currentException = exc;
    return old;
  }

  @Override
  public void validate(Validator v) {
    v.validate(stack);
  }
}
