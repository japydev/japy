package org.japy.kernel.exec;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.ISupportsValidation;
import org.japy.infra.validation.Validator;
import org.japy.kernel.types.IPyObj;

public final class Cell implements ISupportsValidation {
  public static final Cell[] EMPTY_ARRAY = new Cell[0];

  public @Nullable IPyObj value;

  public Cell() {}

  public Cell(IPyObj value) {
    this.value = value;
  }

  @Override
  public void validate(Validator v) {
    v.validateIfNotNull(value);
  }

  @Override
  public String toString() {
    return value != null ? value.toString() : "NULL";
  }
}
