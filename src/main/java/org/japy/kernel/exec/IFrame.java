package org.japy.kernel.exec;

import org.japy.infra.validation.ISupportsValidation;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;

public interface IFrame extends ISupportsValidation {
  IPyDict globals();

  IPyRODict builtins();

  boolean hasLocals();

  IPyDict locals();
}
