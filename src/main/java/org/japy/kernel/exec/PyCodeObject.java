package org.japy.kernel.exec;

import static org.japy.infra.validation.Debug.dcheck;

import java.lang.invoke.MethodHandles;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.CompilerConfig;
import org.japy.compiler.code.CPackage;
import org.japy.jexec.codegen.CodegenConfig;
import org.japy.jexec.pkg.JPackage;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.parser.python.ast.expr.IAstExpr;

class PyCodeObject implements IPyTrueAtomObj, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "code", PyCodeObject.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private static class ExecCode {
    final JPackage jpkg;
    final String moduleBinName;

    private ExecCode(JPackage jpkg, String moduleBinName) {
      this.jpkg = jpkg;
      this.moduleBinName = moduleBinName;
    }
  }

  private static class EvalCode {
    final CompilerConfig compilerConfig;
    final CodegenConfig codegenConfig;
    final IAstExpr expr;

    private EvalCode(CompilerConfig compilerConfig, CodegenConfig codegenConfig, IAstExpr expr) {
      this.compilerConfig = compilerConfig;
      this.codegenConfig = codegenConfig;
      this.expr = expr;
    }
  }

  private final @Nullable ExecCode exec;
  private final @Nullable EvalCode eval;

  private PyCodeObject(@Nullable ExecCode exec, @Nullable EvalCode eval) {
    dcheck((exec == null) == (eval != null));
    this.exec = exec;
    this.eval = eval;
  }

  static PyCodeObject exec(JPackage jpkg, CPackage cpkg) {
    return new PyCodeObject(new ExecCode(jpkg, cpkg.module.binName), null);
  }

  static PyCodeObject eval(
      CompilerConfig compilerConfig, CodegenConfig codegenConfig, IAstExpr expr) {
    return new PyCodeObject(null, new EvalCode(compilerConfig, codegenConfig, expr));
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
