package org.japy.kernel.exec;

import static org.japy.infra.coll.CollUtil.last;
import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import org.japy.infra.validation.ISupportsValidation;
import org.japy.infra.validation.Validator;

final class ThreadStack implements ISupportsValidation {
  private final List<IFrame> frames = new ArrayList<>(50);

  boolean isEmpty() {
    return frames.isEmpty();
  }

  void pop() {
    dcheck(!isEmpty());
    frames.remove(frames.size() - 1);
  }

  void push(IFrame frame) {
    frames.add(frame);
  }

  IFrame frame() {
    return last(frames);
  }

  @Override
  public void validate(Validator v) {
    frames.forEach(v::validate);
  }
}
