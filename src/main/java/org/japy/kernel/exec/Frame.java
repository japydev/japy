package org.japy.kernel.exec;

import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Validator;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;

public final class Frame implements IFrame {
  private static final IFrameObj INVALID_OBJ = new InvalidObj();

  IFrameObj obj = INVALID_OBJ;
  private final List<@Nullable IPyObj> locals;
  private final List<Cell> cells;
  private int firstLocal;
  private int firstCell;

  Frame(List<@Nullable IPyObj> locals, List<Cell> cells) {
    this.locals = locals;
    this.cells = cells;
  }

  @Nullable
  public IPyObj getLocal(int index) {
    return locals.get(firstLocal + index);
  }

  public void setLocal(int index, @Nullable IPyObj obj) {
    locals.set(firstLocal + index, obj);
  }

  public Cell getCell(int index) {
    return cells.get(firstCell + index);
  }

  void init(IFrameObj obj, int localOffset, int cellOffset) {
    this.obj = obj;
    this.firstLocal = localOffset;
    this.firstCell = cellOffset;
  }

  void clear() {
    this.obj = INVALID_OBJ;
  }

  @Override
  public void validate(Validator v) {
    v.validate(obj);
    for (int i = 0, c = obj.localCount(); i != c; ++i) {
      v.validateIfNotNull(getLocal(i));
    }
    for (int i = 0, c = obj.cellCount(); i != c; ++i) {
      v.validateIfNotNull(getCell(i).value);
    }
  }

  @Override
  public IPyDict globals() {
    return obj.globals();
  }

  @Override
  public IPyRODict builtins() {
    return obj.builtins();
  }

  @Override
  public boolean hasLocals() {
    return true;
  }

  @Override
  public IPyDict locals() {
    throw new NotImplementedException("locals");
  }

  private static class InvalidObj implements IFrameObj {
    @Override
    public void validate(Validator v) {
      throw InternalErrorException.notReached();
    }

    @Override
    public int localCount() {
      throw InternalErrorException.notReached();
    }

    @Override
    public int cellCount() {
      throw InternalErrorException.notReached();
    }

    @Override
    public IPyDict globals() {
      throw InternalErrorException.notReached();
    }

    @Override
    public IPyRODict builtins() {
      throw InternalErrorException.notReached();
    }

    @Override
    public String[] localNames() {
      throw InternalErrorException.notReached();
    }

    @Override
    public String[] cellNames() {
      throw InternalErrorException.notReached();
    }
  }
}
