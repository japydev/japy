package org.japy.kernel.exec;

import org.japy.kernel.Kernel;

public final class PyContext implements AutoCloseable {
  public final Kernel kernel;
  public final PyThread thread;

  public PyContext(Kernel kernel, PyThread thread) {
    this.kernel = kernel;
    this.thread = thread;
  }

  @Override
  public void close() {}
}
