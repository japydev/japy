package org.japy.kernel.exec;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.Nullable;

import org.japy.infra.validation.Debug;
import org.japy.kernel.types.IPyObj;

final class FrameCache {
  private int frameCount;
  private final List<Frame> frames = new ArrayList<>();
  private final List<@Nullable IPyObj> locals = new ArrayList<>();
  private final List<Cell> cells = new ArrayList<>();

  Frame push(IFrameObj obj) {
    if (frameCount == frames.size()) {
      frames.add(new Frame(locals, cells));
    }
    Frame frame = frames.get(frameCount++);
    int localCount = obj.localCount();
    int cellCount = obj.cellCount();
    frame.init(obj, locals.size(), cells.size());
    for (int i = 0; i != localCount; ++i) {
      locals.add(null);
    }
    for (int i = 0; i != cellCount; ++i) {
      cells.add(new Cell());
    }
    return frame;
  }

  void pop(Frame frame) {
    if (Debug.ENABLED) {
      dcheck(frames.get(frameCount - 1) == frame);
    }
    for (int i = 0, c = frame.obj.localCount(); i != c; ++i) {
      locals.remove(locals.size() - 1);
    }
    for (int i = 0, c = frame.obj.cellCount(); i != c; ++i) {
      cells.remove(cells.size() - 1);
    }
    frame.clear();
    --frameCount;
  }
}
