package org.japy.kernel.exec;

public enum OptimizationLevel {
  ZERO(0),
  ONE(1),
  TWO(2),
  ;

  public final int value;

  OptimizationLevel(int value) {
    this.value = value;
  }
}
