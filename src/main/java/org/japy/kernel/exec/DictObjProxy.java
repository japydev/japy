package org.japy.kernel.exec;

import java.lang.invoke.MethodHandles;
import java.util.function.BiConsumer;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.str.DeepRepr;
import org.japy.kernel.types.obj.IPyNoValidationObj;

class DictObjProxy implements IPyDict, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "dictobjproxy", DictObjProxy.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private final IPyObj obj;

  DictObjProxy(IPyObj obj) {
    this.obj = obj;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public boolean contains(IPyObj item, PyContext ctx) {
    throw new NotImplementedException("dictobjproxy");
  }

  @Override
  public void setItem(IPyObj key, IPyObj value, PyContext ctx) {
    CollLib.setItem(obj, key, value, ctx);
  }

  @Override
  public void delItem(IPyObj key, PyContext ctx) {
    throw new NotImplementedException("dictobjproxy");
  }

  @Override
  public IPyObj pop(IPyObj key, PyContext ctx) {
    throw new NotImplementedException("dictobjproxy");
  }

  @Override
  public IPyObj pop(IPyObj key, IPyObj dflt, PyContext ctx) {
    throw new NotImplementedException("dictobjproxy");
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    throw new NotImplementedException("dictobjproxy");
  }

  @Override
  public void _fastForEach(BiConsumer<? super IPyObj, ? super IPyObj> func) {
    throw new NotImplementedException("dictobjproxy");
  }

  @Override
  public IPyObj getItem(IPyObj key, PyContext ctx) {
    return CollLib.getItem(obj, key, ctx);
  }

  @Override
  public IPyObj items(PyContext ctx) {
    throw new NotImplementedException("dictobjproxy");
  }

  @Override
  public IPyObj values(PyContext ctx) {
    throw new NotImplementedException("dictobjproxy");
  }

  @Override
  public IPyIter iter(PyContext ctx) {
    throw new NotImplementedException("dictobjproxy");
  }

  @Override
  public void print(DeepRepr dr, PyContext ctx) {
    throw new NotImplementedException("dictobjproxy");
  }

  @Override
  public int len(PyContext ctx) {
    throw new NotImplementedException("dictobjproxy");
  }
}
