package org.japy.kernel.exec;

import static org.japy.kernel.types.misc.PyNone.None;

import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.LogicalOp;
import org.japy.compiler.impl.Constants;
import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.misc.Arithmetics;
import org.japy.kernel.misc.Comparisons;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.set.PySet;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.coll.str.PyStrBuilder;
import org.japy.kernel.types.exc.px.PxSyntaxError;
import org.japy.kernel.types.misc.PySlice;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.parser.python.Parser;
import org.japy.parser.python.ast.expr.*;
import org.japy.parser.python.ast.node.*;
import org.japy.parser.python.ast.visit.AstExprVisit;
import org.japy.parser.python.ast.visit.BaseAstExprEval;

class ExprInterpreter {
  private static class CantEval extends RuntimeException {
    @Override
    public synchronized Throwable fillInStackTrace() {
      return this;
    }
  }

  static @Nullable IPyObj tryEval(PyStr code, PyContext ctx) {
    IAstExpr expr =
        PxSyntaxError.wrap(() -> Parser.parseExpression(code.toString(), Constants.INPUT), ctx);
    try {
      return new Eval(ctx).eval(expr);
    } catch (CantEval ignored) {
      return null;
    }
  }

  private static class Eval extends BaseAstExprEval<IPyObj> {
    private final PyContext ctx;

    private Eval(PyContext ctx) {
      this.ctx = ctx;
    }

    IPyObj eval(IAstExpr expr) {
      return AstExprVisit.eval(expr, this);
    }

    @Override
    public IPyObj visitAny(IAstExpr expr) {
      throw new CantEval();
    }

    @Override
    public IPyObj visit(AstConst expr) {
      return expr.value;
    }

    @Override
    public IPyObj visit(AstString expr) {
      if (expr.chunks.length == 1 && expr.chunks[0] instanceof AstStringLiteral) {
        return PyStr.get(((AstStringLiteral) expr.chunks[0]).text);
      }
      PyStrBuilder sb = new PyStrBuilder();
      for (IAstStringChunk chunk : expr.chunks) {
        if (chunk instanceof AstStringLiteral) {
          sb.appendUnknown(((AstStringLiteral) chunk).text);
        } else {
          throw new CantEval();
        }
      }
      return sb.build();
    }

    @Override
    public IPyObj visit(AstList expr) {
      List<IPyObj> objects = new ArrayList<>(expr.entries.length);
      for (IAstExpr elem : expr.entries) {
        objects.add(eval(elem));
      }
      return PyList.take(objects);
    }

    @Override
    public IPyObj visit(AstSet expr) {
      PySet set = new PySet(expr.entries.length);
      for (IAstExpr elem : expr.entries) {
        set.add(eval(elem), ctx);
      }
      return set;
    }

    @Override
    public IPyObj visit(AstDict expr) {
      PyDict dict = new PyDict(expr.entries.length);
      for (IAstDictEntry entry : expr.entries) {
        if (!(entry instanceof AstKeyDatum)) {
          throw new CantEval();
        }
        AstKeyDatum d = (AstKeyDatum) entry;
        IPyObj key = eval(d.key);
        IPyObj value = eval(d.value);
        dict.setItem(key, value, ctx);
      }
      return dict;
    }

    @Override
    public IPyObj visit(AstTuple expr) {
      int count = expr.entries.length;
      IPyObj[] objects = new IPyObj[count];
      for (int i = 0; i != count; ++i) {
        objects[i] = eval(expr.entries[i]);
      }
      return new PyTuple(objects);
    }

    @Override
    public IPyObj visit(AstParen expr) {
      return eval(expr.expr);
    }

    @Override
    public IPyObj visit(AstNot expr) {
      return ObjLib.not(eval(expr.expr), ctx);
    }

    @Override
    public IPyObj visit(AstComparison expr) {
      if (expr.ops.length > 1) {
        throw new CantEval();
      }

      IPyObj lhs = eval(expr.operands[0]);
      IPyObj rhs = eval(expr.operands[1]);

      switch (expr.ops[0]) {
        case EQ:
          return Comparisons.eq(lhs, rhs, ctx);
        case NE:
          return Comparisons.ne(lhs, rhs, ctx);
        case LT:
          return Comparisons.lt(lhs, rhs, ctx);
        case LE:
          return Comparisons.le(lhs, rhs, ctx);
        case GT:
          return Comparisons.gt(lhs, rhs, ctx);
        case GE:
          return Comparisons.ge(lhs, rhs, ctx);
        case IS:
          return PyBool.of(lhs == rhs);
        case IS_NOT:
          return PyBool.of(lhs != rhs);
        case IN:
          return CollLib.in(lhs, rhs, ctx);
        case NOT_IN:
          return CollLib.notIn(lhs, rhs, ctx);
      }

      throw InternalErrorException.notReached();
    }

    @Override
    public IPyObj visit(AstBinOp expr) {
      IPyObj lhs = eval(expr.lhs);
      IPyObj rhs = eval(expr.rhs);
      return Arithmetics.binOp(lhs, rhs, expr.op, ctx);
    }

    @Override
    public IPyObj visit(AstUnaryOp expr) {
      return Arithmetics.unaryOp(eval(expr.operand), expr.op, ctx);
    }

    @Override
    public IPyObj visit(AstLogicalOp expr) {
      IPyObj lhs = eval(expr.lhs);
      boolean lhsTrue = ObjLib.isTrue(lhs, ctx);
      return lhsTrue ^ (expr.op == LogicalOp.AND) ? lhs : eval(expr.rhs);
    }

    @Override
    public IPyObj visit(AstConditional expr) {
      return ObjLib.isTrue(eval(expr.condition), ctx) ? eval(expr.ifTrue) : eval(expr.ifFalse);
    }

    @Override
    public IPyObj visit(AstSlice expr) {
      IPyObj start = expr.lowerBound != null ? eval(expr.lowerBound) : None;
      IPyObj stop = expr.upperBound != null ? eval(expr.upperBound) : None;
      IPyObj step = expr.step != null ? eval(expr.step) : None;
      return new PySlice(start, stop, step);
    }
  }
}
