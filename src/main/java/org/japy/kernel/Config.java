package org.japy.kernel;

import org.japy.compiler.CompilerConfig;
import org.japy.infra.coll.CollUtil;
import org.japy.jexec.codegen.CodegenConfig;
import org.japy.kernel.exec.OptimizationLevel;

public final class Config {
  public final OptimizationLevel optimizationLevel;
  public final String[] libDirs;
  public final boolean allowNotImplementedBool;
  public final CodegenConfig codegen;
  public final CompilerConfig compiler;

  private Config(Builder builder) {
    libDirs = builder.libDirs;
    optimizationLevel = builder.optimizationLevel;
    allowNotImplementedBool = builder.allowNotImplementedBool;
    codegen = builder.codegen.build();
    compiler = builder.compiler.build();
  }

  public boolean enableDebug() {
    return optimizationLevel == OptimizationLevel.ZERO;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    private String[] libDirs = CollUtil.EMPTY_STRING_ARRAY;
    private OptimizationLevel optimizationLevel = OptimizationLevel.ZERO;
    private final boolean allowNotImplementedBool = false;
    public final CodegenConfig.Builder codegen = CodegenConfig.builder();
    public final CompilerConfig.Builder compiler = CompilerConfig.builder();

    private Builder() {}

    public Config build() {
      return new Config(this);
    }

    public Builder libDirs(String[] libDirs) {
      this.libDirs = libDirs;
      return this;
    }

    public Builder optimizationLevel(OptimizationLevel optimizationLevel) {
      this.optimizationLevel = optimizationLevel;
      compiler.optimizationLevel(optimizationLevel);
      return this;
    }
  }
}
