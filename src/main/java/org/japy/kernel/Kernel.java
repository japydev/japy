package org.japy.kernel;

import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.exec.PyThread;
import org.japy.kernel.types.cls.ext.ExtClassCache;
import org.japy.kernel.types.cls.ext.IExtClassCache;
import org.japy.kernel.types.mod.IPyModule;
import org.japy.kernel.util.Constants;
import org.japy.kernel.util.PyValidator;
import org.japy.lib.IModuleRegistry;
import org.japy.lib.ModuleRegistry;
import org.japy.lib.imp.ImportLib;

public final class Kernel implements AutoCloseable {
  public final Config config;
  public final PyThread mainThread;
  public final IExtClassCache classCache;
  public final IModuleRegistry moduleRegistry;

  public Kernel(Config config) {
    this.config = config;
    mainThread = new PyThread();
    classCache = new ExtClassCache();

    try (PyContext ctx = new PyContext(this, mainThread)) {
      moduleRegistry = new ModuleRegistry(ctx);
      ImportLib.importModule(Constants.__JAPY__, ctx);
    }

    if (Debug.ENABLED) {
      validate();
    }
  }

  public IPyModule builtins() {
    return moduleRegistry.builtins();
  }

  public IPyModule sys() {
    return moduleRegistry.sys();
  }

  @Override
  public void close() {
    ((ModuleRegistry) moduleRegistry).shutdown();
  }

  public void validate() {
    PyValidator v = new PyValidator();
    v.validate(moduleRegistry);
    v.validate(mainThread);
  }
}
