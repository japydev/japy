package org.japy.parser;

import static org.japy.infra.validation.Debug.dcheck;

import com.google.errorprone.annotations.Var;

import org.japy.infra.loc.IHasLocation;
import org.japy.infra.validation.Debug;

public class ParserUtil {
  public static boolean isAsciiDigit(int c) {
    return c >= '0' && c <= '9';
  }

  @SuppressWarnings("UnusedException")
  public static int parseAsciiNumber(String s, IHasLocation location) {
    if (Debug.ENABLED) {
      dcheck(!s.isEmpty());
    }

    @Var int value = 0;
    for (int i = 0, c = s.length(); i != c; ++i) {
      char ch = s.charAt(i);
      if (Debug.ENABLED) {
        dcheck(isAsciiDigit(ch));
      }
      try {
        value = (ch - '0') + Math.multiplyExact(value, 10);
      } catch (ArithmeticException ex) {
        throw new ParserError("number is too large: " + s, location);
      }
    }
    return value;
  }
}
