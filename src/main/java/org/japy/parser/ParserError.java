package org.japy.parser;

import org.japy.compiler.SyntaxError;
import org.japy.infra.loc.IHasLocation;

public class ParserError extends SyntaxError {
  public ParserError(String message, IHasLocation location) {
    super(message, location);
  }

  public ParserError(String message, IHasLocation location, Throwable cause) {
    super(message, location, cause);
  }
}
