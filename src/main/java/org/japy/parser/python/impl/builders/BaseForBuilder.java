package org.japy.parser.python.impl.builders;

import java.util.ArrayList;
import java.util.List;

import com.google.errorprone.annotations.Var;

import org.japy.parser.python.ast.expr.AstStar;
import org.japy.parser.python.ast.expr.AstTuple;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.impl.ParserUtil;
import org.japy.parser.python.impl.builders.ast.TAstTarget;
import org.japy.parser.python.impl.javacc.Token;

public abstract class BaseForBuilder extends ParserUtil implements ITargetConsumer {
  private final List<IAstExpr> targets = new ArrayList<>();
  private boolean trailingComma;

  @Override
  public void target(TAstTarget tgt) {
    @Var IAstExpr e = tgt.expr;
    for (int i = 0; i < tgt.stars.size(); ++i) {
      Token star = tgt.stars.get(tgt.stars.size() - 1 - i);
      e = new AstStar(e, t(star));
    }
    targets.add(e);
  }

  @Override
  public void trailingComma() {
    trailingComma = true;
  }

  protected IAstExpr getTarget() {
    return targets.size() > 1 || trailingComma
        ? new AstTuple(targets.toArray(IAstExpr[]::new))
        : targets.get(0);
  }
}
