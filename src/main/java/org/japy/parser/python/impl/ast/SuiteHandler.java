package org.japy.parser.python.impl.ast;

import org.japy.parser.python.ast.stmt.IAstStmt;

interface SuiteHandler {
  void add(IAstStmt stmt);
}
