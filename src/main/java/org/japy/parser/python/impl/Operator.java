package org.japy.parser.python.impl;

import org.japy.parser.python.impl.javacc.Token;

enum Operator {
  ADD("+", Token.ADD),
  SUB("-", Token.SUB),
  MUL("*", Token.MUL),
  POW("**", Token.POW),
  DIV("/", Token.DIV),
  FLOORDIV("//", Token.FLOORDIV),
  MOD("%", Token.MOD),
  AT("@", Token.AT),
  LSHIFT("<<", Token.LSHIFT),
  RSHIFT(">>", Token.RSHIFT),
  BAND("&", Token.BAND),
  BOR("|", Token.BOR),
  XOR("^", Token.XOR),
  INV("~", Token.INV),
  WALRUS(":=", Token.WALRUS),
  LT("<", Token.LT),
  GT(">", Token.GT),
  LE("<=", Token.LE),
  GE(">=", Token.GE),
  EQ("==", Token.EQ),
  NE("!=", Token.NE),
  ASSIGN("=", Token.ASSIGN),
  IADD("+=", Token.IADD),
  ISUB("-=", Token.ISUB),
  IMUL("*=", Token.IMUL),
  IDIV("/=", Token.IDIV),
  IFLOORDIV("//=", Token.IFLOORDIV),
  IPOW("**=", Token.IPOW),
  IMOD("%=", Token.IMOD),
  IBAND("&=", Token.IBAND),
  IBOR("|=", Token.IBOR),
  IXOR("^=", Token.IXOR),
  IMATUL("@=", Token.IMATMUL),
  ILSHIFT("<<=", Token.ILSHIFT),
  IRSHIFT(">>=", Token.IRSHIFT),
  ;

  public final String text;
  public final int len;
  public final int tokenKind;

  Operator(String text, int tokenKind) {
    this.text = text;
    this.len = text.length();
    this.tokenKind = tokenKind;
  }
}
