package org.japy.parser.python.impl.ast;

import static org.japy.infra.coll.CollUtil.last;
import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.stmt.AstElse;
import org.japy.parser.python.ast.stmt.AstIf;
import org.japy.parser.python.ast.stmt.AstIfBranch;
import org.japy.parser.python.ast.stmt.IAstStmt;

final class IfBuilder implements SuiteHandler, ElseHandler {
  private static class IfBranch {
    final IAstExpr condition;
    final ITokenSpan location;
    final SuiteBuilder body = new SuiteBuilder();

    private IfBranch(IAstExpr condition, ITokenSpan location) {
      this.condition = condition;
      this.location = location;
    }
  }

  private final List<IfBranch> branches = new ArrayList<>();
  private final SuiteBuilder elseBody = new SuiteBuilder();
  private @Nullable ITokenSpan elseLocation;

  public IfBuilder(IAstExpr condition, ITokenSpan location) {
    branches.add(new IfBranch(condition, location));
  }

  public void visitElif(IAstExpr condition, ITokenSpan location) {
    dcheck(elseLocation == null);
    branches.add(new IfBranch(condition, location));
  }

  @Override
  public void visitElse(ITokenSpan location) {
    dcheck(elseLocation == null);
    elseLocation = location;
  }

  @Override
  public void add(IAstStmt stmt) {
    if (elseLocation != null) {
      elseBody.add(stmt);
    } else {
      last(branches).body.add(stmt);
    }
  }

  public IAstStmt build() {
    return new AstIf(
        branches.stream()
            .map(b -> new AstIfBranch(b.condition, b.body.build(), b.location))
            .toArray(AstIfBranch[]::new),
        elseLocation != null ? new AstElse(elseBody.build(), elseLocation) : null);
  }
}
