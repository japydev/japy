package org.japy.parser.python.impl;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.AugOp;
import org.japy.infra.exc.InternalErrorException;
import org.japy.parser.python.ParserInput;
import org.japy.parser.python.ParserVisitor;
import org.japy.parser.python.ast.expr.*;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.*;
import org.japy.parser.python.impl.javacc.PythonParser;

public class ExpressionParser extends ParserWrapper<IAstExpr> {
  public ExpressionParser(Scanner scanner) {
    super(new Visitor(), scanner);
  }

  public IAstExpr parse() {
    return invoke(this, PythonParser::eval_input);
  }

  private static class Visitor implements ParserVisitor<IAstExpr> {
    @Override
    public IAstExpr build(IAstExpr expr) {
      return expr;
    }

    @Override
    public void visitInput(ParserInput input) {}

    @Override
    public IAstExpr build(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitGlobal(AstIdentifier[] names, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitNonlocal(AstIdentifier[] names, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitDel(IAstExpr target, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitAugAssign(AugOp op, IAstExpr target, IAstExpr value) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitAssign(IAstExpr[] targets, IAstExpr value) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitAnnAssign(IAstExpr target, IAstExpr annotation, IAstExpr value) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitAnnExpr(IAstExpr target, IAstExpr annotation) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitAssert(IAstExpr expr, @Nullable IAstExpr message, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitExprStmt(IAstExpr expr) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitReturn(@Nullable IAstExpr value, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitRaise(@Nullable IAstExpr ex, @Nullable IAstExpr from, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitBreak(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitContinue(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitFunc(
        boolean async,
        AstDecorator[] decorators,
        AstIdentifier name,
        AstParam[] params,
        @Nullable IAstExpr retType,
        ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitEndFunc(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitClass(
        AstDecorator[] decorators, AstIdentifier name, IAstArg[] supers, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitEndClass(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitPass(ITokenSpan location) {}

    @Override
    public void visitImport(AstImportedModule[] modules, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitImportFrom(
        AstModuleName module, IAstImportedName[] names, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitIf(IAstExpr condition, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitElif(IAstExpr condition, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitElse(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitEndIfSuite(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitEndIf(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitWhile(IAstExpr condition, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitEndWhileSuite(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitEndWhile(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitFor(boolean async, IAstExpr target, IAstExpr list, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitEndForSuite(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitEndFor(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitWith(boolean async, AstWithItem[] items, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitEndWith(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitTry(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitExcept(
        @Nullable IAstExpr ex, @Nullable AstIdentifier as, ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitFinally(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitEndTrySuite(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void visitEndTry(ITokenSpan location) {
      throw InternalErrorException.notReached();
    }
  }
}
