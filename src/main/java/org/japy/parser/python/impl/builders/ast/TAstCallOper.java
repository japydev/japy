package org.japy.parser.python.impl.builders.ast;

import org.japy.infra.loc.Location;
import org.japy.parser.python.ast.expr.AstCall;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.node.IAstArg;
import org.japy.parser.python.impl.ParserUtil;
import org.japy.parser.python.impl.javacc.Token;

public final class TAstCallOper extends ParserUtil implements ITAstOper {
  public final Token start;
  public final Token end;
  public final IAstArg[] args;

  public TAstCallOper(Token start, Token end, IAstArg[] args) {
    this.start = start;
    this.end = end;
    this.args = args;
  }

  @Override
  public IAstExpr apply(IAstExpr e) {
    return new AstCall(e, args, t(end));
  }

  @Override
  public Location location() {
    return start.location();
  }
}
