package org.japy.parser.python.impl;

import static org.japy.infra.coll.CollUtil.last;
import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.misc.PyEllipsis.Ellipsis;
import static org.japy.kernel.types.misc.PyNone.None;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.LocationUtil;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.num.PyComplex;
import org.japy.kernel.types.num.PyFloat;
import org.japy.kernel.types.num.PyInt;
import org.japy.parser.ParserError;
import org.japy.parser.python.ParserVisitor;
import org.japy.parser.python.ast.expr.*;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.loc.TokenSpan;
import org.japy.parser.python.ast.node.*;
import org.japy.parser.python.impl.builders.BaseForBuilder;
import org.japy.parser.python.impl.builders.ITargetConsumer;
import org.japy.parser.python.impl.builders.ast.TAstTarget;
import org.japy.parser.python.impl.builders.ast.TAstWithItem;
import org.japy.parser.python.impl.javacc.ParseException;
import org.japy.parser.python.impl.javacc.PythonParser;
import org.japy.parser.python.impl.javacc.Token;
import org.japy.parser.python.impl.token.BytesToken;
import org.japy.parser.python.impl.token.FloatToken;
import org.japy.parser.python.impl.token.ImagToken;
import org.japy.parser.python.impl.token.IntToken;

public abstract class ParserBase<T> extends ParserUtil {
  protected final ParserVisitor<T> visitor;

  private final Pattern PAT_CLASS_NAME = Pattern.compile("_*([^_].*)");
  private final Deque<String> classNamePrefixes = new ArrayDeque<>();

  protected ParserBase(ParserVisitor<T> visitor) {
    this.visitor = visitor;
  }

  protected ParserBase() {
    throw InternalErrorException.notReached();
  }

  protected T build(ITokenSpan location) {
    return visitor.build(location);
  }

  protected T build(IAstExpr expr) {
    return visitor.build(expr);
  }

  protected void beginSuite() {}

  protected void endSuite() {}

  private String mangleName(String name) {
    if (classNamePrefixes.isEmpty() || !name.startsWith("__") || name.endsWith("__")) {
      return name;
    }

    return classNamePrefixes.peek() + name;
  }

  private void pushClassName(String name) {
    Matcher m = PAT_CLASS_NAME.matcher(name);
    classNamePrefixes.push(m.matches() ? "_" + m.group(1) : "");
  }

  private void popClassName() {
    classNamePrefixes.pop();
  }

  protected AstIdentifier maybeMangledId(Token token) {
    return new AstIdentifier(t(token), mangleName(token.image));
  }

  protected AstModuleName astModuleName(List<Token> names, Token start) {
    return new AstModuleName(
        new TokenSpan(t(start), t(last(names))),
        0,
        names.stream().map(ParserUtil::nonMangledId).toArray(AstIdentifier[]::new));
  }

  protected AstModuleName astModuleName(
      int dots, AstIdentifier @Nullable [] names, Token start, Token end) {
    return new AstModuleName(
        new TokenSpan(t(start), t(end)), dots, names != null ? names : AstIdentifier.EMPTY_ARRAY);
  }

  protected void varDeclStmt(List<Token> names, Token tokGlobal) {
    AstIdentifier[] ids = names.stream().map(this::maybeMangledId).toArray(AstIdentifier[]::new);
    switch (tokGlobal.kind) {
      case Token.GLOBAL:
        visitor.visitGlobal(ids, new TokenSpan(t(tokGlobal), last(ids).token()));
        break;
      case Token.NONLOCAL:
        visitor.visitNonlocal(ids, new TokenSpan(t(tokGlobal), last(ids).token()));
        break;
      default:
        throw InternalErrorException.notReached(tokGlobal);
    }
  }

  protected AstConst bytes(List<BytesToken> list) {
    if (list.size() == 1) {
      return new AstConst(new PyBytes(list.get(0).bytes), t(list.get(0)));
    }

    @Var int size = 0;
    for (BytesToken t : list) {
      size += t.bytes.length;
    }

    byte[] bytes = new byte[size];
    @Var int offset = 0;
    for (BytesToken t : list) {
      System.arraycopy(t.bytes, 0, bytes, offset, t.bytes.length);
      offset += t.bytes.length;
    }

    return new AstConst(new PyBytes(bytes), t(list.get(0)), t(list.get(list.size() - 1)));
  }

  protected AstStar star(Token tokStar, IAstExpr expr) {
    return new AstStar(expr, t(tokStar));
  }

  protected AstDoubleStar doubleStar(Token tokPow, IAstExpr expr) {
    return new AstDoubleStar(expr, t(tokPow));
  }

  protected IAstExpr constVal(Token token) {
    switch (token.kind) {
      case Token.TRUE:
      case Token.FALSE:
        return new AstConst(PyBool.of(token.kind == Token.TRUE), t(token));
      case Token.NONE:
        return new AstConst(None, t(token));
      case Token.ELLIPSIS:
        return new AstConst(Ellipsis, t(token));
      case Token.INT:
        return new AstConst(PyInt.getMinimal(((IntToken) token).value), t(token));
      case Token.FLOAT:
        return new AstConst(new PyFloat(((FloatToken) token).value), t(token));
      case Token.IMAG:
        return new AstConst(new PyComplex(0, ((ImagToken) token).value), t(token));
      default:
        throw InternalErrorException.notReached();
    }
  }

  protected void compileFunc(
      AstDecorator[] decorators,
      @Nullable Token tokAsync,
      Token name,
      AstParam @Nullable [] params,
      @Nullable IAstExpr retType,
      Token tokDef,
      Token tokColon) {
    visitor.visitFunc(
        tokAsync != null,
        decorators,
        maybeMangledId(name),
        params != null ? params : AstParam.EMPTY_ARRAY,
        retType,
        loc(tokAsync != null ? tokAsync : tokDef, tokColon));
  }

  protected void compileClass(
      AstDecorator[] decorators, Token name, IAstArg[] supers, Token tokClass, Token tokColon) {
    visitor.visitClass(decorators, maybeMangledId(name), supers, loc(tokClass, tokColon));
    pushClassName(name.image);
  }

  protected void compileEndClass(Token end) {
    popClassName();
    visitor.visitEndClass(loc(end));
  }

  protected class AssignCompiler {
    private final List<IAstExpr> exprs = new ArrayList<>();
    private @Nullable IAstExpr annotation;

    public AssignCompiler(IAstExpr lhs) {
      exprs.add(lhs);
    }

    public void add(@Nullable IAstExpr annotation, IAstExpr rhs) {
      if (annotation != null) {
        dcheck(this.annotation == null);
        this.annotation = annotation;
      }
      exprs.add(rhs);
    }

    public void compile() {
      dcheck(exprs.size() > 1);
      if (annotation != null) {
        dcheck(exprs.size() == 2);
        if (exprs.get(1) == null) {
          visitor.visitAnnExpr(exprs.get(0), annotation);
        } else {
          visitor.visitAnnAssign(exprs.get(0), annotation, exprs.get(1));
        }
      } else {
        IAstExpr[] lhs = new IAstExpr[exprs.size() - 1];
        for (int i = 0; i < lhs.length; ++i) {
          lhs[i] = exprs.get(i);
        }
        visitor.visitAssign(lhs, exprs.get(exprs.size() - 1));
      }
    }
  }

  protected class DelCompiler implements ITargetConsumer {
    private final List<IAstExpr> targets = new ArrayList<>();

    public DelCompiler() {}

    @Override
    public void target(TAstTarget tgt) {
      if (!tgt.stars.isEmpty()) {
        throw new ParserError("cannot delete starred", tgt.stars.get(0));
      }

      targets.add(tgt.expr);
    }

    @Override
    public void trailingComma() {}

    public void compile(Token start, Token end) {
      if (targets.isEmpty()) {
        throw new ParserError("invalid syntax", end);
      }

      IAstExpr tgt =
          targets.size() == 1 ? targets.get(0) : new AstTuple(targets.toArray(IAstExpr[]::new));
      visitor.visitDel(tgt, loc(start, end));
    }
  }

  protected class SimpleImportCompiler {
    private final Token tokImport;
    private final List<AstImportedModule> modules = new ArrayList<>();

    public SimpleImportCompiler(Token tokImport) {
      this.tokImport = tokImport;
    }

    public void add(AstModuleName module, @Nullable Token as) {
      modules.add(new AstImportedModule(module, as != null ? nonMangledId(as) : null));
    }

    public void end(Token end) {
      visitor.visitImport(modules.toArray(AstImportedModule[]::new), loc(tokImport, end));
    }
  }

  protected class ImportFromCompiler {
    private final Token tokFrom;
    private final AstModuleName module;
    private final List<IAstImportedName> names = new ArrayList<>();

    public ImportFromCompiler(AstModuleName module, Token tokFrom) {
      this.tokFrom = tokFrom;
      this.module = module;
    }

    public void star(Token star) {
      names.add(new AstImportedStar(t(star)));
    }

    public void add(Token name, @Nullable Token as) {
      names.add(new AstImportedName(nonMangledId(name), as != null ? nonMangledId(as) : null));
    }

    public void end(Token end) {
      visitor.visitImportFrom(module, names.toArray(IAstImportedName[]::new), loc(tokFrom, end));
    }
  }

  protected class ForCompiler extends BaseForBuilder {
    private final @Nullable Token tokAsync;
    private final Token tokFor;

    public ForCompiler(@Nullable Token tokAsync, Token tokFor) {
      this.tokAsync = tokAsync;
      this.tokFor = tokFor;
    }

    public void list(IAstExpr list, Token tokColon) {
      visitor.visitFor(
          tokAsync != null, getTarget(), list, loc(tokAsync != null ? tokAsync : tokFor, tokColon));
    }
  }

  protected class WithCompiler {
    private final @Nullable Token tokAsync;
    private final Token tokWith;
    private final List<TAstWithItem> items = new ArrayList<>();

    public WithCompiler(@Nullable Token tokAsync, Token tokWith) {
      this.tokAsync = tokAsync;
      this.tokWith = tokWith;
    }

    public void add(TAstWithItem item) {
      items.add(item);
    }

    public void begin(Token tokColon) {
      visitor.visitWith(
          tokAsync != null,
          TAstWithItem.grok(items),
          loc(tokAsync != null ? tokAsync : tokWith, tokColon));
    }
  }

  protected interface ParserMethod<T> {
    T invoke(PythonParser<T> parser) throws ParseException;
  }

  protected static <T> T invoke(PythonParser<T> parser, ParserMethod<T> method) {
    Scanner scanner = parser.scanner();
    try {
      parser.visitor.visitInput(parser.scanner());
      parser.disable_tracing();
      return method.invoke(parser);
    } catch (ParseException | StackOverflowError ex) {
      @SuppressWarnings("ConstantConditions")
      Token token = parser.token.next != null ? parser.token.next : parser.token;
      ParserError pe =
          new ParserError(ex.getMessage() != null ? ex.getMessage() : "invalid syntax", token, ex);
      pe.setSourceFile(scanner.sourceFile);
      throw pe;
    } catch (Throwable ex) {
      LocationUtil.setFileIfNotSet(ex, scanner.sourceFile);
      throw ex;
    }
  }
}
