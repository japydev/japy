package org.japy.parser.python.impl;

import org.japy.parser.python.ParserVisitor;
import org.japy.parser.python.impl.javacc.PythonParser;

public class ModuleParser<T> extends ParserWrapper<T> {
  public ModuleParser(ParserVisitor<T> visitor, Scanner scanner) {
    super(visitor, scanner);
  }

  public T parse() {
    return invoke(this, PythonParser::file_input);
  }
}
