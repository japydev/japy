package org.japy.parser.python.impl.ast;

import static org.japy.infra.coll.CollUtil.last;
import static org.japy.infra.coll.CollUtil.pop;
import static org.japy.infra.coll.CollUtil.push;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.AugOp;
import org.japy.infra.exc.InternalErrorException;
import org.japy.parser.python.Parser;
import org.japy.parser.python.ParserInput;
import org.japy.parser.python.ParserVisitor;
import org.japy.parser.python.ast.expr.*;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.*;
import org.japy.parser.python.ast.stmt.*;

public final class AstBuilder<M> implements ParserVisitor<M> {
  private @Nullable ParserInput parserInput;
  private final SuiteBuilder body = new SuiteBuilder();
  private final List<SuiteHandler> suiteHandlers = new ArrayList<>();
  private final Parser.AstConsumer<M> astConsumer;

  public AstBuilder(Parser.AstConsumer<M> astConsumer) {
    this.astConsumer = astConsumer;
    suiteHandlers.add(body::add);
  }

  @Override
  public M build(ITokenSpan location) {
    if (body.isEmpty()) {
      body.add(new AstPass(location));
    }
    dcheckNotNull(parserInput);
    return astConsumer.accept(
        new AstModule(body.build(), parserInput.sourceFile(), location), parserInput);
  }

  @Override
  public void visitInput(ParserInput input) {
    this.parserInput = input;
  }

  private SuiteHandler handler() {
    return last(suiteHandlers);
  }

  @SuppressWarnings("unchecked")
  private <H> H handler(Class<H> klass) {
    for (int i = suiteHandlers.size() - 1; ; --i) {
      SuiteHandler b = suiteHandlers.get(i);
      if (klass.isAssignableFrom(b.getClass())) {
        return (H) b;
      }
    }
  }

  private void add(IAstStmt stmt) {
    handler().add(stmt);
  }

  @Override
  public void visitGlobal(AstIdentifier[] names, ITokenSpan location) {
    add(new AstVarDecl(AstVarDecl.DeclKind.GLOBAL, names, location));
  }

  @Override
  public void visitNonlocal(AstIdentifier[] names, ITokenSpan location) {
    add(new AstVarDecl(AstVarDecl.DeclKind.NONLOCAL, names, location));
  }

  @Override
  public void visitDel(IAstExpr target, ITokenSpan location) {
    add(new AstDel(target, location));
  }

  @Override
  public void visitAugAssign(AugOp op, IAstExpr target, IAstExpr value) {
    add(new AstAugAssign(op, target, value));
  }

  @Override
  public void visitAssign(IAstExpr[] targets, IAstExpr value) {
    add(new AstAssign(targets, value));
  }

  @Override
  public void visitAnnAssign(IAstExpr target, IAstExpr annotation, IAstExpr value) {
    add(new AstAnnAssign(target, annotation, value));
  }

  @Override
  public void visitAnnExpr(IAstExpr expr, IAstExpr annotation) {
    add(new AstAnnExpr(expr, annotation));
  }

  @Override
  public void visitAssert(IAstExpr expr, @Nullable IAstExpr message, ITokenSpan location) {
    add(new AstAssert(expr, message, location));
  }

  @Override
  public void visitExprStmt(IAstExpr expr) {
    add(new AstExprStmt(expr));
  }

  @Override
  public void visitReturn(@Nullable IAstExpr value, ITokenSpan location) {
    add(new AstReturn(value, location));
  }

  @Override
  public void visitRaise(@Nullable IAstExpr ex, @Nullable IAstExpr from, ITokenSpan location) {
    add(new AstRaise(ex, from, location));
  }

  @Override
  public void visitImport(AstImportedModule[] modules, ITokenSpan location) {
    add(new AstImport(modules, location));
  }

  @Override
  public void visitImportFrom(AstModuleName module, IAstImportedName[] names, ITokenSpan location) {
    add(new AstImportFrom(module, names, location));
  }

  @Override
  public void visitBreak(ITokenSpan location) {
    add(new AstBreak(location));
  }

  @Override
  public void visitContinue(ITokenSpan location) {
    add(new AstContinue(location));
  }

  @Override
  public void visitFunc(
      boolean async,
      AstDecorator[] decorators,
      AstIdentifier name,
      AstParam[] params,
      @Nullable IAstExpr retType,
      ITokenSpan location) {
    push(suiteHandlers, new FuncBuilder(async, decorators, name, params, retType, location));
  }

  @Override
  public void visitEndFunc(ITokenSpan location) {
    FuncBuilder b = (FuncBuilder) pop(suiteHandlers);
    add(b.build());
  }

  @Override
  public void visitClass(
      AstDecorator[] decorators, AstIdentifier name, IAstArg[] supers, ITokenSpan location) {
    push(suiteHandlers, new ClassBuilder(decorators, name, supers, location));
  }

  @Override
  public void visitEndClass(ITokenSpan location) {
    ClassBuilder b = (ClassBuilder) pop(suiteHandlers);
    add(b.build());
  }

  @Override
  public void visitPass(ITokenSpan location) {
    add(new AstPass(location));
  }

  @Override
  public void visitIf(IAstExpr condition, ITokenSpan location) {
    push(suiteHandlers, new IfBuilder(condition, location));
  }

  @Override
  public void visitElif(IAstExpr condition, ITokenSpan location) {
    IfBuilder b = handler(IfBuilder.class);
    b.visitElif(condition, location);
  }

  @Override
  public void visitElse(ITokenSpan location) {
    ElseHandler b = handler(ElseHandler.class);
    b.visitElse(location);
  }

  @Override
  public void visitEndIfSuite(ITokenSpan location) {}

  @Override
  public void visitEndIf(ITokenSpan location) {
    IfBuilder b = (IfBuilder) pop(suiteHandlers);
    add(b.build());
  }

  @Override
  public void visitWhile(IAstExpr condition, ITokenSpan location) {
    push(suiteHandlers, new WhileBuilder(condition, location));
  }

  @Override
  public void visitEndWhileSuite(ITokenSpan location) {}

  @Override
  public void visitEndWhile(ITokenSpan location) {
    WhileBuilder b = (WhileBuilder) pop(suiteHandlers);
    add(b.build());
  }

  @Override
  public void visitFor(boolean async, IAstExpr target, IAstExpr list, ITokenSpan location) {
    push(suiteHandlers, new ForBuilder(async, target, list, location));
  }

  @Override
  public void visitEndForSuite(ITokenSpan location) {}

  @Override
  public void visitEndFor(ITokenSpan location) {
    ForBuilder b = (ForBuilder) pop(suiteHandlers);
    add(b.build());
  }

  @Override
  public void visitWith(boolean async, AstWithItem[] items, ITokenSpan location) {
    push(suiteHandlers, new WithBuilder(async, items, location));
  }

  @Override
  public void visitEndWith(ITokenSpan location) {
    WithBuilder b = (WithBuilder) pop(suiteHandlers);
    add(b.build());
  }

  @Override
  public void visitTry(ITokenSpan location) {
    push(suiteHandlers, new TryBuilder(location));
  }

  @Override
  public void visitExcept(@Nullable IAstExpr ex, @Nullable AstIdentifier as, ITokenSpan location) {
    TryBuilder b = handler(TryBuilder.class);
    b.visitExcept(ex, as, location);
  }

  @Override
  public void visitFinally(ITokenSpan location) {
    TryBuilder b = handler(TryBuilder.class);
    b.visitFinally(location);
  }

  @Override
  public void visitEndTrySuite(ITokenSpan location) {}

  @Override
  public void visitEndTry(ITokenSpan location) {
    TryBuilder b = (TryBuilder) pop(suiteHandlers);
    add(b.build());
  }

  @Override
  public M build(IAstExpr expr) {
    throw InternalErrorException.notReached();
  }
}
