package org.japy.parser.python.impl.builders;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.parser.python.ast.expr.AstGenerator;
import org.japy.parser.python.ast.expr.AstParen;
import org.japy.parser.python.ast.expr.AstTuple;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.node.AstCompFor;
import org.japy.parser.python.impl.ParserUtil;
import org.japy.parser.python.impl.javacc.Token;

public final class ParenExprBuilder extends ParserUtil {
  public final List<IAstExpr> exprs = new ArrayList<>();
  public @Nullable AstCompFor compFor;
  public boolean seenComma;

  public void add(IAstExpr expr) {
    exprs.add(expr);
  }

  public void add(AstCompFor compFor) {
    if (this.compFor != null) {
      throw new InternalErrorException("multiple comp fors", compFor);
    }
    this.compFor = compFor;
  }

  public IAstExpr build(Token start, Token end) {
    if (compFor != null) {
      dcheck(exprs.size() == 1);
      return new AstParen(t(start), t(end), new AstGenerator(exprs.get(0), compFor));
    } else if (seenComma || exprs.size() > 1) {
      return new AstTuple(t(start), t(end), exprs.toArray(IAstExpr[]::new));
    } else if (!exprs.isEmpty()) {
      return new AstParen(t(start), t(end), exprs.get(0));
    } else {
      return new AstTuple(t(start), t(end));
    }
  }
}
