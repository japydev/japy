package org.japy.parser.python.impl;

import static org.japy.infra.validation.Debug.dcheck;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.errorprone.annotations.Var;
import org.eclipse.collections.impl.list.mutable.primitive.IntArrayList;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.Location;
import org.japy.infra.loc.Range;
import org.japy.infra.validation.Debug;
import org.japy.parser.python.ParserInput;

class InputReader extends ScannerUtil implements ParserInput {
  private static final byte[] bom = new byte[] {(byte) 0xef, (byte) 0xbb, (byte) 0xbf};
  private static final int bomLen = bom.length;

  private static final Pattern reCoding = Pattern.compile("coding[:=]\\s*(\\S+)");

  protected final String sourceFile;
  protected final byte[] buffer;
  protected final int bufferSize;
  private int curPos;
  private int curLine = 1;
  private int curCol = 1;
  private final IntArrayList lineOffsets = new IntArrayList();

  private static class BufInfo {
    final byte[] buffer;
    final int bufferSize;
    final int startPos;

    BufInfo(byte[] buffer, int bufferSize) {
      this(buffer, bufferSize, 0);
    }

    BufInfo(byte[] buffer, int bufferSize, int startPos) {
      this.buffer = buffer;
      this.bufferSize = bufferSize;
      this.startPos = startPos;
    }
  }

  public InputReader(String sourceFile, byte[] contents, boolean knownUtf8) {
    this.sourceFile = sourceFile;
    BufInfo bufInfo = getBufInfo(contents, knownUtf8);
    buffer = bufInfo.buffer;
    bufferSize = bufInfo.bufferSize;
    curPos = bufInfo.startPos;
    lineOffsets.add(0);
    lineOffsets.add(curPos);
  }

  @Override
  public String getText(Range range) {
    dcheck(!range.isUnknown());
    int start = getBufOffset(range.start);
    return new String(buffer, start, getBufOffset(range.end) - start, StandardCharsets.UTF_8);
  }

  @Override
  public String sourceFile() {
    return sourceFile;
  }

  private int getBufOffset(Location location) {
    return lineOffsets.get(location.line) + location.col - 1;
  }

  private static BufInfo getBufInfo(byte[] contents, boolean knownUtf8) {
    if (knownUtf8) {
      return new BufInfo(contents, contents.length, 0);
    } else if (contents.length >= bomLen && Arrays.equals(contents, 0, bomLen, bom, 0, bomLen)) {
      return new BufInfo(contents, contents.length, bomLen);
    } else {
      return convertToUtf8(contents);
    }
  }

  @SuppressWarnings("ByteBufferBackingArray")
  private static BufInfo convertToUtf8(byte[] buffer) {
    @Var int pos = 0;
    @Var int lineCount = 0;
    while (pos < buffer.length) {
      if (isEol(buffer[pos])) {
        if (lineCount > 0) {
          break;
        }
        ++lineCount;
        if (buffer[pos] == '\r' && pos + 1 < buffer.length && buffer[pos + 1] == '\n') {
          ++pos;
        }
        ++pos;
      } else if (buffer[pos] < 0) {
        break;
      } else {
        ++pos;
      }
    }

    if (pos == 0) {
      return new BufInfo(buffer, buffer.length);
    }

    String text = new String(buffer, 0, pos, StandardCharsets.UTF_8);
    Matcher m = reCoding.matcher(text);
    if (!m.find()) {
      return new BufInfo(buffer, buffer.length);
    }

    String charsetName = m.group(1);
    Charset charset;
    try {
      charset = Charset.forName(charsetName);
    } catch (UnsupportedCharsetException ex) {
      return new BufInfo(buffer, buffer.length);
    }

    if (charset.equals(StandardCharsets.UTF_8)) {
      return new BufInfo(buffer, buffer.length);
    }

    CharsetDecoder decoder = charset.newDecoder();
    decoder.onMalformedInput(CodingErrorAction.REPORT);
    decoder.onUnmappableCharacter(CodingErrorAction.REPORT);
    CharBuffer charBuf;
    try {
      charBuf = decoder.decode(ByteBuffer.wrap(buffer));
    } catch (CharacterCodingException ex) {
      return new BufInfo(buffer, buffer.length);
    }

    CharsetEncoder encoder = StandardCharsets.UTF_8.newEncoder();
    encoder.onMalformedInput(CodingErrorAction.REPORT);
    encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
    try {
      ByteBuffer byteBuffer = encoder.encode(charBuf);
      return new BufInfo(byteBuffer.array(), byteBuffer.limit());
    } catch (CharacterCodingException ex) {
      throw new InternalErrorException(ex.getMessage(), ex);
    }
  }

  public final boolean atEof() {
    return curPos == bufferSize;
  }

  protected final int curPos() {
    return curPos;
  }

  protected final int curLine() {
    return curLine;
  }

  protected final int curCol() {
    return curCol;
  }

  protected final void incPos() {
    ++curPos;
    ++curCol;
  }

  protected final void incPos(int count) {
    for (int i = 0; i < count; ++i) {
      incPos();
    }
  }

  protected final Location curLoc() {
    return new Location(curLine, curCol);
  }

  protected final byte charAt(int offsetFromCur) {
    return buffer[curPos + offsetFromCur];
  }

  protected final byte curChar() {
    return buffer[curPos];
  }

  protected final byte nextChar() {
    return curPos + 1 < bufferSize ? buffer[curPos + 1] : 0;
  }

  protected final byte nextNextChar() {
    return curPos + 2 < bufferSize ? buffer[curPos + 2] : 0;
  }

  protected final void checkCurChar(int c) {
    if (Debug.ENABLED) {
      dcheck(curChar() == (byte) c);
    }
  }

  @SuppressWarnings("SameParameterValue")
  protected final void checkCurChar(int c1, int c2) {
    if (Debug.ENABLED) {
      dcheck(curChar() == (byte) c1 || curChar() == (byte) c2);
    }
  }

  protected final void checkCurCharLetter() {
    if (Debug.ENABLED) {
      dcheck(isLetter(curChar()));
    }
  }

  protected final void checkCurCharDigit() {
    if (Debug.ENABLED) {
      dcheck(isDigit(curChar()));
    }
  }

  protected final void skipEol() {
    byte c = curChar();
    if (Debug.ENABLED) {
      dcheck(isEol(c));
    }
    if (c == '\r' && nextChar() == '\n') {
      ++curPos;
    }
    ++curPos;
    ++curLine;
    curCol = 1;
    lineOffsets.add(curPos);
  }
}
