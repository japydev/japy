package org.japy.parser.python.impl.builders.ast;

import java.util.List;

import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.Location;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.impl.javacc.Token;

public final class TAstTarget implements IHasLocation {
  public final List<Token> stars;
  public final IAstExpr expr;

  public TAstTarget(List<Token> stars, IAstExpr expr) {
    this.stars = stars;
    this.expr = expr;
  }

  @Override
  public Location location() {
    return stars.get(0).start();
  }
}
