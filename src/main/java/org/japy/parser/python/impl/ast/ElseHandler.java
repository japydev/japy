package org.japy.parser.python.impl.ast;

import org.japy.parser.python.ast.loc.ITokenSpan;

interface ElseHandler {
  void visitElse(ITokenSpan location);
}
