package org.japy.parser.python.impl.token;

import org.japy.base.RFConversion;
import org.japy.infra.loc.Location;
import org.japy.parser.ScannerError;
import org.japy.parser.python.impl.Scanner;

public final class RFConvToken extends TokenImpl {
  public enum Conversion {
    A("!a", RFConversion.A),
    S("!s", RFConversion.S),
    R("!r", RFConversion.R);

    public final String text;
    public final RFConversion astConv;

    Conversion(String text, RFConversion astConv) {
      this.text = text;
      this.astConv = astConv;
    }

    public static Conversion get(byte convChar, int line, int col) {
      switch (convChar) {
        case 'a':
          return Conversion.A;
        case 's':
          return Conversion.S;
        case 'r':
          return Conversion.R;
        default:
          throw new ScannerError(
              String.format("invalid conversion character %s", Scanner.formatChar(convChar)),
              new Location(line, col));
      }
    }
  }

  public final Conversion conversion;

  public RFConvToken(Conversion conv, int line, int beginCol, int endCol) {
    super(RF_CONV, conv.text, line, beginCol, line, endCol);
    this.conversion = conv;
  }

  public RFConvToken(byte convChar, int line, int beginCol, int endCol) {
    this(Conversion.get(convChar, line, beginCol), line, beginCol, endCol);
  }
}
