package org.japy.parser.python.impl;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.TokenSpan;
import org.japy.parser.python.impl.javacc.Token;

public abstract class ParserUtil {
  protected <T> List<T> list(@SuppressWarnings("unused") Class<T> clazz) {
    return new ArrayList<>();
  }

  protected Token[] array(List<Token> list) {
    return list.toArray(Token[]::new);
  }

  @SuppressWarnings("unchecked")
  protected <T> T[] array(List<T> list, Class<T> elementClass) {
    return list.toArray(len -> (T[]) Array.newInstance(elementClass, len));
  }

  protected List<IAstExpr> exprList() {
    return new ArrayList<>();
  }

  protected IAstExpr[] exprArray(List<IAstExpr> exprs) {
    return exprs.toArray(IAstExpr[]::new);
  }

  public static AstIdentifier nonMangledId(Token t) {
    return new AstIdentifier(t(t), t.image);
  }

  public static AstIdentifier nonMangledId(
      String name, org.japy.parser.python.ast.loc.Token token) {
    return new AstIdentifier(token, name);
  }

  public static org.japy.parser.python.ast.loc.Token t(Token token) {
    return new org.japy.parser.python.ast.loc.Token(token.start(), token.end());
  }

  public static TokenSpan loc(Token token) {
    return new TokenSpan(t(token), t(token));
  }

  public static TokenSpan loc(Token start, Token end) {
    return new TokenSpan(t(start), t(end));
  }
}
