package org.japy.parser.python.impl.token;

import org.japy.parser.python.impl.javacc.Token;

public final class BytesToken extends TokenImpl {
  public final byte[] bytes;

  public BytesToken(byte[] bytes, int beginLine, int beginCol, int endLine, int endCol) {
    super(Token.BYTES, "BYTES", beginLine, beginCol, endLine, endCol);
    this.bytes = bytes;
  }
}
