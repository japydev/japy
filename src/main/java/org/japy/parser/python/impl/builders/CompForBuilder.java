package org.japy.parser.python.impl.builders;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.node.AstCompFor;
import org.japy.parser.python.ast.node.IAstCompIter;
import org.japy.parser.python.impl.javacc.Token;

public final class CompForBuilder extends BaseForBuilder {
  private @Nullable IAstCompIter iter;
  private @Nullable IAstExpr list;

  public void iter(IAstCompIter iter) {
    this.iter = iter;
  }

  public void list(IAstExpr list) {
    this.list = list;
  }

  public AstCompFor build(@Nullable Token tokAsync, Token tokFor) {
    dcheckNotNull(list);
    return new AstCompFor(
        tokAsync != null, getTarget(), list, iter, tokAsync != null ? t(tokAsync) : t(tokFor));
  }
}
