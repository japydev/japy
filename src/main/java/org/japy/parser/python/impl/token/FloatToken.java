package org.japy.parser.python.impl.token;

public final class FloatToken extends TokenImpl {
  public final double value;

  public FloatToken(double value, int line, int beginCol, int endCol) {
    super(FLOAT, "FLOAT", line, beginCol, endCol);
    this.value = value;
  }

  @Override
  public String toString() {
    return Double.toString(value);
  }
}
