package org.japy.parser.python.impl.token;

import java.math.BigInteger;

public final class IntToken extends TokenImpl {
  public final BigInteger value;

  public IntToken(BigInteger value, int line, int beginCol, int endCol) {
    super(INT, "INTEGER", line, beginCol, endCol);
    this.value = value;
  }

  @Override
  public String toString() {
    return value.toString();
  }
}
