package org.japy.parser.python.impl.ast;

import static org.japy.infra.validation.Debug.dcheck;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.stmt.AstElse;
import org.japy.parser.python.ast.stmt.AstFor;
import org.japy.parser.python.ast.stmt.IAstStmt;

final class ForBuilder implements SuiteHandler, ElseHandler {
  private final boolean async;
  private final IAstExpr target;
  private final IAstExpr list;
  private final ITokenSpan start;
  private final SuiteBuilder body = new SuiteBuilder();
  private @Nullable ITokenSpan elseLocation;
  private final SuiteBuilder elseBody = new SuiteBuilder();

  ForBuilder(boolean async, IAstExpr target, IAstExpr list, ITokenSpan location) {
    this.async = async;
    this.target = target;
    this.list = list;
    this.start = location;
  }

  @Override
  public void visitElse(ITokenSpan location) {
    dcheck(elseLocation == null);
    elseLocation = location;
  }

  @Override
  public void add(IAstStmt stmt) {
    if (elseLocation != null) {
      elseBody.add(stmt);
    } else {
      body.add(stmt);
    }
  }

  public IAstStmt build() {
    return new AstFor(
        async,
        target,
        list,
        start,
        body.build(),
        elseLocation != null ? new AstElse(elseBody.build(), elseLocation) : null);
  }
}
