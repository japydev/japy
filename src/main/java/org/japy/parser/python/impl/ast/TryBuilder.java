package org.japy.parser.python.impl.ast;

import static org.japy.infra.coll.CollUtil.last;
import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.stmt.AstElse;
import org.japy.parser.python.ast.stmt.AstExcept;
import org.japy.parser.python.ast.stmt.AstTry;
import org.japy.parser.python.ast.stmt.IAstStmt;

final class TryBuilder implements SuiteHandler, ElseHandler {
  private static class Except {
    final @Nullable IAstExpr ex;
    final @Nullable IAstExpr as;
    final ITokenSpan location;
    final SuiteBuilder body = new SuiteBuilder();

    private Except(@Nullable IAstExpr ex, @Nullable IAstExpr as, ITokenSpan location) {
      this.ex = ex;
      this.as = as;
      this.location = location;
    }

    AstExcept build() {
      return new AstExcept(ex, as, location, body.build());
    }
  }

  private enum State {
    TRY,
    EXCEPT,
    ELSE,
    FINALLY,
  }

  private final ITokenSpan start;
  private State state = State.TRY;
  private final SuiteBuilder body = new SuiteBuilder();
  private final List<Except> excepts = new ArrayList<>();
  private final SuiteBuilder elseBody = new SuiteBuilder();
  private @Nullable ITokenSpan elseLocation;
  private final SuiteBuilder finallyBody = new SuiteBuilder();
  private @Nullable ITokenSpan finallyLocation;

  public TryBuilder(ITokenSpan start) {
    this.start = start;
  }

  public void visitExcept(@Nullable IAstExpr ex, @Nullable AstIdentifier as, ITokenSpan location) {
    dcheck(state == State.TRY || state == State.EXCEPT);
    state = State.EXCEPT;
    excepts.add(new Except(ex, as, location));
  }

  @Override
  public void visitElse(ITokenSpan location) {
    dcheck(state == State.TRY || state == State.EXCEPT);
    state = State.ELSE;
    elseLocation = location;
  }

  public void visitFinally(ITokenSpan location) {
    dcheck(state == State.TRY || state == State.EXCEPT || state == State.ELSE);
    state = State.FINALLY;
    finallyLocation = location;
  }

  @Override
  public void add(IAstStmt stmt) {
    switch (state) {
      case TRY:
        body.add(stmt);
        break;
      case EXCEPT:
        last(excepts).body.add(stmt);
        break;
      case ELSE:
        elseBody.add(stmt);
        break;
      case FINALLY:
        finallyBody.add(stmt);
        break;
    }
  }

  public IAstStmt build() {
    return new AstTry(
        start,
        body.build(),
        excepts.stream().map(Except::build).toArray(AstExcept[]::new),
        elseLocation != null ? new AstElse(elseBody.build(), elseLocation) : null,
        finallyLocation,
        finallyLocation != null ? finallyBody.build() : null);
  }
}
