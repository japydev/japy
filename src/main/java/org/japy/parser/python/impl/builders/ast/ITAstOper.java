package org.japy.parser.python.impl.builders.ast;

import org.japy.infra.loc.IHasLocation;
import org.japy.parser.python.ast.expr.IAstExpr;

public interface ITAstOper extends IHasLocation {
  IAstExpr apply(IAstExpr e);
}
