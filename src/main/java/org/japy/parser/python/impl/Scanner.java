package org.japy.parser.python.impl;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.eclipse.collections.impl.list.mutable.primitive.IntArrayList;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.LocationUtil;
import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.parser.ScannerError;
import org.japy.parser.python.ScannerInput;
import org.japy.parser.python.impl.javacc.Token;
import org.japy.parser.python.impl.javacc.TokenManager;
import org.japy.parser.python.impl.token.BytesToken;
import org.japy.parser.python.impl.token.RFConvToken;
import org.japy.parser.python.impl.token.TokenImpl;

public final class Scanner extends InputReader implements TokenManager {
  private enum State {
    NEW_LINE,
    REGULAR,
    FSTRING,
    FSTRING2,
    RF,
    RF_FORMAT_SPEC,
    RF_END,
  }

  private enum StringPrefix {
    NONE_S('\'', 0, false, false),
    NONE_D('"', 0, false, false),
    R_S('\'', 1, true, false),
    R_D('"', 1, true, false),
    B_S('\'', 1, false, true),
    B_D('"', 1, false, true),
    RB_S('\'', 2, true, true),
    RB_D('"', 2, true, true),
    U_S('\'', 1, false, false),
    U_D('"', 1, false, false),
    F_S('\'', 1, false, false),
    F_D('"', 1, false, false),
    RF_S('\'', 2, true, false),
    RF_D('"', 2, true, false),
    ;

    public final byte quoteChar;
    public final int len;
    public final boolean raw;
    public final boolean bytes;

    StringPrefix(int quoteChar, int len, boolean raw, boolean bytes) {
      this.quoteChar = (byte) quoteChar;
      this.len = len;
      this.raw = raw;
      this.bytes = bytes;
    }
  }

  private final Deque<Token> pendingTokens = new ArrayDeque<>(20);

  private final NumberScanner ns;
  private State state;
  private final IntArrayList indentation;
  private int parens;

  private final Map<String, String> identifiers = new HashMap<>();

  private static class FStringInfo {
    final StringPrefix prefix;
    final byte quote;
    final boolean longString;

    FStringInfo(StringPrefix prefix, byte quote, boolean longString) {
      this.prefix = prefix;
      this.quote = quote;
      this.longString = longString;
    }
  }

  private static class RFState {
    final State prevState;
    int parens;

    private RFState(State prevState) {
      this.prevState = prevState;
    }
  }

  private @Nullable FStringInfo prevFString;
  private @Nullable FStringInfo curFString;
  private final List<RFState> replFieldStack = new ArrayList<>();

  private final ByteWriter scratch = new ByteWriter();
  private final ByteWriter scratch2 = new ByteWriter();
  private final CharsetDecoder utf8Decoder;

  private Scanner(byte[] content, boolean knownUtf8, String fileName) {
    super(fileName, content, knownUtf8);

    ns = new NumberScanner(this);

    indentation = new IntArrayList(8);
    indentation.add(0);
    state = State.NEW_LINE;

    utf8Decoder = StandardCharsets.UTF_8.newDecoder();
    utf8Decoder.onMalformedInput(CodingErrorAction.REPORT);

    pendingTokens.push(new TokenImpl(Token.START, "<START>", 1, 1, 1));
  }

  public Scanner(Path path) throws IOException {
    this(Files.readAllBytes(path), false, path.toString());
  }

  public Scanner(String text, String sourceFile) {
    this(text.getBytes(StandardCharsets.UTF_8), true, sourceFile);
  }

  public Scanner(byte[] text, String sourceFile) {
    this(text, false, sourceFile);
  }

  public Scanner(ScannerInput input) {
    this(input.getBytes(), input.isUtf8(), input.sourceFile());
  }

  @Override
  public Token getNextToken() {
    if (!pendingTokens.isEmpty()) {
      return pendingTokens.pop();
    }

    try {
      return doGetNextToken();
    } catch (Throwable ex) {
      LocationUtil.setFileIfNotSet(ex, sourceFile);
      throw ex;
    }
  }

  private Token doGetNextToken() {
    while (true) {
      switch (state) {
        case NEW_LINE:
          {
            @Var int indent = skipIndent();
            if (curPos() == bufferSize) {
              // Indents at EOF. Pretend it's just an empty line.
              indent = 0;
            }
            int curIndent = indentation.get(indentation.size() - 1);
            if (indent == curIndent) {
              switchTo(State.REGULAR);
            } else if (indent > curIndent) {
              switchTo(State.REGULAR);
              indentation.add(indent);
              return tokIndent();
            } else {
              int dedentCount = popIndents(indent);
              for (int i = 0; i < dedentCount - 1; ++i) {
                pendingTokens.addLast(tokDedent());
              }
              switchTo(State.REGULAR);
              return tokDedent();
            }
            break;
          }

        case REGULAR:
          {
            @Nullable Token t = nextRegular();
            if (t != null) {
              return t;
            }
            break;
          }

        case RF:
        case RF_END:
          return nextReplField();

        case RF_FORMAT_SPEC:
          return nextReplFieldFormatSpec();

        case FSTRING:
        case FSTRING2:
          return nextFString(curLine(), curCol());
      }
    }
  }

  private void switchTo(State state) {
    this.state = state;
  }

  private int popIndents(int indent) {
    for (int count = 0; ; ++count) {
      dcheck(indentation.size() > 0);
      int prevIndent = indentation.get(indentation.size() - 1);
      if (prevIndent == indent) {
        dcheck(count > 0);
        return count;
      } else if (prevIndent < indent) {
        throw new ScannerError("inconsistent indentation", curLoc());
      }
      indentation.removeAtIndex(indentation.size() - 1);
    }
  }

  private int skipIndent() {
    @Var int indent = 0;
    while (curPos() < bufferSize) {
      byte c = curChar();
      if (c == ' ') {
        ++indent;
        incPos();
      } else if (c == '\t') {
        indent = 8 * (indent / 8 + 1);
        incPos();
      } else if (c == '\f') {
        indent = 0;
        incPos();
      } else if (isEol(c)) {
        indent = 0;
        skipEol();
      } else if (c == '#') {
        indent = 0;
        skipComment();
      } else {
        break;
      }
    }

    return indent;
  }

  private Token tokIndent() {
    return new TokenImpl(Token.INDENT, "INDENT", curLine(), curCol(), curLine(), curCol());
  }

  private Token tokDedent() {
    return new TokenImpl(Token.DEDENT, "DEDENT", curLine(), curCol(), curLine(), curCol());
  }

  private Token tokEof() {
    return new TokenImpl(Token.EOF, "EOF", curLine(), curCol(), curLine(), curCol());
  }

  private Token tokNewLine() {
    return new TokenImpl(Token.NEWLINE, "NEWLINE", curLine(), curCol(), curLine(), curCol());
  }

  private Token eof() {
    int dedentCount = indentation.size() - 1;
    for (int i = 0; i < dedentCount; ++i) {
      pendingTokens.addLast(tokDedent());
    }
    pendingTokens.addLast(tokEof());
    return tokNewLine();
  }

  @CanIgnoreReturnValue
  private @Nullable Token skipComment() {
    checkCurChar('#');
    incPos();
    while (curPos() < bufferSize) {
      byte c = curChar();
      if (isEol(c)) {
        @Nullable Token tok = parens == 0 ? tokNewLine() : null;
        skipEol();
        if (parens == 0) {
          switchTo(State.NEW_LINE);
        }
        return tok;
      } else {
        incPos();
      }
    }

    return null;
  }

  private void skipSpaceToNextToken() {
    while (curPos() < bufferSize) {
      byte c = curChar();
      switch (c) {
        case ' ':
        case '\t':
        case '\f':
          incPos();
          break;

        default:
          return;
      }
    }

    throw unexpectedEof();
  }

  private @Nullable Token nextRegular() {
    dcheck(state == State.REGULAR || state == State.RF || state == State.RF_END);

    while (curPos() < bufferSize) {
      byte c = curChar();

      if (isEol(c)) {
        @Nullable Token tok = parens == 0 ? tokNewLine() : null;
        skipEol();
        if (parens == 0) {
          switchTo(State.NEW_LINE);
          return tok;
        }
        continue;
      }

      switch (c) {
        case '#':
          {
            @Nullable Token tokNewLine = skipComment();
            dcheck((tokNewLine != null) == (state == State.NEW_LINE));
            if (tokNewLine != null) {
              return tokNewLine;
            }
            continue;
          }

        case ' ':
        case '\t':
        case '\f':
          incPos();
          continue;

        case '\\':
          if (isEol(nextChar())) {
            incPos();
            skipEol();
            return null;
          } else {
            throw unexpectedCharacter();
          }

        case '\'':
          return stringLiteral(StringPrefix.NONE_S);
        case '"':
          return stringLiteral(StringPrefix.NONE_D);

        case '.':
          return startsWithDot();

        default:
          if (isLetter(c)) {
            return startsWithLetter();
          }

          if (isDigit(c)) {
            return startsWithDigit();
          }

          return operatorOrDelimiter();
      }
    }

    return eof();
  }

  private ScannerError unexpectedEof() {
    return unexpectedEof(this);
  }

  static ScannerError unexpectedEof(InputReader r) {
    throw new ScannerError("unexpected end of file", r.curLoc());
  }

  private ScannerError unexpectedCharacter() {
    throw new ScannerError(
        String.format("unexpected character %s", formatChar(curChar())), curLoc());
  }

  public static String formatChar(byte c) {
    if (c > 0) {
      char ch = (char) c;
      Character.UnicodeBlock block = Character.UnicodeBlock.of(ch);
      if (!Character.isISOControl(c)
          && block != null
          && !block.equals(Character.UnicodeBlock.SPECIALS)) {
        return String.format("'%c'", ch);
      }
    }

    return String.format("'\\x%02x'", c);
  }

  private Token tokN(int n, int tokenKind, String image) {
    incPos(n);
    return new TokenImpl(tokenKind, image, curLine(), curCol() - n, curLine(), curCol());
  }

  private Token tok(Delim delim) {
    return tokN(delim.len, delim.tokenKind, delim.text);
  }

  private Token tok(Operator op) {
    return tokN(op.len, op.tokenKind, op.text);
  }

  private Token startsWithDot() {
    checkCurChar('.');
    if (curPos() + 1 == bufferSize) {
      return tok(Delim.DOT);
    }

    byte c2 = nextChar();
    if (isDigit(c2)) {
      return decimalNoWholePart();
    }

    if (c2 == '.' && nextNextChar() == '.') {
      return tok(Delim.ELLIPSIS);
    }

    return tok(Delim.DOT);
  }

  private Token operatorOrDelimiter() {
    byte c = curChar();
    byte c2 = nextChar();
    switch (c) {
      case ',':
        return tok(Delim.COMMA);
      case ';':
        return tok(Delim.SEMICOLON);
      case '(':
        pushParens();
        return tok(Delim.LPAREN);
      case ')':
        popParens();
        return tok(Delim.RPAREN);
      case '[':
        pushParens();
        return tok(Delim.LBRACKET);
      case ']':
        popParens();
        return tok(Delim.RBRACKET);
      case '{':
        pushParens();
        return tok(Delim.LBRACE);
      case '}':
        popParens();
        return tok(Delim.RBRACE);

      case ':':
        return c2 == '=' ? tok(Operator.WALRUS) : tok(Delim.COLON);

      case '!':
        if (c2 == '=') {
          return tok(Operator.NE);
        } else {
          throw unexpectedCharacter();
        }

      case '~':
        return tok(Operator.INV);
      case '+':
        return tok(c2 == '=' ? Operator.IADD : Operator.ADD);
      case '%':
        return tok(c2 == '=' ? Operator.IMOD : Operator.MOD);
      case '@':
        return tok(c2 == '=' ? Operator.IMATUL : Operator.AT);
      case '&':
        return tok(c2 == '=' ? Operator.IBAND : Operator.BAND);
      case '|':
        return tok(c2 == '=' ? Operator.IBOR : Operator.BOR);
      case '^':
        return tok(c2 == '=' ? Operator.IXOR : Operator.XOR);
      case '=':
        return tok(c2 == '=' ? Operator.EQ : Operator.ASSIGN);
      case '-':
        return c2 == '=' ? tok(Operator.ISUB) : (c2 == '>' ? tok(Delim.ARROW) : tok(Operator.SUB));
      case '*':
        if (c2 == '=') {
          return tok(Operator.IMUL);
        } else if (c2 == '*') {
          return tok(nextNextChar() == '=' ? Operator.IPOW : Operator.POW);
        } else {
          return tok(Operator.MUL);
        }
      case '/':
        if (c2 == '=') {
          return tok(Operator.IDIV);
        } else if (c2 == '/') {
          return tok(nextNextChar() == '=' ? Operator.IFLOORDIV : Operator.FLOORDIV);
        } else {
          return tok(Operator.DIV);
        }
      case '<':
        if (c2 == '=') {
          return tok(Operator.LE);
        } else if (c2 == '<') {
          return tok(nextNextChar() == '=' ? Operator.ILSHIFT : Operator.LSHIFT);
        } else {
          return tok(Operator.LT);
        }
      case '>':
        if (c2 == '=') {
          return tok(Operator.GE);
        } else if (c2 == '>') {
          return tok(nextNextChar() == '=' ? Operator.IRSHIFT : Operator.RSHIFT);
        } else {
          return tok(Operator.GT);
        }
      default:
        throw unexpectedCharacter();
    }
  }

  private void pushParens() {
    if (replFieldStack.isEmpty()) {
      ++parens;
    } else {
      ++replFieldStack.get(replFieldStack.size() - 1).parens;
    }
  }

  private void popParens() {
    if (replFieldStack.isEmpty()) {
      if (parens == 0) {
        throw new ScannerError("unmatched ')'", curLoc());
      }
      --parens;
    } else {
      RFState rfState = replFieldStack.get(replFieldStack.size() - 1);
      if (rfState.parens == 0) {
        throw new ScannerError("f-string: unmatched ')'", curLoc());
      }
      --rfState.parens;
    }
  }

  private String decode(byte[] buffer, int start, int len) {
    try {
      return utf8Decoder.decode(ByteBuffer.wrap(buffer, start, len)).toString();
    } catch (CharacterCodingException ex) {
      throw new ScannerError("invalid unicode input", curLoc(), ex);
    }
  }

  private String decode(byte[] buffer) {
    return decode(buffer, 0, buffer.length);
  }

  private String decodeScratch() {
    return decode(scratch.buf, 0, scratch.size);
  }

  private String readWord() {
    checkCurCharLetter();
    @Var byte c = curChar();
    scratch.reset();
    scratch.write(c);
    incPos();
    while (curPos() < bufferSize) {
      c = curChar();
      if (!isLetter(c) && !isDigit(c)) {
        break;
      }
      scratch.write(c);
      incPos();
    }

    return decodeScratch();
  }

  private String internName(String name) {
    @Nullable String intern = identifiers.putIfAbsent(name, name);
    return intern != null ? intern : name;
  }

  private Token identifier() {
    return wordLike(false);
  }

  private Token identifierOrKeyword() {
    return wordLike(true);
  }

  private Token wordLike(boolean maybeKeyword) {
    int startCol = curCol();
    String name = readWord();
    if (maybeKeyword) {
      @Nullable Keyword kw = Keyword.lookup(name);
      if (kw != null) {
        return new TokenImpl(kw.tokenKind, kw.text, curLine(), startCol, curCol());
      }
    }

    return new TokenImpl(Token.IDENTIFIER, internName(name), curLine(), startCol, curCol());
  }

  private Token startsWithLetter() {
    checkCurCharLetter();
    byte c = curChar();

    if (curPos() + 1 == bufferSize) {
      return identifier();
    }

    byte c2 = nextChar();
    byte c3 = nextNextChar();

    if (isU(c) && isQuote(c2)) {
      return stringLiteral(c2 == '"' ? StringPrefix.U_D : StringPrefix.U_S);
    }

    if (isR(c)) {
      if (isQuote(c2)) {
        return stringLiteral(c2 == '"' ? StringPrefix.R_D : StringPrefix.R_S);
      } else if (isQuote(c3)) {
        if (isF(c2)) {
          return fString(c3 == '"' ? StringPrefix.RF_D : StringPrefix.RF_S);
        } else if (isB(c2)) {
          return stringLiteral(c3 == '"' ? StringPrefix.RB_D : StringPrefix.RB_S);
        }
      }
    } else if (isF(c)) {
      if (isQuote(c2)) {
        return fString(c2 == '"' ? StringPrefix.F_D : StringPrefix.F_S);
      } else if (isR(c2) && isQuote(c3)) {
        return fString(c3 == '"' ? StringPrefix.RF_D : StringPrefix.RF_S);
      }
    } else if (isB(c)) {
      if (isQuote(c2)) {
        return stringLiteral(c2 == '"' ? StringPrefix.B_D : StringPrefix.B_S);
      } else if (isR(c2) && isQuote(c3)) {
        return stringLiteral(c3 == '"' ? StringPrefix.RB_D : StringPrefix.RB_S);
      }
    }

    return identifierOrKeyword();
  }

  private Token stringLiteral(StringPrefix prefix) {
    byte q = charAt(prefix.len);
    dcheck(q == '"' || q == '\'');
    if (curPos() + prefix.len + 2 < bufferSize
        && charAt(prefix.len + 1) == q
        && charAt(prefix.len + 2) == q) {
      return longStringLiteral(prefix);
    }

    int startLine = curLine();
    int startCol = curCol();
    incPos(prefix.len + 1);

    scratch.reset();

    while (curPos() < bufferSize) {
      byte c = curChar();
      if (c == q) {
        incPos();
        return stringToken(prefix, startLine, startCol);
      }

      if (c == '\\') {
        handleEscape(prefix);
        continue;
      }

      if (isEol(c)) {
        throw new ScannerError("line end inside a string literal", curLoc());
      }

      scratch.write(c);
      incPos();
    }

    throw new ScannerError("unterminated string literal", curLoc());
  }

  private Token longStringLiteral(StringPrefix prefix) {
    byte q = charAt(prefix.len);

    int startLine = curLine();
    int startCol = curCol();
    incPos(prefix.len + 3);

    scratch.reset();

    while (curPos() < bufferSize) {
      byte c = curChar();
      if (c == q && nextChar() == q && nextNextChar() == q) {
        incPos(3);
        return stringToken(prefix, startLine, startCol);
      }

      if (c == '\\') {
        handleEscape(prefix);
        continue;
      }

      if (isEol(c)) {
        scratch.write(c);
        if (c == '\r' && nextChar() == '\n') {
          scratch.write('\n');
        }
        skipEol();
        continue;
      }

      scratch.write(c);
      incPos();
    }

    throw new ScannerError("unterminated string literal", curLoc());
  }

  private void readOctalEscape() {
    byte c1 = curChar();
    byte c2 = nextChar();
    byte c3 = nextNextChar();
    if (Debug.ENABLED) {
      dcheck(isOctal(c1));
    }

    if (!isOctal(c2)) {
      incPos(1);
      scratch.write(octalDigit(c1));
      return;
    }

    if (!isOctal(c3)) {
      incPos(2);
      scratch.write(8 * octalDigit(c1) + octalDigit(c2));
      return;
    }

    int value = 64 * octalDigit(c1) + 8 * octalDigit(c2) + octalDigit(c3);
    if (value >= 256) {
      throw new ScannerError("invalid escape sequence, value is not less than 256", curLoc());
    }
    incPos(3);
    writeByte(value);
  }

  private void readHexEscape() {
    checkCurChar('x');
    if (curPos() + 2 >= bufferSize) {
      throw unexpectedEof();
    }
    byte c1 = nextChar();
    byte c2 = nextNextChar();
    if (!isHex(c1) || !isHex(c2)) {
      throw new ScannerError(
          "invalid escape sequence, expected exactly two hexadecimal digits", curLoc());
    }

    incPos(3);
    writeByte(16 * hexDigit(c1) + hexDigit(c2));
  }

  private void writeCodePoint(int codePoint) {
    // TODO: invalid characters
    ExcUtil.wrapIO(
        () -> scratch.write(Character.toString(codePoint).getBytes(StandardCharsets.UTF_8)));
  }

  private void writeByte(int c) {
    dcheck(c >= 0 && c < 256);
    if (c > Byte.MAX_VALUE) {
      writeCodePoint(c);
    } else {
      scratch.write(c);
    }
  }

  private void readUnicodeEscape(int len) {
    checkCurChar('u', 'U');
    if (curPos() + len >= bufferSize) {
      throw unexpectedEof();
    }
    @Var int value = 0;
    for (int i = 0; i < len; ++i) {
      byte c = charAt(i + 1);
      if (!isHex(c)) {
        throw new ScannerError(
            String.format(
                "unexpected character %s, expected exactly %s hexadecimal digits",
                formatChar(c), len),
            curLoc());
      }
      value = value * 16 + hexDigit(c);
    }
    writeCodePoint(value);
    incPos(len + 1);
  }

  private int getNamedCharacter(String name) {
    try {
      return Character.codePointOf(name);
    } catch (IllegalArgumentException ignored) {
      // unknown code point
    }

    if (name.startsWith("CJK UNIFIED IDEOGRAPH-")) {
      try {
        return Integer.parseInt(name.substring("CJK UNIFIED IDEOGRAPH-".length()), 16);
      } catch (NumberFormatException ignored) {
        // not a number
      }
    }

    // TODO: HANGUL SYLLABLE

    throw new ScannerError("unrecognized character name: " + name, curLoc());
  }

  private void readNamedCharacter() {
    checkCurChar('N');
    incPos();
    if (curChar() != '{') {
      throw new ScannerError(
          String.format("unexpected character %s, expected '{'", formatChar(curChar())), curLoc());
    }

    incPos();

    scratch2.reset();

    while (curPos() < bufferSize) {
      byte c = curChar();

      if (isEol(c)) {
        throw unexpectedCharacter();
      }

      if (c == '}') {
        incPos();
        String name = decode(scratch2.toByteArray());
        writeCodePoint(getNamedCharacter(name));
        return;
      } else {
        scratch2.write(c);
        incPos();
      }
    }

    throw unexpectedEof();
  }

  private void handleEscape(StringPrefix prefix) {
    checkCurChar('\\');

    incPos();

    if (curPos() == bufferSize) {
      throw new ScannerError("unterminated string literal", curLoc());
    }

    byte c = curChar();
    if (isEol(c)) {
      skipEol();
      return;
    }

    if (prefix.raw) {
      scratch.write('\\');
      if (c == prefix.quoteChar || c == '\\') {
        scratch.write(c);
        incPos();
      }
      return;
    }

    switch (c) {
      case '\\':
        scratch.write('\\');
        incPos();
        break;
      case '\'':
        scratch.write('\'');
        incPos();
        break;
      case '"':
        scratch.write('"');
        incPos();
        break;
      case 'a':
        scratch.write(7);
        incPos();
        break;
      case 'b':
        scratch.write(8);
        incPos();
        break;
      case 'f':
        scratch.write('\f');
        incPos();
        break;
      case 'r':
        scratch.write('\r');
        incPos();
        break;
      case 'n':
        scratch.write('\n');
        incPos();
        break;
      case 't':
        scratch.write('\t');
        incPos();
        break;
      case 'v':
        scratch.write(12);
        incPos();
        break;
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
        readOctalEscape();
        break;
      case 'x':
        readHexEscape();
        break;
      case 'N':
        if (prefix.bytes) {
          throw new ScannerError("\\N is not allowed in byte strings", curLoc());
        }
        readNamedCharacter();
        break;
      case 'U':
        if (prefix.bytes) {
          throw new ScannerError("\\u and \\U are not allowed in byte strings", curLoc());
        }
        readUnicodeEscape(8);
        break;
      case 'u':
        if (prefix.bytes) {
          throw new ScannerError("\\u and \\U are not allowed in byte strings", curLoc());
        }
        readUnicodeEscape(4);
        break;
      default:
        // TODO: warning
        scratch.write('\\');
        break;
    }
  }

  private Token stringToken(StringPrefix prefix, int startLine, int startCol) {
    if (prefix.bytes) {
      return new BytesToken(scratch.toByteArray(), startLine, startCol, curLine(), curCol());
    } else {
      return new TokenImpl(Token.STRING, decodeScratch(), startLine, startCol, curLine(), curCol());
    }
  }

  private void pushFString(FStringInfo fsi) {
    switch (state) {
      case REGULAR:
        switchTo(State.FSTRING);
        dcheck(curFString == null);
        break;
      case RF:
        switchTo(State.FSTRING2);
        dcheckNotNull(curFString);
        prevFString = curFString;
        break;
      default:
        throw new InternalErrorException("pushFString " + state, curLoc());
    }

    curFString = fsi;
  }

  private void popFString() {
    switch (state) {
      case FSTRING:
        switchTo(State.REGULAR);
        dcheck(prevFString == null);
        curFString = null;
        break;
      case FSTRING2:
        switchTo(State.RF);
        dcheckNotNull(prevFString);
        curFString = prevFString;
        prevFString = null;
        break;
      default:
        throw new InternalErrorException("popFString " + state, curLoc());
    }
  }

  private void pushReplField() {
    dcheck(
        state == State.FSTRING
            || state == State.FSTRING2
            || state == State.RF
            || state == State.RF_FORMAT_SPEC
            || state == State.RF_END);
    dcheckNotNull(curFString);

    if (curFString.longString) {
      pushParens();
    }

    replFieldStack.add(new RFState(state));
    switchTo(State.RF);
  }

  private void popReplField() {
    dcheck(state == State.RF || state == State.RF_FORMAT_SPEC || state == State.RF_END);
    dcheck(!replFieldStack.isEmpty());
    dcheckNotNull(curFString);

    RFState rfState = replFieldStack.remove(replFieldStack.size() - 1);
    dcheck(rfState.parens == 0);
    switchTo(rfState.prevState);

    if (curFString.longString) {
      popParens();
    }
  }

  private Token fString(StringPrefix prefix) {
    byte quote = charAt(prefix.len);
    int startLine = curLine();
    int startCol = curCol();
    incPos(prefix.len);

    @Var boolean longString = false;
    incPos();
    if (curChar() == quote && nextChar() == quote) {
      longString = true;
      incPos(2);
    }

    pushFString(new FStringInfo(prefix, quote, longString));
    return nextFString(startLine, startCol);
  }

  private Token nextFString(int startLine, int startCol) {
    dcheck(state == State.FSTRING || state == State.FSTRING2);
    dcheckNotNull(curFString);

    scratch.reset();

    while (curPos() < bufferSize) {
      byte c = curChar();
      if (c == curFString.quote) {
        if (curFString.longString) {
          if (nextChar() == c && nextNextChar() == c) {
            incPos(3);
            Token tok = stringToken(curFString.prefix, startLine, startCol);
            popFString();
            return tok;
          }
        } else {
          incPos();
          Token tok = stringToken(curFString.prefix, startLine, startCol);
          popFString();
          return tok;
        }
      }

      if (c == '\\') {
        handleEscape(curFString.prefix);
        continue;
      }

      if (!curFString.longString && isEol(c)) {
        throw new ScannerError("line end inside a string literal", curLoc());
      }

      if (c == '{') {
        if (nextChar() == '{') {
          scratch.write('{');
          incPos(2);
          continue;
        }

        Token cur = stringToken(curFString.prefix, startLine, startCol);
        pendingTokens.addLast(
            new TokenImpl(Token.RF_START, "{", curLine(), curCol(), curLine(), curCol() + 1));
        incPos();
        pushReplField();
        return cur;
      }

      if (c == '}') {
        if (nextChar() == '}') {
          scratch.write('}');
          incPos(2);
          continue;
        }

        throw new ScannerError("lone }", curLoc());
      }

      scratch.write(c);
      incPos();
    }

    throw new ScannerError("unterminated string literal", curLoc());
  }

  private Token nextReplField() {
    dcheck(state == State.RF_END || state == State.RF);
    dcheckNotNull(curFString);

    while (curPos() < bufferSize) {
      skipSpaceToNextToken();

      byte c = curChar();

      if (isEol(c)) {
        if (!curFString.longString) {
          throw new ScannerError("end of line inside a replacement field", curLoc());
        } else {
          skipEol();
          continue;
        }
      }

      switch (c) {
        case '#':
          throw new ScannerError("comment inside a replacement field", curLoc());

        case '}':
          if (replFieldStack.get(replFieldStack.size() - 1).parens != 0) {
            return dcheckNotNull(nextRegular());
          } else {
            Token tok =
                new TokenImpl(Token.RF_END, "}", curLine(), curCol(), curLine(), curCol() + 1);
            incPos();
            popReplField();
            return tok;
          }

        case ':':
        case '!':
        case '=':
          if (state == State.RF_END) {
            return nextReplFieldEnd();
          }
          if (nextChar() != '=' && replFieldStack.get(replFieldStack.size() - 1).parens == 0) {
            switchTo(State.RF_END);
            return nextReplFieldEnd();
          } else {
            return dcheckNotNull(nextRegular());
          }

        default:
          if (state == State.RF_END) {
            return nextReplFieldEnd();
          } else {
            if (c == curFString.quote || (prevFString != null && c == prevFString.quote)) {
              throw new ScannerError("outer f-string quote inside a replacement field", curLoc());
            }
            return dcheckNotNull(nextRegular());
          }
      }
    }

    throw new ScannerError("unterminated string literal", curLoc());
  }

  private Token nextReplFieldEnd() {
    @Var byte c = curChar();
    switch (c) {
      case '=':
        return tokN(1, Token.RF_EQ, "RF_EQ");
      case '!':
        {
          int startCol = curCol();
          incPos();
          skipSpaceToNextToken();
          c = curChar();
          switch (c) {
            case 's':
            case 'a':
            case 'r':
              incPos();
              return new RFConvToken(c, curLine(), startCol, curCol());
            default:
              throw new ScannerError(
                  String.format(
                      "unexpected character %s, expected 'a', 'r', or 's'", formatChar(c)),
                  curLoc());
          }
        }

      case ':':
        {
          Token tok =
              new TokenImpl(
                  Token.RF_SPEC_START,
                  "RF_SPEC_START",
                  curLine(),
                  curCol(),
                  curLine(),
                  curCol() + 1);
          incPos();
          switchTo(State.RF_FORMAT_SPEC);
          return tok;
        }

      case '}':
        {
          Token tok =
              new TokenImpl(Token.RF_END, "RF_END", curLine(), curCol(), curLine(), curCol() + 1);
          popReplField();
          incPos();
          return tok;
        }

      default:
        throw unexpectedCharacter();
    }
  }

  private Token nextReplFieldFormatSpec() {
    int startCol = curCol();
    int startPos = curPos();
    while (curPos() < bufferSize) {
      byte c = curChar();

      if (isEol(c)) {
        throw new ScannerError("unexpected end of line inside a format specifier", curLoc());
      }

      switch (c) {
        case '{':
          {
            Token tok;
            if (curPos() > startPos) {
              tok =
                  new TokenImpl(
                      Token.RF_SPEC_FRAGMENT,
                      decode(buffer, startPos, curPos() - startPos),
                      curLine(),
                      startCol,
                      curCol());
              pendingTokens.addLast(
                  new TokenImpl(
                      Token.RF_START, "RF_START", curLine(), curCol(), curLine(), curCol() + 1));
            } else {
              tok =
                  new TokenImpl(
                      Token.RF_START, "RF_START", curLine(), curCol(), curLine(), curCol() + 1);
            }
            incPos();
            pushReplField();
            return tok;
          }

        case '}':
          {
            Token tok;
            if (curPos() > startPos) {
              tok =
                  new TokenImpl(
                      Token.RF_SPEC_FRAGMENT,
                      decode(buffer, startPos, curPos() - startPos),
                      curLine(),
                      startCol,
                      curCol());
              pendingTokens.addLast(
                  new TokenImpl(
                      Token.RF_SPEC_END, "RF_SPEC_END", curLine(), curCol(), curLine(), curCol()));
            } else {
              tok =
                  new TokenImpl(
                      Token.RF_SPEC_END, "RF_SPEC_END", curLine(), curCol(), curLine(), curCol());
            }
            pendingTokens.addLast(
                new TokenImpl(
                    Token.RF_END, "RF_END", curLine(), curCol(), curLine(), curCol() + 1));
            popReplField();
            incPos();
            return tok;
          }

        default:
          incPos();
      }
    }

    throw unexpectedEof();
  }

  private Token startsWithDigit() {
    return ns.number();
  }

  private Token decimalNoWholePart() {
    return ns.fraction();
  }
}
