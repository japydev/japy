package org.japy.parser.python.impl.token;

import org.japy.parser.python.impl.javacc.Token;

public final class ImagToken extends TokenImpl {
  public final double value;

  public ImagToken(double value, int line, int beginCol, int endCol) {
    super(Token.IMAG, "IMAG", line, beginCol, endCol);
    this.value = value;
  }

  @Override
  public String toString() {
    return value + "j";
  }
}
