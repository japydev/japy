package org.japy.parser.python.impl.token;

import static org.japy.infra.validation.Debug.dcheck;

import org.japy.infra.validation.Debug;
import org.japy.parser.python.impl.javacc.Token;

public class TokenImpl extends Token {
  public TokenImpl(int kind, String image, int beginLine, int beginCol, int endLine, int endCol) {
    super(kind, image);
    this.beginLine = beginLine;
    this.beginColumn = beginCol;
    this.endLine = endLine;
    this.endColumn = endCol;
    if (Debug.ENABLED) {
      dcheck(beginLine <= endLine);
      dcheck(beginLine < endLine || beginCol <= endCol);
    }
  }

  public TokenImpl(int kind, String image, int line, int beginCol, int endCol) {
    this(kind, image, line, beginCol, line, endCol);
  }

  @Override
  public int kind() {
    return kind;
  }

  @Override
  public Token next() {
    return next;
  }

  @Override
  public int beginLine() {
    return beginLine;
  }

  @Override
  public int beginColumn() {
    return beginColumn;
  }

  @Override
  public int endLine() {
    return endLine;
  }

  @Override
  public int endColumn() {
    return endColumn;
  }

  @Override
  public String image() {
    return image;
  }
}
