package org.japy.parser.python.impl.builders;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import org.japy.parser.python.ast.expr.AstGenerator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.node.AstKwArg;
import org.japy.parser.python.ast.node.AstPosArg;
import org.japy.parser.python.ast.node.IAstArg;
import org.japy.parser.python.impl.ParserUtil;
import org.japy.parser.python.impl.javacc.Token;

public final class ArgListBuilder extends ParserUtil {
  public final List<IAstArg> list = new ArrayList<>();

  public void add(IAstExpr e) {
    list.add(new AstPosArg(e));
  }

  public void gen(AstGenerator astGenerator) {
    dcheck(list.size() == 1);
    list.set(0, new AstPosArg(astGenerator));
  }

  public void add(Token identifier, IAstExpr e) {
    list.add(new AstKwArg(nonMangledId(identifier), e));
  }

  public IAstArg[] build() {
    return list.toArray(IAstArg[]::new);
  }
}
