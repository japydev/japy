package org.japy.parser.python.impl;

import static org.japy.infra.validation.Debug.dcheck;

import java.math.BigInteger;

import com.google.errorprone.annotations.Var;

import org.japy.infra.validation.Debug;
import org.japy.parser.ScannerError;
import org.japy.parser.python.impl.javacc.Token;
import org.japy.parser.python.impl.token.FloatToken;
import org.japy.parser.python.impl.token.ImagToken;
import org.japy.parser.python.impl.token.IntToken;

final class NumberScanner extends ScannerUtil {
  private final InputReader r;
  private final CharWriter cw = new CharWriter();

  public NumberScanner(InputReader reader) {
    this.r = reader;
  }

  Token number() {
    reset();
    r.checkCurCharDigit();
    @Var byte c = r.curChar();
    if (c == '0') {
      byte c2 = r.nextChar();
      if (isX(c2)) {
        return prefixedNumber(16, "hexadecimal", ScannerUtil::isHex);
      } else if (isO(c2)) {
        return prefixedNumber(8, "octal", ScannerUtil::isOctal);
      } else if (isB(c2)) {
        return prefixedNumber(2, "binary", ScannerUtil::isBin);
      }
    }

    int wholeEnd = scanDigits(r.curPos(), false, "decimal", ScannerUtil::isDigit);
    if (wholeEnd == r.bufferSize) {
      return getWhole(wholeEnd, false);
    }

    c = r.buffer[wholeEnd];
    if (c == '.') {
      if (wholeEnd + 1 == r.bufferSize) {
        return getDecimal(r.bufferSize, false);
      }
      cw.write('.');
      int fracEnd = scanDigits(wholeEnd + 1, false, "decimal", ScannerUtil::isDigit);
      int expEnd = scanExp(fracEnd);
      boolean imaginary = expEnd < r.bufferSize && isJ(r.buffer[expEnd]);
      return getDecimal(expEnd, imaginary);
    } else if (c == 'e' || c == 'E') {
      int expEnd = scanExp(wholeEnd);
      boolean imaginary = expEnd < r.bufferSize && isJ(r.buffer[expEnd]);
      if (expEnd == wholeEnd) {
        return getWhole(wholeEnd, imaginary);
      } else {
        return getDecimal(expEnd, imaginary);
      }
    } else {
      return getWhole(wholeEnd, isJ(r.buffer[wholeEnd]));
    }
  }

  Token fraction() {
    reset();

    if (Debug.ENABLED) {
      dcheck(r.curChar() == '.');
      dcheck(r.curPos() < r.bufferSize && isDigit(r.nextChar()));
    }

    int fracStart = r.curPos() + 1;
    int fracEnd = scanDigits(fracStart, false, "decimal", ScannerUtil::isDigit);
    int expEnd = scanExp(fracEnd);
    boolean imaginary = fracEnd < r.bufferSize && isJ(r.buffer[fracEnd]);
    return getDecimal(expEnd, imaginary);
  }

  private void reset() {
    cw.reset();
  }

  private interface DigitChecker {
    boolean check(byte c);
  }

  private Token prefixedNumber(int base, String literalName, DigitChecker digitChecker) {
    if (Debug.ENABLED) {
      dcheck(r.curChar() == '0');
      dcheck(isX(r.nextChar()) || isB(r.nextChar()) || isO(r.nextChar()));
    }
    int start = r.curPos() + 2;
    int end = scanDigits(start, true, literalName, digitChecker);
    if (start == end) {
      throw new ScannerError(String.format("invalid %s literal", literalName), r.curLoc());
    }

    BigInteger v;
    try {
      v = new BigInteger(cw.getString(), base);
    } catch (NumberFormatException ex) {
      throw new ScannerError(String.format("invalid %s literal", literalName), r.curLoc(), ex);
    }

    Token tok = new IntToken(v, r.curLine(), r.curCol(), r.curCol() + end - start + 2);
    r.incPos(end - start + 2);
    return tok;
  }

  private int scanDigits(
      int start, boolean allowLeadingUnderscore, String literalName, DigitChecker digitChecker) {
    @Var int end = start;
    @Var boolean underscore = false;
    @Var boolean seenDigits = false;
    while (end < r.bufferSize) {
      byte c = r.buffer[end];
      if (digitChecker.check(c)) {
        underscore = false;
        seenDigits = true;
        cw.write(c);
      } else if (c == '_') {
        if ((!allowLeadingUnderscore && !seenDigits) || underscore) {
          throw new ScannerError(String.format("invalid %s literal", literalName), r.curLoc());
        }
        underscore = true;
      } else if (isDigit(c)) {
        throw new ScannerError(
            String.format("invalid digit '%c' in %s literal", c, literalName), r.curLoc());
      } else {
        break;
      }
      ++end;
    }
    if (underscore) {
      throw new ScannerError(String.format("invalid %s literal", literalName), r.curLoc());
    }
    return end;
  }

  private int scanExp(int start) {
    if (start == r.bufferSize || (r.buffer[start] != 'e' && r.buffer[start] != 'E')) {
      return start;
    }
    int startSize = cw.size();
    cw.write('e');
    @Var int end = start + 1;
    if (end == r.curPos()) {
      throw Scanner.unexpectedEof(r);
    }
    @Var boolean seenSign = false;
    if (r.buffer[end] == '+' || r.buffer[end] == '-') {
      seenSign = true;
      cw.write(r.buffer[end]);
      if (++end == r.curPos()) {
        throw Scanner.unexpectedEof(r);
      }
    }
    int digitsEnd = scanDigits(end, false, "decimal", ScannerUtil::isDigit);
    if (digitsEnd == end) {
      if (seenSign) {
        throw new ScannerError("invalid decimal literal", r.curLoc());
      } else {
        // https://bugs.python.org/issue21642
        cw.setSize(startSize);
        return start;
      }
    }
    return digitsEnd;
  }

  // Integer literals starting with 0 are not allowed, except when it's just a sequence of zeros
  private static boolean isOldOctal(String text) {
    for (int i = 0, c = text.length(); i != c; ++i) {
      if (text.charAt(i) != '0') {
        return i != 0;
      }
    }
    return false;
  }

  private Token makeWhole(String text, int tokLen, boolean imaginary) {
    if (isOldOctal(text)) {
      throw new ScannerError(
          "leading zeros in decimal integer literals are not permitted; use an 0o prefix for octal integers",
          r.curLoc());
    }

    BigInteger v;
    try {
      v = new BigInteger(text);
    } catch (NumberFormatException ex) {
      throw new ScannerError("invalid decimal literal", r.curLoc(), ex);
    }

    if (imaginary) {
      return new ImagToken(v.doubleValue(), r.curLine(), r.curCol(), r.curCol() + tokLen);
    } else {
      return new IntToken(v, r.curLine(), r.curCol(), r.curCol() + tokLen);
    }
  }

  private Token makeDecimal(String text, int tokLen, boolean imaginary) {
    double v;
    try {
      v = Double.parseDouble(text);
    } catch (NumberFormatException ex) {
      throw new ScannerError("invalid decimal literal", r.curLoc(), ex);
    }

    if (imaginary) {
      return new ImagToken(v, r.curLine(), r.curCol(), r.curCol() + tokLen);
    } else {
      return new FloatToken(v, r.curLine(), r.curCol(), r.curCol() + tokLen);
    }
  }

  private interface MakeToken {
    Token make(String text, int tokLen, boolean imaginary);
  }

  private Token getWhole(int endPos, boolean imaginary) {
    return getToken(this::makeWhole, endPos, imaginary);
  }

  private Token getDecimal(int endPos, boolean imaginary) {
    return getToken(this::makeDecimal, endPos, imaginary);
  }

  private Token getToken(MakeToken makeToken, int endPos, boolean imaginary) {
    int len = endPos - r.curPos() + (imaginary ? 1 : 0);

    if (Debug.ENABLED) {
      dcheck(len > 0);
      dcheck(!imaginary || isJ(r.buffer[endPos]));
    }

    String text = cw.getString();

    Token tok = makeToken.make(text, len, imaginary);
    r.incPos(len);
    return tok;
  }
}
