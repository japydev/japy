package org.japy.parser.python.impl.ast;

import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstDecorator;
import org.japy.parser.python.ast.node.IAstArg;
import org.japy.parser.python.ast.stmt.AstClass;
import org.japy.parser.python.ast.stmt.IAstStmt;

final class ClassBuilder implements SuiteHandler {
  private final AstDecorator[] decorators;
  private final AstIdentifier name;
  private final IAstArg[] supers;
  private final ITokenSpan start;

  private final SuiteBuilder body = new SuiteBuilder();

  public ClassBuilder(
      AstDecorator[] decorators, AstIdentifier name, IAstArg[] supers, ITokenSpan start) {
    this.decorators = decorators;
    this.name = name;
    this.supers = supers;
    this.start = start;
  }

  @Override
  public void add(IAstStmt stmt) {
    body.add(stmt);
  }

  public IAstStmt build() {
    return new AstClass(decorators, name, supers, body.build(), start);
  }
}
