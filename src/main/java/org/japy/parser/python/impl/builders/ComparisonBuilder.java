package org.japy.parser.python.impl.builders;

import java.util.ArrayList;
import java.util.List;

import org.japy.base.CompOp;
import org.japy.parser.python.ast.expr.AstComparison;
import org.japy.parser.python.ast.expr.IAstExpr;

public final class ComparisonBuilder {
  private final List<IAstExpr> operands = new ArrayList<>(1);
  private final List<CompOp> ops = new ArrayList<>();

  public ComparisonBuilder(IAstExpr expr) {
    operands.add(expr);
  }

  public void add(CompOp op, IAstExpr expr) {
    ops.add(op);
    operands.add(expr);
  }

  public IAstExpr build() {
    if (ops.isEmpty()) {
      return operands.get(0);
    }
    return new AstComparison(operands.toArray(IAstExpr[]::new), ops.toArray(CompOp[]::new));
  }
}
