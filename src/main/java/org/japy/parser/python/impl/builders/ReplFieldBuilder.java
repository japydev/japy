package org.japy.parser.python.impl.builders;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.Location;
import org.japy.infra.loc.Range;
import org.japy.parser.ParserError;
import org.japy.parser.python.ast.expr.AstString;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.node.AstReplField;
import org.japy.parser.python.ast.node.AstStringLiteral;
import org.japy.parser.python.ast.node.IAstStringChunk;
import org.japy.parser.python.impl.ParserUtil;
import org.japy.parser.python.impl.javacc.PythonParser;
import org.japy.parser.python.impl.javacc.Token;
import org.japy.parser.python.impl.token.RFConvToken;

public final class ReplFieldBuilder extends ParserUtil {
  private final PythonParser<?> parser;
  private @Nullable IAstExpr expr;
  private @Nullable RFConvToken conv;
  private @Nullable List<IAstStringChunk> spec;
  private @Nullable Token eq;

  public ReplFieldBuilder(PythonParser<?> parser) {
    this.parser = parser;
  }

  public void set(IAstExpr expr) {
    if (this.expr != null) {
      throw new InternalErrorException("multiple repl field expressions", expr);
    }
    this.expr = expr;
  }

  public void eq(Token eq) {
    this.eq = eq;
  }

  public void conv(Token tok) {
    dcheck(tok instanceof RFConvToken);
    if (conv != null) {
      throw new InternalErrorException("multiple conversions", tok);
    }
    conv = (RFConvToken) tok;
  }

  public void spec(Token tok) {
    if (spec == null) {
      spec = new ArrayList<>();
    }
    spec.add(new AstStringLiteral(tok.image(), t(tok)));
  }

  public void spec(AstReplField rf) {
    if (spec == null) {
      spec = new ArrayList<>();
    }
    spec.add(rf);
  }

  private String getText(Token after, Token before) {
    return parser
        .scanner()
        .getText(new Range(new Location(after.endLine, after.endColumn), before.start()));
  }

  public AstReplField build(Token start, Token end) {
    if (expr == null) {
      throw new ParserError("replacement field with no expression", start.location());
    }
    return new AstReplField(
        t(start),
        t(end),
        eq != null ? getText(start, eq.next) : null,
        expr,
        conv != null ? conv.conversion.astConv : null,
        spec != null ? new AstString(spec.toArray(IAstStringChunk[]::new)) : null);
  }
}
