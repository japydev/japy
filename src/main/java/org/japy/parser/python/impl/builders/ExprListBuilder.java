package org.japy.parser.python.impl.builders;

import java.util.ArrayList;
import java.util.List;

import org.japy.parser.python.ast.expr.AstTuple;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.impl.ParserUtil;

public final class ExprListBuilder extends ParserUtil {
  private final List<IAstExpr> exprs = new ArrayList<>();
  public boolean trailingComma;

  public void add(IAstExpr expr) {
    exprs.add(expr);
  }

  public IAstExpr build() {
    return exprs.size() > 1 || trailingComma
        ? new AstTuple(exprs.toArray(IAstExpr[]::new))
        : exprs.get(0);
  }
}
