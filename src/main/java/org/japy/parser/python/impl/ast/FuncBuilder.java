package org.japy.parser.python.impl.ast;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstDecorator;
import org.japy.parser.python.ast.node.AstParam;
import org.japy.parser.python.ast.stmt.AstFunc;
import org.japy.parser.python.ast.stmt.AstFuncHeader;
import org.japy.parser.python.ast.stmt.IAstStmt;

final class FuncBuilder implements SuiteHandler {
  private final AstFuncHeader header;

  private final SuiteBuilder body = new SuiteBuilder();

  FuncBuilder(
      boolean async,
      AstDecorator[] decorators,
      AstIdentifier name,
      AstParam[] params,
      @Nullable IAstExpr retType,
      ITokenSpan location) {
    header = new AstFuncHeader(async, decorators, name, params, retType, location);
  }

  @Override
  public void add(IAstStmt stmt) {
    body.add(stmt);
  }

  public IAstStmt build() {
    return new AstFunc(header, body.build());
  }
}
