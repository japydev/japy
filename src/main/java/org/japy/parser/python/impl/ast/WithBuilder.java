package org.japy.parser.python.impl.ast;

import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstWithItem;
import org.japy.parser.python.ast.stmt.AstWith;
import org.japy.parser.python.ast.stmt.IAstStmt;

final class WithBuilder implements SuiteHandler {
  private final boolean async;
  private final AstWithItem[] items;
  private final ITokenSpan location;
  private final SuiteBuilder body = new SuiteBuilder();

  WithBuilder(boolean async, AstWithItem[] items, ITokenSpan location) {
    this.async = async;
    this.items = items;
    this.location = location;
  }

  @Override
  public void add(IAstStmt stmt) {
    body.add(stmt);
  }

  public IAstStmt build() {
    return new AstWith(async, items, body.build(), location);
  }
}
