package org.japy.parser.python.impl.builders.ast;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.SyntaxError;
import org.japy.infra.loc.IHasLocation;
import org.japy.parser.ParserError;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.node.AstWithItem;
import org.japy.parser.python.impl.builders.ITargetConsumer;

public class TAstWithItem implements ITargetConsumer {
  private @Nullable IAstExpr singleExpr;
  private @Nullable TAstTarget singleTarget;
  private final List<TAstWithItem> children = new ArrayList<>();

  public TAstWithItem() {}

  public TAstWithItem(IAstExpr expr) {
    singleExpr = expr;
  }

  public static AstWithItem[] grok(List<TAstWithItem> tItems) {
    List<AstWithItem> items = new ArrayList<>(tItems.size());
    if (tItems.size() > 1) {
      // with X, Y: X and Y are necessarily expressions
      for (TAstWithItem item : tItems) {
        items.add(item.singleItem());
      }
    } else {
      // otherwise it may be something like `with X` or `with (X, Y)`
      tItems.get(0).flatten(items);
    }
    return items.toArray(AstWithItem[]::new);
  }

  private AstWithItem singleItem() {
    if (singleExpr == null) {
      throw new SyntaxError("invalid syntax", dcheckNotNull(children.get(0).singleExpr));
    }
    return new AstWithItem(singleExpr, singleTarget != null ? singleTarget.expr : null);
  }

  private void flatten(List<AstWithItem> items) {
    if (singleExpr != null) {
      items.add(new AstWithItem(singleExpr, singleTarget != null ? singleTarget.expr : null));
    } else {
      for (TAstWithItem child : children) {
        items.add(child.singleItem());
      }
    }
  }

  public void add(TAstWithItem child) {
    dcheck(singleExpr == null);
    children.add(child);
  }

  private void ensureSingleExpr(IHasLocation location) {
    if (singleExpr != null) {
      return;
    }

    if (children.size() > 1) {
      throw new ParserError("invalid syntax", location);
    }

    TAstWithItem child = children.get(0);
    if (child.singleTarget != null || !child.children.isEmpty()) {
      throw new ParserError("invalid syntax", location);
    }

    singleExpr = dcheckNotNull(child.singleExpr);
    children.clear();
  }

  @Override
  public void target(TAstTarget tgt) {
    if (!tgt.stars.isEmpty()) {
      throw new ParserError("starred may not be used here", tgt);
    }

    ensureSingleExpr(tgt);
    singleTarget = tgt;
  }

  @Override
  public void trailingComma() {}

  public void apply(ITAstOper op) {
    dcheck(singleTarget == null);
    ensureSingleExpr(op);
    singleExpr = op.apply(dcheckNotNull(singleExpr));
  }
}
