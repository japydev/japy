package org.japy.parser.python.impl.builders.ast;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.loc.Location;
import org.japy.parser.python.ast.expr.AstSubscript;
import org.japy.parser.python.ast.expr.AstTuple;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.impl.ParserUtil;
import org.japy.parser.python.impl.javacc.Token;

public final class TAstSubOper extends ParserUtil implements ITAstOper {
  public @Nullable Token start;
  public @Nullable Token end;
  public boolean trailingComma;
  public final List<IAstExpr> items = new ArrayList<>();

  public void add(IAstExpr item) {
    items.add(item);
  }

  @Override
  public IAstExpr apply(IAstExpr e) {
    dcheckNotNull(start);
    dcheckNotNull(end);
    return new AstSubscript(e, buildIndex(), t(start), t(end));
  }

  private IAstExpr buildIndex() {
    if (items.size() == 1 && !trailingComma) {
      return items.get(0);
    }

    return new AstTuple(items.toArray(IAstExpr[]::new));
  }

  @Override
  public Location location() {
    return dcheckNotNull(start).location();
  }
}
