package org.japy.parser.python.impl;

import org.japy.parser.python.ParserVisitor;
import org.japy.parser.python.impl.javacc.ParseException;
import org.japy.parser.python.impl.javacc.PythonParser;

public class ParserWrapper<T> extends PythonParser<T> {
  public ParserWrapper(ParserVisitor<T> visitor, Scanner scanner) {
    super(visitor, scanner);
  }

  @Override
  public ParseException generateParseException() {
    if (scanner().atEof()) {
      return new ParseException("unexpected EOF");
    }

    return super.generateParseException();
  }
}
