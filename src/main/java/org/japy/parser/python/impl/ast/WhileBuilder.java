package org.japy.parser.python.impl.ast;

import static org.japy.infra.validation.Debug.dcheck;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.stmt.AstElse;
import org.japy.parser.python.ast.stmt.AstWhile;
import org.japy.parser.python.ast.stmt.IAstStmt;

final class WhileBuilder implements SuiteHandler, ElseHandler {
  private final IAstExpr condition;
  private final ITokenSpan location;
  private final SuiteBuilder body = new SuiteBuilder();
  private @Nullable ITokenSpan elseLocation;
  private final SuiteBuilder elseBody = new SuiteBuilder();

  WhileBuilder(IAstExpr condition, ITokenSpan location) {
    this.condition = condition;
    this.location = location;
  }

  @Override
  public void visitElse(ITokenSpan location) {
    dcheck(elseLocation == null);
    elseLocation = location;
  }

  @Override
  public void add(IAstStmt stmt) {
    if (elseLocation != null) {
      elseBody.add(stmt);
    } else {
      body.add(stmt);
    }
  }

  public IAstStmt build() {
    return new AstWhile(
        condition,
        location,
        body.build(),
        elseLocation != null ? new AstElse(elseBody.build(), elseLocation) : null);
  }
}
