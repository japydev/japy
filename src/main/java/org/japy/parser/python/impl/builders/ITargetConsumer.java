package org.japy.parser.python.impl.builders;

import org.japy.parser.python.impl.builders.ast.TAstTarget;

public interface ITargetConsumer {
  void target(TAstTarget tgt);

  void trailingComma();
}
