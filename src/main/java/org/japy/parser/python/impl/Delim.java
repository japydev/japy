package org.japy.parser.python.impl;

import org.japy.parser.python.impl.javacc.Token;

enum Delim {
  LPAREN("(", Token.LPAREN),
  RPAREN(")", Token.RPAREN),
  LBRACKET("[", Token.LBRACKET),
  RBRACKET("]", Token.RBRACKET),
  LBRACE("{", Token.LBRACE),
  RBRACE("}", Token.RBRACE),
  COMMA(",", Token.COMMA),
  COLON(":", Token.COLON),
  DOT(".", Token.DOT),
  SEMICOLON(";", Token.SEMICOLON),
  ARROW("->", Token.ARROW),
  ELLIPSIS("...", Token.ELLIPSIS),
  ;

  public final String text;
  public final int len;
  public final int tokenKind;

  Delim(String text, int tokenKind) {
    this.text = text;
    this.len = text.length();
    this.tokenKind = tokenKind;
  }
}
