package org.japy.parser.python.impl;

import java.util.HashMap;
import java.util.Map;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.parser.python.impl.javacc.Token;

enum Keyword {
  FALSE("False", Token.FALSE),
  NONE("None", Token.NONE),
  TRUE("True", Token.TRUE),
  AND("and", Token.AND),
  AS("as", Token.AS),
  ASSERT("assert", Token.ASSERT),
  ASYNC("async", Token.ASYNC),
  AWAIT("await", Token.AWAIT),
  BREAK("break", Token.BREAK),
  CLASS("class", Token.CLASS),
  CONTINUE("continue", Token.CONTINUE),
  DEF("def", Token.DEF),
  DEL("del", Token.DEL),
  ELIF("elif", Token.ELIF),
  ELSE("else", Token.ELSE),
  EXCEPT("except", Token.EXCEPT),
  FINALLY("finally", Token.FINALLY),
  FOR("for", Token.FOR),
  FROM("from", Token.FROM),
  GLOBAL("global", Token.GLOBAL),
  IF("if", Token.IF),
  IMPORT("import", Token.IMPORT),
  IN("in", Token.IN),
  IS("is", Token.IS),
  LAMBDA("lambda", Token.LAMBDA),
  NONLOCAL("nonlocal", Token.NONLOCAL),
  NOT("not", Token.NOT),
  OR("or", Token.OR),
  PASS("pass", Token.PASS),
  RAISE("raise", Token.RAISE),
  RETURN("return", Token.RETURN),
  TRY("try", Token.TRY),
  WHILE("while", Token.WHILE),
  WITH("with", Token.WITH),
  YIELD("yield", Token.YIELD),
  ;

  public final String text;
  public final int tokenKind;

  Keyword(String text, int tokenKind) {
    this.text = text;
    this.tokenKind = tokenKind;
  }

  public static @Nullable Keyword lookup(String text) {
    return map.get(text);
  }

  private static final Map<String, Keyword> map;

  static {
    map = new HashMap<>(values().length);
    for (Keyword kw : values()) {
      map.put(kw.text, kw);
    }
  }
}
