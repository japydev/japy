package org.japy.parser.python.impl.token;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.IHasRange;
import org.japy.infra.loc.Location;
import org.japy.infra.loc.Range;
import org.japy.parser.python.impl.javacc.PythonParserConstants;
import org.japy.parser.python.impl.javacc.Token;

public class TokenBase implements PythonParserConstants, IHasRange {
  public int kind() {
    throw InternalErrorException.notReached();
  }

  public Token next() {
    throw InternalErrorException.notReached();
  }

  public int beginLine() {
    throw InternalErrorException.notReached();
  }

  public int beginColumn() {
    throw InternalErrorException.notReached();
  }

  public int endLine() {
    throw InternalErrorException.notReached();
  }

  public int endColumn() {
    throw InternalErrorException.notReached();
  }

  public String image() {
    throw InternalErrorException.notReached();
  }

  @Override
  public Location start() {
    return new Location(beginLine(), beginColumn());
  }

  @Override
  public Location end() {
    return new Location(endLine(), endColumn());
  }

  @Override
  public Range range() {
    return new Range(start(), end());
  }
}
