package org.japy.parser.python.impl.builders;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.parser.python.ast.expr.AstDict;
import org.japy.parser.python.ast.expr.AstDictComp;
import org.japy.parser.python.ast.expr.AstSet;
import org.japy.parser.python.ast.expr.AstSetComp;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.node.AstCompFor;
import org.japy.parser.python.ast.node.AstKeyDatum;
import org.japy.parser.python.ast.node.IAstDictEntry;
import org.japy.parser.python.impl.ParserUtil;
import org.japy.parser.python.impl.javacc.Token;

public final class BraceExprBuilder extends ParserUtil {
  private static final IAstDictEntry[] EMPTY_DICT_ENTRY_ARRAY = new IAstDictEntry[0];

  public final List<IAstExpr> setEntries = new ArrayList<>();
  public final List<IAstDictEntry> dictEntries = new ArrayList<>();
  private @Nullable AstCompFor compFor;

  public void addSet(IAstExpr expr) {
    setEntries.add(expr);
  }

  public void addDict(IAstDictEntry d) {
    dictEntries.add(d);
  }

  public void compFor(AstCompFor compFor) {
    if (this.compFor != null) {
      throw new InternalErrorException("multiple comp fors", compFor);
    }
    this.compFor = compFor;
  }

  public IAstExpr build(Token start, Token end) {
    if (compFor != null) {
      if (!dictEntries.isEmpty()) {
        dcheck(dictEntries.size() == 1);
        IAstDictEntry d = dictEntries.get(0);
        dcheck(d instanceof AstKeyDatum);
        return new AstDictComp(
            t(start), t(end), ((AstKeyDatum) d).key, ((AstKeyDatum) d).value, compFor);
      } else {
        dcheck(setEntries.size() == 1);
        return new AstSetComp(t(start), t(end), setEntries.get(0), compFor);
      }
    } else if (!dictEntries.isEmpty()) {
      return new AstDict(t(start), t(end), dictEntries.toArray(IAstDictEntry[]::new));
    } else if (!setEntries.isEmpty()) {
      return new AstSet(t(start), t(end), setEntries.toArray(IAstExpr[]::new));
    } else {
      return new AstDict(t(start), t(end), EMPTY_DICT_ENTRY_ARRAY);
    }
  }
}
