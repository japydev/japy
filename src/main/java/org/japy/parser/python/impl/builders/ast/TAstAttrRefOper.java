package org.japy.parser.python.impl.builders.ast;

import org.japy.infra.loc.Location;
import org.japy.parser.python.ast.expr.AstAttrRef;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.impl.javacc.Token;

public final class TAstAttrRefOper implements ITAstOper {
  public final Token dot;
  public final AstIdentifier identifier;

  public TAstAttrRefOper(Token dot, AstIdentifier identifier) {
    this.dot = dot;
    this.identifier = identifier;
  }

  @Override
  public IAstExpr apply(IAstExpr e) {
    return new AstAttrRef(e, identifier);
  }

  @Override
  public Location location() {
    return dot.location();
  }
}
