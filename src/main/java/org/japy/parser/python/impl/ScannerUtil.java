package org.japy.parser.python.impl;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.Arrays;

import com.google.errorprone.annotations.Var;

public class ScannerUtil {
  // TODO paragraph end?
  public static boolean isEol(byte c) {
    return c == '\r' || c == '\n';
  }

  public static boolean isEol(char c) {
    return c == '\r' || c == '\n';
  }

  public static boolean isLetter(byte c) {
    // TODO
    return c < 0 || c == '_' || ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z');
  }

  public static boolean isDigit(byte c) {
    return '0' <= c && c <= '9';
  }

  public static boolean isOctal(byte c) {
    return '0' <= c && c <= '7';
  }

  public static boolean isBin(byte c) {
    return c == '0' || c == '1';
  }

  public static boolean isHex(byte c) {
    return isDigit(c) || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
  }

  public static int octalDigit(byte c) {
    return c - '0';
  }

  public static int hexDigit(byte c) {
    if ('0' <= c && c <= '9') {
      return c - '0';
    } else if ('a' <= c && c <= 'f') {
      return c - 'a' + 10;
    } else {
      return c - 'A' + 10;
    }
  }

  public static boolean isQuote(byte c) {
    return c == '"' || c == '\'';
  }

  public static boolean isR(byte c) {
    return c == 'r' || c == 'R';
  }

  public static boolean isU(byte c) {
    return c == 'u' || c == 'U';
  }

  public static boolean isB(byte c) {
    return c == 'b' || c == 'B';
  }

  public static boolean isF(byte c) {
    return c == 'f' || c == 'F';
  }

  public static boolean isX(byte c) {
    return c == 'x' || c == 'X';
  }

  public static boolean isO(byte c) {
    return c == 'o' || c == 'O';
  }

  public static boolean isJ(byte c) {
    return c == 'j' || c == 'J';
  }

  public static class ByteWriter {
    private static final int DEFAULT_CAPACITY = 64;

    protected byte[] buf;
    protected int size;

    public ByteWriter() {
      this(0);
    }

    public ByteWriter(int capacity) {
      buf = new byte[capacity > 0 ? capacity : DEFAULT_CAPACITY];
    }

    public void write(int c) {
      if (++size > buf.length) {
        buf = Arrays.copyOf(buf, Math.max((int) (buf.length * 1.2), size));
      }
      buf[size - 1] = (byte) c;
    }

    public void write(byte[] bytes) {
      write(bytes, 0, bytes.length);
    }

    public void write(byte[] bytes, int start, int end) {
      int chunkLen = end - start;
      if (chunkLen == 0) {
        return;
      }
      if (size + chunkLen > buf.length) {
        buf = Arrays.copyOf(buf, Math.max((int) (buf.length * 1.2), size + chunkLen));
      }
      System.arraycopy(bytes, start, buf, size, chunkLen);
      size += chunkLen;
    }

    public byte[] toByteArray() {
      return Arrays.copyOf(buf, size);
    }

    public void reset() {
      size = 0;
    }

    public int size() {
      return size;
    }

    public void setSize(int size) {
      dcheck(size <= this.size);
      this.size = size;
    }
  }

  protected static class CharWriter {
    private char[] buf = new char[64];
    private int size;

    public void write(int c) {
      if (++size > buf.length) {
        buf = Arrays.copyOf(buf, Math.max((int) (buf.length * 1.2), size));
      }
      buf[size - 1] = (char) c;
    }

    public void reset() {
      size = 0;
    }

    public int size() {
      return size;
    }

    public void setSize(int size) {
      dcheck(size <= this.size);
      this.size = size;
    }

    public String getString() {
      return new String(buf, 0, size);
    }
  }

  private static final String INDENT = " ";
  private static final byte BYTE_INDENT = ' ';

  public static void indent(String text, StringBuilder sb) {
    int len = text.length();
    @Var int pos = 0;
    @Var int lineStart = 0;
    while (pos != len) {
      char c = text.charAt(pos);
      if (!isEol(c)) {
        ++pos;
        continue;
      }

      if (lineStart != pos) {
        sb.append(INDENT);
        sb.append(text, lineStart, pos);
      }

      sb.append(c);
      ++pos;
      if (c == '\r' && pos + 1 != len && text.charAt(pos + 1) == '\n') {
        sb.append(c);
        ++pos;
      }

      lineStart = pos;
    }

    if (lineStart != len) {
      sb.append(INDENT);
      sb.append(text, lineStart, len);
    }
  }

  public static void indent(byte[] text, ByteWriter output) {
    int len = text.length;
    @Var int pos = 0;
    @Var int lineStart = 0;
    while (pos != len) {
      byte c = text[pos];
      if (!isEol(c)) {
        ++pos;
        continue;
      }

      if (lineStart != pos) {
        output.write(BYTE_INDENT);
        output.write(text, lineStart, pos);
      }

      output.write(c);
      ++pos;
      if (c == '\r' && pos + 1 != len && text[pos + 1] == '\n') {
        output.write(c);
        ++pos;
      }

      lineStart = pos;
    }

    if (lineStart != len) {
      output.write(BYTE_INDENT);
      output.write(text, lineStart, len);
    }
  }
}
