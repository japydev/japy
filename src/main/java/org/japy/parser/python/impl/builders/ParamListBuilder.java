package org.japy.parser.python.impl.builders;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.parser.ParserError;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.node.AstParam;
import org.japy.parser.python.impl.ParserUtil;
import org.japy.parser.python.impl.javacc.Token;

public final class ParamListBuilder extends ParserUtil {
  private static final AstParam[] NO_PARAMS = AstParam.EMPTY_ARRAY;

  private final List<AstParam> params = new ArrayList<>();
  private boolean seenDflt;
  private boolean seenKwOnly;
  private boolean seenVarPos;
  private boolean seenVarKw;
  private boolean seenPosOnlySep;

  public void param(AstIdentifier id, @Nullable IAstExpr dflt) {
    param(id, dflt, null);
  }

  public void param(AstIdentifier id, @Nullable IAstExpr dflt, @Nullable IAstExpr annotation) {
    for (AstParam p : params) {
      if (p.name.name.equals(id.name)) {
        throw new ParserError(
            String.format("duplicate argument '%s' in function definition", id.name), id);
      }
    }

    if (dflt != null) {
      seenDflt = true;
      if (seenVarPos || seenKwOnly) {
        params.add(new AstParam(ParamKind.KW_ONLY, id.token(), id, dflt, annotation));
      } else if (seenVarKw) {
        throw new ParserError("keyword parameter follows var-keyword parameter", id);
      } else {
        params.add(new AstParam(ParamKind.POS_OR_KW, id.token(), id, dflt, annotation));
      }
    } else if (seenDflt && !seenKwOnly && !seenVarPos) {
      throw new ParserError("positional parameter follows a keyword parameter", id);
    } else if (seenVarPos) {
      params.add(new AstParam(ParamKind.KW_ONLY, id.token(), id, null, annotation));
    } else if (seenVarKw) {
      throw new ParserError("positional parameter follows var-keyword parameter", id);
    } else if (seenKwOnly) {
      params.add(new AstParam(ParamKind.KW_ONLY, id.token(), id, null, annotation));
    } else {
      params.add(new AstParam(ParamKind.POS_OR_KW, id.token(), id, null, annotation));
    }
  }

  public void posOnlySep(Token div) {
    dcheck(div.kind() == Token.DIV);

    if (params.isEmpty()) {
      throw new ParserError(
          "positional-only parameter separator with any preceding parameters", div);
    } else if (seenVarKw) {
      throw new ParserError(
          "positional-only parameter separator follows var-keyword parameter", div);
    } else if (seenKwOnly) {
      throw new ParserError(
          "positional-only parameter separator follows keyword-only separator", div);
    } else if (seenVarPos) {
      throw new ParserError(
          "positional-only parameter separator follows var-positional parameter", div);
    } else if (seenPosOnlySep) {
      throw new ParserError("multiple positional-only parameter separators", div);
    }

    seenPosOnlySep = true;

    for (int i = 0; i < params.size(); ++i) {
      AstParam p = params.get(i);
      dcheck(p.paramKind == ParamKind.POS_OR_KW);
      params.set(i, new AstParam(ParamKind.POS_ONLY, p.name.start(), p.name, p.dflt, p.annotation));
    }
  }

  public void varPos(Token star, AstIdentifier id) {
    varPos(star, id, null);
  }

  public void varPos(Token star, AstIdentifier id, @Nullable IAstExpr annotation) {
    if (seenVarPos) {
      throw new ParserError("multiple var-positional parameters", star);
    } else if (seenVarKw) {
      throw new ParserError("var-positional parameter follows a var-keyword parameter", star);
    } else if (seenKwOnly) {
      throw new ParserError("var-positional parameter follows keyword-only separator", star);
    }
    dcheck(star.kind() == Token.MUL);
    seenVarPos = true;
    params.add(new AstParam(ParamKind.VAR_POS, t(star), id, null, annotation));
  }

  public void varKw(Token pow, AstIdentifier id) {
    varKw(pow, id, null);
  }

  public void varKw(Token pow, AstIdentifier id, @Nullable IAstExpr annotation) {
    if (seenVarKw) {
      throw new ParserError("multiple var-keyword parameters", pow);
    }
    dcheck(pow.kind() == Token.POW);
    seenVarKw = true;
    params.add(new AstParam(ParamKind.VAR_KW, t(pow), id, null, annotation));
  }

  public void kwOnlySep(Token star) {
    if (seenKwOnly) {
      throw new ParserError("multiple keyword-only separators", star);
    }
    seenKwOnly = true;
  }

  public AstParam[] build() {
    return params.toArray(NO_PARAMS);
  }
}
