package org.japy.parser.python.impl.ast;

import java.util.ArrayList;
import java.util.List;

import org.japy.parser.python.ast.node.AstSuite;
import org.japy.parser.python.ast.stmt.IAstStmt;

public class SuiteBuilder {
  private final List<IAstStmt> stmts = new ArrayList<>();

  public void add(IAstStmt stmt) {
    stmts.add(stmt);
  }

  public boolean isEmpty() {
    return stmts.isEmpty();
  }

  public AstSuite build() {
    return new AstSuite(stmts.toArray(IAstStmt[]::new));
  }
}
