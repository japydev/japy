package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstAwait extends AstExprNode {
  public final IAstExpr expr;

  public AstAwait(Token await, IAstExpr expr) {
    super(FLAG_CAN_AWAIT | expr.suspendFlags(), await, expr.end());
    this.expr = expr;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.AWAIT;
  }

  @Override
  public String toString() {
    return "AWAIT(" + expr + ")";
  }
}
