package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.ITokenSpan;

public class AstBreak extends AstStmtNode {
  public AstBreak(ITokenSpan location) {
    super(0, location);
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.BREAK;
  }

  @Override
  public void validate(Validator v) {}
}
