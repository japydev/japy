package org.japy.parser.python.ast.stmt;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstDecorator;
import org.japy.parser.python.ast.node.AstNode;
import org.japy.parser.python.ast.node.AstParam;

public class AstFuncHeader extends AstNode {
  public final boolean async;
  public final AstDecorator[] decorators;
  public final AstIdentifier name;
  public final AstParam[] params;
  public final @Nullable IAstExpr retType;

  public AstFuncHeader(
      boolean async,
      AstDecorator[] decorators,
      AstIdentifier name,
      AstParam[] params,
      @Nullable IAstExpr retType,
      ITokenSpan location) {
    super(suspendFlags(decorators) | suspendFlags(retType), location);
    this.async = async;
    this.decorators = decorators;
    this.name = name;
    this.params = params;
    this.retType = retType;
  }

  @Override
  public void validate(Validator v) {
    v.validate(decorators);
    v.validate(name);
    v.validate(params);
    v.validateIfNotNull(retType);
  }
}
