package org.japy.parser.python.ast.visit;

import org.japy.infra.exc.InternalErrorException;
import org.japy.parser.python.ast.expr.*;

public final class AstExprVisit {
  private AstExprVisit() {}

  public static void visit(IAstExpr expr, IAstExprVisitor visitor) {
    switch (expr.kind()) {
      case STRING:
        visitor.visitString((AstString) expr);
        break;
      case YIELD:
        visitor.visitYield((AstYieldExpr) expr);
        break;
      case YIELD_FROM:
        visitor.visitYieldFrom((AstYieldFrom) expr);
        break;
      case IDENTIFIER:
        visitor.visitIdentifier((AstIdentifier) expr);
        break;
      case LAMBDA:
        visitor.visitLambda((AstLambda) expr);
        break;
      case WALRUS:
        visitor.visitWalrus((AstWalrus) expr);
        break;
      case GENERATOR:
        visitor.visitGenerator((AstGenerator) expr);
        break;
      case STAR:
        visitor.visitStar((AstStar) expr);
        break;
      case DOUBLE_STAR:
        visitor.visitDoubleStar((AstDoubleStar) expr);
        break;
      case LIST_COMP:
        visitor.visitListComp((AstListComp) expr);
        break;
      case LIST:
        visitor.visitList((AstList) expr);
        break;
      case TUPLE:
        visitor.visitTuple((AstTuple) expr);
        break;
      case PAREN:
        visitor.visitParen((AstParen) expr);
        break;
      case COND:
        visitor.visitConditional((AstConditional) expr);
        break;
      case BIN_OP:
        visitor.visitBinOp((AstBinOp) expr);
        break;
      case COMPARISON:
        visitor.visitComparison((AstComparison) expr);
        break;
      case NOT:
        visitor.visitNot((AstNot) expr);
        break;
      case LOGICAL_OP:
        visitor.visitLogicalOp((AstLogicalOp) expr);
        break;
      case UNARY_OP:
        visitor.visitUnaryOp((AstUnaryOp) expr);
        break;
      case SUBSCRIPT:
        visitor.visitSubscript((AstSubscript) expr);
        break;
      case SLICE:
        visitor.visitSlice((AstSlice) expr);
        break;
      case ATTR_REF:
        visitor.visitAttrRef((AstAttrRef) expr);
        break;
      case CALL:
        visitor.visitCall((AstCall) expr);
        break;
      case DICT:
        visitor.visitDict((AstDict) expr);
        break;
      case DICT_COMP:
        visitor.visitDictComp((AstDictComp) expr);
        break;
      case SET:
        visitor.visitSet((AstSet) expr);
        break;
      case SET_COMP:
        visitor.visitSetComp((AstSetComp) expr);
        break;
      case AWAIT:
        visitor.visitAwait((AstAwait) expr);
        break;
      case CONST:
        visitor.visitConst((AstConst) expr);
        break;
    }
  }

  public static <T> T eval(IAstExpr expr, IAstExprEval<T> eval) {
    switch (expr.kind()) {
      case STRING:
        return eval.visit((AstString) expr);
      case YIELD:
        return eval.visit((AstYieldExpr) expr);
      case YIELD_FROM:
        return eval.visit((AstYieldFrom) expr);
      case IDENTIFIER:
        return eval.visit((AstIdentifier) expr);
      case LAMBDA:
        return eval.visit((AstLambda) expr);
      case WALRUS:
        return eval.visit((AstWalrus) expr);
      case GENERATOR:
        return eval.visit((AstGenerator) expr);
      case STAR:
        return eval.visit((AstStar) expr);
      case DOUBLE_STAR:
        return eval.visit((AstDoubleStar) expr);
      case LIST_COMP:
        return eval.visit((AstListComp) expr);
      case LIST:
        return eval.visit((AstList) expr);
      case TUPLE:
        return eval.visit((AstTuple) expr);
      case PAREN:
        return eval.visit((AstParen) expr);
      case COND:
        return eval.visit((AstConditional) expr);
      case BIN_OP:
        return eval.visit((AstBinOp) expr);
      case COMPARISON:
        return eval.visit((AstComparison) expr);
      case NOT:
        return eval.visit((AstNot) expr);
      case LOGICAL_OP:
        return eval.visit((AstLogicalOp) expr);
      case UNARY_OP:
        return eval.visit((AstUnaryOp) expr);
      case SUBSCRIPT:
        return eval.visit((AstSubscript) expr);
      case SLICE:
        return eval.visit((AstSlice) expr);
      case ATTR_REF:
        return eval.visit((AstAttrRef) expr);
      case CALL:
        return eval.visit((AstCall) expr);
      case DICT:
        return eval.visit((AstDict) expr);
      case SET:
        return eval.visit((AstSet) expr);
      case SET_COMP:
        return eval.visit((AstSetComp) expr);
      case AWAIT:
        return eval.visit((AstAwait) expr);
      case DICT_COMP:
        return eval.visit((AstDictComp) expr);
      case CONST:
        return eval.visit((AstConst) expr);
    }

    throw new InternalErrorException(expr.kind().toString(), expr);
  }
}
