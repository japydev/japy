package org.japy.parser.python.ast.visit;

import org.japy.infra.exc.InternalErrorException;
import org.japy.parser.python.ast.expr.*;

public class BaseAstExprEval<T> implements IAstExprEval<T> {
  protected BaseAstExprEval() {}

  public T visitAny(IAstExpr expr) {
    throw new InternalErrorException(expr.kind().toString(), expr);
  }

  @Override
  public T visit(AstConst expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstString expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstIdentifier expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstWalrus expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstStar expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstDoubleStar expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstList expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstSet expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstDict expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstTuple expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstListComp expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstSetComp expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstDictComp expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstParen expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstCall expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstAttrRef expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstSubscript expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstNot expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstComparison expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstBinOp expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstUnaryOp expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstLogicalOp expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstConditional expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstSlice expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstYieldExpr expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstYieldFrom expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstLambda expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstGenerator expr) {
    return visitAny(expr);
  }

  @Override
  public T visit(AstAwait expr) {
    return visitAny(expr);
  }
}
