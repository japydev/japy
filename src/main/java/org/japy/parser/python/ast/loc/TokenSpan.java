package org.japy.parser.python.ast.loc;

import com.google.errorprone.annotations.Immutable;

@Immutable
public final class TokenSpan implements ITokenSpan {
  public final Token start;
  public final Token end;

  public TokenSpan(Token start, Token end) {
    this.start = start;
    this.end = end;
  }

  public TokenSpan(ITokenSpan start, ITokenSpan end) {
    this.start = start.start();
    this.end = end.end();
  }

  @Override
  public Token start() {
    return start;
  }

  @Override
  public Token end() {
    return end;
  }
}
