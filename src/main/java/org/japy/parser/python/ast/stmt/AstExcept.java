package org.japy.parser.python.ast.stmt;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstNode;
import org.japy.parser.python.ast.node.AstSuite;

public class AstExcept extends AstNode {
  public final @Nullable IAstExpr ex;
  public final @Nullable IAstExpr as;
  public final AstSuite body;

  public AstExcept(
      @Nullable IAstExpr ex, @Nullable IAstExpr as, ITokenSpan location, AstSuite body) {
    super(suspendFlags(ex, as) | body.suspendFlags(), location, body.end());
    this.ex = ex;
    this.as = as;
    this.body = body;
  }

  @Override
  public void validate(Validator v) {
    v.validateIfNotNull(ex);
    v.validateIfNotNull(as);
  }
}
