package org.japy.parser.python.ast.expr;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstSlice extends AstExprNode {
  public final @Nullable IAstExpr lowerBound;
  public final @Nullable IAstExpr upperBound;
  public final @Nullable IAstExpr step;

  public AstSlice(
      Token colon,
      @Nullable Token colon2,
      @Nullable IAstExpr lowerBound,
      @Nullable IAstExpr upperBound,
      @Nullable IAstExpr step) {
    super(
        suspendFlags(lowerBound, upperBound, step)
            | ((lowerBound == null || lowerBound.isConst())
                    && (upperBound == null || upperBound.isConst())
                    && (step == null || step.isConst())
                ? FLAG_CONST
                : 0),
        lowerBound != null ? lowerBound : colon,
        step != null
            ? step
            : (colon2 != null ? colon2 : (upperBound != null ? upperBound : colon)));
    this.lowerBound = lowerBound;
    this.upperBound = upperBound;
    this.step = step;
  }

  @Override
  public void validate(Validator v) {
    v.validateIfNotNull(lowerBound, upperBound, step);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.SLICE;
  }
}
