package org.japy.parser.python.ast.visit;

import org.japy.parser.python.ast.expr.AstAttrRef;
import org.japy.parser.python.ast.expr.AstAwait;
import org.japy.parser.python.ast.expr.AstBinOp;
import org.japy.parser.python.ast.expr.AstCall;
import org.japy.parser.python.ast.expr.AstComparison;
import org.japy.parser.python.ast.expr.AstConditional;
import org.japy.parser.python.ast.expr.AstConst;
import org.japy.parser.python.ast.expr.AstDict;
import org.japy.parser.python.ast.expr.AstDictComp;
import org.japy.parser.python.ast.expr.AstDoubleStar;
import org.japy.parser.python.ast.expr.AstGenerator;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.AstLambda;
import org.japy.parser.python.ast.expr.AstList;
import org.japy.parser.python.ast.expr.AstListComp;
import org.japy.parser.python.ast.expr.AstLogicalOp;
import org.japy.parser.python.ast.expr.AstNot;
import org.japy.parser.python.ast.expr.AstParen;
import org.japy.parser.python.ast.expr.AstSet;
import org.japy.parser.python.ast.expr.AstSetComp;
import org.japy.parser.python.ast.expr.AstSlice;
import org.japy.parser.python.ast.expr.AstStar;
import org.japy.parser.python.ast.expr.AstString;
import org.japy.parser.python.ast.expr.AstSubscript;
import org.japy.parser.python.ast.expr.AstTuple;
import org.japy.parser.python.ast.expr.AstUnaryOp;
import org.japy.parser.python.ast.expr.AstWalrus;
import org.japy.parser.python.ast.expr.AstYieldExpr;
import org.japy.parser.python.ast.expr.AstYieldFrom;

public interface IAstExprEval<T> {
  T visit(AstConst expr);

  T visit(AstString expr);

  T visit(AstIdentifier expr);

  T visit(AstWalrus expr);

  T visit(AstStar expr);

  T visit(AstDoubleStar expr);

  T visit(AstList expr);

  T visit(AstSet expr);

  T visit(AstDict expr);

  T visit(AstTuple expr);

  T visit(AstListComp expr);

  T visit(AstSetComp expr);

  T visit(AstDictComp expr);

  T visit(AstParen expr);

  T visit(AstCall expr);

  T visit(AstAttrRef expr);

  T visit(AstSubscript expr);

  T visit(AstNot expr);

  T visit(AstComparison expr);

  T visit(AstBinOp expr);

  T visit(AstUnaryOp expr);

  T visit(AstLogicalOp expr);

  T visit(AstConditional expr);

  T visit(AstSlice expr);

  T visit(AstYieldExpr expr);

  T visit(AstYieldFrom expr);

  T visit(AstLambda expr);

  T visit(AstGenerator expr);

  T visit(AstAwait expr);
}
