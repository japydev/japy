package org.japy.parser.python.ast.stmt;

import static org.japy.infra.coll.CollUtil.first;
import static org.japy.infra.coll.CollUtil.last;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;

public class AstIf extends AstStmtNode {
  public final AstIfBranch[] branches;
  public final @Nullable AstElse else_;

  public AstIf(AstIfBranch[] branches, @Nullable AstElse else_) {
    super(
        suspendFlags(branches) | suspendFlags(else_),
        first(branches).start(),
        else_ != null ? else_.end() : last(branches).end());
    this.branches = branches;
    this.else_ = else_;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.IF;
  }

  @Override
  public void validate(Validator v) {
    v.validate(branches);
    v.validateIfNotNull(else_);
  }
}
