package org.japy.parser.python.ast.node;

import java.util.Arrays;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.loc.TokenSpan;

public abstract class AstNode implements IAstNode {
  public final ITokenSpan location;
  public final int flags;

  protected AstNode(int flags, ITokenSpan location) {
    this.location = location;
    this.flags = flags;
  }

  protected AstNode(int flags, ITokenSpan start, ITokenSpan end) {
    this(flags, new TokenSpan(start, end));
  }

  @Override
  public int flags() {
    return flags;
  }

  @Override
  public final Token start() {
    return location.start();
  }

  @Override
  public final Token end() {
    return location.end();
  }

  public static boolean canSuspend(IAstNode[] nodes) {
    return Arrays.stream(nodes).anyMatch(IAstNode::canSuspend);
  }

  public static boolean canYield(IAstNode[] nodes) {
    return Arrays.stream(nodes).anyMatch(IAstNode::canYield);
  }

  public static boolean canAwait(IAstNode[] nodes) {
    return Arrays.stream(nodes).anyMatch(IAstNode::canAwait);
  }

  public static boolean canAwait(IAstNode node1, IAstNode node2) {
    return node1.canAwait() || node2.canAwait();
  }

  public static int suspendFlags(@Nullable IAstNode node1) {
    return node1 != null ? node1.suspendFlags() : 0;
  }

  public static int suspendFlags(@Nullable IAstNode node1, @Nullable IAstNode node2) {
    return suspendFlags(node1) | suspendFlags(node2);
  }

  public static int suspendFlags(
      @Nullable IAstNode node1, @Nullable IAstNode node2, @Nullable IAstNode node3) {
    return suspendFlags(node1) | suspendFlags(node2) | suspendFlags(node3);
  }

  public static int suspendFlags(IAstNode @Nullable [] nodes) {
    @Var int flags = 0;
    if (nodes != null) {
      for (IAstNode n : nodes) {
        flags |= n.suspendFlags();
      }
    }
    return flags;
  }
}
