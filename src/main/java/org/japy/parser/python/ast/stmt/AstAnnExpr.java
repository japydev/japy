package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;

public class AstAnnExpr extends AstStmtNode {
  public final IAstExpr expr;
  public final IAstExpr annotation;

  public AstAnnExpr(IAstExpr expr, IAstExpr annotation) {
    super(suspendFlags(expr, annotation), expr.start(), annotation.end());
    this.expr = expr;
    this.annotation = annotation;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.ANNEXPR;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
    v.validate(annotation);
  }
}
