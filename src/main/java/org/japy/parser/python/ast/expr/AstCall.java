package org.japy.parser.python.ast.expr;

import java.util.Arrays;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.node.IAstArg;

public final class AstCall extends AstExprNode {
  public final IAstExpr func;
  public final IAstArg[] args;

  public AstCall(IAstExpr func, IAstArg[] args, Token rparen) {
    super(func.suspendFlags() | suspendFlags(args), func, rparen);
    this.func = func;
    this.args = args;
  }

  @Override
  public void validate(Validator v) {
    v.validate(func);
    v.validate(args);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.CALL;
  }

  @Override
  public String toString() {
    return "CALL(" + func + ", " + Arrays.toString(args) + ')';
  }
}
