package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.node.AstCompFor;

public final class AstDictComp extends AstExprNode {
  public final IAstExpr key;
  public final IAstExpr value;
  public final AstCompFor compFor;

  public AstDictComp(Token lbrace, Token rbrace, IAstExpr key, IAstExpr value, AstCompFor compFor) {
    super(suspendFlags(key, value, compFor), lbrace, rbrace);
    this.key = key;
    this.value = value;
    this.compFor = compFor;
  }

  @Override
  public void validate(Validator v) {
    v.validate(key);
    v.validate(value);
    v.validate(compFor);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.DICT_COMP;
  }
}
