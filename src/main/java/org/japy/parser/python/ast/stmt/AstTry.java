package org.japy.parser.python.ast.stmt;

import static org.japy.infra.coll.CollUtil.last;
import static org.japy.infra.validation.Debug.dcheck;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Debug;
import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstSuite;

public class AstTry extends AstStmtNode {
  public final AstSuite body;
  public final AstExcept[] except;
  public final @Nullable AstElse else_;
  public final @Nullable ITokenSpan finallyLocation;
  public final @Nullable AstSuite finallyBody;

  public AstTry(
      ITokenSpan start,
      AstSuite body,
      AstExcept[] except,
      @Nullable AstElse else_,
      @Nullable ITokenSpan finallyLocation,
      @Nullable AstSuite finallyBody) {
    super(
        body.suspendFlags()
            | suspendFlags(except)
            | suspendFlags(else_)
            | suspendFlags(finallyBody),
        start,
        finallyBody != null
            ? finallyBody.end()
            : (except.length != 0 ? last(except).end() : body.end()));
    this.body = body;
    this.except = except;
    this.else_ = else_;
    this.finallyLocation = finallyLocation;
    this.finallyBody = finallyBody;

    // TODO raise syntax error here
    if (Debug.ENABLED) {
      for (int i = 0; i < except.length - 1; ++i) {
        dcheck(except[i].ex != null);
      }
    }
  }

  public boolean hasExcept() {
    return except.length != 0;
  }

  public boolean hasElse() {
    return else_ != null;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.TRY;
  }

  @Override
  public void validate(Validator v) {
    v.validate(body);
    v.validate(except);
    v.validateIfNotNull(else_);
    v.validateIfNotNull(finallyBody);
  }
}
