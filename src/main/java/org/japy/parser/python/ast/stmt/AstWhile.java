package org.japy.parser.python.ast.stmt;

import static org.japy.infra.util.ObjUtil.or;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstSuite;

public class AstWhile extends AstStmtNode {
  public final IAstExpr condition;
  public final AstSuite body;
  public final @Nullable AstElse else_;

  public AstWhile(IAstExpr condition, ITokenSpan location, AstSuite body, @Nullable AstElse else_) {
    super(suspendFlags(condition, else_) | body.suspendFlags(), location, or(else_, body::end));
    this.condition = condition;
    this.body = body;
    this.else_ = else_;
  }

  @Override
  public void validate(Validator v) {
    v.validate(condition);
    v.validate(body);
    v.validateIfNotNull(else_);
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.WHILE;
  }
}
