package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.kernel.types.IPyObj;
import org.japy.parser.python.ast.loc.Token;

public final class AstConst extends AstExprNode {
  public final IPyObj value;

  public AstConst(IPyObj value, Token token) {
    this(value, token, token);
  }

  public AstConst(IPyObj value, Token start, Token end) {
    super(FLAG_CONST, start, end);
    this.value = value;
  }

  @Override
  public void validate(Validator v) {}

  @Override
  public AstExprKind kind() {
    return AstExprKind.CONST;
  }

  @Override
  public String toString() {
    return "CONST(" + value + ')';
  }
}
