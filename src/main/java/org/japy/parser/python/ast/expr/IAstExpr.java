package org.japy.parser.python.ast.expr;

import org.japy.parser.python.ast.node.IAstNode;

public interface IAstExpr extends IAstNode {
  IAstExpr[] EMPTY_ARRAY = new IAstExpr[0];

  int FLAG_CONST = NEXT_FLAG;
  int FLAG_COMPTIME = NEXT_FLAG << 1;

  AstExprKind kind();

  default boolean isConst() {
    return (flags() & FLAG_CONST) != 0;
  }

  default boolean isCompTimeValue() {
    return (flags() & FLAG_COMPTIME) != 0;
  }
}
