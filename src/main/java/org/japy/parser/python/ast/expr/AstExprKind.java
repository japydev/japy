package org.japy.parser.python.ast.expr;

public enum AstExprKind {
  CONST,
  STRING,
  SLICE,

  IDENTIFIER,
  WALRUS,

  LAMBDA,

  STAR,
  DOUBLE_STAR,

  LIST,
  TUPLE,
  DICT,
  SET,

  LIST_COMP,
  SET_COMP,
  DICT_COMP,

  PAREN,
  COND,
  BIN_OP,
  COMPARISON,
  NOT,
  LOGICAL_OP,
  UNARY_OP,

  ATTR_REF,
  SUBSCRIPT,
  CALL,

  AWAIT,
  YIELD,
  YIELD_FROM,
  GENERATOR,
}
