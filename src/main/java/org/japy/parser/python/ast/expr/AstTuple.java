package org.japy.parser.python.ast.expr;

import static org.japy.infra.coll.CollUtil.first;
import static org.japy.infra.coll.CollUtil.last;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstTuple extends AstExprNode {
  public final IAstExpr[] entries;

  public AstTuple(IAstExpr... entries) {
    super(suspendFlags(entries) | constFlags(entries), first(entries), last(entries));
    this.entries = entries;
  }

  public AstTuple(Token start, Token end, IAstExpr... entries) {
    super(suspendFlags(entries) | constFlags(entries), start, end);
    this.entries = entries;
  }

  public AstTuple(Token start, Token end) {
    this(start, end, IAstExpr.EMPTY_ARRAY);
  }

  @Override
  public void validate(Validator v) {
    v.validate(entries);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.TUPLE;
  }
}
