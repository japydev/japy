package org.japy.parser.python.ast.node;

public interface IAstDictEntry extends IAstNode {
  enum Kind {
    SINGLE,
    STARRED
  }

  Kind dictEntryKind();

  boolean isCompTimeValue();
}
