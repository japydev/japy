package org.japy.parser.python.ast.node;

import org.checkerframework.checker.nullness.qual.Nullable;

public interface IAstCompIter extends IAstNode {
  enum CompIterKind {
    FOR,
    IF
  }

  CompIterKind compIterKind();

  @Nullable
  IAstCompIter nextCompIter();
}
