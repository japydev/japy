package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstSuite;
import org.japy.parser.python.ast.node.AstWithItem;

public class AstWith extends AstStmtNode {
  public final boolean async;
  public final AstWithItem[] items;
  public final AstSuite body;

  public AstWith(boolean async, AstWithItem[] items, AstSuite body, ITokenSpan start) {
    super(
        (async ? FLAG_CAN_AWAIT : 0) | suspendFlags(items) | body.suspendFlags(),
        start,
        body.end());
    this.async = async;
    this.items = items;
    this.body = body;
  }

  @Override
  public void validate(Validator v) {
    v.validate(items);
    v.validate(body);
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.WITH;
  }
}
