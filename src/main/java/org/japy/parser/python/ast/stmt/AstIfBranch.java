package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstNode;
import org.japy.parser.python.ast.node.AstSuite;

public class AstIfBranch extends AstNode {
  public final IAstExpr condition;
  public final AstSuite body;

  public AstIfBranch(IAstExpr condition, AstSuite body, ITokenSpan start) {
    super(condition.suspendFlags() | body.suspendFlags(), start.start(), body.end());
    this.condition = condition;
    this.body = body;
  }

  @Override
  public void validate(Validator v) {
    v.validate(condition);
    v.validate(body);
  }
}
