package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.node.AstTokenNode;

public final class AstIdentifier extends AstTokenNode implements IAstExpr {
  public static final AstIdentifier[] EMPTY_ARRAY = new AstIdentifier[0];

  public final String name;

  public AstIdentifier(Token token, String name) {
    super(token);
    this.name = name;
  }

  @Override
  public void validate(Validator v) {}

  @Override
  public AstExprKind kind() {
    return AstExprKind.IDENTIFIER;
  }

  @Override
  public String toString() {
    return name;
  }
}
