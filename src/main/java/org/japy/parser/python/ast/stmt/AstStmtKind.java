package org.japy.parser.python.ast.stmt;

public enum AstStmtKind {
  ANNASSIGN,
  ANNEXPR,
  ASSERT,
  ASSIGN,
  AUGASSIGN,
  BREAK,
  CLASS,
  CONTINUE,
  DEL,
  EXPR,
  FOR,
  FUNC,
  DECL,
  IF,
  IMPORT,
  IMPORTFROM,
  PASS,
  RAISE,
  TRY,
  WHILE,
  WITH,
  RETURN,
}
