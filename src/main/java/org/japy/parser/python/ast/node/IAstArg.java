package org.japy.parser.python.ast.node;

public interface IAstArg extends IAstNode {
  IAstArg[] EMPTY_ARRAY = new IAstArg[0];

  enum ArgKind {
    POSITIONAL,
    KEYWORD
  }

  ArgKind argKind();
}
