package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstModuleName;
import org.japy.parser.python.ast.node.IAstImportedName;

public class AstImportFrom extends AstStmtNode {
  public final AstModuleName module;
  public final IAstImportedName[] names;

  public AstImportFrom(AstModuleName module, IAstImportedName[] names, ITokenSpan location) {
    super(0, location);
    this.module = module;
    this.names = names;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.IMPORTFROM;
  }

  @Override
  public void validate(Validator v) {
    v.validate(module);
    v.validate(names);
  }
}
