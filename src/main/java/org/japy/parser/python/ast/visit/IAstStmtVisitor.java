package org.japy.parser.python.ast.visit;

import org.japy.parser.python.ast.stmt.AstAnnAssign;
import org.japy.parser.python.ast.stmt.AstAnnExpr;
import org.japy.parser.python.ast.stmt.AstAssert;
import org.japy.parser.python.ast.stmt.AstAssign;
import org.japy.parser.python.ast.stmt.AstAugAssign;
import org.japy.parser.python.ast.stmt.AstBreak;
import org.japy.parser.python.ast.stmt.AstClass;
import org.japy.parser.python.ast.stmt.AstContinue;
import org.japy.parser.python.ast.stmt.AstDel;
import org.japy.parser.python.ast.stmt.AstExprStmt;
import org.japy.parser.python.ast.stmt.AstFor;
import org.japy.parser.python.ast.stmt.AstFunc;
import org.japy.parser.python.ast.stmt.AstIf;
import org.japy.parser.python.ast.stmt.AstImport;
import org.japy.parser.python.ast.stmt.AstImportFrom;
import org.japy.parser.python.ast.stmt.AstPass;
import org.japy.parser.python.ast.stmt.AstRaise;
import org.japy.parser.python.ast.stmt.AstReturn;
import org.japy.parser.python.ast.stmt.AstTry;
import org.japy.parser.python.ast.stmt.AstVarDecl;
import org.japy.parser.python.ast.stmt.AstWhile;
import org.japy.parser.python.ast.stmt.AstWith;

public interface IAstStmtVisitor {
  void visitClass(AstClass stmt);

  void visitFunc(AstFunc stmt);

  void visitImport(AstImport stmt);

  void visitImportFrom(AstImportFrom stmt);

  void visitTry(AstTry stmt);

  void visitIf(AstIf stmt);

  void visitFor(AstFor stmt);

  void visitWith(AstWith stmt);

  void visitWhile(AstWhile stmt);

  void visitPass(AstPass stmt);

  void visitDecl(AstVarDecl stmt);

  void visitRaise(AstRaise stmt);

  void visitReturn(AstReturn stmt);

  void visitExpr(AstExprStmt stmt);

  void visitAnnExpr(AstAnnExpr stmt);

  void visitAssign(AstAssign stmt);

  void visitAnnAssign(AstAnnAssign stmt);

  void visitAugAssign(AstAugAssign stmt);

  void visitDel(AstDel stmt);

  void visitAssert(AstAssert stmt);

  void visitBreak(AstBreak stmt);

  void visitContinue(AstContinue stmt);
}
