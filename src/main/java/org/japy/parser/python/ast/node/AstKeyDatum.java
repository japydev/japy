package org.japy.parser.python.ast.node;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.AstExprNode;
import org.japy.parser.python.ast.expr.IAstExpr;

public final class AstKeyDatum extends AstNode implements IAstDictEntry {
  public final IAstExpr key;
  public final IAstExpr value;

  public AstKeyDatum(IAstExpr key, IAstExpr value) {
    super(
        suspendFlags(key, value) | AstExprNode.compTimeFlag(key, value), key.start(), value.end());
    this.key = key;
    this.value = value;
  }

  @Override
  public void validate(Validator v) {
    v.validate(key);
    v.validate(value);
  }

  @Override
  public Kind dictEntryKind() {
    return Kind.SINGLE;
  }

  @Override
  public boolean isCompTimeValue() {
    return (flags & IAstExpr.FLAG_COMPTIME) != 0;
  }
}
