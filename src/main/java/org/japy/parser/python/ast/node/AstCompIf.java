package org.japy.parser.python.ast.node;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;

public final class AstCompIf extends AstNode implements IAstCompIter {
  public final IAstExpr expr;
  public final @Nullable IAstCompIter next;

  public AstCompIf(IAstExpr expr, @Nullable IAstCompIter next, ITokenSpan start) {
    super(suspendFlags(expr, next), start.start(), next != null ? next.end() : expr.end());
    this.expr = expr;
    this.next = next;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
    v.validateIfNotNull(next);
  }

  @Override
  public CompIterKind compIterKind() {
    return CompIterKind.IF;
  }

  @Override
  public @Nullable IAstCompIter nextCompIter() {
    return next;
  }
}
