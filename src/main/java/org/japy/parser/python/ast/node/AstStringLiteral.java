package org.japy.parser.python.ast.node;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstStringLiteral extends AstTokenNode implements IAstStringChunk {
  public final String text;

  public AstStringLiteral(String text, Token token) {
    super(token);
    this.text = text;
  }

  @Override
  public ChunkKind chunkKind() {
    return ChunkKind.LITERAL;
  }

  @Override
  public boolean isConst() {
    return true;
  }

  @Override
  public void validate(Validator v) {}
}
