package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstDecorator;
import org.japy.parser.python.ast.node.AstSuite;
import org.japy.parser.python.ast.node.IAstArg;

public class AstClass extends AstStmtNode implements IAstBlock {
  public final AstDecorator[] decorators;
  public final AstIdentifier name;
  public final IAstArg[] supers;
  public final AstSuite body;

  public AstClass(
      AstDecorator[] decorators,
      AstIdentifier name,
      IAstArg[] supers,
      AstSuite body,
      ITokenSpan start) {
    super(suspendFlags(decorators) | suspendFlags(supers), start.start(), body.end());
    this.decorators = decorators;
    this.name = name;
    this.supers = supers;
    this.body = body;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.CLASS;
  }

  @Override
  public void validate(Validator v) {
    v.validate(decorators);
    v.validate(name);
    v.validate(supers);
    v.validate(body);
  }

  @Override
  public AstSuite body() {
    return body;
  }
}
