package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstList extends AstExprNode {
  public final IAstExpr[] entries;

  public AstList(Token lbracket, Token rbracket, IAstExpr[] entries) {
    super(suspendFlags(entries) | compTimeFlag(entries), lbracket, rbracket);
    this.entries = entries;
  }

  @Override
  public void validate(Validator v) {
    v.validate(entries);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.LIST;
  }
}
