package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;

public class AstExprStmt extends AstStmtNode {
  public final IAstExpr expr;

  public AstExprStmt(IAstExpr expr) {
    super(expr.suspendFlags(), expr.start(), expr.end());
    this.expr = expr;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.EXPR;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
  }
}
