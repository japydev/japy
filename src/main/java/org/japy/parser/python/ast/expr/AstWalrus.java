package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;

public final class AstWalrus extends AstExprNode {
  public final AstIdentifier identifier;
  public final IAstExpr value;

  public AstWalrus(AstIdentifier identifier, IAstExpr value) {
    super(value.suspendFlags(), identifier, value);
    this.identifier = identifier;
    this.value = value;
  }

  @Override
  public void validate(Validator v) {
    v.validate(value);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.WALRUS;
  }
}
