package org.japy.parser.python.ast.expr;

import java.util.Arrays;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.node.IAstDictEntry;

public final class AstDict extends AstExprNode {
  public final IAstDictEntry[] entries;

  public AstDict(Token start, Token end, IAstDictEntry[] entries) {
    super(
        suspendFlags(entries)
            | (Arrays.stream(entries).allMatch(IAstDictEntry::isCompTimeValue) ? FLAG_COMPTIME : 0),
        start,
        end);
    this.entries = entries;
  }

  @Override
  public void validate(Validator v) {
    v.validate(entries);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.DICT;
  }
}
