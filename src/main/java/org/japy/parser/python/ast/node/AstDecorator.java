package org.japy.parser.python.ast.node;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.Token;

public final class AstDecorator extends AstNode {
  public static final AstDecorator[] EMPTY_ARRAY = new AstDecorator[0];

  public final IAstExpr expr;

  public AstDecorator(Token at, IAstExpr expr) {
    super(expr.suspendFlags(), at, expr.end());
    this.expr = expr;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
  }
}
