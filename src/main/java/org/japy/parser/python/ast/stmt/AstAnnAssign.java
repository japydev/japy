package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;

public class AstAnnAssign extends AstStmtNode {
  public final IAstExpr target;
  public final IAstExpr annotation;
  public final IAstExpr value;

  public AstAnnAssign(IAstExpr target, IAstExpr annotation, IAstExpr value) {
    super(suspendFlags(target, annotation, value), target.start(), value.end());
    this.target = target;
    this.annotation = annotation;
    this.value = value;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.ANNASSIGN;
  }

  @Override
  public void validate(Validator v) {
    v.validate(target);
    v.validate(annotation);
    v.validate(value);
  }
}
