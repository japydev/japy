package org.japy.parser.python.ast.node;

import org.japy.infra.validation.ISupportsValidation;
import org.japy.parser.python.ast.loc.ITokenSpan;

public interface IAstNode extends ISupportsValidation, ITokenSpan {
  int FLAG_CAN_AWAIT = 1;
  int FLAG_CAN_YIELD = 2;
  int NEXT_FLAG = 4;

  default boolean canYield() {
    return (flags() & FLAG_CAN_YIELD) != 0;
  }

  default boolean canAwait() {
    return (flags() & FLAG_CAN_AWAIT) != 0;
  }

  default boolean canSuspend() {
    return canYield() || canAwait();
  }

  default int suspendFlags() {
    return flags() & (FLAG_CAN_AWAIT | FLAG_CAN_YIELD);
  }

  int flags();
}
