package org.japy.parser.python.ast.stmt;

import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstNode;

public abstract class AstStmtNode extends AstNode implements IAstStmt {
  protected AstStmtNode(int flags, ITokenSpan location) {
    super(flags, location);
  }

  protected AstStmtNode(int flags, ITokenSpan start, ITokenSpan end) {
    super(flags, start, end);
  }

  @Override
  public String toString() {
    return kind().toString();
  }
}
