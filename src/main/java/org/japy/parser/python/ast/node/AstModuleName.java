package org.japy.parser.python.ast.node;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.ITokenSpan;

public final class AstModuleName extends AstNode {
  public final int dots;
  public final AstIdentifier[] components;

  public AstModuleName(ITokenSpan location, int dots, AstIdentifier[] components) {
    super(0, location);
    this.dots = dots;
    this.components = components;
  }

  public String toStringNoLeadingDots() {
    return Arrays.stream(components).map(id -> id.name).collect(Collectors.joining("."));
  }

  public String toStringNoLastPart() {
    dcheck(components.length > 1);
    StringBuilder sb = new StringBuilder(".".repeat(dots));
    for (int i = 0, c = components.length - 1; i != c; ++i) {
      if (i != 0) {
        sb.append('.');
      }
      sb.append(components[i].name);
    }
    return sb.toString();
  }

  @Override
  public String toString() {
    return ".".repeat(dots) + toStringNoLeadingDots();
  }

  @Override
  public void validate(Validator v) {}
}
