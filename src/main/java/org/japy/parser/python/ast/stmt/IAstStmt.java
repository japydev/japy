package org.japy.parser.python.ast.stmt;

import org.japy.parser.python.ast.node.IAstNode;

public interface IAstStmt extends IAstNode {
  AstStmtKind kind();

  default boolean isNop() {
    return false;
  }
}
