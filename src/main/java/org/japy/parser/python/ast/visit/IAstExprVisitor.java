package org.japy.parser.python.ast.visit;

import org.japy.parser.python.ast.expr.AstAttrRef;
import org.japy.parser.python.ast.expr.AstAwait;
import org.japy.parser.python.ast.expr.AstBinOp;
import org.japy.parser.python.ast.expr.AstCall;
import org.japy.parser.python.ast.expr.AstComparison;
import org.japy.parser.python.ast.expr.AstConditional;
import org.japy.parser.python.ast.expr.AstConst;
import org.japy.parser.python.ast.expr.AstDict;
import org.japy.parser.python.ast.expr.AstDictComp;
import org.japy.parser.python.ast.expr.AstDoubleStar;
import org.japy.parser.python.ast.expr.AstGenerator;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.AstLambda;
import org.japy.parser.python.ast.expr.AstList;
import org.japy.parser.python.ast.expr.AstListComp;
import org.japy.parser.python.ast.expr.AstLogicalOp;
import org.japy.parser.python.ast.expr.AstNot;
import org.japy.parser.python.ast.expr.AstParen;
import org.japy.parser.python.ast.expr.AstSet;
import org.japy.parser.python.ast.expr.AstSetComp;
import org.japy.parser.python.ast.expr.AstSlice;
import org.japy.parser.python.ast.expr.AstStar;
import org.japy.parser.python.ast.expr.AstString;
import org.japy.parser.python.ast.expr.AstSubscript;
import org.japy.parser.python.ast.expr.AstTuple;
import org.japy.parser.python.ast.expr.AstUnaryOp;
import org.japy.parser.python.ast.expr.AstWalrus;
import org.japy.parser.python.ast.expr.AstYieldExpr;
import org.japy.parser.python.ast.expr.AstYieldFrom;

public interface IAstExprVisitor {
  void visitConst(AstConst expr);

  void visitString(AstString expr);

  void visitIdentifier(AstIdentifier expr);

  void visitWalrus(AstWalrus expr);

  void visitStar(AstStar expr);

  void visitDoubleStar(AstDoubleStar expr);

  void visitList(AstList expr);

  void visitSet(AstSet expr);

  void visitDict(AstDict expr);

  void visitTuple(AstTuple expr);

  void visitListComp(AstListComp expr);

  void visitSetComp(AstSetComp expr);

  void visitDictComp(AstDictComp expr);

  void visitParen(AstParen expr);

  void visitCall(AstCall expr);

  void visitAttrRef(AstAttrRef expr);

  void visitSubscript(AstSubscript expr);

  void visitNot(AstNot expr);

  void visitComparison(AstComparison expr);

  void visitBinOp(AstBinOp expr);

  void visitUnaryOp(AstUnaryOp expr);

  void visitLogicalOp(AstLogicalOp expr);

  void visitConditional(AstConditional expr);

  void visitSlice(AstSlice expr);

  void visitYield(AstYieldExpr expr);

  void visitYieldFrom(AstYieldFrom expr);

  void visitLambda(AstLambda expr);

  void visitGenerator(AstGenerator expr);

  void visitAwait(AstAwait expr);
}
