package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstStar extends AstExprNode {
  public final IAstExpr list;

  public AstStar(IAstExpr list, Token star) {
    super(list.suspendFlags(), star, list);
    this.list = list;
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.STAR;
  }

  @Override
  public void validate(Validator v) {
    v.validate(list);
  }
}
