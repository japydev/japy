package org.japy.parser.python.ast.node;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;

public final class AstPosArg extends AstNode implements IAstArg {
  public final IAstExpr value;

  public AstPosArg(IAstExpr value) {
    super(value.suspendFlags(), value);
    this.value = value;
  }

  @Override
  public void validate(Validator v) {
    v.validate(value);
  }

  @Override
  public ArgKind argKind() {
    return ArgKind.POSITIONAL;
  }

  @Override
  public String toString() {
    return value.toString();
  }
}
