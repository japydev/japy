package org.japy.parser.python.ast.stmt;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstSuite;

public class AstFor extends AstStmtNode {
  public final boolean async;
  public final IAstExpr target;
  public final IAstExpr list;
  public final AstSuite body;
  public final @Nullable AstElse else_;

  public AstFor(
      boolean async,
      IAstExpr target,
      IAstExpr list,
      ITokenSpan start,
      AstSuite body,
      @Nullable AstElse else_) {
    super(
        (async ? FLAG_CAN_AWAIT : 0) | suspendFlags(target, list, else_) | body.suspendFlags(),
        start,
        else_ != null ? else_.end() : body.end());
    this.async = async;
    this.target = target;
    this.list = list;
    this.body = body;
    this.else_ = else_;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.FOR;
  }

  @Override
  public void validate(Validator v) {
    v.validate(target);
    v.validate(list);
    v.validate(body);
    v.validateIfNotNull(else_);
  }
}
