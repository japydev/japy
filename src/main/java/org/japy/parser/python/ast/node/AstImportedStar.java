package org.japy.parser.python.ast.node;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstImportedStar extends AstTokenNode implements IAstImportedName {
  public AstImportedStar(Token token) {
    super(token);
  }

  @Override
  public void validate(Validator v) {}

  @Override
  public Kind kind() {
    return Kind.STAR;
  }
}
