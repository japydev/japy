package org.japy.parser.python.ast.expr;

import org.japy.base.BinOp;
import org.japy.infra.validation.Validator;

public final class AstBinOp extends AstExprNode {
  public final BinOp op;
  public final IAstExpr lhs;
  public final IAstExpr rhs;

  public AstBinOp(BinOp op, IAstExpr lhs, IAstExpr rhs) {
    super(suspendFlags(lhs, rhs) | constFlag(lhs, rhs), lhs, rhs);
    this.op = op;
    this.lhs = lhs;
    this.rhs = rhs;
  }

  @Override
  public void validate(Validator v) {
    v.validate(lhs);
    v.validate(rhs);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.BIN_OP;
  }

  @Override
  public String toString() {
    return op + "(" + lhs + ", " + rhs + ")";
  }
}
