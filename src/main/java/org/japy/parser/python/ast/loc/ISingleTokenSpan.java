package org.japy.parser.python.ast.loc;

public interface ISingleTokenSpan extends ITokenSpan {
  Token token();

  @Override
  default Token start() {
    return token();
  }

  @Override
  default Token end() {
    return token();
  }
}
