package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstParen extends AstExprNode {
  public final IAstExpr expr;

  public AstParen(Token lparen, Token rparen, IAstExpr expr) {
    super(expr.flags(), lparen, rparen);
    this.expr = expr;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.PAREN;
  }

  @Override
  public String toString() {
    return "(" + expr + ')';
  }
}
