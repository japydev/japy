package org.japy.parser.python.ast.expr;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstYieldExpr extends AstExprNode implements IAstYieldExpr {
  public final @Nullable IAstExpr value;

  public AstYieldExpr(Token yield) {
    super(FLAG_CAN_YIELD, yield);
    this.value = null;
  }

  public AstYieldExpr(Token yield, IAstExpr value) {
    super(FLAG_CAN_YIELD | value.suspendFlags(), yield, value);
    this.value = value;
  }

  @Override
  public void validate(Validator v) {
    v.validateIfNotNull(value);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.YIELD;
  }
}
