package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstNode;
import org.japy.parser.python.ast.node.AstSuite;

public class AstElse extends AstNode {
  public final AstSuite body;

  public AstElse(AstSuite body, ITokenSpan location) {
    super(body.suspendFlags(), location, body.end());
    this.body = body;
  }

  @Override
  public void validate(Validator v) {
    v.validate(body);
  }
}
