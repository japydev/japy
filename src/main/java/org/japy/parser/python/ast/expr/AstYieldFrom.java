package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstYieldFrom extends AstExprNode implements IAstYieldExpr {
  public final IAstExpr list;

  public AstYieldFrom(Token yield, IAstExpr list) {
    super(FLAG_CAN_YIELD | list.suspendFlags(), yield, list);
    this.list = list;
  }

  @Override
  public void validate(Validator v) {
    v.validate(list);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.YIELD_FROM;
  }
}
