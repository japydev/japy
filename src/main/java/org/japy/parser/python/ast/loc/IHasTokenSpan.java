package org.japy.parser.python.ast.loc;

import org.japy.infra.loc.Location;

public interface IHasTokenSpan extends ITokenSpan {
  ITokenSpan tokenSpan();

  @Override
  default Token start() {
    return tokenSpan().start();
  }

  @Override
  default Token end() {
    return tokenSpan().end();
  }

  @Override
  default Location location() {
    return tokenSpan().location();
  }
}
