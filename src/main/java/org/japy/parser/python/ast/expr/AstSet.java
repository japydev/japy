package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstSet extends AstExprNode {
  public final IAstExpr[] entries;

  public AstSet(Token lbrace, Token rbrace, IAstExpr[] entries) {
    super(suspendFlags(entries) | compTimeFlag(entries), lbrace, rbrace);
    this.entries = entries;
  }

  @Override
  public void validate(Validator v) {
    v.validate(entries);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.SET;
  }
}
