package org.japy.parser.python.ast.expr;

import java.util.Arrays;

import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstNode;

public abstract class AstExprNode extends AstNode implements IAstExpr {
  public AstExprNode(int flags, ITokenSpan location) {
    super(implyCompTime(flags), location);
  }

  public AstExprNode(int flags, ITokenSpan start, ITokenSpan end) {
    super(implyCompTime(flags), start, end);
  }

  @Override
  public String toString() {
    return kind().toString();
  }

  public static int implyCompTime(int flags) {
    return (flags & FLAG_CONST) != 0 ? (flags | FLAG_COMPTIME) : flags;
  }

  public static int constFlag(IAstExpr expr1) {
    return expr1.isConst() ? FLAG_CONST : 0;
  }

  public static int constFlag(IAstExpr expr1, IAstExpr expr2) {
    return (expr1.isConst() && expr2.isConst()) ? FLAG_CONST : 0;
  }

  public static int constFlag(IAstExpr expr1, IAstExpr expr2, IAstExpr expr3) {
    return (expr1.isConst() && expr2.isConst() && expr3.isConst()) ? FLAG_CONST : 0;
  }

  public static int constFlag(IAstExpr[] exprs) {
    return Arrays.stream(exprs).allMatch(IAstExpr::isConst) ? FLAG_CONST : 0;
  }

  public static int constFlags(IAstExpr[] exprs) {
    boolean isConst = Arrays.stream(exprs).allMatch(IAstExpr::isConst);
    boolean isCompTime = isConst || Arrays.stream(exprs).allMatch(IAstExpr::isCompTimeValue);
    return (isConst ? FLAG_CONST : 0) | (isCompTime ? FLAG_COMPTIME : 0);
  }

  public static int compTimeFlag(IAstExpr expr1, IAstExpr expr2) {
    return expr1.isCompTimeValue() && expr2.isCompTimeValue() ? FLAG_COMPTIME : 0;
  }

  public static int compTimeFlag(IAstExpr[] exprs) {
    return Arrays.stream(exprs).allMatch(IAstExpr::isCompTimeValue) ? FLAG_COMPTIME : 0;
  }
}
