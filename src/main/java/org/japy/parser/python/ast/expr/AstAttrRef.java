package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;

public final class AstAttrRef extends AstExprNode {
  public final IAstExpr obj;
  public final AstIdentifier attr;

  public AstAttrRef(IAstExpr obj, AstIdentifier attr) {
    super(obj.suspendFlags(), obj.start(), attr.end());
    this.obj = obj;
    this.attr = attr;
  }

  @Override
  public void validate(Validator v) {
    v.validate(obj, attr);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.ATTR_REF;
  }

  @Override
  public String toString() {
    return obj + "." + attr;
  }
}
