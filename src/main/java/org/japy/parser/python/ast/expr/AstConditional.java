package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;

public final class AstConditional extends AstExprNode {
  public final IAstExpr ifTrue;
  public final IAstExpr condition;
  public final IAstExpr ifFalse;

  public AstConditional(IAstExpr ifTrue, IAstExpr condition, IAstExpr ifFalse) {
    super(
        suspendFlags(ifTrue, condition, ifFalse) | constFlag(ifTrue, condition, ifFalse),
        ifTrue,
        ifFalse);
    this.ifTrue = ifTrue;
    this.condition = condition;
    this.ifFalse = ifFalse;
  }

  @Override
  public void validate(Validator v) {
    v.validate(ifTrue);
    v.validate(condition);
    v.validate(ifFalse);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.COND;
  }

  @Override
  public String toString() {
    return "IF(" + condition + ", " + ifTrue + ", " + ifFalse + ')';
  }
}
