package org.japy.parser.python.ast.expr;

import static org.japy.infra.coll.CollUtil.first;
import static org.japy.infra.coll.CollUtil.last;

import java.util.Arrays;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.node.IAstStringChunk;

public final class AstString extends AstExprNode {
  public final IAstStringChunk[] chunks;

  public AstString(IAstStringChunk[] chunks) {
    super(
        suspendFlags(chunks)
            | (Arrays.stream(chunks).allMatch(IAstStringChunk::isConst) ? FLAG_CONST : 0),
        first(chunks),
        last(chunks));
    this.chunks = chunks;
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.STRING;
  }

  @Override
  public void validate(Validator v) {
    v.validate(chunks);
  }

  @Override
  public String toString() {
    return "STRING";
  }
}
