package org.japy.parser.python.ast.node;

import static org.japy.infra.util.ObjUtil.or;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;

public final class AstWithItem extends AstNode {
  public final IAstExpr expr;
  public final @Nullable IAstExpr as;

  public AstWithItem(IAstExpr expr, @Nullable IAstExpr as) {
    super(suspendFlags(expr, as), expr.start(), or(as, expr).end());
    this.expr = expr;
    this.as = as;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
    v.validateIfNotNull(as);
  }
}
