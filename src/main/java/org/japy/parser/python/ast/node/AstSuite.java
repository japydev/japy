package org.japy.parser.python.ast.node;

import static org.japy.infra.coll.CollUtil.first;
import static org.japy.infra.coll.CollUtil.last;

import java.util.Arrays;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.stmt.IAstStmt;

public class AstSuite extends AstNode {
  public final IAstStmt[] stmts;

  public AstSuite(IAstStmt[] stmts) {
    super(suspendFlags(stmts), first(stmts), last(stmts));
    this.stmts = stmts;
  }

  @Override
  public void validate(Validator v) {
    v.validate(stmts);
  }

  public boolean isNop() {
    return Arrays.stream(stmts).allMatch(IAstStmt::isNop);
  }
}
