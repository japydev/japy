package org.japy.parser.python.ast.visit;

import org.japy.infra.exc.InternalErrorException;
import org.japy.parser.python.ast.expr.*;

public class BaseAstExprVisitor implements IAstExprVisitor {
  protected BaseAstExprVisitor() {}

  public void visitAny(IAstExpr expr) {
    throw new InternalErrorException(expr.kind().toString(), expr);
  }

  @Override
  public void visitConst(AstConst expr) {
    visitAny(expr);
  }

  @Override
  public void visitString(AstString expr) {
    visitAny(expr);
  }

  @Override
  public void visitIdentifier(AstIdentifier expr) {
    visitAny(expr);
  }

  @Override
  public void visitWalrus(AstWalrus expr) {
    visitAny(expr);
  }

  @Override
  public void visitStar(AstStar expr) {
    visitAny(expr);
  }

  @Override
  public void visitDoubleStar(AstDoubleStar expr) {
    visitAny(expr);
  }

  @Override
  public void visitList(AstList expr) {
    visitAny(expr);
  }

  @Override
  public void visitSet(AstSet expr) {
    visitAny(expr);
  }

  @Override
  public void visitDict(AstDict expr) {
    visitAny(expr);
  }

  @Override
  public void visitTuple(AstTuple expr) {
    visitAny(expr);
  }

  @Override
  public void visitListComp(AstListComp expr) {
    visitAny(expr);
  }

  @Override
  public void visitSetComp(AstSetComp expr) {
    visitAny(expr);
  }

  @Override
  public void visitDictComp(AstDictComp expr) {
    visitAny(expr);
  }

  @Override
  public void visitParen(AstParen expr) {
    visitAny(expr);
  }

  @Override
  public void visitCall(AstCall expr) {
    visitAny(expr);
  }

  @Override
  public void visitAttrRef(AstAttrRef expr) {
    visitAny(expr);
  }

  @Override
  public void visitSubscript(AstSubscript expr) {
    visitAny(expr);
  }

  @Override
  public void visitNot(AstNot expr) {
    visitAny(expr);
  }

  @Override
  public void visitComparison(AstComparison expr) {
    visitAny(expr);
  }

  @Override
  public void visitBinOp(AstBinOp expr) {
    visitAny(expr);
  }

  @Override
  public void visitUnaryOp(AstUnaryOp expr) {
    visitAny(expr);
  }

  @Override
  public void visitLogicalOp(AstLogicalOp expr) {
    visitAny(expr);
  }

  @Override
  public void visitConditional(AstConditional expr) {
    visitAny(expr);
  }

  @Override
  public void visitSlice(AstSlice expr) {
    visitAny(expr);
  }

  @Override
  public void visitYield(AstYieldExpr expr) {
    visitAny(expr);
  }

  @Override
  public void visitYieldFrom(AstYieldFrom expr) {
    visitAny(expr);
  }

  @Override
  public void visitLambda(AstLambda expr) {
    visitAny(expr);
  }

  @Override
  public void visitGenerator(AstGenerator expr) {
    visitAny(expr);
  }

  @Override
  public void visitAwait(AstAwait expr) {
    visitAny(expr);
  }
}
