package org.japy.parser.python.ast.stmt;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;

public class AstReturn extends AstStmtNode {
  public final @Nullable IAstExpr value;

  public AstReturn(@Nullable IAstExpr value, ITokenSpan location) {
    super(suspendFlags(value), location);
    this.value = value;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.RETURN;
  }

  @Override
  public void validate(Validator v) {
    v.validateIfNotNull(value);
  }
}
