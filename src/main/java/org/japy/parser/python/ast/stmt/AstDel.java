package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;

public class AstDel extends AstStmtNode {
  public final IAstExpr expr;

  public AstDel(IAstExpr expr, ITokenSpan location) {
    super(expr.suspendFlags(), location.start(), expr.end());
    this.expr = expr;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.DEL;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
  }
}
