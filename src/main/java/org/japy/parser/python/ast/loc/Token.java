package org.japy.parser.python.ast.loc;

import com.google.errorprone.annotations.Immutable;

import org.japy.infra.loc.Location;

/** Range of characters from start to end exclusive */
@Immutable
public final class Token implements ISingleTokenSpan {
  public final Location start;
  public final Location end;

  public Token(Location start, Location end) {
    this.start = start;
    this.end = end;
  }

  @Override
  public Token token() {
    return this;
  }
}
