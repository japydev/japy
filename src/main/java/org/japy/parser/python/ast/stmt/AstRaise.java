package org.japy.parser.python.ast.stmt;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;

public class AstRaise extends AstStmtNode {
  public final @Nullable IAstExpr ex;
  public final @Nullable IAstExpr from;

  public AstRaise(@Nullable IAstExpr ex, @Nullable IAstExpr from, ITokenSpan location) {
    super(suspendFlags(ex, from), location);
    this.ex = ex;
    this.from = from;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.RAISE;
  }

  @Override
  public void validate(Validator v) {
    v.validateIfNotNull(ex);
    v.validateIfNotNull(from);
  }
}
