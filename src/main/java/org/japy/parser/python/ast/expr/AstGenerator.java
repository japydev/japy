package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.node.AstCompFor;

public final class AstGenerator extends AstExprNode {
  public final IAstExpr expr;
  public final AstCompFor compFor;

  public AstGenerator(IAstExpr expr, AstCompFor compFor) {
    // first list is evaluated eagerly
    super(compFor.list.suspendFlags(), expr.start(), compFor.end());
    this.expr = expr;
    this.compFor = compFor;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
    v.validate(compFor);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.GENERATOR;
  }
}
