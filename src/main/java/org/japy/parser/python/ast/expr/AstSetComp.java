package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.node.AstCompFor;

public final class AstSetComp extends AstExprNode {
  public final IAstExpr expr;
  public final AstCompFor compFor;

  public AstSetComp(Token lbrace, Token rbrace, IAstExpr expr, AstCompFor compFor) {
    super(suspendFlags(expr, compFor), lbrace, rbrace);
    this.expr = expr;
    this.compFor = compFor;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
    v.validate(compFor);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.SET_COMP;
  }
}
