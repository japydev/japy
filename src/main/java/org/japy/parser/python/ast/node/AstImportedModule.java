package org.japy.parser.python.ast.node;

import static org.japy.infra.util.ObjUtil.or;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.AstIdentifier;

public final class AstImportedModule extends AstNode {
  public final AstModuleName module;
  public final @Nullable AstIdentifier as;

  public AstImportedModule(AstModuleName module, @Nullable AstIdentifier as) {
    super(0, module.start(), or(as, module).end());
    this.module = module;
    this.as = as;
  }

  @Override
  public void validate(Validator v) {
    v.validate(module);
  }
}
