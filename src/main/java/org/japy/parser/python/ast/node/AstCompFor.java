package org.japy.parser.python.ast.node;

import static org.japy.infra.util.ObjUtil.or;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;

public final class AstCompFor extends AstNode implements IAstCompIter {
  public final boolean async;
  public final IAstExpr target;
  public final IAstExpr list;
  public final @Nullable IAstCompIter next;

  public AstCompFor(
      boolean async,
      IAstExpr target,
      IAstExpr list,
      @Nullable IAstCompIter next,
      ITokenSpan start) {
    super(
        (async ? FLAG_CAN_AWAIT : 0) | suspendFlags(target, list, next),
        start,
        or(next, list).end());
    this.async = async;
    this.target = target;
    this.list = list;
    this.next = next;
  }

  @Override
  public void validate(Validator v) {
    v.validate(target);
    v.validate(list);
    v.validateIfNotNull(next);
  }

  @Override
  public CompIterKind compIterKind() {
    return CompIterKind.FOR;
  }

  @Override
  public @Nullable IAstCompIter nextCompIter() {
    return next;
  }
}
