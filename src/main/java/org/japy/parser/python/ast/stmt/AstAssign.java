package org.japy.parser.python.ast.stmt;

import static org.japy.infra.coll.CollUtil.first;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;

public class AstAssign extends AstStmtNode {
  public final IAstExpr[] targets;
  public final IAstExpr value;

  public AstAssign(IAstExpr[] targets, IAstExpr value) {
    super(suspendFlags(targets) | value.suspendFlags(), first(targets).start(), value.end());
    this.targets = targets;
    this.value = value;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.ASSIGN;
  }

  @Override
  public void validate(Validator v) {
    v.validate(targets);
    v.validate(value);
  }
}
