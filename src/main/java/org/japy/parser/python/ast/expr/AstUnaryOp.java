package org.japy.parser.python.ast.expr;

import org.japy.base.UnaryOp;
import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstUnaryOp extends AstExprNode {
  public final UnaryOp op;
  public final IAstExpr operand;

  public AstUnaryOp(Token opToken, UnaryOp op, IAstExpr operand) {
    super(operand.flags(), opToken, operand);
    this.op = op;
    this.operand = operand;
  }

  @Override
  public void validate(Validator v) {
    v.validate(operand);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.UNARY_OP;
  }
}
