package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.ITokenSpan;

public class AstContinue extends AstStmtNode {
  public AstContinue(ITokenSpan location) {
    super(0, location);
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.CONTINUE;
  }

  @Override
  public void validate(Validator v) {}
}
