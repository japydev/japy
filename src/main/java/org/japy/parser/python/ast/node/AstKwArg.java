package org.japy.parser.python.ast.node;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.IAstExpr;

public final class AstKwArg extends AstNode implements IAstArg {
  public final AstIdentifier name;
  public final IAstExpr value;

  public AstKwArg(AstIdentifier name, IAstExpr value) {
    super(value.suspendFlags(), name.start(), value.end());
    this.name = name;
    this.value = value;
  }

  @Override
  public void validate(Validator v) {
    v.validate(name, value);
  }

  @Override
  public ArgKind argKind() {
    return ArgKind.KEYWORD;
  }

  @Override
  public String toString() {
    return name + "=" + value;
  }
}
