package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstNot extends AstExprNode {
  public final IAstExpr expr;

  public AstNot(Token not, IAstExpr expr) {
    super(expr.suspendFlags() | constFlag(expr), not, expr);
    this.expr = expr;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.NOT;
  }
}
