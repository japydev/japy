package org.japy.parser.python.ast.expr;

import org.japy.base.LogicalOp;
import org.japy.infra.validation.Validator;

public final class AstLogicalOp extends AstExprNode {
  public final LogicalOp op;
  public final IAstExpr lhs;
  public final IAstExpr rhs;

  public AstLogicalOp(LogicalOp op, IAstExpr lhs, IAstExpr rhs) {
    super(suspendFlags(lhs, rhs) | constFlag(lhs, rhs), lhs, rhs);
    this.op = op;
    this.lhs = lhs;
    this.rhs = rhs;
  }

  @Override
  public void validate(Validator v) {
    v.validate(lhs);
    v.validate(rhs);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.LOGICAL_OP;
  }
}
