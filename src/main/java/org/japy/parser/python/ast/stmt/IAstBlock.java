package org.japy.parser.python.ast.stmt;

import org.japy.parser.python.ast.node.AstSuite;
import org.japy.parser.python.ast.node.IAstNode;

public interface IAstBlock extends IAstNode {
  AstSuite body();
}
