package org.japy.parser.python.ast.node;

public interface IAstImportedName extends IAstNode {
  enum Kind {
    STAR,
    NAME,
  }

  Kind kind();
}
