package org.japy.parser.python.ast.node;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.RFConversion;
import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.AstString;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.Token;

public final class AstReplField extends AstNode implements IAstStringChunk {
  public final @Nullable String exprText;
  public final IAstExpr expr;
  public final @Nullable RFConversion conversion;
  public final @Nullable AstString formatSpec;

  public AstReplField(
      Token start,
      Token end,
      @Nullable String exprText,
      IAstExpr expr,
      @Nullable RFConversion conversion,
      @Nullable AstString formatSpec) {
    super(suspendFlags(expr, formatSpec), start, end);
    this.exprText = exprText;
    this.expr = expr;
    this.conversion = conversion;
    this.formatSpec = formatSpec;
  }

  @Override
  public ChunkKind chunkKind() {
    return ChunkKind.REPLFIELD;
  }

  @Override
  public boolean isConst() {
    return false;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
    v.validateIfNotNull(formatSpec);
  }
}
