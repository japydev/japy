package org.japy.parser.python.ast.visit;

import org.japy.parser.python.ast.stmt.*;

public final class AstStmtVisit {
  private AstStmtVisit() {}

  public static void visit(IAstStmt stmt, IAstStmtVisitor visitor) {
    switch (stmt.kind()) {
      case CLASS:
        visitor.visitClass((AstClass) stmt);
        break;
      case FUNC:
        visitor.visitFunc((AstFunc) stmt);
        break;
      case IMPORT:
        visitor.visitImport((AstImport) stmt);
        break;
      case IMPORTFROM:
        visitor.visitImportFrom((AstImportFrom) stmt);
        break;
      case TRY:
        visitor.visitTry((AstTry) stmt);
        break;
      case IF:
        visitor.visitIf((AstIf) stmt);
        break;
      case FOR:
        visitor.visitFor((AstFor) stmt);
        break;
      case WITH:
        visitor.visitWith((AstWith) stmt);
        break;
      case WHILE:
        visitor.visitWhile((AstWhile) stmt);
        break;
      case PASS:
        visitor.visitPass((AstPass) stmt);
        break;
      case DECL:
        visitor.visitDecl((AstVarDecl) stmt);
        break;
      case RAISE:
        visitor.visitRaise((AstRaise) stmt);
        break;
      case RETURN:
        visitor.visitReturn((AstReturn) stmt);
        break;
      case EXPR:
        visitor.visitExpr((AstExprStmt) stmt);
        break;
      case ANNEXPR:
        visitor.visitAnnExpr((AstAnnExpr) stmt);
        break;
      case ASSIGN:
        visitor.visitAssign((AstAssign) stmt);
        break;
      case ANNASSIGN:
        visitor.visitAnnAssign((AstAnnAssign) stmt);
        break;
      case AUGASSIGN:
        visitor.visitAugAssign((AstAugAssign) stmt);
        break;
      case DEL:
        visitor.visitDel((AstDel) stmt);
        break;
      case ASSERT:
        visitor.visitAssert((AstAssert) stmt);
        break;
      case BREAK:
        visitor.visitBreak((AstBreak) stmt);
        break;
      case CONTINUE:
        visitor.visitContinue((AstContinue) stmt);
        break;
    }
  }
}
