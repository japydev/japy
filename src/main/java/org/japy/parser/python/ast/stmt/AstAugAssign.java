package org.japy.parser.python.ast.stmt;

import org.japy.base.AugOp;
import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;

public class AstAugAssign extends AstStmtNode {
  public final AugOp op;
  public final IAstExpr target;
  public final IAstExpr value;

  public AstAugAssign(AugOp op, IAstExpr target, IAstExpr value) {
    super(suspendFlags(target, value), target.start(), value.end());
    this.op = op;
    this.target = target;
    this.value = value;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.AUGASSIGN;
  }

  @Override
  public void validate(Validator v) {
    v.validate(target);
    v.validate(value);
  }
}
