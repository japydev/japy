package org.japy.parser.python.ast.loc;

import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.Location;

/** Range of tokens from start to end inclusive */
public interface ITokenSpan extends IHasLocation {
  Token start();

  Token end();

  @Override
  default Location location() {
    return start().start;
  }
}
