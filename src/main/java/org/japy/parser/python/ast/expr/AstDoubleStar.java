package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.node.IAstDictEntry;

public final class AstDoubleStar extends AstExprNode implements IAstDictEntry {
  public final IAstExpr dict;

  public AstDoubleStar(IAstExpr dict, Token pow) {
    super(dict.suspendFlags(), pow, dict.end());
    this.dict = dict;
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.DOUBLE_STAR;
  }

  @Override
  public boolean isCompTimeValue() {
    return super.isCompTimeValue();
  }

  @Override
  public void validate(Validator v) {
    v.validate(dict);
  }

  @Override
  public Kind dictEntryKind() {
    return Kind.STARRED;
  }
}
