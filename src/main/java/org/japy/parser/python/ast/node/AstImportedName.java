package org.japy.parser.python.ast.node;

import static org.japy.infra.util.ObjUtil.or;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.AstIdentifier;

public final class AstImportedName extends AstNode implements IAstImportedName {
  public final AstIdentifier identifier;
  public final @Nullable AstIdentifier as;

  public AstImportedName(AstIdentifier identifier, @Nullable AstIdentifier as) {
    super(0, identifier.start(), or(as, identifier).end());
    this.identifier = identifier;
    this.as = as;
  }

  @Override
  public void validate(Validator v) {
    v.validate(identifier);
    v.validateIfNotNull(as);
  }

  @Override
  public Kind kind() {
    return Kind.NAME;
  }
}
