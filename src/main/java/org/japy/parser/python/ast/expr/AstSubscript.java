package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;

public final class AstSubscript extends AstExprNode {
  public final IAstExpr obj;
  public final IAstExpr index;

  public AstSubscript(IAstExpr obj, IAstExpr index, Token lbracket, Token rbracket) {
    super(suspendFlags(obj, index), lbracket, rbracket);
    this.obj = obj;
    this.index = index;
  }

  @Override
  public void validate(Validator v) {
    v.validate(obj);
    v.validate(index);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.SUBSCRIPT;
  }
}
