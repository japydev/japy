package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.node.AstCompFor;

public final class AstListComp extends AstExprNode {
  public final IAstExpr expr;
  public final AstCompFor compFor;

  public AstListComp(Token lbracket, Token rbracket, IAstExpr expr, AstCompFor compFor) {
    super(suspendFlags(expr, compFor), lbracket, rbracket);
    this.expr = expr;
    this.compFor = compFor;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
    v.validate(compFor);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.LIST_COMP;
  }
}
