package org.japy.parser.python.ast.node;

import org.japy.parser.python.ast.loc.ISingleTokenSpan;
import org.japy.parser.python.ast.loc.Token;

public abstract class AstTokenNode extends AstNode implements ISingleTokenSpan {
  protected AstTokenNode(Token token) {
    super(0, token, token);
  }

  @Override
  public Token token() {
    return start();
  }
}
