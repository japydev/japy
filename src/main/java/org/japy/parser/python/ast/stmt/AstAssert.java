package org.japy.parser.python.ast.stmt;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;

public class AstAssert extends AstStmtNode {
  public final IAstExpr expr;
  public final @Nullable IAstExpr message;

  public AstAssert(IAstExpr expr, @Nullable IAstExpr message, ITokenSpan location) {
    super(
        suspendFlags(expr, message),
        location.start(),
        message != null ? message.end() : expr.end());
    this.expr = expr;
    this.message = message;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.ASSERT;
  }

  @Override
  public void validate(Validator v) {
    v.validate(expr);
    v.validateIfNotNull(message);
  }
}
