package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.AstImportedModule;

public class AstImport extends AstStmtNode {
  public final AstImportedModule[] modules;

  public AstImport(AstImportedModule[] modules, ITokenSpan location) {
    super(0, location);
    this.modules = modules;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.IMPORT;
  }

  @Override
  public void validate(Validator v) {
    v.validate(modules);
  }
}
