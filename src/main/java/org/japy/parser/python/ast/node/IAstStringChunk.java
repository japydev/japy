package org.japy.parser.python.ast.node;

public interface IAstStringChunk extends IAstNode {
  enum ChunkKind {
    LITERAL,
    REPLFIELD,
  }

  ChunkKind chunkKind();

  boolean isConst();
}
