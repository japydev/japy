package org.japy.parser.python.ast.expr;

import static org.japy.infra.coll.CollUtil.first;
import static org.japy.infra.coll.CollUtil.last;
import static org.japy.infra.validation.Validator.check;

import org.japy.base.CompOp;
import org.japy.infra.validation.Validator;

public final class AstComparison extends AstExprNode {
  public final IAstExpr[] operands;
  public final CompOp[] ops;

  public AstComparison(IAstExpr[] operands, CompOp[] ops) {
    super(suspendFlags(operands) | constFlag(operands), first(operands), last(operands));
    this.operands = operands;
    this.ops = ops;
  }

  @Override
  public void validate(Validator v) {
    v.validate(operands);
    check(operands.length >= 2, "too few operands");
    check(operands.length == ops.length + 1, "inconsistent operands and ops");
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.COMPARISON;
  }

  @Override
  public String toString() {
    StringBuilder b = new StringBuilder(operands[0].toString());
    for (int i = 0; i != ops.length; ++i) {
      b.append(' ');
      b.append(ops[i]);
      b.append(' ');
      b.append(operands[i + 1]);
    }
    return b.toString();
  }
}
