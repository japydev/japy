package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.node.AstSuite;

public class AstFunc extends AstStmtNode implements IAstBlock {
  public final AstFuncHeader header;
  public final AstSuite body;

  public AstFunc(AstFuncHeader header, AstSuite body) {
    super(header.suspendFlags(), header.start(), body.end());
    this.header = header;
    this.body = body;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.FUNC;
  }

  @Override
  public void validate(Validator v) {
    v.validate(header);
    v.validate(body);
  }

  @Override
  public AstSuite body() {
    return body;
  }
}
