package org.japy.parser.python.ast.node;

import static org.japy.infra.util.ObjUtil.or;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.Token;

public final class AstParam extends AstNode {
  public static final AstParam[] EMPTY_ARRAY = new AstParam[0];

  public final ParamKind paramKind;
  public final AstIdentifier name;
  public final @Nullable IAstExpr dflt;
  public final @Nullable IAstExpr annotation;

  public AstParam(
      ParamKind paramKind,
      Token start,
      AstIdentifier name,
      @Nullable IAstExpr dflt,
      @Nullable IAstExpr annotation) {
    super(suspendFlags(dflt, annotation), start, or(annotation, dflt, name).end());
    this.paramKind = paramKind;
    this.name = name;
    this.dflt = dflt;
    this.annotation = annotation;
  }

  @Override
  public void validate(Validator v) {
    v.validateIfNotNull(dflt);
    v.validateIfNotNull(annotation);
  }
}
