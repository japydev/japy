package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.ITokenSpan;

public class AstPass extends AstStmtNode {
  public AstPass(ITokenSpan location) {
    super(0, location);
  }

  @Override
  public boolean isNop() {
    return true;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.PASS;
  }

  @Override
  public void validate(Validator v) {}
}
