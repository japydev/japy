package org.japy.parser.python.ast.node;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.stmt.IAstBlock;

public class AstModule extends AstNode implements IAstBlock {
  public final AstSuite body;
  public final String sourceFile;

  public AstModule(AstSuite body, String sourceFile, ITokenSpan location) {
    super(0, location);
    this.body = body;
    this.sourceFile = sourceFile;
  }

  @Override
  public void validate(Validator v) {
    v.validate(body);
  }

  @Override
  public AstSuite body() {
    return body;
  }
}
