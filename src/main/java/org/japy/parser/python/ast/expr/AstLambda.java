package org.japy.parser.python.ast.expr;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.node.AstParam;

public final class AstLambda extends AstExprNode {
  public final AstParam[] params;
  public final IAstExpr value;

  public AstLambda(Token lambda, AstParam[] params, IAstExpr value) {
    super(0, lambda, value);
    this.params = params;
    this.value = value;
  }

  @Override
  public void validate(Validator v) {
    v.validate(params);
    v.validate(value);
  }

  @Override
  public AstExprKind kind() {
    return AstExprKind.LAMBDA;
  }
}
