package org.japy.parser.python.ast.stmt;

import org.japy.infra.validation.Validator;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.ITokenSpan;

public class AstVarDecl extends AstStmtNode {
  public enum DeclKind {
    GLOBAL,
    NONLOCAL,
  }

  public final DeclKind declKind;
  public final AstIdentifier[] identifiers;

  public AstVarDecl(DeclKind declKind, AstIdentifier[] identifiers, ITokenSpan location) {
    super(0, location);
    this.declKind = declKind;
    this.identifiers = identifiers;
  }

  @Override
  public AstStmtKind kind() {
    return AstStmtKind.DECL;
  }

  @Override
  public void validate(Validator v) {
    v.validate(identifiers);
  }
}
