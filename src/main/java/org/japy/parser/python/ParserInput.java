package org.japy.parser.python;

import org.japy.infra.loc.Range;

public interface ParserInput {
  String getText(Range range);

  String sourceFile();
}
