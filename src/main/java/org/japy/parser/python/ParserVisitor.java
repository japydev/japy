package org.japy.parser.python;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.AugOp;
import org.japy.parser.python.ast.expr.*;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.*;

public interface ParserVisitor<T> {
  T build(ITokenSpan location);

  T build(IAstExpr expr);

  void visitInput(ParserInput input);

  void visitGlobal(AstIdentifier[] names, ITokenSpan location);

  void visitNonlocal(AstIdentifier[] names, ITokenSpan location);

  void visitDel(IAstExpr target, ITokenSpan location);

  void visitAugAssign(AugOp op, IAstExpr target, IAstExpr value);

  void visitAssign(IAstExpr[] targets, IAstExpr value);

  void visitAnnAssign(IAstExpr target, IAstExpr annotation, IAstExpr value);

  void visitAnnExpr(IAstExpr expr, IAstExpr annotation);

  void visitAssert(IAstExpr expr, @Nullable IAstExpr message, ITokenSpan location);

  void visitExprStmt(IAstExpr expr);

  void visitReturn(@Nullable IAstExpr value, ITokenSpan location);

  void visitRaise(@Nullable IAstExpr ex, @Nullable IAstExpr from, ITokenSpan location);

  void visitBreak(ITokenSpan location);

  void visitContinue(ITokenSpan location);

  void visitImport(AstImportedModule[] modules, ITokenSpan location);

  void visitImportFrom(AstModuleName module, IAstImportedName[] names, ITokenSpan location);

  void visitFunc(
      boolean async,
      AstDecorator[] decorators,
      AstIdentifier name,
      AstParam[] params,
      @Nullable IAstExpr retType,
      ITokenSpan location);

  void visitEndFunc(ITokenSpan location);

  void visitClass(
      AstDecorator[] decorators, AstIdentifier name, IAstArg[] supers, ITokenSpan location);

  void visitEndClass(ITokenSpan location);

  void visitPass(ITokenSpan location);

  void visitIf(IAstExpr condition, ITokenSpan location);

  void visitElif(IAstExpr condition, ITokenSpan location);

  void visitElse(ITokenSpan location);

  void visitEndIfSuite(ITokenSpan location);

  void visitEndIf(ITokenSpan location);

  void visitWhile(IAstExpr condition, ITokenSpan location);

  void visitEndWhileSuite(ITokenSpan location);

  void visitEndWhile(ITokenSpan location);

  void visitFor(boolean async, IAstExpr target, IAstExpr list, ITokenSpan location);

  void visitEndForSuite(ITokenSpan location);

  void visitEndFor(ITokenSpan location);

  void visitWith(boolean async, AstWithItem[] items, ITokenSpan location);

  void visitEndWith(ITokenSpan location);

  void visitTry(ITokenSpan location);

  void visitExcept(@Nullable IAstExpr ex, @Nullable AstIdentifier as, ITokenSpan location);

  void visitFinally(ITokenSpan location);

  void visitEndTrySuite(ITokenSpan location);

  void visitEndTry(ITokenSpan location);
}
