package org.japy.parser.python;

public interface ScannerInput {
  String sourceFile();

  boolean isUtf8();

  byte[] getBytes();
}
