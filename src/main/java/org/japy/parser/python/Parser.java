package org.japy.parser.python;

import static org.japy.infra.coll.CollUtil.last;
import static org.japy.infra.validation.Debug.dcheck;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.japy.compiler.impl.Constants;
import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.node.AstModule;
import org.japy.parser.python.impl.ExpressionParser;
import org.japy.parser.python.impl.ModuleParser;
import org.japy.parser.python.impl.Scanner;
import org.japy.parser.python.impl.ast.AstBuilder;
import org.japy.parser.python.impl.javacc.Token;

public final class Parser {
  private static <T> T parse(Scanner scanner, ParserVisitor<T> visitor) {
    return new ModuleParser<>(visitor, scanner).parse();
  }

  public static void checkExpressionSyntax(String text) {
    @SuppressWarnings("unused")
    IAstExpr ignored = parseExpression(text, Constants.INPUT);
  }

  public static IAstExpr parseExpression(String text, String sourceFile) {
    return new ExpressionParser(new Scanner(text, sourceFile)).parse();
  }

  public static IAstExpr parseExpression(byte[] text, String sourceFile) {
    return new ExpressionParser(new Scanner(text, sourceFile)).parse();
  }

  public interface AstConsumer<T> {
    T accept(AstModule module, ParserInput parserInput);
  }

  public static <T> T parse(Path path, AstConsumer<T> build) {
    return parse(ExcUtil.wrapIO(() -> new Scanner(path)), new AstBuilder<>(build));
  }

  public static <T> T parse(ScannerInput input, AstConsumer<T> build) {
    return parse(new Scanner(input), new AstBuilder<>(build));
  }

  public static <T> T parse(String text, String sourceFile, AstConsumer<T> build) {
    return parse(new Scanner(text, sourceFile), new AstBuilder<>(build));
  }

  public static <T> T parse(byte[] text, String sourceFile, AstConsumer<T> build) {
    return parse(new Scanner(text, sourceFile), new AstBuilder<>(build));
  }

  public static List<Token> tokenize(String input) {
    Scanner scanner = new Scanner(input.strip(), Constants.INPUT);
    Token first = scanner.getNextToken();
    if (Debug.ENABLED) {
      dcheck(first.kind == Token.START);
    }
    List<Token> tokens = new ArrayList<>();
    while (true) {
      Token tok = scanner.getNextToken();
      if (tok.kind == Token.EOF) {
        break;
      }
      tokens.add(tok);
    }
    if (Debug.ENABLED) {
      dcheck(!tokens.isEmpty());
      dcheck(last(tokens).kind == Token.NEWLINE);
    }
    tokens.remove(tokens.size() - 1);
    return tokens;
  }
}
