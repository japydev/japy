package org.japy.parser.formatstring;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.RFConversion;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.Location;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.exc.px.PxSyntaxError;
import org.japy.parser.ParserUtil;
import org.japy.parser.formatstring.javacc.ParseException;

public class FormatStringParser extends org.japy.parser.formatstring.javacc.FormatStringParser {
  public interface Chunk {}

  public static class Literal implements Chunk {
    public final String text;

    public Literal(String text) {
      this.text = text;
    }
  }

  public interface ArgOp {}

  public static class ArgSub implements ArgOp {
    public final int index;

    public ArgSub(int index) {
      this.index = index;
    }
  }

  public static class ArgAttr implements ArgOp {
    public final String attr;

    public ArgAttr(String attr) {
      this.attr = attr;
    }
  }

  public static class Id {
    private final @Nullable String name;
    private final int pos;

    Id(String name) {
      this.name = name;
      this.pos = -1;
    }

    Id(int pos) {
      this.name = null;
      this.pos = pos;
    }

    public boolean isPos() {
      return pos != -1;
    }

    public int pos() {
      dcheck(pos != -1);
      return pos;
    }

    public String name() {
      return dcheckNotNull(name);
    }
  }

  public static class ReplField implements Chunk {
    public final @Nullable Id id;
    public final List<ArgOp> argOps;
    public final @Nullable RFConversion conversion;
    public final List<Chunk> formatSpec;

    public ReplField(
        @Nullable Id id,
        List<ArgOp> argOps,
        @Nullable RFConversion conversion,
        List<Chunk> formatSpec) {
      this.id = id;
      this.argOps = argOps;
      this.conversion = conversion;
      this.formatSpec = formatSpec;
    }
  }

  private static class ReplFieldBuilder {
    @Nullable Id id;
    final List<ArgOp> argOps = new ArrayList<>();
    @Nullable RFConversion conversion;
    final List<Chunk> formatSpec = new ArrayList<>();

    ReplField build() {
      return new ReplField(id, argOps, conversion, formatSpec);
    }
  }

  private final List<Chunk> content = new ArrayList<>();
  private final StringBuilder scratch;
  private @Nullable ReplFieldBuilder outerReplField;
  private @Nullable ReplFieldBuilder curReplField;

  public FormatStringParser(String formatString) {
    super(new StringReader(formatString));
    scratch = new StringBuilder((int) (formatString.length() * 1.5));
  }

  public List<Chunk> parse(PyContext ctx) {
    try {
      Input();
      if (scratch.length() != 0) {
        content.add(new Literal(scratch.toString()));
      }
      return content;
    } catch (ParseException ex) {
      throw new PxSyntaxError(ex, ctx);
    }
  }

  @Override
  protected void append(String s) {
    scratch.append(s);
  }

  @Override
  protected void beginReplField() {
    if (curReplField != null) {
      if (scratch.length() != 0) {
        curReplField.formatSpec.add(new Literal(scratch()));
      }
      dcheck(outerReplField == null);
      outerReplField = curReplField;
    } else {
      if (scratch.length() != 0) {
        content.add(new Literal(scratch()));
      }
    }
    curReplField = new ReplFieldBuilder();
  }

  @Override
  protected void endReplField() {
    dcheckNotNull(curReplField);
    if (outerReplField != null) {
      outerReplField.formatSpec.add(curReplField.build());
    } else {
      content.add(curReplField.build());
    }
    curReplField = outerReplField;
    outerReplField = null;
  }

  @Override
  protected String scratch() {
    String s = scratch.toString();
    scratch.delete(0, scratch.length());
    return s;
  }

  @Override
  protected void conversion(String s) {
    dcheckNotNull(curReplField);
    switch (s) {
      case "a":
        curReplField.conversion = RFConversion.A;
        break;
      case "r":
        curReplField.conversion = RFConversion.R;
        break;
      case "s":
        curReplField.conversion = RFConversion.S;
        break;
      default:
        throw InternalErrorException.notReached();
    }
  }

  @Override
  protected void argName(String name) {
    dcheckNotNull(curReplField);
    dcheck(curReplField.id == null);
    if (!ParserUtil.isAsciiDigit(name.charAt(0))) {
      curReplField.id = new Id(name);
    } else {
      curReplField.id = new Id(ParserUtil.parseAsciiNumber(name, Location.UNKNOWN));
    }
  }

  @Override
  protected void argAttr(String attr) {
    dcheckNotNull(curReplField);
    curReplField.argOps.add(new ArgAttr(attr));
  }

  @Override
  protected void argIndex(String index) {
    dcheckNotNull(curReplField);
    curReplField.argOps.add(new ArgSub(ParserUtil.parseAsciiNumber(index, Location.UNKNOWN)));
  }

  @Override
  protected void addFormatSpec(String spec) {
    dcheckNotNull(curReplField);
    if (!spec.isEmpty()) {
      curReplField.formatSpec.add(new Literal(spec));
    }
  }
}
