package org.japy.parser;

import org.japy.compiler.SyntaxError;
import org.japy.infra.loc.Location;

public class ScannerError extends SyntaxError {
  public ScannerError(String message, Location location) {
    super(message, location);
  }

  public ScannerError(String message, Location location, Throwable cause) {
    super(message, location, cause);
  }
}
