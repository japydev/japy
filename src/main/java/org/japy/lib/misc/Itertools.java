package org.japy.lib.misc;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_ONE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_ZERO;
import static org.japy.kernel.types.coll.seq.PyTuple.EMPTY_TUPLE;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;

import java.lang.invoke.MethodHandles;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Arithmetics;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.iter.IPyIter;
import org.japy.kernel.types.coll.iter.IterLib;
import org.japy.kernel.types.coll.iter.PyEmptyIter;
import org.japy.kernel.types.coll.seq.IPyROSequence;
import org.japy.kernel.types.coll.seq.IntSlice;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxOverflowError;
import org.japy.kernel.types.exc.px.PxStopIteration;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class Itertools extends PyBuiltinModule {
  Itertools() {
    super("itertools", MethodHandles.lookup());
  }

  private static class ChainIter implements IPyIter, IPyNoValidationObj {
    private static final IPyClass TYPE =
        new PyBuiltinClass(
            "chainiter", ChainIter.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

    private final PyTuple iterables;
    private int nextIterable;
    private @Nullable IPyObj curIter;

    ChainIter(PyTuple iterables) {
      this.iterables = iterables;
    }

    @Override
    public IPyClass type() {
      return TYPE;
    }

    @Override
    public IPyObj next(PyContext ctx) {
      while (true) {
        if (curIter != null) {
          try {
            return IterLib.next(curIter, ctx);
          } catch (PxStopIteration ignored) {
            curIter = null;
          }
        }

        if (nextIterable == iterables.len()) {
          throw new PxStopIteration(ctx);
        }

        curIter = IterLib.iter(iterables.get(nextIterable++), ctx);
      }
    }
  }

  @Export
  private static IPyObj chain(
      @Param(value = "iterables", kind = ParamKind.VAR_POS) IPyObj iterables, PyContext ctx) {
    return new ChainIter((PyTuple) iterables);
  }

  private static class RepeatIter implements IPyIter, IPyNoValidationObj {
    private static final IPyClass TYPE =
        new PyBuiltinClass(
            "repeatiter", RepeatIter.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

    private final IPyObj elem;
    private final int count;
    private int next;

    RepeatIter(IPyObj elem, int count) {
      this.elem = elem;
      this.count = count;
    }

    @Override
    public IPyClass type() {
      return TYPE;
    }

    @Override
    public IPyObj next(PyContext ctx) {
      if (next == count) {
        throw new PxStopIteration(ctx);
      }
      ++next;
      return elem;
    }
  }

  private static class InfiniteRepeatIter implements IPyIter, IPyNoValidationObj {
    private static final IPyClass TYPE =
        new PyBuiltinClass(
            "infiniterepeatiter",
            RepeatIter.class,
            MethodHandles.lookup(),
            PyBuiltinClass.INTERNAL);

    private final IPyObj elem;

    InfiniteRepeatIter(IPyObj elem) {
      this.elem = elem;
    }

    @Override
    public IPyClass type() {
      return TYPE;
    }

    @Override
    public IPyObj next(PyContext ctx) {
      return elem;
    }
  }

  @Export
  private static IPyObj repeat(
      @Param("elem") IPyObj elem,
      @Param(value = "n", dflt = DEFAULT_SENTINEL) IPyObj nObj,
      PyContext ctx) {
    if (nObj == Sentinel) {
      return new InfiniteRepeatIter(elem);
    }

    int n = ArgLib.checkArgSmallNonNegInt("repeat", "n", nObj, ctx);
    if (n == 0) {
      return new PyEmptyIter();
    } else {
      return new RepeatIter(elem, n);
    }
  }

  private static class StarMapIter implements IPyIter, IPyNoValidationObj {
    private static final IPyClass TYPE =
        new PyBuiltinClass(
            "starmapiter", StarMapIter.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

    private final IPyObj func;
    private final IPyObj iter;

    private StarMapIter(IPyObj func, IPyObj seq, PyContext ctx) {
      this.func = func;
      this.iter = IterLib.iter(seq, ctx);
    }

    @Override
    public IPyClass type() {
      return TYPE;
    }

    @Override
    public IPyObj next(PyContext ctx) {
      IPyObj args = IterLib.next(iter, ctx);
      return FuncLib.call(func, Args.star(args, ctx), ctx);
    }
  }

  @Export
  private static IPyObj starmap(
      @Param("func") IPyObj func, @Param("seq") IPyObj seq, PyContext ctx) {
    return new StarMapIter(func, seq, ctx);
  }

  @Export
  private static IPyObj islice(
      @Param(value = "args", kind = ParamKind.VAR_POS) IPyObj args, PyContext ctx) {
    PyTuple argTuple = (PyTuple) args;
    IPyObj seq, start, stop, step;
    switch (argTuple.len()) {
      case 2:
        seq = argTuple.get(0);
        start = None;
        stop = argTuple.get(1);
        step = None;
        break;
      case 3:
        seq = argTuple.get(0);
        start = argTuple.get(1);
        stop = argTuple.get(2);
        step = None;
        break;
      case 4:
        seq = argTuple.get(0);
        start = argTuple.get(1);
        stop = argTuple.get(2);
        step = argTuple.get(3);
        break;
      default:
        throw PxException.typeError(
            String.format("islice takes 2, 3, or 4 arguments, got %s", argTuple.len()), ctx);
    }

    IntSlice slice =
        new IntSlice(
            CollLib.sliceIndex(start, ctx),
            CollLib.sliceIndex(stop, ctx),
            CollLib.sliceIndex(step, ctx));
    return IterLib.sliceIter(seq, slice, ctx);
  }

  private static class CountIter implements IPyIter, IPyNoValidationObj {
    private static final IPyClass TYPE =
        new PyBuiltinClass(
            "countiter", CountIter.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

    private IPyObj next;
    private final IPyObj step;

    CountIter(IPyObj start, IPyObj step) {
      this.next = start;
      this.step = step;
    }

    @Override
    public IPyClass type() {
      return TYPE;
    }

    @Override
    public IPyObj next(PyContext ctx) {
      IPyObj result = next;
      next = Arithmetics.add(next, step, ctx);
      return result;
    }
  }

  @Export
  private static IPyObj count(
      @Param(value = "start", dflt = DEFAULT_ZERO) IPyObj start,
      @Param(value = "step", dflt = DEFAULT_ONE) IPyObj step,
      PyContext ctx) {
    return new CountIter(start, step);
  }

  @Export
  private static IPyObj permutations(
      @Param("p") IPyObj list,
      @Param(value = "r", dflt = DEFAULT_SENTINEL) IPyObj subLenObj,
      PyContext ctx) {
    // TODO
    IPyROSequence seq = CollLib.unpackToSequence(list, ctx);
    int len = seq.len(ctx);
    int subLen;
    if (subLenObj != Sentinel) {
      subLen = ArgLib.checkArgSmallNonNegInt("itertools.permutations", "r", subLenObj, ctx);
      if (subLen > len) {
        throw PxException.valueError("sequence length must be not greater than " + len, ctx);
      }
    } else {
      subLen = len;
    }

    if (subLen != len) {
      throw new NotImplementedException("permutations");
    }

    switch (subLen) {
      case 0:
        return IterLib.iter(PyTuple.of(EMPTY_TUPLE), ctx);
      case 1:
        return IterLib.iter(PyTuple.of(PyTuple.of(seq.get(0, ctx))), ctx);
      case 2:
        {
          PyTuple tup = CollLib.unpack(seq, ctx);
          return IterLib.iter(PyTuple.of(tup, PyTuple.of(tup.get(1), tup.get(0))), ctx);
        }
      default:
        throw new NotImplementedException("permutations");
    }
  }

  private static class ProductIter implements IPyIter, IPyNoValidationObj {
    private static final IPyClass TYPE =
        new PyBuiltinClass(
            "productiter", ProductIter.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

    private final PyTuple[] tuples;
    private final int[] indices;
    private boolean done;

    private ProductIter(PyTuple[] tuples) {
      this.tuples = tuples;
      this.indices = new int[tuples.length];
    }

    private PyTuple makeCurTuple() {
      IPyObj[] tup = new IPyObj[tuples.length];
      for (int i = 0; i != tuples.length; ++i) {
        tup[i] = tuples[i].get(indices[i]);
      }
      return new PyTuple(tup);
    }

    private void advance() {
      for (int i = indices.length - 1; i >= 0; --i) {
        int index = indices[i];
        if (index + 1 < tuples[i].len()) {
          indices[i] = index + 1;
          return;
        }
        indices[i] = 0;
      }
      done = true;
    }

    @Override
    public IPyClass type() {
      return TYPE;
    }

    @Override
    public IPyObj next(PyContext ctx) {
      if (done) {
        throw new PxStopIteration(ctx);
      }

      PyTuple value = makeCurTuple();
      advance();
      return value;
    }
  }

  @Export
  private static IPyObj product(
      @Param(value = "iterables", kind = ParamKind.VAR_POS) IPyObj iterables,
      @Param(value = "repeat", kind = ParamKind.KW_ONLY, dflt = DEFAULT_ONE) IPyObj repeatObj,
      PyContext ctx) {
    PyTuple iterTup = (PyTuple) iterables;
    int iterableCount = iterTup.len();
    if (iterableCount == 0) {
      return new PyEmptyIter();
    }
    int repeat = ArgLib.checkArgSmallPosInt("itertools.product", "repeat", repeatObj, ctx);
    if (CollLib.checkMulOverflows(iterableCount, repeat)) {
      throw new PxOverflowError("repeat", ctx);
    }
    PyTuple[] tuples = new PyTuple[iterableCount * repeat];
    for (int i = 0; i != iterableCount; ++i) {
      tuples[i] = CollLib.unpack(iterTup.get(i), ctx);
      if (tuples[i].isEmpty()) {
        return new PyEmptyIter();
      }
    }
    for (int j = 1; j != repeat; ++j) {
      System.arraycopy(tuples, 0, tuples, j * iterableCount, iterableCount);
    }
    return new ProductIter(tuples);
  }

  @Export
  private static IPyObj filterfalse(Args args, PyContext ctx) {
    throw new NotImplementedException("filterfalse");
  }
}
