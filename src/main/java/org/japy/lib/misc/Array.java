package org.japy.lib.misc;

import java.lang.invoke.MethodHandles;

import org.japy.lib.PyBuiltinModule;

class Array extends PyBuiltinModule {
  Array() {
    super("array", MethodHandles.lookup());
  }
}
