package org.japy.lib.misc;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class AtExit extends PyBuiltinModule {
  AtExit() {
    super("atexit", MethodHandles.lookup());
  }

  @Export
  private static IPyObj register(Args args, PyContext ctx) {
    // TODO zzz
    return None;
  }
}
