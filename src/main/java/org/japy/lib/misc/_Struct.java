package org.japy.lib.misc;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class _Struct extends PyBuiltinModule {
  _Struct() {
    super("_struct", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    set(Constants.__DOC__, PyStr.get("TODO"));
  }

  @Export
  private static IPyObj _clearcache(PyContext ctx) {
    throw new NotImplementedException("_struct._clearcache");
  }

  @Export
  private static IPyObj pack(Args args, PyContext ctx) {
    throw new NotImplementedException("_struct.pack");
  }

  @Export
  private static IPyObj unpack(Args args, PyContext ctx) {
    throw new NotImplementedException("_struct.unpack");
  }

  @Export
  private static IPyObj calcsize(Args args, PyContext ctx) {
    // TODO zzz
    return PyInt.get(32);
  }

  @Export
  private static IPyObj Struct(Args args, PyContext ctx) {
    // TODO zzz
    return None;
  }
}
