package org.japy.lib.misc;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class BinAscii extends PyBuiltinModule {
  BinAscii() {
    super("binascii", MethodHandles.lookup());
  }

  @Export
  private static IPyObj crc32(Args args, PyContext ctx) {
    throw new NotImplementedException("binascii.crc32");
  }
}
