package org.japy.lib.misc;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.num.IntLib;
import org.japy.kernel.util.Constants;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class _Operator extends PyBuiltinModule {
  _Operator() {
    super("_operator", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    set(
        Constants.__DOC__,
        PyStr.get(
            "Operator interface.\n"
                + "\n"
                + "This module exports a set of functions implemented in C corresponding\n"
                + "to the intrinsic operators of Python.  For example, operator.add(x, y)\n"
                + "is equivalent to the expression x+y.  The function names are those\n"
                + "used for special methods; variants without leading and trailing\n"
                + "'__' are also provided for convenience."));
  }

  @Export
  private static IPyObj index(@Param("number") IPyObj number, PyContext ctx) {
    return IntLib.asIntExact(number, ctx);
  }
}
