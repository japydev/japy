package org.japy.lib.misc;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.num.FloatLib;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class Time extends PyBuiltinModule {
  Time() {
    super("time", MethodHandles.lookup());
  }

  // TODO zzz

  @Export
  private static IPyObj perf_counter(PyContext ctx) {
    return PyInt.ZERO;
  }

  @Export
  private static IPyObj time(Args args, PyContext ctx) {
    return PyInt.ZERO;
  }

  @Export
  private static IPyObj localtime(Args args, PyContext ctx) {
    throw new NotImplementedException("localtime");
  }

  @Export
  private static IPyObj sleep(@Param("interval") IPyObj interval, PyContext ctx) {
    double v = FloatLib.toFloat(interval, ctx).value;
    try {
      Thread.sleep((long) (v * 1000));
    } catch (InterruptedException ignored) {
      // ignore
    }
    return None;
  }
}
