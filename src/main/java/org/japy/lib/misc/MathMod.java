package org.japy.lib.misc;

import static org.japy.kernel.util.PyUtil.wrap;

import java.lang.invoke.MethodHandles;

import com.google.errorprone.annotations.Var;

import org.japy.base.ParamKind;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.num.FloatLib;
import org.japy.kernel.types.num.IntLib;
import org.japy.kernel.types.num.MathLib;
import org.japy.kernel.types.num.NumLib;
import org.japy.kernel.types.num.PyFloat;
import org.japy.kernel.types.num.PyInt;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class MathMod extends PyBuiltinModule {
  MathMod() {
    super("math", MethodHandles.lookup());
  }

  @Export(
      doc =
          "Truncates the Real x to the nearest Integral toward 0.\n"
              + "\n"
              + "Uses the __trunc__ magic method.")
  private static IPyObj trunc(@Param("number") IPyObj number, PyContext ctx) {
    return NumLib.trunc(number, ctx);
  }

  @Export(
      doc = "Return the floor of x as an Integral.\n" + "\n" + "This is the largest integer <= x.")
  private static IPyObj floor(@Param("number") IPyObj number, PyContext ctx) {
    return NumLib.floor(number, ctx);
  }

  @Export(
      doc =
          "Return the ceiling of x as an Integral.\n" + "\n" + "This is the smallest integer >= x.")
  private static IPyObj ceil(@Param("number") IPyObj number, PyContext ctx) {
    return NumLib.ceil(number, ctx);
  }

  @Export(doc = "Return True if x is a positive or negative infinity, and False otherwise.")
  private static IPyObj isinf(@Param("number") IPyObj number, PyContext ctx) {
    return wrap(number instanceof PyFloat && Double.isInfinite(((PyFloat) number).value));
  }

  @Export(doc = "Return True if x is a NaN (not a number), and False otherwise.")
  private static IPyObj isnan(@Param("number") IPyObj number, PyContext ctx) {
    return wrap(number instanceof PyFloat && Double.isNaN(((PyFloat) number).value));
  }

  @Export(
      doc =
          "Return a float with the magnitude (absolute value) of x but the sign of y.\n"
              + "\n"
              + "copysign(1.0, -0.0) returns -1.0.")
  private static IPyObj copysign(
      @Param("magnitude") IPyObj magnitude, @Param("sign") IPyObj sign, PyContext ctx) {
    return FloatLib.copysign(magnitude, sign, ctx);
  }

  @Export(doc = "Return x * (2**i).\n" + "\n" + "This is essentially the inverse of frexp().")
  private static IPyObj ldexp(@Param("x") IPyObj x, @Param("i") IPyObj i, PyContext ctx) {
    return FloatLib.ldexp(x, i, ctx);
  }

  @Export(
      doc =
          "Return the mantissa and exponent of x, as pair (m, e).\n"
              + "\n"
              + "m is a float and e is an int, such that x = m * 2.**e.\n"
              + "If x is 0, m and e are both 0.  Else 0.5 <= abs(m) < 1.0.")
  private static IPyObj frexp(@Param("x") IPyObj x, PyContext ctx) {
    return FloatLib.frexp(x, ctx);
  }

  @Export(doc = "Greatest Common Divisor")
  private static IPyObj gcd(
      @Param(value = "integers", kind = ParamKind.VAR_POS) IPyObj integers, PyContext ctx) {
    @Var PyInt gcd = PyInt.ZERO;
    @Var boolean first = true;
    for (IPyObj i : ((PyTuple) integers).__array()) {
      if (first) {
        gcd = ArgLib.checkArgInt("gcd", "i", i, ctx);
        first = false;
      } else {
        gcd = IntLib.gcd(gcd, ArgLib.checkArgInt("gcd", "i", i, ctx));
      }
    }
    return gcd;
  }

  @Export(
      doc =
          "Return the arc tangent (measured in radians) of y/x.\n"
              + "\n"
              + "Unlike atan(y/x), the signs of both x and y are considered.")
  private static IPyObj atan2(@Param("y") IPyObj y, @Param("x") IPyObj x, PyContext ctx) {
    return MathLib.atan2(y, x, ctx);
  }

  @Export(
      doc =
          "Return the arc tangent (measured in radians) of x.\n"
              + "\n"
              + "The result is between -pi/2 and pi/2.")
  private static IPyObj atan(@Param("x") IPyObj x, PyContext ctx) {
    return MathLib.atan(x, ctx);
  }
}
