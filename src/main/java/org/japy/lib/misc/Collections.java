package org.japy.lib.misc;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.dict.PyDefaultDict;
import org.japy.kernel.types.coll.seq.PyDeque;
import org.japy.lib.PyBuiltinModule;

class Collections extends PyBuiltinModule {
  Collections() {
    super("_collections", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    set("deque", PyDeque.TYPE);
    set("defaultdict", PyDefaultDict.TYPE);
  }
}
