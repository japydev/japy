package org.japy.lib.misc;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Constants;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

public class _Codecs extends PyBuiltinModule {
  _Codecs() {
    super(Constants._CODECS, MethodHandles.lookup());
  }

  // TODO zzz
  @Export
  private static IPyObj lookup_error(Args args, PyContext ctx) {
    return None;
  }

  // TODO zzz
  @Export
  private static IPyObj lookup(Args args, PyContext ctx) {
    return None;
  }

  // TODO zzz
  @Export
  private static IPyObj register(@Param("search_function") IPyObj searchFunction, PyContext ctx) {
    return None;
  }
}
