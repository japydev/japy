package org.japy.lib.misc;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class _Signal extends PyBuiltinModule {
  _Signal() {
    super("_signal", MethodHandles.lookup());
  }

  // TODO

  @Export
  private static IPyObj signal(Args args, PyContext ctx) {
    throw new NotImplementedException("signal");
  }

  @Export
  private static IPyObj getsignal(Args args, PyContext ctx) {
    throw new NotImplementedException("getsignal");
  }
}
