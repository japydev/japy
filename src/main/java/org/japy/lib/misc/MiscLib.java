package org.japy.lib.misc;

import org.japy.lib.ModuleRegistry;

public class MiscLib {
  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new _Codecs());
    moduleRegistry.register(new _Signal());
    moduleRegistry.register(new Time());
    moduleRegistry.register(new Collections());
    moduleRegistry.register(new Itertools());
    moduleRegistry.register(new MathMod());
    moduleRegistry.register(new _Struct());
    moduleRegistry.register(new BinAscii());
    moduleRegistry.register(new Array());
    moduleRegistry.register(new AtExit());
    moduleRegistry.register(new _Operator());
  }
}
