package org.japy.lib;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_EMPTY_STRING;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_EMPTY_TUPLE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_FALSE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NEG_ONE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_ZERO;
import static org.japy.kernel.types.misc.PyEllipsis.Ellipsis;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PyNotImplemented.NotImplemented;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;
import static org.japy.kernel.types.misc.PySentinel.nullIfSentinel;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;
import static org.japy.kernel.util.PyUtil.wrap;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.ExecLib;
import org.japy.kernel.exec.OptimizationLevel;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Arithmetics;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.PyObjectClass;
import org.japy.kernel.types.cls.PySuper;
import org.japy.kernel.types.cls.PyTypeClass;
import org.japy.kernel.types.cls.desc.PyClassMethod;
import org.japy.kernel.types.cls.desc.PyProperty;
import org.japy.kernel.types.cls.desc.PyStaticMethod;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.iter.IterLib;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.mem.PyMemoryView;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyRange;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.set.PyFrozenSet;
import org.japy.kernel.types.coll.set.PySet;
import org.japy.kernel.types.coll.str.ObjFormatLib;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyByteArray;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.coll.str.UnicodeLib;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.misc.PyIdObj;
import org.japy.kernel.types.misc.PySlice;
import org.japy.kernel.types.num.IntLib;
import org.japy.kernel.types.num.NumLib;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.num.PyComplex;
import org.japy.kernel.types.num.PyFloat;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.PyConstants;
import org.japy.lib.annotations.Export;
import org.japy.lib.imp.ImportLib;

class Builtins extends PyBuiltinModule {
  Builtins() {
    super("builtins", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    set("bool", PyBool.TYPE());
    set("bytearray", PyByteArray.TYPE);
    set("bytes", PyBytes.TYPE);
    set("classmethod", PyClassMethod.TYPE);
    set("complex", PyComplex.TYPE);
    set("dict", PyDict.TYPE);
    set("float", PyFloat.TYPE);
    set("frozenset", PyFrozenSet.TYPE);
    set("int", PyInt.TYPE);
    set("list", PyList.TYPE);
    set("memoryview", PyMemoryView.TYPE);
    set("object", PyObjectClass.INSTANCE);
    set("property", PyProperty.TYPE);
    set("range", PyRange.TYPE);
    set("set", PySet.TYPE);
    set("slice", PySlice.TYPE);
    set("staticmethod", PyStaticMethod.TYPE());
    set("str", PyStr.TYPE);
    set("super", PySuper.TYPE);
    set("tuple", PyTuple.TYPE);
    set("type", PyTypeClass.INSTANCE);

    set("Ellipsis", Ellipsis);
    set("NotImplemented", NotImplemented);

    exportExceptions();
  }

  private void exportExceptions() {
    for (Field field : PyBuiltinExc.class.getDeclaredFields()) {
      if (!field.getName().equals(PyBuiltinExc.JavaException.name())) {
        try {
          set(field.getName(), (IPyObj) field.get(null));
        } catch (IllegalAccessException ex) {
          throw ExcUtil.rethrow(ex);
        }
      }
    }
  }

  @Export
  private static IPyObj exec(
      @Param("object") IPyObj obj,
      @Param(value = "globals", dflt = DEFAULT_SENTINEL) IPyObj globalsObj,
      @Param(value = "locals", dflt = DEFAULT_SENTINEL) IPyObj localsObj,
      @Param(value = "__filename__", dflt = DEFAULT_SENTINEL, kind = ParamKind.KW_ONLY)
          IPyObj filenameObj,
      PyContext ctx) {
    ExecLib.exec(
        obj,
        nullIfSentinel(globalsObj),
        nullIfSentinel(localsObj),
        nullIfSentinel(filenameObj),
        ctx);
    return None;
  }

  @Export
  private static IPyObj eval(
      @Param("expression") IPyObj expr,
      @Param(value = "globals", dflt = DEFAULT_SENTINEL) IPyObj globalsObj,
      @Param(value = "locals", dflt = DEFAULT_SENTINEL) IPyObj localsObj,
      PyContext ctx) {
    return ExecLib.eval(expr, nullIfSentinel(globalsObj), nullIfSentinel(localsObj), ctx);
  }

  @Export
  private static IPyObj compile(
      @Param("source") IPyObj source,
      @Param("filename") IPyObj filenameArg,
      @Param("mode") IPyObj modeArg,
      @Param(value = "flags", dflt = DEFAULT_ZERO) IPyObj flagsArg,
      @Param(value = "dont_inherit", dflt = DEFAULT_FALSE) IPyObj dontInheritArg,
      @Param(value = "optimize", dflt = DEFAULT_NEG_ONE) IPyObj optimizeArg,
      PyContext ctx) {
    if (!(source instanceof PyStr) && !(source instanceof PyBytes)) {
      throw PxException.typeError("in compile(): 'source' must be a string or a bytes object", ctx);
    }
    PyStr filename = ArgLib.checkArgStr("compile", "filename", filenameArg, ctx);
    String modeStr = ArgLib.checkArgStr("compile", "mode", modeArg, ctx).toString();
    ExecLib.CompileMode mode;
    switch (modeStr) {
      case "exec":
        mode = ExecLib.CompileMode.EXEC;
        break;
      case "eval":
        mode = ExecLib.CompileMode.EVAL;
        break;
      case "single":
        mode = ExecLib.CompileMode.SINGLE;
        break;
      default:
        throw PxException.valueError(
            "in compile(): 'mode' must be one of 'exec', 'eval', or 'single'", ctx);
    }
    int flags = ArgLib.checkArgSmallInt("compile", "flags", flagsArg, ctx);
    boolean dontInherit = ArgLib.checkArgBool("compile", "dont_inherit", dontInheritArg, ctx);
    int optimize = ArgLib.checkArgSmallInt("compile", "optimize", optimizeArg, ctx);
    @Nullable OptimizationLevel optLevel;
    switch (optimize) {
      case -1:
        optLevel = null;
        break;
      case 0:
        optLevel = OptimizationLevel.ZERO;
        break;
      case 1:
        optLevel = OptimizationLevel.ONE;
        break;
      case 2:
        optLevel = OptimizationLevel.TWO;
        break;
      default:
        throw PxException.valueError("in compile(): 'optimize' must be one of -1, 0, 1, or 2", ctx);
    }
    return ExecLib.compile(source, filename.toString(), mode, flags, dontInherit, optLevel, ctx);
  }

  @Export
  private static IPyObj repr(@Param("object") IPyObj obj, PyContext ctx) {
    return PrintLib.repr(obj, ctx);
  }

  @Export
  private static IPyObj len(@Param("object") IPyObj obj, PyContext ctx) {
    return PyInt.get(CollLib.len(obj, ctx));
  }

  @Export
  private static IPyObj __import__(
      @Param("name") IPyObj name,
      @Param(value = "globals", dflt = DEFAULT_NONE) IPyObj globals,
      @Param(value = "locals", dflt = DEFAULT_NONE) IPyObj locals,
      @Param(value = "fromlist", dflt = DEFAULT_EMPTY_TUPLE) IPyObj fromlist,
      @Param(value = "level", dflt = DEFAULT_ZERO) IPyObj level,
      PyContext ctx) {
    return ImportLib.__import__(name, globals, locals, fromlist, level, ctx);
  }

  @Export
  private static IPyObj hasattr(
      @Param("object") IPyObj obj, @Param("attr") IPyObj attr, PyContext ctx) {
    return PyBool.of(ObjLib.hasAttr(obj, ObjLib.attrName(attr, ctx), ctx));
  }

  @Export
  private static IPyObj getattr(
      @Param("object") IPyObj obj,
      @Param("attr") IPyObj attr,
      @Param(value = "default", dflt = DEFAULT_SENTINEL) IPyObj dflt,
      PyContext ctx) {
    if (dflt != Sentinel) {
      return ObjLib.getAttrOr(obj, ObjLib.attrName(attr, ctx), dflt, ctx);
    } else {
      return ObjLib.getAttr(obj, ObjLib.attrName(attr, ctx), ctx);
    }
  }

  @Export
  private static IPyObj delattr(
      @Param("object") IPyObj obj, @Param("attr") IPyObj attr, PyContext ctx) {
    ObjLib.delAttr(obj, ObjLib.attrName(attr, ctx), ctx);
    return None;
  }

  @Export
  private static IPyObj setattr(
      @Param("object") IPyObj obj,
      @Param("attr") IPyObj attr,
      @Param("value") IPyObj value,
      PyContext ctx) {
    ObjLib.setAttr(obj, ObjLib.attrName(attr, ctx), value, ctx);
    return None;
  }

  @Export
  private static IPyObj iter(Args args, PyContext ctx) {
    ArgParser.verifyNoKwArgs(args, "iter", ctx);
    switch (args.posCount()) {
      case 1:
        return IterLib.iter(args.getPos(0), ctx);
      case 2:
        return IterLib.iter(args.getPos(0), args.getPos(1));
      default:
        throw PxException.typeError("iter() takes one or two arguments", ctx);
    }
  }

  @Export
  private static IPyObj next(@Param("iter") IPyObj obj, PyContext ctx) {
    return IterLib.next(obj, ctx);
  }

  @Export
  private static IPyObj hash(@Param("object") IPyObj obj, PyContext ctx) {
    return wrap(obj.hashCode(ctx));
  }

  @Export
  private static IPyObj enumerate(
      @Param("iterable") IPyObj obj,
      @Param(value = "start", dflt = DEFAULT_ZERO) IPyObj startObj,
      PyContext ctx) {
    int start = ArgLib.checkArgSmallInt("enumerate", "start", startObj, ctx);
    return IterLib.enumerate(obj, start, ctx);
  }

  @Export
  private static IPyObj reversed(@Param("coll") IPyObj obj, PyContext ctx) {
    return IterLib.reversed(obj, ctx);
  }

  @Export
  private static IPyObj zip(
      @Param(value = "iterables", kind = ParamKind.VAR_POS) IPyObj iterables,
      @Param(value = "strict", kind = ParamKind.KW_ONLY, dflt = DEFAULT_FALSE) IPyObj strict,
      PyContext ctx) {
    return IterLib.zip((PyTuple) iterables, ObjLib.isTrue(strict, ctx), ctx);
  }

  @Export
  private static IPyObj isinstance(
      @Param("object") IPyObj obj, @Param("classinfo") IPyObj classInfo, PyContext ctx) {
    return PyBool.of(ObjLib.isInstance(obj, classInfo, ctx));
  }

  @Export
  private static IPyObj issubclass(
      @Param("class") IPyObj cls, @Param("classinfo") IPyObj classInfo, PyContext ctx) {
    return PyBool.of(ObjLib.isSubclass(cls, classInfo, ctx));
  }

  @Export
  private static IPyObj dir(
      @Param(value = "object", dflt = DEFAULT_SENTINEL) IPyObj obj, PyContext ctx) {
    if (obj == Sentinel) {
      IPyDict locals = ctx.thread.frame().locals();
      return CollLib.sortedKeys(locals, ctx);
    } else {
      return ObjLib.dir(obj, ctx);
    }
  }

  @Export
  private static IPyObj locals(PyContext ctx) {
    return ctx.thread.frame().locals();
  }

  @Export
  private static IPyObj globals(PyContext ctx) {
    return ctx.thread.frame().globals();
  }

  @Export
  private static IPyObj open(Args args, PyContext ctx) {
    throw new NotImplementedException("open");
  }

  @Export
  private static IPyObj abs(@Param("object") IPyObj obj, PyContext ctx) {
    return Arithmetics.abs(obj, ctx);
  }

  @Export
  private static IPyObj chr(@Param("ordinal") IPyObj ordinal, PyContext ctx) {
    return UnicodeLib.chr(ordinal, ctx);
  }

  @Export
  private static IPyObj map(
      @Param("function") IPyObj func,
      @Param(value = "iterables", kind = ParamKind.VAR_POS) IPyObj iterables,
      PyContext ctx) {
    return IterLib.map(func, (PyTuple) iterables, ctx);
  }

  @Export
  private static IPyObj any(@Param("iterable") IPyObj iterable, PyContext ctx) {
    for (IPyObj v : new JavaIterable(iterable, ctx)) {
      if (ObjLib.isTrue(v, ctx)) {
        return True;
      }
    }
    return False;
  }

  @Export
  private static IPyObj all(@Param("iterable") IPyObj iterable, PyContext ctx) {
    for (IPyObj v : new JavaIterable(iterable, ctx)) {
      if (!ObjLib.isTrue(v, ctx)) {
        return False;
      }
    }
    return True;
  }

  @Export
  private static IPyObj callable(@Param("object") IPyObj obj, PyContext ctx) {
    return FuncLib.callable(obj);
  }

  @Export
  private static IPyObj sorted(
      @Param("iterable") IPyObj iterable,
      @Param(value = "key", dflt = DEFAULT_NONE, kind = ParamKind.KW_ONLY) IPyObj key,
      @Param(value = "reverse", dflt = DEFAULT_FALSE, kind = ParamKind.KW_ONLY) IPyObj reverse,
      PyContext ctx) {
    return CollLib.sorted(iterable, key, ObjLib.isTrue(reverse, ctx), ctx);
  }

  @Export
  private static IPyObj vars(
      @Param(value = "object", dflt = DEFAULT_SENTINEL) IPyObj obj, PyContext ctx) {
    return obj != Sentinel ? ObjLib.vars(obj, ctx) : locals(ctx);
  }

  @Export
  private static IPyObj divmod(@Param("a") IPyObj a, @Param("b") IPyObj b, PyContext ctx) {
    return Arithmetics.divmod(a, b, ctx);
  }

  @Export
  private static IPyObj round(
      @Param("number") IPyObj number,
      @Param(value = "ndigits", dflt = DEFAULT_SENTINEL, kind = ParamKind.POS_OR_KW) IPyObj ndigits,
      PyContext ctx) {
    if (ndigits == Sentinel) {
      return NumLib.round(number, ctx);
    } else {
      return NumLib.round(number, ndigits, ctx);
    }
  }

  @SuppressWarnings({"UseOfSystemOutOrSystemErr", "SystemOut"})
  @Export
  private static IPyObj print(
      @Param(value = "objects", kind = ParamKind.VAR_POS) IPyObj objects,
      @Param(value = "sep", kind = ParamKind.KW_ONLY, defaultStr = " ") IPyObj sepObj,
      @Param(value = "end", kind = ParamKind.KW_ONLY, defaultStr = "\n") IPyObj endObj,
      @Param(value = "file", kind = ParamKind.KW_ONLY, dflt = DEFAULT_SENTINEL) IPyObj file,
      @Param(value = "flush", kind = ParamKind.KW_ONLY, dflt = DEFAULT_FALSE) IPyObj flushObj,
      PyContext ctx) {
    // TODO
    if (file != Sentinel) {
      throw new NotImplementedException("print");
    }
    PyStr sep =
        sepObj != None ? ArgLib.checkArgStr("print", "sep", sepObj, ctx) : PyConstants.SPACE;
    PyStr end = endObj != None ? ArgLib.checkArgStr("print", "end", endObj, ctx) : PyConstants.NL;
    boolean flush = ArgLib.checkArgBool("print", "flush", flushObj, ctx);
    PyTuple objTup = (PyTuple) objects;
    int count = objTup.len();
    for (int i = 0; i != count; ++i) {
      if (i != 0) {
        System.out.print(sep);
      }
      System.out.print(PrintLib.str(objTup.get(i), ctx));
    }
    System.out.print(end);
    return None;
  }

  private static final ArgParser MINMAX_ARG_PARSER_ITERABLE =
      ArgParser.builder(3)
          .add("iterable", ParamKind.POS_ONLY)
          .add("key", ParamKind.KW_ONLY, None)
          .add("default", ParamKind.KW_ONLY, None)
          .build();

  private static final ArgParser MINMAX_ARG_PARSER_ARGS =
      ArgParser.builder(2)
          .add("args", ParamKind.VAR_POS)
          .add("key", ParamKind.KW_ONLY, None)
          .build();

  @Export
  private static IPyObj min(Args args, PyContext ctx) {
    switch (args.posCount()) {
      case 0:
        throw PxException.typeError("in min: at least one positional argument is required", ctx);
      case 1:
        {
          IPyObj[] argVals = MINMAX_ARG_PARSER_ITERABLE.parse(args, "min", ctx);
          return NumLib.min(argVals[0], argVals[1], argVals[2], ctx);
        }
      default:
        {
          IPyObj[] argVals = MINMAX_ARG_PARSER_ARGS.parse(args, "min", ctx);
          return NumLib.min((PyTuple) argVals[0], argVals[1], ctx);
        }
    }
  }

  @Export
  private static IPyObj max(Args args, PyContext ctx) {
    switch (args.posCount()) {
      case 0:
        throw PxException.typeError("in max: at least one positional argument is required", ctx);
      case 1:
        {
          IPyObj[] argVals = MINMAX_ARG_PARSER_ITERABLE.parse(args, "max", ctx);
          return NumLib.max(argVals[0], argVals[1], argVals[2], ctx);
        }
      default:
        {
          IPyObj[] argVals = MINMAX_ARG_PARSER_ARGS.parse(args, "max", ctx);
          return NumLib.max((PyTuple) argVals[0], argVals[1], ctx);
        }
    }
  }

  @Export
  private static IPyObj filter(
      @Param("function") IPyObj function, @Param("iterable") IPyObj iterable, PyContext ctx) {
    return IterLib.filter(function, iterable, ctx);
  }

  @Export
  private static IPyObj sum(
      @Param("iterable") IPyObj iterable,
      @Param(value = "start", kind = ParamKind.POS_OR_KW, dflt = DEFAULT_ZERO) IPyObj start,
      PyContext ctx) {
    if (start instanceof PyStr) {
      throw PxException.typeError("start may not be a string", ctx);
    }
    @Var IPyObj sum = start;
    for (IPyObj elem : new JavaIterable(iterable, ctx)) {
      sum = Arithmetics.add(sum, elem, ctx);
    }
    return sum;
  }

  @Export
  private static IPyObj id(@Param("object") IPyObj obj, PyContext ctx) {
    return new PyIdObj(obj);
  }

  @Export
  private static IPyObj ord(@Param("char") IPyObj chrArg, PyContext ctx) {
    PyStr chr = ArgLib.checkArgStr("ord", "char", chrArg, ctx);
    return chr.ord(ctx);
  }

  @Export
  private static IPyObj format(
      @Param("value") IPyObj value,
      @Param(value = "format_spec", dflt = DEFAULT_EMPTY_STRING) IPyObj formatSpec,
      PyContext ctx) {
    return ObjFormatLib.format(
        value, ArgLib.checkArgStr("format", "format_spec", formatSpec, ctx), ctx);
  }

  @Export
  private static IPyObj pow(
      @Param("base") IPyObj base,
      @Param("exp") IPyObj exp,
      @Param(value = "mod", dflt = DEFAULT_NONE) IPyObj mod,
      PyContext ctx) {
    if (mod != None) {
      return Arithmetics.pow(base, exp, mod, ctx);
    } else {
      return Arithmetics.pow(base, exp, ctx);
    }
  }

  @Export
  private static IPyObj bin(@Param("number") IPyObj n, PyContext ctx) {
    return IntLib.bin(n, ctx);
  }

  @Export
  private static IPyObj hex(@Param("number") IPyObj n, PyContext ctx) {
    return IntLib.hex(n, ctx);
  }

  @Export
  private static IPyObj oct(@Param("number") IPyObj n, PyContext ctx) {
    return IntLib.oct(n, ctx);
  }
}
