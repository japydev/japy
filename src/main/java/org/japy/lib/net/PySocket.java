package org.japy.lib.net;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.Args;

public class PySocket implements IPyTrueAtomObj, IPyNoValidationObj {
  static final IPyClass TYPE =
      new PyBuiltinClass(
          "_socket.socket", PySocket.class, MethodHandles.lookup(), PyBuiltinClass.SUBCLASSABLE);

  @SubClassConstructor
  protected PySocket(Args args, PyContext ctx) {
    // TODO zzz
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
