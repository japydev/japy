package org.japy.lib.net;

import org.japy.lib.ModuleRegistry;

public class NetLib {
  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new _Socket());
    moduleRegistry.register(new Select());
  }
}
