package org.japy.lib.net;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.num.PyInt;
import org.japy.lib.PyBuiltinModule;

class _Socket extends PyBuiltinModule {
  _Socket() {
    super("_socket", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    set("socket", PySocket.TYPE);

    // TODO zzz
    set("AF_INET", PyInt.ZERO);
    set("AF_UNIX", PyInt.ZERO);
    set("AF_UNSPEC", PyInt.ZERO);
    set("AI_PASSIVE", PyInt.ZERO);
    set("SOCK_STREAM", PyInt.ZERO);
    set("SOCK_DGRAM", PyInt.ZERO);
    set("SOCK_RAW", PyInt.ZERO);
  }
}
