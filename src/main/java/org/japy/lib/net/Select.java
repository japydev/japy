package org.japy.lib.net;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class Select extends PyBuiltinModule {
  Select() {
    super("select", MethodHandles.lookup());
  }

  @Export
  private IPyObj select(Args args, PyContext ctx) {
    throw new NotImplementedException("select");
  }
}
