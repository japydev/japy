package org.japy.lib;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;
import static org.japy.kernel.types.coll.seq.PyTuple.EMPTY_TUPLE;
import static org.japy.kernel.types.coll.str.PyStr.EMPTY_STRING;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;
import static org.japy.kernel.util.PyUtil.wrap;
import static org.japy.kernel.util.PyUtil.wrapUcs2;

import java.lang.invoke.MethodHandles;

import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.SysLib;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.HashLib;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyNamedTuple;
import org.japy.kernel.types.coll.seq.PyNamedTupleClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxSystemExit;
import org.japy.kernel.types.mod.IPyModule;
import org.japy.kernel.types.num.PyInt;
import org.japy.lib.annotations.Export;
import org.japy.lib.annotations.Platform;
import org.japy.lib.imp.ImportLib;
import org.japy.lib.io.IOLib;

public class SysMod extends PyBuiltinModule implements IPyModule {
  private final ModuleRegistry moduleRegistry;

  SysMod(ModuleRegistry moduleRegistry) {
    super("sys", MethodHandles.lookup());
    this.moduleRegistry = moduleRegistry;
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    ImportLib.setupSys(this, moduleRegistry, ctx);

    set("maxsize", wrap(Integer.MAX_VALUE));
    set("byteorder", wrapUcs2("little"));
    set("implementation", SysLib.implementation(ctx));
    set("flags", makeSysFlags(ctx));
    set("hash_info", HashLib.hashInfo());
    // TODO zzz
    set("version_info", PyTuple.of(wrap(3), wrap(9), wrap(10)));

    set("stdout", IOLib.stdout());
    set("stderr", IOLib.stderr());

    // TODO zzz
    set("base_prefix", PyStr.get(System.getProperty("user.dir")));
    set("platform", wrapUcs2("java"));
    set("executable", EMPTY_STRING);
    set("warnoptions", EMPTY_TUPLE);
    set("argv", PyList.of(wrapUcs2("japy")));
  }

  private static final PyNamedTupleClass SYS_FLAGS_TYPE =
      new PyNamedTupleClass(
          "sys.flags",
          "debug",
          "inspect",
          "interactive",
          "isolated",
          "optimize",
          "dont_write_bytecode",
          "no_user_site",
          "no_site",
          "ignore_environment",
          "verbose",
          "bytes_warning",
          "quiet",
          "hash_randomization",
          "dev_mode",
          "utf8_mode");

  private IPyObj makeSysFlags(PyContext ctx) {
    return new PyNamedTuple(
        SYS_FLAGS_TYPE,
        False /*debug*/,
        False /*inspect*/,
        True /*interactive*/,
        False /*isolated*/,
        wrap(ctx.kernel.config.optimizationLevel.value) /*optimize*/,
        False /*dont_write_bytecode*/,
        False /*no_user_site*/,
        False /*no_site*/,
        False /*ignore_environment*/,
        False /*verbose*/,
        False /*bytes_warning*/,
        False /*quiet*/,
        False /*hash_randomization*/,
        wrap(Debug.ENABLED) /*dev_mode*/,
        True /*utf8_mode*/);
  }

  @Export
  private static IPyObj exc_info(PyContext ctx) {
    return SysLib.excInfo(ctx);
  }

  private static final PyNamedTupleClass WINDOWS_VERSION_TYPE =
      new PyNamedTupleClass(
          "windowsversion", "major", "minor", "build", "platform", "service_pack");

  @Export(platform = Platform.WINDOWS)
  private static IPyObj getwindowsversion(PyContext ctx) {
    // TODO
    // major=10, minor=0, build=22000, platform=2, service_pack=''
    return new PyNamedTuple(
        WINDOWS_VERSION_TYPE,
        PyInt.get(10),
        PyInt.ZERO,
        PyInt.get(22000),
        PyInt.get(2),
        EMPTY_STRING);
  }

  @Export
  private static IPyObj getfilesystemencoding(PyContext ctx) {
    return SysLib.getFilesystemEncoding(ctx);
  }

  @Export
  private static IPyObj getfilesystemencodeerrors(PyContext ctx) {
    return SysLib.getFilesystemEncodeErrors(ctx);
  }

  @Export
  private static IPyObj intern(@Param("string") IPyObj str, PyContext ctx) {
    // TODO
    return str;
  }

  @Export
  private static IPyObj getrecursionlimit(PyContext ctx) {
    // TODO
    return wrap(8);
  }

  @Export
  private static IPyObj exit(
      @Param(value = "status", dflt = DEFAULT_NONE) IPyObj status, PyContext ctx) {
    throw new PxSystemExit(status, ctx);
  }
}
