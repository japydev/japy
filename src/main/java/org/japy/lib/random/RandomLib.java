package org.japy.lib.random;

import org.japy.lib.ModuleRegistry;

public class RandomLib {
  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new RandomMod());
  }
}
