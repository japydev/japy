package org.japy.lib.random;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;
import java.util.Random;

import com.google.errorprone.annotations.Var;

import org.japy.base.ParamKind;
import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxIndexError;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.num.PyFloat;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class RandomMod extends PyBuiltinModule {
  private final Random random = new Random();

  RandomMod() {
    super("random", MethodHandles.lookup());
  }

  // TODO zzz

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);
  }

  @Export
  private IPyObj Random(PyContext ctx) {
    throw new NotImplementedException("Random");
  }

  @Export
  private IPyObj random(PyContext ctx) {
    return new PyFloat(random.nextDouble());
  }

  @Export
  private IPyObj randint(@Param("a") IPyObj arg1, @Param("b") IPyObj arg2, PyContext ctx) {
    int a = ArgLib.checkArgSmallInt("randint", "a", arg1, ctx);
    int b = ArgLib.checkArgSmallInt("randint", "b", arg2, ctx);
    if (a > b) {
      throw PxException.valueError("empty range", ctx);
    } else if (a == b) {
      return PyInt.get(a);
    } else if ((long) b - a + 1 <= Integer.MAX_VALUE) {
      return PyInt.get(random.nextInt(b - a + 1) + a);
    } else {
      throw new NotImplementedException("random");
    }
  }

  @Export
  private IPyObj randrange(
      @Param(value = "args", kind = ParamKind.VAR_POS) IPyObj args, PyContext ctx) {
    PyTuple argTup = (PyTuple) args;
    @Var IPyObj startArg = None;
    IPyObj stopArg;
    @Var IPyObj stepArg = None;
    switch (argTup.len()) {
      case 1:
        stopArg = argTup.get(0);
        break;
      case 2:
        startArg = argTup.get(0);
        stopArg = argTup.get(1);
        break;
      case 3:
        startArg = argTup.get(0);
        stopArg = argTup.get(1);
        stepArg = argTup.get(2);
        break;
      default:
        throw PxException.typeError("randrange() takes 1, 2, or 3 arguments", ctx);
    }

    if (stepArg != None) {
      throw new NotImplementedException("randrange");
    }
    int start = startArg == None ? 0 : ArgLib.checkArgSmallInt("randrange", "start", startArg, ctx);
    int stop = ArgLib.checkArgSmallInt("randrange", "stop", stopArg, ctx);
    if (start >= stop) {
      throw PxException.valueError("empty range", ctx);
    } else if ((long) stop - start <= Integer.MAX_VALUE) {
      return PyInt.get(random.nextInt(stop - start) + start);
    } else {
      throw new NotImplementedException("randrange");
    }
  }

  @Export
  private IPyObj choice(@Param("seq") IPyObj seq, PyContext ctx) {
    int len = CollLib.len(seq, ctx);
    if (len == 0) {
      throw new PxIndexError("sequence is empty", ctx);
    }
    return CollLib.getItem(seq, PyInt.get(random.nextInt(len)), ctx);
  }

  @Export
  private IPyObj shuffle(Args args, PyContext ctx) {
    throw new NotImplementedException("shuffle");
  }
}
