package org.japy.lib.os;

class Constants {
  static final int S_IFDIR = 0x4000; // directory
  static final int S_IFCHR = 0x2000; // character device
  static final int S_IFBLK = 0x6000; // block device
  static final int S_IFREG = 0x8000; // regular file
  static final int S_IFIFO = 0x1000; // fifo (named pipe)
  static final int S_IFLNK = 0xa000; // symbolic link
  static final int S_IFSOCK = 0xc000; // socket file

  static final int S_ISUID = 0x800; // set UID bit
  static final int S_ISGID = 0x400; // set GID bit
  static final int S_ENFMT = S_ISGID; // file locking enforcement
  static final int S_ISVTX = 0x200; // sticky bit
  static final int S_IREAD = 0x100; // Unix V7 synonym for S_IRUSR
  static final int S_IWRITE = 0x080; // Unix V7 synonym for S_IWUSR
  static final int S_IEXEC = 0x040; // Unix V7 synonym for S_IXUSR
  static final int S_IRWXU = 0x1c0; // mask for owner permissions
  static final int S_IRUSR = 0x100; // read by owner
  static final int S_IWUSR = 0x080; // write by owner
  static final int S_IXUSR = 0x040; // execute by owner
  static final int S_IRWXG = 0x038; // mask for group permissions
  static final int S_IRGRP = 0x020; // read by group
  static final int S_IWGRP = 0x010; // write by group
  static final int S_IXGRP = 0x008; // execute by group
  static final int S_IRWXO = 0x007; // mask for others (not in group) permissions
  static final int S_IROTH = 0x004; // read by others
  static final int S_IWOTH = 0x002; // write by others
  static final int S_IXOTH = 0x001; // execute by others

  static final int FILE_ATTRIBUTE_ARCHIVE = 32;
  static final int FILE_ATTRIBUTE_COMPRESSED = 2048;
  static final int FILE_ATTRIBUTE_DEVICE = 64;
  static final int FILE_ATTRIBUTE_DIRECTORY = 16;
  static final int FILE_ATTRIBUTE_ENCRYPTED = 16384;
  static final int FILE_ATTRIBUTE_HIDDEN = 2;
  static final int FILE_ATTRIBUTE_INTEGRITY_STREAM = 32768;
  static final int FILE_ATTRIBUTE_NORMAL = 128;
  static final int FILE_ATTRIBUTE_NOT_CONTENT_INDEXED = 8192;
  static final int FILE_ATTRIBUTE_NO_SCRUB_DATA = 131072;
  static final int FILE_ATTRIBUTE_OFFLINE = 4096;
  static final int FILE_ATTRIBUTE_READONLY = 1;
  static final int FILE_ATTRIBUTE_REPARSE_POINT = 1024;
  static final int FILE_ATTRIBUTE_SPARSE_FILE = 512;
  static final int FILE_ATTRIBUTE_SYSTEM = 4;
  static final int FILE_ATTRIBUTE_TEMPORARY = 256;
  static final int FILE_ATTRIBUTE_VIRTUAL = 65536;
}
