package org.japy.lib.os;

enum WindowsAttr {
  st_file_attributes,
  st_reparse_tag,
}
