package org.japy.lib.os;

import java.lang.invoke.MethodHandles;

class Posix extends BaseOs {
  Posix() {
    super("posix", MethodHandles.lookup());
  }
}
