package org.japy.lib.os;

import java.lang.invoke.MethodHandles;

class Nt extends BaseOs {
  Nt() {
    super("nt", MethodHandles.lookup());
  }
}
