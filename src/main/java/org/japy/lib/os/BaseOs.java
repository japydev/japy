package org.japy.lib.os;

import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_TRUE;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.util.PyUtil.wrap;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.DosFileAttributes;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.PosixFileAttributes;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.PlatformLib;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.seq.PyNamedTupleClass;
import org.japy.kernel.types.coll.set.PyFrozenSet;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

abstract class BaseOs extends PyBuiltinModule {
  protected @Nullable PyNamedTupleClass statResultClass;

  protected BaseOs(String name, MethodHandles.Lookup lookup) {
    super(name, lookup);
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    statResultClass = PyStatResult.makeClass();
    set("stat_result", statResultClass);

    // TODO zzz
    set("_have_functions", new PyFrozenSet());
    set("environ", new PyDict());
    set("F_OK", wrap(1));
    set("X_OK", wrap(2));
    set("terminal_size", new PyNamedTupleClass("os.terminal_size", "columns", "lines"));
  }

  private static final LinkOption[] LINK_OPTIONS_FOLLOW = new LinkOption[0];
  private static final LinkOption[] LINK_OPTIONS_NO_FOLLOW =
      new LinkOption[] {LinkOption.NOFOLLOW_LINKS};

  private static IPyObj fileTimeToSeconds(FileTime time) {
    return None;
  }

  private static IPyObj fileTimeToNanoseconds(FileTime time) {
    return None;
  }

  private PyStatResult.Builder getBasicFileAttributes(BasicFileAttributes attrs) {
    PyStatResult.Builder b = PyStatResult.builder(dcheckNotNull(statResultClass));

    b.set(CommonAttr.st_size, wrap(attrs.size()));

    FileTime mtime = attrs.lastModifiedTime();
    b.set(CommonAttr.st_mtime, fileTimeToSeconds(mtime));
    b.set(CommonAttr.st_mtime_ns, fileTimeToNanoseconds(mtime));

    FileTime atime = attrs.lastAccessTime();
    b.set(CommonAttr.st_atime, fileTimeToSeconds(atime));
    b.set(CommonAttr.st_atime_ns, fileTimeToNanoseconds(atime));

    FileTime ctime = attrs.creationTime();
    b.set(CommonAttr.st_ctime, fileTimeToSeconds(ctime));
    b.set(CommonAttr.st_ctime_ns, fileTimeToNanoseconds(ctime));

    return b;
  }

  private IPyObj makeStatResult(PosixFileAttributes attrs) {
    PyStatResult.Builder b = getBasicFileAttributes(attrs);
    throw new NotImplementedException("makeStatResult");
  }

  private IPyObj makeStatResult(DosFileAttributes attrs) {
    PyStatResult.Builder b = getBasicFileAttributes(attrs);
    @Var int mode = 0;
    @Var int fileAttributes = 0;
    if (attrs.isRegularFile()) {
      mode |= Constants.S_IFREG;
      fileAttributes |= Constants.FILE_ATTRIBUTE_NORMAL;
    }
    if (attrs.isDirectory()) {
      mode |= Constants.S_IFDIR;
      fileAttributes |= Constants.FILE_ATTRIBUTE_DIRECTORY;
    }
    if (attrs.isSymbolicLink()) {
      mode |= Constants.S_IFLNK;
      fileAttributes |= Constants.FILE_ATTRIBUTE_REPARSE_POINT;
    }
    if (attrs.isReadOnly()) {
      fileAttributes |= Constants.FILE_ATTRIBUTE_READONLY;
    }
    if (attrs.isArchive()) {
      fileAttributes |= Constants.FILE_ATTRIBUTE_ARCHIVE;
    }
    if (attrs.isHidden()) {
      fileAttributes |= Constants.FILE_ATTRIBUTE_HIDDEN;
    }
    if (attrs.isSystem()) {
      fileAttributes |= Constants.FILE_ATTRIBUTE_SYSTEM;
    }
    mode |= Constants.S_IRWXU | Constants.S_IRWXG | Constants.S_IRWXO;

    b.set(CommonAttr.st_mode, wrap(mode));
    b.set(WindowsAttr.st_file_attributes, wrap(fileAttributes));

    b.set(WindowsAttr.st_reparse_tag, PyInt.ZERO);
    b.set(CommonAttr.st_nlink, PyInt.ONE);
    b.set(CommonAttr.st_uid, PyInt.ZERO);
    b.set(CommonAttr.st_gid, PyInt.ZERO);
    b.set(CommonAttr.st_ino, PyInt.ZERO);
    b.set(CommonAttr.st_dev, PyInt.ZERO);

    return b.build();
  }

  private static RuntimeException pxFromIOException(IOException ex) {
    // TODO zzz
    throw ExcUtil.rethrow(ex);
  }

  @Export
  protected IPyObj stat(
      @Param("path") IPyObj pathObj,
      @Param(value = "follow_symlinks", kind = ParamKind.KW_ONLY, dflt = DEFAULT_TRUE)
          IPyObj followSymlinksObj,
      PyContext ctx) {
    if (!(pathObj instanceof PyStr)) {
      throw new NotImplementedException("stat");
    }
    PyStr pathString = (PyStr) pathObj;
    boolean followSymlinks =
        ArgLib.checkArgBool("os.stat", "follow_symlinks", followSymlinksObj, ctx);

    LinkOption[] linkOptions = followSymlinks ? LINK_OPTIONS_FOLLOW : LINK_OPTIONS_NO_FOLLOW;
    Path path = Paths.get(pathString.toString());

    try {
      if (PlatformLib.isWindows()) {
        return makeStatResult(Files.readAttributes(path, DosFileAttributes.class, linkOptions));
      } else {
        return makeStatResult(Files.readAttributes(path, PosixFileAttributes.class, linkOptions));
      }
    } catch (IOException ex) {
      throw pxFromIOException(ex);
    }
  }

  @Export
  protected static IPyObj listdir(
      @Param(value = "path", defaultStr = ".") IPyObj pathObj, PyContext ctx) {
    if (!(pathObj instanceof PyStr)) {
      throw new NotImplementedException("listdir");
    }
    String @Nullable [] files = new File(pathObj.toString()).list();
    if (files == null) {
      // TODO zzz
      throw new UncheckedIOException(new IOException("not a dir"));
    }
    return CollLib.listOf(files);
  }

  @Export
  protected static IPyObj open(Args args, PyContext ctx) {
    throw new NotImplementedException("open");
  }

  @Export
  protected static IPyObj unlink(Args args, PyContext ctx) {
    throw new NotImplementedException("unlink");
  }

  @Export
  protected static IPyObj rmdir(Args args, PyContext ctx) {
    throw new NotImplementedException("rmdir");
  }

  @Export
  protected static IPyObj getcwd(PyContext ctx) {
    return PyStr.get(System.getProperty("user.dir"));
  }

  @Export
  protected static IPyObj getpid(PyContext ctx) {
    return PyInt.get(ProcessHandle.current().pid());
  }
}
