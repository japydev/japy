package org.japy.lib.os;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.util.PyConstants;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class PlatformMod extends PyBuiltinModule {
  PlatformMod() {
    super("platform", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    // TODO zzz
    set("system", None);
  }

  @Export
  private static IPyObj python_implementation(PyContext ctx) {
    return PyConstants.IMPL_NAME_CAP;
  }
}
