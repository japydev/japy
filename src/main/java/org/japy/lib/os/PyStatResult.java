package org.japy.lib.os;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;

import org.japy.infra.validation.Debug;
import org.japy.kernel.misc.PlatformLib;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.seq.PyNamedTuple;
import org.japy.kernel.types.coll.seq.PyNamedTupleClass;

class PyStatResult extends PyNamedTuple {
  PyStatResult(PyNamedTupleClass cls, IPyObj[] values) {
    super(cls, values);
  }

  static PyNamedTupleClass makeClass() {
    List<String> fields =
        Arrays.stream(CommonAttr.values()).map(Enum::name).collect(Collectors.toList());

    if (PlatformLib.isWindows()) {
      fields.addAll(
          Arrays.stream(WindowsAttr.values()).map(Enum::name).collect(Collectors.toList()));
    }

    return new PyNamedTupleClass("os.statresult", fields.toArray(String[]::new));
  }

  static Builder builder(PyNamedTupleClass cls) {
    return new Builder(cls);
  }

  @CanIgnoreReturnValue
  static class Builder {
    private final PyNamedTupleClass cls;
    private final IPyObj[] values;

    private Builder(PyNamedTupleClass cls) {
      this.cls = cls;
      this.values = new IPyObj[cls.len()];
    }

    Builder set(CommonAttr attr, IPyObj value) {
      values[attr.ordinal()] = value;
      return this;
    }

    Builder set(WindowsAttr attr, IPyObj value) {
      values[CommonAttr.values().length + attr.ordinal()] = value;
      return this;
    }

    @CheckReturnValue
    PyStatResult build() {
      if (Debug.ENABLED) {
        dcheck(Arrays.stream(values).noneMatch(Objects::isNull));
      }

      return new PyStatResult(cls, values);
    }
  }
}
