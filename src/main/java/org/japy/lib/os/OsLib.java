package org.japy.lib.os;

import org.japy.kernel.misc.PlatformLib;
import org.japy.lib.ModuleRegistry;

public class OsLib {
  public static void register(ModuleRegistry moduleRegistry) {
    if (PlatformLib.isWindows()) {
      moduleRegistry.register(new Nt());
    } else {
      moduleRegistry.register(new Posix());
    }
    moduleRegistry.register(new PlatformMod());
  }
}
