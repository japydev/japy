package org.japy.lib.os;

enum CommonAttr {
  st_mode,
  st_ino,
  st_dev,
  st_nlink,
  st_uid,
  st_gid,
  st_size,
  st_atime,
  st_mtime,
  st_ctime,
  st_atime_ns,
  st_mtime_ns,
  st_ctime_ns,
}
