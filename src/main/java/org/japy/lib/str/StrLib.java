package org.japy.lib.str;

import org.japy.lib.ModuleRegistry;

public class StrLib {
  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new _String());
  }
}
