package org.japy.lib.str;

import java.lang.invoke.MethodHandles;

import org.japy.lib.PyBuiltinModule;

class _String extends PyBuiltinModule {
  _String() {
    super("_string", MethodHandles.lookup());
  }
}
