package org.japy.lib.subprocess;

import org.japy.lib.ModuleRegistry;

public class SubProcessLib {
  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new SubProcess());
  }
}
