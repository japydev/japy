package org.japy.lib.subprocess;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.num.PyInt;
import org.japy.lib.PyBuiltinModule;

class SubProcess extends PyBuiltinModule {
  SubProcess() {
    super("subprocess", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    // TODO zzz
    set("PIPE", PyInt.ZERO);
    set("STDOUT", PyInt.ZERO);
    set("DEVNULL", PyInt.ZERO);
  }
}
