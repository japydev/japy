package org.japy.lib.context;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyAtomObj;

class PyContextToken implements IPyAtomObj, IPyNoValidationObj {
  static final IPyClass TYPE =
      new PyBuiltinClass(
          "_contextvars.Token",
          PyContextToken.class,
          MethodHandles.lookup(),
          PyBuiltinClass.INTERNAL);

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
