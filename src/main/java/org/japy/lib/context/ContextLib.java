package org.japy.lib.context;

import org.japy.lib.ModuleRegistry;

public class ContextLib {
  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new _Contextvars());
  }
}
