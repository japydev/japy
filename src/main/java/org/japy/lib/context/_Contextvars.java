package org.japy.lib.context;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class _Contextvars extends PyBuiltinModule {
  _Contextvars() {
    super("_contextvars", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    set("Context", PyContextObj.TYPE);
    set("ContextVar", PyContextVar.TYPE);
    set("Token", PyContextToken.TYPE);
  }

  @Export
  private static IPyObj copy_context(Args args, PyContext ctx) {
    throw new NotImplementedException("copy_context");
  }
}
