package org.japy.lib.context;

import static org.japy.kernel.types.misc.PySentinel.Sentinel;
import static org.japy.kernel.types.misc.PySentinel.nullIfSentinel;

import java.lang.invoke.MethodHandles;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.ParamKind;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyAtomObj;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

class PyContextVar implements IPyAtomObj, IPyNoValidationObj {
  static final IPyClass TYPE =
      new PyBuiltinClass(
          "_contextvars.ContextVar",
          PyContextVar.class,
          MethodHandles.lookup(),
          PyBuiltinClass.CALLABLE);

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(2).add("name").add("default", ParamKind.KW_ONLY, Sentinel).build();

  private final String name;
  private final @Nullable IPyObj dflt;

  @Constructor
  PyContextVar(Args args, PyContext ctx) {
    IPyObj[] argVals = ARG_PARSER.parse(args, "ContextVar", ctx);
    name = ArgLib.checkArgStr("ContextVar", "name", argVals[0], ctx).toString();
    dflt = nullIfSentinel(argVals[1]);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
