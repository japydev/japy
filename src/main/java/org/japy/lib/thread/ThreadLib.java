package org.japy.lib.thread;

import org.japy.lib.ModuleRegistry;

public class ThreadLib {
  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new _Thread());
  }
}
