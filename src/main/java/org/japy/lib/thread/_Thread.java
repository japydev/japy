package org.japy.lib.thread;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class _Thread extends PyBuiltinModule {
  _Thread() {
    super("_thread", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    set("RLock", RLock.TYPE);
  }

  // TODO
  @Export
  private static IPyObj get_ident(PyContext ctx) {
    return PyInt.ZERO;
  }

  @Export
  private static IPyObj allocate_lock(Args args, PyContext ctx) {
    throw new NotImplementedException("allocate_lock");
  }

  private static class RLock implements IPyTrueAtomObj, IPyNoValidationObj {
    private static final IPyClass TYPE =
        new PyBuiltinClass("RLock", RLock.class, MethodHandles.lookup(), PyBuiltinClass.CALLABLE);

    @Constructor
    private RLock(Args args, PyContext ctx) {
      // TODO
    }

    @InstanceMethod
    public IPyObj acquire(PyContext ctx) {
      return None;
    }

    @InstanceMethod
    public IPyObj release(PyContext ctx) {
      return None;
    }

    @Override
    public IPyClass type() {
      return TYPE;
    }

    @InstanceMethod
    private IPyObj __enter__(PyContext ctx) {
      return None;
    }

    @InstanceMethod
    private IPyObj __exit__(Args args, PyContext ctx) {
      return None;
    }
  }
}
