package org.japy.lib.re;

import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.str.PyStr;

interface PatternImpl {
  IPyObj match(PyStr str, int pos, int endpos);

  IPyObj search(PyStr str, int pos, int endpos);
}
