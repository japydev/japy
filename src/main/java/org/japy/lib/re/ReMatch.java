package org.japy.lib.re;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

class ReMatch implements IPyTrueAtomObj, IPyNoValidationObj {
  static final IPyClass TYPE =
      new PyBuiltinClass(
          "re.Match", ReMatch.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private final MatchImpl impl;

  ReMatch(MatchImpl impl) {
    this.impl = impl;
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @InstanceMethod
  private IPyObj groups(@Param(value = "default", dflt = DEFAULT_NONE) IPyObj dflt, PyContext ctx) {
    return impl.groups(dflt, ctx);
  }

  @InstanceMethod
  private IPyObj group(Args args, PyContext ctx) {
    ArgParser.verifyNoKwArgs(args, "group", ctx);
    if (args.posCount() == 0) {
      return impl.group(0, ctx);
    } else if (args.posCount() == 1) {
      return group(args.getPos(0), ctx);
    } else {
      IPyObj[] groups = new IPyObj[args.posCount()];
      for (int i = 0; i != groups.length; ++i) {
        groups[i] = group(args.getPos(i), ctx);
      }
      return PyTuple.of(groups);
    }
  }

  private IPyObj group(IPyObj group, PyContext ctx) {
    if (group instanceof PyInt) {
      return impl.group(ArgLib.checkArgSmallInt("group", "group", group, ctx), ctx);
    } else if (group instanceof PyStr) {
      return impl.group(group.toString(), ctx);
    } else {
      throw PxException.typeError(
          "expected a group number or name, got " + PrintLib.repr(group, ctx), ctx);
    }
  }
}
