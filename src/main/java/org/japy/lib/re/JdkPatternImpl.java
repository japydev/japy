package org.japy.lib.re;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.misc.PyNone.None;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.str.CharSource;
import org.japy.kernel.types.coll.str.PyStr;

class JdkPatternImpl implements PatternImpl {
  final Pattern pattern;
  final Map<String, String> namedGroups;
  final int groupCount;

  public JdkPatternImpl(CharSource pattern, Node expr, int flags) {
    Translator translator = new Translator(pattern, expr, flags);
    this.pattern = translator.translate();
    this.namedGroups = translator.namedGroups;
    this.groupCount = translator.groupCount;
  }

  @Override
  public IPyObj match(PyStr str, int pos, int endpos) {
    if (!str.isUcs2()) {
      throw new NotImplementedException("match");
    }
    Matcher matcher = pattern.matcher(str.toString());
    if (matcher.region(pos, endpos).useTransparentBounds(true).matches()) {
      return new ReMatch(new JdkMatchImpl(this, matcher));
    }
    return None;
  }

  @Override
  public IPyObj search(PyStr str, int pos, int endpos) {
    if (!str.isUcs2()) {
      throw new NotImplementedException("search");
    }
    Matcher matcher = pattern.matcher(str.toString());
    if (matcher.region(pos, endpos).useTransparentBounds(true).find()) {
      return new ReMatch(new JdkMatchImpl(this, matcher));
    }
    return None;
  }

  private static class Translator implements Node.Visitor {
    private final CharSource pattern;
    private final Node expr;
    private int flags;
    private final StringBuilder sb;
    private final Map<String, String> namedGroups = new HashMap<>();
    private int groupCount = 1;

    private Translator(CharSource pattern, Node expr, int flags) {
      this.pattern = pattern;
      this.expr = expr;
      this.flags = flags;
      this.sb = new StringBuilder();
    }

    private Pattern translate() {
      try {
        Node.visit(expr, this);
      } catch (InternalErrorException | NotImplementedException ex) {
        throw new InternalErrorException(ex.getMessage() + ":" + pattern, (Throwable) ex);
      }
      return Pattern.compile(sb.toString(), translateFlags(flags));
    }

    private int translateFlags(int pythonFlags) {
      int unicodeFlags;
      if ((pythonFlags & ReFlag.UNICODE) != 0) {
        dcheck((pythonFlags & (ReFlag.ASCII | ReFlag.LOCALE)) == 0);
        dcheck(pattern.kind() != CharSource.Kind.BYTES);
        unicodeFlags = Pattern.UNICODE_CASE | Pattern.UNICODE_CHARACTER_CLASS;
      } else if ((pythonFlags & ReFlag.ASCII) != 0) {
        dcheck((pythonFlags & (ReFlag.UNICODE | ReFlag.LOCALE)) == 0);
        unicodeFlags = 0;
      } else if ((pythonFlags & ReFlag.LOCALE) != 0) {
        dcheck((pythonFlags & (ReFlag.UNICODE | ReFlag.ASCII)) == 0);
        throw new NotImplementedException("LOCALE");
      } else {
        throw new InternalErrorException("UNICODE, LOCALE, or ASCII are not set");
      }

      return unicodeFlags
          | ((pythonFlags & ReFlag.DOTALL) != 0 ? Pattern.DOTALL : 0)
          | ((pythonFlags & ReFlag.IGNORECASE) != 0 ? Pattern.CASE_INSENSITIVE : 0)
          | ((pythonFlags & ReFlag.MULTILINE) != 0 ? Pattern.MULTILINE : 0);
    }

    private static final Pattern RE_INVALID_NAME_CHARS = Pattern.compile("[^a-zA-Z0-9]+");

    private String translateName(String pythonName) {
      @Var String sanitized = RE_INVALID_NAME_CHARS.matcher(pythonName).replaceAll("");
      if (sanitized.isEmpty()) {
        sanitized = "g";
      }
      for (int i = 1; ; ++i) {
        String name;
        if (i == 1) {
          name = sanitized;
        } else {
          name = sanitized + i;
        }
        if (!namedGroups.containsKey(name)) {
          namedGroups.put(pythonName, name);
          return name;
        }
      }
    }

    @Override
    public void visitGlobalFlags(int flags) {
      this.flags |= flags;
    }

    private void writeOtherFlags(int flags, StringBuilder sb) {
      if ((flags & ReFlag.LOCALE) != 0) {
        throw new NotImplementedException("writeFlags");
      }
      if ((flags & ReFlag.UNICODE) != 0) {
        throw new NotImplementedException("writeFlags");
      }
      if ((flags & ReFlag.IGNORECASE) != 0) {
        sb.append('i');
      }
      if ((flags & ReFlag.MULTILINE) != 0) {
        sb.append('m');
      }
      if ((flags & ReFlag.DOTALL) != 0) {
        sb.append('s');
      }
    }

    private void writeLocalFlags(int plusFlags, int minusFlags) {
      StringBuilder plus = new StringBuilder(4);
      StringBuilder minus = new StringBuilder(4);
      if ((plusFlags & ReFlag.ASCII) != 0) {
        if ((minusFlags & ReFlag.ASCII) == 0) {
          minus.append("uU");
        }
      } else if ((minusFlags & ReFlag.ASCII) != 0) {
        plus.append("uU");
      }
      writeOtherFlags(plusFlags, plus);
      writeOtherFlags(minusFlags, minus);
      sb.append(plus);
      String minusStr = minus.toString();
      if (!minusStr.isEmpty()) {
        sb.append('-');
        sb.append(minusStr);
      }
    }

    @Override
    public void visitLocalFlags(int plusFlags, int minusFlags, Node child) {
      sb.append("(?");
      writeLocalFlags(plusFlags, minusFlags);
      sb.append(':');
      Node.visit(child, this);
      sb.append(')');
    }

    @Override
    public void visitBackRef(Node.CaptureRef ref) {
      throw new NotImplementedException("visit");
    }

    @Override
    public void visitComment() {
      throw new NotImplementedException("visit");
    }

    @Override
    public void visitOr(List<Node> branches) {
      @Var boolean first = true;
      for (Node node : branches) {
        if (!first) {
          sb.append('|');
        }
        first = false;
        Node.visit(node, this);
      }
    }

    @Override
    public void visitSet(List<Node> elements, boolean inverse) {
      sb.append(inverse ? "[^" : "[");
      for (Node node : elements) {
        visitSetElement(node);
      }
      sb.append(']');
    }

    private void writeSimpleChar(int ch) {
      if (pattern.kind() == CharSource.Kind.BYTES) {
        sb.append((char) ch);
      } else {
        sb.appendCodePoint(ch);
      }
    }

    private void writeCharInSet(int ch) {
      switch (ch) {
        case '[':
          sb.append("\\[");
          return;
        case ']':
          sb.append("\\]");
          return;
        case '-':
          sb.append("\\-");
          return;
        case '\\':
          sb.append("\\\\");
          return;
        default:
          break;
      }

      writeSimpleChar(ch);
    }

    private void visitSetElement(Node node) {
      switch (node.kind()) {
        case CHAR:
          writeCharInSet(((Node.Char) node).ch);
          break;

        case CHAR_CLASS:
          visitCharClass((Node.CharClass) node);
          break;

        case RANGE:
          {
            Node.Range range = (Node.Range) node;
            writeCharInSet(range.start);
            sb.append('-');
            writeCharInSet(range.end);
            break;
          }

        default:
          throw new InternalErrorException("unhandled set element " + node);
      }
    }

    @Override
    public void visitRep(Node.Quantifier quantifier, Node child) {
      Node.visit(child, this);
      switch (quantifier.kind) {
        case STAR:
          sb.append('*');
          break;
        case PLUS:
          sb.append('+');
          break;
        case MAYBE:
          sb.append('?');
          break;
        case COUNT:
          sb.append('{');
          sb.append(quantifier.m);
          sb.append('}');
          break;
        case RANGE:
          sb.append('{');
          sb.append(quantifier.m);
          sb.append(',');
          sb.append(quantifier.n);
          sb.append('}');
          break;
      }
      if (!quantifier.greedy) {
        sb.append('?');
      }
    }

    @Override
    public void visitSeq(List<Node> children) {
      for (Node node : children) {
        Node.visit(node, this);
      }
    }

    @Override
    public void visitEmpty() {}

    @Override
    public void visitChar(int ch) {
      switch (ch) {
        case '[':
          sb.append("\\[");
          return;
        case ']':
          sb.append("\\]");
          return;
        case '(':
          sb.append("\\(");
          return;
        case ')':
          sb.append("\\)");
          return;
        case '{':
          sb.append("\\{");
          return;
        case '}':
          sb.append("\\}");
          return;
        case '\\':
          sb.append("\\\\");
          return;
        case '*':
          sb.append("\\*");
          return;
        case '+':
          sb.append("\\+");
          return;
        case '?':
          sb.append("\\?");
          return;
        case '.':
          sb.append("\\.");
          return;
        case '\t':
          sb.append("\\t");
          return;
        case '\r':
          sb.append("\\r");
          return;
        case '\n':
          sb.append("\\n");
          return;
        default:
          break;
      }

      writeSimpleChar(ch);
    }

    @Override
    public void visitGroup(Node child) {
      sb.append("(?:");
      Node.visit(child, this);
      sb.append(")");
    }

    @Override
    public void visitCapture(Node.CaptureId id, Node child) {
      ++groupCount;
      sb.append('(');
      if (id.isNamed()) {
        sb.append("?<");
        sb.append(translateName(id.name()));
        sb.append(">");
      }
      Node.visit(child, this);
      sb.append(')');
    }

    @Override
    public void visitAssertion(Node.AssertionKind kind, Node child) {
      sb.append("(?");
      switch (kind) {
        case LOOKAHEAD:
          sb.append('=');
          break;
        case NEGATIVE_LOOKAHEAD:
          sb.append('!');
          break;
        case LOOKBEHIND:
          sb.append("<=");
          break;
        case NEGATIVE_LOOKBEHIND:
          sb.append("<!");
          break;
      }
      Node.visit(child, this);
      sb.append(')');
    }

    @Override
    public void visitCharClass(Node.CharClass cc) {
      switch (cc) {
        case WORD:
          sb.append("\\w");
          break;
        case NONWORD:
          sb.append("\\W");
          break;
        case DIGIT:
          sb.append("\\d");
          break;
        case NONDIGIT:
          sb.append("\\D");
          break;
        case SPACE:
          sb.append("\\s");
          break;
        case NONSPACE:
          sb.append("\\S");
          break;
      }
    }

    @Override
    public void visitConditional(Node.CaptureRef ref, Node yes, @Nullable Node no) {
      throw new NotImplementedException("visit");
    }

    @Override
    public void visitSpecialMatch(Node.SpecialMatch kind) {
      switch (kind) {
        case START:
          sb.append("\\A");
          break;
        case END:
          sb.append("\\z");
          break;
        case WORD_BOUNDARY:
          sb.append("\\b");
          break;
        case WORD_NONBOUNDARY:
          sb.append("\\B");
          break;
      }
    }

    @Override
    public void visitDot() {
      sb.append('.');
    }
  }
}
