package org.japy.lib.re;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;

interface MatchImpl {
  IPyObj group(int index, PyContext ctx);

  IPyObj group(String name, PyContext ctx);

  IPyObj groups(IPyObj dflt, PyContext ctx);
}
