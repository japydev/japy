package org.japy.lib.re;

class ReFlag {
  static final int ASCII = 0x1;
  static final int IGNORECASE = 0x2;
  static final int LOCALE = 0x4;
  static final int UNICODE = 0x8;
  static final int MULTILINE = 0x10;
  static final int DOTALL = 0x20;
  static final int VERBOSE = 0x40;
}
