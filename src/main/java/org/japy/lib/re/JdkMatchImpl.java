package org.japy.lib.re;

import static org.japy.kernel.types.misc.PyNone.None;

import java.util.regex.Matcher;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxIndexError;

class JdkMatchImpl implements MatchImpl {
  private final JdkPatternImpl patternImpl;
  private final Matcher matcher;

  JdkMatchImpl(JdkPatternImpl patternImpl, Matcher matcher) {
    this.patternImpl = patternImpl;
    this.matcher = matcher;
  }

  @Override
  public IPyObj group(int index, PyContext ctx) {
    if (index < 0 || index >= patternImpl.groupCount) {
      throw new PxIndexError(
          String.format(
              "invalid group number %s, valid values are 0 to %s",
              index, patternImpl.groupCount - 1),
          ctx);
    }
    @Nullable String group = matcher.group(index);
    return group != null ? PyStr.get(group) : None;
  }

  @Override
  public IPyObj group(String name, PyContext ctx) {
    @Nullable String realName = patternImpl.namedGroups.get(name);
    if (realName == null) {
      throw new PxIndexError(String.format("invalid group name %s", name), ctx);
    }
    @Nullable String group = matcher.group(realName);
    return group != null ? PyStr.get(group) : None;
  }

  @Override
  public IPyObj groups(IPyObj dflt, PyContext ctx) {
    int count = matcher.groupCount();
    IPyObj[] groups = new IPyObj[count];
    for (int i = 0; i != count; ++i) {
      String group = matcher.group(i + 1);
      groups[i] = group != null ? PyStr.get(group) : dflt;
    }
    return new PyTuple(groups);
  }
}
