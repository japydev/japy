package org.japy.lib.re;

import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExc;
import org.japy.kernel.types.exc.py.PyExcType;
import org.japy.kernel.util.Args;

public class PyReError extends PyExc {
  public static final PyExcType TYPE =
      new PyExcType("re.error", PyBuiltinExc.Exception, PyReError.class, PxReError.class);

  public IPyObj msg;
  public IPyObj pattern;
  public IPyObj pos;

  @Constructor
  @SubClassConstructor
  public PyReError(IPyClass cls, Args args, PyContext ctx) {
    super(cls, args, ctx);
    this.msg = args.posCount() != 0 ? args.getPos(0) : None;
    this.pattern = None;
    this.pos = None;
  }

  //  public PyReError(String msg, @Nullable String pattern, int pos, PyContext ctx) {
  //    super(TYPE, PyTuple.of(makeBaseMessage(msg, pos)), ctx);
  //    this.msg = PyStr.get(msg);
  //    this.pattern = pattern != null ? PyStr.get(pattern) : None;
  //    this.pos = pos >= 0 ? PyInt.get(pos) : None;
  //  }

  //  private static IPyObj makeBaseMessage(String msg, int pos) {
  //    return pos >= 0 ? PyStr.get(String.format("%s at position %d", msg, pos)) : PyStr.get(msg);
  //  }
}
