package org.japy.lib.re;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_SENTINEL;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_ZERO;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.misc.PySentinel.Sentinel;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.IPyCharSource;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

class RePattern implements IPyTrueAtomObj, IPyNoValidationObj {
  static final IPyClass TYPE =
      new PyBuiltinClass(
          "re.Pattern", RePattern.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private final IPyObj pattern;
  private final PatternImpl impl;

  public RePattern(IPyObj patternObj, IPyObj flagsObj, PyContext ctx) {
    if (!(patternObj instanceof IPyCharSource)) {
      throw PxException.typeError(
          String.format(
              "pattern must be a string or a bytes instance, got '%s'", patternObj.typeName()),
          ctx);
    }
    int flags = ArgLib.checkArgSmallInt("Pattern", "flags", flagsObj, ctx);
    pattern = patternObj;
    impl = ReCompiler.compile(((IPyCharSource) patternObj).asCharSource(), flags, ctx);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @InstanceAttribute
  private IPyObj flags(PyContext ctx) {
    throw new NotImplementedException("flags");
  }

  @InstanceAttribute
  private IPyObj groups(PyContext ctx) {
    throw new NotImplementedException("groups");
  }

  @InstanceAttribute
  private IPyObj groupindex(PyContext ctx) {
    throw new NotImplementedException("groupindex");
  }

  @InstanceAttribute
  private IPyObj pattern(PyContext ctx) {
    return pattern;
  }

  @InstanceMethod
  private IPyObj search(
      @Param("string") IPyObj strObj,
      @Param(value = "pos", dflt = DEFAULT_SENTINEL) IPyObj posObj,
      @Param(value = "endpos", dflt = DEFAULT_SENTINEL) IPyObj endposObj,
      PyContext ctx) {
    PyStr str = ArgLib.checkArgStr("re.Pattern.search", "string", strObj, ctx);
    int pos =
        posObj != Sentinel
            ? ArgLib.checkArgSmallNonNegInt("re.Pattern.search", "pos", posObj, ctx)
            : 0;
    int endpos =
        endposObj != Sentinel
            ? Math.min(
                ArgLib.checkArgSmallNonNegInt("re.Pattern.search", "endpos", endposObj, ctx),
                str.len())
            : str.len();
    if (endpos < pos) {
      return None;
    }
    return impl.search(str, pos, endpos);
  }

  @InstanceMethod
  private IPyObj match(
      @Param("string") IPyObj strObj,
      @Param(value = "pos", dflt = DEFAULT_SENTINEL) IPyObj posObj,
      @Param(value = "endpos", dflt = DEFAULT_SENTINEL) IPyObj endposObj,
      PyContext ctx) {
    PyStr str = ArgLib.checkArgStr("re.Pattern.match", "string", strObj, ctx);
    int pos =
        posObj != Sentinel
            ? ArgLib.checkArgSmallNonNegInt("re.Pattern.match", "pos", posObj, ctx)
            : 0;
    int endpos =
        endposObj != Sentinel
            ? Math.min(
                ArgLib.checkArgSmallNonNegInt("re.Pattern.match", "endpos", endposObj, ctx),
                str.len())
            : str.len();
    if (endpos < pos) {
      return None;
    }
    return impl.match(str, pos, endpos);
  }

  @InstanceMethod
  private IPyObj fullmatch(
      @Param("string") IPyObj str,
      @Param(value = "pos", dflt = DEFAULT_SENTINEL) IPyObj pos,
      @Param(value = "endpos", dflt = DEFAULT_SENTINEL) IPyObj endpos,
      PyContext ctx) {
    throw new NotImplementedException("fullmatch");
  }

  @InstanceMethod
  private IPyObj split(
      @Param("string") IPyObj str,
      @Param(value = "maxsplit", dflt = DEFAULT_ZERO) IPyObj maxsplit,
      PyContext ctx) {
    throw new NotImplementedException("split");
  }

  @InstanceMethod
  private IPyObj findall(
      @Param("string") IPyObj str,
      @Param(value = "pos", dflt = DEFAULT_SENTINEL) IPyObj pos,
      @Param(value = "endpos", dflt = DEFAULT_SENTINEL) IPyObj endpos,
      PyContext ctx) {
    throw new NotImplementedException("findall");
  }

  @InstanceMethod
  private IPyObj finditer(
      @Param("string") IPyObj str,
      @Param(value = "pos", dflt = DEFAULT_SENTINEL) IPyObj pos,
      @Param(value = "endpos", dflt = DEFAULT_SENTINEL) IPyObj endpos,
      PyContext ctx) {
    throw new NotImplementedException("finditer");
  }

  @InstanceMethod
  private IPyObj sub(
      @Param("repl") IPyObj repl,
      @Param("string") IPyObj str,
      @Param(value = "count", dflt = DEFAULT_ZERO) IPyObj count,
      PyContext ctx) {
    throw new NotImplementedException("sub");
  }

  @InstanceMethod
  private IPyObj subn(
      @Param("repl") IPyObj repl,
      @Param("string") IPyObj str,
      @Param(value = "count", dflt = DEFAULT_ZERO) IPyObj count,
      PyContext ctx) {
    throw new NotImplementedException("subn");
  }
}
