package org.japy.lib.re;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;

@SuppressWarnings("InterfaceWithOnlyStatics")
interface Node {
  enum Kind {
    EMPTY,
    SEQ,
    OR,
    CAPTURE,
    REP,
    GROUP,
    COMMENT,
    BACKREF,
    ASSERTION,
    GLOBAL_FLAGS,
    LOCAL_FLAGS,
    CONDITIONAL,
    CHAR,
    RANGE,
    SET,
    CHAR_CLASS,
    DOT,
    SPECIAL_MATCH
  }

  Kind kind();

  enum AssertionKind {
    LOOKAHEAD,
    NEGATIVE_LOOKAHEAD,
    LOOKBEHIND,
    NEGATIVE_LOOKBEHIND,
  }

  enum SpecialMatch implements Node {
    START,
    WORD_BOUNDARY,
    WORD_NONBOUNDARY,
    END;

    @Override
    public Kind kind() {
      return Kind.SPECIAL_MATCH;
    }
  }

  class CaptureId {
    private final int pos;
    private final @Nullable String name;

    public CaptureId(int pos) {
      dcheck(pos >= 0);
      this.pos = pos;
      this.name = null;
    }

    public CaptureId(int pos, String name) {
      dcheck(pos >= 0);
      this.pos = pos;
      this.name = name;
    }

    public boolean isNamed() {
      return name != null;
    }

    public String name() {
      return dcheckNotNull(name);
    }
  }

  class CaptureRef implements Node {
    private final int pos;
    private final @Nullable String name;

    public CaptureRef(int pos) {
      dcheck(pos >= 0);
      this.pos = pos;
      this.name = null;
    }

    public CaptureRef(String name) {
      this.pos = -1;
      this.name = name;
    }

    @Override
    public Kind kind() {
      return Kind.BACKREF;
    }
  }

  enum QuantifierKind {
    STAR,
    PLUS,
    MAYBE,
    COUNT,
    RANGE,
  }

  class Quantifier {
    public final QuantifierKind kind;
    public final int m;
    public final int n;
    public final boolean greedy;

    private Quantifier(QuantifierKind kind, int m, int n, boolean greedy) {
      this.kind = kind;
      this.m = m;
      this.n = n;
      this.greedy = greedy;
    }

    private static Quantifier simple(int ch, boolean greedy) {
      switch (ch) {
        case '*':
          return new Quantifier(QuantifierKind.STAR, -1, -1, greedy);
        case '+':
          return new Quantifier(QuantifierKind.PLUS, -1, -1, greedy);
        case '?':
          return new Quantifier(QuantifierKind.MAYBE, -1, -1, greedy);
        default:
          throw InternalErrorException.notReached();
      }
    }

    public static Quantifier greedy(int ch) {
      return simple(ch, true);
    }

    public static Quantifier nonGreedy(int ch) {
      return simple(ch, false);
    }

    public static Quantifier count(int m) {
      return new Quantifier(QuantifierKind.COUNT, m, -1, true);
    }

    public static Quantifier range(int m, int n, boolean greedy) {
      return new Quantifier(QuantifierKind.RANGE, m, n, greedy);
    }
  }

  class Empty implements Node {
    @Override
    public Kind kind() {
      return Kind.EMPTY;
    }
  }

  class Seq implements Node {
    public final List<Node> children;

    public Seq(List<Node> children) {
      this.children = children;
    }

    @Override
    public Kind kind() {
      return Kind.SEQ;
    }
  }

  class Or implements Node {
    public final List<Node> branches;

    public Or(List<Node> branches) {
      this.branches = branches;
    }

    @Override
    public Kind kind() {
      return Kind.OR;
    }
  }

  class Capture implements Node {
    public final CaptureId id;
    public final Node child;

    public Capture(CaptureId id, Node child) {
      this.id = id;
      this.child = child;
    }

    @Override
    public Kind kind() {
      return Kind.CAPTURE;
    }
  }

  class Rep implements Node {
    public final Node child;
    public final Quantifier quantifier;

    public Rep(Node child, Quantifier quantifier) {
      this.child = child;
      this.quantifier = quantifier;
    }

    @Override
    public Kind kind() {
      return Kind.REP;
    }
  }

  class Group implements Node {
    public final Node child;

    public Group(Node child) {
      this.child = child;
    }

    @Override
    public Kind kind() {
      return Kind.GROUP;
    }
  }

  class Comment implements Node {
    @Override
    public Kind kind() {
      return Kind.COMMENT;
    }
  }

  class Assertion implements Node {
    public final AssertionKind kind;
    public final Node node;

    public Assertion(AssertionKind kind, Node node) {
      this.kind = kind;
      this.node = node;
    }

    @Override
    public Kind kind() {
      return Kind.ASSERTION;
    }
  }

  class GlobalFlags implements Node {
    public final int flags;

    public GlobalFlags(int flags) {
      this.flags = flags;
    }

    @Override
    public Kind kind() {
      return Kind.GLOBAL_FLAGS;
    }
  }

  class LocalFlags implements Node {
    public final Node child;
    public final int plusFlags;
    public final int minusFlags;

    public LocalFlags(Node child, int plusFlags, int minusFlags) {
      this.child = child;
      this.plusFlags = plusFlags;
      this.minusFlags = minusFlags;
    }

    @Override
    public Kind kind() {
      return Kind.LOCAL_FLAGS;
    }
  }

  class Conditional implements Node {
    public final CaptureRef ref;
    public final Node yes;
    public final @Nullable Node no;

    public Conditional(CaptureRef ref, Node yes, @Nullable Node no) {
      this.ref = ref;
      this.yes = yes;
      this.no = no;
    }

    @Override
    public Kind kind() {
      return Kind.CONDITIONAL;
    }
  }

  class Char implements Node {
    public final int ch;

    public Char(int ch) {
      this.ch = ch;
    }

    @Override
    public Kind kind() {
      return Kind.CHAR;
    }
  }

  class Range implements Node {
    public final int start;
    public final int end;

    public Range(int start, int end) {
      this.start = start;
      this.end = end;
    }

    @Override
    public Kind kind() {
      return Kind.RANGE;
    }
  }

  class Set implements Node {
    public final boolean inverse;
    public final List<Node> elements;

    public Set(boolean inverse, List<Node> elements) {
      this.inverse = inverse;
      this.elements = elements;
    }

    @Override
    public Kind kind() {
      return Kind.SET;
    }
  }

  enum CharClass implements Node {
    DIGIT,
    NONDIGIT,
    SPACE,
    NONSPACE,
    WORD,
    NONWORD,
    ;

    @Override
    public Kind kind() {
      return Kind.CHAR_CLASS;
    }
  }

  interface Visitor {
    void visitGlobalFlags(int flags);

    void visitLocalFlags(int plusFlags, int minusFlags, Node child);

    void visitBackRef(CaptureRef ref);

    void visitComment();

    void visitOr(List<Node> branches);

    void visitSet(List<Node> elements, boolean inverse);

    void visitRep(Quantifier quantifier, Node child);

    void visitSeq(List<Node> children);

    void visitEmpty();

    void visitChar(int ch);

    void visitGroup(Node child);

    void visitCapture(CaptureId id, Node child);

    void visitAssertion(AssertionKind kind, Node child);

    void visitCharClass(CharClass cc);

    void visitConditional(CaptureRef ref, Node yes, @Nullable Node no);

    void visitSpecialMatch(SpecialMatch kind);

    void visitDot();
  }

  static void visit(Node node, Visitor visitor) {
    switch (node.kind()) {
      case GLOBAL_FLAGS:
        {
          GlobalFlags f = (GlobalFlags) node;
          visitor.visitGlobalFlags(f.flags);
          break;
        }
      case LOCAL_FLAGS:
        {
          LocalFlags f = (LocalFlags) node;
          visitor.visitLocalFlags(f.plusFlags, f.minusFlags, f.child);
          break;
        }
      case BACKREF:
        visitor.visitBackRef((CaptureRef) node);
        break;
      case COMMENT:
        {
          visitor.visitComment();
          break;
        }
      case OR:
        {
          Or or = (Or) node;
          visitor.visitOr(or.branches);
          break;
        }
      case SET:
        {
          Set set = (Set) node;
          visitor.visitSet(set.elements, set.inverse);
          break;
        }
      case REP:
        {
          Rep rep = (Rep) node;
          visitor.visitRep(rep.quantifier, rep.child);
          break;
        }
      case SEQ:
        {
          Seq seq = (Seq) node;
          visitor.visitSeq(seq.children);
          break;
        }
      case EMPTY:
        {
          visitor.visitEmpty();
          break;
        }
      case CHAR:
        {
          Char c = (Char) node;
          visitor.visitChar(c.ch);
          break;
        }
      case GROUP:
        {
          Group g = (Group) node;
          visitor.visitGroup(g.child);
          break;
        }
      case CAPTURE:
        {
          Capture c = (Capture) node;
          visitor.visitCapture(c.id, c.child);
          break;
        }
      case ASSERTION:
        {
          Assertion a = (Assertion) node;
          visitor.visitAssertion(a.kind, a.node);
          break;
        }
      case CHAR_CLASS:
        {
          CharClass cc = (CharClass) node;
          visitor.visitCharClass(cc);
          break;
        }
      case CONDITIONAL:
        {
          Conditional c = (Conditional) node;
          visitor.visitConditional(c.ref, c.yes, c.no);
          break;
        }
      case SPECIAL_MATCH:
        {
          SpecialMatch m = (SpecialMatch) node;
          visitor.visitSpecialMatch(m);
          break;
        }
      case DOT:
        visitor.visitDot();
        break;

      case RANGE:
        throw InternalErrorException.notReached();
    }
  }

  class Dot implements Node {
    @Override
    public Kind kind() {
      return Kind.DOT;
    }
  }
}
