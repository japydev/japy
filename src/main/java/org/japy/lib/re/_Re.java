package org.japy.lib.re;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_ZERO;
import static org.japy.kernel.util.PyUtil.wrap;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.num.PyBool;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class _Re extends PyBuiltinModule {
  _Re() {
    super("_re", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    set("FLAG_ASCII", wrap(ReFlag.ASCII));
    set("FLAG_IGNORECASE", wrap(ReFlag.IGNORECASE));
    set("FLAG_LOCALE", wrap(ReFlag.LOCALE));
    set("FLAG_UNICODE", wrap(ReFlag.UNICODE));
    set("FLAG_MULTILINE", wrap(ReFlag.MULTILINE));
    set("FLAG_DOTALL", wrap(ReFlag.DOTALL));
    set("FLAG_VERBOSE", wrap(ReFlag.VERBOSE));

    set("error", PyReError.TYPE);
    set("Pattern", RePattern.TYPE);
    set("Match", ReMatch.TYPE);
  }

  @Export
  private static IPyObj isstring(@Param("obj") IPyObj obj, PyContext ctx) {
    return PyBool.of(obj instanceof PyStr || obj instanceof PyBytes);
  }

  @Export
  private static IPyObj compile(
      @Param("pattern") IPyObj pattern,
      @Param(value = "flags", dflt = DEFAULT_ZERO) IPyObj flags,
      PyContext ctx) {
    return new RePattern(pattern, flags, ctx);
  }
}
