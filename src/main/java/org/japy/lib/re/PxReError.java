package org.japy.lib.re;

import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.py.PyBaseException;

public class PxReError extends PxException {
  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxReError(PyBaseException pyExc) {
    super(pyExc);
  }
}
