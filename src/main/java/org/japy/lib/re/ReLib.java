package org.japy.lib.re;

import org.japy.lib.ModuleRegistry;

public class ReLib {
  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new _Re());
  }
}
