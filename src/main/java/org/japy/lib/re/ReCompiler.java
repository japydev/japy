package org.japy.lib.re;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.util.ArrayList;
import java.util.List;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.eclipse.collections.impl.stack.mutable.primitive.BooleanArrayStack;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.str.CharSource;
import org.japy.kernel.types.coll.str.UnicodeLib;
import org.japy.kernel.types.exc.px.PxException;

class ReCompiler {
  static final int NO_CAPTURE = -1;

  private final CharSource source;

  private final int length;
  private int cursor;
  private int captureCount;
  private final BooleanArrayStack verbose;

  private ReCompiler(CharSource source, boolean verbose) {
    this.source = source;
    this.length = source.len();
    this.verbose = new BooleanArrayStack();
    this.verbose.push(verbose);
  }

  public static PatternImpl compile(CharSource pattern, @Var int flags, PyContext ctx) {
    if (pattern.kind() == CharSource.Kind.BYTES) {
      if ((flags & ReFlag.UNICODE) != 0) {
        throw PxException.valueError("UNICODE may not be used with a byte pattern", ctx);
      }
      if ((flags & ReFlag.LOCALE) == 0) {
        flags |= ReFlag.ASCII;
      } else if ((flags & ReFlag.ASCII) != 0) {
        throw PxException.valueError("LOCALE is not compatible with ASCII", ctx);
      }
    } else {
      if ((flags & ReFlag.LOCALE) != 0) {
        throw PxException.valueError("LOCALE is not supported for unicode strings", ctx);
      }
      if ((flags & ReFlag.ASCII) == 0) {
        flags |= ReFlag.UNICODE;
      } else if ((flags & ReFlag.UNICODE) != 0) {
        throw PxException.valueError("ASCII is not compatible with UNICODE", ctx);
      }
    }

    Node expr = new ReCompiler(pattern, (flags & ReFlag.VERBOSE) != 0).expr();
    return new JdkPatternImpl(pattern, expr, flags);
  }

  private boolean verbose() {
    return verbose.peek();
  }

  private int curChar() {
    if (cursor == length) {
      throw error("unexpected EOF");
    }
    int ch = source.getChar(cursor);
    if (Debug.ENABLED) {
      dcheck(ch >= 0);
    }
    return ch;
  }

  private boolean atEof() {
    if (Debug.ENABLED) {
      dcheck(cursor <= length);
    }
    return cursor == length;
  }

  private void skipComment() {
    if (Debug.ENABLED) {
      dcheck(curChar() == '#');
    }
    ++cursor;
    while (!atEof()) {
      int ch = curChar();
      switch (ch) {
        case '\r':
          ++cursor;
          if (!atEof() && curChar() == '\n') {
            ++cursor;
          }
          return;

        case '\n':
        case '\f':
          ++cursor;
          return;

        default:
          ++cursor;
          break;
      }
    }
  }

  private void skipIgnorable() {
    if (!verbose()) {
      return;
    }

    while (!atEof()) {
      switch (curChar()) {
        case '#':
          skipComment();
          break;

        case ' ':
        case '\t':
        case '\r':
        case '\n':
        case '\f':
          ++cursor;
          break;

        default:
          return;
      }
    }
  }

  @SuppressWarnings("ThrowSpecificExceptions")
  private RuntimeException error(String what) {
    throw new RuntimeException(what + ":\n" + source);
  }

  private Node expr() {
    List<Node> branches = new ArrayList<>(1);
    @Var boolean done = false;
    while (!atEof() && !done) {
      skipIgnorable();
      if (atEof()) {
        break;
      }

      switch (curChar()) {
        case '|':
          branches.add(new Node.Empty());
          ++cursor;
          break;

        case ')':
          done = true;
          break;

        default:
          {
            Node branch = branch();
            branches.add(branch);
            if (!atEof() && curChar() == '|') {
              ++cursor;
            }
            break;
          }
      }
    }
    if (branches.isEmpty()) {
      return new Node.Empty();
    } else if (branches.size() == 1) {
      return branches.get(0);
    } else {
      return new Node.Or(branches);
    }
  }

  private Node branch() {
    List<Node> atoms = new ArrayList<>(1);
    @Var boolean done = false;
    while (!atEof() && !done) {
      skipIgnorable();
      if (atEof()) {
        break;
      }
      switch (curChar()) {
        case '|':
        case ')':
          done = true;
          break;

        default:
          {
            Node atom = atom();
            atoms.add(atom);
            break;
          }
      }
    }
    if (atoms.size() == 1) {
      return atoms.get(0);
    } else {
      return new Node.Seq(atoms);
    }
  }

  private Node atom() {
    Node atom;
    switch (curChar()) {
      case '?':
      case '*':
      case ']':
        throw error(String.format("unexpected character '%c'", curChar()));

      case '(':
        atom = group();
        break;

      case '[':
        atom = range();
        break;

      case '.':
        atom = new Node.Dot();
        ++cursor;
        break;

      default:
        atom = character();
        break;
    }

    if (!atEof()) {
      Node.@Nullable Quantifier q = maybeQuantifier();
      if (q != null) {
        return new Node.Rep(atom, q);
      }
    }

    return atom;
  }

  private Node.@Nullable Quantifier maybeQuantifier() {
    int ch = curChar();
    switch (ch) {
      case '*':
      case '+':
      case '?':
        ++cursor;
        if (!atEof() && curChar() == '?') {
          ++cursor;
          return Node.Quantifier.nonGreedy(ch);
        } else {
          return Node.Quantifier.greedy(ch);
        }

      case '{':
        break;

      default:
        return null;
    }

    ++cursor;
    int m = number();
    @Var int n = -1;
    if (curChar() == ',') {
      ++cursor;
      n = number();
    }
    if (curChar() != '}') {
      throw error(String.format("expected '}', got '%c'", curChar()));
    }
    ++cursor;
    @Var boolean greedy = true;
    if (n >= 0 && !atEof() && curChar() == '?') {
      ++cursor;
      greedy = false;
    }

    return n >= 0 ? Node.Quantifier.range(m, n, greedy) : Node.Quantifier.count(m);
  }

  private static boolean isAsciiOct(int ch) {
    return ((ch - '0') | ('7' - ch)) >= 0;
  }

  private static boolean isAsciiDec(int ch) {
    return ((ch - '0') | ('9' - ch)) >= 0;
  }

  private static boolean isAsciiHex(int ch) {
    return isAsciiDec(ch) || ((ch - 'a') | ('f' - ch)) >= 0 || ((ch - 'A') | ('F' - ch)) >= 0;
  }

  private static boolean isAsciiLetter(int ch) {
    return ((ch - 'a') | ('z' - ch)) >= 0 || ((ch - 'A') | ('Z' - ch)) >= 0;
  }

  @SuppressWarnings("UnusedException")
  private int number() {
    @Var int ch = curChar();
    if (!isAsciiDec(ch)) {
      throw error(String.format("expected a digit, got '%c'", curChar()));
    }
    @Var int n = 0;
    while (true) {
      try {
        n = Math.multiplyExact(n, 10) + (ch - '0');
      } catch (ArithmeticException ex) {
        throw error("overflow in the group index");
      }

      ++cursor;
      if (atEof()) {
        return n;
      }
      ch = curChar();
      if (!isAsciiDec(ch)) {
        return n;
      }
    }
  }

  private static class GroupPrefix {
    enum Kind {
      CAPTURE,
      NONCAPTURING,
      LOOKAHEAD,
      NEGATIVE_LOOKAHEAD,
      LOOKBEHIND,
      NEGATIVE_LOOKBEHIND,
      LOCAL_FLAGS,
      GLOBAL_FLAGS,
      COMMENT,
      BACKREF,
      YES_NO,
    }

    final Kind kind;
    final Node.@Nullable CaptureId id;
    final Node.@Nullable CaptureRef ref;
    final int plusFlags;
    final int minusFlags;

    private GroupPrefix(
        Kind kind,
        Node.@Nullable CaptureId id,
        Node.@Nullable CaptureRef ref,
        int plusFlags,
        int minusFlags) {
      this.kind = kind;
      this.id = id;
      this.ref = ref;
      this.plusFlags = plusFlags;
      this.minusFlags = minusFlags;
    }

    public GroupPrefix(Kind kind) {
      this(kind, null, null, 0, 0);
      switch (kind) {
        case LOOKAHEAD:
        case LOOKBEHIND:
        case NONCAPTURING:
        case NEGATIVE_LOOKAHEAD:
        case NEGATIVE_LOOKBEHIND:
        case COMMENT:
          break;
        default:
          throw InternalErrorException.notReached();
      }
    }

    static GroupPrefix yesNo(int id) {
      return new GroupPrefix(Kind.YES_NO, null, new Node.CaptureRef(id), 0, 0);
    }

    static GroupPrefix yesNo(String id) {
      return new GroupPrefix(Kind.YES_NO, null, new Node.CaptureRef(id), 0, 0);
    }

    static GroupPrefix capture(int pos, String name) {
      return new GroupPrefix(Kind.CAPTURE, new Node.CaptureId(pos, name), null, 0, 0);
    }

    static GroupPrefix backRef(String id) {
      return new GroupPrefix(Kind.BACKREF, null, new Node.CaptureRef(id), 0, 0);
    }

    static GroupPrefix capture(int capture) {
      return new GroupPrefix(Kind.CAPTURE, new Node.CaptureId(capture), null, 0, 0);
    }

    static GroupPrefix localFlags(int plusFlags, int minusFlags) {
      return new GroupPrefix(Kind.LOCAL_FLAGS, null, null, plusFlags, minusFlags);
    }

    static GroupPrefix globalFlags(int plusFlags) {
      return new GroupPrefix(Kind.GLOBAL_FLAGS, null, null, plusFlags, 0);
    }
  }

  private int aiLmsux() {
    @Var int flags = 0;
    while (true) {
      switch (curChar()) {
        case 'a':
          flags |= ReFlag.ASCII;
          break;
        case 'i':
          flags |= ReFlag.IGNORECASE;
          break;
        case 'L':
          flags |= ReFlag.LOCALE;
          break;
        case 'm':
          flags |= ReFlag.MULTILINE;
          break;
        case 's':
          flags |= ReFlag.DOTALL;
          break;
        case 'u':
          flags |= ReFlag.UNICODE;
          break;
        case 'x':
          flags |= ReFlag.VERBOSE;
          break;
        default:
          return flags;
      }
      ++cursor;
    }
  }

  private int imsx() {
    @Var int flags = 0;
    while (true) {
      switch (curChar()) {
        case 'i':
          flags |= ReFlag.IGNORECASE;
          break;
        case 'm':
          flags |= ReFlag.MULTILINE;
          break;
        case 's':
          flags |= ReFlag.DOTALL;
          break;
        case 'x':
          flags |= ReFlag.VERBOSE;
          break;
        default:
          return flags;
      }
      ++cursor;
    }
  }

  private GroupPrefix flagsPrefix() {
    int plusFlags = aiLmsux();
    @Var int minusFlags = 0;
    if (curChar() == '-') {
      ++cursor;
      minusFlags = imsx();
    }
    switch (curChar()) {
      case ':':
        ++cursor;
        if ((plusFlags & minusFlags) != 0) {
          throw error("same flag added and removed");
        }
        if ((plusFlags & ReFlag.VERBOSE) != 0) {
          verbose.push(true);
        } else if ((minusFlags & ReFlag.VERBOSE) != 0) {
          verbose.push(false);
        }
        return GroupPrefix.localFlags(plusFlags, minusFlags);
      case ')':
        if (minusFlags != 0) {
          throw error("global flags with -");
        }
        if ((plusFlags & ReFlag.VERBOSE) != 0) {
          if (verbose.size() != 1) {
            throw error("global flags not at the beginning of the pattern");
          }
          verbose.clear();
          verbose.push(true);
        }
        return GroupPrefix.globalFlags(plusFlags);
      default:
        throw error(String.format("unexpected character '%c'", curChar()));
    }
  }

  private GroupPrefix inlineComment() {
    if (Debug.ENABLED) {
      dcheck(curChar() == '#');
    }
    ++cursor;
    while (curChar() != ')') {
      ++cursor;
    }
    return new GroupPrefix(GroupPrefix.Kind.COMMENT);
  }

  private GroupPrefix groupPrefix() {
    if (Debug.ENABLED) {
      dcheck(curChar() == '(');
    }

    ++cursor;
    if (curChar() != '?') {
      return GroupPrefix.capture(captureCount++);
    }

    ++cursor;
    switch (curChar()) {
      case 'a':
      case 'i':
      case 'L':
      case 'm':
      case 's':
      case 'u':
      case 'x':
      case '-':
        return flagsPrefix();

      case ':':
        ++cursor;
        return new GroupPrefix(GroupPrefix.Kind.NONCAPTURING);

      case 'P':
        return questionP();

      case '#':
        return inlineComment();

      case '=':
        ++cursor;
        return new GroupPrefix(GroupPrefix.Kind.LOOKAHEAD);

      case '!':
        ++cursor;
        return new GroupPrefix(GroupPrefix.Kind.NEGATIVE_LOOKAHEAD);

      case '<':
        ++cursor;
        switch (curChar()) {
          case '=':
            return new GroupPrefix(GroupPrefix.Kind.LOOKBEHIND);
          case '!':
            return new GroupPrefix(GroupPrefix.Kind.NEGATIVE_LOOKBEHIND);
          default:
            throw error(String.format("unexpected character '%c'", curChar()));
        }

      case '(':
        return yesNoPrefix();

      default:
        throw error(String.format("unexpected character '%c'", curChar()));
    }
  }

  private GroupPrefix yesNoPrefix() {
    if (Debug.ENABLED) {
      dcheck(curChar() == '(');
    }
    ++cursor;
    @Var int capture = NO_CAPTURE;
    @Var
    @Nullable
    String namedCapture = null;
    if (isAsciiDec(curChar())) {
      capture = number();
    } else {
      namedCapture = identifier();
    }
    if (curChar() != ')') {
      throw error(String.format("expected ')', got '%c'", curChar()));
    }
    if (capture >= 0) {
      return GroupPrefix.yesNo(capture);
    } else {
      return GroupPrefix.yesNo(dcheckNotNull(namedCapture));
    }
  }

  private String identifier() {
    @Var boolean seenChar = false;
    StringBuilder sb = new StringBuilder(10);
    while (!atEof()) {
      int ch = curChar();
      if (ch == '_' || isAsciiLetter(ch) || (seenChar && isAsciiDec(ch))) {
        sb.append((char) ch);
        seenChar = true;
        ++cursor;
      } else {
        break;
      }
    }
    if (!seenChar) {
      throw error("invalid identifier");
    }
    return sb.toString();
  }

  private GroupPrefix questionP() {
    if (Debug.ENABLED) {
      dcheck(curChar() == 'P');
    }
    ++cursor;
    switch (curChar()) {
      case '<':
        {
          ++cursor;
          String id = identifier();
          if (curChar() != '>') {
            throw error(String.format("expected '>', got '%c'", curChar()));
          }
          ++cursor;
          return GroupPrefix.capture(captureCount++, id);
        }

      case '=':
        {
          ++cursor;
          String id = identifier();
          if (curChar() != ')') {
            throw error(String.format("expected ')', got '%c'", curChar()));
          }
          return GroupPrefix.backRef(id);
        }

      default:
        throw error(String.format("unexpected character '%c'", curChar()));
    }
  }

  private Node group() {
    if (Debug.ENABLED) {
      dcheck(curChar() == '(');
    }

    GroupPrefix prefix = groupPrefix();
    @Var
    @Nullable
    Node expr = null;
    if (curChar() != ')') {
      expr = expr();
    }
    if (atEof()) {
      throw error("unexpected EOF");
    }
    if (Debug.ENABLED) {
      dcheck(curChar() == ')');
    }
    ++cursor;
    return makeGroup(prefix, expr);
  }

  private static Node ensureNode(@Nullable Node node) {
    return node != null ? node : new Node.Empty();
  }

  private Node makeGroup(GroupPrefix prefix, @Nullable Node expr) {
    switch (prefix.kind) {
      case CAPTURE:
        return new Node.Capture(dcheckNotNull(prefix.id), ensureNode(expr));
      case NONCAPTURING:
        return new Node.Group(ensureNode(expr));
      case COMMENT:
        dcheck(expr == null);
        return new Node.Comment();
      case BACKREF:
        dcheck(expr == null);
        return dcheckNotNull(prefix.ref);
      case LOOKAHEAD:
        return new Node.Assertion(Node.AssertionKind.LOOKAHEAD, ensureNode(expr));
      case NEGATIVE_LOOKAHEAD:
        return new Node.Assertion(Node.AssertionKind.NEGATIVE_LOOKAHEAD, ensureNode(expr));
      case LOOKBEHIND:
        return new Node.Assertion(Node.AssertionKind.LOOKBEHIND, ensureNode(expr));
      case NEGATIVE_LOOKBEHIND:
        return new Node.Assertion(Node.AssertionKind.NEGATIVE_LOOKBEHIND, ensureNode(expr));
      case GLOBAL_FLAGS:
        dcheck(expr == null);
        return new Node.GlobalFlags(prefix.plusFlags);
      case LOCAL_FLAGS:
        {
          if (((prefix.plusFlags | prefix.minusFlags) & ReFlag.VERBOSE) != 0) {
            verbose.pop();
            dcheck(!verbose.isEmpty());
          }
          return new Node.LocalFlags(ensureNode(expr), prefix.plusFlags, prefix.minusFlags);
        }
      case YES_NO:
        {
          if (expr instanceof Node.Or) {
            Node.Or or = (Node.Or) expr;
            if (or.branches.size() != 2) {
              throw error("too many branches");
            }
            return new Node.Conditional(
                dcheckNotNull(prefix.ref), or.branches.get(0), or.branches.get(1));
          } else {
            return new Node.Conditional(dcheckNotNull(prefix.ref), ensureNode(expr), null);
          }
        }
    }

    throw InternalErrorException.notReached();
  }

  private Node range() {
    if (Debug.ENABLED) {
      dcheck(curChar() == '[');
    }
    ++cursor;

    List<Node> elements = new ArrayList<>(10);
    boolean inverse = curChar() == '^';
    if (inverse) {
      ++cursor;
    }

    @Var
    @Nullable
    Node prev = null;
    @Var boolean minus = false;
    @Var boolean done = false;
    while (!done) {
      int ch = curChar();
      ++cursor;

      switch (ch) {
        case ']':
          if (elements.isEmpty() && prev == null) {
            prev = new Node.Char(']');
          } else {
            if (prev != null) {
              elements.add(prev);
              prev = null;
            }
            if (minus) {
              elements.add(new Node.Char('-'));
              minus = false;
            }
            done = true;
          }
          break;

        case '-':
          if (elements.isEmpty() && prev == null) {
            elements.add(new Node.Char('-'));
          } else if (minus) {
            throw error("unexpected '-'");
          } else if (prev == null) {
            if (curChar() == ']') {
              elements.add(new Node.Char('-'));
              ++cursor;
              done = true;
            } else {
              throw error("unexpected '-'");
            }
          } else {
            minus = true;
          }
          break;

        default:
          {
            Node node = ch == '\\' ? setEscape() : new Node.Char(ch);
            if (minus) {
              if (!(prev instanceof Node.Char) || !(node instanceof Node.Char)) {
                throw error("unexpected '-'");
              } else {
                elements.add(new Node.Range(((Node.Char) prev).ch, ((Node.Char) node).ch));
              }
              minus = false;
            } else if (prev != null) {
              elements.add(prev);
            }
            prev = node;
            break;
          }
      }
    }

    return new Node.Set(inverse, elements);
  }

  private Node setEscape() {
    int ch = curChar();
    ++cursor;

    switch (ch) {
      case 'd':
        return Node.CharClass.DIGIT;
      case 'D':
        return Node.CharClass.NONDIGIT;
      case 's':
        return Node.CharClass.SPACE;
      case 'S':
        return Node.CharClass.NONSPACE;
      case 'w':
        return Node.CharClass.WORD;
      case 'W':
        return Node.CharClass.NONWORD;

      case 'a':
        return new Node.Char(0x07);
      case 'b':
        return new Node.Char('\b');
      case 'f':
        return new Node.Char('\f');
      case 'n':
        return new Node.Char('\n');
      case 'r':
        return new Node.Char('\r');
      case 't':
        return new Node.Char('\t');
      case 'v':
        return new Node.Char(0x0B);

      case 'x':
        return hex2();
      case 'u':
        if (source.kind() == CharSource.Kind.BYTES) {
          throw error("unicode escape in a byte pattern");
        }
        return hex4();
      case 'U':
        if (source.kind() == CharSource.Kind.BYTES) {
          throw error("unicode escape in a byte pattern");
        }
        return hex8();
      case 'N':
        if (source.kind() == CharSource.Kind.BYTES) {
          throw error("unicode escape in a byte pattern");
        }
        return namedChar();

      default:
        break;
    }

    if (isAsciiDec(ch)) {
      if (isAsciiOct(ch)) {
        return octal(ch);
      } else {
        throw error("invalid escape");
      }
    }

    if (isAsciiLetter(ch)) {
      throw error("invalid escape");
    }

    return new Node.Char(ch);
  }

  private static Node namedChar() {
    throw new NotImplementedException("namedChar");
  }

  private int readHexChar() {
    int ch = curChar();
    ++cursor;
    if (((ch - '0') | ('9' - ch)) >= 0) {
      return ch - '0';
    } else if (((ch - 'a') | ('f' - ch)) >= 0) {
      return ch - 'a';
    } else if (((ch - 'A') | ('F' - ch)) >= 0) {
      return ch - 'A';
    } else {
      throw error("invalid hex escape");
    }
  }

  private Node hex2() {
    int value = 16 * readHexChar() + readHexChar();
    return byteChar(value);
  }

  private Node uniChar(int value) {
    if (value > UnicodeLib.MAX_CODEPOINT) {
      throw error("invalid unicode escape");
    }
    dcheck(source.kind() != CharSource.Kind.BYTES);
    return new Node.Char(value);
  }

  private Node hex4() {
    @Var int value = 0;
    for (int i = 0; i != 4; ++i) {
      value = 16 * value + readHexChar();
    }
    return uniChar(value);
  }

  private Node hex8() {
    @Var long value = 0;
    for (int i = 0; i != 8; ++i) {
      value = 16 * value + readHexChar();
    }
    if (value >= UnicodeLib.MAX_CODEPOINT) {
      throw error("invalid unicode escape");
    }
    return uniChar((int) value);
  }

  private Node character() {
    int ch = curChar();
    ++cursor;

    if (ch == '\\') {
      return escape();
    }

    return new Node.Char(ch);
  }

  private Node escape() {
    int ch = curChar();

    switch (ch) {
      case 'A':
        ++cursor;
        return Node.SpecialMatch.START;
      case 'b':
        ++cursor;
        return Node.SpecialMatch.WORD_BOUNDARY;
      case 'B':
        ++cursor;
        return Node.SpecialMatch.WORD_NONBOUNDARY;
      case 'Z':
        ++cursor;
        return Node.SpecialMatch.END;
      default:
        break;
    }

    if (isAsciiDec(ch)) {
      return backrefOrOctal();
    }

    return setEscape();
  }

  private static int numValue(int ch) {
    return ch - '0';
  }

  private static int hexValue(int ch) {
    if (Debug.ENABLED) {
      dcheck(isAsciiHex(ch));
    }

    if (isAsciiDec(ch)) {
      return ch - '0';
    } else if (((ch - 'a') | ('f' - ch)) >= 0) {
      return ch - 'a';
    } else {
      return ch - 'A';
    }
  }

  private static int numValue(int ch1, int ch2) {
    return 10 * numValue(ch1) + numValue(ch2);
  }

  private Node byteChar(int value) {
    if (value >= 128) {
      if (source.kind() != CharSource.Kind.BYTES) {
        throw error("non-unicode byte in a unicode pattern");
      }
    }

    return new Node.Char(value);
  }

  private Node octalChar(int ch1, int ch2, int ch3) {
    int value;
    if (ch3 != 0) {
      value = 64 * numValue(ch1) + 8 * numValue(ch2) + numValue(ch3);
    } else if (ch2 != 0) {
      value = 8 * numValue(ch1) + numValue(ch2);
    } else {
      value = numValue(ch1);
    }

    return byteChar(value);
  }

  private Node hexChar(int ch1, int ch2) {
    int value = 16 * hexValue(ch1) + hexValue(ch2);

    if (value >= 128 && source.kind() != CharSource.Kind.BYTES) {
      throw error("non-unicode byte in a unicode pattern");
    }

    return new Node.Char(value);
  }

  private Node octal(int ch1) {
    @Var int ch2 = 0;
    @Var int ch3 = 0;
    if (!atEof()) {
      @Var int ch = curChar();
      if (isAsciiOct(ch)) {
        ch2 = ch;
        ++cursor;
        if (!atEof()) {
          ch = curChar();
          if (isAsciiOct(ch)) {
            ch3 = ch;
            ++cursor;
          }
        }
      }
    }
    return octalChar(ch1, ch2, ch3);
  }

  private Node backrefOrOctal() {
    int ch1 = curChar();
    ++cursor;
    if (ch1 == '0') {
      return octal(ch1);
    }
    if (atEof()) {
      return new Node.CaptureRef(numValue(ch1));
    }

    int ch2 = curChar();
    if (!isAsciiDec(ch2)) {
      return new Node.CaptureRef(numValue(ch1));
    }
    ++cursor;
    if (!isAsciiOct(ch2) || atEof()) {
      return new Node.CaptureRef(numValue(ch1, ch2));
    }

    int ch3 = curChar();
    if (isAsciiOct(ch3)) {
      ++cursor;
      return octalChar(ch1, ch2, ch3);
    } else {
      return new Node.CaptureRef(numValue(ch1, ch2));
    }
  }
}
