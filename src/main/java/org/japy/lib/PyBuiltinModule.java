package org.japy.lib;

import static org.japy.infra.util.ObjUtil.or;

import java.lang.annotation.Annotation;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.types.Consumer3;
import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.ValidationError;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.PlatformLib;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.FuncHandle;
import org.japy.kernel.types.func.FuncAdapterLib;
import org.japy.kernel.types.func.PyBuiltinFunc;
import org.japy.kernel.types.mod.PyModule;
import org.japy.lib.annotations.Export;

public class PyBuiltinModule extends PyModule {
  private static final MethodHandles.Lookup DEFAULT_LOOKUP = MethodHandles.lookup();

  private final MethodHandles.@Nullable Lookup childLookup;

  protected PyBuiltinModule(String name) {
    super(name);
    childLookup = null;
    if (Debug.ENABLED) {
      validateMembers();
    }
  }

  protected PyBuiltinModule(String name, MethodHandles.Lookup lookup) {
    super(name);
    childLookup = lookup;
    if (Debug.ENABLED) {
      validateMembers();
    }
  }

  protected void exec(PyContext ctx) {
    exportContent();
  }

  protected void shutdown() {}

  private MethodHandles.Lookup lookup() {
    return or(childLookup, DEFAULT_LOOKUP);
  }

  private <A extends Annotation> void foreachAnnotatedMethod(
      @SuppressWarnings("SameParameterValue") Class<A> annClass,
      Consumer3<A, Method, FuncHandle> func) {
    for (Class<?> cls = getClass(); cls != PyBuiltinModule.class; cls = cls.getSuperclass()) {
      foreachAnnotatedMethod(cls, annClass, func);
    }
  }

  private <A extends Annotation> void foreachAnnotatedMethod(
      Class<?> moduleClass, Class<A> annClass, Consumer3<A, Method, FuncHandle> func) {
    ExcUtil.wrap(
        () -> {
          for (Method method : moduleClass.getDeclaredMethods()) {
            A ann = method.getAnnotation(annClass);
            if (ann != null) {
              @Var FuncHandle fh = FuncHandle.method(method, lookup());
              if (!Modifier.isStatic(method.getModifiers())) {
                fh = fh.bindTo(this);
              }
              func.accept(ann, method, fh);
            }
          }
        });
  }

  private static boolean isEnabled(Export ann) {
    switch (ann.platform()) {
      case ALL:
        return true;
      case WINDOWS:
        return PlatformLib.isWindows();
      case POSIX:
        return !PlatformLib.isWindows();
    }

    throw InternalErrorException.notReached();
  }

  public void exportContent() {
    foreachAnnotatedMethod(
        Export.class,
        (exp, method, mh) -> {
          if (isEnabled(exp)) {
            @Var String name = exp.value();
            if (name.isEmpty()) {
              name = method.getName();
            }
            moduleDict()
                .set(
                    name,
                    new PyBuiltinFunc(FuncAdapterLib.makeBuiltinFunc(mh, method, name), name));
          }
        });

    for (Field field : getClass().getDeclaredFields()) {
      Export exp = field.getAnnotation(Export.class);
      if (exp != null && isEnabled(exp)) {
        IPyObj value = ExcUtil.wrap(() -> (IPyObj) field.get(this));
        @Var String name = exp.value();
        if (name.isEmpty()) {
          name = field.getName();
        }
        moduleDict().set(name, value);
      }
    }
  }

  public void validateMembers() {
    @Var boolean needLookup = false;

    for (Method method : getClass().getDeclaredMethods()) {
      if (method.isAnnotationPresent(Export.class) && !Modifier.isPublic(method.getModifiers())) {
        needLookup = true;
        break;
      }
    }

    if (needLookup) {
      if (childLookup == null) {
        throw new ValidationError("lookup is not provided in class " + getClass().getSimpleName());
      }
    }

    for (Field field : getClass().getDeclaredFields()) {
      try {
        if (!field.isAnnotationPresent(Export.class)) {
          continue;
        }

        if (!IPyObj.class.isAssignableFrom(field.getType())) {
          throw new ValidationError("not a PyObj");
        }
      } catch (Throwable ex) {
        throw new InternalErrorException(
            String.format(
                "Field %s.%s: %s", getClass().getSimpleName(), field.getName(), ex.getMessage()),
            ex);
      }
    }
  }
}
