package org.japy.lib.compat;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.weak.WeakLib;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

final class _WeakRef extends PyBuiltinModule {
  _WeakRef() {
    super("_weakref", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    set("ProxyType", WeakLib.proxyType());
    set("CallableProxyType", WeakLib.callableProxyType());
    set("ref", WeakLib.referenceType());
    set("ReferenceType", WeakLib.referenceType());
  }

  @Export
  private static IPyObj getweakrefcount(Args args, PyContext ctx) {
    throw new NotImplementedException("getweakrefcount");
  }

  @Export
  private static IPyObj getweakrefs(Args args, PyContext ctx) {
    throw new NotImplementedException("getweakrefs");
  }

  @Export
  private static IPyObj proxy(Args args, PyContext ctx) {
    throw new NotImplementedException("proxy");
  }

  @Export
  private static IPyObj _remove_dead_weakref(Args args, PyContext ctx) {
    throw new NotImplementedException("_remove_dead_weakref");
  }
}
