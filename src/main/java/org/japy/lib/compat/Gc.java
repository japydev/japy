package org.japy.lib.compat;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class Gc extends PyBuiltinModule {
  Gc() {
    super("gc", MethodHandles.lookup());
  }

  @Export
  private IPyObj collect(Args args, PyContext ctx) {
    // TODO zzz
    System.gc();
    return None;
  }
}
