package org.japy.lib.compat;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.weak.WeakLib;
import org.japy.kernel.util.Args;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

final class _WeakRefSet extends PyBuiltinModule {
  _WeakRefSet() {
    super("_weakrefset", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    set("WeakSet", WeakLib.weakSetType());
  }

  @Export
  private static IPyObj _IterationGuard(Args args, PyContext ctx) {
    throw new NotImplementedException("_IterationGuard");
  }
}
