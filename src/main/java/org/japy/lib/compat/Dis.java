package org.japy.lib.compat;

import org.japy.lib.PyBuiltinModule;

class Dis extends PyBuiltinModule {
  Dis() {
    super("dis");
  }
}
