package org.japy.lib.compat;

import org.japy.lib.ModuleRegistry;

public class CompatLib {
  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new _WeakRefSet());
    moduleRegistry.register(new _WeakRef());
    moduleRegistry.register(new Gc());
    moduleRegistry.register(new Ast());
    moduleRegistry.register(new Dis());
  }
}
