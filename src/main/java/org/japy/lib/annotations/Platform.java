package org.japy.lib.annotations;

public enum Platform {
  WINDOWS,
  POSIX,
  ALL,
}
