package org.japy.lib;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.PyAttrDict;

public class PyModuleMap extends PyAttrDict {
  private static final IPyClass TYPE =
      new PyBuiltinClass("modulemap", PyModuleMap.class, PyBuiltinClass.INTERNAL);

  public PyModuleMap() {}

  private PyModuleMap(PyModuleMap dict) {
    super(dict);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj copy(PyContext ctx) {
    return new PyModuleMap(this);
  }
}
