package org.japy.lib.imp;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.Set;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.mod.PyModule;
import org.japy.kernel.util.Constants;

final class EmbeddedFinder implements AbstractFinder {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "embeddedfinder", EmbeddedFinder.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private static final Set<String> EMBEDDED_FILES;

  static {
    EMBEDDED_FILES = new HashSet<>();
    EMBEDDED_FILES.add(Constants.__JAPY__);
    EMBEDDED_FILES.add("re");
    EMBEDDED_FILES.add("importlib");
    EMBEDDED_FILES.add("_importlibutil");
    EMBEDDED_FILES.add("types");
    EMBEDDED_FILES.add("sysconfig");
    EMBEDDED_FILES.add("errno");
    EMBEDDED_FILES.add("threading");
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj findSpec(PyStr name, IPyObj path, @Nullable PyModule target, PyContext ctx) {
    if (path != None || !EMBEDDED_FILES.contains(name.toString())) {
      return None;
    }

    return ModuleSpec.makeEmbedded(name);
  }
}
