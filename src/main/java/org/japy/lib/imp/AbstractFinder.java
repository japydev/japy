package org.japy.lib.imp;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;
import static org.japy.kernel.types.misc.PyNone.None;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.mod.PyModule;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

interface AbstractFinder extends IPyTrueAtomObj, IPyNoValidationObj {
  IPyObj findSpec(PyStr name, IPyObj path, @Nullable PyModule target, PyContext ctx);

  @InstanceMethod(ImportConstants.FIND_SPEC)
  default IPyObj findSpec(
      @Param("name") IPyObj nameObj,
      @Param("path") IPyObj path,
      @Param(value = "target", dflt = DEFAULT_NONE) IPyObj target,
      PyContext ctx) {
    PyStr name = ArgLib.checkArgStr(ImportConstants.FIND_SPEC, "name", nameObj, ctx);
    if (target != None && !(target instanceof PyModule)) {
      throw PxException.typeError("target must be a module or None", ctx);
    }
    return findSpec(name, path, target != None ? (PyModule) target : null, ctx);
  }
}
