package org.japy.lib.imp;

import static org.japy.kernel.types.misc.PyNone.None;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.mod.PyModule;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

public interface AbstractLoader extends IPyTrueAtomObj, IPyNoValidationObj {
  void execModule(PyModule module, PyContext ctx);

  @InstanceMethod
  default IPyObj exec_module(@Param("module") IPyObj module, PyContext ctx) {
    if (!(module instanceof PyModule)) {
      throw PxException.typeError("expected a module, got " + PrintLib.repr(module, ctx), ctx);
    }
    execModule((PyModule) module, ctx);
    return None;
  }
}
