package org.japy.lib.imp;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.mod.PyModule;
import org.japy.lib.ModuleRegistry;

final class BuiltinLoader implements AbstractLoader {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "builtinloader", BuiltinLoader.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  public static final BuiltinLoader INSTANCE = new BuiltinLoader();

  private BuiltinLoader() {}

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public void execModule(PyModule module, PyContext ctx) {
    ((ModuleRegistry) ctx.kernel.moduleRegistry).execBuiltin(module, ctx);
  }

  @InstanceMethod(ImportConstants.CREATE_MODULE)
  IPyObj createModule(@Param("name") IPyObj name, PyContext ctx) {
    return ((ModuleRegistry) ctx.kernel.moduleRegistry).getBuiltin(name.toString());
  }
}
