package org.japy.lib.imp;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.mod.PyModule;
import org.japy.lib.ModuleRegistry;

final class BuiltinFinder implements AbstractFinder {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "builtinfinder", BuiltinFinder.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj findSpec(PyStr name, IPyObj path, @Nullable PyModule target, PyContext ctx) {
    if (path != None || !((ModuleRegistry) ctx.kernel.moduleRegistry).isBuiltin(name.toString())) {
      return None;
    }

    return ModuleSpec.makeBuiltin(name);
  }
}
