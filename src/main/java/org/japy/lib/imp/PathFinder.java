package org.japy.lib.imp;

import static org.japy.kernel.types.coll.str.PyStr.EMPTY_STRING;
import static org.japy.kernel.types.misc.PyNone.None;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Warnings;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.dict.IPyMapping;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxImportError;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.mod.PyModule;
import org.japy.kernel.types.obj.ObjLib;

final class PathFinder implements AbstractFinder {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "pathfinder", PathFinder.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public IPyObj findSpec(PyStr name, @Var IPyObj path, @Nullable PyModule target, PyContext ctx) {
    if (path == None) {
      path = ctx.kernel.sys().get(ImportConstants.PATH);
      if (path == null) {
        throw new PxImportError("sys.path is missing", ctx);
      }
    }

    List<IPyObj> nsPortionSpecs = new ArrayList<>();

    for (IPyObj entry : new JavaIterable(path, ctx)) {
      IPyObj entryFinder = getEntryFinder(entry, ctx);
      if (entryFinder != None) {
        IPyObj spec;
        if (target != null) {
          spec = FuncLib.callMeth(entryFinder, ImportConstants.FIND_SPEC, name, target, ctx);
        } else {
          spec = FuncLib.callMeth(entryFinder, ImportConstants.FIND_SPEC, name, ctx);
        }
        if (spec == None) {
          continue;
        }
        if (ObjLib.getAttr(spec, ImportConstants.SUBMODULE_SEARCH_LOCATIONS, ctx) == None
            || ObjLib.getAttrOr(spec, ImportConstants.ORIGIN, None, ctx) != None) {
          return spec;
        }
        nsPortionSpecs.add(spec);
      }
    }

    if (nsPortionSpecs.isEmpty()) {
      return None;
    }

    // TODO detect changes
    PyList searchLocations = new PyList();
    for (IPyObj spec : nsPortionSpecs) {
      searchLocations.extend(
          ObjLib.getAttr(spec, ImportConstants.SUBMODULE_SEARCH_LOCATIONS, ctx), ctx);
    }
    return ModuleSpec.makeForNsPkg(
        nsPortionSpecs.get(0), searchLocations, ImportLib.parent(name.toString()), ctx);
  }

  private static IPyObj getEntryFinder(IPyObj pathEntry, PyContext ctx) {
    if (!(pathEntry instanceof PyStr) && !(pathEntry instanceof PyBytes)) {
      Warnings.warn("sys.path entry is not a string or bytes", ctx);
      return None;
    }

    if (pathEntry.isEqual(EMPTY_STRING, ctx)) {
      return getEntryFinderForCurrentDir(ctx);
    }

    @Nullable IPyObj cache = ctx.kernel.sys().get(ImportConstants.PATH_IMPORTER_CACHE);
    if (cache == null) {
      throw new PxImportError("sys.path_importer_cache is missing", ctx);
    }
    if (!(cache instanceof IPyMapping)) {
      throw new PxImportError("sys.path_importer_cache is not a dict", ctx);
    }

    @Nullable IPyObj cachedFinder = ((IPyMapping) cache).getOrNull(pathEntry, ctx);
    if (cachedFinder != null) {
      return cachedFinder;
    }

    IPyObj finder = findEntryFinder(pathEntry, ctx);
    ((IPyMapping) cache).setItem(pathEntry, finder, ctx);
    return finder;
  }

  private static IPyObj getEntryFinderForCurrentDir(PyContext ctx) {
    String currentDir = System.getProperty("user.dir");
    if (!new File(currentDir).isDirectory()) {
      return None;
    }

    return getEntryFinder(PyStr.get(currentDir), ctx);
  }

  private static IPyObj findEntryFinder(IPyObj pathEntry, PyContext ctx) {
    @Nullable IPyObj hooks = ctx.kernel.sys().get(ImportConstants.PATH_HOOKS);
    if (hooks == null) {
      throw new PxImportError("sys.path_hooks is missing", ctx);
    }

    for (IPyObj hook : new JavaIterable(hooks, ctx)) {
      try {
        return FuncLib.call(hook, pathEntry, ctx);
      } catch (PxImportError ignored) {
        //
      }
    }

    return None;
  }
}
