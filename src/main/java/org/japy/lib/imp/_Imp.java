package org.japy.lib.imp;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.lib.PyBuiltinModule;

final class _Imp extends PyBuiltinModule {
  public _Imp() {
    super("_imp", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {}
}
