package org.japy.lib.imp;

import static org.japy.kernel.types.misc.PyNone.None;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.nio.file.Path;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.cls.builtin.PySimpleObject;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.ObjLib;

final class ModuleSpec extends PySimpleObject {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "modulespec", ModuleSpec.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  private static ModuleSpec make(
      PyStr name,
      AbstractLoader loader,
      IPyObj parent,
      IPyObj origin,
      IPyObj submoduleSearchLocations,
      boolean hasLocation) {
    ModuleSpec spec = new ModuleSpec();
    spec.dict.set(ImportConstants.NAME, name);
    spec.dict.set(ImportConstants.LOADER, loader);
    spec.dict.set(ImportConstants.PARENT, parent);
    spec.dict.set(ImportConstants.ORIGIN, origin);
    spec.dict.set(ImportConstants.CACHED, None);
    spec.dict.set(ImportConstants.SUBMODULE_SEARCH_LOCATIONS, submoduleSearchLocations);
    spec.dict.set(ImportConstants.HAS_LOCATION, PyBool.of(hasLocation));
    return spec;
  }

  static ModuleSpec makeBuiltin(PyStr name) {
    return make(name, BuiltinLoader.INSTANCE, None, ImportPyConstants.BUILT_IN, None, false);
  }

  static ModuleSpec makeEmbedded(PyStr name) {
    return make(name, EmbeddedLoader.INSTANCE, None, PyStr.get("jar:" + name), None, false);
  }

  static ModuleSpec makeForSourceFile(PyStr name, IPyObj parent, File file) {
    return make(name, SourceFileLoader.INSTANCE, parent, PyStr.get(file.toString()), None, true);
  }

  static ModuleSpec makeForPkgInit(PyStr name, IPyObj parent, Path init) {
    return make(
        name,
        SourceFileLoader.INSTANCE,
        parent,
        PyStr.get(init.toString()),
        PyList.of(PyStr.get(init.getParent().toString())),
        true);
  }

  static ModuleSpec makeForNsPkgPortion(PyStr name, Path dir) {
    ModuleSpec spec = new ModuleSpec();
    spec.dict.set(ImportConstants.NAME, name);
    spec.dict.set(ImportConstants.SUBMODULE_SEARCH_LOCATIONS, PyList.of(PyStr.get(dir.toString())));
    return spec;
  }

  public static IPyObj makeForNsPkg(
      IPyObj portionSpec, PyList searchLocations, IPyObj parent, PyContext ctx) {
    return make(
        (PyStr) ObjLib.getAttr(portionSpec, ImportConstants.NAME, ctx),
        NamespaceLoader.INSTANCE,
        parent,
        None,
        searchLocations,
        false);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
