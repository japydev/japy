package org.japy.lib.imp;

import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;

import java.nio.file.Paths;
import java.util.regex.Pattern;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.coll.StrUtil;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.ExecLib;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyROMapping;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.iter.JavaIterable;
import org.japy.kernel.types.coll.seq.IPyROSequence;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxImportError;
import org.japy.kernel.types.exc.px.PxModuleNotFoundError;
import org.japy.kernel.types.exc.px.PxRuntimeError;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.mod.PyModule;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.lib.ModuleRegistry;
import org.japy.lib.SysMod;

public final class ImportLib {
  private static final Pattern DOT = Pattern.compile("[.]");

  public static void importStar(IPyObj from, IPyDict to, PyContext ctx) {
    if (from instanceof PyModule) {
      importStarFromModule((PyModule) from, to, ctx);
    } else {
      importStarFromObject(from, to, ctx);
    }
  }

  private static void importStarFromModule(PyModule from, IPyDict to, PyContext ctx) {
    @Nullable IPyObj all = from.get(ImportConstants.__ALL__);
    if (all != null) {
      CollLib.forEach(
          all,
          name -> {
            @Nullable IPyObj obj = from.moduleDict().getOrNull(name, ctx);
            if (obj == null) {
              // zzz
              throw new PxRuntimeError(
                  String.format("there is no '%s' in module %s", name, from.name()), ctx);
            }
            to.setItem(name, obj, ctx);
          },
          ctx);
    } else {
      from.moduleDict()
          .forEach(
              (key, value) -> {
                if (key instanceof PyStr) {
                  String name = key.toString();
                  if (!name.startsWith("_")) {
                    to.setItem(key, value, ctx);
                  }
                }
              },
              ctx);
    }
  }

  private static void importStarFromObject(IPyObj from, IPyDict to, PyContext ctx) {
    IPyObj all = ObjLib.getAttrOr(from, ImportConstants.__ALL__, None, ctx);
    if (all == None) {
      throw new PxImportError(
          String.format("cannot import * from a non-module object of type '%s'", from.typeName()),
          ctx);
    }

    CollLib.forEach(
        all,
        name -> {
          IPyObj obj = ObjLib.getAttr(from, ObjLib.attrName(name, ctx), ctx);
          to.setItem(name, obj, ctx);
        },
        ctx);
  }

  public static IPyObj __import__(
      IPyObj nameObj,
      IPyObj globalsObj,
      IPyObj localsObj,
      IPyObj fromlistObj,
      IPyObj levelObj,
      PyContext ctx) {
    if (!(nameObj instanceof PyStr)) {
      throw PxException.typeError("module name must be a string", ctx);
    }
    if (globalsObj != None && !(globalsObj instanceof IPyROMapping)) {
      throw PxException.typeError("globals must be None or a dict", ctx);
    }
    if (localsObj != None && !(localsObj instanceof IPyROMapping)) {
      throw PxException.typeError("locals must be None or a dict", ctx);
    }
    if (!(fromlistObj instanceof IPyROSequence)) {
      throw PxException.typeError("fromlist must be a list or tuple", ctx);
    }
    if (!(levelObj instanceof PyInt)) {
      throw PxException.typeError("level must be an int", ctx);
    }

    IPyROMapping globals =
        globalsObj != None ? (IPyROMapping) globalsObj : ctx.thread.frame().globals();
    IPyROSequence fromlist = (IPyROSequence) fromlistObj;
    PyInt level = (PyInt) levelObj;
    if (!level.isSmall()) {
      throw PxException.typeError("level is too large", ctx);
    } else if (level.signum() == -1) {
      throw PxException.typeError("negative level", ctx);
    }

    try {
      return __import__(nameObj.toString(), globals, fromlist, level.intValue(), ctx);
    } catch (Throwable ex) {
      throw convertToImportError(ex, ctx);
    }
  }

  private static RuntimeException convertToImportError(Throwable ex, PyContext ctx) {
    return ExcUtil.rethrow(ex);
    //    if (ex instanceof PxImportError) {
    //      return (PxImportError) ex;
    //    }
    //
    //    if (ex instanceof PxBaseException) {
    //      return new PxImportError((PxBaseException) ex, ctx);
    //    }
    //
    //    if (ex instanceof InternalErrorException) {
    //      return (InternalErrorException) ex;
    //    }
    //
    //    return new PxImportError(new PxJavaException(ex, ctx), ctx);
  }

  static String[] parseModuleName(String name, PyContext ctx) {
    String[] comps = DOT.split(name, 0);
    for (String comp : comps) {
      if (comp.isEmpty()) {
        throw PxException.typeError("invalid module name '" + name + "'", ctx);
      }
    }
    return comps;
  }

  static String tail(String name) {
    int lastDot = name.lastIndexOf('.');
    return lastDot >= 0 ? name.substring(lastDot + 1) : name;
  }

  static IPyObj parent(String name) {
    int lastDot = name.lastIndexOf('.');
    return lastDot >= 0 ? PyStr.get(name.substring(0, lastDot)) : None;
  }

  private static IPyObj __import__(
      String moduleName, IPyROMapping globals, IPyROSequence fromlist, int level, PyContext ctx) {
    IPyObj module = importModule(moduleName, globals, level, ctx);

    if (fromlist.len(ctx) == 0) {
      String[] nameComps = parseModuleName(moduleName, ctx);
      return importModule(nameComps[0], globals, level, ctx);
    }

    fromlist.forEach(
        nameObj -> checkImportedName(module, moduleName, nameObj.toString(), ctx), ctx);

    return module;
  }

  private static void checkImportedName(
      IPyObj modObj, String moduleName, String name, PyContext ctx) {
    if (name.equals("*")) {
      return;
    }

    if (!(modObj instanceof PyModule)) {
      throw PxException.typeError(
          String.format("cannot import a name from an object of type '%s'", modObj.typeName()),
          ctx);
    }

    PyModule module = (PyModule) modObj;

    if (module.contains(name)) {
      return;
    }

    if (module.contains(ImportConstants.__PATH__)) {
      module.set(
          name,
          importModule((moduleName.isEmpty() ? module.name() : moduleName) + "." + name, ctx));
      return;
    }

    throw new PxImportError(
        String.format("cannot import name '%s' from '%s'", name, moduleName), ctx);
  }

  private static IPyObj importModule(String name, IPyROMapping globals, int level, PyContext ctx) {
    if (level == 0) {
      return importModule(name, ctx);
    }

    @Nullable IPyObj pkgObj = globals.getOrNull(ImportConstants.__PACKAGE__, ctx);
    if (pkgObj == null) {
      throw new PxImportError("__package__ is missing", ctx);
    }
    if (!(pkgObj instanceof PyStr)) {
      throw new PxImportError("__package__ is not a string", ctx);
    }
    String pkg = pkgObj.toString();
    if (pkg.isEmpty()) {
      throw new PxImportError("relative import not in a package", ctx);
    }
    String[] pkgComps = parseModuleName(pkg, ctx);
    if (level > pkgComps.length + 1) {
      throw new PxImportError("level is too large", ctx);
    } else if (level == pkgComps.length + 1) {
      // dots reached toplevel, like `import ..sys` in pkg/foo.py
      return importModule(name, ctx);
    } else {
      String prefix = StrUtil.join(pkgComps, ".", pkgComps.length - level + 1);
      return importModule(name.isEmpty() ? prefix : prefix + "." + name, ctx);
    }
  }

  private static @Nullable IPyObj getCachedModule(String name, PyContext ctx) {
    @Nullable IPyObj cached = ((ModuleRegistry) ctx.kernel.moduleRegistry).findModule(name);
    if (cached == null) {
      return null;
    }
    if (cached == None) {
      throw new PxModuleNotFoundError(name, ctx);
    }
    return cached;
  }

  private static IPyObj getPkgPath(PyModule module, PyContext ctx) {
    @Nullable IPyObj path = module.get(ImportConstants.__PATH__);
    if (path == null) {
      throw new PxImportError("no __path__ in module " + module.name(), ctx);
    }
    return path;
  }

  @CanIgnoreReturnValue
  public static IPyObj importModule(String name, PyContext ctx) {
    try {
      @Nullable IPyObj cached = getCachedModule(name, ctx);
      if (cached != null) {
        return cached;
      }

      String[] comps = parseModuleName(name, ctx);

      @Var
      @Nullable
      IPyObj module = null;
      for (int i = 0; i != comps.length; ++i) {
        String submoduleName = StrUtil.join(comps, ".", i + 1);
        if (i != 0) {
          // importing parent may have imported the child, e.g. this is how os.path comes to be
          @Nullable IPyObj cachedSubmodule = getCachedModule(submoduleName, ctx);
          if (cachedSubmodule != null) {
            module = cachedSubmodule;
            continue;
          }
        }

        if (module != null && !(module instanceof PyModule)) {
          throw PxException.typeError(
              String.format(
                  "cannot import a submodule from an object of type '%s'", module.typeName()),
              ctx);
        }

        IPyObj pkgPath = module != null ? getPkgPath((PyModule) module, ctx) : None;
        IPyObj submodule = importModule(submoduleName, pkgPath, ctx);
        if (module != null) {
          ((PyModule) module).set(comps[i], submodule);
        }
        module = submodule;
      }

      return dcheckNotNull(module);
    } catch (Throwable ex) {
      throw convertToImportError(ex, ctx);
    }
  }

  private static IPyObj importModule(String name, IPyObj pkgPath, PyContext ctx) {
    @Nullable IPyObj cached = getCachedModule(name, ctx);
    if (cached != null) {
      return cached;
    }

    @Nullable IPyObj metaPath = ctx.kernel.sys().get(ImportConstants.META_PATH);
    if (metaPath == null) {
      throw new PxImportError("sys.meta_path is missing", ctx);
    }

    for (IPyObj metaFinder : new JavaIterable(metaPath, ctx)) {
      IPyObj spec =
          FuncLib.callMeth(
              metaFinder, ImportConstants.FIND_SPEC, PyStr.get(name), pkgPath, None, ctx);
      if (spec != None) {
        return importModuleFromSpec(spec, ctx);
      }
    }

    throw new PxModuleNotFoundError(name, ctx);
  }

  private static PyStr getNameFromSpec(IPyObj spec, PyContext ctx) {
    IPyObj name = ObjLib.getAttr(spec, ImportConstants.NAME, ctx);
    if (!(name instanceof PyStr)) {
      throw PxException.typeError("spec.name is not a string", ctx);
    }
    return (PyStr) name;
  }

  private static IPyObj getLoaderFromSpec(IPyObj spec, PyContext ctx) {
    @Nullable IPyObj loader = ObjLib.getAttrOrNull(spec, ImportConstants.LOADER, ctx);
    if (loader == null) {
      throw new PxImportError("spec.loader is missing", ctx);
    }
    return loader;
  }

  private static IPyObj importModuleFromSpec(IPyObj spec, PyContext ctx) {
    IPyObj loader = getLoaderFromSpec(spec, ctx);
    PyStr name = getNameFromSpec(spec, ctx);

    PyModule module;
    @Nullable
    IPyObj createModule = ObjLib.getAttrOrNull(loader, ImportConstants.CREATE_MODULE, ctx);
    if (createModule != null) {
      IPyObj modObj = FuncLib.call(createModule, name, ctx);
      if (!(modObj instanceof PyModule)) {
        throw PxException.typeError(
            "expected a module object from spec.create_module(), got " + PrintLib.repr(modObj, ctx),
            ctx);
      }
      module = (PyModule) modObj;
    } else {
      module = new PyModule(name.toString());
    }

    setModuleAttrsFromSpec(module, spec, name, loader, ctx);

    ModuleRegistry registry = (ModuleRegistry) ctx.kernel.moduleRegistry;

    registry.modules.set(name.toString(), module);
    try {
      FuncLib.callMeth(loader, ImportConstants.EXEC_MODULE, module, ctx);
    } catch (Throwable ex) {
      registry.modules.del(name.toString());
      throw ex;
    }

    return dcheckNotNull(getCachedModule(name.toString(), ctx));
  }

  private static void setModuleAttrsFromSpec(
      PyModule module, IPyObj spec, PyStr name, IPyObj loader, PyContext ctx) {
    module.set(ImportConstants.__NAME__, name);
    module.set(ImportConstants.__LOADER__, loader);
    module.set(ImportConstants.__SPEC__, spec);
    IPyObj parent = ObjLib.getAttr(spec, ImportConstants.PARENT, ctx);
    module.set(ImportConstants.__PACKAGE__, parent);
    IPyObj origin = ObjLib.getAttr(spec, ImportConstants.ORIGIN, ctx);
    if (origin != None
        && ObjLib.isTrue(ObjLib.getAttr(spec, ImportConstants.HAS_LOCATION, ctx), ctx)) {
      module.set(ImportConstants.__FILE__, origin);
    }
    IPyObj cached = ObjLib.getAttr(spec, ImportConstants.CACHED, ctx);
    if (cached != None) {
      module.set(ImportConstants.__CACHED__, cached);
    }
    IPyObj path = ObjLib.getAttr(spec, ImportConstants.SUBMODULE_SEARCH_LOCATIONS, ctx);
    if (path != None) {
      module.set(ImportConstants.__PATH__, path);
      module.set(ImportConstants.__PACKAGE__, name);
    }
  }

  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new _Imp());
  }

  public static void setupSys(SysMod sys, ModuleRegistry registry, PyContext ctx) {
    sys.set(
        ImportConstants.META_PATH,
        PyList.of(new BuiltinFinder(), new EmbeddedFinder(), new PathFinder()));
    sys.set(ImportConstants.PATH_HOOKS, PyList.of(new PathHook()));
    sys.set(ImportConstants.MODULES, registry.modules);
    sys.set(ImportConstants.BUITIN_MODULE_NAMES, registry.builtinModuleNames);
    sys.set(ImportConstants.PATH, CollLib.listOf(ctx.kernel.config.libDirs));
    sys.set(ImportConstants.PATH_IMPORTER_CACHE, new PyDict());
  }

  static void execFileModule(PyModule module, PyContext ctx) {
    @Nullable IPyObj spec = module.get(ImportConstants.__SPEC__);
    if (spec == null) {
      throw InternalErrorException.notReached();
    }

    IPyObj origin = ObjLib.getAttr(spec, ImportConstants.ORIGIN, ctx);

    ExecLib.execFile(module, Paths.get(origin.toString()), ctx);
  }
}
