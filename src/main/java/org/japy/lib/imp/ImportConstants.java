package org.japy.lib.imp;

final class ImportConstants {
  public static final String NAME = "name";
  public static final String __ALL__ = "__all__";
  public static final String __NAME__ = "__name__";
  public static final String __FILE__ = "__file__";
  public static final String __CACHED__ = "__cached__";
  public static final String __PATH__ = "__path__";
  public static final String __LOADER__ = "__loader__";
  public static final String __SPEC__ = "__spec__";
  public static final String __PACKAGE__ = "__package__";
  public static final String META_PATH = "meta_path";
  public static final String LOADER = "loader";
  public static final String LOADER_STATE = "loader_state";
  public static final String CACHED = "cached";
  public static final String PARENT = "parent";
  public static final String ORIGIN = "origin";
  public static final String HAS_LOCATION = "has_location";
  public static final String SUBMODULE_SEARCH_LOCATIONS = "submodule_search_locations";
  public static final String FIND_SPEC = "find_spec";
  public static final String CREATE_MODULE = "create_module";
  public static final String EXEC_MODULE = "exec_module";
  public static final String BUILT_IN = "built-in";
  public static final String PATH = "path";
  public static final String PATH_IMPORTER_CACHE = "path_importer_cache";
  public static final String PATH_HOOKS = "path_hooks";
  public static final String BUITIN_MODULE_NAMES = "builtin_module_names";
  public static final String MODULES = "modules";
}
