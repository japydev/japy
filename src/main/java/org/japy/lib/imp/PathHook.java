package org.japy.lib.imp;

import java.io.File;
import java.lang.invoke.MethodHandles;

import org.japy.base.ParamKind;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxImportError;
import org.japy.kernel.types.func.ArgLib;
import org.japy.kernel.types.func.IPyFunc;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

final class PathHook implements IPyFunc, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "pathhook", PathHook.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  @Override
  public IPyClass type() {
    return TYPE;
  }

  private static final ArgParser ARG_PARSER =
      ArgParser.builder(1).add("path_entry", ParamKind.POS_ONLY).build();

  @Override
  public IPyObj call(Args args, PyContext ctx) {
    PyStr entry =
        ArgLib.checkArgStr("pathhook", "path_entry", ARG_PARSER.parse1(args, "pathhook", ctx), ctx);
    if (!new File(entry.toString()).isDirectory()) {
      throw new PxImportError("not a directory", ctx);
    }
    return new PathEntryFinder(entry);
  }
}
