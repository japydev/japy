package org.japy.lib.imp;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.ExecLib;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.mod.PyModule;

final class EmbeddedLoader implements AbstractLoader {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "embeddedloader", EmbeddedLoader.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  public static final EmbeddedLoader INSTANCE = new EmbeddedLoader();

  private EmbeddedLoader() {}

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @Override
  public void execModule(PyModule module, PyContext ctx) {
    ExecLib.execEmbedded(module, ctx);
  }
}
