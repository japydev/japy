package org.japy.lib.imp;

import static org.japy.kernel.types.misc.PyNone.None;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

final class PathEntryFinder implements IPyTrueAtomObj, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "pathentryfinder",
          PathEntryFinder.class,
          MethodHandles.lookup(),
          PyBuiltinClass.INTERNAL);

  private final Path dir;

  PathEntryFinder(PyStr dir) {
    this.dir = Paths.get(dir.toString());
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @InstanceMethod(ImportConstants.FIND_SPEC)
  IPyObj findSpec(@Param("name") IPyObj nameObj, PyContext ctx) {
    String leafName = ImportLib.tail(nameObj.toString());
    IPyObj parent = ImportLib.parent(nameObj.toString());

    File file = dir.resolve(leafName + ".py").toFile();
    if (file.exists()) {
      return ModuleSpec.makeForSourceFile((PyStr) nameObj, parent, file);
    }

    Path pkgDir = dir.resolve(leafName);
    if (!pkgDir.toFile().isDirectory()) {
      return None;
    }

    Path init = pkgDir.resolve("__init__.py");
    if (init.toFile().isFile()) {
      return ModuleSpec.makeForPkgInit((PyStr) nameObj, parent, init);
    }

    return ModuleSpec.makeForNsPkgPortion((PyStr) nameObj, pkgDir);
  }
}
