package org.japy.lib.imp;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.mod.PyModule;

final class NamespaceLoader implements AbstractLoader {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "namespaceloader",
          NamespaceLoader.class,
          MethodHandles.lookup(),
          PyBuiltinClass.INTERNAL);

  static final NamespaceLoader INSTANCE = new NamespaceLoader();

  private NamespaceLoader() {}

  @Override
  public void execModule(PyModule module, PyContext ctx) {}

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
