package org.japy.lib.imp;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.mod.PyModule;

final class SourceFileLoader implements AbstractLoader {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "fileloader", SourceFileLoader.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  public static final SourceFileLoader INSTANCE = new SourceFileLoader();

  private SourceFileLoader() {}

  @Override
  public void execModule(PyModule module, PyContext ctx) {
    ImportLib.execFileModule(module, ctx);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
