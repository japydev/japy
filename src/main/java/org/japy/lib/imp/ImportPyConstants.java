package org.japy.lib.imp;

import org.japy.kernel.types.coll.str.PyStr;

final class ImportPyConstants {
  public static final PyStr BUILT_IN = PyStr.ucs2(ImportConstants.BUILT_IN);
}
