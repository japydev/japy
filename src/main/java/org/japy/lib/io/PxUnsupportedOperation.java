package org.japy.lib.io;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxOSError;
import org.japy.kernel.types.exc.py.PyBaseException;

public class PxUnsupportedOperation extends PxOSError {
  public PxUnsupportedOperation(String message, PyContext ctx) {
    super(message, PyUnsupportedOperation.TYPE, new PyTuple(PyStr.get(message)), ctx, null);
  }

  /** Constructor to be used by PyExcType to create java siblings for python exceptions. */
  @Deprecated
  public PxUnsupportedOperation(PyBaseException pyExc) {
    super(pyExc);
  }
}
