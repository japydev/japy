package org.japy.lib.io;

import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

@SuppressWarnings({"UseOfSystemOutOrSystemErr", "SystemOut"})
class SillyStderr implements IPyTrueAtomObj, IPyNoValidationObj {
  private static final IPyClass TYPE =
      new PyBuiltinClass(
          "stderr", SillyStderr.class, MethodHandles.lookup(), PyBuiltinClass.INTERNAL);

  SillyStderr() {}

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @InstanceMethod
  private IPyObj write(@Param("string") IPyObj str, PyContext ctx) {
    System.err.print(str);
    return None;
  }

  @InstanceMethod
  private IPyObj flush(PyContext ctx) {
    System.err.flush();
    return None;
  }
}
