package org.japy.lib.io;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.Args;

abstract class PyBytesIO extends PyBufferedIOBase implements IPyRawIOBase {
  static final PyBuiltinClass TYPE =
      new PyBuiltinClass(
          "_io._BytesIO",
          PyBufferedIOBase.TYPE,
          PyBytesIO.class,
          MethodHandles.lookup(),
          PyBuiltinClass.CALLABLE);

  @Constructor
  public PyBytesIO(Args args, PyContext ctx) {
    super(args, ctx);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @InstanceMethod
  IPyObj getbuffer(PyContext ctx) {
    throw new NotImplementedException("getbuffer");
  }

  @InstanceMethod
  IPyObj getvalue(PyContext ctx) {
    throw new NotImplementedException("getvalue");
  }
}
