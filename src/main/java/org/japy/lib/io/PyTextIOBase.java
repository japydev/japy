package org.japy.lib.io;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NEG_ONE;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.Args;

public abstract class PyTextIOBase extends PyIOBase {
  static final PyBuiltinClass TYPE =
      new PyBuiltinClass(
          "_io._TextIOBase",
          PyIOBase.TYPE,
          PyTextIOBase.class,
          MethodHandles.lookup(),
          PyBuiltinClass.SUBCLASSABLE);

  @SubClassConstructor
  protected PyTextIOBase(Args args, PyContext ctx) {
    super(args, ctx);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @InstanceAttribute
  IPyObj encoding(PyContext ctx) {
    throw new NotImplementedException("encoding");
  }

  @InstanceAttribute
  IPyObj errors(PyContext ctx) {
    throw new NotImplementedException("errors");
  }

  @InstanceAttribute
  IPyObj newlines(PyContext ctx) {
    throw new NotImplementedException("newlines");
  }

  @InstanceAttribute
  IPyObj buffer(PyContext ctx) {
    throw new NotImplementedException("buffer");
  }

  @InstanceMethod
  IPyObj detach(PyContext ctx) {
    throw new NotImplementedException("detach");
  }

  @InstanceMethod
  IPyObj read(@Param(value = "size", dflt = DEFAULT_NEG_ONE) IPyObj size, PyContext ctx) {
    throw new NotImplementedException("read");
  }

  @InstanceMethod
  IPyObj write(@Param("s") IPyObj str, PyContext ctx) {
    throw new NotImplementedException("write");
  }
}
