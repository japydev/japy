package org.japy.lib.io;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.Args;

abstract class PyBufferedRWPair extends PyBufferedIOBase implements IPyRawIOBase {
  static final PyBuiltinClass TYPE =
      new PyBuiltinClass(
          "_io._BufferedRWPair",
          PyBufferedIOBase.TYPE,
          PyBufferedRWPair.class,
          MethodHandles.lookup(),
          PyBuiltinClass.CALLABLE);

  @Constructor
  public PyBufferedRWPair(Args args, PyContext ctx) {
    super(args, ctx);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
