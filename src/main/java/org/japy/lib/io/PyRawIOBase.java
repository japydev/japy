package org.japy.lib.io;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.Args;

public abstract class PyRawIOBase extends PyIOBase implements IPyRawIOBase {
  static final PyBuiltinClass TYPE =
      new PyBuiltinClass(
          "_io._RawIOBase",
          PyIOBase.TYPE,
          PyRawIOBase.class,
          MethodHandles.lookup(),
          PyBuiltinClass.SUBCLASSABLE);

  @SubClassConstructor
  protected PyRawIOBase(Args args, PyContext ctx) {
    super(args, ctx);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
