package org.japy.lib.io;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.Args;

abstract class PyBufferedWriter extends PyBufferedIOBase implements IPyRawIOBase {
  static final PyBuiltinClass TYPE =
      new PyBuiltinClass(
          "_io._BufferedWriter",
          PyBufferedIOBase.TYPE,
          PyBufferedWriter.class,
          MethodHandles.lookup(),
          PyBuiltinClass.CALLABLE);

  @Constructor
  public PyBufferedWriter(Args args, PyContext ctx) {
    super(args, ctx);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
