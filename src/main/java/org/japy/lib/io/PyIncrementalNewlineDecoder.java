package org.japy.lib.io;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

public class PyIncrementalNewlineDecoder implements IPyTrueAtomObj, IPyNoValidationObj {
  public static final IPyClass TYPE =
      new PyBuiltinClass(
          "_io.IncrementalNewlineDecoder",
          PyIncrementalNewlineDecoder.class,
          MethodHandles.lookup(),
          PyBuiltinClass.INTERNAL);

  @Override
  public IPyClass type() {
    return TYPE;
  }
}
