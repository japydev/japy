package org.japy.lib.io;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.ParamDefault;

interface IPyBufferedIOBase extends IPyIOBase {
  @InstanceMethod
  IPyObj detach(PyContext ctx);

  @InstanceMethod
  IPyObj read(
      @Param(value = "size", dflt = ParamDefault.DEFAULT_NEG_ONE) IPyObj size, PyContext ctx);

  @InstanceMethod
  IPyObj read1(
      @Param(value = "size", dflt = ParamDefault.DEFAULT_NEG_ONE) IPyObj size, PyContext ctx);

  @InstanceMethod
  IPyObj readinto(@Param("buf") IPyObj buf, PyContext ctx);

  @InstanceMethod
  IPyObj readinto1(@Param("buf") IPyObj buf, PyContext ctx);

  @InstanceMethod
  IPyObj write(@Param("buf") IPyObj buf, PyContext ctx);
}
