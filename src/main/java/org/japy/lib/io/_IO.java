package org.japy.lib.io;

import static org.japy.kernel.util.PyUtil.wrap;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class _IO extends PyBuiltinModule implements IIOModule {
  static final long DEFAULT_BUFFER_SIZE = 8 * 1024;

  _IO() {
    super("_io", MethodHandles.lookup());
  }

  @Override
  protected void exec(PyContext ctx) {
    super.exec(ctx);

    set("DEFAULT_BUFFER_SIZE", wrap(DEFAULT_BUFFER_SIZE));

    set("_BufferedIOBase", PyBufferedIOBase.TYPE);
    set("BufferedRandom", PyBufferedRandom.TYPE);
    set("BufferedReader", PyBufferedReader.TYPE);
    set("BufferedRWPair", PyBufferedRWPair.TYPE);
    set("BufferedWriter", PyBufferedWriter.TYPE);
    set("BytesIO", PyBytesIO.TYPE);
    set("FileIO", PyFileIO.TYPE);
    set("_IOBase", PyIOBase.TYPE);
    set("_RawIOBase", PyRawIOBase.TYPE);
    set("StringIO", PyStringIO.TYPE);
    set("_TextIOBase", PyTextIOBase.TYPE);
    set("TextIOWrapper", PyTextIOWrapper.TYPE);
    set("BlockingIOError", PyBuiltinExc.BlockingIOError);
    set("UnsupportedOperation", PyUnsupportedOperation.TYPE);
    set("IncrementalNewlineDecoder", PyIncrementalNewlineDecoder.TYPE);
  }

  @Export
  private IPyObj open(PyContext ctx) {
    throw new NotImplementedException("open");
  }

  @Export
  private IPyObj open_code(PyContext ctx) {
    throw new NotImplementedException("open_code");
  }
}
