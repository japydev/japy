package org.japy.lib.io;

import org.japy.kernel.types.IPyObj;
import org.japy.lib.ModuleRegistry;

public class IOLib {
  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new _IO());
  }

  public static IPyObj stdout() {
    return new SillyStderr();
  }

  public static IPyObj stderr() {
    return new SillyStderr();
  }
}
