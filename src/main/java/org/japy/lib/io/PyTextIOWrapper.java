package org.japy.lib.io;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.Args;

abstract class PyTextIOWrapper extends PyTextIOBase {
  static final PyBuiltinClass TYPE =
      new PyBuiltinClass(
          "_io._TextIOWrapper",
          PyTextIOBase.TYPE,
          PyTextIOWrapper.class,
          MethodHandles.lookup(),
          PyBuiltinClass.CALLABLE);

  @Constructor
  public PyTextIOWrapper(Args args, PyContext ctx) {
    super(args, ctx);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @InstanceAttribute
  IPyObj line_buffering(PyContext ctx) {
    throw new NotImplementedException("line_buffering");
  }

  @InstanceAttribute
  IPyObj write_through(PyContext ctx) {
    throw new NotImplementedException("write_through");
  }

  @InstanceMethod
  IPyObj reconfigure(PyContext ctx) {
    throw new NotImplementedException("reconfigure");
  }
}
