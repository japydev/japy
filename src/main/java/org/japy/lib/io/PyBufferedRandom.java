package org.japy.lib.io;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.Args;

abstract class PyBufferedRandom extends PyBufferedIOBase implements IPyRawIOBase {
  static final PyBuiltinClass TYPE =
      new PyBuiltinClass(
          "_io._BufferedRandom",
          PyBufferedIOBase.TYPE,
          PyBufferedRandom.class,
          MethodHandles.lookup(),
          PyBuiltinClass.CALLABLE);

  @Constructor
  public PyBufferedRandom(Args args, PyContext ctx) {
    super(args, ctx);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @InstanceMethod
  IPyObj peek(@Param(value = "size", dflt = DEFAULT_NONE) PyContext ctx) {
    throw new NotImplementedException("peek");
  }
}
