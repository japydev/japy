package org.japy.lib.io;

import java.lang.invoke.MethodHandles;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.builtin.PyBuiltinClass;
import org.japy.kernel.util.Args;

public abstract class PyBufferedIOBase extends PyIOBase implements IPyBufferedIOBase {
  static final PyBuiltinClass TYPE =
      new PyBuiltinClass(
          "_io._BufferedIOBase",
          PyIOBase.TYPE,
          PyBufferedIOBase.class,
          MethodHandles.lookup(),
          PyBuiltinClass.SUBCLASSABLE);

  @SubClassConstructor
  protected PyBufferedIOBase(Args args, PyContext ctx) {
    super(args, ctx);
  }

  @Override
  public IPyClass type() {
    return TYPE;
  }

  @InstanceAttribute
  IPyObj raw(PyContext ctx) {
    throw new NotImplementedException("raw");
  }
}
