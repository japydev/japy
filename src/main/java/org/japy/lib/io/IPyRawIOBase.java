package org.japy.lib.io;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.annotations.ParamDefault;

interface IPyRawIOBase extends IPyIOBase {
  @InstanceMethod
  IPyObj read(
      @Param(value = "size", dflt = ParamDefault.DEFAULT_NEG_ONE) IPyObj size, PyContext ctx);

  @InstanceMethod
  IPyObj readall(PyContext ctx);

  @InstanceMethod
  IPyObj readinto(@Param("buf") IPyObj buf, PyContext ctx);

  @InstanceMethod
  IPyObj write(@Param("buf") IPyObj buf, PyContext ctx);
}
