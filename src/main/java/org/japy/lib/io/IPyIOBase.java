package org.japy.lib.io;

import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NEG_ONE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_NONE;
import static org.japy.kernel.types.annotations.ParamDefault.DEFAULT_ZERO;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.util.PyUtil.wrap;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.InstanceAttribute;
import org.japy.kernel.types.annotations.InstanceMethod;
import org.japy.kernel.types.annotations.Param;
import org.japy.kernel.types.coll.iter.IPyIterable;
import org.japy.kernel.types.obj.IPyNoValidationObj;
import org.japy.kernel.types.obj.proto.IPyExitManager;
import org.japy.kernel.types.obj.proto.IPyTrueAtomObj;

public interface IPyIOBase extends IPyTrueAtomObj, IPyIterable, IPyExitManager, IPyNoValidationObj {
  boolean closed(PyContext ctx);

  void close(PyContext ctx);

  int fileno(PyContext ctx);

  void flush(PyContext ctx);

  boolean isatty(PyContext ctx);

  boolean readable(PyContext ctx);

  boolean seekable(PyContext ctx);

  boolean writable(PyContext ctx);

  IPyObj readline(IPyObj size, PyContext ctx);

  IPyObj readlines(IPyObj hint, PyContext ctx);

  long seek(IPyObj offset, IPyObj whence, PyContext ctx);

  long tell(PyContext ctx);

  long truncate(IPyObj size, PyContext ctx);

  void writelines(IPyObj lines, PyContext ctx);

  @InstanceAttribute("closed")
  default IPyObj __closed(PyContext ctx) {
    return wrap(closed(ctx));
  }

  @InstanceMethod("close")
  default IPyObj __close(PyContext ctx) {
    close(ctx);
    return None;
  }

  @InstanceMethod("fileno")
  default IPyObj __fileno(PyContext ctx) {
    return wrap(fileno(ctx));
  }

  @InstanceMethod("flush")
  default IPyObj __flush(PyContext ctx) {
    flush(ctx);
    return None;
  }

  @InstanceMethod("isatty")
  default IPyObj __isatty(PyContext ctx) {
    return wrap(isatty(ctx));
  }

  @InstanceMethod("readable")
  default IPyObj __readable(PyContext ctx) {
    return wrap(readable(ctx));
  }

  @InstanceMethod("readline")
  default IPyObj __readline(
      @Param(value = "size", dflt = DEFAULT_NEG_ONE) IPyObj size, PyContext ctx) {
    return readline(size, ctx);
  }

  @InstanceMethod("readlines")
  default IPyObj __readlines(
      @Param(value = "hint", dflt = DEFAULT_NEG_ONE) IPyObj hint, PyContext ctx) {
    return readlines(hint, ctx);
  }

  @InstanceMethod("seek")
  default IPyObj __seek(
      @Param("offset") IPyObj offset,
      @Param(value = "whence", dflt = DEFAULT_ZERO) IPyObj whence,
      PyContext ctx) {
    return wrap(seek(offset, whence, ctx));
  }

  @InstanceMethod("seekable")
  default IPyObj __seekable(PyContext ctx) {
    return wrap(seekable(ctx));
  }

  @InstanceMethod("tell")
  default IPyObj __tell(PyContext ctx) {
    return wrap(tell(ctx));
  }

  @InstanceMethod("truncate")
  default IPyObj __truncate(
      @Param(value = "size", dflt = DEFAULT_NONE) IPyObj size, PyContext ctx) {
    return wrap(truncate(size, ctx));
  }

  @InstanceMethod("writable")
  default IPyObj __writable(PyContext ctx) {
    return wrap(writable(ctx));
  }

  @InstanceMethod("writelines")
  default IPyObj __writelines(@Param("lines") IPyObj lines, PyContext ctx) {
    writelines(lines, ctx);
    return None;
  }
}
