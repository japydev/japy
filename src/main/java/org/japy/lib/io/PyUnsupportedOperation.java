package org.japy.lib.io;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.annotations.Constructor;
import org.japy.kernel.types.annotations.SubClassConstructor;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.exc.py.PyExcType;
import org.japy.kernel.types.exc.py.PyOSError;
import org.japy.kernel.util.Args;

public class PyUnsupportedOperation extends PyOSError {
  static final PyExcType TYPE =
      new PyExcType(
          "_io.UnsupportedOperation",
          new PyExcType[] {PyBuiltinExc.OSError, PyBuiltinExc.ValueError},
          PyUnsupportedOperation.class,
          PxUnsupportedOperation.class);

  @Constructor
  @SubClassConstructor
  public PyUnsupportedOperation(IPyClass cls, Args args, PyContext ctx) {
    super(cls, args, ctx);
  }
}
