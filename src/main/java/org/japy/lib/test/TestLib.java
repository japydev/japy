package org.japy.lib.test;

import org.japy.lib.ModuleRegistry;

public class TestLib {
  public static void register(ModuleRegistry moduleRegistry) {
    moduleRegistry.register(new _TestCAPI());
    moduleRegistry.register(new _TestInternalCAPI());
  }
}
