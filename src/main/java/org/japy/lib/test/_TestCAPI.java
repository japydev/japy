package org.japy.lib.test;

import java.lang.invoke.MethodHandles;

import org.japy.lib.PyBuiltinModule;

class _TestCAPI extends PyBuiltinModule {
  _TestCAPI() {
    super("_testcapi", MethodHandles.lookup());
  }
}
