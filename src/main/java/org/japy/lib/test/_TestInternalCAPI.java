package org.japy.lib.test;

import static org.japy.kernel.types.num.PyBool.True;

import java.lang.invoke.MethodHandles;

import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.lib.PyBuiltinModule;
import org.japy.lib.annotations.Export;

class _TestInternalCAPI extends PyBuiltinModule {
  _TestInternalCAPI() {
    super("_testinternalcapi", MethodHandles.lookup());
  }

  @Export
  private static IPyObj get_configs(PyContext ctx) {
    PyDict config = new PyDict();
    config.setItem("_use_peg_parser", True, ctx);
    PyDict configs = new PyDict();
    configs.setItem("config", config, ctx);
    return configs;
  }
}
