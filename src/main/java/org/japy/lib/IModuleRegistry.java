package org.japy.lib;

import org.japy.infra.validation.ISupportsValidation;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.mod.IPyModule;

public interface IModuleRegistry extends ISupportsValidation {
  IPyModule builtins();

  IPyModule sys();

  IPyModule ensureBuiltin(String name, PyContext ctx);
}
