package org.japy.lib;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.HashMap;
import java.util.Map;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.Validator;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.mod.IPyModule;
import org.japy.kernel.types.mod.PyModule;
import org.japy.kernel.util.Constants;
import org.japy.lib.compat.CompatLib;
import org.japy.lib.context.ContextLib;
import org.japy.lib.imp.ImportLib;
import org.japy.lib.io.IOLib;
import org.japy.lib.misc.MiscLib;
import org.japy.lib.net.NetLib;
import org.japy.lib.os.OsLib;
import org.japy.lib.random.RandomLib;
import org.japy.lib.re.ReLib;
import org.japy.lib.str.StrLib;
import org.japy.lib.subprocess.SubProcessLib;
import org.japy.lib.test.TestLib;
import org.japy.lib.thread.ThreadLib;

public class ModuleRegistry implements IModuleRegistry {
  public final Builtins builtins;
  public final SysMod sys;
  public final PyModuleMap modules = new PyModuleMap();
  public final PyTuple builtinModuleNames;

  private final Map<String, PyBuiltinModule> builtinModules = new HashMap<>();

  public ModuleRegistry(PyContext ctx) {
    register(new Builtins());
    register(new SysMod(this));

    MiscLib.register(this);
    ContextLib.register(this);
    OsLib.register(this);
    ImportLib.register(this);
    CompatLib.register(this);
    ReLib.register(this);
    RandomLib.register(this);
    IOLib.register(this);
    TestLib.register(this);
    ThreadLib.register(this);
    SubProcessLib.register(this);
    StrLib.register(this);
    NetLib.register(this);

    builtinModuleNames = CollLib.tupleOfStrings(builtinModules.keySet().stream().sorted());

    builtins = (Builtins) importBuiltin(Constants.BUILTINS, ctx);
    sys = (SysMod) importBuiltin(Constants.SYS, ctx);
  }

  public @Nullable IPyObj findModule(String name) {
    return modules.get(name);
  }

  public boolean isBuiltin(String name) {
    return builtinModules.containsKey(name);
  }

  public void execBuiltin(PyModule module, PyContext ctx) {
    dcheck(module instanceof PyBuiltinModule);
    ((PyBuiltinModule) module).exec(ctx);
  }

  public PyBuiltinModule getBuiltin(String name) {
    return builtinModules.computeIfAbsent(
        name,
        (n) -> {
          throw new InternalErrorException("no builtin module " + name);
        });
  }

  @Override
  public IPyModule ensureBuiltin(String name, PyContext ctx) {
    @Nullable IPyObj mod = modules.get(name);
    if (mod != null) {
      return (IPyModule) mod;
    }
    return importBuiltin(name, ctx);
  }

  private PyModule importBuiltin(String name, PyContext ctx) {
    if (Debug.ENABLED) {
      dcheck(!modules.contains(name));
    }

    @Nullable PyBuiltinModule bmod = builtinModules.get(name);
    if (bmod == null) {
      throw new InternalErrorException("no builtin module " + name);
    }

    try {
      modules.set(name, bmod);
      bmod.exec(ctx);
      return bmod;
    } catch (Throwable ex) {
      modules.del(name);
      throw ex;
    }
  }

  @Override
  public void validate(Validator v) {
    modules.forEachValue(v::validate);
  }

  public void shutdown() {}

  public void register(PyBuiltinModule module) {
    if (builtinModules.containsKey(module.name())) {
      throw new InternalErrorException(
          "builtin module " + module.name() + " is already registered");
    }
    builtinModules.put(module.name(), module);
  }

  @Override
  public IPyModule builtins() {
    return builtins;
  }

  @Override
  public IPyModule sys() {
    return sys;
  }
}
