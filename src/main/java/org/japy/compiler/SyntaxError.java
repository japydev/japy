package org.japy.compiler;

import org.japy.infra.exc.UserError;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.IHasSourceFile;
import org.japy.infra.loc.IHasWritableLocation;
import org.japy.infra.loc.IHasWritableSourceFile;
import org.japy.infra.loc.Location;

@SuppressWarnings("NonFinalFieldOfException")
public class SyntaxError extends UserError implements IHasWritableLocation, IHasWritableSourceFile {
  private Location location;
  private String sourceFile = IHasSourceFile.UNKNOWN_FILE;

  public SyntaxError(String message, IHasLocation location) {
    super(message);
    this.location = location.location();
  }

  public SyntaxError(String message, IHasLocation location, Throwable cause) {
    super(message, cause);
    this.location = location.location();
  }

  @Override
  public Location location() {
    return location;
  }

  @Override
  public String sourceFile() {
    return sourceFile;
  }

  @Override
  public void setLocation(Location location) {
    this.location = location;
  }

  @Override
  public void setSourceFile(String sourceFile) {
    this.sourceFile = sourceFile;
  }
}
