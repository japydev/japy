package org.japy.compiler.impl;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;

public enum SpecialVar {
  __DEBUG__("__debug__"),
  STOP_ASYNC_ITERATION("#StopAsyncIteration"),
  ;

  public static final String PREFIX = "#";

  public final String name;

  SpecialVar(String name) {
    this.name = name;
  }

  public static @Nullable SpecialVar get(String name) {
    if (name.equals(__DEBUG__.name)) {
      return __DEBUG__;
    }

    if (!name.startsWith(PREFIX)) {
      return null;
    }

    for (SpecialVar v : values()) {
      if (v.name.equals(name)) {
        return v;
      }
    }

    throw InternalErrorException.notReached("unknown special variable " + name);
  }
}
