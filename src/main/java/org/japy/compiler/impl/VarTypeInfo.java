package org.japy.compiler.impl;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.Type;

import org.japy.compiler.code.CVarType;
import org.japy.jexec.codegen.InstanceMethod;
import org.japy.jexec.codegen.StaticMethod;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.iter.IPyForIter;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.set.PySet;
import org.japy.kernel.types.coll.str.PyStrBuilder;
import org.japy.kernel.types.exc.py.PyBaseException;

public class VarTypeInfo {
  @SuppressWarnings("ImmutableEnumChecker")
  public enum PrimitiveType {
    INT(
        Type.INT_TYPE,
        Type.getType(Integer.class),
        StaticMethod.INTEGER_VALUEOF,
        InstanceMethod.INTEGER_INTVALUE),
    BOOL(
        Type.BOOLEAN_TYPE,
        Type.getType(Boolean.class),
        StaticMethod.BOOLEAN_VALUEOF,
        InstanceMethod.BOOLEAN_BOOLEANVALUE),
    ;

    public final Type type;
    public final Type boxType;
    public final StaticMethod box;
    public final InstanceMethod unbox;

    PrimitiveType(Type type, Type boxType, StaticMethod box, InstanceMethod unbox) {
      this.type = type;
      this.boxType = boxType;
      this.box = box;
      this.unbox = unbox;
    }
  }

  public final Type type;
  public final boolean isPyObj;
  public final String descriptor;
  private final @Nullable PrimitiveType primitiveType;

  public PrimitiveType primitiveType() {
    return dcheckNotNull(primitiveType);
  }

  public boolean isPrimitive() {
    return primitiveType != null;
  }

  public static VarTypeInfo get(CVarType type) {
    return TYPE_INFO[type.ordinal()];
  }

  private VarTypeInfo(Type type, boolean isPyObj, @Nullable PrimitiveType primitiveType) {
    this.type = type;
    this.isPyObj = isPyObj;
    this.primitiveType = primitiveType;
    this.descriptor = type.getDescriptor();
  }

  private VarTypeInfo(Type type, boolean isPyObj) {
    this(type, isPyObj, null);
  }

  private VarTypeInfo(PrimitiveType type) {
    this(type.type, false, type);
  }

  private static final VarTypeInfo[] TYPE_INFO;

  static {
    TYPE_INFO = new VarTypeInfo[CVarType.values().length];
    TYPE_INFO[CVarType.PYOBJ.ordinal()] = new VarTypeInfo(Type.getType(IPyObj.class), true);
    TYPE_INFO[CVarType.PYFORITER.ordinal()] = new VarTypeInfo(Type.getType(IPyForIter.class), true);
    TYPE_INFO[CVarType.PYEXC.ordinal()] =
        new VarTypeInfo(Type.getType(PyBaseException.class), true);
    TYPE_INFO[CVarType.PYLIST.ordinal()] = new VarTypeInfo(Type.getType(PyList.class), true);
    TYPE_INFO[CVarType.PYSET.ordinal()] = new VarTypeInfo(Type.getType(PySet.class), true);
    TYPE_INFO[CVarType.PYDICT.ordinal()] = new VarTypeInfo(Type.getType(PyDict.class), true);
    TYPE_INFO[CVarType.STRB.ordinal()] = new VarTypeInfo(Type.getType(PyStrBuilder.class), false);
    TYPE_INFO[CVarType.EXC.ordinal()] = new VarTypeInfo(Type.getType(Throwable.class), false);
    TYPE_INFO[CVarType.BOOL.ordinal()] = new VarTypeInfo(PrimitiveType.BOOL);
    TYPE_INFO[CVarType.INT.ordinal()] = new VarTypeInfo(PrimitiveType.INT);
  }

  @SuppressWarnings("unused")
  private static void dummy(CVarType type) {
    switch (type) {
      case STRB:
      case INT:
      case PYEXC:
      case PYDICT:
      case PYLIST:
      case PYSET:
      case BOOL:
      case PYOBJ:
      case PYFORITER:
      case EXC:
        break;
    }
  }
}
