package org.japy.compiler.impl;

import java.util.Arrays;

import org.japy.base.BinOp;
import org.japy.base.LogicalOp;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.num.PyInt;
import org.japy.parser.python.ast.expr.*;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.*;
import org.japy.parser.python.impl.ParserUtil;

abstract class CodeWriter {
  private final Context ctx;
  private final ITokenSpan location;

  public CodeWriter(Context ctx, ITokenSpan location) {
    this.ctx = ctx;
    this.location = location;
  }

  public static void writeCode(CodeWriter w) {
    w.write();
  }

  abstract void write();

  public AstCall call(IAstExpr func, IAstExpr... args) {
    return new AstCall(
        func, Arrays.stream(args).map(AstPosArg::new).toArray(IAstArg[]::new), func.end());
  }

  public AstCall call(String func, IAstExpr... args) {
    return call(id(func), args);
  }

  public AstCall call(SpecialFunc func, IAstExpr... args) {
    return call(id(func), args);
  }

  public AstCall call(IAstExpr func, LocalVar arg) {
    return call(func, arg.identifier);
  }

  public AstCall call(IAstExpr func, TempVar arg) {
    return call(func, arg.identifier);
  }

  public AstCall call(String func, LocalVar arg) {
    return call(func, arg.identifier);
  }

  public AstCall call(String func, TempVar arg) {
    return call(func, arg.identifier);
  }

  public AstCall call(SpecialFunc func, TempVar arg) {
    return call(func, arg.identifier);
  }

  public AstCall call(LocalVar func, LocalVar arg) {
    return call(func.identifier, arg.identifier);
  }

  public AstCall call(TempVar func, TempVar arg) {
    return call(func.identifier, arg.identifier);
  }

  public AstCall type(IAstExpr obj) {
    return call(SpecialFunc.TYPE, obj);
  }

  public AstCall type(TempVar var) {
    return type(var.identifier);
  }

  public AstIdentifier id(String name) {
    return ParserUtil.nonMangledId(name, location.start());
  }

  public AstIdentifier id(SpecialFunc func) {
    return id(func.name);
  }

  public AstIdentifier id(SpecialVar var) {
    return id(var.name);
  }

  public AstAttrRef attr(IAstExpr obj, String attr) {
    return new AstAttrRef(obj, id(attr));
  }

  public AstStar star(IAstExpr expr) {
    return new AstStar(expr, expr.start());
  }

  public AstStar starSysExcInfo() {
    return new AstStar(call(SpecialFunc.SYS_EXC_INFO), location.start());
  }

  public AstLogicalOp and(IAstExpr lhs, IAstExpr rhs) {
    return new AstLogicalOp(LogicalOp.AND, lhs, rhs);
  }

  public AstNot not(IAstExpr expr) {
    return new AstNot(expr.start(), expr);
  }

  public void eval(IAstExpr expr) {
    ctx.compiler.compileExprStmt(expr);
  }

  public AstSubscript subscript(IAstExpr obj, IAstExpr index) {
    return new AstSubscript(obj, index, index.start(), index.end());
  }

  public AstSlice slice(IAstExpr start, IAstExpr end) {
    return new AstSlice(start.end(), null, start, end, null);
  }

  public AstConst value(IPyObj value) {
    return new AstConst(value, location.start());
  }

  public AstConst value(int value) {
    return new AstConst(PyInt.get(value), location.start());
  }

  public IAstExpr binOp(BinOp op, IAstExpr lhs, IAstExpr rhs) {
    return new AstBinOp(op, lhs, rhs);
  }

  public AstWalrus walrus(AstIdentifier identifier, IAstExpr value) {
    return new AstWalrus(identifier, value);
  }

  public AstWalrus walrus(LocalVar target, IAstExpr value) {
    return walrus(target.identifier, value);
  }

  public AstWalrus walrus(TempVar target, IAstExpr value) {
    return walrus(target.identifier, value);
  }

  public AstAwait await(IAstExpr expr) {
    return new AstAwait(expr.start(), expr);
  }

  public void if_(IAstExpr expr, Runnable then) {
    ctx.compiler.compileIf(expr, then);
  }

  public void assign(IAstExpr target, IAstExpr value) {
    ctx.compiler.compileAssign(target, value);
  }

  public void assign(IAstExpr target, LocalVar value) {
    ctx.compiler.compileAssign(target, value.identifier);
  }

  public void assign(IAstExpr target, TempVar value) {
    ctx.compiler.compileAssign(target, value.identifier);
  }

  public void assign(LocalVar target, IAstExpr value) {
    ctx.compiler.compileAssign(target.identifier, value);
  }

  public void assign(TempVar target, IAstExpr value) {
    ctx.compiler.compileAssign(target.identifier, value);
  }

  public void assign(TempVar target, IPyObj value) {
    assign(target, value(value));
  }

  public void nonlocal(LocalVar var) {
    ctx.nonlocalDecl(var.identifier);
  }

  public void stmt(IAstExpr expr) {
    ctx.compiler.compileExprStmt(expr);
  }

  public void yield_(IAstExpr expr) {
    ctx.compiler.compileExprStmt(new AstYieldExpr(expr.start(), expr));
  }

  public void raise() {
    ctx.compiler.compileRaise(null, null, location);
  }

  public void raise(IAstExpr expr) {
    ctx.compiler.compileRaise(expr, null, location);
  }
}
