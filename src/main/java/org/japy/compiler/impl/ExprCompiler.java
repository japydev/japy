package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CInstr0.Code.*;
import static org.japy.compiler.code.instr.CInstrI.Code.*;
import static org.japy.compiler.code.instr.CInstrI2.Code.*;
import static org.japy.compiler.code.instr.CInstrR.Code.*;
import static org.japy.compiler.code.instr.CInstrS.Code.*;
import static org.japy.compiler.code.instr.CJump.Code.*;
import static org.japy.compiler.impl.instr.CTInstrN.Code.*;
import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.misc.PyNone.None;

import java.util.Arrays;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.IntConsumer;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.LogicalOp;
import org.japy.base.RFConversion;
import org.japy.compiler.CompilerConfig;
import org.japy.compiler.SyntaxError;
import org.japy.compiler.code.CVarType;
import org.japy.compiler.code.instr.CInstr0;
import org.japy.compiler.code.instr.CInstrI;
import org.japy.compiler.code.instr.CLabel;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.validation.Debug;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.parser.python.Parser;
import org.japy.parser.python.ast.expr.*;
import org.japy.parser.python.ast.node.*;
import org.japy.parser.python.ast.visit.AstExprVisit;
import org.japy.parser.python.ast.visit.BaseAstExprVisitor;
import org.japy.parser.python.ast.visit.IAstExprVisitor;

final class ExprCompiler {
  private final Context ctx;
  private final ICompiler compiler;
  private final CompilerConfig config;
  private final Visitor visitor = new Visitor();
  private final Visitor0 visitor0 = new Visitor0();
  private final BoolVisitor boolVisitor = new BoolVisitor();

  ExprCompiler(Context ctx, ICompiler compiler, CompilerConfig config) {
    this.ctx = ctx;
    this.compiler = compiler;
    this.config = config;
  }

  public void compile(IAstExpr expr) {
    visitor.compile(expr);
  }

  public void compile(IAstExpr expr1, IAstExpr expr2) {
    if (expr1.canSuspend() || expr2.canSuspend()) {
      throw new NotImplementedException("suspend");
    }
    compile(expr1);
    compile(expr2);
  }

  public void compileBool(IAstExpr expr) {
    boolVisitor.compileBool(expr);
  }

  public void compile0(IAstExpr expr) {
    visitor0.compile0(expr);
  }

  public void compileList(int length, IntConsumer writeElement, IHasLocation location) {
    ctx.write(NEWLIST, length, location);
    for (int i = 0; i != length; ++i) {
      writeElement.accept(i);
      ctx.write(LISTAPPEND, location);
    }
  }

  private static class ResultVar {
    enum Kind {
      TEMP,
      IDENTIFIER,
    }

    final Kind kind;
    final Object obj;

    ResultVar(AstIdentifier identifier) {
      this.kind = Kind.IDENTIFIER;
      this.obj = identifier;
    }

    ResultVar(TempVar var) {
      this.kind = Kind.TEMP;
      this.obj = var;
    }

    AstIdentifier identifier() {
      dcheck(kind == Kind.IDENTIFIER);
      return (AstIdentifier) obj;
    }

    TempVar temp() {
      dcheck(kind == Kind.TEMP);
      return (TempVar) obj;
    }
  }

  private void load(ResultVar rv, IHasLocation location) {
    switch (rv.kind) {
      case IDENTIFIER:
        compile(rv.identifier());
        break;
      case TEMP:
        ctx.write(LOAD, rv.temp(), location);
        break;
    }
  }

  private void popIfTemp(ResultVar v, IHasLocation location) {
    switch (v.kind) {
      case TEMP:
        ctx.popTemp(v.temp(), location);
        break;
      case IDENTIFIER:
        break;
    }
  }

  private ResultVar compileAsVar(IAstExpr expr) {
    if (expr instanceof AstIdentifier) {
      AstIdentifier identifier = (AstIdentifier) expr;
      // If it's not a temp variable then it could be modified
      // while the generator is suspended, so it's not safe
      // to just use it, we still need a temp.
      if (identifier.name.startsWith(Constants.TEMP_PREFIX)) {
        return new ResultVar(identifier);
      }
    }

    TempVar v = ctx.pushTemp("t", CVarType.PYOBJ, expr);
    compile(expr);
    ctx.write(STORE, v, expr);
    return new ResultVar(v);
  }

  private static class Count {
    private static final int LOTS = Integer.MAX_VALUE / 2;

    private static boolean isLots(int count) {
      return count >= LOTS;
    }
  }

  private enum ArgCount {
    ZERO(0),
    ONE(1),
    TWO(2),
    VAR(Integer.MIN_VALUE / 2),
    ;

    final int count;

    ArgCount(int count) {
      this.count = count;
    }
  }

  private ArgCount writeFixedPosArgs(IAstArg[] args, BiConsumer<IAstExpr, Integer> loadRawArg) {
    switch (args.length) {
      case 1:
        loadRawArg.accept(((AstPosArg) args[0]).value, 0);
        return ArgCount.ONE;

      case 2:
        loadRawArg.accept(((AstPosArg) args[0]).value, 0);
        loadRawArg.accept(((AstPosArg) args[1]).value, 1);
        return ArgCount.TWO;

      default:
        break;
    }

    ctx.write(NEWARGSB, args.length, 0, args[0]);
    for (int i = 0; i != args.length; ++i) {
      loadRawArg.accept(((AstPosArg) args[i]).value, i);
      ctx.write(ADDARG, args[i]);
    }
    ctx.write(BUILDARGS, args[0]);
    return ArgCount.VAR;
  }

  private ArgCount writePosArgsMaybeStarred(
      IAstArg[] args, int initialCapacity, BiConsumer<IAstExpr, Integer> loadRawArg) {
    ctx.write(NEWARGSB, initialCapacity, 0, args[0]);
    for (int i = 0; i != args.length; ++i) {
      IAstArg arg = args[i];
      AstPosArg posArg = (AstPosArg) arg;
      if (posArg.value.kind() == AstExprKind.STAR) {
        loadRawArg.accept(((AstStar) posArg.value).list, i);
        ctx.write(ADDARGTUPLE, arg);
      } else {
        loadRawArg.accept(posArg.value, i);
        ctx.write(ADDARG, arg);
      }
    }
    ctx.write(BUILDARGS, args[0]);
    return ArgCount.VAR;
  }

  private void collectFixedPosArgs(IAstArg[] args, BiConsumer<IAstExpr, Integer> loadRawArg) {
    for (int i = 0; i != args.length; ++i) {
      IAstArg arg = args[i];
      switch (arg.argKind()) {
        case POSITIONAL:
          {
            AstPosArg posArg = (AstPosArg) arg;
            if (((AstPosArg) arg).value.kind() != AstExprKind.DOUBLE_STAR) {
              loadRawArg.accept(posArg.value, i);
              ctx.write(ADDARG, arg);
            }
            break;
          }

        case KEYWORD:
          break;
      }
    }
  }

  private void collectPosArgsMaybeStarred(
      IAstArg[] args, BiConsumer<IAstExpr, Integer> loadRawArg) {
    for (int i = 0; i != args.length; ++i) {
      IAstArg arg = args[i];
      switch (arg.argKind()) {
        case POSITIONAL:
          {
            AstPosArg posArg = (AstPosArg) arg;
            if (posArg.value.kind() != AstExprKind.DOUBLE_STAR) {
              if (posArg.value.kind() == AstExprKind.STAR) {
                loadRawArg.accept(((AstStar) posArg.value).list, i);
                ctx.write(ADDARGTUPLE, arg);
              } else {
                loadRawArg.accept(posArg.value, i);
                ctx.write(ADDARG, arg);
              }
            }
            break;
          }

        case KEYWORD:
          break;
      }
    }
  }

  private void collectKwArgs(IAstArg[] args, BiConsumer<IAstExpr, Integer> loadRawArg) {
    for (int i = 0; i != args.length; ++i) {
      IAstArg arg = args[i];
      switch (arg.argKind()) {
        case POSITIONAL:
          {
            AstPosArg posArg = (AstPosArg) arg;
            if (posArg.value.kind() == AstExprKind.DOUBLE_STAR) {
              loadRawArg.accept(((AstDoubleStar) posArg.value).dict, i);
              ctx.write(ADDKWARGDICT, arg);
              break;
            }
            break;
          }

        case KEYWORD:
          {
            AstKwArg kwArg = (AstKwArg) arg;
            loadRawArg.accept(kwArg.value, i);
            ctx.write(ADDKWARG, kwArg.name.name, arg);
            break;
          }
      }
    }
  }

  private static int estimateCount(int count) {
    return Count.isLots(count) ? -1 : count;
  }

  void compileArgs(IAstArg[] args, IHasLocation location) {
    if (AstNode.canSuspend(args)) {
      throw new NotImplementedException("suspend");
    }

    ArgCount count = writeArgs(args, (o, i) -> compile(o));
    switch (count) {
      case ZERO:
        ctx.write(LOADNULLARGS, location);
        break;
      case ONE:
        ctx.write(MAKEARGS, 1, location);
        break;
      case TWO:
        ctx.write(MAKEARGS, 2, location);
        break;
      case VAR:
        break;
    }
  }

  private ArgCount writeArgs(IAstArg[] args, BiConsumer<IAstExpr, Integer> loadRawArg) {
    if (args.length == 0) {
      return ArgCount.ZERO;
    }

    @Var int posCount = 0;
    @Var boolean seenStar = false;
    @Var int kwCount = 0;

    for (IAstArg arg : args) {
      switch (arg.argKind()) {
        case POSITIONAL:
          {
            AstPosArg posArg = (AstPosArg) arg;
            switch (posArg.value.kind()) {
              case STAR:
                seenStar = true;
                ++posCount;
                break;
              case DOUBLE_STAR:
                kwCount = Count.LOTS;
                break;
              default:
                ++posCount;
                break;
            }
            break;
          }

        case KEYWORD:
          ++kwCount;
          break;
      }
    }

    if (kwCount == 0) {
      if (!seenStar) {
        return writeFixedPosArgs(args, loadRawArg);
      } else {
        return writePosArgsMaybeStarred(args, posCount, loadRawArg);
      }
    }

    ctx.write(NEWARGSB, posCount, estimateCount(kwCount), args[0]);

    if (posCount != 0) {
      if (seenStar) {
        collectPosArgsMaybeStarred(args, loadRawArg);
      } else {
        collectFixedPosArgs(args, loadRawArg);
      }
    }

    collectKwArgs(args, loadRawArg);
    ctx.write(BUILDARGS, args[0]);
    return ArgCount.VAR;
  }

  private void compileCall(
      AstCall expr,
      boolean needRetVal,
      Consumer<IAstExpr> loadFunc,
      BiConsumer<IAstExpr, Integer> loadRawArg) {

    loadFunc.accept(expr.func);

    ArgCount argCount = writeArgs(expr.args, loadRawArg);
    switch (argCount) {
      case ZERO:
      case ONE:
      case TWO:
        ctx.write(needRetVal ? CALL : VCALL, argCount.count, expr);
        break;
      case VAR:
        ctx.write(needRetVal ? CALLARGS : VCALLARGS, expr);
        break;
    }
  }

  private void compileCall(AstCall expr, boolean needRetVal) {
    if (compileSpecialCall(expr, needRetVal)) {
      return;
    }

    if (AstNode.canSuspend(expr.args)) {
      compileCallWithSuspend(expr, needRetVal);
      return;
    }

    compileCall(expr, needRetVal, this::compile, (arg, iArg) -> compile(arg));
  }

  private void compileCallWithSuspend(AstCall call, boolean needRetVal) {
    ResultVar func = compileAsVar(call.func);

    int argCount = call.args.length;
    ResultVar[] args = new ResultVar[argCount];
    for (int i = 0; i != argCount; ++i) {
      IAstArg arg = call.args[i];
      switch (arg.argKind()) {
        case POSITIONAL:
          {
            AstPosArg posArg = (AstPosArg) arg;
            switch (posArg.value.kind()) {
              case STAR:
                args[i] = compileAsVar(((AstStar) posArg.value).list);
                break;
              case DOUBLE_STAR:
                args[i] = compileAsVar(((AstDoubleStar) posArg.value).dict);
                break;
              default:
                args[i] = compileAsVar(posArg.value);
                break;
            }
            break;
          }
        case KEYWORD:
          {
            AstKwArg kwArg = (AstKwArg) arg;
            args[i] = compileAsVar(kwArg.value);
            break;
          }
      }
    }

    compileCall(call, needRetVal, o -> load(func, o), (arg, iArg) -> load(args[iArg], arg));

    for (int i = argCount - 1; i >= 0; --i) {
      popIfTemp(args[i], call.end().end);
    }
    popIfTemp(func, call.end().end);
  }

  private static void writeSimpleArgs(
      IAstArg[] args, int count, BiConsumer<IAstExpr, Integer> loadArg) {
    dcheck(args.length == count);
    for (int i = 0; i != count; ++i) {
      dcheck(args[i] instanceof AstPosArg);
      loadArg.accept(((AstPosArg) args[i]).value, i);
    }
  }

  private boolean compileSpecialCall(
      SpecialFunc func, AstCall call, boolean needValue, BiConsumer<IAstExpr, Integer> loadArg) {
    switch (func) {
      case NOT_NULL:
        writeSimpleArgs(call.args, 1, loadArg);
        ctx.write(ISNOTNULL, call);
        dcheck(needValue);
        break;

      case MAKEFORITER:
        writeSimpleArgs(call.args, 1, loadArg);
        ctx.write(MAKEFORITER, call);
        dcheck(needValue);
        break;

      case FORITERNEXT:
        writeSimpleArgs(call.args, 1, loadArg);
        ctx.write(FORITERNEXT, call);
        dcheck(needValue);
        break;

      case SYS_EXC_INFO:
        dcheck(call.args.length == 0);
        ctx.write(SYSEXCINFO, call);
        dcheck(needValue);
        break;

      case NEW_LIST:
        dcheck(call.args.length == 0);
        ctx.write(NEWLIST, -1, call);
        dcheck(needValue);
        break;

      case LIST_ADD:
        writeSimpleArgs(call.args, 2, loadArg);
        ctx.write(LISTADDVOID, call);
        dcheck(!needValue);
        break;

      case NEW_SET:
        dcheck(call.args.length == 0);
        ctx.write(NEWSET, -1, call);
        dcheck(needValue);
        break;

      case SET_ADD:
        writeSimpleArgs(call.args, 2, loadArg);
        ctx.write(SETADDVOID, call);
        dcheck(!needValue);
        break;

      case NEW_DICT:
        dcheck(call.args.length == 0);
        ctx.write(NEWDICT, -1, call);
        dcheck(needValue);
        break;

      case DICT_ADD:
        writeSimpleArgs(call.args, 3, loadArg);
        ctx.write(VDICTADD, call);
        dcheck(!needValue);
        break;

      case LEN:
        writeSimpleArgs(call.args, 1, loadArg);
        ctx.write(LEN, call);
        dcheck(needValue);
        break;

      case ASSERTION_FAILED:
        writeSimpleArgs(call.args, 1, loadArg);
        ctx.write(ASSERTIONFAILED, call);
        dcheck(!needValue);
        break;

      case ASSERTION_FAILED_MSG:
        writeSimpleArgs(call.args, 2, loadArg);
        ctx.write(ASSERTIONFAILEDMSG, call);
        dcheck(!needValue);
        break;

      case TYPE:
        writeSimpleArgs(call.args, 1, loadArg);
        ctx.write(PYTYPE, call);
        dcheck(needValue);
        break;
    }

    return true;
  }

  private boolean compileSpecialCall(AstCall call, boolean needValue) {
    if (!(call.func instanceof AstIdentifier)) {
      return false;
    }

    AstIdentifier identifier = (AstIdentifier) call.func;

    if (identifier.name.equals(Constants.SUPER)) {
      return compileSuper(call, needValue);
    }

    @Nullable SpecialFunc func = SpecialFunc.get(((AstIdentifier) call.func).name);
    if (func == null) {
      return false;
    }

    if (!call.canSuspend()) {
      return compileSpecialCall(func, call, needValue, (e, i) -> compile(e));
    }

    int count = call.args.length;
    ResultVar[] temps = new ResultVar[count];
    for (int i = 0; i != count; ++i) {
      IAstArg arg = call.args[i];
      if (Debug.ENABLED) {
        dcheck(arg instanceof AstPosArg);
      }
      temps[i] = compileAsVar(((AstPosArg) arg).value);
    }

    boolean result = compileSpecialCall(func, call, needValue, (e, i) -> load(temps[i], e));
    dcheck(result);

    for (int i = count - 1; i >= 0; --i) {
      popIfTemp(temps[i], call.end().end);
    }

    return true;
  }

  private @Nullable FuncBlock getEnclosingFuncBlock() {
    for (@Nullable Block b = ctx.block(); b != null; b = b.parentBlock) {
      if (b instanceof FuncBlock && !((FuncBlock) b).isGeneratorExpr()) {
        return (FuncBlock) b;
      }
    }
    return null;
  }

  private boolean compileSuper(AstCall call, boolean needValue) {
    if (call.args.length != 0) {
      return false;
    }

    @Nullable FuncBlock func = getEnclosingFuncBlock();
    if (func == null || func.params.length == 0 || !(func.parentBlock instanceof ClassBlock)) {
      return false;
    }

    func.setNeedClassCell();

    // super() -> super(__class__, <firstarg>)
    compile(call.func);
    compile(new AstIdentifier(call.func.start(), Constants.__CLASS__));
    compile(func.params[0].name);
    ctx.write(needValue ? CALL : VCALL, 2, call);
    return true;
  }

  private void seen__class__() {
    @Nullable FuncBlock func = getEnclosingFuncBlock();
    if (func == null || !(func.parentBlock instanceof ClassBlock)) {
      return;
    }

    func.setNeedClassCell();
  }

  private boolean compileSpecialVar(AstIdentifier identifier) {
    @Nullable SpecialVar v = SpecialVar.get(identifier.name);
    if (v == null) {
      return false;
    }

    ctx.write(SPECIAL, v.ordinal(), identifier);
    return true;
  }

  void compileOrNone(@Nullable IAstExpr expr, IHasLocation location) {
    if (expr != null) {
      compile(expr);
    } else {
      ctx.write(None, location);
    }
  }

  void compileParamDefaults(FuncBlock func) {
    if (AstNode.canSuspend(func.params)) {
      throw new NotImplementedException("suspend");
    }

    boolean hasDefaults = Arrays.stream(func.params).anyMatch(p -> p.dflt != null);

    if (!hasDefaults) {
      ctx.write(LOADNULLAOBJ, func);
      return;
    }

    ctx.write(NEWARRAY, func.params.length, func);
    for (int i = 0; i != func.params.length; ++i) {
      AstParam p = func.params[i];
      ctx.write(DUPA, p);
      if (p.dflt != null) {
        if (p.dflt.canSuspend()) {
          throw new NotImplementedException("suspending param default");
        }
        compile(p.dflt);
      } else {
        ctx.write(LOADNULLOBJ, p);
      }
      ctx.write(SETARRAY, i, p);
    }
  }

  void compileAnnotation(IAstExpr annotation) {
    if (compiler.annotationsAsStrings()) {
      String text = compiler.getText(annotation);
      ctx.write(PyStr.get(text), annotation);
      if (Debug.ENABLED) {
        Parser.checkExpressionSyntax(text);
      }
    } else {
      compile(annotation);
    }
  }

  void compileAnnotation0(IAstExpr annotation) {
    if (!compiler.annotationsAsStrings()) {
      compile0(annotation);
    }
  }

  void compileAnnotations(FuncBlock func) {
    @Var int count = func.retType != null ? 1 : 0;
    for (AstParam p : func.params) {
      if (p.annotation != null) {
        ++count;
      }
    }

    if (count == 0) {
      ctx.write(LOADNULLOBJ, func);
      return;
    }

    // This is just to pass the tests, I doubt anyone will really use yield or await in annotations
    if (!(ctx.scope() instanceof FuncBlock)) {
      if (AstNode.canYield(func.params)) {
        throw new SyntaxError("'yield' outside function", func.params[0]);
      }
      if (func.retType != null && func.retType.canYield()) {
        throw new SyntaxError("'yield' outside function", func.retType);
      }
      if (AstNode.canAwait(func.params)) {
        throw new SyntaxError("'await' outside function", func.params[0]);
      }
      if (func.retType != null && func.retType.canAwait()) {
        throw new SyntaxError("'await' outside function", func.retType);
      }
    }

    if (AstNode.canSuspend(func.params) || (func.retType != null && func.retType.canSuspend())) {
      throw new NotImplementedException("annotation with suspend");
    }

    ctx.write(NEWDICT, count, func);
    for (AstParam p : func.params) {
      if (p.annotation != null) {
        ctx.write(PyStr.get(p.name.name), p);
        compileAnnotation(p.annotation);
        ctx.write(DICTADD, p);
      }
    }

    if (func.retType != null) {
      ctx.write(PyStr.ucs2("return"), func.retType);
      compileAnnotation(func.retType);
      ctx.write(DICTADD, func.retType);
    }
  }

  private class Visitor implements IAstExprVisitor {
    private void compile(IAstExpr expr) {
      ConstEval.@Nullable Result r = ConstEval.tryEval(expr);
      if (r != null) {
        ctx.write(r.value, expr);
        return;
      }

      AstExprVisit.visit(expr, this);
    }

    private void appendReplField(AstReplField rf) {
      @Var
      @Nullable
      RFConversion conversion = rf.conversion;
      if (conversion == null) {
        if (rf.exprText != null) {
          conversion = RFConversion.R;
        }
      }

      if (rf.exprText != null) {
        ctx.write(STRBAPPENDS, rf.exprText, rf);
      }

      compile(rf.expr);

      if (conversion != null) {
        switch (conversion) {
          case S:
            ctx.write(STR, rf);
            break;
          case A:
            ctx.write(ASCII, rf);
            break;
          case R:
            ctx.write(REPR, rf);
            break;
        }
      }

      if (rf.formatSpec != null) {
        compile(rf.formatSpec);
      } else {
        ctx.write(PyStr.EMPTY_STRING, rf);
      }

      ctx.write(FORMAT, rf);
      ctx.write(STRBAPPEND, rf);
    }

    @Override
    public void visitString(AstString expr) {
      if (expr.canSuspend()) {
        ctx.write(NOTIMPLEMENTED, "yield/await inside f-string", expr);
        ctx.write(None, expr);
        return;
      }

      if (expr.chunks.length == 1 && expr.chunks[0] instanceof AstStringLiteral) {
        ctx.write(PyStr.get(((AstStringLiteral) expr.chunks[0]).text), expr);
        return;
      }

      ctx.write(NEWSTRB, expr);
      for (IAstStringChunk chunk : expr.chunks) {
        switch (chunk.chunkKind()) {
          case LITERAL:
            {
              AstStringLiteral literal = (AstStringLiteral) chunk;
              ctx.write(STRBAPPENDS, literal.text, literal);
              break;
            }
          case REPLFIELD:
            {
              appendReplField((AstReplField) chunk);
              break;
            }
        }
      }
      ctx.write(BUILDSTR, expr);
    }

    @Override
    public void visitIdentifier(AstIdentifier expr) {
      if (compileSpecialVar(expr)) {
        return;
      }

      if (expr.name.equals(Constants.__CLASS__)) {
        seen__class__();
      }

      Ref ref = ctx.referencedName(expr);
      switch (ref.kind()) {
        case NONLOCAL:
        case UNKNOWN:
        case GLOBAL:
        case UPVALUE:
        case LOCAL:
          ctx.write(LOADNAME, expr, expr);
          break;

        case TEMP:
          {
            TempVar var = ref.temp();
            switch (var.type) {
              case PYOBJ:
              case PYFORITER:
              case PYEXC:
              case PYDICT:
              case PYLIST:
              case PYSET:
                ctx.write(LOADNAME, var.identifier, expr);
                break;
              case STRB:
                ctx.write(LOADNAMESTRB, var.identifier, expr);
                break;
              case EXC:
                ctx.write(LOADNAMEEXC, var.identifier, expr);
                break;
              case BOOL:
                ctx.write(LOADNAMEBOOL, var.identifier, expr);
                break;
              case INT:
                throw InternalErrorException.notReached(expr);
            }
          }
      }
    }

    @Override
    public void visitWalrus(AstWalrus expr) {
      if (expr.canSuspend()) {
        throw new NotImplementedException("suspend");
      }

      ctx.block().walrusName(expr.identifier);
      compile(expr.value);
      ctx.write(DUP, expr);
      ctx.write(STORENAME, expr.identifier, expr);
    }

    private int countEntries(IAstExpr[] entries) {
      @Var int count = 0;
      for (IAstExpr expr : entries) {
        if (expr.kind() == AstExprKind.STAR) {
          return Count.LOTS;
        } else {
          ++count;
        }
      }
      return count;
    }

    private boolean makeShortSequence(
        IAstExpr expr,
        IAstExpr[] entries,
        int count,
        CInstrI.Code opCode,
        BiConsumer<IAstExpr, Integer> loadRaw) {

      if (count <= 4) {
        for (int i = 0; i != count; ++i) {
          loadRaw.accept(entries[i], i);
        }
        ctx.write(opCode, count, expr);
        return true;
      }

      return false;
    }

    private void makeSequence(
        IAstExpr expr,
        IAstExpr[] entries,
        CInstrI.Code opNewSeq,
        CInstr0.Code opAddStar,
        CInstr0.Code opAddOne,
        BiConsumer<IAstExpr, Integer> loadRaw) {
      ctx.write(opNewSeq, entries.length, expr);
      for (int i = 0; i != entries.length; ++i) {
        IAstExpr entry = entries[i];
        if (entry.kind() == AstExprKind.STAR) {
          loadRaw.accept(((AstStar) entry).list, i);
          ctx.write(opAddStar, expr);
        } else {
          loadRaw.accept(entry, i);
          ctx.write(opAddOne, expr);
        }
      }
    }

    private void visitList(AstList expr, BiConsumer<IAstExpr, Integer> loadRaw) {
      int count = countEntries(expr.entries);
      if (!makeShortSequence(expr, expr.entries, count, MAKELIST, loadRaw)) {
        makeSequence(expr, expr.entries, NEWLIST, LISTADDSTAR, LISTAPPEND, loadRaw);
      }
    }

    @Override
    public void visitList(AstList expr) {
      if (!expr.canSuspend()) {
        visitList(expr, (e, i) -> compile(e));
      } else {
        visitEntriesWithSuspend(expr.entries, loadRaw -> visitList(expr, loadRaw));
      }
    }

    private void visitSet(AstSet expr, BiConsumer<IAstExpr, Integer> loadRaw) {
      int count = countEntries(expr.entries);
      if (!makeShortSequence(expr, expr.entries, count, MAKESET, loadRaw)) {
        makeSequence(expr, expr.entries, NEWSET, SETADDSTAR, SETADD, loadRaw);
      }
    }

    @Override
    public void visitSet(AstSet expr) {
      if (!expr.canSuspend()) {
        visitSet(expr, (e, i) -> compile(e));
      } else {
        visitEntriesWithSuspend(expr.entries, loadRaw -> visitSet(expr, loadRaw));
      }
    }

    private void visitTuple(AstTuple expr, BiConsumer<IAstExpr, Integer> loadRaw) {
      int count = countEntries(expr.entries);
      if (makeShortSequence(expr, expr.entries, count, MAKETUP, loadRaw)) {
        return;
      }

      if (!Count.isLots(count)) {
        ctx.write(NEWARRAY, count, expr);
        for (int i = 0; i != expr.entries.length; ++i) {
          IAstExpr entry = expr.entries[i];
          ctx.write(DUPA, entry);
          loadRaw.accept(entry, i);
          ctx.write(SETARRAY, i, entry);
        }
        ctx.write(NEWTUPLE, expr);
        return;
      }

      makeSequence(expr, expr.entries, NEWTUPB, TUPBADDSTAR, TUPBADD, loadRaw);
      ctx.write(BUILDTUP, expr);
    }

    private void visitEntriesWithSuspend(
        IAstExpr[] entries, Consumer<BiConsumer<IAstExpr, Integer>> build) {
      int count = entries.length;
      ResultVar[] temps = new ResultVar[count];
      for (int i = 0; i != count; ++i) {
        IAstExpr entry = entries[i];
        if (entry.kind() == AstExprKind.STAR) {
          temps[i] = compileAsVar(((AstStar) entry).list);
        } else {
          temps[i] = compileAsVar(entry);
        }
      }

      build.accept((e, i) -> load(temps[i], e));

      for (int i = count - 1; i >= 0; --i) {
        popIfTemp(temps[i], entries[i]);
      }
    }

    @Override
    public void visitTuple(AstTuple expr) {
      if (!expr.canSuspend()) {
        visitTuple(expr, (e, i) -> compile(e));
      } else {
        visitEntriesWithSuspend(expr.entries, loadRaw -> visitTuple(expr, loadRaw));
      }
    }

    @Override
    public void visitDict(AstDict expr) {
      if (expr.canSuspend()) {
        throw new NotImplementedException("suspend");
      }

      ctx.write(NEWDICT, expr.entries.length, expr);
      for (IAstDictEntry entry : expr.entries) {
        switch (entry.dictEntryKind()) {
          case STARRED:
            compile(((AstDoubleStar) entry).dict);
            ctx.write(DICTADDSTAR, expr);
            break;
          case SINGLE:
            {
              AstKeyDatum d = (AstKeyDatum) entry;
              compile(d.key);
              compile(d.value);
              ctx.write(DICTADD, expr);
              break;
            }
        }
      }
    }

    private abstract class CompWriter extends CodeWriter {
      private final AstCompFor start;

      private CompWriter(AstCompFor compFor) {
        super(ctx, compFor);
        this.start = compFor;
      }

      @Override
      void write() {
        write(start);
      }

      protected abstract void writeBody();

      private void write(IAstCompIter iter) {
        switch (iter.compIterKind()) {
          case IF:
            writeIf((AstCompIf) iter);
            break;
          case FOR:
            writeFor((AstCompFor) iter);
            break;
        }
      }

      private void writeNext(IAstCompIter iter) {
        @Nullable IAstCompIter next = iter.nextCompIter();
        if (next != null) {
          write(next);
        } else {
          writeBody();
        }
      }

      private void writeIf(AstCompIf compIf) {
        compiler.compileIf(compIf.expr, () -> writeNext(compIf));
      }

      private void writeFor(AstCompFor compFor) {
        compiler.compileFor(
            compFor.async, compFor.target, compFor.list, compFor, () -> writeNext(compFor));
      }
    }

    private void writeSeqComp(
        String name,
        IAstExpr expr,
        AstCompFor compFor,
        CVarType type,
        BiConsumer<CodeWriter, AstIdentifier> writeBody) {
      TempVar t = ctx.pushTemp(name, type, expr);

      CompBlock compBlock = new CompBlock(ctx, expr);

      SpecialFunc constructor;
      switch (type) {
        case PYLIST:
          constructor = SpecialFunc.NEW_LIST;
          break;
        case PYSET:
          constructor = SpecialFunc.NEW_SET;
          break;
        case PYDICT:
          constructor = SpecialFunc.NEW_DICT;
          break;
        default:
          throw InternalErrorException.notReached();
      }

      CodeWriter.writeCode(
          new CompWriter(compFor) {
            @Override
            void write() {
              assign(t, call(constructor));
              super.write();
            }

            @Override
            protected void writeBody() {
              writeBody.accept(this, t.identifier);
            }
          });

      compBlock.end(ctx, expr);

      ctx.write(LOADNAME, t.identifier, expr);
      ctx.popTemp(t, expr);
    }

    @Override
    public void visitListComp(AstListComp expr) {
      writeSeqComp(
          "$list",
          expr,
          expr.compFor,
          CVarType.PYLIST,
          (w, temp) -> w.stmt(w.call(SpecialFunc.LIST_ADD, temp, expr.expr)));
    }

    @Override
    public void visitSetComp(AstSetComp expr) {
      writeSeqComp(
          "$set",
          expr,
          expr.compFor,
          CVarType.PYSET,
          (w, temp) -> w.stmt(w.call(SpecialFunc.SET_ADD, temp, expr.expr)));
    }

    @Override
    public void visitDictComp(AstDictComp expr) {
      writeSeqComp(
          "$dict",
          expr,
          expr.compFor,
          CVarType.PYDICT,
          (w, temp) -> w.stmt(w.call(SpecialFunc.DICT_ADD, temp, expr.key, expr.value)));
    }

    @Override
    public void visitGenerator(AstGenerator expr) {
      // eagerly evaluate leftmost iterable and make sure it's indeed an iterable
      TempVar t = ctx.pushTemp("list", CVarType.PYOBJ, expr);
      compile(expr.compFor.list);
      ctx.write(ITER, expr);
      ctx.write(STORE, t, expr);

      FuncBlock f = new FuncBlock(ctx, expr);

      AstCompFor cf =
          new AstCompFor(
              expr.compFor.async,
              expr.compFor.target,
              t.identifier,
              expr.compFor.next,
              expr.compFor.start());
      CodeWriter.writeCode(
          new CompWriter(cf) {
            @Override
            protected void writeBody() {
              yield_(expr.expr);
            }
          });

      f.end(ctx, expr);
      ctx.write(CALL, 0, expr);

      ctx.popTemp(t, expr);
    }

    @Override
    public void visitParen(AstParen expr) {
      compile(expr.expr);
    }

    @Override
    public void visitCall(AstCall expr) {
      compileCall(expr, true);
    }

    @Override
    public void visitAttrRef(AstAttrRef expr) {
      if (expr.canSuspend()) {
        throw new NotImplementedException("suspend");
      }

      compile(expr.obj);
      ctx.write(GETATTR, expr.attr.name, expr);
    }

    @Override
    public void visitSubscript(AstSubscript expr) {
      if (expr.canSuspend()) {
        throw new NotImplementedException("suspend");
      }

      compile(expr.obj);
      compile(expr.index);
      ctx.write(GETITEM, expr);
    }

    @Override
    public void visitNot(AstNot expr) {
      compile(expr.expr);
      ctx.write(NOTOBJ, expr);
    }

    private void visitComparison(AstComparison expr, BiConsumer<IAstExpr, Integer> loadOperand) {
      if (expr.ops.length == 1) {
        loadOperand.accept(expr.operands[0], 0);
        loadOperand.accept(expr.operands[1], 1);
        ctx.write(COMPOP, expr.ops[0].ordinal(), expr);
        return;
      }

      CLabel done = ctx.newLabel();
      CLabel shortCircuit = ctx.newLabel();
      loadOperand.accept(expr.operands[0], 0);
      for (int i = 0; i != expr.ops.length; ++i) {
        IAstExpr next = expr.operands[i + 1];
        loadOperand.accept(next, i + 1);
        boolean last = i == expr.ops.length - 1;
        if (!last) {
          ctx.write(DUPX1, next);
        }
        ctx.write(COMPOP, expr.ops[i].ordinal(), next);
        if (!last) {
          ctx.write(DUP, next);
          ctx.jump(IFFALSEOBJ, shortCircuit, next);
          ctx.write(POP, next);
        } else {
          ctx.jump(done, next);
        }
      }
      ctx.write(shortCircuit, expr);
      ctx.write(SWAP, expr);
      ctx.write(POP, expr);
      ctx.write(done, expr);
    }

    private void visitComparisonWithSuspend(AstComparison expr) {
      int count = expr.operands.length;
      ResultVar[] temps = new ResultVar[count];
      for (int i = 0; i != count; ++i) {
        temps[i] = compileAsVar(expr.operands[i]);
      }

      visitComparison(expr, (e, i) -> load(temps[i], e));

      for (int i = count - 1; i >= 0; --i) {
        popIfTemp(temps[i], expr.operands[i]);
      }
    }

    @Override
    public void visitComparison(AstComparison expr) {
      if (expr.canSuspend()) {
        visitComparisonWithSuspend(expr);
        return;
      }

      visitComparison(expr, (e, i) -> compile(e));
    }

    @Override
    public void visitBinOp(AstBinOp expr) {
      if (!expr.rhs.canSuspend()) {
        compile(expr.lhs);
        compile(expr.rhs);
        ctx.write(BINOP, expr.op.ordinal(), expr);
        return;
      }

      ResultVar lhs = compileAsVar(expr.lhs);
      compile(expr.rhs);
      load(lhs, expr.lhs);
      ctx.write(SWAP, expr);
      ctx.write(BINOP, expr.op.ordinal(), expr);
      popIfTemp(lhs, expr);
    }

    @Override
    public void visitUnaryOp(AstUnaryOp expr) {
      compile(expr.operand);
      ctx.write(UNARYOP, expr.op.ordinal(), expr);
    }

    @Override
    public void visitLogicalOp(AstLogicalOp expr) {
      if (expr.canSuspend()) {
        throw new NotImplementedException("suspend");
      }

      CLabel done = ctx.newLabel();
      compile(expr.lhs);
      ctx.write(DUP, expr.lhs);
      ctx.jump(expr.op == LogicalOp.AND ? IFFALSEOBJ : IFTRUEOBJ, done, expr.lhs);
      ctx.write(POP, expr.lhs);
      compile(expr.rhs);
      ctx.write(done, expr);
    }

    @Override
    public void visitConditional(AstConditional expr) {
      if (expr.canSuspend()) {
        throw new NotImplementedException("suspend");
      }

      compile(expr.condition);
      CLabel ifFalse = ctx.newLabel();
      ctx.jump(IFFALSEOBJ, ifFalse, expr.condition);
      compile(expr.ifTrue);
      CLabel done = ctx.newLabel();
      ctx.jump(done, expr.ifTrue);
      ctx.write(ifFalse, expr.ifFalse);
      compile(expr.ifFalse);
      ctx.write(done, expr);
    }

    @Override
    public void visitSlice(AstSlice expr) {
      if (expr.canSuspend()) {
        throw new NotImplementedException("suspend");
      }

      compileOrNone(expr.lowerBound, expr);
      compileOrNone(expr.upperBound, expr);
      compileOrNone(expr.step, expr);
      ctx.write(MAKESLICE, expr);
    }

    private void checkYieldAllowed(IAstExpr expr, boolean yieldFrom) {
      Block scope = ctx.scope();
      if (!(scope instanceof FuncBlock)) {
        throw new SyntaxError("yield not in a function", expr);
      }
      if (ctx.block() instanceof CompBlock) {
        throw new SyntaxError("yield inside comprehension", expr);
      }
      dcheck(((FuncBlock) scope).isGenerator());
      if (yieldFrom && ((FuncBlock) scope).isAsync()) {
        throw new SyntaxError("yield from in a coroutine", expr);
      }
    }

    @Override
    public void visitYield(AstYieldExpr expr) {
      Block scope = ctx.scope();
      if (!(scope instanceof FuncBlock)) {
        throw new SyntaxError("yield not in a function", expr);
      }
      dcheck(((FuncBlock) scope).isGenerator());
      compileOrNone(expr.value, expr);
      ctx.write(YIELD, expr);
    }

    @Override
    public void visitYieldFrom(AstYieldFrom expr) {
      Block scope = ctx.scope();
      if (!(scope instanceof FuncBlock)) {
        throw new SyntaxError("yield not in a function", expr);
      }
      dcheck(((FuncBlock) scope).isGenerator());
      if (((FuncBlock) scope).isAsync()) {
        throw new SyntaxError("yield from in a coroutine", expr);
      }
      compile(expr.list);
      ctx.write(YIELDFROM, expr);
    }

    @Override
    public void visitAwait(AstAwait expr) {
      Scope<?> block = ctx.scope();
      if (!(block instanceof FuncBlock)) {
        throw new SyntaxError("await not in a function", expr);
      }
      if (!((FuncBlock) block).isAsync()) {
        throw new SyntaxError("await not in a coroutine", expr);
      }
      compile(expr.expr);
      ctx.write(AWAIT, expr);
    }

    @Override
    public void visitConst(AstConst expr) {
      ctx.write(expr.value, expr);
    }

    @Override
    public void visitLambda(AstLambda expr) {
      FuncBlock f = new FuncBlock(ctx, expr);
      compiler.compileReturn(expr.value, expr);
      f.end(ctx, expr);
    }

    @Override
    public void visitStar(AstStar expr) {
      throw InternalErrorException.notReached(expr.kind().toString(), expr);
    }

    @Override
    public void visitDoubleStar(AstDoubleStar expr) {
      throw InternalErrorException.notReached(expr.kind().toString(), expr);
    }
  }

  private class Visitor0 extends BaseAstExprVisitor {
    private void compile0(IAstExpr expr) {
      ConstEval.@Nullable Result r = ConstEval.tryEval(expr);
      if (r != null) {
        return;
      }

      AstExprVisit.visit(expr, this);
    }

    @Override
    public void visitAny(IAstExpr expr) {
      compile(expr);
      ctx.write(POP, expr);
    }

    @Override
    public void visitParen(AstParen expr) {
      compile0(expr.expr);
    }

    @Override
    public void visitCall(AstCall expr) {
      compileCall(expr, false);
    }
  }

  private class BoolVisitor extends BaseAstExprVisitor {
    private void compileBool(IAstExpr expr) {
      @Nullable Boolean constVal = ConstEval.tryEvalBool(expr);
      if (constVal != null) {
        ctx.write(constVal ? LOADTRUE : LOADFALSE, expr);
        return;
      }

      AstExprVisit.visit(expr, this);
    }

    @Override
    public void visitAny(IAstExpr expr) {
      compile(expr);
      ctx.write(TOBOOL, expr);
    }

    @Override
    public void visitNot(AstNot expr) {
      compileBool(expr.expr);
      ctx.write(INVBOOL, expr);
    }

    @Override
    public void visitComparison(AstComparison expr) {
      if (expr.canSuspend() || expr.ops.length != 1) {
        visitAny(expr);
        return;
      }

      compile(expr.operands[0]);
      compile(expr.operands[1]);
      ctx.write(BCOMPOP, expr.ops[0].ordinal(), expr);
    }

    @Override
    public void visitLogicalOp(AstLogicalOp expr) {
      if (expr.canSuspend()) {
        visitAny(expr);
        return;
      }

      CLabel done = ctx.newLabel();
      compileBool(expr.lhs);
      ctx.write(DUPBOOL, expr.lhs);
      ctx.jump(expr.op == LogicalOp.AND ? IFFALSEBOOL : IFTRUEBOOL, done, expr.lhs);
      ctx.write(POPBOOL, expr.lhs);
      compileBool(expr.rhs);
      ctx.write(done, expr);
    }
  }
}
