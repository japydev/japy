package org.japy.compiler.impl;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.loc.IHasLocation;
import org.japy.kernel.types.IPyObj;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.stmt.IAstBlock;

interface ICompiler {
  boolean annotationsAsStrings();

  void compileBlockBody(IAstBlock astBlock, Scope<?> block);

  String getText(IAstExpr expr);

  void compileBool(IAstExpr expr);

  void compileValue(IAstExpr expr);

  void writeConstant(IPyObj value, IHasLocation location);

  void compileReturn(@Nullable IAstExpr value, ITokenSpan location);

  default void compileIf(IAstExpr condition, Runnable writeIfTrue) {
    compileIf(condition, writeIfTrue, null);
  }

  void compileIf(IAstExpr condition, Runnable writeIfTrue, @Nullable Runnable writeIfFalse);

  void compileAssign(IAstExpr target, IAstExpr value);

  void compileExprStmt(IAstExpr expr);

  void compileRaise(@Nullable IAstExpr expr, @Nullable IAstExpr from, ITokenSpan location);

  default void compileFor(
      boolean async, IAstExpr target, IAstExpr list, ITokenSpan location, Runnable writeBody) {
    compileFor(async, target, list, location, writeBody, null, null);
  }

  void compileFor(
      boolean async,
      IAstExpr target,
      IAstExpr list,
      ITokenSpan location,
      Runnable writeBody,
      @Nullable Runnable writeElse,
      @Nullable ITokenSpan elseLocation);

  void compileWhile(
      IAstExpr condition,
      Runnable writeBody,
      @Nullable Runnable writeElse,
      @Nullable ITokenSpan elseLocation,
      ITokenSpan loopSpan);

  void defineFunction(FuncBlock func);

  void defineClass(ClassBlock klass);

  void compileTryFinally(
      ITokenSpan wholeSpan,
      IHasLocation finallyLocation,
      Runnable writeBody,
      Runnable writeFinally);
}
