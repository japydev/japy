package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CInstr0.Code.NOP;
import static org.japy.compiler.code.instr.CInstr0.Code.THROW;
import static org.japy.compiler.code.instr.CInstrR.Code.*;
import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import org.japy.compiler.code.CVarType;
import org.japy.compiler.code.instr.CJump;
import org.japy.compiler.code.instr.CLabel;
import org.japy.compiler.code.instr.CSwitch;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.Location;
import org.japy.parser.python.ast.loc.ITokenSpan;

class TryFinallyHandler extends BaseControlHandler {
  private static class GotoAfterFinally {
    final CLabel target;
    final Location location;
    final CLabel trampoline;

    private GotoAfterFinally(Context ctx, CLabel target, ITokenSpan location) {
      this.target = target;
      this.location = location.location();
      this.trampoline = ctx.newLabel();
    }
  }

  private final TryHandler tryHandler;
  private final List<GotoAfterFinally> gotosAfterFinally = new ArrayList<>();
  private final CLabel lBeginTry;
  private final CLabel lBeginFinally;
  private final TempVar exc;
  private final TempVar goTo;
  private final int initGoTo;

  TryFinallyHandler(Context ctx, ITokenSpan location) {
    super(ctx);
    tryHandler = new TryHandler(ctx);

    lBeginFinally = ctx.newLabel();

    lBeginTry = ctx.newLabel();
    exc = ctx.pushTemp("exc", CVarType.EXC, true, location);
    goTo = ctx.pushTemp("goTo", CVarType.INT, false, location);
    ctx.write(INITTEMP, goTo, location);
    initGoTo = ctx.scope().instructions.size() - 1;

    ctx.write(lBeginTry, location);
    ctx.write(NOP, location);
  }

  public static void compile(
      Context ctx,
      ITokenSpan location,
      Runnable writeBody,
      Runnable writeFinally,
      ITokenSpan finallyLocation) {
    TryFinallyHandler h = new TryFinallyHandler(ctx, location);
    writeBody.run();
    h.beginFinally(finallyLocation);
    writeFinally.run();
    h.end(location.end().end);
  }

  public void beginFinally(IHasLocation location) {
    dcheck(ctx.topHandler() == tryHandler);
    tryHandler.end();

    CLabel lEndTry = ctx.newLabel();
    ctx.write(lEndTry, location);

    ctx.jump(lBeginFinally, location);

    ctx.beginExcHandler(lBeginTry, lEndTry, location);
    ctx.write(STOREEXC, exc, location);

    ctx.write(lBeginFinally, location);
  }

  public void end(IHasLocation location) {
    dcheck(ctx.topHandler() != tryHandler);

    ctx.write(LOADEXC, exc, location);
    CLabel lNoExc = ctx.newLabel();
    ctx.jump(CJump.Code.IFNULLOBJ, lNoExc, location);
    ctx.write(LOADEXC, exc, location);
    ctx.write(THROW, location);

    ctx.write(lNoExc, location);

    if (!gotosAfterFinally.isEmpty()) {
      writeGotosAfterFinally(location);
    } else {
      ctx.scope().nopTemp(initGoTo, goTo);
    }

    ctx.popTemp(goTo, location);
    ctx.popTemp(exc, location);

    ctx.pop(this);
  }

  private void writeGotosAfterFinally(IHasLocation location) {
    int gotoCount = gotosAfterFinally.size();
    CLabel[] gotos = new CLabel[gotoCount + 1];
    CLabel fallThrough = ctx.newLabel();
    gotos[0] = fallThrough;
    for (int i = 0; i != gotoCount; ++i) {
      gotos[i + 1] = gotosAfterFinally.get(i).target;
    }

    ctx.write(LOADINT, goTo, location);
    ctx.write(new CSwitch(gotos, location));

    for (int i = 0; i != gotoCount; ++i) {
      GotoAfterFinally g = gotosAfterFinally.get(i);
      ctx.write(g.trampoline, location);
      ctx.write(STORECONSTINT, goTo, i + 1, location);
      ctx.jump(lBeginFinally, location);
    }

    ctx.write(fallThrough, location);
  }

  private class TryHandler extends BaseControlHandler implements CloseableHandler {
    private TryHandler(Context ctx) {
      super(ctx);
    }

    @Override
    public CLabel close(CLabel target, ITokenSpan location) {
      GotoAfterFinally g = new GotoAfterFinally(ctx, target, location);
      gotosAfterFinally.add(g);
      return g.trampoline;
    }

    public void end() {
      ctx.pop(this);
    }
  }
}
