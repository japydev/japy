package org.japy.compiler.impl;

import org.japy.compiler.code.CInstrSink;
import org.japy.compiler.code.instr.CInstr;
import org.japy.compiler.code.instr.CInstrR;
import org.japy.compiler.code.instr.CInstrS;
import org.japy.compiler.code.instr.ICInstr;
import org.japy.compiler.impl.instr.CTInstr;
import org.japy.compiler.impl.instr.CTInstrN;
import org.japy.compiler.impl.instr.CTInstrR;
import org.japy.compiler.impl.instr.CTInstrTR;
import org.japy.infra.loc.IHasLocation;
import org.japy.parser.python.ast.expr.AstIdentifier;

interface InstrSink extends CInstrSink {
  void write(CTInstr instr);

  @SuppressWarnings("MethodOverloadsMethodOfSuperclass")
  default void write(ICInstr instr) {
    if (instr instanceof CInstr) {
      write((CInstr) instr);
    } else {
      write((CTInstr) instr);
    }
  }

  default void write(CInstrS.Code code, AstIdentifier identifier) {
    write(new CInstrS(code, identifier.name, identifier));
  }

  default void write(CTInstrN.Code code, AstIdentifier identifier, IHasLocation location) {
    write(new CTInstrN(code, identifier.name, location));
  }

  default void write(CTInstrN.Code code, LocalVar var, IHasLocation location) {
    write(new CTInstrN(code, var.identifier.name, location));
  }

  default void write(CTInstrN.Code code, AstIdentifier identifier, int arg, IHasLocation location) {
    write(new CTInstrN(code, identifier.name, arg, location));
  }

  default void write(CInstrR.Code code, LocalVar var, IHasLocation location) {
    write(new CTInstrR(code, Ref.local(var), location));
  }

  default void write(CInstrR.Code code, LocalVar var, int arg, IHasLocation location) {
    write(new CTInstrR(code, Ref.local(var), arg, location));
  }

  default void write(CTInstrTR.Code code, LocalVar var, IHasLocation location) {
    write(new CTInstrTR(code, Ref.local(var), location));
  }

  default void write(CTInstrTR.Code code, LocalVar var) {
    write(new CTInstrTR(code, Ref.local(var), var));
  }

  default void write(CInstrR.Code code, TempVar var, IHasLocation location) {
    write(new CTInstrR(code, Ref.temp(var), location));
  }

  default void write(CInstrR.Code code, TempVar var, int arg, IHasLocation location) {
    write(new CTInstrR(code, Ref.temp(var), arg, location));
  }

  default void write(CTInstrTR.Code code, TempVar var, IHasLocation location) {
    write(new CTInstrTR(code, Ref.temp(var), location));
  }

  default void write(CTInstrTR.Code code, TempVar var) {
    write(new CTInstrTR(code, Ref.temp(var), var));
  }
}
