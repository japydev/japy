package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CInstr0.Code.IGNOP;
import static org.japy.compiler.code.instr.CInstrR.Code.*;
import static org.japy.compiler.impl.instr.CTInstrTR.Code.*;
import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.infra.validation.Validator.check;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.SyntaxError;
import org.japy.compiler.code.CExcHandler;
import org.japy.compiler.code.CVarType;
import org.japy.compiler.code.instr.CCatch;
import org.japy.compiler.code.instr.CInstr;
import org.japy.compiler.code.instr.CInstr0;
import org.japy.compiler.code.instr.CLabel;
import org.japy.compiler.code.instr.ICInstr;
import org.japy.compiler.impl.instr.CTInstr;
import org.japy.compiler.impl.instr.CTInstrR;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.validation.Debug;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.impl.ParserUtil;

abstract class Scope<T extends Scope<T>> extends Block {
  protected int flags;
  protected final List<ICInstr> instructions = new ArrayList<>();
  private final List<CExcHandler> excHandlers = new ArrayList<>();

  protected final Map<String, Ref> names = new HashMap<>();
  protected final List<LocalVar> locals = new ArrayList<>();
  protected int tempCount;
  private int labelCount;

  protected @Nullable ScopeWriter<T, ?, ?> writer;

  protected Scope(ITokenSpan location) {
    super(location);
  }

  protected Scope(Scope<?> parent, ITokenSpan location) {
    super(parent, location);
  }

  ScopeWriter<T, ?, ?> writer(ScopeWriter<?, ?, ?> parent) {
    if (writer == null) {
      writer = makeWriter(parent);
    } else {
      dcheck(writer.parent == parent);
    }
    return writer;
  }

  abstract ScopeWriter<T, ?, ?> makeWriter(ScopeWriter<?, ?, ?> parent);

  TempVar retVal() {
    throw InternalErrorException.notReached();
  }

  abstract void setDocString(int docString, Token location);

  abstract CLabel retLabel();

  List<ICInstr> instructions() {
    return instructions;
  }

  List<LocalVar> locals() {
    return locals;
  }

  Map<String, Ref> names() {
    return names;
  }

  List<CExcHandler> excHandlers() {
    return excHandlers;
  }

  @Override
  CLabel newLabel() {
    return new CLabel(labelCount++);
  }

  @Override
  public void write(CInstr instr) {
    if (Debug.ENABLED) {
      dcheck(!instr.location().isUnknown());
    }
    instructions.add(instr);
  }

  @Override
  public void write(CTInstr instr) {
    if (Debug.ENABLED) {
      dcheck(!instr.location().isUnknown());
    }
    instructions.add(instr);
  }

  @Override
  protected String newTempName(String base) {
    dcheckNotNull(parentBlock);
    return parentBlock.newTempName(base);
  }

  private AstIdentifier newTempName(String name, ITokenSpan location) {
    return ParserUtil.nonMangledId(newTempName(name), location.start());
  }

  private TempVar newTemp(String name, CVarType type, ITokenSpan location) {
    return new TempVar(this, newTempName(name, location), type);
  }

  @Override
  TempVar pushTemp(String name, CVarType type, boolean init, ITokenSpan location) {
    TempVar var = newTemp(name, type, location);
    ++tempCount;
    check(names.put(var.name(), Ref.temp(var)) == null, "bad temp");
    write(TBEGINTEMP, var);
    if (init) {
      write(INITTEMP, var, location);
    }
    return var;
  }

  static void eraseTemp(InstrSink sink, TempVar var, IHasLocation location) {
    if (!VarTypeInfo.get(var.type).isPrimitive()) {
      sink.write(ERASETEMP, var, location);
    }
  }

  void nopTemp(int initOp, TempVar var) {
    if (Debug.ENABLED) {
      ICInstr instr = instructions.get(initOp);
      dcheck(
          instr instanceof CTInstrR
              && ((CTInstrR) instr).ref.temp() == var
              && ((CTInstrR) instr).code == INITTEMP);
    }
    instructions.set(initOp, new CInstr0(IGNOP, var));
    var.setUnused();
  }

  @Override
  void popTemp(TempVar var, boolean erase, IHasLocation location) {
    check(--tempCount >= 0, "bad temp");
    if (erase) {
      eraseTemp(this, var, location);
    }
    write(TENDTEMP, var, location);
    check(names.remove(var.name()) != null, "bad temp");
  }

  @Override
  Ref referencedName(AstIdentifier identifier) {
    return names.computeIfAbsent(
        identifier.name,
        name -> {
          if (parentBlock instanceof CompBlock) {
            @Nullable String translated = ((CompBlock) parentBlock).translateName(name);
            if (translated != null) {
              return Ref.unknown(translated, this);
            }
          }

          return Ref.unknown(name, this);
        });
  }

  @Override
  void boundName(AstIdentifier identifier) {
    @Nullable Ref ref = names.get(identifier.name);
    if (ref == null || ref.kind() == Ref.Kind.UNKNOWN) {
      LocalVar var = new LocalVar(this, identifier);
      locals.add(var);
      if (ref == null) {
        names.put(identifier.name, Ref.local(var));
      } else {
        ref.makeLocal(var);
      }
    }
  }

  @Override
  void walrusName(AstIdentifier identifier) {
    boundName(identifier);
  }

  @Override
  void nonlocalDecl(AstIdentifier identifier) {
    @Nullable Ref ref = names.get(identifier.name);
    if (ref == null) {
      names.put(identifier.name, Ref.nonlocal(identifier.name, this));
    } else if (ref.kind() == Ref.Kind.UNKNOWN) {
      ref.makeNonlocal();
    } else if (ref.kind() != Ref.Kind.NONLOCAL) {
      throw new SyntaxError("variable '" + identifier.name + "' is already bound", identifier);
    }
  }

  @Override
  void globalDecl(AstIdentifier identifier) {
    @Nullable Ref ref = names.get(identifier.name);
    if (ref == null) {
      names.put(identifier.name, Ref.global(identifier.name, this));
    } else if (ref.kind() == Ref.Kind.UNKNOWN) {
      ref.makeGlobal();
    } else if (ref.kind() != Ref.Kind.GLOBAL) {
      throw new SyntaxError("variable '" + identifier.name + "' is already bound", identifier);
    }
  }

  @Override
  public void addExcHandler(CLabel handler, CLabel start, CLabel end) {
    CExcHandler h = new CExcHandler(handler, start, end);
    write(new CCatch(h, end));
    excHandlers.add(h);
  }
}
