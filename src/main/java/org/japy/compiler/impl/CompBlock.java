package org.japy.compiler.impl;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.SyntaxError;
import org.japy.compiler.code.CVarType;
import org.japy.compiler.code.instr.CInstr;
import org.japy.compiler.code.instr.CLabel;
import org.japy.compiler.code.instr.ICInstr;
import org.japy.compiler.impl.instr.CTInstr;
import org.japy.compiler.impl.instr.CTInstrN;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.IHasLocation;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.ITokenSpan;

final class CompBlock extends Block {
  private final Scope<?> scope;
  private final int firstInstruction;
  private final Map<String, TempVar> nameMap = new HashMap<>();

  CompBlock(Context ctx, ITokenSpan location) {
    super(ctx.block(), location);
    scope = ctx.scope();
    firstInstruction = scope.instructions().size();
    ctx.pushBlock(this);
  }

  void end(Context ctx, ITokenSpan location) {
    ctx.popBlock(this);
    dcheck(ctx.scope() == scope);

    List<ICInstr> instructions = scope.instructions();
    for (int i = firstInstruction, c = instructions.size(); i != c; ++i) {
      rewriteNames(instructions.get(i));
    }

    nameMap.forEach((n, v) -> ctx.popTemp(v, location));
  }

  private void rewriteNames(ICInstr instr) {
    if (!(instr instanceof CTInstrN)) {
      return;
    }

    CTInstrN ctiid = (CTInstrN) instr;
    @Nullable TempVar v = nameMap.get(ctiid.name());
    if (v != null) {
      ctiid.setName(v.identifier.name);
    }
  }

  public @Nullable String translateName(String name) {
    @Nullable TempVar v = nameMap.get(name);
    return v != null ? v.name() : null;
  }

  @Override
  String newTempName(String base) {
    return scope.newTempName(base);
  }

  @Override
  TempVar pushTemp(String name, CVarType type, boolean init, ITokenSpan location) {
    return scope.pushTemp(name, type, init, location);
  }

  @Override
  void popTemp(TempVar var, boolean erase, IHasLocation location) {
    scope.popTemp(var, erase, location);
  }

  @Override
  Ref referencedName(AstIdentifier identifier) {
    @Nullable TempVar v = nameMap.get(identifier.name);
    if (v != null) {
      return Ref.temp(v);
    }
    return scope.referencedName(identifier);
  }

  @Override
  void boundName(AstIdentifier identifier) {
    if (nameMap.containsKey(identifier.name) || scope.names().containsKey(identifier.name)) {
      return;
    }

    TempVar t = scope.pushTemp(identifier.name, CVarType.PYOBJ, false, identifier);
    nameMap.put(identifier.name, t);
  }

  @Override
  void walrusName(AstIdentifier identifier) {
    if (nameMap.containsKey(identifier.name)) {
      throw new SyntaxError("invalid assignment expression", identifier);
    }

    dcheckNotNull(parentBlock);
    parentBlock.walrusName(identifier);
  }

  @Override
  void nonlocalDecl(AstIdentifier identifier) {
    throw InternalErrorException.notReached();
  }

  @Override
  void globalDecl(AstIdentifier identifier) {
    throw InternalErrorException.notReached();
  }

  @Override
  void addExcHandler(CLabel handler, CLabel start, CLabel end) {
    scope.addExcHandler(handler, start, end);
  }

  @Override
  CLabel newLabel() {
    return scope.newLabel();
  }

  @Override
  public void write(CInstr instr) {
    scope.write(instr);
  }

  @Override
  public void write(CTInstr instr) {
    scope.write(instr);
  }
}
