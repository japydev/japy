package org.japy.compiler.impl;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Validator.check;

import org.checkerframework.checker.nullness.qual.MonotonicNonNull;

import org.japy.compiler.code.CLocalVar;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.ValidationError;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.ISingleTokenSpan;
import org.japy.parser.python.ast.loc.Token;

public final class LocalVar implements ISingleTokenSpan {
  public final Block block;
  public final AstIdentifier identifier;
  private boolean isCell;

  public @MonotonicNonNull CLocalVar cv;

  LocalVar(Block block, AstIdentifier identifier) {
    this.block = block;
    this.identifier = identifier;
    if (Debug.ENABLED) {
      dcheck(!identifier.name.startsWith(Constants.TEMP_PREFIX));
    }
  }

  boolean isCell() {
    return isCell;
  }

  void setCell(CLocalVar cv) {
    check(cv.isCell, () -> new ValidationError("not a cell: " + cv));
    check(this.cv == null, () -> new ValidationError("already resolved: " + this));
    check(!isCell, () -> new ValidationError("already a cell: " + this));
    isCell = true;
    this.cv = cv;
  }

  String name() {
    return identifier.name;
  }

  @Override
  public Token token() {
    return identifier.token();
  }

  @Override
  public String toString() {
    return "LOCAL(" + identifier + ")";
  }
}
