package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CInstrI.Code.CONST;
import static org.japy.compiler.impl.instr.CTInstrN.Code.*;

import java.util.HashSet;
import java.util.Set;

import com.google.errorprone.annotations.Var;

import org.japy.compiler.code.instr.CLabel;
import org.japy.infra.exc.InternalErrorException;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.node.AstModule;

final class ModuleBlock extends Scope<ModuleBlock> {
  private final AstModule astModule;
  private final Set<String> tempNames = new HashSet<>();
  private int nonDocStringInsn;

  ModuleBlock(AstModule astModule) {
    super(astModule.location);
    this.astModule = astModule;
  }

  @Override
  String sourceFile() {
    return astModule.sourceFile;
  }

  @Override
  protected String newTempName(@Var String base) {
    if (!base.startsWith(Constants.TEMP_PREFIX)) {
      base = Constants.TEMP_PREFIX + base;
    }

    if (!tempNames.contains(base)) {
      tempNames.add(base);
      return base;
    }

    for (int i = 2; ; ++i) {
      String name = base + i;
      if (!tempNames.contains(name)) {
        tempNames.add(name);
        return name;
      }
    }
  }

  @Override
  ModuleWriter makeWriter(ScopeWriter<?, ?, ?> parent) {
    throw InternalErrorException.notReached();
  }

  @Override
  void setDocString(int docString, Token location) {
    AstIdentifier id = new AstIdentifier(location, Constants.__DOC__);
    boundName(id);
    write(CONST, docString, location);
    write(STORENAME, id, location);
    nonDocStringInsn = instructions.size();
  }

  @Override
  CLabel retLabel() {
    throw InternalErrorException.notReached();
  }

  @Override
  void boundName(AstIdentifier identifier) {
    // everything is global in modules
    names.computeIfAbsent(identifier.name, name -> Ref.global(name, this));
  }

  @Override
  Ref referencedName(AstIdentifier identifier) {
    // everything is global in modules
    return names.computeIfAbsent(identifier.name, name -> Ref.global(name, this));
  }

  boolean hasNonDocStringInsn() {
    return instructions.size() > nonDocStringInsn;
  }
}
