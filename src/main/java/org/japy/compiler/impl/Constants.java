package org.japy.compiler.impl;

import java.util.Set;

public final class Constants {
  public static final String INPUT = "<input>";

  static final String SUPER = "super";
  static final String LOCALS = "locals";
  static final String DIR = "dir";
  static final String LAMBDA = "<lambda>";

  public static final String __DOC__ = "__doc__";

  static final String __ENTER__ = "__enter__";
  static final String __AENTER__ = "__aenter__";
  static final String __EXIT__ = "__exit__";
  static final String __AEXIT__ = "__aexit__";
  static final String __AITER__ = "__aiter__";
  static final String __ANEXT__ = "__anext__";
  static final String __CLASS__ = "__class__";
  static final String __FUTURE__ = "__future__";

  static final String ANNOTATIONS = "annotations";
  static final Set<String> PAST_FUTURE_IMPORTS =
      Set.of(
          "absolute_import",
          "division",
          "generators",
          "generator_stop",
          "unicode_literals",
          "print_function",
          "nested_scopes",
          "with_statement");

  static final String TEMP_PREFIX = "$";
  static final String MODULE = "$module";
  static final String BIN_NAME_SEPARATOR = "$";

  private Constants() {}
}
