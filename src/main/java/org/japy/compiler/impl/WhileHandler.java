package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CJump.Code.IFFALSEBOOL;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.code.instr.CLabel;
import org.japy.infra.loc.IHasLocation;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;

class WhileHandler extends BaseControlHandler implements LoopHandler {
  private final CLabel lBegin;
  private final CLabel lBreak;
  private final @Nullable CLabel lElse;

  private WhileHandler(Context ctx, IAstExpr condition, boolean hasElse) {
    super(ctx);

    lBegin = ctx.newLabel();
    lBreak = ctx.newLabel();
    lElse = hasElse ? ctx.newLabel() : null;

    ctx.write(lBegin, condition);

    @Nullable Boolean constCondition = ConstEval.tryEvalBool(condition);
    if (constCondition != null) {
      if (!constCondition) {
        ctx.jump(lElse != null ? lElse : lBreak, condition);
      }
    } else {
      ctx.compiler.compileBool(condition);
      ctx.jump(IFFALSEBOOL, lElse != null ? lElse : lBreak, condition);
    }
  }

  public static void compile(
      Context ctx,
      IAstExpr condition,
      Runnable writeBody,
      @Nullable Runnable writeElse,
      @Nullable ITokenSpan elseLocation,
      ITokenSpan loopSpan) {
    WhileHandler h = new WhileHandler(ctx, condition, writeElse != null);
    writeBody.run();
    ctx.jump(h.lBegin, condition);
    if (writeElse != null) {
      h.beginElse(dcheckNotNull(elseLocation));
      writeElse.run();
    }
    h.end(loopSpan.end().end);
  }

  private void beginElse(ITokenSpan location) {
    ctx.write(dcheckNotNull(lElse), location);
  }

  private void end(IHasLocation location) {
    ctx.write(lBreak, location);
    ctx.pop(this);
  }

  @Override
  public void visitBreak(ITokenSpan location) {
    ctx.goTo(lBreak, this, location);
  }

  @Override
  public void visitContinue(ITokenSpan location) {
    ctx.goTo(lBegin, this, location);
  }
}
