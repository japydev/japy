package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CInstrR.Code.*;
import static org.japy.compiler.impl.instr.CTInstrTR.Code.*;
import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Validator.check;
import static org.japy.infra.validation.Validator.checkNotNull;
import static org.japy.infra.validation.Validator.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.code.CExcHandler;
import org.japy.compiler.code.instr.CInstr;
import org.japy.compiler.code.instr.CInstrR;
import org.japy.compiler.code.instr.CJump;
import org.japy.compiler.code.instr.CLabel;
import org.japy.compiler.code.instr.CSwitch;
import org.japy.compiler.code.instr.ICInstr;
import org.japy.compiler.impl.instr.CTInstr;
import org.japy.compiler.impl.instr.CTInstrTR;
import org.japy.infra.validation.Debug;

public final class Analyzer {
  private final List<? extends ICInstr> instructions;
  private final CExcHandler[] excHandlers;
  private final byte[] cFlags;
  private final int instructionCount;
  private final Map<CLabel, Integer> labelMap;

  public static class BlockInfo {
    public final byte[] instructionFlags;
    public final int blockFlags;

    public BlockInfo(byte[] instructionFlags, int blockFlags) {
      this.instructionFlags = instructionFlags;
      this.blockFlags = blockFlags;
    }
  }

  public static BlockInfo analyze(List<? extends ICInstr> instructions, CExcHandler[] excHandlers) {
    return new Analyzer(instructions, excHandlers).analyze();
  }

  private Analyzer(List<? extends ICInstr> instructions, CExcHandler[] excHandlers) {
    this.instructions = instructions;
    this.excHandlers = excHandlers;
    instructionCount = instructions.size();
    cFlags = new byte[instructionCount];

    labelMap = new HashMap<>();
    for (int i = 0; i != instructionCount; ++i) {
      ICInstr instr = instructions.get(i);
      if (instr instanceof CLabel) {
        labelMap.put((CLabel) instr, i);
      }
    }

    for (int i = 0; i != instructionCount; ++i) {
      ICInstr instr = instructions.get(i);
      if (instr instanceof CJump) {
        check(labelMap.containsKey(((CJump) instr).target), "unknown jump target");
      }
    }
  }

  private BlockInfo analyze() {
    @Nullable InstrState[] stackState = new StackStateComputer().compute();
    int blockFlags = finalizeFlags(stackState);
    return new BlockInfo(cFlags, blockFlags);
  }

  private int finalizeFlags(@Nullable InstrState[] stackState) {
    for (int i = 0; i != instructionCount; ++i) {
      int flags = cFlags[i];
      if (stackState[i] == null && isExecutable(instructions.get(i))) {
        cFlags[i] = (byte) (flags | CInstr.FLAG_UNREACHABLE);
      }
    }
    return 0;
  }

  private static boolean isExecutable(ICInstr instr) {
    if (instr instanceof CInstr) {
      CInstr ci = (CInstr) instr;
      switch (((CInstr) instr).kind()) {
        case INSTRR:
          {
            CInstrR cir = (CInstrR) ci;
            return cir.code != BEGINTEMP && cir.code != ENDTEMP;
          }
        case CATCH:
          return false;
        default:
          return true;
      }
    } else {
      CTInstr cti = (CTInstr) instr;
      if (cti.kind() != CTInstr.Kind.TINSTRTR) {
        return true;
      }

      CTInstrTR ctiv = (CTInstrTR) cti;
      return ctiv.code != TBEGINTEMP && ctiv.code != TENDTEMP;
    }
  }

  private int getLabelAddress(CLabel label) {
    return checkNotNull(labelMap.get(label), "unknown label " + label);
  }

  private static final class InstrState {
    final short opStackBefore;
    final short popOp;
    final short pushOp;

    short opStackAfter() {
      return (short) (opStackBefore - popOp + pushOp);
    }

    private InstrState(int opStackBefore, int popOp, int pushOp) {
      this.opStackBefore = (short) opStackBefore;
      this.popOp = (short) popOp;
      this.pushOp = (short) pushOp;
    }
  }

  private boolean isBreak(int address) {
    return (cFlags[address] & (CInstr.FLAG_JUMP | CInstr.FLAG_RET | CInstr.FLAG_THROW)) != 0;
  }

  private class StackStateComputer {
    private final @Nullable InstrState[] stackState;
    private final Map<CLabel, Integer> pending = new HashMap<>();

    StackStateComputer() {
      stackState = new InstrState[instructionCount];
    }

    @Nullable
    InstrState[] compute() {
      // TODO
      for (CExcHandler h : excHandlers) {
        pending.put(h.handler, 1);
        pending.put(h.start, 0);
        pending.put(h.end, 0);
      }

      @Var int address = 0;
      @Var int opStack = 0;
      while (true) {
        check(address == instructionCount || stackState[address] == null, "oops");

        while (address != instructionCount) {
          ICInstr instr = instructions.get(address);

          @Var
          @Nullable
          InstrState state = stackState[address];
          if (state != null) {
            if (Debug.ENABLED) {
              dcheck(state.opStackAfter() == opStack, "inconsistent operand stack", instr);
            }
            break;
          }

          state = execute(instr, opStack, address);
          stackState[address] = state;
          opStack += state.pushOp - state.popOp;
          if (Debug.ENABLED) {
            dcheck(opStack == state.opStackAfter());
          }

          if (isBreak(address)) {
            break;
          }

          ++address;
        }

        if (pending.isEmpty()) {
          break;
        }

        Map.Entry<CLabel, Integer> e = pending.entrySet().iterator().next();
        CLabel label = e.getKey();
        opStack = e.getValue();
        address = getLabelAddress(label);
      }

      return stackState;
    }

    private void jumpTo(CLabel label, int opStack) {
      int address = getLabelAddress(label);
      @Nullable InstrState state = stackState[address];
      if (state == null) {
        int expected = pending.computeIfAbsent(label, k -> opStack);
        if (Debug.ENABLED) {
          dcheck(expected == opStack, "inconsistent operand stack", label);
        }
      } else {
        if (Debug.ENABLED) {
          dcheck(state.opStackAfter() == opStack, "inconsistent operand stack", label);
        }
      }
    }

    private void label(CLabel label, int opStack) {
      @Nullable Integer expected = pending.get(label);
      if (expected != null && Debug.ENABLED) {
        dcheck(expected == opStack, "inconsistent operand stack", label);
      }
      pending.remove(label);
    }

    private InstrState execute(ICInstr instr, int opStack, int address) {
      cFlags[address] = (byte) instr.defaultFlags();

      int popOp;
      int pushOp;

      if (Debug.ENABLED) {
        if ((cFlags[address] & CInstr.FLAG_RET) != 0) {
          if (opStack == 0) {
            fail("return with nothing on stack", instr);
          } else {
            check(opStack == 1, "return with non-empty stack", instr);
          }
        }

        popOp = instr.popCount();
        pushOp = instr.pushCount();
      } else {
        popOp = 0;
        pushOp = 0;
      }

      if (instr instanceof CInstr) {
        CInstr ci = (CInstr) instr;
        switch (ci.kind()) {
          case JUMP:
            jumpTo(((CJump) ci).target, opStack - popOp);
            break;

          case SWITCH:
            for (CLabel target : ((CSwitch) ci).targets) {
              jumpTo(target, opStack - popOp);
            }
            break;

          case LABEL:
            label((CLabel) ci, opStack);
            break;

          default:
            break;
        }
      }

      if (Debug.ENABLED) {
        dcheck(opStack >= 0 && opStack >= popOp, "popped too much from stack", instr);
      }

      return new InstrState(opStack, popOp, pushOp);
    }
  }
}
