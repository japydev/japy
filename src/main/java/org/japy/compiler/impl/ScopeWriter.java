package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CInstrR.Code.*;
import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.infra.validation.Validator.check;
import static org.japy.infra.validation.Validator.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.CompilerConfig;
import org.japy.compiler.code.CBlock;
import org.japy.compiler.code.CClass;
import org.japy.compiler.code.CExcHandler;
import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CLocalVar;
import org.japy.compiler.code.CRef;
import org.japy.compiler.code.CTempVar;
import org.japy.compiler.code.CUpvalue;
import org.japy.compiler.code.instr.*;
import org.japy.compiler.impl.instr.*;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.LocationUtil;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.ValidationError;

abstract class ScopeWriter<T extends Scope<T>, C extends CBlock, B extends CBlock.Builder<C, B>> {
  private static final Pattern BAD_NAME_CHARS = Pattern.compile("[^0-9A-Za-z_]+");

  final T scope;
  final @Nullable ScopeWriter<?, ?, ?> parent;
  protected final B blockBuilder;
  protected final CompilerConfig config;

  final @Nullable String codeName;
  final String binName;
  private @Nullable Set<String> childNames;

  protected int flags;
  private final CExcHandler[] excHandlers;

  protected final Map<String, Ref> names;
  private final List<LocalVar> locals;
  private final List<@Nullable TempVar> temps = new ArrayList<>();
  private int tempCount;

  protected ScopeWriter(
      T scope,
      @Nullable String codeName,
      @Nullable ScopeWriter<?, ?, ?> parent,
      CompilerConfig config,
      B blockBuilder) {
    dcheck(parent == null ? scope.parentBlock == null : scope.parentBlock == parent.scope);
    this.scope = scope;
    this.parent = parent;
    this.blockBuilder = blockBuilder;
    this.flags = scope.flags;
    this.names = scope.names();
    this.locals = scope.locals();
    this.excHandlers = scope.excHandlers().toArray(CExcHandler[]::new);
    this.codeName = codeName;
    this.binName = makeBinName();
    this.config = config;

    if (Debug.ENABLED) {
      if (scope.tempCount != 0) {
        throw new InternalErrorException(
            "non-freed temps "
                + names.keySet().stream()
                    .filter(n -> n.startsWith(Constants.TEMP_PREFIX))
                    .collect(Collectors.toList()));
      }
    }
  }

  C build() {
    try {
      List<ICInstr> instructions = scope.instructions();
      Analyzer.BlockInfo blockInfo = Analyzer.analyze(instructions, excHandlers);
      secondPass(firstPass(instructions, blockInfo));
      C cb =
          blockBuilder
              .location(scope.location())
              .binName(binName)
              .excHandlers(excHandlers)
              .addFlags(flags)
              .sourceFile(scope.sourceFile())
              .sourceLine(scope.location().line)
              .build();
      if (!(this instanceof ModuleWriter)) {
        moduleWriter().children.add(cb);
      }
      return cb;
    } catch (Throwable ex) {
      LocationUtil.setLocationIfNotSet(ex, scope.location);
      throw ex;
    }
  }

  protected abstract @Nullable String qualName();

  protected String makeQualName(@Nullable String parentName, String name) {
    return parentName != null ? parentName + "." + name : name;
  }

  protected void setUsesLocals() {}

  private ModuleWriter moduleWriter() {
    for (@Nullable ScopeWriter<?, ?, ?> w = this; w != null; w = w.parent) {
      if (w instanceof ModuleWriter) {
        dcheck(w.parent == null);
        return (ModuleWriter) w;
      }
    }
    throw InternalErrorException.notReached();
  }

  private String makeBinName() {
    dcheckNotNull(codeName);
    if (parent != null && parent.parent != null) {
      return parent.binName + Constants.BIN_NAME_SEPARATOR + parent.makeUniqueChildName(codeName);
    } else if (parent != null) {
      return parent.makeUniqueChildName(codeName);
    } else {
      return makeUniqueChildName(codeName);
    }
  }

  protected String makeUniqueChildName(@Var String childName) {
    if (childNames == null) {
      childNames = new HashSet<>();
    }

    childName = BAD_NAME_CHARS.matcher(childName).replaceAll("");

    for (int i = 1; ; ++i) {
      String name = i == 1 ? childName : childName + i;
      if (!childNames.contains(name)) {
        childNames.add(name);
        return name;
      }
    }
  }

  private List<ICInstr> firstPass(List<ICInstr> oldInstructions, Analyzer.BlockInfo blockInfo) {
    flags |= blockInfo.blockFlags;

    Rewriter1 rewriter = new Rewriter1();
    for (int i = 0, c = oldInstructions.size(); i != c; ++i) {
      if ((blockInfo.instructionFlags[i] & CInstr.FLAG_UNREACHABLE) == 0) {
        rewriter.write(oldInstructions.get(i));
      }
    }

    dcheck(tempCount == 0);

    return rewriter.instructions;
  }

  private void secondPass(List<ICInstr> instructions) {
    Rewriter2 r2 = new Rewriter2();
    for (ICInstr instr : instructions) {
      r2.write(instr);
    }
    dcheck(tempCount == 0);

    // make sure unused function parameters don't get lost
    for (LocalVar v : locals) {
      @SuppressWarnings("unused")
      CLocalVar cv = resolveLocalVar(v);
    }
  }

  private void forEachLocal(Consumer<? super LocalVar> func) {}

  @CanIgnoreReturnValue
  private int addTemp(TempVar var) {
    dcheck(!var.isUnused());

    @Nullable Ref oldRef = names.put(var.name(), Ref.temp(var));
    check(oldRef == null, () -> new ValidationError("bad temp " + var));

    @Var int index;
    if (tempCount == temps.size()) {
      temps.add(null);
      index = tempCount;
    } else {
      for (index = 0; index != temps.size(); ++index) {
        if (temps.get(index) == null) {
          break;
        }
      }
    }
    temps.set(index, var);
    ++tempCount;
    return index;
  }

  private void removeTemp(TempVar var) {
    dcheck(!var.isUnused());
    dcheck(tempCount != 0);

    check(names.remove(var.name()) != null, () -> new ValidationError("bad temp " + var));

    @Var int index;
    if (var.cv != null) {
      index = var.cv.index;
      dcheck(temps.get(index) == var);
    } else {
      index = temps.size() - 1;
      while (index >= 0 && temps.get(index) != var) {
        --index;
      }
    }

    dcheck(index >= 0);
    temps.set(index, null);
    --tempCount;
  }

  private class Rewriter1 implements InstrSink {
    private final List<ICInstr> instructions = new ArrayList<>();

    @Override
    public void write(CInstr instr) {
      instructions.add(instr);
    }

    @Override
    public void write(CTInstr instr) {
      rewriteInstr(instr);
    }

    private void rewriteInstr(CTInstr instr) {
      switch (instr.kind()) {
        case TINSTRTR:
          {
            CTInstrTR ci = (CTInstrTR) instr;
            switch (ci.code) {
              case TBEGINTEMP:
                if (!ci.ref.temp().isUnused()) {
                  addTemp(ci.ref.temp());
                  instructions.add(instr);
                }
                break;
              case TENDTEMP:
                if (!ci.ref.temp().isUnused()) {
                  removeTemp(ci.ref.temp());
                  instructions.add(instr);
                }
                break;
            }
            break;
          }

        case TINSTRN:
        case TINSTRR:
          instructions.add(instr);
          break;

        case TCLASS:
          {
            ClassWriter w = ((CTClass) instr).block.writer(ScopeWriter.this);
            CClass cls = w.build();
            write(new CMakeClass(cls));
            break;
          }

        case TFUNC:
          {
            FuncWriter w = ((CTFunc) instr).block.writer(ScopeWriter.this);
            CFunc func = w.build();
            write(new CMakeFunc(func));
            break;
          }
      }
    }
  }

  private class Rewriter2 implements InstrSink {
    @Override
    public void write(CInstr instr) {
      blockBuilder.write(instr);
    }

    @Override
    public void write(CTInstr instr) {
      rewriteInstr(instr);
    }

    private void rewriteInstr(CTInstr instr) {
      switch (instr.kind()) {
        case TINSTRR:
          {
            CTInstrR ci = (CTInstrR) instr;
            switch (ci.ref.kind()) {
              case LOCAL:
                {
                  CLocalVar var = resolveLocalVar(ci.ref._local());
                  write(ci.code, var, ci.arg, ci);
                  break;
                }
              case TEMP:
                {
                  CTempVar var = resolveTempVar(ci.ref.temp());
                  write(ci.code, var, ci.arg, ci);
                  break;
                }
              default:
                throw InternalErrorException.notReached();
            }
            break;
          }

        case TINSTRN:
          rewriteInstrId((CTInstrN) instr);
          break;

        case TINSTRTR:
          {
            CTInstrTR ci = (CTInstrTR) instr;
            switch (ci.code) {
              case TBEGINTEMP:
                {
                  TempVar tv = ci.ref.temp();
                  int index = addTemp(tv);
                  CTempVar cv = resolveTempVar(tv);
                  dcheck(index == cv.index);
                  write(BEGINTEMP, cv, tv);
                  break;
                }
              case TENDTEMP:
                {
                  TempVar tv = ci.ref.temp();
                  CTempVar cv = resolveTempVar(tv);
                  removeTemp(tv);
                  write(ENDTEMP, cv, tv);
                  break;
                }
            }
            break;
          }

        case TCLASS:
        case TFUNC:
          throw InternalErrorException.notReached();
      }
    }

    private void rewriteInstrId(CTInstrN instr) {
      Ref ref = resolveName(instr.name(), instr);

      switch (ref.kind()) {
        case LOCAL:
          write(instr.code.rCode, resolveLocalVar(ref._local()), instr.arg, instr);
          break;

        case TEMP:
          write(instr.code.rCode, resolveTempVar(ref.temp()), instr.arg, instr);
          break;

        case GLOBAL:
          // TODO dir(x) is fine
          if (instr.name().equals(Constants.LOCALS) || instr.name().equals(Constants.DIR)) {
            setUsesLocals();
          }
          write(instr.code.rCode, CRef.global(instr.name()), instr.arg, instr);
          break;

        case UPVALUE:
          write(instr.code.rCode, ref.upvalue(), instr.arg, instr);
          break;

        case UNKNOWN:
        case NONLOCAL:
          throw InternalErrorException.notReached(ref.toString(), instr);
      }
    }
  }

  protected CLocalVar resolveLocalVar(LocalVar var) {
    if (Debug.ENABLED) {
      dcheck(var.block == scope);
      dcheck(locals.contains(var), var.toString(), var);
    }

    if (var.cv == null) {
      if (var.isCell()) {
        throw InternalErrorException.notReached("unresolved cell");
      } else {
        var.cv = blockBuilder.local(var.name());
      }
    }

    return var.cv;
  }

  protected CTempVar resolveTempVar(TempVar var) {
    if (Debug.ENABLED) {
      dcheck(var.block == scope);
      dcheck(temps.contains(var), var.toString(), var);
    }

    if (var.cv == null) {
      if (var.isCell()) {
        throw InternalErrorException.notReached("unresolved cell");
      } else {
        var.cv = blockBuilder.temp(var.name(), var.type, temps.indexOf(var));
        dcheck(var.cv.index >= 0);
      }
    }

    return var.cv;
  }

  protected void makeCell(TempVar var) {
    dcheck(var.block == scope);

    if (var.isCell()) {
      return;
    }

    int index = temps.indexOf(var);
    dcheck(index >= 0);
    var.setCell(blockBuilder.tempCell(var.name(), var.type, index));
  }

  protected void makeCell(LocalVar var) {
    throw InternalErrorException.notReached();
  }

  private boolean resolveParentTemp(Ref ref, String name) {
    if (parent == null) {
      return false;
    }

    @Nullable Ref parentRef = parent.names.get(name);
    if (parentRef == null || parentRef.kind() != Ref.Kind.TEMP) {
      return false;
    }

    TempVar parentVar = parentRef.temp();
    parent.makeCell(parentVar);
    CUpvalue upvalue =
        blockBuilder.upvalue(CUpvalue.Kind.TEMP, parentVar.name(), parentVar.cv.index);
    ref.makeUpvalue(upvalue);
    return true;
  }

  private @Nullable FuncWriter getParentFuncBuilder() {
    for (@Nullable ScopeWriter<?, ?, ?> b = parent; b != null; b = b.parent) {
      if (b instanceof FuncWriter) {
        return (FuncWriter) b;
      }
    }
    return null;
  }

  private static CUpvalue makeUpvalueFromParent(
      ScopeWriter<?, ?, ?> writer, FuncWriter func, Ref funcRef) {
    @Nullable CUpvalue upvalue = writer.blockBuilder.findUpvalue(funcRef.name);
    if (upvalue != null) {
      return upvalue;
    }

    if (writer.parent == func) {
      switch (funcRef.kind()) {
        case LOCAL:
          {
            LocalVar funcLocal = funcRef._local();
            func.makeCell(funcLocal);
            return writer.blockBuilder.upvalue(
                CUpvalue.Kind.CELL, funcRef.name, funcLocal.cv.index);
          }

        case TEMP:
          {
            throw InternalErrorException.notReached();
            //          TempVar funcLocal = funcRef.temp();
            //          func.makeCell(funcLocal);
            //          return writer.blockBuilder.upvalue(
            //              CUpvalue.Kind.CELL, funcRef.name, funcLocal.cv.index);
          }

        case UPVALUE:
          {
            CUpvalue funcUpvalue = funcRef.upvalue();
            return writer.blockBuilder.upvalue(
                CUpvalue.Kind.UPVALUE, funcRef.name, funcUpvalue.index);
          }

        default:
          throw InternalErrorException.notReached();
      }
    }

    dcheckNotNull(writer.parent);
    dcheck(writer.parent instanceof ClassWriter);
    CUpvalue parentUpvalue = makeUpvalueFromParent(writer.parent, func, funcRef);
    return writer.blockBuilder.upvalue(CUpvalue.Kind.UPVALUE, funcRef.name, parentUpvalue.index);
  }

  private boolean resolveNameFromParentFunc(Ref ref, String name, IHasLocation location) {
    @Nullable FuncWriter parentFunc = getParentFuncBuilder();

    if (parentFunc == null) {
      return false;
    }

    Ref parentRef = parentFunc.resolveName(name, location);
    dcheck(parentRef.block == parentFunc.scope);

    switch (parentRef.kind()) {
      case GLOBAL:
        return false;

      case UNKNOWN:
      case NONLOCAL:
        throw fail("unknown variable after resolving: " + name, location);

      case TEMP:
        throw InternalErrorException.notReached("temp upvalue " + name, location);

      case UPVALUE:
      case LOCAL:
        break;
    }

    CUpvalue upvalue = makeUpvalueFromParent(this, parentFunc, parentRef);
    ref.makeUpvalue(upvalue);
    return true;
  }

  private void resolveUnknown(Ref ref, String name, IHasLocation location) {
    if (Debug.ENABLED) {
      dcheck(
          ref.kind() == Ref.Kind.UNKNOWN
              || (ref.kind() == Ref.Kind.NONLOCAL && !(this instanceof ModuleWriter)),
          name);
    }

    if (!resolveParentTemp(ref, name) && !resolveNameFromParentFunc(ref, name, location)) {
      ref.makeGlobal();
    }
  }

  protected Ref resolveName(String name, IHasLocation location) {
    Ref ref = names.computeIfAbsent(name, (n) -> Ref.unknown(name, scope));

    switch (ref.kind()) {
      case GLOBAL:
      case LOCAL:
      case TEMP:
      case UPVALUE:
        return ref;

      case NONLOCAL:
      case UNKNOWN:
        resolveUnknown(ref, name, location);
        return ref;
    }

    throw InternalErrorException.notReached(location);
  }
}
