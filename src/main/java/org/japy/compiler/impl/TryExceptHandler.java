package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CInstr0.Code.*;
import static org.japy.compiler.code.instr.CInstrR.Code.*;
import static org.japy.compiler.impl.instr.CTInstrN.Code.*;
import static org.japy.infra.coll.CollUtil.first;
import static org.japy.infra.coll.CollUtil.last;
import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.code.CVarType;
import org.japy.compiler.code.instr.CJump;
import org.japy.compiler.code.instr.CLabel;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.Location;
import org.japy.infra.validation.Debug;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;

class TryExceptHandler extends BaseControlHandler {
  private abstract static class Handler {
    final CLabel lStart;
    final ITokenSpan location;

    protected Handler(Context ctx, ITokenSpan location) {
      this.lStart = ctx.newLabel();
      this.location = location;
    }
  }

  private static class ExHandler extends Handler {
    final IAstExpr ex;

    ExHandler(Context ctx, IAstExpr ex, ITokenSpan location) {
      super(ctx, location);
      this.ex = ex;
    }
  }

  private static class CatchAllHandler extends Handler {
    CatchAllHandler(Context ctx, ITokenSpan location) {
      super(ctx, location);
    }
  }

  private final CLabel lBeginTry;
  private final CLabel lEndTry;
  private final CLabel lEnd;
  private final List<Handler> handlers = new ArrayList<>();
  private final TempVar javaExc;
  private final TempVar pyExc;
  private final TempVar savedPyExc;

  TryExceptHandler(
      Context ctx, ITokenSpan trySpan, Runnable writeBody, @Nullable Runnable writeElse) {
    super(ctx);

    javaExc = ctx.pushTemp("exc", CVarType.EXC, trySpan);
    pyExc = ctx.pushTemp("pyExc", CVarType.PYEXC, trySpan);
    savedPyExc = ctx.pushTemp("savedPyExc", CVarType.PYEXC, trySpan);
    lBeginTry = ctx.newLabel();
    ctx.write(lBeginTry, trySpan);
    ctx.write(NOP, trySpan);
    writeBody.run();
    lEndTry = ctx.newLabel();
    ctx.write(lEndTry, trySpan.end().end);

    if (writeElse != null) {
      writeElse.run();
    }

    lEnd = ctx.newLabel();
    ctx.jump(lEnd, trySpan.end().end);
  }

  static void compileTryExcept(
      Context ctx, ITokenSpan location, Runnable writeBody, Runnable writeExcept) {
    TryExceptHandler h = new TryExceptHandler(ctx, location, writeBody, null);
    h.visitHandler(location, null, null, writeExcept);
    h.end(location.end().end);
  }

  void visitHandler(
      ITokenSpan handlerSpan, @Nullable IAstExpr ex, @Nullable IAstExpr as, Runnable writeBody) {
    if (Debug.ENABLED) {
      dcheck(handlers.isEmpty() || !(last(handlers) instanceof CatchAllHandler));
    }

    Handler h;
    if (ex != null) {
      h = new ExHandler(ctx, ex, handlerSpan);
    } else {
      h = new CatchAllHandler(ctx, handlerSpan);
    }
    handlers.add(h);

    Location start = handlerSpan.start().start;
    Location end = handlerSpan.end().end;

    ctx.write(h.lStart, start);

    if (as != null) {
      ctx.compiler.compileAssign(as, pyExc.identifier);
    }

    ctx.write(LOAD, pyExc, start);
    ctx.write(SETCURRENTEXC, start);
    ctx.write(STORE, savedPyExc, start);

    ctx.compiler.compileTryFinally(
        h.location,
        end,
        writeBody,
        () -> {
          ctx.write(LOAD, savedPyExc, end);
          ctx.write(SETCURRENTEXC, end);
          ctx.write(POP, end);
          ctx.write(ERASETEMP, savedPyExc, end);
          ctx.write(ERASETEMP, pyExc, end);
          ctx.write(ERASETEMP, javaExc, end);
          if (as instanceof AstIdentifier) {
            ctx.write(DELNAMEIFBOUND, (AstIdentifier) as, end);
          }
        });

    ctx.jump(lEnd, end);
  }

  private void writeMatch(ExHandler h) {
    ctx.write(LOAD, pyExc, h.location);
    ctx.compiler.compileValue(h.ex);
    ctx.write(EXCMATCH, h.location);
    CLabel noMatch = ctx.newLabel();
    ctx.jump(CJump.Code.IFFALSEBOOL, noMatch, h.location);
    ctx.jump(h.lStart, h.location);
    ctx.write(noMatch, h.location.end().end);
  }

  private void writeExceptionHandler() {
    ITokenSpan location = first(handlers).location;
    ctx.beginExcHandler(lBeginTry, lEndTry, location);
    ctx.write(DUP, location);
    ctx.write(STOREEXC, javaExc, location);
    ctx.write(CAPTUREEXC, pyExc, location);

    @Var boolean wroteCatchAll = false;
    for (Handler h : handlers) {
      dcheck(!wroteCatchAll);
      if (h instanceof CatchAllHandler) {
        ctx.jump(h.lStart, h.location);
        wroteCatchAll = true;
      } else {
        writeMatch((ExHandler) h);
      }
    }

    IHasLocation end = last(handlers).location.end();

    if (!wroteCatchAll) {
      ctx.write(LOADEXC, javaExc, end);
      ctx.write(ERASETEMP, pyExc, end);
      ctx.write(ERASETEMP, javaExc, end);
      ctx.write(THROW, end);
    }

    ctx.popTemp(savedPyExc, false, end);
    ctx.popTemp(pyExc, false, end);
    ctx.popTemp(javaExc, false, end);
  }

  void end(IHasLocation location) {
    writeExceptionHandler();
    ctx.write(lEnd, location);
    ctx.pop(this);
  }
}
