package org.japy.compiler.impl;

import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.CompilerConfig;
import org.japy.compiler.code.CBlock;
import org.japy.compiler.code.CModule;

final class ModuleWriter extends ScopeWriter<ModuleBlock, CModule, CModule.Builder> {
  final List<CBlock> children;

  ModuleWriter(ModuleBlock moduleBlock, CompilerConfig config, List<CBlock> children) {
    super(moduleBlock, Constants.MODULE, null, config, CModule.builder());
    this.children = children;
  }

  @Override
  protected @Nullable String qualName() {
    return null;
  }
}
