package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CInstrI.Code.CONST;
import static org.japy.compiler.impl.instr.CTInstrN.Code.*;

import org.japy.compiler.code.instr.CLabel;
import org.japy.infra.exc.InternalErrorException;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.stmt.AstClass;

public final class ClassBlock extends Scope<ClassBlock> {
  public final AstClass klass;

  private ClassBlock(Context ctx, AstClass klass) {
    super(ctx.scope(), klass);

    this.klass = klass;

    ctx.pushBlock(this);
  }

  private void end(Context ctx) {
    ctx.popBlock(this);
    ctx.compiler.defineClass(this);
  }

  static void compile(Context ctx, AstClass klass) {
    ClassBlock c = new ClassBlock(ctx, klass);
    ctx.compiler.compileBlockBody(klass, c);
    c.end(ctx);
  }

  @Override
  ClassWriter writer(ScopeWriter<?, ?, ?> parent) {
    return (ClassWriter) super.writer(parent);
  }

  @Override
  ClassWriter makeWriter(ScopeWriter<?, ?, ?> parent) {
    return new ClassWriter(this, parent);
  }

  @Override
  void setDocString(int docString, Token location) {
    AstIdentifier id = new AstIdentifier(location, Constants.__DOC__);
    boundName(id);
    write(CONST, docString, location);
    write(STORENAME, id, location);
  }

  @Override
  CLabel retLabel() {
    throw InternalErrorException.notReached();
  }
}
