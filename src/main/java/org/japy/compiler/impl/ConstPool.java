package org.japy.compiler.impl;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.misc.PyEllipsis.Ellipsis;
import static org.japy.kernel.types.misc.PyNone.None;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.mutable.MutableInt;

import org.japy.infra.coll.CollUtil;
import org.japy.infra.validation.Debug;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.set.PySet;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.misc.PySlice;
import org.japy.kernel.types.num.PyComplex;
import org.japy.kernel.types.num.PyFloat;
import org.japy.kernel.types.num.PyInt;

class ConstPool {
  private static final int LOTS_OF_ENTRIES = 4;

  private final List<IPyObj> constants = new ArrayList<>();

  public static boolean isLiteral(IPyObj value) {
    if (isSimpleLiteral(value)) {
      return true;
    }

    if (isContainer(value)) {
      MutableInt count = new MutableInt();
      countEntries(value, count);
      return count.intValue() <= LOTS_OF_ENTRIES;
    }

    return false;
  }

  private static boolean isContainer(IPyObj value) {
    return value instanceof PyTuple
        || value instanceof PyList
        || value instanceof PySet
        || value instanceof PyDict;
  }

  private static boolean isSimpleLiteral(IPyObj value) {
    // TODO store, dedup strings and bytes?
    return value == None
        || value == Ellipsis
        || (value instanceof PyStr && ((PyStr) value).isUcs2())
        || (value instanceof PyInt && ((PyInt) value).isLong())
        || (value instanceof PySlice
            && isLiteral(((PySlice) value).start)
            && isLiteral(((PySlice) value).stop)
            && isLiteral(((PySlice) value).step))
        || value instanceof PyFloat
        || value instanceof PyComplex
        || (value instanceof PyBytes && ((PyBytes) value).isAscii());
  }

  private static void countEntries(IPyObj value, MutableInt count) {
    CollUtil.doWithStop(
        () -> {
          if (count.incrementAndGet() > LOTS_OF_ENTRIES) {
            throw new CollUtil.StopIteration();
          }
          if (value instanceof PyTuple) {
            ((PyTuple) value).forEachNoCtx(v -> countEntries(v, count));
          } else if (value instanceof PySet) {
            ((PySet) value).forEachNoCtx(v -> countEntries(v, count));
          } else if (value instanceof PyList) {
            ((PyList) value).forEachNoCtx(v -> countEntries(v, count));
          } else if (value instanceof PyDict) {
            ((PyDict) value)
                .forEach(
                    (k, v) -> {
                      countEntries(k, count);
                      countEntries(v, count);
                    });
          } else if (!isSimpleLiteral(value)) {
            count.setValue(2 * LOTS_OF_ENTRIES);
            throw new CollUtil.StopIteration();
          }
        });
  }

  public int add(IPyObj value, boolean force) {
    if (!force && Debug.ENABLED) {
      dcheck(!isLiteral(value));
    }

    constants.add(value);
    return constants.size() - 1;
  }

  public int add(IPyObj value) {
    return add(value, false);
  }

  public IPyObj[] toArray() {
    return constants.toArray(IPyObj[]::new);
  }
}
