package org.japy.compiler.impl;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CUpvalue;
import org.japy.kernel.util.Signature;
import org.japy.parser.python.ast.node.AstParam;

final class FuncWriter extends ScopeWriter<FuncBlock, CFunc, CFunc.Builder> {
  private final String qualName;

  FuncWriter(FuncBlock funcBlock, ScopeWriter<?, ?, ?> parent) {
    super(
        funcBlock,
        funcBlock.name != null ? funcBlock.name.name : Constants.LAMBDA,
        parent,
        parent.config,
        CFunc.builder());
    dcheck(parent.scope == funcBlock.parentBlock);

    qualName = makeQualName(parent.qualName(), dcheckNotNull(codeName));
    blockBuilder.name(dcheckNotNull(codeName)).qualName(qualName);

    if (funcBlock.needClassCell()) {
      CUpvalue u =
          blockBuilder
              .addFlags(CFunc.FLAG_HAS_CLASS_CELL)
              .upvalue(CUpvalue.Kind.CLASS, Constants.__CLASS__, CUpvalue.INVALID_INDEX);
      dcheck(u.index == 0);
      @Nullable Ref ref = names.get(u.name);
      if (ref != null) {
        ref.makeUpvalue(u);
      } else {
        names.put(u.name, Ref.upvalue(u, scope));
      }
    }
  }

  @Override
  protected void setUsesLocals() {
    blockBuilder.addFlags(CFunc.FLAG_USES_LOCALS);
  }

  @Override
  CFunc build() {
    blockBuilder.signature(makeSignature()).docString(scope.docString());
    return super.build();
  }

  private Signature makeSignature() {
    Signature.Builder b = Signature.builder(scope.params.length);
    for (AstParam p : scope.params) {
      b.add(p.name.name, p.paramKind);
    }
    return b.build();
  }

  @Override
  protected void makeCell(LocalVar var) {
    if (var.isCell()) {
      return;
    }

    var.setCell(blockBuilder.newCell(var.name()));
  }

  @Override
  protected String qualName() {
    return qualName;
  }
}
