package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CInstr0.Code.*;
import static org.japy.compiler.code.instr.CInstrI.Code.*;
import static org.japy.compiler.code.instr.CInstrS.Code.*;
import static org.japy.compiler.code.instr.CJump.Code.*;
import static org.japy.compiler.impl.instr.CTInstrN.Code.*;
import static org.japy.infra.coll.CollUtil.last;
import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.coll.seq.PyTuple.EMPTY_TUPLE;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.AugOp;
import org.japy.base.BinOp;
import org.japy.compiler.CompilerConfig;
import org.japy.compiler.SyntaxError;
import org.japy.compiler.code.CBlock;
import org.japy.compiler.code.CVarType;
import org.japy.compiler.code.instr.CInstrI;
import org.japy.compiler.code.instr.CLabel;
import org.japy.compiler.code.instr.CLiteral;
import org.japy.compiler.impl.instr.CTClass;
import org.japy.compiler.impl.instr.CTFunc;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.LocationUtil;
import org.japy.infra.loc.Range;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Warnings;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.num.PyInt;
import org.japy.parser.python.ParserInput;
import org.japy.parser.python.ast.expr.*;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.node.*;
import org.japy.parser.python.ast.stmt.*;
import org.japy.parser.python.ast.visit.AstStmtVisit;
import org.japy.parser.python.ast.visit.IAstStmtVisitor;

class Compiler implements ICompiler {
  private final Context ctx;
  private final CompilerConfig config;
  private final ConstPool constPool;
  private final ParserInput input;
  private final ExprCompiler ec;
  private final @Nullable PyContext pyCtx;
  private @Nullable AstBlockProcessor astProcessor;
  private boolean annotationsAsStrings;

  Compiler(
      ModuleBlock module,
      CompilerConfig config,
      ConstPool constPool,
      ParserInput input,
      @Nullable PyContext pyCtx) {
    this.ctx = new Context(module, this);
    this.config = config;
    this.annotationsAsStrings = config.annotationsAsStrings();
    this.constPool = constPool;
    this.input = input;
    this.pyCtx = pyCtx;
    this.ec = new ExprCompiler(ctx, this, config);
  }

  void compileModule(AstModule astModule, ModuleBlock module) {
    astProcessor = new AstBlockProcessor(astModule, module);
    astProcessor.processBlock();
  }

  void warn(String message, IHasLocation location) {
    if (pyCtx != null) {
      Warnings.warn(message, pyCtx);
    }
  }

  @Override
  public boolean annotationsAsStrings() {
    return annotationsAsStrings;
  }

  @Override
  public void compileBlockBody(IAstBlock astBlock, Scope<?> block) {
    @Nullable AstBlockProcessor saved = astProcessor;
    astProcessor = new AstBlockProcessor(astBlock, block);
    astProcessor.processBlock();
    astProcessor = saved;
  }

  @Override
  public String getText(IAstExpr expr) {
    String text = input.getText(new Range(expr.start().start, expr.end().end));
    return text.strip();
  }

  @Override
  public void compileBool(IAstExpr expr) {
    ec.compileBool(expr);
  }

  @Override
  public void compileValue(IAstExpr expr) {
    ec.compile(expr);
  }

  @Override
  public void writeConstant(IPyObj value, IHasLocation location) {
    if (ConstPool.isLiteral(value)) {
      ctx.write(new CLiteral(value, location));
    } else {
      int index = constPool.add(value);
      ctx.write(CInstrI.Code.CONST, index, location);
    }
  }

  @Override
  public void compileReturn(@Nullable IAstExpr value, ITokenSpan location) {
    Block block = ctx.block();
    if (!(block instanceof FuncBlock)) {
      throw new SyntaxError("return not in a function", location);
    }
    ec.compileOrNone(value, location);
    ctx.ret(location);
  }

  @Override
  public void compileIf(IAstExpr condition, Runnable writeIfTrue, @Nullable Runnable writeIfFalse) {
    ec.compileBool(condition);
    CLabel ifFalse = ctx.newLabel();
    ctx.jump(IFFALSEBOOL, ifFalse, condition);
    writeIfTrue.run();
    if (writeIfFalse != null) {
      CLabel done = ctx.newLabel();
      ctx.jump(done, condition);
      ctx.write(ifFalse, condition);
      writeIfFalse.run();
      ctx.write(done, condition);
    } else {
      ctx.write(ifFalse, condition);
    }
  }

  private void evalAnnAssignTarget(IAstExpr target) {
    switch (target.kind()) {
      case IDENTIFIER:
        return;
      case ATTR_REF:
        ec.compile0(((AstAttrRef) target).obj);
        return;
      case SUBSCRIPT:
        {
          AstSubscript sub = (AstSubscript) target;
          ec.compile0(sub.obj);
          ec.compile0(sub.index);
          return;
        }
      case PAREN:
        evalAnnAssignTarget(((AstParen) target).expr);
        break;
      default:
        throw new SyntaxError("invalid syntax", target);
    }
  }

  private void assignTupleFromStack(IAstExpr[] targets) {
    int targetCount = targets.length;
    dcheck(targetCount != 0);
    @Var int tmp = targets.length;
    @Var
    @Nullable
    IAstExpr tmpE = null;
    for (int i = 0; i != targets.length; ++i) {
      IAstExpr tgt = skipParens(targets[i]);
      if (tgt.kind() == AstExprKind.STAR) {
        if (tmp < targets.length) {
          throw new SyntaxError("multiple starred assignment targets", tgt);
        }
        tmp = i;
        tmpE = ((AstStar) tgt).list;
      }
    }
    int star = tmp;
    @Nullable IAstExpr underStar = tmpE;

    ITokenSpan location = targets[0];

    if (star == targetCount) {
      ctx.write(UNPACKEXACT, targetCount, location);
      for (int i = 0; i != targetCount; ++i) {
        if (i != targetCount - 1) {
          ctx.write(DUP, targets[i]);
        }
        ctx.write(GETTUPLE, i, targets[i]);
        assignFromStack(targets[i]);
      }
      return;
    }

    TempVar list = ctx.pushTemp("list", CVarType.PYOBJ, location);
    TempVar len = ctx.pushTemp("len", CVarType.PYOBJ, location);
    ctx.write(UNPACKATLEAST, targetCount - 1, location);
    ctx.write(STORENAME, list.identifier, location);
    CodeWriter.writeCode(
        new CodeWriter(ctx, location) {
          @Override
          void write() {
            // $len = len($list)
            assign(len, call(SpecialFunc.LEN, list));

            for (int i = 0; i != star; ++i) {
              // tgt = $list[i]
              assign(targets[i], subscript(list.identifier, value(i)));
            }

            // *tgt = $list[star:star + $len - (targetCount - 1)]
            assign(
                underStar,
                subscript(
                    list.identifier,
                    slice(
                        value(star),
                        binOp(BinOp.ADD, len.identifier, value(star - targetCount + 1)))));

            for (int i = star + 1; i != targetCount; ++i) {
              // tgt = $list[$len - (targetCount - i)]
              assign(
                  targets[i],
                  subscript(
                      list.identifier, binOp(BinOp.SUB, len.identifier, value(targetCount - i))));
            }
          }
        });
    ctx.popTemp(len, location);
    ctx.popTemp(list, location);
  }

  private void assignFromStack(IAstExpr target) {
    dcheck(!target.canSuspend());

    switch (target.kind()) {
      case TUPLE:
        {
          AstTuple targetTuple = (AstTuple) target;
          if (targetTuple.entries.length != 0) {
            assignTupleFromStack(targetTuple.entries);
          } else {
            ctx.write(POP, target);
          }
          break;
        }

      case LIST:
        {
          AstList targetList = (AstList) target;
          if (targetList.entries.length != 0) {
            assignTupleFromStack(targetList.entries);
          } else {
            ctx.write(POP, target);
          }
          break;
        }

      case IDENTIFIER:
        {
          AstIdentifier identifier = (AstIdentifier) target;
          ctx.boundName(identifier);
          ctx.write(STORENAME, identifier, target);
          break;
        }

      case ATTR_REF:
        {
          AstAttrRef attrRef = (AstAttrRef) target;
          ec.compile(attrRef.obj);
          ctx.write(SWAP, attrRef.attr);
          ctx.write(SETATTR, attrRef.attr);
          break;
        }

      case SUBSCRIPT:
        {
          AstSubscript sub = (AstSubscript) target;
          ec.compile(sub.obj);
          ctx.write(SWAP, sub);
          ec.compile(sub.index);
          ctx.write(SWAP, sub);
          ctx.write(SETITEM, sub);
          break;
        }

      case PAREN:
        assignFromStack(((AstParen) target).expr);
        break;

      case STAR:
        throw InternalErrorException.notReached();

      default:
        throw new SyntaxError("invalid assignment target", target);
    }
  }

  private void visitAssign(IAstExpr[] targets, IAstExpr value) {
    if (AstNode.canSuspend(targets)) {
      throw new NotImplementedException("suspend");
    }

    dcheck(targets.length != 0);
    ec.compile(value);
    for (int i = 0; i != targets.length; ++i) {
      if (i != targets.length - 1) {
        ctx.write(DUP, targets[i]);
      }
      assignFromStack(targets[i]);
    }
  }

  @Override
  public void compileAssign(IAstExpr target, IAstExpr value) {
    visitAssign(new IAstExpr[] {target}, value);
  }

  @Override
  public void compileExprStmt(IAstExpr expr) {
    ec.compile0(expr);
  }

  private void compileBreak(ITokenSpan location) {
    @Nullable LoopHandler h = ctx.findHandler(LoopHandler.class);
    if (h == null) {
      throw new SyntaxError("break not in a loop", location);
    }
    h.visitBreak(location);
  }

  private void compileContinue(ITokenSpan location) {
    @Nullable LoopHandler h = ctx.findHandler(LoopHandler.class);
    if (h == null) {
      throw new SyntaxError("continue not in a loop", location);
    }
    h.visitContinue(location);
  }

  @Override
  public void compileRaise(@Nullable IAstExpr ex, @Nullable IAstExpr from, ITokenSpan location) {
    if (ex != null) {
      if (from != null) {
        ec.compile(ex, from);
        ctx.write(RAISE_FROM, location);
      } else {
        ec.compile(ex);
        ctx.write(RAISE_EX, location);
      }
    } else {
      ctx.write(RAISE, location);
    }
  }

  private void compileAsyncFor(
      IAstExpr target,
      IAstExpr iter,
      ITokenSpan location,
      Runnable writeBody,
      @Nullable Runnable writeElse,
      @Nullable ITokenSpan elseLocation) {
    // async for TARGET in ITER:
    //    SUITE
    // else:
    //    SUITE2
    //
    // ->
    //
    // iter = (ITER)
    // iter = type(iter).__aiter__(iter)
    // anext = type(iter).__anext__
    // running = True
    //
    // while running:
    //    try:
    //        TARGET = await anext(iter)
    //    except StopAsyncIteration:
    //        running = False
    //    else:
    //        SUITE
    // else:
    //    SUITE2

    TempVar tempIter = ctx.pushTemp("iter", CVarType.PYOBJ, iter);
    TempVar tempAnext = ctx.pushTemp("anext", CVarType.PYOBJ, iter);
    @Nullable
    TempVar tempRunning = writeElse != null ? ctx.pushTemp("running", CVarType.PYOBJ, iter) : null;

    CodeWriter.writeCode(
        new CodeWriter(ctx, iter) {
          @Override
          void write() {
            // iter = (ITER)
            assign(tempIter, iter);
            // iter = type(iter).__aiter__(iter)
            assign(tempIter, call(attr(type(tempIter), Constants.__AITER__), tempIter));
            // anext = type(iter).__anext__
            assign(tempAnext, attr(type(tempIter), Constants.__ANEXT__));
            // running = True
            if (writeElse != null) {
              assign(tempRunning, True);
            }

            Runnable writeWhileBody =
                () -> {
                  TryExceptHandler h =
                      new TryExceptHandler(
                          ctx,
                          location,
                          // TARGET = await anext(iter)
                          () -> assign(target, await(call(tempAnext, tempIter))),
                          // SUITE
                          writeBody);
                  h.visitHandler(
                      location,
                      id(SpecialVar.STOP_ASYNC_ITERATION),
                      null,
                      () -> {
                        // running = False
                        if (writeElse != null) {
                          assign(tempRunning, False);
                        } else {
                          compileBreak(location);
                        }
                      });
                  h.end(location);
                };

            compileWhile(
                writeElse != null ? tempRunning.identifier : value(True),
                writeWhileBody,
                writeElse,
                elseLocation,
                location);
          }
        });

    if (tempRunning != null) {
      ctx.popTemp(tempRunning, location.end().end);
    }
    ctx.popTemp(tempAnext, location.end().end);
    ctx.popTemp(tempIter, location.end().end);
  }

  private void compileRegularFor(
      IAstExpr target,
      IAstExpr iter,
      ITokenSpan location,
      Runnable writeBody,
      @Nullable Runnable writeElse,
      @Nullable ITokenSpan elseLocation) {
    // for TARGET in ITER:
    //    SUITE
    // else:
    //    SUITE2
    // ->
    //
    // iter = @iter(ITER)
    // while @notnull(value := @next(iter)):
    //    TARGET = value
    //    SUITE
    // else:
    //    SUITE2

    TempVar tempIter = ctx.pushTemp("iter", CVarType.PYFORITER, iter);
    TempVar tempValue = ctx.pushTemp("value", CVarType.PYOBJ, target);

    CodeWriter.writeCode(
        new CodeWriter(ctx, iter) {
          @Override
          void write() {
            // iter = @iter(ITER)
            assign(tempIter, call(id(SpecialFunc.MAKEFORITER), iter));
            compileWhile(
                // @notnull(value := @next(iter))
                call(
                    SpecialFunc.NOT_NULL,
                    walrus(tempValue, call(SpecialFunc.FORITERNEXT, tempIter))),
                () -> {
                  // TARGET = value
                  assign(target, tempValue.identifier);
                  // SUITE
                  writeBody.run();
                },
                // SUITE2
                writeElse,
                elseLocation,
                location);
          }
        });

    ctx.popTemp(tempValue, location.end().end);
    ctx.popTemp(tempIter, location.end().end);
  }

  @Override
  public void compileFor(
      boolean async,
      IAstExpr target,
      IAstExpr list,
      ITokenSpan location,
      Runnable writeBody,
      @Nullable Runnable writeElse,
      @Nullable ITokenSpan elseLocation) {
    if (async) {
      compileAsyncFor(target, list, location, writeBody, writeElse, elseLocation);
    } else {
      compileRegularFor(target, list, location, writeBody, writeElse, elseLocation);
    }
  }

  @Override
  public void compileWhile(
      IAstExpr condition,
      Runnable writeBody,
      @Nullable Runnable writeElse,
      @Nullable ITokenSpan elseLocation,
      ITokenSpan loopSpan) {
    WhileHandler.compile(ctx, condition, writeBody, writeElse, elseLocation, loopSpan);
  }

  @Override
  public void defineFunction(FuncBlock func) {
    ec.compileAnnotations(func);
    ec.compileParamDefaults(func);
    ctx.write(new CTFunc(func));
  }

  @Override
  public void defineClass(ClassBlock klass) {
    ec.compileArgs(klass.klass.supers, klass);
    ctx.write(new CTClass(klass));
  }

  @Override
  public void compileTryFinally(
      ITokenSpan wholeSpan,
      IHasLocation finallyLocation,
      Runnable writeBody,
      Runnable writeFinally) {
    TryFinallyHandler h = new TryFinallyHandler(ctx, wholeSpan);
    writeBody.run();
    h.beginFinally(dcheckNotNull(finallyLocation));
    writeFinally.run();
    h.end(wholeSpan.end().end);
  }

  private void futureImport(AstIdentifier identifier) {
    if (identifier.name.equals(Constants.ANNOTATIONS)) {
      annotationsAsStrings = true;
      return;
    }

    if (Constants.PAST_FUTURE_IMPORTS.contains(identifier.name)) {
      return;
    }

    throw new SyntaxError("unknown future feature: " + identifier.name, identifier);
  }

  private static IAstExpr skipParens(@Var IAstExpr expr) {
    while (expr.kind() == AstExprKind.PAREN) {
      expr = ((AstParen) expr).expr;
    }
    return expr;
  }

  private class AstBlockProcessor implements IAstStmtVisitor {
    private final IAstBlock astBlock;
    private final Scope<?> block;

    AstBlockProcessor(IAstBlock astBlock, Scope<?> block) {
      this.astBlock = astBlock;
      this.block = block;
    }

    void processBlock() {
      AstSuite suite = astBlock.body();
      int start = getDocString(suite.stmts[0]) ? 1 : 0;
      for (int i = start; i != suite.stmts.length; ++i) {
        processStmt(suite.stmts[i]);
      }
    }

    private void processStmt(IAstStmt stmt) {
      try {
        AstStmtVisit.visit(stmt, this);
      } catch (Throwable ex) {
        LocationUtil.setLocationIfNotSet(ex, stmt);
        throw ex;
      }
    }

    void processSuite(AstSuite suite) {
      for (IAstStmt stmt : suite.stmts) {
        processStmt(stmt);
      }
    }

    void processSuiteEnsureOp(AstSuite suite) {
      if (suite.isNop()) {
        ctx.write(NOP, suite);
      } else {
        processSuite(suite);
      }
    }

    private boolean getDocString(IAstStmt stmt) {
      if (!(stmt instanceof AstExprStmt)) {
        return false;
      }

      IAstExpr expr = ((AstExprStmt) stmt).expr;
      if (!expr.isConst() || !(expr instanceof AstString)) {
        return false;
      }

      PyStr docString = ConstEval.evalConstString((AstString) expr);
      block.setDocString(constPool.add(docString, true), stmt.start());
      return true;
    }

    private void applyDecorators(AstDecorator[] decorators) {
      if (Debug.ENABLED) {
        dcheck(!AstNode.canSuspend(decorators));
      }

      for (int i = decorators.length - 1; i >= 0; --i) {
        ec.compile(decorators[i].expr);
        ctx.write(SWAP, decorators[i]);
        ctx.write(CALL, 1, decorators[i]);
      }
    }

    @Override
    public void visitClass(AstClass stmt) {
      ctx.boundName(stmt.name);
      ClassBlock.compile(ctx, stmt);
      applyDecorators(stmt.decorators);
      ctx.write(STORENAME, stmt.name, stmt);
    }

    @Override
    public void visitFunc(AstFunc stmt) {
      ctx.boundName(stmt.header.name);
      FuncBlock.compile(ctx, stmt);
      applyDecorators(stmt.header.decorators);
      ctx.write(STORENAME, stmt.header.name, stmt);
    }

    // import x.y as z -> __t = __import__('x', ['y'], 0); z = __t.y
    private void visitImportAsMultiLevel(AstImportedModule mod, ITokenSpan location) {
      String lastPart = last(mod.module.components).name;
      ctx.write(PyStr.get(mod.module.toStringNoLastPart()), mod);
      ec.compileList(1, i -> ctx.write(PyStr.get(lastPart), mod), mod);
      ctx.write(PyInt.ZERO, mod);
      ctx.write(IMPORT, mod);
      ctx.write(GETATTR, lastPart, mod);
      AstIdentifier target = dcheckNotNull(mod.as);
      ctx.boundName(target);
      ctx.write(STORENAME, target, location);
    }

    // import y as z -> __t = __import__('y', [], 0); z = __t
    private void visitImportAsSingleLevel(AstImportedModule mod, ITokenSpan location) {
      ctx.write(PyStr.get(mod.module.toString()), mod);
      ctx.write(EMPTY_TUPLE, mod);
      ctx.write(PyInt.ZERO, mod);
      ctx.write(IMPORT, mod);
      AstIdentifier target = dcheckNotNull(mod.as);
      ctx.boundName(target);
      ctx.write(STORENAME, target, location);
    }

    // import <mod> as x
    private void visitImportAs(AstImportedModule mod, ITokenSpan location) {
      if (mod.module.components.length != 1) {
        visitImportAsMultiLevel(mod, location);
      } else {
        visitImportAsSingleLevel(mod, location);
      }
    }

    // import <mod> -> __import__(<mod>, [], 0)
    private void visitDirectImport(AstImportedModule mod, ITokenSpan location) {
      ctx.write(PyStr.get(mod.module.toString()), mod);
      ctx.write(EMPTY_TUPLE, mod);
      ctx.write(PyInt.ZERO, mod);
      ctx.write(IMPORT, mod);
      AstIdentifier target = mod.module.components[0];
      ctx.boundName(target);
      ctx.write(STORENAME, target, location);
    }

    private void visitImport(AstImportedModule mod, ITokenSpan location) {
      dcheck(mod.module.dots == 0);
      if (mod.as != null) {
        visitImportAs(mod, location);
      } else {
        visitDirectImport(mod, location);
      }
    }

    @Override
    public void visitImport(AstImport stmt) {
      for (AstImportedModule mod : stmt.modules) {
        visitImport(mod, mod);
      }
    }

    private void visitFutureImport(IAstImportedName[] names, ITokenSpan location) {
      if (!(ctx.block() instanceof ModuleBlock)
          || ((ModuleBlock) ctx.block()).hasNonDocStringInsn()) {
        throw new SyntaxError("future import is not allowed here", location);
      }

      for (IAstImportedName name : names) {
        switch (name.kind()) {
          case STAR:
            throw new SyntaxError("from future import *", name);
          case NAME:
            {
              AstImportedName impName = (AstImportedName) name;
              if (impName.as != null) {
                throw new SyntaxError("from future import as name", impName.as);
              }
              futureImport(impName.identifier);
              break;
            }
        }
      }
    }

    @Override
    public void visitImportFrom(AstImportFrom stmt) {
      if (stmt.module.dots == 0
          && stmt.module.components.length == 1
          && stmt.module.components[0].name.equals(Constants.__FUTURE__)) {
        visitFutureImport(stmt.names, stmt);
        return;
      }

      @Var boolean hasStar = false;

      for (IAstImportedName name : stmt.names) {
        switch (name.kind()) {
          case NAME:
            if (hasStar) {
              throw new SyntaxError("import * and name is not allowed", stmt);
            }
            break;

          case STAR:
            if (!(ctx.block() instanceof ModuleBlock)) {
              throw new SyntaxError("import * is not allowed here", stmt);
            }
            if (hasStar) {
              throw new SyntaxError("import *, *", stmt);
            }
            hasStar = true;
            break;
        }
      }

      ctx.write(PyStr.get(stmt.module.toStringNoLeadingDots()), stmt);

      if (hasStar) {
        ec.compileList(1, i -> ctx.write(PyStr.ucs2("*"), stmt), stmt);
      } else {
        ec.compileList(
            stmt.names.length,
            i -> {
              AstImportedName name = (AstImportedName) stmt.names[i];
              ctx.write(PyStr.get(name.identifier.name), stmt);
            },
            stmt);
      }

      ctx.write(PyInt.get(stmt.module.dots), stmt);
      ctx.write(IMPORT, stmt);

      if (hasStar) {
        ctx.write(IMPORTSTAR, stmt);
      } else {
        for (int i = 0; i != stmt.names.length; ++i) {
          AstImportedName name = (AstImportedName) stmt.names[i];
          if (i != stmt.names.length - 1) {
            ctx.write(DUP, name);
          }
          ctx.write(GETATTR, name.identifier);
          AstIdentifier target = name.as != null ? name.as : name.identifier;
          ctx.boundName(target);
          ctx.write(STORENAME, target, stmt.location);
        }
      }
    }

    private void compileTryExcept(AstTry stmt) {
      dcheck(stmt.hasExcept());
      TryExceptHandler h =
          new TryExceptHandler(
              ctx,
              stmt,
              () -> processSuiteEnsureOp(stmt.body),
              stmt.else_ != null ? () -> processSuite(stmt.else_.body) : null);
      for (AstExcept except : stmt.except) {
        h.visitHandler(except, except.ex, except.as, () -> processSuite(except.body));
      }
      h.end(stmt.end().end);
    }

    @Override
    public void visitTry(AstTry stmt) {
      // Decompose try-except-else-finally into a combination of try-finally and try-except-else
      if (stmt.finallyBody != null) {
        compileTryFinally(
            stmt,
            dcheckNotNull(stmt.finallyLocation),
            () -> {
              if (stmt.hasExcept()) {
                compileTryExcept(stmt);
              } else {
                dcheck(!stmt.hasElse());
                processSuiteEnsureOp(stmt.body);
              }
            },
            () -> processSuite(stmt.finallyBody));
      } else {
        dcheck(stmt.hasExcept());
        compileTryExcept(stmt);
      }
    }

    private void visitIf(AstIf stmt, int ibranch) {
      AstIfBranch b = stmt.branches[ibranch];
      if (ibranch != stmt.branches.length - 1) {
        compileIf(b.condition, () -> processSuite(b.body), () -> visitIf(stmt, ibranch + 1));
      } else if (stmt.else_ != null) {
        compileIf(b.condition, () -> processSuite(b.body), () -> processSuite(stmt.else_.body));
      } else {
        compileIf(b.condition, () -> processSuite(b.body));
      }
    }

    @Override
    public void visitIf(AstIf stmt) {
      visitIf(stmt, 0);
    }

    @Override
    public void visitFor(AstFor stmt) {
      compileFor(
          stmt.async,
          stmt.target,
          stmt.list,
          stmt,
          () -> processSuite(stmt.body),
          stmt.else_ != null ? () -> processSuite(stmt.else_.body) : null,
          stmt.else_);
    }

    private void compileWith(
        boolean async,
        IAstExpr expr,
        @Nullable IAstExpr target,
        Runnable writeBody,
        ITokenSpan location) {
      // with EXPRESSION as TARGET:
      //    SUITE
      //
      // ->
      //
      // manager = (EXPRESSION)
      // enter = type(manager).__enter__
      // exit = type(manager).__exit__
      // value = enter(manager)
      // hit_except = False
      //
      // try:
      //    TARGET = value
      //    SUITE
      // except:
      //    hit_except = True
      //    if not exit(manager, *sys.exc_info()):
      //        raise
      // finally:
      //    if not hit_except:
      //        exit(manager, None, None, None)

      // async with EXPRESSION as TARGET:
      //    SUITE
      //
      // ->
      //
      // manager = (EXPRESSION)
      // aenter = type(manager).__aenter__
      // aexit = type(manager).__aexit__
      // value = await aenter(manager)
      // hit_except = False
      //
      // try:
      //    TARGET = value
      //    SUITE
      // except:
      //    hit_except = True
      //    if not await aexit(manager, *sys.exc_info()):
      //        raise
      // finally:
      //    if not hit_except:
      //        await aexit(manager, None, None, None)

      TempVar manager = ctx.pushTemp("manager", CVarType.PYOBJ, location);
      TempVar enter = ctx.pushTemp("enter", CVarType.PYOBJ, location);
      TempVar exit = ctx.pushTemp("exit", CVarType.PYOBJ, location);
      TempVar value = ctx.pushTemp("value", CVarType.PYOBJ, location);
      TempVar hitExcept = ctx.pushTemp("hit_except", CVarType.PYOBJ, location);

      CodeWriter.writeCode(
          new CodeWriter(ctx, location) {
            @Override
            void write() {
              // manager = (EXPRESSION)
              assign(manager, expr);
              // enter = type(manager).__enter__
              // aenter = type(manager).__aenter__
              assign(
                  enter, attr(type(manager), async ? Constants.__AENTER__ : Constants.__ENTER__));
              // exit = type(manager).__exit__
              // aexit = type(manager).__aexit__
              assign(exit, attr(type(manager), async ? Constants.__AEXIT__ : Constants.__EXIT__));
              // value = enter(manager)
              // value = await aenter(manager)
              assign(value, async ? await(call(enter, manager)) : call(enter, manager));
              // hit_except = False
              assign(hitExcept, False);

              TryFinallyHandler.compile(
                  ctx,
                  location,
                  () ->
                      TryExceptHandler.compileTryExcept(
                          ctx,
                          location,
                          () -> {
                            // TARGET = value
                            if (target != null) {
                              assign(target, value);
                            }
                            // SUITE
                            writeBody.run();
                          },
                          () -> {
                            // hit_except = True
                            assign(hitExcept, True);
                            // if not exit(manager, *sys.exc_info()):
                            //     raise
                            // if not await aexit(manager, *sys.exc_info()):
                            //     raise
                            IAstExpr callExit =
                                call(exit.identifier, manager.identifier, starSysExcInfo());
                            if_(not(async ? await(callExit) : callExit), this::raise);
                          }),
                  () -> {
                    // if not hit_except:
                    //     exit(manager, None, None, None)
                    //     await aexit(manager, None, None, None)
                    if_(
                        not(hitExcept.identifier),
                        () -> {
                          IAstExpr callExit =
                              call(
                                  exit.identifier,
                                  manager.identifier,
                                  value(None),
                                  value(None),
                                  value(None));
                          eval(async ? await(callExit) : callExit);
                        });
                  },
                  location);
            }
          });

      ctx.popTemp(hitExcept, location.end().end);
      ctx.popTemp(value, location.end().end);
      ctx.popTemp(exit, location.end().end);
      ctx.popTemp(enter, location.end().end);
      ctx.popTemp(manager, location.end().end);
    }

    private void visitWith(AstWith stmt, int i) {
      compileWith(
          stmt.async,
          stmt.items[i].expr,
          stmt.items[i].as,
          () -> {
            if (i == stmt.items.length - 1) {
              processSuite(stmt.body);
            } else {
              visitWith(stmt, i + 1);
            }
          },
          stmt);
    }

    @Override
    public void visitWith(AstWith stmt) {
      visitWith(stmt, 0);
    }

    @Override
    public void visitWhile(AstWhile stmt) {
      compileWhile(
          stmt.condition,
          () -> processSuite(stmt.body),
          stmt.else_ != null ? () -> processSuite(stmt.else_.body) : null,
          stmt.else_ != null ? stmt.else_ : null,
          stmt);
    }

    @Override
    public void visitPass(AstPass stmt) {}

    @Override
    public void visitDecl(AstVarDecl stmt) {
      for (AstIdentifier identifier : stmt.identifiers) {
        switch (stmt.declKind) {
          case GLOBAL:
            ctx.globalDecl(identifier);
            break;
          case NONLOCAL:
            ctx.nonlocalDecl(identifier);
            break;
        }
      }
    }

    @Override
    public void visitRaise(AstRaise stmt) {
      compileRaise(stmt.ex, stmt.from, stmt);
    }

    @Override
    public void visitReturn(AstReturn stmt) {
      compileReturn(stmt.value, stmt);
    }

    @Override
    public void visitExpr(AstExprStmt stmt) {
      compileExprStmt(stmt.expr);
    }

    @Override
    public void visitAnnExpr(AstAnnExpr stmt) {
      IAstExpr target = stmt.expr;
      if (target.kind() == AstExprKind.IDENTIFIER && ctx.scope() instanceof FuncBlock) {
        ctx.block().boundName((AstIdentifier) target);
      }

      evalAnnAssignTarget(target);

      compileAnnotation(target, stmt.annotation);
    }

    @Override
    public void visitAssign(AstAssign stmt) {
      Compiler.this.visitAssign(stmt.targets, stmt.value);
    }

    private void checkValidAnnotationTarget(IAstExpr target, Scope<?> scope) {
      switch (target.kind()) {
        case IDENTIFIER:
          {
            if (scope instanceof ModuleBlock) {
              return;
            }

            AstIdentifier id = (AstIdentifier) target;
            @Nullable Ref ref = scope.names.get(id.name);
            if (ref != null) {
              switch (ref.kind()) {
                case GLOBAL:
                  throw new SyntaxError(
                      String.format("annotated name '%s' can't be global", id.name), target);

                case UPVALUE:
                case NONLOCAL:
                  throw new SyntaxError(
                      String.format("annotated name '%s' can't be nonlocal", id.name), target);

                default:
                  break;
              }
            }

            return;
          }

        case ATTR_REF:
        case SUBSCRIPT:
          return;

        case PAREN:
          checkValidAnnotationTarget(((AstParen) target).expr, scope);
          return;

        case TUPLE:
          throw new SyntaxError("only single target (not tuple) can be annotated", target);

        case LIST:
          throw new SyntaxError("only single target (not list) can be annotated", target);

        default:
          throw InternalErrorException.notReached();
      }
    }

    private void compileAnnotation(IAstExpr target, IAstExpr annotation) {
      Scope<?> scope = ctx.scope();
      checkValidAnnotationTarget(target, scope);

      // Annotations are not evaluated in functions
      if (scope instanceof FuncBlock) {
        return;
      }

      if (target.kind() == AstExprKind.IDENTIFIER) {
        ctx.boundName(
            new AstIdentifier(annotation.start(), org.japy.kernel.util.Constants.__ANNOTATIONS__));
        ec.compileAnnotation(annotation);
        scope.flags |= CBlock.FLAG_HAS_ANNOTATIONS;
        ctx.write(ANNOTATENAME, (AstIdentifier) target, target);
      } else {
        ec.compileAnnotation0(annotation);
      }
    }

    @Override
    public void visitAnnAssign(AstAnnAssign stmt) {
      IAstExpr target = stmt.target;
      if (target.kind() == AstExprKind.IDENTIFIER) {
        ctx.block().boundName((AstIdentifier) target);
      }

      compileAssign(target, stmt.value);

      compileAnnotation(target, stmt.annotation);
    }

    private void visitAugAssign(AugOp op, @Var IAstExpr target, IAstExpr value) {
      target = skipParens(target);

      switch (target.kind()) {
        case IDENTIFIER:
          ec.compile(value);
          ctx.write(AUGASSIGNNAME, (AstIdentifier) target, op.ordinal(), value);
          break;

        case ATTR_REF:
          {
            AstAttrRef attrRef = (AstAttrRef) target;
            ec.compile(value);
            ec.compile(attrRef.obj);
            ctx.write(SWAP, target);
            ctx.write(AUGASSIGNATTR, attrRef.attr.name, op.ordinal(), target);
            break;
          }

        case SUBSCRIPT:
          {
            AstSubscript sub = (AstSubscript) target;
            ec.compile(value);
            ec.compile(sub.obj);
            ctx.write(SWAP, target);
            ec.compile(sub.index);
            ctx.write(SWAP, target);
            ctx.write(AUGASSIGNINDEX, op.ordinal(), target);
            break;
          }

        default:
          throw new SyntaxError("invalid assignment target", target);
      }
    }

    @Override
    public void visitAugAssign(AstAugAssign stmt) {
      visitAugAssign(stmt.op, stmt.target, stmt.value);
    }

    private void visitDel(IAstExpr target, IHasLocation location) {
      switch (target.kind()) {
        case TUPLE:
          {
            AstTuple t = (AstTuple) target;
            for (IAstExpr expr : t.entries) {
              visitDel(expr, location);
            }
            break;
          }

        case LIST:
          {
            AstList t = (AstList) target;
            for (IAstExpr expr : t.entries) {
              visitDel(expr, location);
            }
            break;
          }

        case PAREN:
          visitDel(((AstParen) target).expr, location);
          break;

        case IDENTIFIER:
          {
            AstIdentifier identifier = (AstIdentifier) target;
            ctx.boundName(identifier);
            ctx.write(DELNAME, identifier, location);
            break;
          }

        case ATTR_REF:
          {
            AstAttrRef attrRef = (AstAttrRef) target;
            ec.compile(attrRef.obj);
            ctx.write(DELATTR, attrRef.attr);
            break;
          }

        case SUBSCRIPT:
          {
            AstSubscript sub = (AstSubscript) target;
            ec.compile(sub.obj);
            ec.compile(sub.index);
            ctx.write(DELITEM, target);
            break;
          }

        default:
          throw new SyntaxError("invalid del target", target);
      }
    }

    @Override
    public void visitDel(AstDel stmt) {
      visitDel(stmt.expr, stmt);
    }

    private boolean isAlwaysTrue(IAstExpr expr) {
      @Nullable Boolean b = ConstEval.tryEvalBool(expr);
      if (b != null) {
        return b;
      }

      if (expr.kind() == AstExprKind.TUPLE) {
        return ((AstTuple) expr).entries.length != 0;
      }

      return false;
    }

    @Override
    public void visitAssert(AstAssert stmt) {
      if (!config.enableAssert()) {
        return;
      }

      String exprText = getText(stmt.expr);

      if (isAlwaysTrue(stmt.expr)) {
        warn("assertion is always true", stmt);
      }

      CodeWriter.writeCode(
          new CodeWriter(ctx, stmt.expr) {
            @Override
            void write() {
              if_(
                  id(SpecialVar.__DEBUG__),
                  () ->
                      if_(
                          not(stmt.expr),
                          () -> {
                            if (stmt.message == null) {
                              eval(call(SpecialFunc.ASSERTION_FAILED, value(PyStr.get(exprText))));
                            } else {
                              eval(
                                  call(
                                      SpecialFunc.ASSERTION_FAILED_MSG,
                                      stmt.message,
                                      value(PyStr.get(exprText))));
                            }
                          }));
            }
          });
    }

    @Override
    public void visitBreak(AstBreak stmt) {
      Compiler.this.compileBreak(stmt);
    }

    @Override
    public void visitContinue(AstContinue stmt) {
      Compiler.this.compileContinue(stmt);
    }
  }
}
