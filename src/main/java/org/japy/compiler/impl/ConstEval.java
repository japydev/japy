package org.japy.compiler.impl;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;

import java.util.Arrays;
import java.util.function.Supplier;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.LogicalOp;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.IHasWritableLocation;
import org.japy.infra.validation.Debug;
import org.japy.kernel.misc.ConstOps;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.set.PySet;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.coll.str.PyStrBuilder;
import org.japy.kernel.types.exc.px.PxBaseException;
import org.japy.kernel.types.misc.PySlice;
import org.japy.kernel.types.num.PyBool;
import org.japy.parser.python.ast.expr.*;
import org.japy.parser.python.ast.node.AstKeyDatum;
import org.japy.parser.python.ast.node.AstStringLiteral;
import org.japy.parser.python.ast.node.IAstDictEntry;
import org.japy.parser.python.ast.visit.AstExprVisit;
import org.japy.parser.python.ast.visit.BaseAstExprEval;

final class ConstEval {
  public enum ResultType {
    CONST,
    COMPTIME,
  }

  public static class Result {
    final ResultType type;
    final IPyObj value;

    public Result(ResultType type, IPyObj value) {
      this.type = type;
      this.value = value;
    }
  }

  private static final ConstEvalVisitor constVisitor = new ConstEvalVisitor();
  private static final CompTimeVisitor compTimeVisitor = new CompTimeVisitor();

  public static @Nullable Result tryEval(IAstExpr expr) {
    return wrapErrors(
        () -> {
          if (expr.isConst()) {
            return new Result(ResultType.CONST, evalConst(expr));
          } else if (expr.isCompTimeValue()) {
            return new Result(ResultType.COMPTIME, evalCompTime(expr));
          } else {
            return null;
          }
        },
        expr);
  }

  public static PyStr evalConstString(AstString expr) {
    @Nullable Result r = tryEval(expr);
    if (Debug.ENABLED) {
      dcheckNotNull(r);
      dcheck(r.type == ResultType.CONST);
      dcheck(r.value instanceof PyStr);
    }
    return (PyStr) r.value;
  }

  public static @Nullable Boolean tryEvalBool(IAstExpr expr) {
    return wrapErrors(() -> expr.isConst() ? evalBool(expr) : null, expr);
  }

  static <T> @Nullable T wrapErrors(Supplier<@Nullable T> func, IHasLocation location) {
    try {
      return func.get();
    } catch (NotImplementedException | ConstOps.NotSupportedException | PxBaseException ex) {
      return null;
    } catch (RuntimeException ex) {
      if (ex instanceof IHasWritableLocation) {
        ((IHasWritableLocation) ex).setLocationIfNotSet(location.location());
        throw ex;
      }
      if (!(ex instanceof IHasLocation) || ((IHasLocation) ex).location().isUnknown()) {
        throw new InternalErrorException(ex.getMessage(), location, ex);
      }
      throw ex;
    }
  }

  private static IPyObj evalConst(IAstExpr expr) {
    if (Debug.ENABLED) {
      dcheck(expr.isConst());
    }
    return AstExprVisit.eval(expr, constVisitor);
  }

  private static boolean evalBool(IAstExpr expr) {
    if (Debug.ENABLED) {
      dcheck(expr.isConst());
    }
    return ConstOps.isTrue(evalConst(expr));
  }

  private static IPyObj evalCompTime(IAstExpr expr) {
    if (Debug.ENABLED) {
      dcheck(expr.isCompTimeValue());
    }
    return AstExprVisit.eval(expr, compTimeVisitor);
  }

  private static class ConstEvalVisitor extends BaseAstExprEval<IPyObj> {
    @Override
    public IPyObj visitAny(IAstExpr expr) {
      throw new InternalErrorException("unhandled const expression type " + expr.kind(), expr);
    }

    @Override
    public IPyObj visit(AstConst expr) {
      return expr.value;
    }

    @Override
    public IPyObj visit(AstString expr) {
      if (expr.chunks.length == 1) {
        return PyStr.get(((AstStringLiteral) expr.chunks[0]).text);
      }
      PyStrBuilder b = new PyStrBuilder();
      for (int i = 0; i != expr.chunks.length; ++i) {
        b.appendUnknown(((AstStringLiteral) expr.chunks[i]).text);
      }
      return b.build();
    }

    @Override
    public IPyObj visit(AstTuple expr) {
      return new PyTuple(
          Arrays.stream(expr.entries).map(ConstEval::evalConst).toArray(IPyObj[]::new));
    }

    @Override
    public IPyObj visit(AstParen expr) {
      return evalConst(expr.expr);
    }

    @Override
    public IPyObj visit(AstNot expr) {
      return PyBool.of(!evalBool(expr.expr));
    }

    @Override
    public IPyObj visit(AstComparison expr) {
      @Var IPyObj lhs = evalConst(expr.operands[0]);
      for (int i = 0; i != expr.ops.length; ++i) {
        IPyObj rhs = evalConst(expr.operands[i + 1]);
        if (!ConstOps.compOp(expr.ops[i], lhs, rhs)) {
          return False;
        }
        lhs = rhs;
      }
      return True;
    }

    @Override
    public IPyObj visit(AstBinOp expr) {
      // There is test code which does things like 'T' * 2 ** 25, which produces a string too large
      // to be stored as a const. We don't really win anything with const-folding string operations,
      // so we just don't try.
      IPyObj lhs = evalConst(expr.lhs);
      IPyObj rhs = evalConst(expr.rhs);
      if (lhs instanceof PyStr
          || lhs instanceof PyBytes
          || rhs instanceof PyStr
          || rhs instanceof PyBytes) {
        throw new ConstOps.NotSupportedException();
      }
      return ConstOps.binOp(expr.op, lhs, rhs);
    }

    @Override
    public IPyObj visit(AstUnaryOp expr) {
      return ConstOps.unaryOp(expr.op, evalConst(expr.operand));
    }

    @Override
    public IPyObj visit(AstLogicalOp expr) {
      IPyObj lhs = evalConst(expr.lhs);
      return (expr.op == LogicalOp.AND) == ConstOps.isTrue(lhs) ? evalConst(expr.rhs) : lhs;
    }

    @Override
    public IPyObj visit(AstConditional expr) {
      return evalBool(expr.condition) ? evalConst(expr.ifTrue) : evalConst(expr.ifFalse);
    }

    @Override
    public IPyObj visit(AstSlice expr) {
      return new PySlice(
          expr.lowerBound != null ? evalConst(expr.lowerBound) : None,
          expr.upperBound != null ? evalConst(expr.upperBound) : None,
          expr.step != null ? evalConst(expr.step) : None);
    }
  }

  private static class CompTimeVisitor extends BaseAstExprEval<IPyObj> {
    @Override
    public IPyObj visitAny(IAstExpr expr) {
      throw new InternalErrorException(
          "unhandled comptime value expression type " + expr.kind(), expr);
    }

    @Override
    public IPyObj visit(AstTuple expr) {
      return new PyTuple(
          Arrays.stream(expr.entries).map(ConstEval::evalCompTime).toArray(IPyObj[]::new));
    }

    @Override
    public IPyObj visit(AstList expr) {
      return new PyList(
          Arrays.stream(expr.entries).map(ConstEval::evalCompTime).toArray(IPyObj[]::new));
    }

    @Override
    public IPyObj visit(AstSet expr) {
      return new PySet(
          Arrays.stream(expr.entries).map(ConstEval::evalCompTime).toArray(IPyObj[]::new),
          ConstOps.CONTEXT);
    }

    @Override
    public IPyObj visit(AstDict expr) {
      PyDict dict = new PyDict(expr.entries.length);
      for (IAstDictEntry e : expr.entries) {
        AstKeyDatum d = (AstKeyDatum) e;
        dict.setItem(evalCompTime(d.key), evalCompTime(d.value), ConstOps.CONTEXT);
      }
      return dict;
    }

    @Override
    public IPyObj visit(AstParen expr) {
      return evalCompTime(expr.expr);
    }

    @Override
    public IPyObj visit(AstConst expr) {
      return constVisitor.visit(expr);
    }

    @Override
    public IPyObj visit(AstString expr) {
      return constVisitor.visit(expr);
    }

    @Override
    public IPyObj visit(AstSlice expr) {
      return constVisitor.visit(expr);
    }

    @Override
    public IPyObj visit(AstNot expr) {
      throw new ConstOps.NotSupportedException();
    }

    @Override
    public IPyObj visit(AstComparison expr) {
      throw new ConstOps.NotSupportedException();
    }

    @Override
    public IPyObj visit(AstBinOp expr) {
      throw new ConstOps.NotSupportedException();
    }

    @Override
    public IPyObj visit(AstUnaryOp expr) {
      throw new ConstOps.NotSupportedException();
    }

    @Override
    public IPyObj visit(AstLogicalOp expr) {
      throw new ConstOps.NotSupportedException();
    }

    @Override
    public IPyObj visit(AstConditional expr) {
      throw new ConstOps.NotSupportedException();
    }
  }
}
