package org.japy.compiler.impl;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.code.CUpvalue;

public final class Ref {
  enum Kind {
    UNKNOWN,
    NONLOCAL,
    LOCAL,
    TEMP,
    GLOBAL,
    UPVALUE,
  }

  private Kind kind;
  public final String name;
  final Block block;
  private @Nullable Object referent;

  private Ref(Kind kind, String name, Block block, @Nullable Object referent) {
    this.kind = kind;
    this.name = name;
    this.block = block;
    this.referent = referent;
  }

  Kind kind() {
    return kind;
  }

  LocalVar _local() {
    dcheck(kind == Kind.LOCAL);
    dcheckNotNull(referent);
    return (LocalVar) referent;
  }

  TempVar temp() {
    dcheck(kind == Kind.TEMP);
    dcheckNotNull(referent);
    return (TempVar) referent;
  }

  CUpvalue upvalue() {
    dcheck(kind == Kind.UPVALUE);
    dcheckNotNull(referent);
    return (CUpvalue) referent;
  }

  void makeLocal(LocalVar localVar) {
    dcheck(kind == Kind.UNKNOWN);
    kind = Kind.LOCAL;
    referent = localVar;
  }

  void makeGlobal() {
    dcheck(kind == Kind.UNKNOWN || kind == Kind.NONLOCAL);
    dcheck(!name.startsWith(Constants.TEMP_PREFIX));
    kind = Kind.GLOBAL;
    referent = null;
  }

  void makeNonlocal() {
    dcheck(kind == Kind.UNKNOWN);
    kind = Kind.NONLOCAL;
  }

  void makeUpvalue(CUpvalue upvalue) {
    dcheck(kind == Kind.UNKNOWN || kind == Kind.NONLOCAL);
    kind = Kind.UPVALUE;
    referent = upvalue;
  }

  static Ref upvalue(CUpvalue upvalue, Block block) {
    return new Ref(Kind.UPVALUE, upvalue.name, block, upvalue);
  }

  static Ref unknown(String name, Block block) {
    return new Ref(Kind.UNKNOWN, name, block, null);
  }

  static Ref nonlocal(String name, Block block) {
    return new Ref(Kind.NONLOCAL, name, block, null);
  }

  static Ref global(String name, Block block) {
    dcheck(!name.startsWith(Constants.TEMP_PREFIX));
    return new Ref(Kind.GLOBAL, name, block, null);
  }

  static Ref local(LocalVar var) {
    return new Ref(Kind.LOCAL, var.identifier.name, var.block, var);
  }

  static Ref temp(TempVar var) {
    return new Ref(Kind.TEMP, var.identifier.name, var.block, var);
  }

  @Override
  public String toString() {
    return "Ref{"
        + "kind="
        + kind
        + ", name="
        + name
        + ", block="
        + block
        + ", referent="
        + referent
        + '}';
  }
}
