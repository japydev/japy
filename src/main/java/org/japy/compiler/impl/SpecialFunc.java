package org.japy.compiler.impl;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;

enum SpecialFunc {
  LEN("@len"),
  MAKEFORITER("@make_for_iter"),
  FORITERNEXT("@for_iter_next"),
  SYS_EXC_INFO("@sys_exc_info"),
  NOT_NULL("@not_null"),
  NEW_LIST("@new_list"),
  LIST_ADD("@list_add"),
  NEW_SET("@new_set"),
  SET_ADD("@set_add"),
  NEW_DICT("@new_dict"),
  DICT_ADD("@dict_add"),
  ASSERTION_FAILED("@assertion_failed"),
  ASSERTION_FAILED_MSG("@assertion_failed_msg"),
  TYPE("@type"),
  ;

  public static final String PREFIX = "@";

  public final String name;

  SpecialFunc(String name) {
    this.name = name;
  }

  public static @Nullable SpecialFunc get(String name) {
    if (!name.startsWith(PREFIX)) {
      return null;
    }

    for (SpecialFunc v : values()) {
      if (v.name.equals(name)) {
        return v;
      }
    }

    throw InternalErrorException.notReached("unknown special function " + name);
  }
}
