package org.japy.compiler.impl;

import org.checkerframework.checker.nullness.qual.Nullable;

abstract class BaseControlHandler implements ControlHandler {
  private final @Nullable ControlHandler parent;
  protected final int id;
  protected final Context ctx;

  protected BaseControlHandler(Context ctx) {
    this.parent = ctx.topHandler();
    this.ctx = ctx;
    ctx.push(this);
    if (parent instanceof BaseControlHandler) {
      id = ((BaseControlHandler) parent).id + 1;
    } else {
      id = 1;
    }
  }

  @Override
  public @Nullable ControlHandler parent() {
    return parent;
  }
}
