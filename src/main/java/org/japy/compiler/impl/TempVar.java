package org.japy.compiler.impl;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Validator.check;

import org.checkerframework.checker.nullness.qual.MonotonicNonNull;

import org.japy.compiler.code.CTempVar;
import org.japy.compiler.code.CVarType;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.ValidationError;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.ISingleTokenSpan;
import org.japy.parser.python.ast.loc.Token;

public final class TempVar implements ISingleTokenSpan {
  public static final int FLAG_CELL = 1;
  public static final int FLAG_UNUSED = 2;

  public final Block block;
  public final AstIdentifier identifier;
  public final CVarType type;
  private int flags;

  public @MonotonicNonNull CTempVar cv;

  TempVar(Block block, AstIdentifier identifier, CVarType type) {
    this.block = block;
    this.type = type;
    this.identifier = identifier;
    if (Debug.ENABLED) {
      dcheck(identifier.name.startsWith(Constants.TEMP_PREFIX));
    }
  }

  boolean isCell() {
    return (flags & FLAG_CELL) != 0;
  }

  void setCell(CTempVar cv) {
    check(cv.isCell, () -> new ValidationError("not a cell: " + cv));
    check(this.cv == null, () -> new ValidationError("already resolved: " + this));
    check((flags & FLAG_CELL) == 0, () -> new ValidationError("already a cell: " + this));
    this.flags |= FLAG_CELL;
    this.cv = cv;
  }

  void setUnused() {
    check(cv == null, () -> new ValidationError("already resolved: " + this));
    this.flags |= FLAG_UNUSED;
  }

  boolean isUnused() {
    return (flags & FLAG_UNUSED) != 0;
  }

  String name() {
    return identifier.name;
  }

  @Override
  public Token token() {
    return identifier.token();
  }

  @Override
  public String toString() {
    return "TEMP(" + identifier + ")";
  }
}
