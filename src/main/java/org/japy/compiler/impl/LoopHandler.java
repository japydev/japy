package org.japy.compiler.impl;

import org.japy.parser.python.ast.loc.ITokenSpan;

public interface LoopHandler extends ControlHandler {
  void visitBreak(ITokenSpan location);

  void visitContinue(ITokenSpan location);
}
