package org.japy.compiler.impl;

import java.util.ArrayList;
import java.util.List;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.CompilerConfig;
import org.japy.compiler.code.CBlock;
import org.japy.compiler.code.CModule;
import org.japy.compiler.code.CPackage;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.parser.python.ParserInput;
import org.japy.parser.python.ast.node.AstModule;

public final class PackageBuilder {
  private final CompilerConfig config;
  private final Compiler compiler;
  private final ConstPool constPool = new ConstPool();

  private final AstModule astModule;
  private final ModuleBlock module;

  private PackageBuilder(
      CompilerConfig config, AstModule astModule, ParserInput input, @Nullable PyContext ctx) {
    this.config = config;
    this.astModule = astModule;
    this.module = new ModuleBlock(astModule);
    this.compiler = new Compiler(module, config, constPool, input, ctx);
  }

  public static CPackage build(
      CompilerConfig config, AstModule module, ParserInput parserInput, @Nullable PyContext ctx) {
    PackageBuilder b = new PackageBuilder(config, module, parserInput, ctx);
    return b.build();
  }

  private CPackage build() {
    compiler.compileModule(astModule, module);

    List<CBlock> children = new ArrayList<>();
    CModule mod = new ModuleWriter(module, config, children).build();
    CPackage pkg = new CPackage(mod, children.toArray(CBlock[]::new), constPool.toArray());

    if (Debug.ENABLED) {
      pkg.validate();
    }

    return pkg;
  }
}
