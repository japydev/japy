package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CInstr0.Code.IGNOP;
import static org.japy.compiler.code.instr.CInstr0.Code.RET;
import static org.japy.compiler.code.instr.CInstrR.Code.STORE;
import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.infra.validation.Validator.check;
import static org.japy.infra.validation.Validator.checkNotNull;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.code.CVarType;
import org.japy.compiler.code.instr.CInstr;
import org.japy.compiler.code.instr.CLabel;
import org.japy.compiler.impl.instr.CTInstr;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.validation.Debug;
import org.japy.kernel.types.IPyObj;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.ITokenSpan;

final class Context implements InstrSink {
  private static class BlockContext {
    final @Nullable BlockContext prev;
    final Block block;
    ControlHandler controlHandler;

    BlockContext(@Nullable BlockContext prev, Block block) {
      this.prev = prev;
      this.block = block;
      controlHandler = new RootControlHandler();
    }

    private static class RootControlHandler implements ControlHandler {
      @Override
      public @Nullable ControlHandler parent() {
        return null;
      }
    }
  }

  private BlockContext blockContext;
  public final ICompiler compiler;

  Context(ModuleBlock module, ICompiler compiler) {
    this.blockContext = new BlockContext(null, module);
    this.compiler = compiler;
  }

  Block block() {
    return blockContext.block;
  }

  Scope<?> scope() {
    for (@Nullable Block b = block(); b != null; b = b.parentBlock) {
      if (b instanceof Scope) {
        return (Scope<?>) b;
      }
    }
    throw InternalErrorException.notReached();
  }

  void pushBlock(Block block) {
    if (Debug.ENABLED) {
      dcheck(block.parentBlock == block() || block.parentBlock == scope(), "pushing bad block");
    }
    blockContext = new BlockContext(blockContext, block);
  }

  void popBlock(Block block) {
    check(block == block(), "popped wrong block");
    checkNotNull(blockContext.prev, "popped module");
    blockContext = blockContext.prev;
  }

  void push(ControlHandler handler) {
    check(handler.parent() == blockContext.controlHandler, "inconsistent handler stack");
    blockContext.controlHandler = handler;
  }

  void pop(ControlHandler handler) {
    check(blockContext.controlHandler == handler, "popped wrong handler");
    @Nullable ControlHandler parent = handler.parent();
    checkNotNull(parent, "popped root handler");
    blockContext.controlHandler = parent;
  }

  ControlHandler topHandler() {
    return blockContext.controlHandler;
  }

  <T extends ControlHandler> @Nullable T findHandler(Class<T> klass) {
    return findHandler(blockContext.controlHandler, klass);
  }

  @SuppressWarnings("unchecked")
  <T extends ControlHandler> @Nullable T findHandler(
      @Nullable ControlHandler start, Class<T> klass) {
    for (@Nullable ControlHandler h = start; h != null; h = h.parent()) {
      if (klass.isAssignableFrom(h.getClass())) {
        return (T) h;
      }
    }
    return null;
  }

  <T extends ControlHandler> T getHandler(Class<T> klass) {
    return dcheckNotNull(findHandler(klass));
  }

  @Override
  public void write(CInstr instr) {
    block().write(instr);
  }

  @Override
  public void write(CTInstr instr) {
    block().write(instr);
  }

  CLabel newLabel() {
    return block().newLabel();
  }

  CLabel mark(IHasLocation location) {
    CLabel label = newLabel();
    write(label, location);
    return label;
  }

  TempVar pushTemp(String name, CVarType type, ITokenSpan location) {
    return pushTemp(name, type, false, location);
  }

  TempVar pushTemp(String name, CVarType type, boolean init, ITokenSpan location) {
    return block().pushTemp(name, type, init, location);
  }

  void popTemp(TempVar var, boolean erase, IHasLocation location) {
    block().popTemp(var, erase, location);
  }

  void popTemp(TempVar var, IHasLocation location) {
    popTemp(var, true, location);
  }

  Ref referencedName(AstIdentifier identifier) {
    return block().referencedName(identifier);
  }

  void boundName(AstIdentifier identifier) {
    block().boundName(identifier);
  }

  void nonlocalDecl(AstIdentifier identifier) {
    block().nonlocalDecl(identifier);
  }

  void globalDecl(AstIdentifier identifier) {
    block().globalDecl(identifier);
  }

  private List<CloseableHandler> getCloseableHandlers(@Nullable ControlHandler limit) {
    List<CloseableHandler> list = new ArrayList<>();
    for (@Nullable ControlHandler h = topHandler(); h != null && h != limit; h = h.parent()) {
      if (h instanceof CloseableHandler) {
        list.add((CloseableHandler) h);
      }
    }
    return Lists.reverse(list);
  }

  void ret(ITokenSpan location) {
    List<CloseableHandler> handlers = getCloseableHandlers(null);
    if (handlers.isEmpty()) {
      // to patch to jump to return label if this function is async or a generator
      write(IGNOP, location);
      write(RET, location);
      return;
    }

    TempVar retVal = scope().retVal();
    write(STORE, retVal, location);

    @Var CLabel target = scope().retLabel();
    for (CloseableHandler h : handlers) {
      target = h.close(target, location);
    }

    jump(target, location);
  }

  void goTo(@Var CLabel target, ControlHandler inHandler, ITokenSpan location) {
    List<CloseableHandler> handlers = getCloseableHandlers(inHandler);
    if (handlers.isEmpty()) {
      jump(target, location);
      return;
    }

    for (CloseableHandler h : handlers) {
      target = h.close(target, location);
    }

    jump(target, location);
  }

  void addExcHandler(CLabel handler, CLabel start, CLabel end) {
    block().addExcHandler(handler, start, end);
  }

  void beginExcHandler(CLabel start, CLabel end, IHasLocation location) {
    CLabel handler = newLabel();
    addExcHandler(handler, start, end);
    write(handler, location);
  }

  public void write(IPyObj value, IHasLocation location) {
    compiler.writeConstant(value, location);
  }
}
