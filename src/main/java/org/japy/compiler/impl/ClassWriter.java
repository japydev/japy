package org.japy.compiler.impl;

import static org.japy.infra.validation.Debug.dcheck;

import org.japy.compiler.code.CClass;

final class ClassWriter extends ScopeWriter<ClassBlock, CClass, CClass.Builder> {
  private final String qualName;

  ClassWriter(ClassBlock classBlock, ScopeWriter<?, ?, ?> parent) {
    super(classBlock, classBlock.klass.name.name, parent, parent.config, CClass.builder());
    dcheck(parent.scope == classBlock.parentBlock);

    String name = classBlock.klass.name.name;
    qualName = makeQualName(parent.qualName(), name);
    blockBuilder.name(name).qualName(qualName);
  }

  @Override
  protected String qualName() {
    return qualName;
  }
}
