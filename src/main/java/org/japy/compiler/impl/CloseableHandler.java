package org.japy.compiler.impl;

import org.japy.compiler.code.instr.CLabel;
import org.japy.parser.python.ast.loc.ITokenSpan;

interface CloseableHandler extends ControlHandler {
  /**
   * Write code necessary to clean up and jump to the target label.
   *
   * <p>If it writes any code, it must make sure it restores the state afterwards, e.g.
   *
   * <pre>{@code
   * CLabel goBack = ctx.newLabel();
   * ctx.jump(goBack);
   * ... code ...
   * ctx.write(goBack);
   * }</pre>
   *
   * @return label to which the caller should jump in order to ultimately get to the target.
   */
  CLabel close(CLabel target, ITokenSpan location);
}
