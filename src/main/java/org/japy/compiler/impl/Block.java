package org.japy.compiler.impl;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.code.CVarType;
import org.japy.compiler.code.instr.CLabel;
import org.japy.infra.loc.IHasLocation;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.loc.IHasTokenSpan;
import org.japy.parser.python.ast.loc.ITokenSpan;

abstract class Block implements InstrSink, IHasTokenSpan {
  final @Nullable Block parentBlock;
  final ITokenSpan location;

  protected Block(ITokenSpan location) {
    this.parentBlock = null;
    this.location = location;
  }

  protected Block(Block parentBlock, ITokenSpan location) {
    this.parentBlock = parentBlock;
    this.location = location;
  }

  String sourceFile() {
    return dcheckNotNull(parentBlock).sourceFile();
  }

  @Override
  public ITokenSpan tokenSpan() {
    return location;
  }

  abstract String newTempName(String base);

  abstract TempVar pushTemp(String name, CVarType type, boolean init, ITokenSpan location);

  abstract void popTemp(TempVar var, boolean erase, IHasLocation location);

  abstract Ref referencedName(AstIdentifier identifier);

  abstract void boundName(AstIdentifier identifier);

  abstract void walrusName(AstIdentifier identifier);

  abstract void nonlocalDecl(AstIdentifier identifier);

  abstract void globalDecl(AstIdentifier identifier);

  abstract void addExcHandler(CLabel handler, CLabel start, CLabel end);

  abstract CLabel newLabel();
}
