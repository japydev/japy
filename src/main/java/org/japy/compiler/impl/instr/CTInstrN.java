package org.japy.compiler.impl.instr;

import static org.japy.compiler.code.instr.CInstrR.INVALID_ARG;
import static org.japy.compiler.code.instr.COpInfo.*;
import static org.japy.infra.validation.Debug.dcheck;

import org.japy.compiler.code.instr.CInstrR;
import org.japy.compiler.code.instr.COpInfo;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.validation.Debug;

public final class CTInstrN extends CTInstr {
  public enum Code {
    DELNAME(NO_OP, CInstrR.Code.DEL),
    DELNAMEIFBOUND(NO_OP, CInstrR.Code.DELIFBOUND),
    STORENAME(opi(OPS_OBJ, NO_OPS), CInstrR.Code.STORE),
    LOADNAME(opi(NO_OPS, OPS_OBJ), CInstrR.Code.LOAD),
    LOADNAMEBOOL(opi(NO_OPS, OPS_BOOL), CInstrR.Code.LOADBOOL),
    LOADNAMESTRB(opi(NO_OPS, OPS_STRB), CInstrR.Code.LOADSTRB),
    LOADNAMEEXC(opi(NO_OPS, OPS_EXC), CInstrR.Code.LOADEXC),
    AUGASSIGNNAME(opi(OPS_OBJ, NO_OPS), CInstrR.Code.AUGASSIGN, true),
    ANNOTATENAME(opi(OPS_OBJ, NO_OPS), CInstrR.Code.ANNOTATE),
    ;

    public final COpInfo opInfo;
    public final CInstrR.Code rCode;
    public final boolean needArg;

    Code(COpInfo opInfo, CInstrR.Code rCode) {
      this(opInfo, rCode, false);
    }

    Code(COpInfo opInfo, CInstrR.Code rCode, boolean needArg) {
      this.opInfo = opInfo;
      this.rCode = rCode;
      this.needArg = needArg;
    }
  }

  public final Code code;
  private String name;
  public final int arg;

  public CTInstrN(Code code, String name, IHasLocation location) {
    this(code, name, INVALID_ARG, location);
  }

  public CTInstrN(Code code, String name, int arg, IHasLocation location) {
    super(location);
    this.name = name;
    this.code = code;
    this.arg = arg;
    if (Debug.ENABLED) {
      dcheck(!name.startsWith("@") && !name.startsWith("#"));
      dcheck(code.needArg == (arg != INVALID_ARG));
    }
  }

  public String name() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public Kind kind() {
    return Kind.TINSTRN;
  }

  @Override
  public COpInfo opInfo() {
    return code.opInfo;
  }

  @Override
  public String toString() {
    return code + "(" + name + ")";
  }
}
