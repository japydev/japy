package org.japy.compiler.impl.instr;

import static org.japy.compiler.code.instr.COpInfo.*;

import org.japy.compiler.code.instr.COpInfo;
import org.japy.compiler.impl.Ref;
import org.japy.infra.loc.IHasLocation;

public final class CTInstrTR extends CTInstr {
  public enum Code {
    TBEGINTEMP,
    TENDTEMP,
    ;

    public final COpInfo opInfo = NO_OP;
  }

  public final Code code;
  public final Ref ref;

  public CTInstrTR(Code code, Ref ref, IHasLocation location) {
    super(location);
    this.code = code;
    this.ref = ref;
  }

  @Override
  public Kind kind() {
    return Kind.TINSTRTR;
  }

  @Override
  public COpInfo opInfo() {
    return code.opInfo;
  }

  @Override
  public String toString() {
    return code + "(" + ref.name + ")";
  }
}
