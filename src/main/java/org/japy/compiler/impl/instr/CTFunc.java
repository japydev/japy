package org.japy.compiler.impl.instr;

import static org.japy.compiler.code.instr.COpInfo.*;

import org.japy.compiler.code.instr.COpInfo;
import org.japy.compiler.impl.FuncBlock;

public final class CTFunc extends CTInstr {
  private static final COpInfo OP_INFO = opi(OPS_OBJ2, OPS_OBJ);

  public final FuncBlock block;

  public CTFunc(FuncBlock block) {
    super(block);
    this.block = block;
  }

  @Override
  public COpInfo opInfo() {
    return OP_INFO;
  }

  @Override
  public Kind kind() {
    return Kind.TFUNC;
  }
}
