package org.japy.compiler.impl.instr;

import org.japy.compiler.code.instr.ICInstr;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.IHasWritableLocation;
import org.japy.infra.loc.Location;

public abstract class CTInstr implements ICInstr, IHasWritableLocation {
  public enum Kind {
    TINSTRR,
    TINSTRTR,
    TINSTRN,
    TCLASS,
    TFUNC,
  }

  private Location location;

  protected CTInstr(IHasLocation location) {
    this.location = location.location();
  }

  public abstract Kind kind();

  @Override
  public void setLocation(Location location) {
    this.location = location;
  }

  @Override
  public Location location() {
    return location;
  }
}
