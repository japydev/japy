package org.japy.compiler.impl.instr;

import static org.japy.compiler.code.instr.COpInfo.*;

import org.japy.compiler.code.instr.COpInfo;
import org.japy.compiler.impl.ClassBlock;

public final class CTClass extends CTInstr {
  private static final COpInfo OP_INFO = opi(OPS_ARGS, OPS_OBJ);

  public final ClassBlock block;

  public CTClass(ClassBlock block) {
    super(block);
    this.block = block;
  }

  @Override
  public COpInfo opInfo() {
    return OP_INFO;
  }

  @Override
  public Kind kind() {
    return Kind.TCLASS;
  }
}
