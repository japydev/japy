package org.japy.compiler.impl.instr;

import org.japy.compiler.code.instr.CInstrR;
import org.japy.compiler.code.instr.COpInfo;
import org.japy.compiler.impl.Ref;
import org.japy.infra.loc.IHasLocation;

public final class CTInstrR extends CTInstr {
  public final CInstrR.Code code;
  public final Ref ref;
  public final int arg;

  public CTInstrR(CInstrR.Code code, Ref ref, IHasLocation location) {
    this(code, ref, CInstrR.INVALID_ARG, location);
  }

  public CTInstrR(CInstrR.Code code, Ref ref, int arg, IHasLocation location) {
    super(location);
    this.code = code;
    this.ref = ref;
    this.arg = arg;
  }

  @Override
  public String toString() {
    return code + "(" + ref.name + ")";
  }

  @Override
  public COpInfo opInfo() {
    return code.opInfo;
  }

  @Override
  public Kind kind() {
    return Kind.TINSTRR;
  }
}
