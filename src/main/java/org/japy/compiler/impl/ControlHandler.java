package org.japy.compiler.impl;

import org.checkerframework.checker.nullness.qual.Nullable;

interface ControlHandler {
  @Nullable
  ControlHandler parent();
}
