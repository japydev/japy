package org.japy.compiler.impl;

import static org.japy.compiler.code.instr.CInstr0.Code.*;
import static org.japy.compiler.code.instr.CInstrI.Code.*;
import static org.japy.compiler.code.instr.CInstrR.Code.*;
import static org.japy.compiler.code.instr.CJump.Code.JUMP_ALWAYS;
import static org.japy.compiler.impl.instr.CTInstrN.Code.*;
import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyNone.None;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.SyntaxError;
import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CVarType;
import org.japy.compiler.code.instr.CInstr0;
import org.japy.compiler.code.instr.CJump;
import org.japy.compiler.code.instr.CLabel;
import org.japy.compiler.code.instr.ICInstr;
import org.japy.compiler.impl.instr.CTInstrR;
import org.japy.infra.validation.Debug;
import org.japy.parser.python.ast.expr.AstGenerator;
import org.japy.parser.python.ast.expr.AstIdentifier;
import org.japy.parser.python.ast.expr.AstLambda;
import org.japy.parser.python.ast.expr.IAstExpr;
import org.japy.parser.python.ast.loc.ITokenSpan;
import org.japy.parser.python.ast.loc.Token;
import org.japy.parser.python.ast.node.AstNode;
import org.japy.parser.python.ast.node.AstParam;
import org.japy.parser.python.ast.stmt.AstFunc;

public final class FuncBlock extends Scope<FuncBlock> {
  public final @Nullable AstIdentifier name;
  public final AstParam[] params;
  public final @Nullable IAstExpr retType;
  private boolean needClassCell;
  private int docString = -1;

  private final TempVar retVal;
  private final CLabel retLabel;
  private final int initRetVal;
  private boolean retValUsed;

  private FuncBlock(
      Context ctx,
      int flags,
      @Nullable AstIdentifier name,
      AstParam[] params,
      @Nullable IAstExpr retType,
      ITokenSpan location) {
    super(ctx.scope(), location);

    this.flags |= flags;
    this.name = name;
    this.params = params;
    this.retType = retType;

    ctx.pushBlock(this);

    for (AstParam p : params) {
      param(p.name);
    }

    dcheck((flags & CFunc.FLAG_GENERATOR_EXPR) == 0 || ((flags & CFunc.FLAG_GENERATOR) != 0));

    if ((flags & (CFunc.FLAG_ASYNC | CFunc.FLAG_GENERATOR)) == 0) {
      writeParseArgs();
    }

    retVal = ctx.pushTemp("ret", CVarType.PYOBJ, location);
    ctx.write(INITTEMP, retVal, location);
    initRetVal = instructions().size() - 1;
    retLabel = ctx.newLabel();
  }

  FuncBlock(Context ctx, AstLambda expr) {
    this(ctx, expr.value.canYield() ? CFunc.FLAG_GENERATOR : 0, null, expr.params, null, expr);
    if (Debug.ENABLED) {
      dcheck(!expr.value.canSuspend() || expr.value.canYield());
      dcheck(!expr.value.canAwait());
    }
  }

  FuncBlock(Context ctx, AstGenerator expr) {
    this(
        ctx,
        (AstNode.canAwait(expr.expr, expr.compFor) ? CFunc.FLAG_ASYNC : 0)
            | CFunc.FLAG_GENERATOR
            | CFunc.FLAG_GENERATOR_EXPR,
        null,
        AstParam.EMPTY_ARRAY,
        null,
        expr);
  }

  FuncBlock(Context ctx, AstFunc func) {
    this(
        ctx,
        (func.header.async ? CFunc.FLAG_ASYNC : 0)
            | (func.body.canYield() ? CFunc.FLAG_GENERATOR : 0),
        func.header.name,
        func.header.params,
        func.header.retType,
        func);
  }

  boolean isAsync() {
    return (flags & CFunc.FLAG_ASYNC) != 0;
  }

  boolean isGenerator() {
    return (flags & CFunc.FLAG_GENERATOR) != 0;
  }

  boolean isGeneratorExpr() {
    return (flags & CFunc.FLAG_GENERATOR_EXPR) != 0;
  }

  static void compile(Context ctx, AstFunc func) {
    FuncBlock f = new FuncBlock(ctx, func);
    ctx.compiler.compileBlockBody(func, f);
    f.end(ctx, func);
  }

  private void param(AstIdentifier identifier) {
    if (Debug.ENABLED) {
      dcheck(!names.containsKey(identifier.name));
      dcheck(!identifier.name.startsWith(Constants.TEMP_PREFIX));
    }
    LocalVar var = new LocalVar(this, identifier);
    locals.add(var);
    names.put(identifier.name, Ref.local(var));
  }

  private void writeParseArgs() {
    if (params.length == 0) {
      write(PARSEARGS0, this);
      return;
    }

    if (params.length == 1) {
      write(PARSEARGS1, this);
      write(STORENAME, params[0].name, params[0]);
      return;
    }

    write(PARSEARGS, this);
    for (int i = 0; i != params.length; ++i) {
      if (i != params.length - 1) {
        write(DUP, params[i]);
      }
      write(AALOAD, i, params[i]);
      write(STORENAME, params[i].name, params[i]);
    }
  }

  boolean needClassCell() {
    return needClassCell;
  }

  void setNeedClassCell() {
    dcheck(writer == null);
    needClassCell = true;
  }

  @Override
  FuncWriter writer(ScopeWriter<?, ?, ?> parent) {
    return (FuncWriter) super.writer(parent);
  }

  @Override
  FuncWriter makeWriter(ScopeWriter<?, ?, ?> parent) {
    return new FuncWriter(this, parent);
  }

  @Override
  TempVar retVal() {
    retValUsed = true;
    return retVal;
  }

  int docString() {
    return docString;
  }

  @Override
  void setDocString(int docString, Token location) {
    this.docString = docString;
  }

  @Override
  CLabel retLabel() {
    retValUsed = true;
    return retLabel;
  }

  @Override
  public void walrusName(AstIdentifier identifier) {
    // In generator expressions assignment expression's scope is the parent one, not the
    // generator's scope.
    if (!isGeneratorExpr()) {
      boundName(identifier);
      return;
    }

    // Assignments to the iteration variables are not allowed
    @Nullable Ref ref = names().get(identifier.name);
    if (ref != null) {
      if (ref.kind() == Ref.Kind.LOCAL) {
        throw new SyntaxError("invalid assignment expression", identifier);
      }
      return;
    }

    dcheckNotNull(parentBlock);
    parentBlock.walrusName(identifier);
  }

  private void patchReturns() {
    for (int i = 0, c = instructions.size(); i != c; ++i) {
      ICInstr instr = instructions.get(i);
      if (instr instanceof CInstr0 && ((CInstr0) instr).code == RET) {
        if (Debug.ENABLED) {
          ICInstr prev = instructions.get(i - 1);
          dcheck(prev instanceof CInstr0 && ((CInstr0) prev).code == IGNOP);
        }

        TempVar r = retVal();
        instructions.set(i - 1, new CTInstrR(STORE, Ref.temp(r), instr));
        instructions.set(i, new CJump(JUMP_ALWAYS, retLabel(), instr));
      }
    }
  }

  void end(Context ctx, ITokenSpan location) {
    if (isGenerator() || isAsync()) {
      patchReturns();
    }

    if (retValUsed) {
      ctx.write(retLabel, location);
      ctx.write(LOAD, retVal, location);
      if (Debug.ENABLED) {
        dcheck(ctx.findHandler(CloseableHandler.class) == null);
      }
      ctx.write(RET, location);
    } else {
      nopTemp(initRetVal, retVal);
      ctx.write(None, location);
      ctx.write(RET, location);
    }

    ctx.popTemp(retVal, false, location);

    ctx.popBlock(this);

    ctx.compiler.defineFunction(this);
  }
}
