package org.japy.compiler;

import java.nio.file.Path;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.code.CPackage;
import org.japy.compiler.impl.PackageBuilder;
import org.japy.kernel.exec.PyContext;
import org.japy.parser.python.Parser;
import org.japy.parser.python.ScannerInput;

public final class PackageCompiler {
  private PackageCompiler() {}

  public static CPackage compile(Path path, CompilerConfig config, @Nullable PyContext ctx) {
    return Parser.parse(
        path, (module, parserInput) -> PackageBuilder.build(config, module, parserInput, ctx));
  }

  public static CPackage compile(
      ScannerInput input, CompilerConfig config, @Nullable PyContext ctx) {
    return Parser.parse(
        input, (module, parserInput) -> PackageBuilder.build(config, module, parserInput, ctx));
  }

  public static CPackage compile(
      String text, String sourceFile, CompilerConfig config, @Nullable PyContext ctx) {
    return Parser.parse(
        text,
        sourceFile,
        (module, parserInput) -> PackageBuilder.build(config, module, parserInput, ctx));
  }

  public static CPackage compile(
      byte[] text, String sourceFile, CompilerConfig config, @Nullable PyContext ctx) {
    return Parser.parse(
        text,
        sourceFile,
        (module, parserInput) -> PackageBuilder.build(config, module, parserInput, ctx));
  }
}
