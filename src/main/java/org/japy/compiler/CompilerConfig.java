package org.japy.compiler;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;

import org.japy.kernel.exec.OptimizationLevel;

public final class CompilerConfig {
  public static final int ENABLE_ASSERT = 1;
  public static final int ENABLE_DOCSTRINGS = 2;
  // from __future__ import annotations
  public static final int ANNOTATIONS_AS_STRINGS = 4;

  public final int flags;

  private CompilerConfig(Builder builder) {
    flags = builder.flags;
  }

  public boolean enableAssert() {
    return (flags & ENABLE_ASSERT) != 0;
  }

  public boolean enableDocStrings() {
    return (flags & ENABLE_DOCSTRINGS) != 0;
  }

  public boolean annotationsAsStrings() {
    return (flags & ANNOTATIONS_AS_STRINGS) != 0;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static Builder builder(CompilerConfig config) {
    return new Builder(config);
  }

  @CanIgnoreReturnValue
  public static class Builder {
    private int flags;

    private Builder() {
      flags = ENABLE_ASSERT | ENABLE_DOCSTRINGS;
    }

    private Builder(CompilerConfig config) {
      flags = config.flags;
    }

    public Builder optimizationLevel(OptimizationLevel optLevel) {
      switch (optLevel) {
        case ZERO:
          flags |= ENABLE_ASSERT | ENABLE_DOCSTRINGS;
          break;
        case ONE:
          flags &= ~ENABLE_ASSERT;
          flags |= ENABLE_DOCSTRINGS;
          break;
        case TWO:
          flags &= (ENABLE_ASSERT | ENABLE_DOCSTRINGS);
          break;
      }
      return this;
    }

    @CheckReturnValue
    public CompilerConfig build() {
      return new CompilerConfig(this);
    }
  }
}
