package org.japy.compiler.code;

import org.japy.kernel.types.IPyObj;

public class CPackage {
  public final CModule module;
  public final CBlock[] children;
  public final IPyObj[] constants;

  public CPackage(CModule module, CBlock[] children, IPyObj[] constants) {
    this.module = module;
    this.children = children;
    this.constants = constants;
  }

  public void validate() {
    new CValidator(this).validate();
  }
}
