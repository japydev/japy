package org.japy.compiler.code;

import static org.japy.infra.validation.Debug.dcheck;

public final class CLocalVar {
  public final String name;
  public final boolean isCell;
  public final int index;

  public CLocalVar(String name, boolean isCell, int index) {
    this.name = name;
    this.isCell = isCell;
    this.index = index;
    dcheck(index >= 0);
  }

  @Override
  public String toString() {
    String kind = isCell ? "CELL" : "LOCAL";
    return kind + "(" + name + ", " + index + ")";
  }
}
