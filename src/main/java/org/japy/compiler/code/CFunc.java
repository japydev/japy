package org.japy.compiler.code;

import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.infra.validation.Validator.check;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.validation.Validator;
import org.japy.kernel.util.Signature;

public final class CFunc extends CBlock {
  /**
   * It is a generator, i.e. a function which can `yield`. It returns a generator object when
   * called.
   */
  public static final int FLAG_GENERATOR = NEXT_FLAG;

  /** It is a generator function created for a generator expression {@code (x for x in foo)}. */
  public static final int FLAG_GENERATOR_EXPR = NEXT_FLAG << 1;

  /** It is a coroutine function. */
  public static final int FLAG_ASYNC = NEXT_FLAG << 2;

  /** This method has the __class__ cell which contains the parent class instance. */
  public static final int FLAG_HAS_CLASS_CELL = NEXT_FLAG << 3;

  /** This method calls global locals() directly by name. */
  public static final int FLAG_USES_LOCALS = NEXT_FLAG << 4;

  public final String name;
  /** Qualified name without the module name */
  public final String qualName;

  public final Signature signature;
  public final CLocalVar[] cells;
  public final int docString;

  private CFunc(Builder builder) {
    super(builder);
    name = dcheckNotNull(builder.name);
    qualName = dcheckNotNull(builder.qualName);
    cells = builder.cells.toArray(CLocalVar[]::new);
    signature = dcheckNotNull(builder.signature);
    docString = builder.docString;
  }

  public boolean isAsync() {
    return (flags & FLAG_ASYNC) != 0;
  }

  public boolean isGenerator() {
    return (flags & FLAG_GENERATOR) != 0;
  }

  public boolean isGeneratorExpr() {
    return (flags & FLAG_GENERATOR_EXPR) != 0;
  }

  public boolean usesLocals() {
    return (flags & FLAG_USES_LOCALS) != 0;
  }

  public static Builder builder() {
    return new Builder();
  }

  public CLocalVar getLocal(String name, IHasLocation location) {
    return Arrays.stream(locals)
        .filter(v -> v.name.equals(name))
        .findFirst()
        .orElseGet(
            () ->
                Arrays.stream(cells)
                    .filter(v -> v.name.equals(name))
                    .findFirst()
                    .orElseThrow(
                        () ->
                            new InternalErrorException(
                                "unknown local \"" + name + "\"", location)));
  }

  public boolean isParam(CLocalVar var) {
    return Arrays.stream(signature.paramNames).anyMatch(n -> n.equals(var.name));
  }

  @CanIgnoreReturnValue
  public static class Builder extends CBlock.Builder<CFunc, Builder> {
    private final List<CLocalVar> cells = new ArrayList<>();
    private @Nullable String name;
    private @Nullable String qualName;
    private @Nullable Signature signature;
    private int docString = -1;

    private Builder() {}

    @Override
    @CheckReturnValue
    public CFunc build() {
      return new CFunc(this);
    }

    public Builder name(String name) {
      this.name = name;
      return this;
    }

    public Builder qualName(String qualName) {
      this.qualName = qualName;
      return this;
    }

    public Builder docString(int index) {
      this.docString = index;
      return this;
    }

    public Builder signature(Signature signature) {
      this.signature = signature;
      return this;
    }

    @CheckReturnValue
    public CLocalVar newCell(String name) {
      CLocalVar v = new CLocalVar(name, true, cells.size());
      cells.add(v);
      return v;
    }
  }

  @Override
  public void validate(Validator v) {
    super.validate(v);

    boolean hasClassCell = (flags & FLAG_HAS_CLASS_CELL) != 0;

    if (hasClassCell) {
      check(upvalues.length != 0, "missing __class__ upvalue");
      check(upvalues[0].kind == CUpvalue.Kind.CLASS, "missing __class__ upvalue");
    }

    for (int i = hasClassCell ? 1 : 0; i < upvalues.length; ++i) {
      check(upvalues[i].kind != CUpvalue.Kind.CLASS, "unexpected __class__ upvalue");
    }
  }
}
