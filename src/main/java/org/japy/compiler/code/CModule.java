package org.japy.compiler.code;

import com.google.errorprone.annotations.CanIgnoreReturnValue;

public final class CModule extends CBlock {
  private CModule(Builder builder) {
    super(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @CanIgnoreReturnValue
  public static class Builder extends CBlock.Builder<CModule, Builder> {
    @Override
    public CModule build() {
      return new CModule(this);
    }
  }
}
