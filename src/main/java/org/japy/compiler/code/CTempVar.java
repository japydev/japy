package org.japy.compiler.code;

import static org.japy.infra.validation.Debug.dcheck;

public final class CTempVar {
  public final String name;
  public final CVarType type;
  public final boolean isCell;
  public final int index;

  public CTempVar(String name, CVarType type, boolean isCell, int index) {
    this.name = name;
    this.type = type;
    this.isCell = isCell;
    this.index = index;
    dcheck(index >= 0);
  }

  @Override
  public String toString() {
    String kind = isCell ? "TEMPCELL" : "TEMP";
    return kind + "(" + name + ", " + type + ", " + index + ")";
  }
}
