package org.japy.compiler.code;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;

public final class CClass extends CBlock {
  public final String name;
  /** Qualified name without the module name */
  public final String qualName;

  private CClass(Builder builder) {
    super(builder);
    this.name = dcheckNotNull(builder.name);
    this.qualName = dcheckNotNull(builder.qualName);
  }

  public static Builder builder() {
    return new Builder();
  }

  @CanIgnoreReturnValue
  public static class Builder extends CBlock.Builder<CClass, Builder> {
    private @Nullable String name;
    private @Nullable String qualName;

    private Builder() {}

    public Builder name(String name) {
      this.name = name;
      return this;
    }

    public Builder qualName(String qualName) {
      this.qualName = qualName;
      return this;
    }

    @Override
    @CheckReturnValue
    public CClass build() {
      return new CClass(this);
    }
  }
}
