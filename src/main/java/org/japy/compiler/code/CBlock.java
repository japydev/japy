package org.japy.compiler.code;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.util.ArrayList;
import java.util.List;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.CheckReturnValue;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.code.instr.CInstr;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.Location;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.ISupportsValidation;
import org.japy.infra.validation.Validator;

public abstract class CBlock implements ISupportsValidation, IHasLocation {
  public static final int FLAG_HAS_ANNOTATIONS = 1;

  protected static final int NEXT_FLAG = 2;

  /**
   * Unique name in the code package, like Klass$method or __main__. This is the generated Java
   * class's name (minus possible codegen suffix).
   */
  public final String binName;

  public final String sourceFile;
  public final @Nullable String debugPath;
  public final int sourceLine;
  public final int flags;
  public final CInstr[] instructions;
  public final CExcHandler[] excHandlers;
  public final CLocalVar[] locals;
  public final CUpvalue[] upvalues;
  public final int tempCount;
  public final Location location;

  protected CBlock(Builder<?, ?> builder) {
    binName = dcheckNotNull(builder.binName);
    flags = builder.flags;
    instructions = builder.instructions.toArray(CInstr[]::new);
    excHandlers = dcheckNotNull(builder.excHandlers);
    locals = builder.locals.toArray(CLocalVar[]::new);
    upvalues = builder.upvalues.toArray(CUpvalue[]::new);
    tempCount = builder.tempCount;
    location = dcheckNotNull(builder.location);
    sourceFile = dcheckNotNull(builder.sourceFile);
    sourceLine = dcheckNotNull(builder.sourceLine);
    if (Debug.ENABLED) {
      debugPath = CValidator.getChunkPath(sourceFile);
    } else {
      debugPath = null;
    }
  }

  @Override
  public Location location() {
    return location;
  }

  public boolean hasAnnotations() {
    return (flags & FLAG_HAS_ANNOTATIONS) != 0;
  }

  @SuppressWarnings("unchecked")
  @CanIgnoreReturnValue
  public abstract static class Builder<C extends CBlock, B extends Builder<C, B>>
      implements CInstrSink {
    private @Nullable String binName;
    private int flags;
    private final List<CInstr> instructions = new ArrayList<>();
    private CExcHandler @Nullable [] excHandlers;
    private final List<CLocalVar> locals = new ArrayList<>();
    private final List<CUpvalue> upvalues = new ArrayList<>();
    private int tempCount;
    private @Nullable Location location;
    private @Nullable String sourceFile;
    private @Nullable Integer sourceLine;

    protected Builder() {}

    @CheckReturnValue
    public abstract C build();

    public B binName(String binName) {
      this.binName = binName;
      return (B) this;
    }

    public B location(Location location) {
      this.location = location;
      return (B) this;
    }

    public B sourceFile(String sourceFile) {
      this.sourceFile = sourceFile;
      return (B) this;
    }

    public B sourceLine(int sourceLine) {
      this.sourceLine = sourceLine;
      return (B) this;
    }

    public B addFlags(int flags) {
      this.flags |= flags;
      return (B) this;
    }

    public B excHandlers(CExcHandler[] excHandlers) {
      this.excHandlers = excHandlers;
      return (B) this;
    }

    @CheckReturnValue
    public CLocalVar local(String name) {
      CLocalVar v = new CLocalVar(name, false, locals.size());
      locals.add(v);
      return v;
    }

    @CheckReturnValue
    public CTempVar temp(String name, CVarType type, int index) {
      tempCount = Math.max(index + 1, tempCount);
      return new CTempVar(name, type, false, index);
    }

    @CheckReturnValue
    public CTempVar tempCell(String name, CVarType type, int index) {
      tempCount = Math.max(index + 1, tempCount);
      return new CTempVar(name, type, true, index);
    }

    @CheckReturnValue
    public CUpvalue upvalue(CUpvalue.Kind kind, String name, int parentIndex) {
      CUpvalue upvalue = new CUpvalue(kind, parentIndex, upvalues.size(), name);
      upvalues.add(upvalue);
      return upvalue;
    }

    @CheckReturnValue
    public @Nullable CUpvalue findUpvalue(String name) {
      for (CUpvalue u : upvalues) {
        if (u.name.equals(name)) {
          return u;
        }
      }
      return null;
    }

    @Override
    public void write(CInstr instr) {
      instructions.add(instr);
    }
  }

  @Override
  public void validate(Validator v) {
    if (v instanceof CValidator) {
      ((CValidator) v).validateBlock(this);
    }
  }
}
