package org.japy.compiler.code;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.io.IOException;
import java.io.PrintStream;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.code.instr.CCatch;
import org.japy.compiler.code.instr.CInstr;
import org.japy.compiler.code.instr.CInstrI;
import org.japy.compiler.code.instr.CInstrR;
import org.japy.compiler.code.instr.CLabel;
import org.japy.compiler.code.instr.CMakeClass;
import org.japy.compiler.code.instr.CMakeFunc;
import org.japy.compiler.impl.Analyzer;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.validation.Debug;
import org.japy.infra.validation.ValidationError;
import org.japy.infra.validation.Validator;

public final class CValidator extends Validator {
  final CPackage pkg;
  private final Map<String, CRef> names = new HashMap<>();

  CValidator(CPackage pkg) {
    super(false);
    this.pkg = pkg;
  }

  void validate() {
    if (Debug.ENABLED) {
      dumpPackage();
    }
    validate(pkg.module);
    validate(pkg.children);
  }

  void validateBlock(CBlock block) {
    if (block instanceof CModule) {
      check(block == pkg.module, "rogue module block");
      validateModule((CModule) block);
    } else {
      check(Arrays.stream(pkg.children).anyMatch(b -> b == block), "unknown block");
      if (!(block instanceof CClass) && !(block instanceof CFunc)) {
        throw InternalErrorException.notReached();
      }
    }

    checkAllReachable(block);

    for (CLocalVar var : block.locals) {
      check(
          names.put(var.name, new CRef(var)) == null,
          () -> new ValidationError("duplicate variable " + var));
      check(!var.isCell, () -> new ValidationError("cell non-cell " + var));
    }
    if (block instanceof CFunc) {
      for (CLocalVar var : ((CFunc) block).cells) {
        check(
            names.put(var.name, new CRef(var)) == null,
            () -> new ValidationError("duplicate variable " + var));
        check(var.isCell, () -> new ValidationError("non-cell cell " + var));
      }
    }
    for (CUpvalue upvalue : block.upvalues) {
      if (!names.containsKey(upvalue.name)) {
        names.put(upvalue.name, new CRef(upvalue));
      } else {
        // Classes may have upvalues "patched through" from enclosing scope to contained
        // functions, which are not visible at class level.
        if (!(block instanceof CClass)) {
          fail("duplicate variable " + upvalue);
        }
      }
    }

    for (CInstr instr : block.instructions) {
      validate(instr, block);
    }

    names.clear();
  }

  private static void validateModule(CModule module) {
    check(module.locals.length == 0, "locals in a module");
    check(module.upvalues.length == 0, "upvalues in a module");
  }

  private void validate(CInstr instr, CBlock block) {
    switch (instr.kind()) {
      case INSTRI:
        {
          CInstrI ci = (CInstrI) instr;
          if (ci.code == CInstrI.Code.CONST) {
            check(ci.arg >= 0 && ci.arg < pkg.constants.length, "bad constant index");
          }
          break;
        }
      case CATCH:
        {
          CCatch c = (CCatch) instr;
          check(Arrays.stream(block.excHandlers).anyMatch(h -> h == c.excHandler), "bad catch");
          break;
        }
      case SWITCH:
      case JUMP:
      case LABEL:
      case INSTRS:
      case INSTR0:
      case INSTRI2:
      case LITERAL:
        break;
      case INSTRR:
        {
          CInstrR ci = (CInstrR) instr;
          validateReference(ci.ref);
          break;
        }
      case MAKEFUNC:
        {
          CMakeFunc mf = (CMakeFunc) instr;
          check(Arrays.stream(pkg.children).anyMatch(c -> c == mf.func), "unknown function");
          break;
        }
      case MAKECLASS:
        {
          CMakeClass mc = (CMakeClass) instr;
          check(Arrays.stream(pkg.children).anyMatch(c -> c == mc.klass), "unknown class");
          break;
        }
    }
  }

  private void validateReference(CRef ref) {
    switch (ref.kind) {
      case LOCAL:
        {
          CLocalVar v = ref.local();
          @Nullable CRef myRef = names.get(ref.name());
          check(myRef != null, () -> new ValidationError("unknown local " + v));
          check(myRef.kind == CRef.Kind.LOCAL, () -> new ValidationError("non-local local " + v));
          check(v.isCell == myRef.local().isCell, () -> new ValidationError("cell non-cell " + v));
          break;
        }
      case TEMP:
        {
          CTempVar v = ref.temp();
          @Nullable CRef myRef = names.get(ref.name());
          check(myRef == null, () -> new ValidationError("temp non-temp " + v));
          break;
        }
      case UPVALUE:
        {
          CUpvalue u = ref.upvalue();
          @Nullable CRef myRef = names.get(ref.name());
          check(myRef != null, () -> new ValidationError("unknown upvalue " + u));
          check(
              myRef.kind == CRef.Kind.UPVALUE,
              () -> new ValidationError("non-upvalue upvalue " + u));
          break;
        }
      case GLOBAL:
        {
          check(
              !names.containsKey(ref.name()),
              () -> new ValidationError("local global " + ref.name()));
          break;
        }
    }
  }

  private static void checkAllReachable(CBlock block) {
    Analyzer.BlockInfo bi = Analyzer.analyze(Arrays.asList(block.instructions), block.excHandlers);
    check((bi.blockFlags & ~block.flags) == 0, "missing flags");
    for (int i = 0; i != block.instructions.length; ++i) {
      check(
          (bi.instructionFlags[i] & CInstr.FLAG_UNREACHABLE) == 0,
          "new unreachable instruction",
          block.instructions[i]);
    }
  }

  private void dumpPackage() {
    if (Debug.ENABLED) {
      dumpBlock(pkg.module);
      Arrays.stream(pkg.children).forEach(CValidator::dumpBlock);
    }
  }

  private static final AtomicInteger COUNTER = new AtomicInteger();
  private static final Pattern FILE_CHARS = Pattern.compile("[/\\\\.]");
  private static final Pattern BAD_CHARS = Pattern.compile("[^a-zA-Z0-9_.]+");

  public static String getChunkPath(@Var @Nullable String sourceFile) {
    String subdir;
    if (sourceFile == null || !FILE_CHARS.matcher(sourceFile).find()) {
      sourceFile =
          (sourceFile != null ? BAD_CHARS.matcher(sourceFile).replaceAll("") : "input")
              + COUNTER.incrementAndGet();
      subdir = "input/" + sourceFile;
    } else {
      subdir = Paths.get(sourceFile).toString().replace(":", "/");
    }

    Path dir = Paths.get("build/jc/", subdir);
    if (!dir.toFile().exists() && !dir.toFile().mkdirs()) {
      throw new InternalErrorException("failed to create output dir " + dir);
    }
    return dir.toString();
  }

  private static void dumpBlock(CBlock block) {
    Path path = Paths.get(dcheckNotNull(block.debugPath)).resolve(block.binName + ".txt");
    try (PrintStream s = new PrintStream(path.toFile(), StandardCharsets.UTF_8)) {
      writeBlock(block, s);
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    }
  }

  private static void writeBlock(CBlock block, PrintStream out) {
    for (CInstr instr : block.instructions) {
      out.println((instr instanceof CLabel ? "" : " ") + instr);
    }

    if (block.excHandlers.length != 0) {
      out.println();
      for (CExcHandler h : block.excHandlers) {
        out.printf("TRYCATCHBLOCK %s %s %s%n", h.start, h.end, h.handler);
      }
    }
  }
}
