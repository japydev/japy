package org.japy.compiler.code;

import org.japy.compiler.code.instr.CLabel;

public final class CExcHandler {
  public final CLabel handler;
  public final CLabel start;
  public final CLabel end;

  public CExcHandler(CLabel handler, CLabel start, CLabel end) {
    this.handler = handler;
    this.start = start;
    this.end = end;
  }
}
