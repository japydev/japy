package org.japy.compiler.code;

import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.set.PySet;

public enum CVarType {
  /** {@link IPyObj} */
  PYOBJ,

  /** {@link org.japy.kernel.types.coll.iter.IPyForIter} */
  PYFORITER,

  /** {@link org.japy.kernel.types.exc.py.PyBaseException} */
  PYEXC,

  /** {@link org.japy.kernel.types.coll.seq.PyList} */
  PYLIST,

  /** {@link PySet} */
  PYSET,

  /** {@link org.japy.kernel.types.coll.dict.PyDict} */
  PYDICT,

  /** {@link org.japy.kernel.types.coll.str.PyStrBuilder} */
  STRB,

  /** {@link Throwable} */
  EXC,

  /** {@code boolean} */
  BOOL,

  /** {@code int} */
  INT,
}
