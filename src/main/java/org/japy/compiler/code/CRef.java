package org.japy.compiler.code;

import static org.japy.infra.validation.Debug.dcheck;

import org.japy.infra.exc.InternalErrorException;

public final class CRef {
  public enum Kind {
    LOCAL,
    TEMP,
    UPVALUE,
    GLOBAL,
  }

  public final Kind kind;
  private final Object referent;

  private CRef(Kind kind, Object referent) {
    this.kind = kind;
    this.referent = referent;
  }

  public CRef(CLocalVar var) {
    this(Kind.LOCAL, var);
  }

  public CRef(CTempVar var) {
    this(Kind.TEMP, var);
  }

  public CRef(CUpvalue upvalue) {
    this(Kind.UPVALUE, upvalue);
  }

  public static CRef global(String name) {
    return new CRef(Kind.GLOBAL, name);
  }

  public CLocalVar local() {
    dcheck(kind == Kind.LOCAL);
    return (CLocalVar) referent;
  }

  public CTempVar temp() {
    dcheck(kind == Kind.TEMP);
    return (CTempVar) referent;
  }

  public CUpvalue upvalue() {
    dcheck(kind == Kind.UPVALUE);
    return (CUpvalue) referent;
  }

  public CVarType type() {
    switch (kind) {
      case UPVALUE:
      case GLOBAL:
      case LOCAL:
        return CVarType.PYOBJ;
      case TEMP:
        return ((CTempVar) referent).type;
    }

    throw InternalErrorException.notReached();
  }

  public String name() {
    switch (kind) {
      case GLOBAL:
        return (String) referent;
      case LOCAL:
        return ((CLocalVar) referent).name;
      case TEMP:
        return ((CTempVar) referent).name;
      case UPVALUE:
        return ((CUpvalue) referent).name;
    }

    throw InternalErrorException.notReached();
  }

  @Override
  public String toString() {
    switch (kind) {
      case GLOBAL:
        return "GLOBAL(" + referent + ')';
      case LOCAL:
      case TEMP:
      case UPVALUE:
        return referent.toString();
    }

    throw InternalErrorException.notReached();
  }
}
