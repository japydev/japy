package org.japy.compiler.code;

import static org.japy.infra.validation.Debug.dcheck;

public final class CUpvalue {
  public enum Kind {
    /** Upvalue refers to a local cell in the outer block */
    CELL,

    /** Upvalue refers to a temp cell in the outer block */
    TEMP,

    /** Upvalue refers to an upvalue in the outer block */
    UPVALUE,

    /** Magic __class__ cell */
    CLASS,
  }

  public static final int INVALID_INDEX = Integer.MIN_VALUE / 2;

  public final Kind kind;
  public final int parentIndex;
  public final int index;
  public final String name;

  public CUpvalue(Kind kind, int parentIndex, int index, String name) {
    this.kind = kind;
    this.parentIndex = parentIndex;
    this.index = index;
    this.name = name;
    dcheck(index >= 0);
    dcheck(parentIndex >= 0 || parentIndex == INVALID_INDEX);
  }

  @Override
  public String toString() {
    return "CUpvalue{"
        + "kind="
        + kind
        + ", parentIndex="
        + parentIndex
        + ", index="
        + index
        + ", name='"
        + name
        + '\''
        + '}';
  }
}
