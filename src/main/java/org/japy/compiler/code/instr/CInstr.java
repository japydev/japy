package org.japy.compiler.code.instr;

import static org.japy.infra.validation.Debug.dcheck;

import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.Location;
import org.japy.infra.validation.Debug;

public abstract class CInstr implements ICInstr {
  /** This instruction always throws an exception. */
  public static final int FLAG_THROW = 1;

  /** This is a return instruction. */
  public static final int FLAG_RET = 2;

  /** This is an unconditional jump instruction. */
  public static final int FLAG_JUMP = 4;

  /** This is a yield instruction. */
  public static final int FLAG_YIELD = 8;

  /** This is an await instruction. */
  public static final int FLAG_AWAIT = 16;

  /** This instruction can't actually be reached, i.e. it's dead code which may be eliminated. */
  public static final int FLAG_UNREACHABLE = 32;

  public static final int LAST_FLAG = 64;

  public enum Kind {
    LITERAL,
    LABEL,
    INSTR0,
    INSTRI,
    INSTRI2,
    JUMP,
    INSTRS,
    INSTRR,
    SWITCH,
    MAKECLASS,
    MAKEFUNC,
    CATCH,
  }

  protected Location location;

  protected CInstr(IHasLocation location) {
    this.location = location.location();
    if (Debug.ENABLED) {
      dcheck(!this.location.isUnknown() || this instanceof CLabel);
    }
  }

  public abstract Kind kind();

  @Override
  public final Location location() {
    return location;
  }

  static {
    // Analyzer collects flags in a byte array
    //noinspection ConstantConditions
    dcheck(LAST_FLAG <= 128);
  }
}
