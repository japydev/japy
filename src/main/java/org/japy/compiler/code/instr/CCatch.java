package org.japy.compiler.code.instr;

import org.japy.compiler.code.CExcHandler;
import org.japy.infra.loc.IHasLocation;

public class CCatch extends CInstr {
  public final CExcHandler excHandler;

  public CCatch(CExcHandler excHandler, IHasLocation location) {
    super(location);
    this.excHandler = excHandler;
  }

  @Override
  public Kind kind() {
    return Kind.CATCH;
  }

  @Override
  public COpInfo opInfo() {
    return COpInfo.NO_OP;
  }

  @Override
  public String toString() {
    return String.format("CATCH(%s, %s, %s)", excHandler.start, excHandler.end, excHandler.handler);
  }
}
