package org.japy.compiler.code.instr;

import static org.japy.compiler.code.instr.COpInfo.*;

import org.japy.infra.loc.IHasLocation;

public final class CJump extends CInstr {
  public enum Code {
    JUMP_ALWAYS(opi(FLAG_JUMP, NO_OPS, NO_OPS)),
    IFTRUEOBJ(opi(OPS_OBJ, NO_OPS)),
    IFFALSEOBJ(opi(OPS_OBJ, NO_OPS)),
    IFNULLOBJ(opi(OPS_OBJ, NO_OPS)),
    IFTRUEBOOL(opi(OPS_BOOL, NO_OPS)),
    IFFALSEBOOL(opi(OPS_BOOL, NO_OPS)),
    IFNULLEXC(opi(OPS_EXC, NO_OPS)),
    ;

    public final COpInfo opInfo;

    Code(COpInfo opInfo) {
      this.opInfo = opInfo;
    }
  }

  public final CLabel target;
  public final Code code;

  public CJump(Code code, CLabel target, IHasLocation location) {
    super(location);
    this.code = code;
    this.target = target;
  }

  @Override
  public Kind kind() {
    return Kind.JUMP;
  }

  @Override
  public COpInfo opInfo() {
    return code.opInfo;
  }

  @Override
  public String toString() {
    return "JUMP(" + code + ", " + target + ")";
  }
}
