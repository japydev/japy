package org.japy.compiler.code.instr;

import static org.japy.compiler.code.instr.COpInfo.*;

import org.japy.infra.loc.IHasLocation;

public final class CInstrI extends CInstr {
  public enum Code {
    BINOP(opi(OPS_OBJ2, OPS_OBJ)),
    UNARYOP(opi(OPS_OBJ, OPS_OBJ)),
    UNPACKEXACT(opi(OPS_OBJ, OPS_OBJ)),
    UNPACKATLEAST(opi(OPS_OBJ, OPS_OBJ)),
    GETTUPLE(opi(OPS_OBJ, OPS_OBJ)),
    BCOMPOP(opi(OPS_OBJ2, OPS_BOOL)),
    COMPOP(opi(OPS_OBJ2, OPS_OBJ)),
    NEWLIST(opi(NO_OPS, OPS_OBJ)),
    NEWSET(opi(NO_OPS, OPS_OBJ)),
    NEWTUPB(opi(NO_OPS, OPS_TUPB)),
    NEWDICT(opi(NO_OPS, OPS_OBJ)),
    CALL(opi(vops(OP_OBJ), OPS_OBJ)),
    VCALL(opi(vops(OP_OBJ), NO_OPS)),
    MAKELIST(opi(vops(OP_OBJ), OPS_OBJ)),
    MAKESET(opi(vops(OP_OBJ), OPS_OBJ)),
    MAKETUP(opi(vops(OP_OBJ), OPS_OBJ)),
    AUGASSIGNINDEX(opi(OPS_OBJ3, NO_OPS)),
    MAKEARGS(opi(vops(OP_OBJ), OPS_ARGS)),
    AALOAD(opi(OPS_AOBJ, OPS_OBJ)),
    CONST(opi(NO_OPS, OPS_OBJ)),
    NEWARRAY(opi(NO_OPS, OPS_AOBJ)),
    SETARRAY(opi(ops(OP_AOBJ, OP_OBJ), NO_OPS)),
    SPECIAL(opi(NO_OPS, OPS_OBJ)),
    ;

    public final COpInfo opInfo;

    Code(COpInfo opInfo) {
      this.opInfo = opInfo;
    }
  }

  public final Code code;
  public final int arg;

  public CInstrI(Code code, int arg, IHasLocation location) {
    super(location);
    this.arg = arg;
    this.code = code;
  }

  @Override
  public Kind kind() {
    return Kind.INSTRI;
  }

  @Override
  public COpInfo opInfo() {
    return code.opInfo;
  }

  @Override
  public int popCount() {
    switch (code) {
      case CALL:
      case VCALL:
        return arg + 1;
      case MAKESET:
      case MAKETUP:
      case MAKELIST:
      case MAKEARGS:
        return arg;
      default:
        return super.popCount();
    }
  }

  @Override
  public String toString() {
    return code + "(" + arg + ")";
  }
}
