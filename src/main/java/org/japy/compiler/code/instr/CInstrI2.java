package org.japy.compiler.code.instr;

import static org.japy.compiler.code.instr.COpInfo.*;

import org.japy.infra.loc.IHasLocation;

public final class CInstrI2 extends CInstr {
  public enum Code {
    NEWARGSB(opi(NO_OPS, OPS_ARGSB)),
    ;

    public final COpInfo opInfo;

    Code(COpInfo opInfo) {
      this.opInfo = opInfo;
    }
  }

  public final Code code;
  public final int arg1;
  public final int arg2;

  public CInstrI2(Code code, int arg1, int arg2, IHasLocation location) {
    super(location);
    this.arg1 = arg1;
    this.arg2 = arg2;
    this.code = code;
  }

  @Override
  public Kind kind() {
    return Kind.INSTRI2;
  }

  @Override
  public COpInfo opInfo() {
    return code.opInfo;
  }

  @Override
  public String toString() {
    return code + "(" + arg1 + ", " + arg2 + ")";
  }
}
