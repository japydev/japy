package org.japy.compiler.code.instr;

import static org.japy.infra.validation.Debug.dcheck;

import org.japy.infra.loc.Location;
import org.japy.infra.validation.Debug;

public final class CLabel extends CInstr {
  public final int index;
  public final String name;

  public CLabel(int index) {
    this(index, "L" + index);
  }

  public CLabel(int index, String name) {
    super(Location.UNKNOWN);
    this.index = index;
    this.name = name;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  @Override
  public String toString() {
    return name;
  }

  @Override
  public COpInfo opInfo() {
    return COpInfo.NO_OP;
  }

  @Override
  public Kind kind() {
    return Kind.LABEL;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof CLabel)) return false;
    if (Debug.ENABLED) {
      CLabel label = (CLabel) o;
      dcheck(index != label.index);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return index;
  }
}
