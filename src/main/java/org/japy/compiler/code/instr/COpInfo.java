package org.japy.compiler.code.instr;

import static org.japy.infra.validation.Debug.dcheck;

import com.google.errorprone.annotations.Immutable;

import org.japy.kernel.types.IPyObj;

@Immutable
public final class COpInfo {
  @SuppressWarnings("InnerClassFieldHidesOuterClassField")
  public enum OpType {
    /** {@link IPyObj} */
    OP_OBJ,
    /** {@link IPyObj}[] */
    OP_AOBJ,
    /** {@link org.japy.kernel.util.Args} */
    OP_ARGS,
    /** {@link org.japy.kernel.util.Args.Builder} */
    OP_ARGSB,
    /** {@code boolean} */
    OP_BOOL,
    /** {@code int} */
    OP_INT,
    /** {@link Throwable} */
    OP_EXC,
    /** {@link org.japy.kernel.types.coll.seq.PyTuple.Builder} */
    OP_TUPB,
    /** {@link org.japy.kernel.types.coll.str.PyStrBuilder} */
    OP_STRB,
    /** {@link String} */
    OP_STR,
    ;

    @Immutable
    public static class Set {
      @SuppressWarnings("Immutable")
      public final OpType[] types;

      public final boolean isVar;

      private Set(OpType[] types, boolean isVar) {
        dcheck(!isVar || types.length == 1);
        this.types = types;
        this.isVar = isVar;
      }

      public Set(OpType... types) {
        this(types, false);
      }

      public static Set var(OpType type) {
        return new Set(new OpType[] {type}, true);
      }

      public boolean isVar() {
        return isVar;
      }

      public OpType type(int i) {
        dcheck(!isVar);
        return types[i];
      }

      public OpType type() {
        dcheck(isVar);
        return types[0];
      }

      public int size() {
        dcheck(!isVar);
        return types.length;
      }

      public boolean isEmpty() {
        return types.length == 0;
      }
    }
  }

  public final int flags;
  public final OpType.Set pop;
  public final OpType.Set push;

  public COpInfo(OpType.Set pop, OpType.Set push) {
    this(0, pop, push);
  }

  public COpInfo(int flags, OpType.Set pop, OpType.Set push) {
    this.flags = flags;
    this.pop = pop;
    this.push = push;
  }

  public static COpInfo opi(OpType.Set noops) {
    dcheck(noops.isEmpty());
    return new COpInfo(noops, noops);
  }

  public static COpInfo opi(OpType.Set pop, OpType.Set push) {
    return new COpInfo(pop, push);
  }

  public static COpInfo opi(int flags, OpType.Set pop, OpType.Set push) {
    return new COpInfo(flags, pop, push);
  }

  public int popCount() {
    return pop.size();
  }

  public int pushCount() {
    return push.size();
  }

  public static OpType.Set ops(OpType... types) {
    return new OpType.Set(types);
  }

  public static OpType.Set vops(OpType type) {
    return OpType.Set.var(type);
  }

  public static final OpType OP_OBJ = OpType.OP_OBJ;
  public static final OpType OP_AOBJ = OpType.OP_AOBJ;
  public static final OpType OP_ARGS = OpType.OP_ARGS;
  public static final OpType OP_ARGSB = OpType.OP_ARGSB;
  public static final OpType OP_BOOL = OpType.OP_BOOL;
  public static final OpType OP_INT = OpType.OP_INT;
  public static final OpType OP_EXC = OpType.OP_EXC;
  public static final OpType OP_TUPB = OpType.OP_TUPB;
  public static final OpType OP_STRB = OpType.OP_STRB;
  public static final OpType OP_STR = OpType.OP_STR;

  public static final OpType.Set NO_OPS = ops();
  public static final OpType.Set OPS_OBJ = ops(OP_OBJ);
  public static final OpType.Set OPS_OBJ2 = ops(OP_OBJ, OP_OBJ);
  public static final OpType.Set OPS_OBJ3 = ops(OP_OBJ, OP_OBJ, OP_OBJ);
  public static final OpType.Set OPS_STR = ops(OP_STR);
  public static final OpType.Set OPS_ARGS = ops(OP_ARGS);
  public static final OpType.Set OPS_ARGSB = ops(OP_ARGSB);
  public static final OpType.Set OPS_AOBJ = ops(OP_AOBJ);
  public static final OpType.Set OPS_AOBJ2 = ops(OP_AOBJ, OP_AOBJ);
  public static final OpType.Set OPS_BOOL = ops(OP_BOOL);
  public static final OpType.Set OPS_BOOL2 = ops(OP_BOOL, OP_BOOL);
  public static final OpType.Set OPS_TUPB = ops(OP_TUPB);
  public static final OpType.Set OPS_INT = ops(OP_INT);
  public static final OpType.Set OPS_EXC = ops(OP_EXC);
  public static final OpType.Set OPS_EXC2 = ops(OP_EXC, OP_EXC);
  public static final OpType.Set OPS_STRB = ops(OP_STRB);

  public static final COpInfo NO_OP = opi(NO_OPS);
}
