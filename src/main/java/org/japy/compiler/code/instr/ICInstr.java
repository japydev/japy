package org.japy.compiler.code.instr;

import org.japy.infra.loc.IHasLocation;

public interface ICInstr extends IHasLocation {
  COpInfo opInfo();

  default int defaultFlags() {
    return opInfo().flags;
  }

  default int popCount() {
    return opInfo().popCount();
  }

  default int pushCount() {
    return opInfo().pushCount();
  }
}
