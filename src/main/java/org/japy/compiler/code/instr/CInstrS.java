package org.japy.compiler.code.instr;

import static org.japy.compiler.code.instr.COpInfo.*;
import static org.japy.infra.validation.Debug.dcheck;

import org.japy.infra.loc.IHasLocation;

public final class CInstrS extends CInstr {
  public enum Code {
    // no FLAG_THROW on these, so it doesn't confuse the unreachable code check
    NOTIMPLEMENTED(NO_OP),
    DBC(NO_OP),
    DELATTR(opi(OPS_OBJ, NO_OPS)),
    SETATTR(opi(OPS_OBJ2, NO_OPS)),
    GETATTR(opi(OPS_OBJ, OPS_OBJ)),
    ADDKWARG(opi(ops(OP_ARGSB, OP_OBJ), OPS_ARGSB)),
    AUGASSIGNATTR(opi(OPS_OBJ2, NO_OPS), true),
    STRBAPPENDS(opi(OPS_STRB, OPS_STRB)),
    STRING(opi(NO_OPS, OPS_STR)),
    ;

    public final COpInfo opInfo;
    public final boolean needIntArg;

    Code(COpInfo opInfo) {
      this(opInfo, false);
    }

    Code(COpInfo opInfo, boolean needIntArg) {
      this.opInfo = opInfo;
      this.needIntArg = needIntArg;
    }
  }

  public final Code code;
  public final String arg;
  public final int intarg;

  public static final int INVALID_ARG = Integer.MAX_VALUE;

  public CInstrS(Code code, String arg, IHasLocation location) {
    this(code, arg, INVALID_ARG, location);
  }

  public CInstrS(Code code, String arg, int intarg, IHasLocation location) {
    super(location);
    this.arg = arg;
    this.intarg = intarg;
    this.code = code;
    dcheck((intarg != INVALID_ARG) == code.needIntArg);
  }

  @Override
  public Kind kind() {
    return Kind.INSTRS;
  }

  @Override
  public COpInfo opInfo() {
    return code.opInfo;
  }

  @Override
  public String toString() {
    if (code.needIntArg) {
      return code + "(" + arg + ", " + intarg + ")";
    } else {
      return code + "(" + arg + ")";
    }
  }
}
