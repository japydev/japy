package org.japy.compiler.code.instr;

import static org.japy.compiler.code.instr.COpInfo.*;

import java.util.Arrays;

import org.japy.infra.loc.IHasLocation;

public final class CSwitch extends CInstr {
  private static final COpInfo OP_INFO = opi(OPS_INT, NO_OPS);

  public final CLabel[] targets;

  public CSwitch(CLabel[] targets, IHasLocation location) {
    super(location);
    this.targets = targets;
  }

  @Override
  public COpInfo opInfo() {
    return OP_INFO;
  }

  @Override
  public Kind kind() {
    return Kind.SWITCH;
  }

  @Override
  public String toString() {
    return "SWITCH" + Arrays.toString(targets);
  }
}
