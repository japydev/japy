package org.japy.compiler.code.instr;

import static org.japy.compiler.code.instr.COpInfo.*;

import org.japy.infra.loc.IHasLocation;
import org.japy.kernel.types.IPyObj;

public final class CLiteral extends CInstr {
  private static final COpInfo OP_INFO = opi(NO_OPS, OPS_OBJ);

  public final IPyObj value;

  public CLiteral(IPyObj value, IHasLocation location) {
    super(location);
    this.value = value;
  }

  @Override
  public String toString() {
    return "LITERAL(" + value + ")";
  }

  @Override
  public COpInfo opInfo() {
    return OP_INFO;
  }

  @Override
  public Kind kind() {
    return Kind.LITERAL;
  }
}
