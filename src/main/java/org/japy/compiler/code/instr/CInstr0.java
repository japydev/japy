package org.japy.compiler.code.instr;

import static org.japy.compiler.code.instr.COpInfo.*;

import org.japy.infra.loc.IHasLocation;

public final class CInstr0 extends CInstr {
  public enum Code {
    DUP(opi(OPS_OBJ, OPS_OBJ2)),
    DUPA(opi(OPS_AOBJ, OPS_AOBJ2)),
    DUPX1(opi(OPS_OBJ2, OPS_OBJ3)),
    DUPBOOL(opi(OPS_BOOL, OPS_BOOL2)),
    DUPEXC(opi(OPS_EXC, OPS_EXC2)),
    POP(opi(OPS_OBJ, NO_OPS)),
    POPBOOL(opi(OPS_BOOL, NO_OPS)),
    POPEXC(opi(OPS_EXC, NO_OPS)),
    SWAP(opi(OPS_OBJ2, OPS_OBJ2)),
    TOBOOL(opi(OPS_OBJ, OPS_BOOL)),
    FROMBOOL(opi(OPS_BOOL, OPS_OBJ)),
    INVBOOL(opi(OPS_BOOL, OPS_BOOL)),
    NOTOBJ(opi(OPS_OBJ, OPS_OBJ)),
    ISNOTNULL(opi(OPS_OBJ, OPS_OBJ)),
    LOADTRUE(opi(NO_OPS, OPS_BOOL)),
    LOADFALSE(opi(NO_OPS, OPS_BOOL)),
    LOADNULLOBJ(opi(NO_OPS, OPS_OBJ)),
    LOADNULLARGS(opi(NO_OPS, OPS_ARGS)),
    LOADNULLAOBJ(opi(NO_OPS, OPS_AOBJ)),
    MAKEFORITER(opi(OPS_OBJ, OPS_OBJ)),
    FORITERNEXT(opi(OPS_OBJ, OPS_OBJ)),
    ITER(opi(OPS_OBJ, OPS_OBJ)),
    SETITEM(opi(OPS_OBJ3, NO_OPS)),
    GETITEM(opi(OPS_OBJ2, OPS_OBJ)),
    DELITEM(opi(OPS_OBJ2, NO_OPS)),
    LEN(opi(OPS_OBJ, OPS_OBJ)),
    ASSERTIONFAILED(opi(FLAG_THROW, OPS_OBJ, NO_OPS)),
    ASSERTIONFAILEDMSG(opi(FLAG_THROW, OPS_OBJ2, NO_OPS)),
    PYTYPE(opi(OPS_OBJ, OPS_OBJ)),
    RET(opi(FLAG_RET, OPS_OBJ, NO_OPS)),
    RAISE(opi(FLAG_THROW, NO_OPS, NO_OPS)),
    RAISE_EX(opi(FLAG_THROW, OPS_OBJ, NO_OPS)),
    RAISE_FROM(opi(FLAG_THROW, OPS_OBJ2, NO_OPS)),
    THROW(opi(FLAG_THROW, OPS_EXC, NO_OPS)),
    YIELD(opi(FLAG_YIELD, OPS_OBJ, OPS_OBJ)),
    YIELDFROM(opi(FLAG_YIELD, OPS_OBJ, OPS_OBJ)),
    AWAIT(opi(FLAG_AWAIT, OPS_OBJ, OPS_OBJ)),
    CALLARGS(opi(ops(OP_OBJ, OP_ARGS), OPS_OBJ)),
    VCALLARGS(opi(ops(OP_OBJ, OP_ARGS), NO_OPS)),
    ADDARG(opi(ops(OP_ARGSB, OP_OBJ), OPS_ARGSB)),
    ADDARGTUPLE(opi(ops(OP_ARGSB, OP_OBJ), OPS_ARGSB)),
    ADDKWARGDICT(opi(ops(OP_ARGSB, OP_OBJ), OPS_ARGSB)),
    BUILDARGS(opi(OPS_ARGSB, OPS_ARGS)),
    SYSEXCINFO(opi(NO_OPS, OPS_OBJ)),
    MAKESLICE(opi(OPS_OBJ3, OPS_OBJ)),
    LISTAPPEND(opi(OPS_OBJ2, OPS_OBJ)),
    LISTADDVOID(opi(OPS_OBJ2, NO_OPS)),
    LISTADDSTAR(opi(OPS_OBJ2, OPS_OBJ)),
    SETADD(opi(OPS_OBJ2, OPS_OBJ)),
    SETADDVOID(opi(OPS_OBJ2, NO_OPS)),
    SETADDSTAR(opi(OPS_OBJ2, OPS_OBJ)),
    TUPBADD(opi(ops(OP_TUPB, OP_OBJ), OPS_TUPB)),
    TUPBADDSTAR(opi(ops(OP_TUPB, OP_OBJ), OPS_TUPB)),
    BUILDTUP(opi(OPS_TUPB, OPS_OBJ)),
    DICTADDSTAR(opi(OPS_OBJ2, OPS_OBJ)),
    DICTADD(opi(OPS_OBJ3, OPS_OBJ)),
    VDICTADD(opi(OPS_OBJ3, NO_OPS)),
    SETCURRENTEXC(opi(OPS_OBJ, OPS_OBJ)),
    EXCMATCH(opi(OPS_OBJ2, OPS_BOOL)),
    PARSEARGS0(NO_OP),
    PARSEARGS1(opi(NO_OPS, OPS_OBJ)),
    PARSEARGS(opi(NO_OPS, OPS_AOBJ)),
    NEWSTRB(opi(NO_OPS, OPS_STRB)),
    BUILDSTR(opi(OPS_STRB, OPS_OBJ)),
    STRBAPPEND(opi(ops(OP_STRB, OP_OBJ), OPS_STRB)),
    REPR(opi(OPS_OBJ, OPS_OBJ)),
    STR(opi(OPS_OBJ, OPS_OBJ)),
    ASCII(opi(OPS_OBJ, OPS_OBJ)),
    FORMAT(opi(OPS_OBJ2, OPS_OBJ)),
    NOP(NO_OP),
    IGNOP(NO_OP),
    NEWTUPLE(opi(OPS_AOBJ, OPS_OBJ)),
    // __import__(op1, fromlist=op2, level=op3)
    IMPORT(opi(OPS_OBJ3, OPS_OBJ)),
    // from op1 import *
    IMPORTSTAR(opi(OPS_OBJ, NO_OPS)),
    ;

    public final COpInfo opInfo;

    Code(COpInfo opInfo) {
      this.opInfo = opInfo;
    }
  }

  public final Code code;

  public CInstr0(Code code, IHasLocation location) {
    super(location);
    this.code = code;
  }

  @Override
  public Kind kind() {
    return Kind.INSTR0;
  }

  @Override
  public String toString() {
    return code.toString();
  }

  @Override
  public COpInfo opInfo() {
    return code.opInfo;
  }
}
