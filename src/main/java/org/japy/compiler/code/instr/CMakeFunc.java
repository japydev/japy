package org.japy.compiler.code.instr;

import static org.japy.compiler.code.instr.COpInfo.*;

import org.japy.compiler.code.CFunc;

public final class CMakeFunc extends CInstr {
  private static final COpInfo OP_INFO = opi(OPS_OBJ2, OPS_OBJ);

  public final CFunc func;

  public CMakeFunc(CFunc func) {
    super(func);
    this.func = func;
  }

  @Override
  public Kind kind() {
    return Kind.MAKEFUNC;
  }

  @Override
  public String toString() {
    return "MAKEFUNC(" + func.binName + ")";
  }

  @Override
  public COpInfo opInfo() {
    return OP_INFO;
  }
}
