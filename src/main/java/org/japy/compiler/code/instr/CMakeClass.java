package org.japy.compiler.code.instr;

import static org.japy.compiler.code.instr.COpInfo.OPS_ARGS;
import static org.japy.compiler.code.instr.COpInfo.OPS_OBJ;
import static org.japy.compiler.code.instr.COpInfo.opi;

import org.japy.compiler.code.CClass;

public final class CMakeClass extends CInstr {
  private static final COpInfo OP_INFO = opi(OPS_ARGS, OPS_OBJ);

  public final CClass klass;

  public CMakeClass(CClass klass) {
    super(klass);
    this.klass = klass;
  }

  @Override
  public Kind kind() {
    return Kind.MAKECLASS;
  }

  @Override
  public String toString() {
    return "MAKECLASS(" + klass.binName + ")";
  }

  @Override
  public COpInfo opInfo() {
    return OP_INFO;
  }
}
