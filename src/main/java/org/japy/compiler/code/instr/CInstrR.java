package org.japy.compiler.code.instr;

import static org.japy.compiler.code.instr.COpInfo.*;
import static org.japy.infra.validation.Validator.check;

import org.japy.compiler.code.CRef;
import org.japy.compiler.code.CVarType;
import org.japy.compiler.impl.VarTypeInfo;
import org.japy.infra.loc.IHasLocation;
import org.japy.infra.validation.Debug;

public final class CInstrR extends CInstr {
  public enum Code {
    LOAD(opi(NO_OPS, OPS_OBJ)),
    STORE(opi(OPS_OBJ, NO_OPS)),
    STOREEXC(opi(OPS_EXC, NO_OPS)),
    DEL(NO_OP),
    DELIFBOUND(NO_OP),
    ERASETEMP(NO_OP),
    INITTEMP(NO_OP),
    LOADBOOL(opi(NO_OPS, OPS_BOOL)),
    LOADSTRB(opi(NO_OPS, OPS_STRB)),
    LOADEXC(opi(NO_OPS, OPS_EXC)),
    LOADINT(opi(NO_OPS, OPS_INT)),
    STORECONSTINT(NO_OP, true),
    CAPTUREEXC(opi(OPS_EXC, NO_OPS)),
    BEGINTEMP(NO_OP),
    ENDTEMP(NO_OP),
    AUGASSIGN(opi(OPS_OBJ, NO_OPS), true),
    ANNOTATE(opi(OPS_OBJ, NO_OPS)),
    ;

    public final boolean needArg;
    public final COpInfo opInfo;

    Code(COpInfo opInfo) {
      this(opInfo, false);
    }

    Code(COpInfo opInfo, boolean needArg) {
      this.opInfo = opInfo;
      this.needArg = needArg;
    }
  }

  public static final int INVALID_ARG = Integer.MAX_VALUE;

  public final Code code;
  public final CRef ref;
  public final int arg;

  public CInstrR(Code code, CRef ref, IHasLocation location) {
    this(code, ref, INVALID_ARG, location);
  }

  public CInstrR(Code code, CRef ref, int arg, IHasLocation location) {
    super(location);
    this.code = code;
    this.ref = ref;
    this.arg = arg;

    if (Debug.ENABLED) {
      validate();
    }
  }

  @Override
  public Kind kind() {
    return Kind.INSTRR;
  }

  @Override
  public COpInfo opInfo() {
    return code.opInfo;
  }

  private void validate() {
    check(code.needArg == (arg != INVALID_ARG), "bad argument");
    switch (code) {
      case ERASETEMP:
        check(
            !VarTypeInfo.get(ref.type()).isPrimitive(),
            "bad type " + ref.type() + " for instruction " + code + ", var " + ref.name());
        break;
      case LOAD:
      case STORE:
      case DEL:
      case DELIFBOUND:
      case CAPTUREEXC:
      case AUGASSIGN:
      case ANNOTATE:
        check(
            VarTypeInfo.get(ref.type()).isPyObj,
            "bad type " + ref.type() + " for instruction " + code + ", var " + ref.name());
        break;
      case LOADBOOL:
        check(
            ref.type() == CVarType.BOOL,
            "bad type " + ref.type() + " for instruction " + code + ", var " + ref.name());
        break;
      case LOADSTRB:
        check(
            ref.type() == CVarType.STRB,
            "bad type " + ref.type() + " for instruction " + code + ", var " + ref.name());
        break;
      case LOADEXC:
      case STOREEXC:
        check(
            ref.type() == CVarType.EXC,
            "bad type " + ref.type() + " for instruction " + code + ", var " + ref.name());
        break;
      case LOADINT:
      case STORECONSTINT:
        check(
            ref.type() == CVarType.INT,
            "bad type " + ref.type() + " for instruction " + code + ", var " + ref.name());
        break;
      case BEGINTEMP:
      case ENDTEMP:
      case INITTEMP:
        break;
    }
  }

  @Override
  public String toString() {
    return code + "(" + ref + (code.needArg ? ", " + arg : "") + ")";
  }
}
