package org.japy.compiler.code;

import com.google.errorprone.annotations.DoNotCall;

import org.japy.base.BinOp;
import org.japy.base.UnaryOp;
import org.japy.compiler.code.instr.*;
import org.japy.compiler.code.instr.CLabel;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.IHasLocation;

public interface CInstrSink {
  void write(CInstr instr);

  default void write(CInstr0.Code code, IHasLocation location) {
    write(new CInstr0(code, location));
  }

  default void write(CInstrI.Code code, int arg, IHasLocation location) {
    write(new CInstrI(code, arg, location));
  }

  default void write(CInstrI2.Code code, int arg1, int arg2, IHasLocation location) {
    write(new CInstrI2(code, arg1, arg2, location));
  }

  default void write(CInstrS.Code code, String arg, IHasLocation location) {
    write(new CInstrS(code, arg, location));
  }

  default void write(CInstrS.Code code, String arg, int intarg, IHasLocation location) {
    write(new CInstrS(code, arg, intarg, location));
  }

  default void write(BinOp op, IHasLocation location) {
    write(CInstrI.Code.BINOP, op.ordinal(), location);
  }

  default void write(UnaryOp op, IHasLocation location) {
    write(CInstrI.Code.UNARYOP, op.ordinal(), location);
  }

  default void write(CInstrR.Code code, CLocalVar var, IHasLocation location) {
    write(new CInstrR(code, new CRef(var), location));
  }

  default void write(CInstrR.Code code, CLocalVar var, int arg, IHasLocation location) {
    write(code, new CRef(var), arg, location);
  }

  default void write(CInstrR.Code code, CTempVar var, IHasLocation location) {
    write(new CInstrR(code, new CRef(var), location));
  }

  default void write(CInstrR.Code code, CTempVar var, int arg, IHasLocation location) {
    write(code, new CRef(var), arg, location);
  }

  default void write(CInstrR.Code code, CUpvalue upvalue, IHasLocation location) {
    write(code, new CRef(upvalue), location);
  }

  default void write(CInstrR.Code code, CUpvalue upvalue, int arg, IHasLocation location) {
    write(code, new CRef(upvalue), arg, location);
  }

  default void write(CInstrR.Code code, CRef ref, IHasLocation location) {
    write(new CInstrR(code, ref, location));
  }

  default void write(CInstrR.Code code, CRef ref, int arg, IHasLocation location) {
    write(new CInstrR(code, ref, arg, location));
  }

  default void write(CLabel label, IHasLocation location) {
    label.setLocation(location.location());
    write((CInstr) label);
  }

  default void jump(CLabel target, IHasLocation location) {
    write(new CJump(CJump.Code.JUMP_ALWAYS, target, location));
  }

  default void jump(CJump.Code code, CLabel target, IHasLocation location) {
    write(new CJump(code, target, location));
  }

  @DoNotCall
  @Deprecated
  @SuppressWarnings("DoNotCall")
  default void write(@SuppressWarnings("unused") CLabel label) {
    throw InternalErrorException.notReached();
  }
}
