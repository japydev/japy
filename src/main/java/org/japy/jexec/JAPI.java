package org.japy.jexec;

import static org.japy.infra.util.ObjUtil.or;
import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.kernel.types.misc.PyNone.None;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.base.AugOp;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.util.ExcUtil;
import org.japy.infra.validation.Debug;
import org.japy.jexec.codegen.Constants;
import org.japy.jexec.obj.IJGenObj;
import org.japy.jexec.obj.JAsyncGeneratorFunc;
import org.japy.jexec.obj.JCoroutineFunc;
import org.japy.jexec.obj.JFunc;
import org.japy.jexec.obj.JGeneratorFunc;
import org.japy.jexec.obj.JGeneratorImpl;
import org.japy.jexec.pkg.JPackage;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Arithmetics;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.set.PySet;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.px.PxAssertionError;
import org.japy.kernel.types.exc.px.PxException;
import org.japy.kernel.types.exc.px.PxNameError;
import org.japy.kernel.types.exc.px.PxRuntimeError;
import org.japy.kernel.types.exc.px.PxUnboundLocalError;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.func.PyFuncHeader;
import org.japy.kernel.types.misc.PySlice;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.Args;

public final class JAPI {
  @SuppressWarnings("unchecked")
  public static IPyObj makeFunc(PyFuncHeader header, String binName, IJGenObj self) {
    Class<?> klass = JPackage.getBlockClass(self, binName);
    if (header.isAsync || header.isGenerator) {
      if (Debug.ENABLED) {
        dcheck(JGeneratorImpl.class.isAssignableFrom(klass));
      }
      Class<? extends JGeneratorImpl> genClass = (Class<? extends JGeneratorImpl>) klass;
      if (header.isAsync) {
        if (header.isGenerator) {
          return new JAsyncGeneratorFunc(header, genClass);
        } else {
          return new JCoroutineFunc(header, genClass);
        }
      } else {
        return new JGeneratorFunc(header, genClass);
      }
    }

    if (Debug.ENABLED) {
      dcheck(JFunc.class.isAssignableFrom(klass));
    }
    Class<? extends JFunc> funcClass = (Class<? extends JFunc>) klass;
    try {
      Constructor<? extends JFunc> constructor = funcClass.getConstructor(PyFuncHeader.class);
      return constructor.newInstance(header);
    } catch (NoSuchMethodException
        | InvocationTargetException
        | InstantiationException
        | IllegalAccessException ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  public static void oops(String what) {
    throw new NotImplementedException(what);
  }

  public static PyBool __debug__(PyContext ctx) {
    return PyBool.of(ctx.kernel.config.enableDebug());
  }

  public static Throwable assertionFailed(IPyObj exprText, PyContext ctx) {
    // TODO expression text
    throw new PxAssertionError(null, ctx);
  }

  public static Throwable assertionFailed(IPyObj msg, IPyObj exprText, PyContext ctx) {
    // TODO expression text
    throw new PxAssertionError(PrintLib.str(msg, ctx), ctx);
  }

  public static boolean excMatch(PyBaseException ex, IPyObj spec, PyContext ctx) {
    if (spec instanceof PyTuple) {
      for (int i = 0, c = ((PyTuple) spec).len(ctx); i != c; ++i) {
        if (excMatch(ex, ((PyTuple) spec).get(i), ctx)) {
          return true;
        }
      }
      return false;
    }

    if (!(spec instanceof IPyClass)) {
      throw PxException.typeError(
          String.format("expected an exception class, got '%s'", spec.typeName()), ctx);
    }

    IPyClass cls = (IPyClass) spec;
    if (!cls.isSubClassRaw(PyBuiltinExc.BaseException)) {
      throw PxException.typeError(
          String.format("expected an exception class, got '%s'", cls.name()), ctx);
    }

    return ex.isInstanceRaw(cls);
  }

  public static IPyObj getLocal(IPyDict locals, String name, PyContext ctx) {
    @Nullable IPyObj v = locals.getOrNull(name, ctx);
    if (v != null) {
      return v;
    }
    throw PxUnboundLocalError.localNotDefined(name, ctx);
  }

  public static void delLocal(IPyDict locals, String name, PyContext ctx) {
    if (locals.popOrNull(name, ctx) == null) {
      throw PxUnboundLocalError.localNotDefined(name, ctx);
    }
  }

  public static PyList listExtend(PyList list, IPyObj other, PyContext ctx) {
    list.extend(other, ctx);
    return list;
  }

  public static PySet setAdd(PySet set, IPyObj obj, PyContext ctx) {
    set.add(obj, ctx);
    return set;
  }

  public static PySet setExtend(PySet set, IPyObj obj, PyContext ctx) {
    set.extend(obj, ctx);
    return set;
  }

  public static PyDict dictAdd(PyDict dict, IPyObj key, IPyObj value, PyContext ctx) {
    dict.setItem(key, value, ctx);
    return dict;
  }

  public static PyDict dictExtend(PyDict dict, IPyObj pairs, PyContext ctx) {
    dict.extend(pairs, ctx);
    return dict;
  }

  public static IPyObj import_(
      IPyObj name, IPyObj fromlist, IPyObj level, IPyDict dict, PyContext ctx) {
    @Nullable
    IPyObj __import__ = ctx.kernel.builtins().get(org.japy.kernel.util.Constants.__IMPORT__);
    if (__import__ == null) {
      throw new PxRuntimeError("builtins.__import__ is missing", ctx);
    }
    return FuncLib.call(__import__, Args.of(name, dict, None, fromlist, level), ctx);
  }

  public static PyTuple unpackExact(IPyObj list, int count, PyContext ctx) {
    PyTuple tup = CollLib.unpack(list, ctx);
    if (tup.len() > count) {
      throw PxException.valueError(
          String.format("too many values to unpack (expected %s)", count), ctx);
    } else if (tup.len() < count) {
      throw PxException.valueError(
          String.format("not enough values to unpack (expected %s, got %s)", count, tup.len()),
          ctx);
    }
    return tup;
  }

  public static PyTuple unpackAtLeast(IPyObj list, int count, PyContext ctx) {
    PyTuple tup = CollLib.unpack(list, ctx);
    if (tup.len() < count) {
      throw PxException.valueError(
          String.format("not enough values to unpack (expected %s, got %s)", count, tup.len()),
          ctx);
    }
    return tup;
  }

  public static void augAssignIndex(
      IPyObj coll, IPyObj index, IPyObj value, AugOp op, PyContext ctx) {
    IPyObj target = CollLib.getItem(coll, index, ctx);
    IPyObj result = Arithmetics.augOp(op, target, value, ctx);
    CollLib.setItem(coll, index, result, ctx);
  }

  public static void augAssignAttr(IPyObj obj, String attr, IPyObj value, AugOp op, PyContext ctx) {
    IPyObj target = ObjLib.getAttr(obj, attr, ctx);
    IPyObj result = Arithmetics.augOp(op, target, value, ctx);
    ObjLib.setAttr(obj, attr, result, ctx);
  }

  public static PySlice slice(IPyObj start, IPyObj stop, IPyObj step) {
    return new PySlice(start, stop, step);
  }

  public static PyStr getModuleName(IPyRODict globals, PyContext ctx) {
    @Nullable IPyObj name = globals.getOrNull(org.japy.kernel.util.Constants.__NAME__, ctx);
    if (name == null) {
      throw new InternalErrorException("module name is missing");
    }
    if (!(name instanceof PyStr)) {
      throw PxException.typeError("module name is not a string", ctx);
    }
    return (PyStr) name;
  }

  public static PyStr getQualName(String name, IPyRODict globals, PyContext ctx) {
    String moduleName = getModuleName(globals, ctx).toString();
    return PyStr.get(
        !moduleName.equals(org.japy.kernel.util.Constants.__MAIN__)
            ? moduleName + "." + name
            : name);
  }

  public static IPyObj getClassLocal(
      @Nullable IPyObj v, String name, IPyRODict globals, IPyRODict builtins, PyContext ctx) {
    return or(v, () -> getGlobal(name, globals, builtins, ctx));
  }

  public static IPyObj getGlobal(
      String name, IPyRODict globals, IPyRODict builtins, PyContext ctx) {
    @Nullable IPyObj gv = globals.getOrNull(name, ctx);
    if (gv != null) {
      return gv;
    }
    @Nullable IPyObj bv = builtins.getOrNull(name, ctx);
    if (bv != null) {
      return bv;
    }
    throw PxNameError.nameNotDefined(name, ctx);
  }

  public static void delGlobal(IPyDict globals, String name, PyContext ctx) {
    if (globals.popOrNull(name, ctx) == null) {
      throw PxNameError.nameNotDefined(name, ctx);
    }
  }

  private static String[] getNames(IJGenObj obj, String field) {
    try {
      Field f = obj.getClass().getField(field);
      return ((String[]) f.get(null));
    } catch (NoSuchFieldException | IllegalAccessException ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  public static String[] getLocalNames(IJGenObj obj) {
    return getNames(obj, Constants.FIELD_LOCAL_NAMES);
  }

  public static String[] getCellNames(IJGenObj obj) {
    return getNames(obj, Constants.FIELD_CELL_NAMES);
  }

  public static RuntimeException unboundLocal(IJGenObj obj, int index, PyContext ctx) {
    return PxUnboundLocalError.localNotDefined(getLocalNames(obj)[index], ctx);
  }

  public static RuntimeException unboundCell(IJGenObj obj, int index, PyContext ctx) {
    return PxUnboundLocalError.localNotDefined(getCellNames(obj)[index], ctx);
  }

  public static RuntimeException unboundUpvalue(IJGenObj obj, int index, PyContext ctx) {
    throw PxNameError.upvalueNotDefined(getNames(obj, Constants.FIELD_UPVALUE_NAMES)[index], ctx);
  }

  public static void createAnnotationDict(IPyDict namespace, PyContext ctx) {
    if (namespace.getOrNull(org.japy.kernel.util.Constants.__ANNOTATIONS__, ctx) == null) {
      namespace.setItem(org.japy.kernel.util.Constants.__ANNOTATIONS__, new PyDict(), ctx);
    }
  }

  public static PyDict dict(IPyObj key1, IPyObj value1, PyContext ctx) {
    PyDict d = new PyDict(1);
    d.setItem(key1, value1, ctx);
    return d;
  }

  public static PyDict dict(IPyObj key1, IPyObj value1, IPyObj key2, IPyObj value2, PyContext ctx) {
    PyDict d = new PyDict(2);
    d.setItem(key1, value1, ctx);
    d.setItem(key2, value2, ctx);
    return d;
  }

  public static PyDict dict(
      IPyObj key1,
      IPyObj value1,
      IPyObj key2,
      IPyObj value2,
      IPyObj key3,
      IPyObj value3,
      PyContext ctx) {
    PyDict d = new PyDict(3);
    d.setItem(key1, value1, ctx);
    d.setItem(key2, value2, ctx);
    d.setItem(key3, value3, ctx);
    return d;
  }

  public static PyDict dict(
      IPyObj key1,
      IPyObj value1,
      IPyObj key2,
      IPyObj value2,
      IPyObj key3,
      IPyObj value3,
      IPyObj key4,
      IPyObj value4,
      PyContext ctx) {
    PyDict d = new PyDict(4);
    d.setItem(key1, value1, ctx);
    d.setItem(key2, value2, ctx);
    d.setItem(key3, value3, ctx);
    d.setItem(key4, value4, ctx);
    return d;
  }

  private JAPI() {}
}
