package org.japy.jexec.obj;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.lang.reflect.InvocationTargetException;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.jexec.pkg.JPackage;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.mod.IPyExtModuleExec;

public abstract class JModuleExec implements IPyExtModuleExec, IJGenObj {
  protected final IPyObj[] constants;
  protected @Nullable IPyDict globals;
  protected @Nullable IPyDict builtins;

  protected JModuleExec() {
    this.constants = JPackage.getConstants(this);
  }

  @Override
  public final void exec(IPyDict namespace, IPyRODict builtins, PyContext ctx) {
    ctx.thread.push(this);
    try {
      invoke(namespace, builtins, ctx);
    } finally {
      ctx.thread.pop(this);
    }
  }

  protected abstract void invoke(IPyDict namespace, IPyRODict builtins, PyContext ctx);

  @Override
  public IPyDict globals() {
    return dcheckNotNull(globals);
  }

  @Override
  public IPyRODict builtins() {
    return dcheckNotNull(builtins);
  }

  @Override
  public IPyDict locals() {
    return dcheckNotNull(globals);
  }

  public static JModuleExec create(JPackage pkg, String binName) {
    @SuppressWarnings("unchecked")
    Class<? extends JModuleExec> clazz = (Class<? extends JModuleExec>) pkg.getBlockClass(binName);
    try {
      return clazz.getConstructor().newInstance();
    } catch (InstantiationException
        | IllegalAccessException
        | InvocationTargetException
        | NoSuchMethodException ex) {
      throw ExcUtil.rethrow(ex);
    }
  }
}
