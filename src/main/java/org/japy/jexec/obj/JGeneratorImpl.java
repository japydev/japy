package org.japy.jexec.obj;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.ISupportsValidation;
import org.japy.infra.validation.Validator;
import org.japy.jexec.JAPI;
import org.japy.jexec.pkg.JPackage;
import org.japy.kernel.exec.Cell;
import org.japy.kernel.exec.IFrame;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.func.PyFuncHeader;
import org.japy.kernel.types.gen.GeneratorImpl;

public abstract class JGeneratorImpl extends GeneratorImpl implements IFrame, IJGenObj {
  protected final IPyObj[] constants;
  protected int nextPoint;
  protected final @Nullable Object @Nullable [] temps;

  protected JGeneratorImpl(PyFuncHeader header, int tempCount) {
    super(header);
    this.constants = JPackage.getConstants(this);
    this.temps = tempCount != 0 ? new Object[tempCount] : null;
  }

  @Override
  protected final IPyObj resume(IPyObj value, @Nullable PyBaseException ex, PyContext ctx) {
    ctx.thread.push(this);
    try {
      return resumeImpl(value, ex, ctx);
    } finally {
      ctx.thread.pop(this);
    }
  }

  protected abstract IPyObj resumeImpl(IPyObj value, @Nullable PyBaseException ex, PyContext ctx);

  protected IPyObj lv(@Nullable IPyObj v, int index, PyContext ctx) {
    if (v != null) {
      return v;
    }
    throw JAPI.unboundLocal(this, index, ctx);
  }

  protected IPyObj cv(@Nullable IPyObj v, int index, PyContext ctx) {
    if (v != null) {
      return v;
    }
    throw JAPI.unboundCell(this, index, ctx);
  }

  protected IPyObj uv(Cell c, int index, PyContext ctx) {
    @Nullable IPyObj v = c.value;
    if (v != null) {
      return v;
    }
    throw JAPI.unboundUpvalue(this, index, ctx);
  }

  @Override
  public void validate(Validator v) {
    v.validate(constants);
    if (temps != null) {
      for (Object o : temps) {
        if (o instanceof ISupportsValidation) {
          v.validate((ISupportsValidation) o);
        }
      }
    }
  }

  @Override
  public IPyDict globals() {
    return header.globals;
  }

  @Override
  public IPyRODict builtins() {
    return header.builtins;
  }

  @Override
  public boolean hasLocals() {
    return true;
  }

  @Override
  public IPyDict locals() {
    throw new NotImplementedException("locals");
  }
}
