package org.japy.jexec.obj;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

import org.japy.infra.util.ExcUtil;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.func.PyFunc;
import org.japy.kernel.types.func.PyFuncHeader;
import org.japy.kernel.types.gen.PyGenerator;
import org.japy.kernel.util.Args;

public class JGeneratorFunc extends PyFunc {
  private static final MethodType METHOD_TYPE =
      MethodType.methodType(JGeneratorImpl.class, PyFuncHeader.class, Args.class, PyContext.class);

  private final MethodHandle createGenerator;

  public JGeneratorFunc(PyFuncHeader header, Class<? extends JGeneratorImpl> genClass) {
    super(header);
    try {
      this.createGenerator =
          MethodHandles.lookup()
              .unreflectConstructor(
                  genClass.getConstructor(PyFuncHeader.class, Args.class, PyContext.class))
              .asType(METHOD_TYPE);
    } catch (IllegalAccessException | NoSuchMethodException ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  @Override
  public IPyObj call(Args args, PyContext ctx) {
    try {
      return new PyGenerator((JGeneratorImpl) createGenerator.invokeExact(header, args, ctx));
    } catch (Throwable ex) {
      throw ExcUtil.rethrow(ex);
    }
  }
}
