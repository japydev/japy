package org.japy.jexec.obj;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.infra.util.ExcUtil;
import org.japy.jexec.JAPI;
import org.japy.jexec.pkg.JPackage;
import org.japy.kernel.exec.Cell;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.ext.IPyClassExec;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;

public abstract class JClassExec implements IPyClassExec, IJGenObj {
  protected final Cell[] upvalues;
  protected final IPyObj[] constants;
  protected @Nullable IPyDict globals;
  protected @Nullable IPyRODict builtins;

  protected JClassExec(Cell[] upvalues) {
    this.upvalues = upvalues;
    this.constants = JPackage.getConstants(this);
  }

  @Override
  public final void exec(
      IPyDict dict, IPyDict globals, IPyRODict builtins, Cell classCell, PyContext ctx) {
    ctx.thread.push(this);
    try {
      invoke(dict, globals, builtins, classCell, ctx);
    } finally {
      ctx.thread.pop(this);
    }
  }

  protected abstract void invoke(
      IPyDict dict, IPyDict globals, IPyRODict builtins, Cell classCell, PyContext ctx);

  @Override
  public IPyDict globals() {
    return dcheckNotNull(globals);
  }

  @Override
  public IPyRODict builtins() {
    return dcheckNotNull(builtins);
  }

  public static JClassExec make(IJGenObj self, String binName, Cell[] upvalues) {
    Class<?> execClass = JPackage.getBlockClass(self, binName);
    try {
      Constructor<?> constructor = execClass.getConstructor(Cell[].class);
      return (JClassExec) constructor.newInstance((Object) upvalues);
    } catch (NoSuchMethodException
        | InvocationTargetException
        | InstantiationException
        | IllegalAccessException ex) {
      throw ExcUtil.rethrow(ex);
    }
  }

  protected IPyObj uv(Cell c, int index, PyContext ctx) {
    @Nullable IPyObj v = c.value;
    if (v != null) {
      return v;
    }
    throw JAPI.unboundUpvalue(this, index, ctx);
  }
}
