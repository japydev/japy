package org.japy.jexec.obj;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.jexec.JAPI;
import org.japy.jexec.pkg.JPackage;
import org.japy.kernel.exec.Cell;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.func.PyFunc;
import org.japy.kernel.types.func.PyFuncHeader;

public abstract class JFunc extends PyFunc implements IJGenObj {
  protected final IPyObj[] constants;

  protected JFunc(PyFuncHeader header) {
    super(header);
    this.constants = JPackage.getConstants(this);
  }

  protected IPyObj lv(@Nullable IPyObj v, int index, PyContext ctx) {
    if (v != null) {
      return v;
    }
    throw JAPI.unboundLocal(this, index, ctx);
  }

  protected IPyObj cv(@Nullable IPyObj v, int index, PyContext ctx) {
    if (v != null) {
      return v;
    }
    throw JAPI.unboundCell(this, index, ctx);
  }

  protected IPyObj uv(Cell c, int index, PyContext ctx) {
    @Nullable IPyObj v = c.value;
    if (v != null) {
      return v;
    }
    throw JAPI.unboundUpvalue(this, index, ctx);
  }
}
