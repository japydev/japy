package org.japy.jexec.obj;

import org.japy.infra.exc.InternalErrorException;
import org.japy.kernel.exec.IFrame;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.func.PyFuncHeader;
import org.japy.kernel.util.Args;

public abstract class JNativeFunc extends JFunc implements IFrame {
  protected JNativeFunc(PyFuncHeader header) {
    super(header);
  }

  @Override
  public final IPyObj call(Args args, PyContext ctx) {
    ctx.thread.push(this);
    try {
      return invoke(args, ctx);
    } finally {
      ctx.thread.pop(this);
    }
  }

  protected abstract IPyObj invoke(Args args, PyContext ctx);

  @Override
  public IPyDict globals() {
    return header.globals;
  }

  @Override
  public IPyRODict builtins() {
    return header.builtins;
  }

  @Override
  public boolean hasLocals() {
    return false;
  }

  @Override
  public IPyDict locals() {
    throw new InternalErrorException("JNativeFunc.locals() called");
  }
}
