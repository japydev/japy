package org.japy.jexec.obj;

import org.japy.infra.exc.NotImplementedException;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.func.PyFuncHeader;
import org.japy.kernel.util.Args;

public class JAsyncGeneratorFunc extends JGeneratorFuncBase {
  public JAsyncGeneratorFunc(PyFuncHeader header, Class<? extends JGeneratorImpl> genClass) {
    super(header, genClass);
  }

  @Override
  public IPyObj call(Args args, PyContext ctx) {
    throw new NotImplementedException("async generator func");
  }
}
