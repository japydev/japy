package org.japy.jexec.obj;

import org.japy.kernel.exec.Frame;
import org.japy.kernel.exec.IFrameObj;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.func.PyFuncHeader;
import org.japy.kernel.util.Args;

public abstract class JFrameFunc extends JFunc implements IFrameObj {
  private final int localCount;
  private final int cellCount;

  protected JFrameFunc(PyFuncHeader header, int localCount, int cellCount) {
    super(header);
    this.localCount = localCount;
    this.cellCount = cellCount;
  }

  public abstract IPyObj call(Frame frame, Args args, PyContext ctx);

  @Override
  public final IPyObj call(Args args, PyContext ctx) {
    Frame frame = ctx.thread.push(this);
    try {
      return call(frame, args, ctx);
    } finally {
      ctx.thread.pop(frame);
    }
  }

  @Override
  public int localCount() {
    return localCount;
  }

  @Override
  public int cellCount() {
    return cellCount;
  }

  @Override
  public IPyDict globals() {
    return header.globals;
  }

  @Override
  public IPyRODict builtins() {
    return header.builtins;
  }
}
