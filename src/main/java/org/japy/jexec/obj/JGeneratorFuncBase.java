package org.japy.jexec.obj;

import org.japy.kernel.types.func.PyFunc;
import org.japy.kernel.types.func.PyFuncHeader;

abstract class JGeneratorFuncBase extends PyFunc {
  protected JGeneratorFuncBase(PyFuncHeader header, Class<? extends JGeneratorImpl> genClass) {
    super(header);
  }
}
