package org.japy.jexec.pkg;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.japy.compiler.code.CBlock;
import org.japy.infra.validation.Debug;
import org.japy.jexec.codegen.ClassDumper;
import org.japy.jexec.codegen.ClassValidator;
import org.japy.jexec.codegen.Codegen;

final class JValidator {
  private static Path getClassFilePath(CBlock block, String ext) {
    return Paths.get(dcheckNotNull(block.debugPath)).resolve(block.binName + ext);
  }

  static void validate(Codegen.ClassData cd, CBlock block) {
    if (Debug.ENABLED) {
      ClassDumper.writeClassFile(cd.code, getClassFilePath(block, ".class"));
    }
    try {
      ClassValidator.validate(cd.code, cd.className, JValidator.class.getClassLoader());
    } catch (Throwable ex) {
      ClassDumper.writeClassCode(cd.code, getClassFilePath(block, ".java.txt"));
      throw ex;
    }
  }
}
