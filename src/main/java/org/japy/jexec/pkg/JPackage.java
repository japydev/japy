package org.japy.jexec.pkg;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.util.HashMap;
import java.util.Map;

import org.checkerframework.checker.nullness.qual.Nullable;

import org.japy.compiler.code.CBlock;
import org.japy.compiler.code.CPackage;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.validation.Debug;
import org.japy.jexec.codegen.Codegen;
import org.japy.jexec.codegen.CodegenConfig;
import org.japy.jexec.obj.IJGenObj;
import org.japy.kernel.types.IPyObj;

public final class JPackage extends ClassLoader {
  private static class BlockData {
    private @Nullable CBlock block;
    private volatile @Nullable String className;

    private BlockData(CBlock block) {
      this.block = block;
    }
  }

  private final Map<String, BlockData> binNameToData = new HashMap<>();
  private final Map<String, byte[]> classNameToCode = new HashMap<>();
  private final IPyObj[] constants;
  private final CodegenConfig codegenConfig;

  public JPackage(CPackage pkg, CodegenConfig codegenConfig) {
    this.codegenConfig = codegenConfig;
    constants = pkg.constants;
    binNameToData.put(pkg.module.binName, new BlockData(pkg.module));
    for (CBlock child : pkg.children) {
      if (Debug.ENABLED) {
        dcheck(!binNameToData.containsKey(child.binName));
      }
      binNameToData.put(child.binName, new BlockData(child));
    }
  }

  private static JPackage get(IJGenObj obj) {
    ClassLoader cl = obj.getClass().getClassLoader();
    dcheck(cl instanceof JPackage);
    return (JPackage) cl;
  }

  public static IPyObj[] getConstants(IJGenObj obj) {
    return get(obj).constants;
  }

  public static Class<?> getBlockClass(IJGenObj obj, String binName) {
    return get(obj).getBlockClass(binName);
  }

  public Class<?> getBlockClass(String binName) {
    BlockData bd = getBlockData(binName);

    if (bd.className == null) {
      synchronized (this) {
        if (bd.className == null) {
          Codegen.ClassData cd = Codegen.generate(dcheckNotNull(bd.block), codegenConfig);

          if (Debug.ENABLED) {
            JValidator.validate(cd, bd.block);
          }

          classNameToCode.put(cd.className, cd.code);
          bd.className = cd.className;
          bd.block = null;
        }
      }
    }

    try {
      return loadClass(bd.className);
    } catch (ClassNotFoundException ex) {
      throw new InternalErrorException("could not find self", ex);
    }
  }

  @Override
  protected Class<?> findClass(String name) throws ClassNotFoundException {
    byte @Nullable [] code;
    synchronized (this) {
      code = classNameToCode.get(name);
    }
    if (code != null) {
      return defineClass(name, code, 0, code.length);
    }

    return super.findClass(name);
  }

  private BlockData getBlockData(String binName) {
    return binNameToData.computeIfAbsent(
        binName,
        k -> {
          throw InternalErrorException.notReached();
        });
  }
}
