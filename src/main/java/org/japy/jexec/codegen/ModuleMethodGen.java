package org.japy.jexec.codegen;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CLocalVar;
import org.japy.compiler.code.CModule;
import org.japy.infra.exc.InternalErrorException;

final class ModuleMethodGen extends ExecMethodGen<CModule> {
  // void exec(IPyDict namespace, IPyRODict builtins, PyContext ctx);
  private static final int INS = 1;
  private static final int IBUILTINS = 2;
  private static final int ICTX = 3;
  private static final int IFIRSTVAR = 4;

  ModuleMethodGen(MethInfo<CModule> methInfo, MethodVisitor mv, CodegenConfig config) {
    super(methInfo, mv, config);
  }

  @Override
  protected void declareMethodParameters(MethodVisitor mv, Label start, Label end) {
    mv.visitLocalVariable("ns", Constants.D_IPYDICT, null, start, end, INS);
    mv.visitLocalVariable("builtins", Constants.D_IPYRODICT, null, start, end, IBUILTINS);
    mv.visitLocalVariable("ctx", Constants.D_PYCONTEXT, null, start, end, ICTX);
  }

  @Override
  protected void pushContext() {
    mw.loadLocal(ICTX);
  }

  @Override
  protected TempHandler createTempHandler() {
    return new JvmTempHandler(this, block, IFIRSTVAR);
  }

  @Override
  protected void pushGlobals() {
    mw.loadLocal(INS);
  }

  @Override
  protected void pushBuiltins() {
    mw.loadLocal(IBUILTINS);
  }

  @Override
  protected void checkLocalOnStack(CLocalVar var) {
    throw InternalErrorException.notReached();
  }

  @Override
  protected void beginMethod() {
    super.beginMethod();

    mw.setFieldOfThis(InstanceField.JMODULEEXEC_GLOBALS, this::pushGlobals);
    mw.setFieldOfThis(InstanceField.JMODULEEXEC_BUILTINS, this::pushBuiltins);

    if (block.hasAnnotations()) {
      mw.loadLocal(INS);
      pushContext();
      mw.call(StaticMethod.JAPI_CREATEANNOTATIONDICT);
    }
  }

  @Override
  protected void pushAnnotations() {
    mw.loadLocal(INS);
    mw.getField(StaticField.PYCONSTANTS_ANNOTATIONS);
    pushContext();
    mw.call(InstanceMethod.IPYROMAPPINGPROTOCOL_GETITEM);
  }

  @Override
  protected void pushUpvalues() {
    throw InternalErrorException.notReached();
  }

  @Override
  protected void pushConstants() {
    mw.thisDot(InstanceField.JMODULEEXEC_CONSTANTS);
  }

  @Override
  protected LocalHandler createLocalHandler() {
    return new NoLocalHandler();
  }

  private static class NoLocalHandler implements LocalHandler {
    @Override
    public void init(CLocalVar var) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void load(CLocalVar var, boolean raiseIfNotBound) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void store(CLocalVar var) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void del(CLocalVar var, boolean raiseIfNotBound) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void loadCell(int index) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void begin(CLocalVar var) {
      throw InternalErrorException.notReached();
    }

    @Override
    public void finish(CLocalVar var, Label last) {
      throw InternalErrorException.notReached();
    }
  }
}
