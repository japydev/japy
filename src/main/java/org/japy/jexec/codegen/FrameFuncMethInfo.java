package org.japy.jexec.codegen;

import java.lang.invoke.MethodType;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CLocalVar;
import org.japy.compiler.code.CUpvalue;

final class FrameFuncMethInfo extends MethInfo<CFunc> {
  // <init>(PyFuncHeader header)
  private static final int ICONSTR_HEADER = 1;

  public FrameFuncMethInfo(CFunc func) {
    super(Kind.FRAME_FUNC, func);
  }

  @Override
  String baseClassName() {
    return Constants.N_JFRAMEFUNC;
  }

  @Override
  CUpvalue @Nullable [] upvalues() {
    return block.upvalues;
  }

  @Override
  CLocalVar @Nullable [] locals() {
    return block.locals;
  }

  @Override
  CLocalVar @Nullable [] cells() {
    return block.cells;
  }

  @Override
  void writeInit(MethodWriter mw) {
    // <init>(PyFuncHeader header)
    //   -> JFrameFunc(PyFuncHeader header, int localCount, int cellCount)
    mw.callThisDot(
        InstanceMethod.JFRAMEFUNC_INIT,
        () -> {
          mw.loadLocal(ICONSTR_HEADER);
          mw.push(block.locals.length);
          mw.push(block.cells.length);
        });
  }

  @Override
  void declareConstructorParameters(MethodVisitor mv, Label start, Label end) {
    mv.visitLocalVariable("header", Constants.D_PYFUNCHEADER, null, start, end, ICONSTR_HEADER);
  }

  @Override
  String initDescriptor() {
    return Constants.MD_GEN_FRAMEFUNC_INIT;
  }

  @Override
  String methodName() {
    return Constants.GEN_FRAMEFUNC_METHOD;
  }

  @Override
  MethodType methodType() {
    return Constants.MT_GEN_FRAMEFUNC_METHOD;
  }

  @Override
  MethodGen<?> getMethodGen(MethodVisitor mv, CodegenConfig config) {
    return new FrameFuncMethodGen(this, mv, config);
  }
}
