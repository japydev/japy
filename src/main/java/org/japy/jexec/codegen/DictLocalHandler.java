package org.japy.jexec.codegen;

import org.objectweb.asm.Label;

import org.japy.compiler.code.CLocalVar;
import org.japy.infra.exc.InternalErrorException;

final class DictLocalHandler implements LocalHandler {
  private final MethodGen<?> gen;
  private final MethodWriter mw;
  private final int dictIndex;

  DictLocalHandler(MethodGen<?> gen, int dictIndex) {
    this.gen = gen;
    this.mw = gen.mw;
    this.dictIndex = dictIndex;
  }

  @Override
  public void init(CLocalVar var) {}

  private void loadDict() {
    mw.loadLocal(dictIndex);
  }

  @Override
  public void load(CLocalVar var, boolean raiseIfNotBound) {
    loadDict();
    mw.push(var.name);
    gen.pushContext();
    if (raiseIfNotBound) {
      mw.call(StaticMethod.JAPI_GETLOCAL);
    } else {
      mw.call(InstanceMethod.IPYDICT_GETORNULL_S);
    }
  }

  @Override
  public void store(CLocalVar var) {
    loadDict();
    mw.swap();
    mw.push(var.name);
    mw.swap();
    gen.pushContext();
    mw.call(InstanceMethod.IPYDICT_SET_S);
  }

  @Override
  public void del(CLocalVar var, boolean raiseIfNotBound) {
    loadDict();
    if (raiseIfNotBound) {
      mw.push(var.name);
      gen.pushContext();
      mw.call(StaticMethod.JAPI_DELLOCAL);
    } else {
      mw.push(var.name);
      mw.call(InstanceMethod.IPYDICT_REMOVE_S);
      mw.pop();
    }
  }

  @Override
  public void loadCell(int index) {
    throw InternalErrorException.notReached();
  }

  @Override
  public void begin(CLocalVar var) {}

  @Override
  public void finish(CLocalVar var, Label last) {}
}
