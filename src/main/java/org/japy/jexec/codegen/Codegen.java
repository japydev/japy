package org.japy.jexec.codegen;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.util.function.Function;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.util.CheckClassAdapter;

import org.japy.compiler.code.CBlock;
import org.japy.compiler.code.CClass;
import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CLocalVar;
import org.japy.compiler.code.CModule;
import org.japy.compiler.code.CUpvalue;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.validation.Debug;

public final class Codegen implements Opcodes {
  private final MethInfo<?> methInfo;
  final CodegenConfig config;

  private final ClassWriter classWriter;
  private final ClassVisitor cv;

  @SuppressWarnings({"ConstantConditions", "assignment"})
  MethodWriter mw = null;

  @SuppressWarnings({"ConstantConditions", "assignment"})
  MethodGen<?> methodGen = null;

  private @Nullable Label lStart;

  private static final int STATIC_FIELD_FLAGS = ACC_PUBLIC | ACC_STATIC | ACC_FINAL;

  public static class ClassData {
    public final String className;
    public final byte[] code;

    public ClassData(String className, byte[] code) {
      this.className = className;
      this.code = code;
    }
  }

  private Codegen(CBlock block, CodegenConfig config) {
    this.config = config;
    this.methInfo = getMethInfo(block, config);

    classWriter = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
    if (Debug.ENABLED) {
      // checkDataFlow=false because it's not compatible with COMPUTE_FRAMES.
      // Data flow is checked afterwards in validate().
      cv = new CheckClassAdapter(classWriter, false);
    } else {
      cv = classWriter;
    }

    if (Debug.ENABLED) {
      dcheck(getObjKind(block, config) == methInfo.kind);
    }
  }

  static MethInfo.Kind getObjKind(CBlock block, CodegenConfig config) {
    if (block instanceof CModule) {
      return MethInfo.Kind.MODULE;
    } else if (block instanceof CClass) {
      return MethInfo.Kind.CLASS;
    }

    CFunc func = (CFunc) block;
    if (func.isAsync() || func.isGenerator()) {
      return MethInfo.Kind.GENERATOR;
    }
    if (config.sneakyLocals() || func.usesLocals()) {
      return MethInfo.Kind.FRAME_FUNC;
    }
    return MethInfo.Kind.NATIVE_FUNC;
  }

  private static MethInfo<?> getMethInfo(CBlock block, CodegenConfig config) {
    switch (getObjKind(block, config)) {
      case MODULE:
        return new ModuleMethInfo((CModule) block);
      case CLASS:
        return new ClassMethInfo((CClass) block);
      case NATIVE_FUNC:
        return new NativeFuncMethInfo((CFunc) block);
      case FRAME_FUNC:
        return new FrameFuncMethInfo((CFunc) block);
      case GENERATOR:
        return new GeneratorMethInfo((CFunc) block);
    }

    throw InternalErrorException.notReached();
  }

  public static ClassData generate(CBlock block, CodegenConfig config) {
    return new Codegen(block, config).generate();
  }

  private ClassData generate() {
    beginClass();
    writeFunc();
    writeStaticConstructor();
    writeConstructor();
    finishClass();

    return new ClassData(methInfo.internalClassName.replace('/', '.'), classWriter.toByteArray());
  }

  private <T> void setNamesField(T[] things, Function<T, String> getName, String fieldName) {
    if (things.length == 0) {
      mw.pushNull();
    } else {
      mw.pushStringArray(things.length, i -> getName.apply(things[i]));
    }
    mw.visitFieldInsn(PUTSTATIC, methInfo.internalClassName, fieldName, Constants.D_ASTRING);
  }

  private void declareNamesField(String fieldName) {
    cv.visitField(STATIC_FIELD_FLAGS, fieldName, Constants.D_ASTRING, null, null);
  }

  private void beginClass() {
    cv.visit(
        Constants.JAVA_VERSION,
        ACC_PUBLIC | ACC_FINAL | ACC_SUPER,
        methInfo.internalClassName,
        null,
        methInfo.baseClassName(),
        null);
    cv.visitSource(methInfo.block.sourceFile, null);

    cv.visitField(STATIC_FIELD_FLAGS, Constants.FIELD_SOURCE_FILE, Constants.D_STRING, null, null);
    cv.visitField(STATIC_FIELD_FLAGS, Constants.FIELD_SOURCE_LINE, Constants.D_INT, null, null);

    if (methInfo.upvalues() != null) {
      declareNamesField(Constants.FIELD_UPVALUE_NAMES);
    }

    if (methInfo.cells() != null) {
      declareNamesField(Constants.FIELD_CELL_NAMES);
    }

    if (methInfo.locals() != null) {
      declareNamesField(Constants.FIELD_LOCAL_NAMES);
    }

    methInfo.declareFields(cv);
  }

  private void finishClass() {
    cv.visitEnd();
  }

  @SuppressWarnings({"ConstantConditions", "assignment"})
  private void finishMethod() {
    mw.visitMaxs(0, 0);
    mw.visitEnd();
    mw = null;
    methodGen = null;
    lStart = null;
  }

  private void writeStaticConstructor() {
    @SuppressWarnings("argument")
    MethodVisitor mv =
        cv.visitMethod(ACC_PUBLIC | ACC_STATIC, "<clinit>", Constants.MD_CLINIT, null, null);
    mw = new MethodWriter(mv, config);
    mw.visitCode();

    mw.push(methInfo.block.sourceFile);
    mw.visitFieldInsn(
        PUTSTATIC, methInfo.internalClassName, Constants.FIELD_SOURCE_FILE, Constants.D_STRING);

    mw.push(methInfo.block.sourceLine);
    mw.visitFieldInsn(
        PUTSTATIC, methInfo.internalClassName, Constants.FIELD_SOURCE_LINE, Constants.D_INT);

    CUpvalue @Nullable [] upvalues = methInfo.upvalues();
    if (upvalues != null) {
      setNamesField(upvalues, u -> u.name, Constants.FIELD_UPVALUE_NAMES);
    }

    CLocalVar @Nullable [] locals = methInfo.locals();
    if (locals != null) {
      setNamesField(locals, l -> l.name, Constants.FIELD_LOCAL_NAMES);
    }

    CLocalVar @Nullable [] cells = methInfo.cells();
    if (cells != null) {
      setNamesField(cells, l -> l.name, Constants.FIELD_CELL_NAMES);
    }

    mw.returnVoid();

    finishMethod();
  }

  @SuppressWarnings("argument")
  private void writeConstructor() {
    MethodVisitor mv = cv.visitMethod(ACC_PUBLIC, "<init>", methInfo.initDescriptor(), null, null);
    mw = new MethodWriter(mv, config);
    mw.visitCode();

    if (config.debugInfo()) {
      lStart = mw.mark();
    }

    methInfo.writeInit(mw);

    mw.returnVoid();

    if (config.debugInfo()) {
      Label lEnd = mw.mark();
      methInfo.declareConstructorParameters(mv, lStart, lEnd);
    }

    finishMethod();
  }

  private void writeFunc() {
    beginMethod();
    Label last = methodGen.writeBody();
    endMethod(last);
  }

  private void beginMethod() {
    //noinspection ConstantConditions
    dcheck(mw == null && methodGen == null);

    @SuppressWarnings("argument")
    MethodVisitor mv =
        cv.visitMethod(
            ACC_PUBLIC,
            methInfo.methodName(),
            methInfo.methodType().toMethodDescriptorString(),
            null,
            null);

    methodGen = methInfo.getMethodGen(mv, config);
    mw = methodGen.mw;
    mv.visitCode();

    if (config.debugInfo()) {
      lStart = mw.mark();
    }

    methodGen.beginMethod();
  }

  @SuppressWarnings("argument")
  private void endMethod(Label last) {
    methodGen.endMethod(last);

    if (config.debugInfo()) {
      methodGen.declareMethodParameters(mw, dcheckNotNull(lStart), last);
    }

    finishMethod();
  }
}
