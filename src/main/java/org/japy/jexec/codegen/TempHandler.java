package org.japy.jexec.codegen;

import org.japy.compiler.code.CTempVar;

interface TempHandler {
  void loadTempCell(int index);

  void beginTemp(CTempVar var);

  void endTemp(CTempVar var);

  void initTemp(CTempVar var);

  void store(CTempVar var);

  void del(CTempVar var);

  void load(CTempVar var);
}
