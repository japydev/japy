package org.japy.jexec.codegen;

import static org.japy.infra.util.ExcUtil.wrap;
import static org.japy.infra.validation.Debug.dcheck;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigInteger;

import org.objectweb.asm.Type;

import org.japy.base.AugOp;
import org.japy.jexec.JAPI;
import org.japy.jexec.obj.IJGenObj;
import org.japy.jexec.obj.JClassExec;
import org.japy.kernel.exec.Cell;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.misc.Arithmetics;
import org.japy.kernel.misc.Comparisons;
import org.japy.kernel.misc.SysLib;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.CollLib;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.iter.IterLib;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.set.PySet;
import org.japy.kernel.types.coll.str.ObjFormatLib;
import org.japy.kernel.types.coll.str.PrintLib;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.ExcLib;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.func.FuncLib;
import org.japy.kernel.types.func.PyFuncHeader;
import org.japy.kernel.types.misc.PySlice;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.num.PyComplex;
import org.japy.kernel.types.num.PyFloat;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.types.obj.ObjLib;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;
import org.japy.kernel.util.Signature;
import org.japy.lib.imp.ImportLib;

public enum StaticMethod {
  // spotless:off

  ARGPARSER_MAKE(wrap(() -> ArgParser.class.getMethod("make", IPyObj[].class, Signature.class))),
  ARGPARSER_VERIFYNOARGS(wrap(() -> ArgParser.class.getMethod("verifyNoArgs", Args.class, String.class, PyContext.class))),

  ARGS_BUILDER(wrap(() -> Args.class.getMethod("builder", int.class, int.class, PyContext.class))),
  ARGS_OF_0(wrap(() -> Args.class.getMethod("of"))),
  ARGS_OF_1(wrap(() -> Args.class.getMethod("of", IPyObj.class))),
  ARGS_OF_2(wrap(() -> Args.class.getMethod("of", IPyObj.class, IPyObj.class))),

  ARITHMETICS_ADD(wrap(() -> Arithmetics.class.getMethod("add", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_SUB(wrap(() -> Arithmetics.class.getMethod("sub", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_MUL(wrap(() -> Arithmetics.class.getMethod("mul", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_DIV(wrap(() -> Arithmetics.class.getMethod("truediv", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_FLOORDIV(wrap(() -> Arithmetics.class.getMethod("floordiv", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_DIVMOD(wrap(() -> Arithmetics.class.getMethod("divmod", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_MOD(wrap(() -> Arithmetics.class.getMethod("mod", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_POW(wrap(() -> Arithmetics.class.getMethod("pow", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_AND(wrap(() -> Arithmetics.class.getMethod("and", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_OR(wrap(() -> Arithmetics.class.getMethod("or", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_XOR(wrap(() -> Arithmetics.class.getMethod("xor", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_LSHIFT(wrap(() -> Arithmetics.class.getMethod("lshift", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_RSHIFT(wrap(() -> Arithmetics.class.getMethod("rshift", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_MATMUL(wrap(() -> Arithmetics.class.getMethod("matmul", IPyObj.class, IPyObj.class, PyContext.class))),

  ARITHMETICS_IADD(wrap(() -> Arithmetics.class.getMethod("iadd", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_ISUB(wrap(() -> Arithmetics.class.getMethod("isub", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_IMUL(wrap(() -> Arithmetics.class.getMethod("imul", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_IDIV(wrap(() -> Arithmetics.class.getMethod("itruediv", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_IFLOORDIV(wrap(() -> Arithmetics.class.getMethod("ifloordiv", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_IMOD(wrap(() -> Arithmetics.class.getMethod("imod", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_IPOW(wrap(() -> Arithmetics.class.getMethod("ipow", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_IBAND(wrap(() -> Arithmetics.class.getMethod("iand", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_IBOR(wrap(() -> Arithmetics.class.getMethod("ior", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_IXOR(wrap(() -> Arithmetics.class.getMethod("ixor", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_ILSHIFT(wrap(() -> Arithmetics.class.getMethod("ilshift", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_IRSHIFT(wrap(() -> Arithmetics.class.getMethod("irshift", IPyObj.class, IPyObj.class, PyContext.class))),
  ARITHMETICS_IMATMUL(wrap(() -> Arithmetics.class.getMethod("imatmul", IPyObj.class, IPyObj.class, PyContext.class))),

  ARITHMETICS_POS(wrap(() -> Arithmetics.class.getMethod("pos", IPyObj.class, PyContext.class))),
  ARITHMETICS_NEG(wrap(() -> Arithmetics.class.getMethod("neg", IPyObj.class, PyContext.class))),
  ARITHMETICS_INVERT(wrap(() -> Arithmetics.class.getMethod("invert", IPyObj.class, PyContext.class))),
  ARITHMETICS_ABS(wrap(() -> Arithmetics.class.getMethod("abs", IPyObj.class, PyContext.class))),

  BOOLEAN_VALUEOF(wrap(() -> Boolean.class.getMethod("valueOf", boolean.class))),

  COLLLIB_DELITEM(wrap(() -> CollLib.class.getMethod("delItem", IPyObj.class, IPyObj.class, PyContext.class))),
  COLLLIB_GETITEM(wrap(() -> CollLib.class.getMethod("getItem", IPyObj.class, IPyObj.class, PyContext.class))),
  COLLLIB_IN(wrap(() -> CollLib.class.getMethod("in", IPyObj.class, IPyObj.class, PyContext.class))),
  COLLLIB_LEN(wrap(() -> CollLib.class.getMethod("len", IPyObj.class, PyContext.class))),
  COLLLIB_NOTIN(wrap(() -> CollLib.class.getMethod("notIn", IPyObj.class, IPyObj.class, PyContext.class))),
  COLLLIB_SETITEM(wrap(() -> CollLib.class.getMethod("setItem", IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class))),

  COMPARISONS_EQ(wrap(() -> Comparisons.class.getMethod("eq", IPyObj.class, IPyObj.class, PyContext.class))),
  COMPARISONS_NE(wrap(() -> Comparisons.class.getMethod("ne", IPyObj.class, IPyObj.class, PyContext.class))),
  COMPARISONS_LT(wrap(() -> Comparisons.class.getMethod("lt", IPyObj.class, IPyObj.class, PyContext.class))),
  COMPARISONS_LE(wrap(() -> Comparisons.class.getMethod("le", IPyObj.class, IPyObj.class, PyContext.class))),
  COMPARISONS_GT(wrap(() -> Comparisons.class.getMethod("gt", IPyObj.class, IPyObj.class, PyContext.class))),
  COMPARISONS_GE(wrap(() -> Comparisons.class.getMethod("ge", IPyObj.class, IPyObj.class, PyContext.class))),

  DOUBLE_VALUEOF(wrap(() -> Double.class.getMethod("valueOf", double.class))),

  EXCLIB_CAPTUREEXC(wrap(() -> ExcLib.class.getMethod("captureExc", Throwable.class, PyContext.class))),
  EXCLIB_RAISE(wrap(() -> ExcLib.class.getMethod("raise", PyContext.class))),
  EXCLIB_RAISE_EX(wrap(() -> ExcLib.class.getMethod("raise", IPyObj.class, PyContext.class))),
  EXCLIB_RAISE_FROM(wrap(() -> ExcLib.class.getMethod("raise", IPyObj.class, IPyObj.class, PyContext.class))),
  EXCLIB_SETCURRENTEXC(wrap(() -> ExcLib.class.getMethod("setCurrentExc", PyBaseException.class, PyContext.class))),

  FUNCLIB_CALL0(wrap(() -> FuncLib.class.getMethod("call", IPyObj.class, PyContext.class))),
  FUNCLIB_CALL1(wrap(() -> FuncLib.class.getMethod("call", IPyObj.class, IPyObj.class, PyContext.class))),
  FUNCLIB_CALL2(wrap(() -> FuncLib.class.getMethod("call", IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class))),
  FUNCLIB_CALL3(wrap(() -> FuncLib.class.getMethod("call", IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class))),
  FUNCLIB_CALL_ARGS(wrap(() -> FuncLib.class.getMethod("call", IPyObj.class, Args.class, PyContext.class))),

  IMPORTLIB_IMPORTSTAR(wrap(() -> ImportLib.class.getMethod("importStar", IPyObj.class, IPyDict.class, PyContext.class))),

  ITERLIB_ITER(wrap(() -> IterLib.class.getMethod("iter", IPyObj.class, PyContext.class))),
  ITERLIB_MAKEFORITER(wrap(() -> IterLib.class.getMethod("makeForIter", IPyObj.class, PyContext.class))),

  INTEGER_VALUEOF(wrap(() -> Integer.class.getMethod("valueOf", int.class))),

  JAPI_ASSERTIONFAILED(wrap(() -> JAPI.class.getMethod("assertionFailed", IPyObj.class, PyContext.class))),
  JAPI_ASSERTIONFAILEDMSG(wrap(() -> JAPI.class.getMethod("assertionFailed", IPyObj.class, IPyObj.class, PyContext.class))),
  JAPI_AUGASSIGN_ATTR(wrap(() -> JAPI.class.getMethod("augAssignAttr", IPyObj.class, String.class, IPyObj.class, AugOp.class, PyContext.class))),
  JAPI_AUGASSIGN_INDEX(wrap(() -> JAPI.class.getMethod("augAssignIndex", IPyObj.class, IPyObj.class, IPyObj.class, AugOp.class, PyContext.class))),
  JAPI_DEBUG(wrap(() -> JAPI.class.getMethod("__debug__", PyContext.class))),
  JAPI_DELGLOBAL(wrap(() -> JAPI.class.getMethod("delGlobal", IPyDict.class, String.class, PyContext.class))),
  JAPI_DELLOCAL(wrap(() -> JAPI.class.getMethod("delLocal", IPyDict.class, String.class, PyContext.class))),
  JAPI_DICTADD(wrap(() -> JAPI.class.getMethod("dictAdd", PyDict.class, IPyObj.class, IPyObj.class, PyContext.class))),
  JAPI_DICTEXTEND(wrap(() -> JAPI.class.getMethod("dictExtend", PyDict.class, IPyObj.class, PyContext.class))),
  JAPI_EXCMATCH(wrap(() -> JAPI.class.getMethod("excMatch", PyBaseException.class, IPyObj.class, PyContext.class))),
  JAPI_GETGLOBAL(wrap(() -> JAPI.class.getMethod("getGlobal", String.class, IPyRODict.class, IPyRODict.class, PyContext.class))),
  JAPI_GETCLASSLOCAL(wrap(() -> JAPI.class.getMethod("getClassLocal", IPyObj.class, String.class, IPyRODict.class, IPyRODict.class, PyContext.class))),
  JAPI_GETLOCAL(wrap(() -> JAPI.class.getMethod("getLocal", IPyDict.class, String.class, PyContext.class))),
  JAPI_IMPORT(wrap(() -> JAPI.class.getMethod("import_", IPyObj.class, IPyObj.class, IPyObj.class, IPyDict.class, PyContext.class))),
  JAPI_LISTEXTEND(wrap(() -> JAPI.class.getMethod("listExtend", PyList.class, IPyObj.class, PyContext.class))),
  JAPI_MAKEFUNC(wrap(() -> JAPI.class.getMethod("makeFunc", PyFuncHeader.class, String.class, IJGenObj.class))),
  JAPI_OOPS(wrap(() -> JAPI.class.getMethod("oops", String.class))),
  JAPI_SETADD(wrap(() -> JAPI.class.getMethod("setAdd", PySet.class, IPyObj.class, PyContext.class))),
  JAPI_SETEXTEND(wrap(() -> JAPI.class.getMethod("setExtend", PySet.class, IPyObj.class, PyContext.class))),
  JAPI_SLICE(wrap(() -> JAPI.class.getMethod("slice", IPyObj.class, IPyObj.class, IPyObj.class))),
  JAPI_UNBOUNDCELL(wrap(() -> JAPI.class.getMethod("unboundCell", IJGenObj.class, int.class, PyContext.class))),
  JAPI_UNBOUNDLOCAL(wrap(() -> JAPI.class.getMethod("unboundLocal", IJGenObj.class, int.class, PyContext.class))),
  JAPI_UNPACKATLEAST(wrap(() -> JAPI.class.getMethod("unpackAtLeast", IPyObj.class, int.class, PyContext.class))),
  JAPI_UNPACKEXACT(wrap(() -> JAPI.class.getMethod("unpackExact", IPyObj.class, int.class, PyContext.class))),
  JAPI_GETMODULENAME(wrap(() -> JAPI.class.getMethod("getModuleName", IPyRODict.class, PyContext.class))),
  JAPI_GETQUALNAME(wrap(() -> JAPI.class.getMethod("getQualName", String.class, IPyRODict.class, PyContext.class))),
  JAPI_CREATEANNOTATIONDICT(wrap(() -> JAPI.class.getMethod("createAnnotationDict", IPyDict.class, PyContext.class))),
  JAPI_DICT_1(wrap(() -> JAPI.class.getMethod("dict", IPyObj.class, IPyObj.class, PyContext.class))),
  JAPI_DICT_2(wrap(() -> JAPI.class.getMethod("dict", IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class))),
  JAPI_DICT_3(wrap(() -> JAPI.class.getMethod("dict", IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class))),
  JAPI_DICT_4(wrap(() -> JAPI.class.getMethod("dict", IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class))),

  JCLASSEXEC_MAKE(wrap(() -> JClassExec.class.getMethod("make", IJGenObj.class, String.class, Cell[].class))),

  LONG_VALUEOF(wrap(() -> Long.class.getMethod("valueOf", long.class))),

  OBJFORMATLIB_FORMAT(wrap(() -> ObjFormatLib.class.getMethod("format", IPyObj.class, PyStr.class, PyContext.class))),

  OBJLIB_ISTRUE(wrap(() -> ObjLib.class.getMethod("isTrue", IPyObj.class, PyContext.class))),
  OBJLIB_NOT(wrap(() -> ObjLib.class.getMethod("not", IPyObj.class, PyContext.class))),
  OBJLIB_GETATTR(wrap(() -> ObjLib.class.getMethod("getAttr", IPyObj.class, String.class, PyContext.class))),
  OBJLIB_SETATTR(wrap(() -> ObjLib.class.getMethod("setAttr", IPyObj.class, String.class, IPyObj.class, PyContext.class))),
  OBJLIB_DELATTR(wrap(() -> ObjLib.class.getMethod("delAttr", IPyObj.class, String.class, PyContext.class))),

  PYBOOL_OF(wrap(() -> PyBool.class.getMethod("of", boolean.class))),

  PYBYTES_FROMASCII(wrap(() -> PyBytes.class.getMethod("fromAscii", String.class))),

  PYCOMPLEX_VALUEOF(wrap(() -> PyComplex.class.getMethod("valueOf", double.class, double.class))),

  PYFLOAT_OF(wrap(() -> PyFloat.class.getMethod("of", double.class))),

  PYFUNCHEADER_BUILDER(wrap(() -> PyFuncHeader.class.getMethod("builder"))),

  PYINT_GET_I(wrap(() -> PyInt.class.getMethod("get", int.class))),
  PYINT_GETLONGUNCHECKED(wrap(() -> PyInt.class.getMethod("getLongUnchecked", long.class))),
  PYINT_GETBIGUNCHECKED(wrap(() -> PyInt.class.getMethod("getBigUnchecked", BigInteger.class))),

  PYLIST_OF0(wrap(() -> PyList.class.getMethod("of"))),
  PYLIST_OF1(wrap(() -> PyList.class.getMethod("of", IPyObj.class))),
  PYLIST_OF2(wrap(() -> PyList.class.getMethod("of", IPyObj.class, IPyObj.class))),
  PYLIST_OF3(wrap(() -> PyList.class.getMethod("of", IPyObj.class, IPyObj.class, IPyObj.class))),
  PYLIST_OF4(wrap(() -> PyList.class.getMethod("of", IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class))),

  PYSET_OF_0(wrap(() -> PySet.class.getMethod("of"))),
  PYSET_OF_1(wrap(() -> PySet.class.getMethod("of", IPyObj.class, PyContext.class))),
  PYSET_OF_2(wrap(() -> PySet.class.getMethod("of", IPyObj.class, IPyObj.class, PyContext.class))),
  PYSET_OF_3(wrap(() -> PySet.class.getMethod("of", IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class))),
  PYSET_OF_4(wrap(() -> PySet.class.getMethod("of", IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class, PyContext.class))),

  PYSLICE_OF(wrap(() -> PySlice.class.getMethod("of", IPyObj.class, IPyObj.class, IPyObj.class))),

  PYSTR_GET(wrap(() -> PyStr.class.getMethod("get", String.class))),
  PYSTR_UCS2(wrap(() -> PyStr.class.getMethod("ucs2", String.class))),
  PYSTR_UTF32(wrap(() -> PyStr.class.getMethod("utf32", String.class))),

  PYTUPLE_OF1(wrap(() -> PyTuple.class.getMethod("of", IPyObj.class))),
  PYTUPLE_OF2(wrap(() -> PyTuple.class.getMethod("of", IPyObj.class, IPyObj.class))),
  PYTUPLE_OF3(wrap(() -> PyTuple.class.getMethod("of", IPyObj.class, IPyObj.class, IPyObj.class))),
  PYTUPLE_OF4(wrap(() -> PyTuple.class.getMethod("of", IPyObj.class, IPyObj.class, IPyObj.class, IPyObj.class))),
  PYTUPLE_OF(wrap(() -> PyTuple.class.getMethod("of", IPyObj[].class))),
  PYTUPLE_BUILDER(wrap(() -> PyTuple.class.getMethod("builder", int.class))),

  SIGNATURE_MAKE(wrap(() -> Signature.class.getMethod("make", String[].class, int[].class))),

  STRLIB_STR(wrap(() -> PrintLib.class.getMethod("str", IPyObj.class, PyContext.class))),
  STRLIB_REPR(wrap(() -> PrintLib.class.getMethod("repr", IPyObj.class, PyContext.class))),
  STRLIB_ASCII(wrap(() -> PrintLib.class.getMethod("ascii", IPyObj.class, PyContext.class))),

  SYSLIB_EXCINFO(wrap(() -> SysLib.class.getMethod("excInfo", PyContext.class))),

  ;
  // spotless:on

  public final String internalOwnerName;
  public final String name;
  public final String descriptor;
  public final int paramCount;

  StaticMethod(Method method) {
    dcheck(Modifier.isStatic(method.getModifiers()));
    this.internalOwnerName = Type.getInternalName(method.getDeclaringClass());
    this.name = method.getName();
    this.descriptor = Type.getMethodDescriptor(method);
    this.paramCount = method.getParameterCount();
  }
}
