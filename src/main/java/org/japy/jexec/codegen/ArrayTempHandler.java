package org.japy.jexec.codegen;

import org.objectweb.asm.Opcodes;

import org.japy.compiler.code.CTempVar;
import org.japy.compiler.code.CVarType;
import org.japy.compiler.impl.VarTypeInfo;

final class ArrayTempHandler implements TempHandler, Opcodes {
  private final MethodWriter mw;
  private final Runnable pushTempArray;

  ArrayTempHandler(MethodWriter mw, Runnable pushTempArray) {
    this.mw = mw;
    this.pushTempArray = pushTempArray;
  }

  @Override
  public void loadTempCell(int index) {
    mw.aaload(pushTempArray, index);
    mw.checkCast(Constants.T_CELL);
  }

  @Override
  public void beginTemp(CTempVar var) {
    if (var.isCell) {
      mw.aastore(pushTempArray, var.index, () -> mw.createObj(InstanceMethod.CELL_INIT_NULL));
    }
  }

  @Override
  public void endTemp(CTempVar var) {}

  @Override
  public void initTemp(CTempVar var) {
    VarTypeInfo ti = VarTypeInfo.get(var.type);
    if (ti.isPrimitive()) {
      pushTempArray.run();
      mw.push(var.index);
      switch (ti.primitiveType()) {
        case INT:
        case BOOL:
          mw.push(0);
          break;
      }
      mw.call(ti.primitiveType().box);
      mw.visitInsn(AASTORE);
    }
  }

  @Override
  public void store(CTempVar var) {
    if (var.isCell) {
      mw.setFieldFromStack(() -> loadTempCell(var.index), InstanceField.CELL_VALUE);
    } else {
      pushTempArray.run();
      mw.swap();
      mw.push(var.index);
      mw.swap();
      VarTypeInfo ti = VarTypeInfo.get(var.type);
      if (ti.isPrimitive()) {
        mw.call(ti.primitiveType().box);
      }
      mw.visitInsn(AASTORE);
    }
  }

  @Override
  public void del(CTempVar var) {
    mw.aastoreNull(pushTempArray, var.index);
  }

  private void castObject(CTempVar var) {
    VarTypeInfo ti = VarTypeInfo.get(var.type);
    if (ti.isPrimitive()) {
      mw.checkCast(ti.primitiveType().boxType);
      mw.call(ti.primitiveType().unbox);
    } else {
      mw.checkCast(ti.type);
    }
  }

  private void castPyObject(CTempVar var) {
    if (var.type != CVarType.PYOBJ) {
      castObject(var);
    }
  }

  @Override
  public void load(CTempVar var) {
    if (var.isCell) {
      mw.dot(() -> loadTempCell(var.index), InstanceField.CELL_VALUE);
      castPyObject(var);
    } else {
      mw.aaload(pushTempArray, var.index);
      castObject(var);
    }
  }
}
