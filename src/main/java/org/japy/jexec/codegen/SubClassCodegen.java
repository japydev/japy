package org.japy.jexec.codegen;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.annotations.Protocol;
import org.japy.kernel.types.annotations.ProtocolMethod;
import org.japy.kernel.types.cls.ext.ExtClassLib;
import org.japy.kernel.types.cls.ext.adapters.IPyFuncAdapter;
import org.japy.kernel.types.coll.IPyContains;
import org.japy.kernel.types.coll.dict.IPyMappingProtocol;
import org.japy.kernel.types.coll.dict.IPyROMappingProtocol;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.func.IPyFunc;
import org.japy.kernel.types.obj.proto.IPyIntHashableObj;
import org.japy.kernel.types.obj.proto.IPyObjWithAscii;
import org.japy.kernel.types.obj.proto.IPyObjWithDeepRepr;
import org.japy.kernel.types.obj.proto.IPyObjWithLen;
import org.japy.kernel.types.obj.proto.IPyObjWithRepr;
import org.japy.kernel.types.obj.proto.IPyObjWithStr;
import org.japy.kernel.types.obj.proto.IPyObjWithTruth;

public class SubClassCodegen implements Opcodes {
  public abstract static class Adapter {
    final Class<?> protocolInterface;
    final Class<?> adapterInterface;
    final String adapterInterfaceName;

    protected Adapter(Class<?> protocolInterface) {
      this.protocolInterface = protocolInterface;

      @Nullable Protocol pAnn = protocolInterface.getAnnotation(Protocol.class);
      this.adapterInterface = dcheckNotNull(pAnn).value();
      this.adapterInterfaceName = Type.getInternalName(adapterInterface);

      if (Debug.ENABLED) {
        dcheck(IPyObj.class.isAssignableFrom(protocolInterface));
        @Nullable Protocol pann = protocolInterface.getAnnotation(Protocol.class);
        dcheckNotNull(pann);
        dcheck(pann.value() == adapterInterface);
        org.japy.kernel.types.annotations.@Nullable Adapter aann =
            adapterInterface.getAnnotation(org.japy.kernel.types.annotations.Adapter.class);
        dcheckNotNull(aann);
        dcheck(aann.value() == protocolInterface);
      }
    }

    public abstract void implement(ClassGenHelper cgh);
  }

  private final List<Adapter> adapters = new ArrayList<>();

  public SubClassCodegen() {
    adapters.add(new SimpleAdapter(IPyObjWithTruth.class));
    adapters.add(new SimpleAdapter(IPyObjWithLen.class));
    adapters.add(new SimpleAdapter(IPyIntHashableObj.class));
    adapters.add(new SimpleAdapter(IPyObjWithStr.class));
    adapters.add(new SimpleAdapter(IPyObjWithRepr.class));
    adapters.add(new SimpleAdapter(IPyObjWithDeepRepr.class));
    adapters.add(new SimpleAdapter(IPyObjWithAscii.class));
    adapters.add(new IPyFuncAdapterImpl());
    adapters.add(new SimpleAdapter(IPyContains.class));
    adapters.add(new SimpleAdapter(IPyROMappingProtocol.class));
    adapters.add(new SimpleAdapter(IPyMappingProtocol.class));
  }

  public byte[] generate(Class<?> baseClass, String className, boolean needClassParam) {
    return new ClassGen(baseClass, className, needClassParam).generate();
  }

  private void applyAdapterImplementers(Class<?> baseClass, Consumer<Adapter> func) {
    for (Adapter impl : adapters) {
      if (impl.protocolInterface.isAssignableFrom(baseClass)) {
        func.accept(impl);
      }
    }
    if (Debug.ENABLED) {
      checkAllProtocolsAreCovered(baseClass);
    }
  }

  private static void collectInterfaces(Class<?> klass, Set<Class<?>> interfaces) {
    for (Class<?> iface : klass.getInterfaces()) {
      if (IPyObj.class.isAssignableFrom(iface)) {
        interfaces.add(iface);
        collectInterfaces(iface, interfaces);
      }
    }
  }

  private void checkAllProtocolsAreCovered(Class<?> klass) {
    Set<Class<?>> interfaces = new HashSet<>();
    collectInterfaces(klass, interfaces);
    for (Class<?> iface : interfaces) {
      @Nullable Protocol ann = iface.getAnnotation(Protocol.class);
      if (ann != null) {
        if (!ExtClassLib.PROTOCOLS.contains(iface)) {
          throw new InternalErrorException(
              String.format("protocol %s is not in the ExtClassLib.PROTOCOLS list", iface));
        }
        if (adapters.stream().noneMatch(a -> a.protocolInterface == iface)) {
          throw new InternalErrorException(
              String.format(
                  "protocol %s is implemented by %s, but there is no adapter for it",
                  iface, klass));
        }
      }
    }
  }

  private class ClassGen {
    // public class PyFooBar$Ext extends PyFooBar implements IPyBuiltinSubClassInstance {
    //   private final PyExtBuiltinSubClass type;
    //   private final PyInstanceDict dict;
    //
    //   public PyFooBar$Ext(PyExtBuiltinSubClass type, Args args, PyContext ctx) {
    //     super([type.builtinAncestor()], args, ctx);
    //     this.type = type;
    //     this.dict = new PyInstanceDict();
    //   }

    private final Class<?> baseClass;
    private final boolean needClassParam;
    private final String internalClassName;
    private final ClassGenHelper cgh;

    private ClassGen(Class<?> baseClass, String className, boolean needClassParam) {
      this.baseClass = baseClass;
      this.needClassParam = needClassParam;
      this.internalClassName = className.replace(".", "/");

      List<String> interfaces = new ArrayList<>();
      applyAdapterImplementers(baseClass, impl -> interfaces.add(impl.adapterInterfaceName));

      this.cgh =
          new ClassGenHelper(internalClassName, baseClass, interfaces.toArray(String[]::new));
    }

    private byte[] generate() {
      beginClass();

      writeConstructor();
      writeType();
      writeInstanceDict();

      applyAdapterImplementers(baseClass, impl -> impl.implement(cgh));

      byte[] classFile = cgh.finishClass();

      if (Debug.ENABLED) {
        validate(classFile);
      }

      return classFile;
    }

    private void beginClass() {
      cgh.cv.visitField(
          ACC_PRIVATE | ACC_FINAL,
          Constants.SUBCLASS_FIELD_TYPE,
          Constants.D_PYEXTBUILTINSUBCLASS,
          null,
          null);
      cgh.cv.visitField(
          ACC_PRIVATE | ACC_FINAL,
          Constants.SUBCLASS_FIELD_DICT,
          Constants.D_PYINSTANCEDICT,
          null,
          null);
    }

    // public PyFooBar$Ext(PyExtBuiltinSubClass type, Args args, PyContext ctx)
    private void writeConstructor() {
      MethodWriter mw = cgh.beginMethod("<init>", Constants.MD_SUBCLASS_INIT);

      Label lStart = mw.mark();

      int itype = 1;
      int iargs = 2;
      int ictx = 3;

      // super(type, args, ctx);
      //  or
      // super(args, ctx);
      mw.pushThis();
      if (needClassParam) {
        mw.loadLocal(itype);
      }
      mw.loadLocal(iargs);
      mw.loadLocal(ictx);
      mw.visitMethodInsn(
          INVOKESPECIAL,
          cgh.baseClassName,
          "<init>",
          needClassParam
              ? Constants.MD_BUILTIN_INIT_WITH_CLASS
              : Constants.MD_BUILTIN_INIT_NO_CLASS,
          false);

      // this.type = type;
      mw.pushThis();
      mw.loadLocal(itype);
      mw.visitFieldInsn(
          PUTFIELD,
          internalClassName,
          Constants.SUBCLASS_FIELD_TYPE,
          Constants.D_PYEXTBUILTINSUBCLASS);

      // this.dict = new PyInstanceDict();
      mw.pushThis();
      mw.createObj(InstanceMethod.PYINSTANCEDICT_INIT);
      mw.visitFieldInsn(
          PUTFIELD, internalClassName, Constants.SUBCLASS_FIELD_DICT, Constants.D_PYINSTANCEDICT);

      mw.returnVoid();

      Label lEnd = mw.mark();
      mw.visitLocalVariable("type", Constants.D_PYEXTBUILTINSUBCLASS, null, lStart, lEnd, itype);
      mw.visitLocalVariable("args", Constants.D_ARGS, null, lStart, lEnd, iargs);
      mw.visitLocalVariable("ctx", Constants.D_PYCONTEXT, null, lStart, lEnd, ictx);

      cgh.finishMethod(mw);
    }

    // public IPyClass type() {
    //   return type;
    // }
    private void writeType() {
      MethodWriter mw = cgh.beginMethod("type", Constants.MD_IPYOBJ_TYPE);

      mw.pushThis();
      mw.visitFieldInsn(
          GETFIELD,
          cgh.thisClassName,
          Constants.SUBCLASS_FIELD_TYPE,
          Constants.D_PYEXTBUILTINSUBCLASS);
      mw.returnTop();

      cgh.finishMethod(mw);
    }

    // public IPyInstanceDict instanceDict() {
    //   return dict;
    // }
    private void writeInstanceDict() {
      MethodWriter mw = cgh.beginMethod("instanceDict", Constants.MD_IPYOBJ_INSTANCEDICT);

      mw.pushThis();
      mw.visitFieldInsn(
          GETFIELD, cgh.thisClassName, Constants.SUBCLASS_FIELD_DICT, Constants.D_PYINSTANCEDICT);
      mw.returnTop();

      cgh.finishMethod(mw);
    }

    private void validate(byte[] code) {
      ClassDumper.writeClassFile(code, getDumpFile(".class"));

      try {
        ClassValidator.validate(code, internalClassName, getClass().getClassLoader());
      } catch (Throwable ex) {
        ClassDumper.writeClassCode(code, getDumpFile(".txt"));
        throw ex;
      }
    }

    private Path getDumpFile(String ext) {
      Path destDir = Paths.get("build/jc/ext/");
      if (!destDir.toFile().exists() && !destDir.toFile().mkdirs()) {
        throw new InternalErrorException("failed to create output dir " + destDir);
      }
      return destDir.resolve(baseClass.getSimpleName() + ext);
    }
  }

  private static String getDescriptor(Class<?> retType, int objArgCount) {
    if (retType == IPyObj.class) {
      switch (objArgCount) {
        case 0:
          return Constants.MD_OBJ_CTX;
        case 1:
          return Constants.MD_OBJ_OBJ_CTX;
        case 2:
          return Constants.MD_OBJ_OBJ2_CTX;
        case 3:
          return Constants.MD_OBJ_OBJ3_CTX;
        default:
          throw new InternalErrorException("too many arguments");
      }
    }

    if (retType == PyStr.class) {
      switch (objArgCount) {
        case 0:
          return Constants.MD_PYSTR_CTX;
        case 1:
          return Constants.MD_PYSTR_OBJ_CTX;
        case 2:
          return Constants.MD_PYSTR_OBJ2_CTX;
        case 3:
          return Constants.MD_PYSTR_OBJ3_CTX;
        default:
          throw new InternalErrorException("too many arguments");
      }
    }

    if (retType == void.class) {
      switch (objArgCount) {
        case 0:
          return Constants.MD_VOID_CTX;
        case 1:
          return Constants.MD_VOID_OBJ_CTX;
        case 2:
          return Constants.MD_VOID_OBJ2_CTX;
        case 3:
          return Constants.MD_VOID_OBJ3_CTX;
        default:
          throw new InternalErrorException("too many arguments");
      }
    }

    if (retType == boolean.class) {
      switch (objArgCount) {
        case 0:
          return Constants.MD_BOOL_CTX;
        case 1:
          return Constants.MD_BOOL_OBJ_CTX;
        case 2:
          return Constants.MD_BOOL_OBJ2_CTX;
        case 3:
          return Constants.MD_BOOL_OBJ3_CTX;
        default:
          throw new InternalErrorException("too many arguments");
      }
    }

    if (retType == int.class) {
      switch (objArgCount) {
        case 0:
          return Constants.MD_INT_CTX;
        case 1:
          return Constants.MD_INT_OBJ_CTX;
        case 2:
          return Constants.MD_INT_OBJ2_CTX;
        case 3:
          return Constants.MD_INT_OBJ3_CTX;
        default:
          throw new InternalErrorException("too many arguments");
      }
    }

    throw new InternalErrorException("unhandled return type " + retType);
  }

  private static void writeRedirect(
      ClassGenHelper cgh,
      Class<?> retType,
      int objArgCount,
      String name,
      BiConsumer<MethodWriter, String> callMethod) {
    String descriptor = getDescriptor(retType, objArgCount);

    MethodWriter mw = cgh.beginMethod(name, descriptor);

    Label lStart = mw.mark();

    mw.pushThis();
    for (int i = 0; i < objArgCount; ++i) {
      mw.loadLocal(i + 1);
    }
    mw.loadLocal(objArgCount + 1);

    callMethod.accept(mw, descriptor);

    if (!retType.isPrimitive()) {
      mw.visitInsn(ARETURN);
    } else if (retType == boolean.class || retType == int.class) {
      mw.visitInsn(IRETURN);
    } else if (retType == void.class) {
      mw.visitInsn(RETURN);
    } else {
      throw new InternalErrorException("unhandled return type " + retType);
    }

    Label lEnd = mw.mark();
    for (int i = 0; i < objArgCount; ++i) {
      mw.visitLocalVariable("arg" + (i + 1), Constants.D_IPYOBJ, null, lStart, lEnd, i + 1);
    }
    mw.visitLocalVariable("ctx", Constants.D_PYCONTEXT, null, lStart, lEnd, objArgCount + 1);

    cgh.finishMethod(mw);
  }

  // <rettype> <name>(IPyObj arg1, ..., IPyObj argN, PyContext ctx) {
  //   return <thisName>(arg1, ..., argN, ctx);
  // }
  private static void writeThis(
      ClassGenHelper cgh,
      Class<?> retType,
      int objArgCount,
      String name,
      String thisName,
      Class<?> adapterInterface) {
    writeRedirect(
        cgh,
        retType,
        objArgCount,
        name,
        (mw, descriptor) ->
            mw.visitMethodInsn(
                INVOKEINTERFACE,
                Type.getInternalName(adapterInterface),
                thisName,
                descriptor,
                true));
  }

  // <rettype> <baseName>(IPyObj arg1, ..., IPyObj argN, PyContext ctx) {
  //   return super.<name>(arg1, ..., argN, ctx);
  // }
  private static void writeBase(
      ClassGenHelper cgh, Class<?> retType, int objArgCount, String name, String baseName) {
    writeRedirect(
        cgh,
        retType,
        objArgCount,
        baseName,
        (mw, descriptor) ->
            mw.visitMethodInsn(INVOKESPECIAL, cgh.baseClassName, name, descriptor, false));
  }

  private static class SimpleAdapter extends Adapter {
    private static class MethodInfo {
      private final String name;
      private final Class<?> retType;
      private final int objArgCount;

      private MethodInfo(String name, Class<?> retType, int objArgCount) {
        this.retType = retType;
        this.objArgCount = objArgCount;
        this.name = name;
      }

      String thisName() {
        return "this" + name.substring(0, 1).toUpperCase() + name.substring(1);
      }

      String baseName() {
        return "base" + name.substring(0, 1).toUpperCase() + name.substring(1);
      }
    }

    private final List<MethodInfo> methods = new ArrayList<>();

    SimpleAdapter(Class<?> protocolInterface) {
      super(protocolInterface);

      for (Method method : protocolInterface.getDeclaredMethods()) {
        @Nullable ProtocolMethod pmAnn = method.getAnnotation(ProtocolMethod.class);
        if (pmAnn != null) {
          methods.add(
              new MethodInfo(
                  method.getName(), method.getReturnType(), method.getParameterCount() - 1));
        }
      }

      if (Debug.ENABLED) {
        if (methods.isEmpty()) {
          throw new InternalErrorException(
              "no ProtocolMethod-annotated methods in protocol class " + protocolInterface);
        }
        for (MethodInfo m : methods) {
          validate(m);
        }
      }
    }

    private void validate(MethodInfo m) {
      Class<?>[] ptypes = new Class<?>[m.objArgCount + 1];
      for (int i = 0; i != m.objArgCount; ++i) {
        ptypes[i] = IPyObj.class;
      }
      ptypes[m.objArgCount] = PyContext.class;
      validateProtocolMethod(m.name, ptypes, m.retType);
      validateAdapterMethod(m.thisName(), ptypes, m.retType);
      validateAdapterMethod(m.baseName(), ptypes, m.retType);
    }

    private void validateProtocolMethod(String name, Class<?>[] ptypes, Class<?> retType) {
      try {
        Method method = protocolInterface.getDeclaredMethod(name, ptypes);
        if (method.getReturnType() != retType) {
          throw new InternalErrorException(
              String.format(
                  "adapter method %s.%s has unexpected return type %s, expected %s",
                  method.getDeclaringClass(), method.getName(), method.getReturnType(), retType));
        }
      } catch (NoSuchMethodException ex) {
        throw new InternalErrorException(
            String.format("no method '%s' in protocol interface %s", name, protocolInterface), ex);
      }
    }

    private void validateAdapterMethod(String name, Class<?>[] ptypes, Class<?> retType) {
      try {
        Method method = adapterInterface.getDeclaredMethod(name, ptypes);
        if (method.getReturnType() != retType) {
          throw new InternalErrorException(
              String.format(
                  "adapter method %s.%s has unexpected return type %s, expected %s",
                  method.getDeclaringClass(), method.getName(), method.getReturnType(), retType));
        }
      } catch (NoSuchMethodException ex) {
        throw new InternalErrorException(
            String.format("no method '%s' in adapter interface %s", name, protocolInterface), ex);
      }
    }

    @Override
    public void implement(ClassGenHelper cgh) {
      for (MethodInfo m : methods) {
        writeThis(cgh, m.retType, m.objArgCount, m.name, m.thisName(), adapterInterface);
        writeBase(cgh, m.retType, m.objArgCount, m.name, m.baseName());
      }
    }
  }

  private static class IPyFuncAdapterImpl extends Adapter {
    IPyFuncAdapterImpl() {
      super(IPyFunc.class);
    }

    @Override
    public void implement(ClassGenHelper cgh) {
      writeCall(cgh);
      writeBaseCall(cgh);
    }

    // IPyObj call(Args args, PyContext ctx) {
    //   return thisCall(ctx);
    // }
    private static void writeCall(ClassGenHelper cgh) {
      MethodWriter mw = cgh.beginMethod("call", Constants.MD_IPYCALLABLE_CALL);

      int iargs = 1;
      int ictx = 2;
      Label lStart = mw.mark();

      mw.pushThis();
      mw.loadLocal(iargs);
      mw.loadLocal(ictx);
      mw.visitMethodInsn(
          INVOKEINTERFACE,
          Type.getInternalName(IPyFuncAdapter.class),
          "thisCall",
          Constants.MD_IPYCALLABLE_CALL,
          true);
      mw.visitInsn(ARETURN);

      Label lEnd = mw.mark();
      mw.visitLocalVariable("args", Constants.D_ARGS, null, lStart, lEnd, ictx);
      mw.visitLocalVariable("ctx", Constants.D_PYCONTEXT, null, lStart, lEnd, ictx);

      cgh.finishMethod(mw);
    }

    // IPyObj baseCall(Args args, PyContext ctx) {
    //   return super.call(args, ctx);
    // }
    private static void writeBaseCall(ClassGenHelper cgh) {
      MethodWriter mw = cgh.beginMethod("baseCall", Constants.MD_IPYCALLABLE_CALL);

      int iargs = 1;
      int ictx = 2;
      Label lStart = mw.mark();

      mw.pushThis();
      mw.loadLocal(iargs);
      mw.loadLocal(ictx);
      mw.visitMethodInsn(
          INVOKESPECIAL, cgh.baseClassName, "call", Constants.MD_IPYCALLABLE_CALL, false);
      mw.visitInsn(ARETURN);

      Label lEnd = mw.mark();
      mw.visitLocalVariable("args", Constants.D_ARGS, null, lStart, lEnd, ictx);
      mw.visitLocalVariable("ctx", Constants.D_PYCONTEXT, null, lStart, lEnd, ictx);

      cgh.finishMethod(mw);
    }
  }
}
