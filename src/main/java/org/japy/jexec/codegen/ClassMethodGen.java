package org.japy.jexec.codegen;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CClass;
import org.japy.compiler.code.CLocalVar;
import org.japy.infra.exc.InternalErrorException;

final class ClassMethodGen extends ExecMethodGen<CClass> {
  // void exec(IPyDict dict, IPyDict globals, IPyRODict builtins, Cell classCell,
  // PyContext ctx)
  private static final int IDICT = 1;
  private static final int IGLOBALS = 2;
  private static final int IBUILTINS = 3;
  private static final int ICLASSCELL = 4;
  private static final int ICTX = 5;
  private static final int IFIRSTVAR = 6;

  ClassMethodGen(MethInfo<CClass> methInfo, MethodVisitor mv, CodegenConfig config) {
    super(methInfo, mv, config);
  }

  @Override
  protected void declareMethodParameters(MethodVisitor mv, Label start, Label end) {
    mv.visitLocalVariable("dict", Constants.D_IPYDICT, null, start, end, IDICT);
    mv.visitLocalVariable("globals", Constants.D_IPYDICT, null, start, end, IGLOBALS);
    mv.visitLocalVariable("builtins", Constants.D_IPYDICT, null, start, end, IBUILTINS);
    mv.visitLocalVariable("classCell", Constants.D_CELL, null, start, end, ICLASSCELL);
    mv.visitLocalVariable("ctx", Constants.D_PYCONTEXT, null, start, end, ICTX);
  }

  @Override
  protected void pushContext() {
    mw.loadLocal(ICTX);
  }

  @Override
  protected void checkLocalOnStack(CLocalVar var) {
    throw InternalErrorException.notReached();
  }

  @Override
  protected void loadClassCell() {
    mw.loadLocal(ICLASSCELL);
  }

  @Override
  protected LocalHandler createLocalHandler() {
    return new DictLocalHandler(this, IDICT);
  }

  @Override
  protected TempHandler createTempHandler() {
    return new JvmTempHandler(this, block, IFIRSTVAR);
  }

  @Override
  protected void beginMethod() {
    super.beginMethod();

    mw.setFieldOfThis(InstanceField.JCLASSEXEC_GLOBALS, this::pushGlobals);
    mw.setFieldOfThis(InstanceField.JCLASSEXEC_BUILTINS, this::pushBuiltins);

    mw.loadLocal(IDICT);
    mw.push(org.japy.kernel.util.Constants.__MODULE__);
    pushGlobals();
    pushContext();
    mw.call(StaticMethod.JAPI_GETMODULENAME);
    pushContext();
    mw.call(InstanceMethod.IPYDICT_SET_S);

    if (block.hasAnnotations()) {
      mw.loadLocal(IDICT);
      pushContext();
      mw.call(StaticMethod.JAPI_CREATEANNOTATIONDICT);
    }
  }

  @Override
  protected void pushAnnotations() {
    mw.loadLocal(IDICT);
    mw.getField(StaticField.PYCONSTANTS_ANNOTATIONS);
    pushContext();
    mw.call(InstanceMethod.IPYROMAPPINGPROTOCOL_GETITEM);
  }

  @Override
  protected void pushUpvalues() {
    mw.thisDot(InstanceField.JCLASSEXEC_UPVALUES);
  }

  @Override
  protected void pushConstants() {
    mw.thisDot(InstanceField.JCLASSEXEC_CONSTANTS);
  }

  @Override
  protected void pushGlobals() {
    mw.loadLocal(IGLOBALS);
  }

  @Override
  protected void pushBuiltins() {
    mw.loadLocal(IBUILTINS);
  }
}
