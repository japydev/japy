package org.japy.jexec.codegen;

import java.lang.invoke.MethodType;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CClass;
import org.japy.compiler.code.CUpvalue;

final class ClassMethInfo extends MethInfo<CClass> {
  // <>(Cell[] upvalues)
  private static final int ICONSTR_UPVALUES = 1;

  public ClassMethInfo(CClass cls) {
    super(Kind.CLASS, cls);
  }

  @Override
  String baseClassName() {
    return Constants.N_JCLASSEXEC;
  }

  @Override
  CUpvalue @Nullable [] upvalues() {
    return block.upvalues;
  }

  @Override
  void writeInit(MethodWriter mw) {
    // <>(Cell[] upvalues)
    // -> JClassExec(Cell[] upvalues)
    mw.callThisDot(InstanceMethod.JCLASSEXEC_INIT, () -> mw.loadLocal(ICONSTR_UPVALUES));
  }

  @Override
  void declareConstructorParameters(MethodVisitor mv, Label start, Label end) {
    mv.visitLocalVariable("upvalues", Constants.D_ACELL, null, start, end, ICONSTR_UPVALUES);
  }

  @Override
  String initDescriptor() {
    return Constants.MD_GEN_CLASSEXEC_INIT;
  }

  @Override
  String methodName() {
    return Constants.GEN_CLASSEXEC_METHOD;
  }

  @Override
  MethodType methodType() {
    return Constants.MT_GEN_CLASSEXEC_METHOD;
  }

  @Override
  ClassMethodGen getMethodGen(MethodVisitor mv, CodegenConfig config) {
    return new ClassMethodGen(this, mv, config);
  }
}
