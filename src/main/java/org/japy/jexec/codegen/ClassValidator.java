package org.japy.jexec.codegen;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.tree.analysis.BasicValue;
import org.objectweb.asm.tree.analysis.SimpleVerifier;
import org.objectweb.asm.util.CheckClassAdapter;

import org.japy.infra.validation.ValidationError;

public class ClassValidator {
  public static void validate(byte[] code, String className, ClassLoader classLoader) {
    ClassReader classReader = new ClassReader(code);
    ClassNode classNode = new ClassNode();

    try {
      classReader.accept(new CheckClassAdapter(classNode, true), 0);
    } catch (RuntimeException ex) {
      throw new ValidationError("error verifying class " + className + ": " + ex.getMessage(), ex);
    }

    Type syperType = Type.getObjectType(classNode.superName);
    List<MethodNode> methods = classNode.methods;

    List<Type> interfaces = new ArrayList<>();
    for (String interfaceName : classNode.interfaces) {
      interfaces.add(Type.getObjectType(interfaceName));
    }

    for (MethodNode method : methods) {
      SimpleVerifier verifier =
          new SimpleVerifier(
              Type.getObjectType(classNode.name),
              syperType,
              interfaces,
              (classNode.access & Opcodes.ACC_INTERFACE) != 0);
      Analyzer<BasicValue> analyzer = new Analyzer<>(verifier);
      verifier.setClassLoader(classLoader);
      try {
        analyzer.analyze(classNode.name, method);
      } catch (AnalyzerException e) {
        throw new ValidationError(
            "error verifying method " + className + "." + method.name + ": " + e.getMessage(), e);
      }
    }
  }
}
