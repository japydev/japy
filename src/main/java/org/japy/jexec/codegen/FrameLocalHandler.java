package org.japy.jexec.codegen;

import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;

import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CLocalVar;

final class FrameLocalHandler implements LocalHandler, Opcodes {
  private final MethodGen<CFunc> gen;
  private final MethodWriter mw;
  private final Runnable loadFrame;

  FrameLocalHandler(MethodGen<CFunc> gen, Runnable loadFrame) {
    this.gen = gen;
    this.mw = gen.mw;
    this.loadFrame = loadFrame;
  }

  private void loadCell(CLocalVar var) {
    loadCell(var.index);
  }

  @Override
  public void load(CLocalVar var, boolean raiseIfNotBound) {
    if (var.isCell) {
      mw.dot(() -> loadCell(var), InstanceField.CELL_VALUE);
    } else {
      loadFrame.run();
      mw.push(var.index);
      mw.call(InstanceMethod.FRAME_GETLOCAL);
    }
    if (raiseIfNotBound) {
      gen.checkLocalOnStackIfNeeded(var);
    }
  }

  @Override
  public void store(CLocalVar var) {
    if (var.isCell) {
      mw.setFieldFromStack(() -> loadCell(var), InstanceField.CELL_VALUE);
    } else {
      loadFrame.run();
      mw.swap();
      mw.push(var.index);
      mw.swap();
      mw.call(InstanceMethod.FRAME_SETLOCAL);
    }
  }

  @Override
  public void del(CLocalVar var, boolean raiseIfNotBound) {
    if (var.isCell) {
      loadCell(var);
      if (raiseIfNotBound) {
        mw.dot(mw::dup, InstanceField.CELL_VALUE);
        gen.popAndCheckBoundLocal(var);
      }
      mw.putFieldNull(InstanceField.CELL_VALUE);
    } else {
      if (raiseIfNotBound) {
        loadFrame.run();
        mw.push(var.index);
        mw.call(InstanceMethod.FRAME_GETLOCAL);
        gen.popAndCheckBoundLocal(var);
      }
      loadFrame.run();
      mw.push(var.index);
      mw.pushNull();
      mw.call(InstanceMethod.FRAME_SETLOCAL);
    }
  }

  @Override
  public void loadCell(int index) {
    loadFrame.run();
    mw.push(index);
    mw.call(InstanceMethod.FRAME_GETCELL);
  }

  @Override
  public void init(CLocalVar var) {}

  @Override
  public void begin(CLocalVar var) {}

  @Override
  public void finish(CLocalVar var, Label last) {}
}
