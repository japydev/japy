package org.japy.jexec.codegen;

import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CBlock;

public abstract class ExecMethodGen<T extends CBlock> extends MethodGen<T> {
  protected ExecMethodGen(MethInfo<T> methInfo, MethodVisitor mv, CodegenConfig config) {
    super(methInfo, mv, config);
  }
}
