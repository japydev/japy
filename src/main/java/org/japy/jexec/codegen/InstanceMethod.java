package org.japy.jexec.codegen;

import static org.japy.infra.util.ExcUtil.wrap;
import static org.japy.infra.validation.Debug.dcheck;

import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigInteger;

import org.objectweb.asm.Type;

import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.exc.NotImplementedException;
import org.japy.infra.validation.Debug;
import org.japy.jexec.obj.JClassExec;
import org.japy.jexec.obj.JFrameFunc;
import org.japy.jexec.obj.JFunc;
import org.japy.jexec.obj.JGeneratorImpl;
import org.japy.jexec.obj.JModuleExec;
import org.japy.jexec.obj.JNativeFunc;
import org.japy.kernel.exec.Cell;
import org.japy.kernel.exec.Frame;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.ext.IPyClassExec;
import org.japy.kernel.types.cls.ext.PyClassBuilder;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.dict.IPyROMappingProtocol;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.iter.IPyForIter;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.set.PySet;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.coll.str.PyStrBuilder;
import org.japy.kernel.types.exc.px.PxUnboundLocalError;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.func.PyFuncHeader;
import org.japy.kernel.types.gen.IPyGeneratorSupport;
import org.japy.kernel.types.misc.PySlice;
import org.japy.kernel.types.obj.PyInstanceDict;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.Args;

@SuppressWarnings("ImmutableEnumChecker")
public enum InstanceMethod {
  // spotless:off

  ARGPARSER_PARSE(wrap(() -> ArgParser.class.getMethod("parse", Args.class, String.class, PyContext.class))),
  ARGPARSER_PARSE1(wrap(() -> ArgParser.class.getMethod("parse1", Args.class, String.class, PyContext.class))),

  ARGS_BUILDER_ADD_KW(wrap(() -> Args.Builder.class.getMethod("add", String.class, IPyObj.class))),
  ARGS_BUILDER_ADD_POS(wrap(() -> Args.Builder.class.getMethod("add", IPyObj.class))),
  ARGS_BUILDER_BUILD(wrap(() -> Args.Builder.class.getMethod("build"))),
  ARGS_BUILDER_UNPACK_AND_ADD_KW(wrap(() -> Args.Builder.class.getMethod("unpackAndAddKw", IPyObj.class, PyContext.class))),
  ARGS_BUILDER_UNPACK_AND_ADD_POS(wrap(() -> Args.Builder.class.getMethod("unpackAndAddPos", IPyObj.class, PyContext.class))),

  BIGINTEGER_INIT_SI(wrap(() -> BigInteger.class.getConstructor(String.class, int.class))),

  BOOLEAN_BOOLEANVALUE(wrap(() -> Boolean.class.getMethod("booleanValue"))),

  CELL_INIT_NULL(wrap(() -> Cell.class.getConstructor())),

  FRAME_GETLOCAL(wrap(() -> Frame.class.getMethod("getLocal", int.class))),
  FRAME_SETLOCAL(wrap(() -> Frame.class.getMethod("setLocal", int.class, IPyObj.class))),
  FRAME_GETCELL(wrap(() -> Frame.class.getMethod("getCell", int.class))),

  INTEGER_INTVALUE(wrap(() -> Integer.class.getMethod("intValue"))),

  INTERNALERROR_INIT_S(wrap(() -> InternalErrorException.class.getConstructor(String.class))),

  IPYDICT_SET_O(wrap(() -> IPyDict.class.getMethod("setItem", IPyObj.class, IPyObj.class, PyContext.class))),
  IPYDICT_SET_S(wrap(() -> IPyDict.class.getMethod("setItem", String.class, IPyObj.class, PyContext.class))),
  IPYDICT_REMOVE_S(wrap(() -> IPyDict.class.getMethod("popOrNull", String.class, PyContext.class))),
  IPYDICT_GETORNULL_S(wrap(() -> IPyDict.class.getMethod("getOrNull", String.class, PyContext.class))),

  IPYFORITER_NEXT(wrap(() -> IPyForIter.class.getMethod("next", PyContext.class))),

  IPYOBJ_TYPE(wrap(() -> IPyObj.class.getMethod("type"))),

  IPYROMAPPINGPROTOCOL_GETITEM(wrap(() -> IPyROMappingProtocol.class.getMethod("getItem", IPyObj.class, PyContext.class))),

  IPYGENERATORSUPPORT_YIELDFROM(wrap(() -> IPyGeneratorSupport.class.getMethod("yieldFrom", IPyObj.class, PyContext.class))),
  IPYGENERATORSUPPORT_AWAIT(wrap(() -> IPyGeneratorSupport.class.getMethod("await", IPyObj.class, PyContext.class))),
  IPYGENERATORSUPPORT_RETHROW(wrap(() -> IPyGeneratorSupport.class.getMethod("rethrow", Throwable.class, PyContext.class))),
  IPYGENERATORSUPPORT_RETURNVALUE(wrap(() -> IPyGeneratorSupport.class.getMethod("returnValue", IPyObj.class, PyContext.class))),

  JCLASSEXEC_GETUPVALUECHECKED(wrap(() -> JClassExec.class.getDeclaredMethod("uv", Cell.class, int.class, PyContext.class))),
  JCLASSEXEC_INIT(wrap(() -> JClassExec.class.getDeclaredConstructor(Cell[].class))),

  JFRAMEFUNC_INIT(wrap(() -> JFrameFunc.class.getDeclaredConstructor(PyFuncHeader.class, int.class, int.class))),

  JFUNC_GETLOCALCHECKED(wrap(() -> JFunc.class.getDeclaredMethod("lv", IPyObj.class, int.class, PyContext.class))),
  JFUNC_GETCELLCHECKED(wrap(() -> JFunc.class.getDeclaredMethod("cv", IPyObj.class, int.class, PyContext.class))),
  JFUNC_GETUPVALUECHECKED(wrap(() -> JFunc.class.getDeclaredMethod("uv", Cell.class, int.class, PyContext.class))),

  JGENERATORIMPL_GETLOCALCHECKED(wrap(() -> JGeneratorImpl.class.getDeclaredMethod("lv", IPyObj.class, int.class, PyContext.class))),
  JGENERATORIMPL_GETCELLCHECKED(wrap(() -> JGeneratorImpl.class.getDeclaredMethod("cv", IPyObj.class, int.class, PyContext.class))),
  JGENERATORIMPL_GETUPVALUECHECKED(wrap(() -> JGeneratorImpl.class.getDeclaredMethod("uv", Cell.class, int.class, PyContext.class))),
  JGENERATORIMPL_INIT(wrap(() -> JGeneratorImpl.class.getDeclaredConstructor(PyFuncHeader.class, int.class))),

  JMODULEEXEC_INIT(wrap(() -> JModuleExec.class.getDeclaredConstructor())),

  JNATIVEFUNC_INIT(wrap(() -> JNativeFunc.class.getDeclaredConstructor(PyFuncHeader.class))),

  NOTIMPLEMENTEDEXCEPTION_INIT_S(wrap(() -> NotImplementedException.class.getConstructor(String.class))),

  PXUNBOUNDLOCALERROR_INIT(wrap(() -> PxUnboundLocalError.class.getConstructor(String.class, PyContext.class))),

  PYBASEEXCEPTION_PXEXC(wrap(() -> PyBaseException.class.getMethod("pxExc"))),

  PYCLASSBUILDER_INIT(wrap(() -> PyClassBuilder.class.getConstructor())),
  PYCLASSBUILDER_BUILD(wrap(() -> PyClassBuilder.class.getMethod("build", PyContext.class))),
  PYCLASSBUILDER_ARGS(wrap(() -> PyClassBuilder.class.getMethod("args", Args.class))),
  PYCLASSBUILDER_NAME(wrap(() -> PyClassBuilder.class.getMethod("name", PyStr.class))),
  PYCLASSBUILDER_QUALNAME(wrap(() -> PyClassBuilder.class.getMethod("qualName", PyStr.class))),
  PYCLASSBUILDER_EXEC(wrap(() -> PyClassBuilder.class.getMethod("exec", IPyClassExec.class))),
  PYCLASSBUILDER_BUILTINS(wrap(() -> PyClassBuilder.class.getMethod("builtins", IPyRODict.class))),
  PYCLASSBUILDER_GLOBALS(wrap(() -> PyClassBuilder.class.getMethod("globals", IPyDict.class))),

  PYDICT_INIT_I(wrap(() -> PyDict.class.getConstructor(int.class))),
  PYDICT_INIT_V(wrap(() -> PyDict.class.getConstructor())),

  PYFUNCHEADER_BUILDER_BUILD(wrap(() -> PyFuncHeader.Builder.class.getMethod("build"))),
  PYFUNCHEADER_BUILDER_NAME(wrap(() -> PyFuncHeader.Builder.class.getMethod("name", PyStr.class))),
  PYFUNCHEADER_BUILDER_QUALNAME(wrap(() -> PyFuncHeader.Builder.class.getMethod("qualName", PyStr.class))),
  PYFUNCHEADER_BUILDER_MODULE(wrap(() -> PyFuncHeader.Builder.class.getMethod("module", PyStr.class))),
  PYFUNCHEADER_BUILDER_ANNOTATIONS(wrap(() -> PyFuncHeader.Builder.class.getMethod("annotations", IPyObj.class))),
  PYFUNCHEADER_BUILDER_ARGPARSER(wrap(() -> PyFuncHeader.Builder.class.getMethod("argParser", ArgParser.class))),
  PYFUNCHEADER_BUILDER_UPVALUES(wrap(() -> PyFuncHeader.Builder.class.getMethod("upvalues", Cell[].class))),
  PYFUNCHEADER_BUILDER_GLOBALS(wrap(() -> PyFuncHeader.Builder.class.getMethod("globals", IPyDict.class))),
  PYFUNCHEADER_BUILDER_BUILTINS(wrap(() -> PyFuncHeader.Builder.class.getMethod("builtins", IPyRODict.class))),
  PYFUNCHEADER_BUILDER_DOCSTRING(wrap(() -> PyFuncHeader.Builder.class.getMethod("docString", IPyObj.class))),
  PYFUNCHEADER_BUILDER_ISASYNC(wrap(() -> PyFuncHeader.Builder.class.getMethod("isAsync", boolean.class))),
  PYFUNCHEADER_BUILDER_ISGENERATOR(wrap(() -> PyFuncHeader.Builder.class.getMethod("isGenerator", boolean.class))),

  PYINSTANCEDICT_INIT(wrap(() -> PyInstanceDict.class.getConstructor())),

  PYLIST_APPEND(wrap(() -> PyList.class.getMethod("append", IPyObj.class))),
  PYLIST_APPENDNV(wrap(() -> PyList.class.getMethod("appendNV", IPyObj.class))),
  PYLIST_INIT_I(wrap(() -> PyList.class.getConstructor(int.class))),

  PYSET_ADD(wrap(() -> PySet.class.getMethod("add", IPyObj.class, PyContext.class))),
  PYSET_INIT_I(wrap(() -> PySet.class.getConstructor(int.class))),
  PYSET_INIT_V(wrap(() -> PySet.class.getConstructor())),

  PYSLICE_INIT(wrap(() -> PySlice.class.getConstructor(IPyObj.class, IPyObj.class, IPyObj.class))),

  PYSTRBUILDER_APPEND(wrap(() -> PyStrBuilder.class.getMethod("append", PyStr.class))),
  PYSTRBUILDER_APPENDUNKNOWN(wrap(() -> PyStrBuilder.class.getMethod("appendUnknown", String.class))),
  PYSTRBUILDER_BUILD(wrap(() -> PyStrBuilder.class.getMethod("build"))),
  PYSTRBUILDER_INIT(wrap(() -> PyStrBuilder.class.getConstructor())),

  PYTUPLE_GET(wrap(() -> PyTuple.class.getMethod("get", int.class))),

  PYTUPLE_BUILDER_ADD(wrap(() -> PyTuple.Builder.class.getMethod("add", IPyObj.class))),
  PYTUPLE_BUILDER_EXTEND(wrap(() -> PyTuple.Builder.class.getMethod("extend", IPyObj.class, PyContext.class))),
  PYTUPLE_BUILDER_BUILD(wrap(() -> PyTuple.Builder.class.getMethod("build"))),

  ;

  // spotless:on

  public final Type ownerType;

  public final String ownerInternalName;
  public final String descriptor;
  public final int paramCount;

  @SuppressWarnings("ImmutableEnumChecker")
  private final Executable method;

  private final int flags;

  private static final int IS_CONSTRUCTOR = 1;
  private static final int IS_INTERFACE = 2;

  public boolean isConstructor() {
    return (flags & IS_CONSTRUCTOR) != 0;
  }

  public boolean isInterface() {
    return (flags & IS_INTERFACE) != 0;
  }

  public String methodName() {
    return isConstructor() ? "<init>" : method.getName();
  }

  public int paramCount() {
    return method.getParameterCount();
  }

  InstanceMethod(Executable method, int flags, String descriptor) {
    this.method = method;
    this.flags = flags;
    this.descriptor = descriptor;
    this.ownerType = Type.getType(method.getDeclaringClass());
    this.ownerInternalName = ownerType.getInternalName();
    this.paramCount = method.getParameterCount();

    if (Debug.ENABLED) {
      dcheck(!isInterface() || !isConstructor());
      dcheck(!Modifier.isStatic(method.getModifiers()));
    }
  }

  InstanceMethod(Constructor<?> constructor) {
    this(constructor, IS_CONSTRUCTOR, Type.getConstructorDescriptor(constructor));
  }

  InstanceMethod(Method method) {
    this(
        method,
        method.getDeclaringClass().isInterface() ? IS_INTERFACE : 0,
        Type.getMethodDescriptor(method));
  }
}
