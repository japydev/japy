package org.japy.jexec.codegen;

import static org.objectweb.asm.Opcodes.*;

import org.objectweb.asm.Label;

import org.japy.compiler.code.CLocalVar;

final class GeneratorLocalHandler implements LocalHandler {
  private final GeneratorMethodGen gen;
  private final MethodWriter mw;

  GeneratorLocalHandler(GeneratorMethodGen gen) {
    this.gen = gen;
    this.mw = gen.mw;
  }

  private void getField(CLocalVar var) {
    mw.pushThis();
    mw.visitFieldInsn(
        GETFIELD,
        gen.methInfo.internalClassName,
        var.name,
        var.isCell ? Constants.D_CELL : Constants.D_IPYOBJ);
  }

  private void putField(CLocalVar var) {
    mw.pushThis();
    mw.swap();
    mw.visitFieldInsn(
        PUTFIELD,
        gen.methInfo.internalClassName,
        var.name,
        var.isCell ? Constants.D_CELL : Constants.D_IPYOBJ);
  }

  @Override
  public void load(CLocalVar var, boolean raiseIfNotBound) {
    getField(var);
    if (var.isCell) {
      mw.getField(InstanceField.CELL_VALUE);
    }
    if (raiseIfNotBound) {
      gen.checkLocalOnStackIfNeeded(var);
    }
  }

  @Override
  public void store(CLocalVar var) {
    if (var.isCell) {
      mw.setFieldFromStack(() -> getField(var), InstanceField.CELL_VALUE);
    } else {
      putField(var);
    }
  }

  @Override
  public void del(CLocalVar var, boolean raiseIfNotBound) {
    if (var.isCell) {
      getField(var);
      if (raiseIfNotBound) {
        mw.dot(mw::dup, InstanceField.CELL_VALUE);
        gen.popAndCheckBoundLocal(var);
      }
      mw.putFieldNull(InstanceField.CELL_VALUE);
    } else {
      if (raiseIfNotBound) {
        getField(var);
        gen.popAndCheckBoundLocal(var);
      }
      mw.pushNull();
      putField(var);
    }
  }

  @Override
  public void loadCell(int index) {
    getField(gen.block.cells[index]);
  }

  @Override
  public void init(CLocalVar var) {}

  @Override
  public void begin(CLocalVar var) {}

  @Override
  public void finish(CLocalVar var, Label last) {}
}
