package org.japy.jexec.codegen;

public final class CodegenConfig {
  public static final int LINE_NUMBERS = 1;
  public static final int DEBUG_INFO = 2;
  public static final int SNEAKY_LOCALS = 4;
  public static final int NULL_CHECKS = 8;

  public final int flags;

  public boolean lineNumbers() {
    return (flags & LINE_NUMBERS) != 0;
  }

  public boolean debugInfo() {
    return (flags & DEBUG_INFO) != 0;
  }

  public boolean sneakyLocals() {
    return (flags & SNEAKY_LOCALS) != 0;
  }

  public boolean nullChecks() {
    return (flags & NULL_CHECKS) != 0;
  }

  public static Builder builder() {
    return new Builder();
  }

  private CodegenConfig(Builder builder) {
    flags = builder.flags;
  }

  public static class Builder {
    private int flags = LINE_NUMBERS | DEBUG_INFO | SNEAKY_LOCALS | NULL_CHECKS;

    private Builder() {}

    public CodegenConfig build() {
      return new CodegenConfig(this);
    }

    private void changeFlag(int flag, boolean set) {
      if (set) {
        flags |= flag;
      } else {
        flags &= ~flag;
      }
    }

    public Builder lineNumbers(boolean lineNumbers) {
      changeFlag(LINE_NUMBERS, lineNumbers);
      return this;
    }

    public Builder debugInfo(boolean debugInfo) {
      changeFlag(DEBUG_INFO, debugInfo);
      return this;
    }

    public Builder sneakyLocals(boolean sneakyLocals) {
      changeFlag(SNEAKY_LOCALS, sneakyLocals);
      return this;
    }

    public Builder nullChecks(boolean nullChecks) {
      changeFlag(NULL_CHECKS, nullChecks);
      return this;
    }
  }
}
