package org.japy.jexec.codegen;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CLocalVar;
import org.japy.compiler.code.instr.CInstr;
import org.japy.compiler.code.instr.CInstr0;
import org.japy.infra.exc.InternalErrorException;

// IPyObj resumeImpl(IPyObj value, PyBaseException ex, PyContext ctx);
final class GeneratorMethodGen extends MethodGen<CFunc> {
  private static final int IVALUE = 1;
  private static final int IEX = 2;
  private static final int ICTX = 3;

  private enum SuspensionPoint {
    YIELD,
    YIELD_FROM,
    AWAIT,
  }

  private final Label lDispatch;
  private final Label lBodyStart;
  private final Label lBodyEnd;
  private final Label lBodyExcHandler;
  private boolean wroteBodyExcHandler;
  private boolean wroteBodyEnd;
  private final List<Label> dispatchTargets = new ArrayList<>();

  GeneratorMethodGen(GeneratorMethInfo methInfo, MethodVisitor mv, CodegenConfig config) {
    super(methInfo, mv, config);
    lDispatch = new Label();
    lBodyStart = new Label();
    lBodyEnd = new Label();
    lBodyExcHandler = new Label();
  }

  @Override
  protected LocalHandler createLocalHandler() {
    return new GeneratorLocalHandler(this);
  }

  @Override
  protected TempHandler createTempHandler() {
    return new ArrayTempHandler(mw, () -> mw.thisDot(InstanceField.JGENERATORIMPL_TEMPS));
  }

  @Override
  protected void declareMethodParameters(MethodVisitor mv, Label start, Label end) {
    mv.visitLocalVariable("value", Constants.D_IPYOBJ, null, start, end, IVALUE);
    mv.visitLocalVariable("ex", Constants.D_PYBASEEXCEPTION, null, start, end, IEX);
    mv.visitLocalVariable("ctx", Constants.D_PYCONTEXT, null, start, end, ICTX);
  }

  @Override
  protected void pushUpvalues() {
    mw.thisDot(InstanceField.GENERATORIMPL_HEADER, InstanceField.PYFUNCHEADER_UPVALUES);
  }

  @Override
  protected void pushConstants() {
    mw.thisDot(InstanceField.JGENERATORIMPL_CONSTANTS);
  }

  @Override
  protected void pushContext() {
    mw.loadLocal(ICTX);
  }

  @Override
  protected void pushGlobals() {
    mw.thisDot(InstanceField.GENERATORIMPL_HEADER, InstanceField.PYFUNCHEADER_GLOBALS);
  }

  @Override
  protected void pushBuiltins() {
    mw.thisDot(InstanceField.GENERATORIMPL_HEADER, InstanceField.PYFUNCHEADER_BUILTINS);
  }

  private void pushSupport() {
    mw.thisDot(InstanceField.GENERATORIMPL_SUPPORT);
  }

  @Override
  protected void checkLocalOnStack(CLocalVar var) {
    mw.pushThis();
    mw.swap();
    mw.push(var.index);
    pushContext();
    if (var.isCell) {
      mw.call(InstanceMethod.JGENERATORIMPL_GETCELLCHECKED);
    } else {
      mw.call(InstanceMethod.JGENERATORIMPL_GETLOCALCHECKED);
    }
  }

  @Override
  protected void beginMethod() {
    super.beginMethod();

    mw.visitTryCatchBlock(lBodyStart, lBodyEnd, lBodyExcHandler, null);

    mw.jump(lDispatch);
    dispatchTargets.add(lBodyStart);
    mw.visitLabel(lBodyStart);
  }

  private void writeBodyExcHandler() {
    dcheck(!wroteBodyExcHandler);
    wroteBodyExcHandler = true;
    if (!wroteBodyEnd) {
      wroteBodyEnd = true;
      mw.visitLabel(lBodyEnd);
    }
    mw.visitLabel(lBodyExcHandler);
    pushSupport();
    mw.swap();
    pushContext();
    mw.call(InstanceMethod.IPYGENERATORSUPPORT_RETHROW);
    mw.throw_();
  }

  private void return_() {
    dcheck(!wroteBodyEnd);
    wroteBodyEnd = true;
    mw.visitLabel(lBodyEnd);
    Label ret = new Label();
    mw.jump(ret);
    writeBodyExcHandler();
    mw.visitLabel(ret);
    pushSupport();
    mw.swap();
    pushContext();
    mw.call(InstanceMethod.IPYGENERATORSUPPORT_RETURNVALUE);
    mw.throw_();
  }

  @Override
  protected void endMethod(Label last) {
    if (!wroteBodyExcHandler) {
      writeBodyExcHandler();
    }
    dcheck(wroteBodyEnd);

    mw.visitLabel(lDispatch);
    Label lInvalid = new Label();
    mw.thisDot(InstanceField.JGENERATORIMPL_NEXTPOINT);
    mw.visitTableSwitchInsn(
        0, dispatchTargets.size() - 1, lInvalid, dispatchTargets.toArray(Label[]::new));
    mw.visitLabel(lInvalid);
    mw.throw_(InstanceMethod.INTERNALERROR_INIT_S, "invalid jump target");
    super.endMethod(last);
  }

  private void suspend(SuspensionPoint suspensionPoint, Label next) {
    if (suspensionPoint == SuspensionPoint.YIELD) {
      // `yield value` - just return it, there is nothing else to do
      mw.returnTop();
      return;
    }

    // otherwise invoke appropriate IPyGeneratorSupport method and see if we actually need to
    // suspend
    pushSupport();
    mw.swap();
    pushContext();
    switch (suspensionPoint) {
      case YIELD_FROM:
        mw.call(InstanceMethod.IPYGENERATORSUPPORT_YIELDFROM);
        break;
      case AWAIT:
        mw.call(InstanceMethod.IPYGENERATORSUPPORT_AWAIT);
        break;
        //noinspection ConstantConditions
      case YIELD:
        throw InternalErrorException.notReached();
    }

    mw.dup();
    mw.visitTypeInsn(INSTANCEOF, Constants.N_IPYGENERATORSUPPORT_CONTINUE);
    Label doContinue = new Label();
    mw.jumpIfTrue(doContinue);
    mw.returnTop();

    mw.visitLabel(doContinue);
    mw.checkCast(Constants.T_IPYGENERATORSUPPORT_CONTINUE);
    mw.getField(InstanceField.IPYGENERATORSUPPORT_CONTINUE_VALUE);
    mw.storeLocal(IVALUE);
    mw.storeLocalNull(IEX);
    mw.jump(next);
  }

  private void suspendAndResume(SuspensionPoint suspensionPoint) {
    int nextPoint = dispatchTargets.size();

    mw.setFieldOfThis(InstanceField.JGENERATORIMPL_NEXTPOINT, () -> mw.push(nextPoint));

    Label next = new Label();

    suspend(suspensionPoint, next);

    dispatchTargets.add(next);

    mw.visitLabel(next);

    mw.loadLocal(IEX);
    Label noException = new Label();
    mw.jumpIfNull(noException);
    mw.loadLocal(IEX);
    mw.call(InstanceMethod.PYBASEEXCEPTION_PXEXC);
    mw.throw_();
    mw.visitLabel(noException);

    mw.loadLocal(IVALUE);
  }

  @Override
  protected void writeInstr(CInstr instr) {
    if (instr.kind() == CInstr.Kind.INSTR0) {
      switch (((CInstr0) instr).code) {
        case YIELD:
          suspendAndResume(SuspensionPoint.YIELD);
          return;
        case YIELDFROM:
          suspendAndResume(SuspensionPoint.YIELD_FROM);
          return;
        case AWAIT:
          suspendAndResume(SuspensionPoint.AWAIT);
          return;
        case RET:
          return_();
          return;
        default:
          break;
      }
    }

    super.writeInstr(instr);
  }
}
