package org.japy.jexec.codegen;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;
import static org.japy.kernel.types.misc.PyEllipsis.Ellipsis;
import static org.japy.kernel.types.misc.PyNone.None;
import static org.japy.kernel.types.num.PyBool.False;
import static org.japy.kernel.types.num.PyBool.True;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import com.google.errorprone.annotations.Var;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import org.japy.base.AugOp;
import org.japy.base.BinOp;
import org.japy.base.CompOp;
import org.japy.base.UnaryOp;
import org.japy.compiler.code.CBlock;
import org.japy.compiler.code.CClass;
import org.japy.compiler.code.CExcHandler;
import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CLocalVar;
import org.japy.compiler.code.CRef;
import org.japy.compiler.code.CUpvalue;
import org.japy.compiler.code.instr.CInstr;
import org.japy.compiler.code.instr.CInstr0;
import org.japy.compiler.code.instr.CInstrI;
import org.japy.compiler.code.instr.CInstrI2;
import org.japy.compiler.code.instr.CInstrR;
import org.japy.compiler.code.instr.CInstrS;
import org.japy.compiler.code.instr.CJump;
import org.japy.compiler.code.instr.CLabel;
import org.japy.compiler.code.instr.CLiteral;
import org.japy.compiler.code.instr.CMakeClass;
import org.japy.compiler.code.instr.CMakeFunc;
import org.japy.compiler.code.instr.CSwitch;
import org.japy.compiler.impl.SpecialVar;
import org.japy.infra.exc.InternalErrorException;
import org.japy.infra.loc.LocationUtil;
import org.japy.infra.validation.Debug;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.seq.PyList;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.set.PySet;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.misc.PySlice;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.num.PyComplex;
import org.japy.kernel.types.num.PyFloat;
import org.japy.kernel.types.num.PyInt;

abstract class MethodGen<T extends CBlock> implements Opcodes {
  protected final T block;
  protected final MethInfo.Kind kind;
  protected @Nullable LocalHandler localHandler;
  protected @Nullable TempHandler tempHandler;
  protected final MethodWriter mw;
  protected final MethInfo<T> methInfo;
  protected final CodegenConfig config;

  private final Map<CLabel, Label> labels = new HashMap<>();
  private @Nullable Label lastInstruction;

  protected MethodGen(MethInfo<T> methInfo, MethodVisitor mv, CodegenConfig config) {
    this.block = methInfo.block;
    this.kind = methInfo.kind;
    this.methInfo = methInfo;
    this.config = config;
    this.mw = new MethodWriter(mv, config);
  }

  protected abstract LocalHandler createLocalHandler();

  protected abstract TempHandler createTempHandler();

  protected abstract void declareMethodParameters(MethodVisitor mv, Label start, Label end);

  protected abstract void pushUpvalues();

  protected abstract void pushConstants();

  protected abstract void pushContext();

  protected void parseArgs() {
    throw InternalErrorException.notReached();
  }

  protected void parseArgs0() {
    throw InternalErrorException.notReached();
  }

  protected void parseArgs1() {
    throw InternalErrorException.notReached();
  }

  protected abstract void pushGlobals();

  protected abstract void pushBuiltins();

  protected abstract void checkLocalOnStack(CLocalVar var);

  protected void pushAnnotations() {
    throw InternalErrorException.notReached();
  }

  protected void loadClassCell() {
    throw InternalErrorException.notReached();
  }

  protected LocalHandler localHandler() {
    if (localHandler == null) {
      localHandler = createLocalHandler();
    }
    return localHandler;
  }

  private TempHandler tempHandler() {
    if (tempHandler == null) {
      tempHandler = createTempHandler();
    }
    return tempHandler;
  }

  protected void beginMethod() {
    for (CExcHandler h : block.excHandlers) {
      mw.visitTryCatchBlock(
          getLabel(h.start), getLabel(h.end), getLabel(h.handler), Constants.N_THROWABLE);
    }
  }

  protected void endMethod(Label last) {
    for (CLocalVar var : block.locals) {
      localHandler().finish(var, last);
    }
  }

  protected Label writeBody() {
    @Var int executableCount = block.instructions.length;
    if (!isVoid()) {
      for (int i = block.instructions.length - 1; i >= 0; --i) {
        if (isExecutable(block.instructions[i])) {
          executableCount = i + 1;
          break;
        }
      }
    }

    for (int i = 0; i != executableCount; ++i) {
      CInstr instr = block.instructions[i];
      if (isExecutable(instr)) {
        mw.at(instr);
        if (!isVoid() && i == executableCount - 1) {
          lastInstruction = mw.mark();
        }
      }
      writeInstrWithErrorHandling(instr);
    }

    if (isVoid()) {
      lastInstruction = mw.mark();
      mw.returnVoid();
    }

    dcheckNotNull(lastInstruction);

    for (int i = executableCount; i != block.instructions.length; ++i) {
      writeInstrWithErrorHandling(block.instructions[i]);
    }

    return lastInstruction;
  }

  private void writeInstrWithErrorHandling(CInstr instr) {
    try {
      writeInstr(instr);
    } catch (Throwable ex) {
      LocationUtil.setLocationIfNotSet(ex, instr);
      throw ex;
    }
  }

  private static boolean isExecutable(CInstr instr) {
    switch (instr.kind()) {
      case INSTRR:
        {
          CInstrR ci = (CInstrR) instr;
          switch (ci.code) {
            case BEGINTEMP:
            case ENDTEMP:
              return false;
            default:
              return true;
          }
        }

      case CATCH:
        return false;

      default:
        return true;
    }
  }

  private boolean isVoid() {
    return methInfo.methodType().returnType() == void.class;
  }

  protected Label labelForLocalVar() {
    return lastInstruction != null ? lastInstruction : mw.mark();
  }

  private static boolean canBeNull(CLocalVar var) {
    return true;
  }

  void checkLocalOnStackIfNeeded(CLocalVar var) {
    if (config.nullChecks() && canBeNull(var)) {
      checkLocalOnStack(var);
    }
  }

  void popAndCheckBoundLocal(CLocalVar var) {
    Label notNull = new Label();
    mw.jumpIfNonNull(notNull);
    mw.pushThis();
    mw.push(var.index);
    pushContext();
    mw.call(var.isCell ? StaticMethod.JAPI_UNBOUNDCELL : StaticMethod.JAPI_UNBOUNDLOCAL);
    mw.throw_();
    mw.visitLabel(notNull);
  }

  private void loadUpvalueCell(int index) {
    mw.aaload(this::pushUpvalues, index);
  }

  private void loadUpvalueCell(CUpvalue upvalue) {
    loadUpvalueCell(upvalue.index);
  }

  private void loadUpvalueChecked(CUpvalue upvalue) {
    loadUpvalueCell(upvalue);

    mw.pushThis();
    mw.swap();
    mw.push(upvalue.index);
    pushContext();
    switch (kind) {
      case FRAME_FUNC:
      case NATIVE_FUNC:
        mw.call(InstanceMethod.JFUNC_GETUPVALUECHECKED);
        break;
      case GENERATOR:
        mw.call(InstanceMethod.JGENERATORIMPL_GETUPVALUECHECKED);
        break;
      case CLASS:
        mw.call(InstanceMethod.JCLASSEXEC_GETUPVALUECHECKED);
        break;
      case MODULE:
        throw InternalErrorException.notReached();
    }
  }

  private Label getLabel(CLabel cl) {
    return labels.computeIfAbsent(cl, k -> new Label());
  }

  private void pushPyConst(IPyObj value) {
    if (value == None) {
      mw.getField(StaticField.PYNONE_NONE);
    } else if (value == True) {
      mw.getField(StaticField.PYBOOL_TRUE);
    } else if (value == False) {
      mw.getField(StaticField.PYBOOL_FALSE);
    } else if (value == Ellipsis) {
      mw.getField(StaticField.PYELLIPSIS_ELLIPSIS);
    } else if (value instanceof PySlice) {
      mw.createObj(
          InstanceMethod.PYSLICE_INIT,
          () -> {
            pushPyConst(((PySlice) value).start);
            pushPyConst(((PySlice) value).stop);
            pushPyConst(((PySlice) value).step);
          });
    } else if (value instanceof PyInt) {
      pushPyConst((PyInt) value);
    } else if (value instanceof PyFloat) {
      mw.push(((PyFloat) value).value);
      mw.call(StaticMethod.PYFLOAT_OF);
    } else if (value instanceof PyComplex) {
      mw.push(((PyComplex) value).real);
      mw.push(((PyComplex) value).imag);
      mw.call(StaticMethod.PYCOMPLEX_VALUEOF);
    } else if (value instanceof PyTuple) {
      pushPyConst((PyTuple) value);
    } else if (value instanceof PyList) {
      pushPyConst((PyList) value);
    } else if (value instanceof PySet) {
      pushPyConst((PySet) value);
    } else if (value instanceof PyDict) {
      pushPyConst((PyDict) value);
    } else if (value instanceof PyStr) {
      pushPyConst((PyStr) value);
    } else if (value instanceof PyBytes) {
      pushPyConst((PyBytes) value);
    } else {
      throw new InternalErrorException("unhandled constant value type: " + value);
    }
  }

  private void pushPyConst(PyBool value) {
    mw.getField(value.value() ? StaticField.PYBOOL_TRUE : StaticField.PYBOOL_FALSE);
  }

  private void pushPyConst(PyInt value) {
    if (value.isSmallRep()) {
      int v = value.intUnchecked();
      switch (v) {
        case 0:
          mw.getField(StaticField.PYINT_ZERO);
          break;
        case 1:
          mw.getField(StaticField.PYINT_ONE);
          break;
        case -1:
          mw.getField(StaticField.PYINT_NEGATIVEONE);
          break;
        default:
          mw.push(value.intUnchecked());
          mw.call(StaticMethod.PYINT_GET_I);
          break;
      }
    } else if (value.isLongRep()) {
      mw.push(value.longUnchecked());
      mw.call(StaticMethod.PYINT_GETLONGUNCHECKED);
    } else {
      BigInteger bi = value.bigIntUnchecked();
      mw.createObj(
          InstanceMethod.BIGINTEGER_INIT_SI,
          () -> {
            mw.push(bi.toString(36));
            mw.push(36);
          });
      mw.call(StaticMethod.PYINT_GETBIGUNCHECKED);
    }
  }

  private void pushPyConst(PyStr value) {
    mw.push(value.toString());
    mw.call(value.isUcs2() ? StaticMethod.PYSTR_UCS2 : StaticMethod.PYSTR_UTF32);
  }

  private void pushPyConst(PyBytes value) {
    if (Debug.ENABLED) {
      dcheck(value.isAscii());
    }
    mw.push(value.asAsciiString());
    mw.call(StaticMethod.PYBYTES_FROMASCII);
  }

  private void pushPyConst(PyTuple value) {
    int len = value.len();
    if (len == 0) {
      mw.getField(StaticField.PYTUPLE_EMPTY);
      return;
    }
    if (len > 4) {
      throw new InternalErrorException("tuple is too big");
    }
    for (int i = 0; i < len; ++i) {
      pushPyConst(value.get(i));
    }
    makeTuple(len);
  }

  private void pushPyConst(PyList value) {
    int len = value.len();
    if (len > 4) {
      throw new InternalErrorException("list is too big");
    }
    for (int i = 0; i < len; ++i) {
      pushPyConst(value.content.get(i));
    }
    makeList(len);
  }

  private void pushPyConst(PySet value) {
    int len = value.len();
    if (len == 0) {
      mw.createObj(InstanceMethod.PYSET_INIT_V);
      return;
    }
    if (len > 4) {
      throw new InternalErrorException("set is too big");
    }
    value.forEachNoCtx(this::pushPyConst);
    pushContext();
    switch (len) {
      case 1:
        mw.call(StaticMethod.PYSET_OF_1);
        break;
      case 2:
        mw.call(StaticMethod.PYSET_OF_2);
        break;
      case 3:
        mw.call(StaticMethod.PYSET_OF_3);
        break;
      case 4:
        mw.call(StaticMethod.PYSET_OF_4);
        break;
      default:
        throw InternalErrorException.notReached();
    }
  }

  private void pushPyConst(PyDict value) {
    int len = value.len();
    if (len == 0) {
      mw.createObj(InstanceMethod.PYDICT_INIT_V);
      return;
    }
    if (len > 4) {
      throw new InternalErrorException("dict is too big");
    }
    value.forEach(
        (k, v) -> {
          pushPyConst(k);
          pushPyConst(v);
        });
    pushContext();
    switch (len) {
      case 1:
        mw.call(StaticMethod.JAPI_DICT_1);
        break;
      case 2:
        mw.call(StaticMethod.JAPI_DICT_2);
        break;
      case 3:
        mw.call(StaticMethod.JAPI_DICT_3);
        break;
      case 4:
        mw.call(StaticMethod.JAPI_DICT_4);
        break;
      default:
        throw InternalErrorException.notReached();
    }
  }

  private void loadUpvalueForChild(CUpvalue upvalue) {
    switch (upvalue.kind) {
      case UPVALUE:
        loadUpvalueCell(upvalue.parentIndex);
        break;
      case CELL:
        localHandler().loadCell(upvalue.parentIndex);
        break;
      case TEMP:
        tempHandler().loadTempCell(upvalue.parentIndex);
        break;
      case CLASS:
        loadClassCell();
        break;
    }
  }

  private void pushUpvaluesForChild(CUpvalue[] upvalues) {
    if (upvalues.length == 0) {
      mw.pushNull();
    } else {
      mw.pushArray(Constants.N_CELL, upvalues.length, i -> loadUpvalueForChild(upvalues[i]));
    }
  }

  private void pushConst(int index) {
    mw.aaload(this::pushConstants, index);
  }

  private void makeClass(CClass klass) {
    // stack has @Nullable Args

    mw.createObj(InstanceMethod.PYCLASSBUILDER_INIT);

    mw.swap();
    mw.call(InstanceMethod.PYCLASSBUILDER_ARGS);

    pushPyConst(PyStr.get(klass.name));
    mw.call(InstanceMethod.PYCLASSBUILDER_NAME);

    mw.push(klass.qualName);
    pushGlobals();
    pushContext();
    mw.call(StaticMethod.JAPI_GETQUALNAME);
    mw.call(InstanceMethod.PYCLASSBUILDER_QUALNAME);

    pushGlobals();
    mw.call(InstanceMethod.PYCLASSBUILDER_GLOBALS);
    pushBuiltins();
    mw.call(InstanceMethod.PYCLASSBUILDER_BUILTINS);

    // JClassExec make(IJGenObj self, String binName, Cell[] upvalues)
    mw.pushThis();
    mw.push(klass.binName);
    pushUpvaluesForChild(klass.upvalues);
    mw.call(StaticMethod.JCLASSEXEC_MAKE);
    mw.call(InstanceMethod.PYCLASSBUILDER_EXEC);

    pushContext();
    mw.call(InstanceMethod.PYCLASSBUILDER_BUILD);
  }

  private void pushArgParser(CFunc func) {
    // stack has IPyObj[] defaults
    if (func.signature.paramCount() == 0) {
      mw.pop();
      mw.getField(StaticField.ARGPARSER_NOPARAMS);
    } else {
      mw.push(func.signature.paramNames);
      mw.push(func.signature.paramKinds);
      mw.call(StaticMethod.SIGNATURE_MAKE);
      mw.call(StaticMethod.ARGPARSER_MAKE);
    }
  }

  private void makeFunc(CFunc func) {
    // stack has IPyObj annotations, IPyObj[] defaults
    pushArgParser(func);
    mw.call(StaticMethod.PYFUNCHEADER_BUILDER);
    mw.swap();
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_ARGPARSER);
    mw.swap();
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_ANNOTATIONS);
    pushUpvaluesForChild(func.upvalues);
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_UPVALUES);

    if (func.docString >= 0) {
      pushConst(func.docString);
    } else {
      mw.pushNull();
    }
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_DOCSTRING);

    pushPyConst(PyStr.get(func.name));
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_NAME);

    mw.push(func.qualName);
    pushGlobals();
    pushContext();
    mw.call(StaticMethod.JAPI_GETQUALNAME);
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_QUALNAME);

    pushGlobals();
    pushContext();
    mw.call(StaticMethod.JAPI_GETMODULENAME);
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_MODULE);

    pushGlobals();
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_GLOBALS);

    pushBuiltins();
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_BUILTINS);

    mw.push(func.isAsync());
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_ISASYNC);

    mw.push(func.isGenerator());
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_ISGENERATOR);

    // IPyObj makeFunc(PyFuncHeader header, String binName, IJGenObj self)
    mw.call(InstanceMethod.PYFUNCHEADER_BUILDER_BUILD);
    mw.push(func.binName);
    mw.pushThis();
    mw.call(StaticMethod.JAPI_MAKEFUNC);
  }

  private void load(CRef ref) {
    switch (ref.kind) {
      case LOCAL:
        if (block instanceof CClass) {
          // https://docs.python.org/3/reference/executionmodel.html#naming-and-binding
          // -- A class definition is an executable statement that may use and define names. These
          // -- references follow the normal rules for name resolution with an exception that
          // -- unbound local variables are looked up in the global namespace.
          localHandler().load(ref.local(), false);
          mw.push(ref.name());
          pushGlobals();
          pushBuiltins();
          pushContext();
          mw.call(StaticMethod.JAPI_GETCLASSLOCAL);
        } else {
          localHandler().load(ref.local(), true);
        }
        break;

      case TEMP:
        tempHandler().load(ref.temp());
        break;

      case GLOBAL:
        mw.push(ref.name());
        pushGlobals();
        pushBuiltins();
        pushContext();
        mw.call(StaticMethod.JAPI_GETGLOBAL);
        break;

      case UPVALUE:
        loadUpvalueChecked(ref.upvalue());
        break;
    }
  }

  private void store(CRef ref) {
    switch (ref.kind) {
      case GLOBAL:
        pushGlobals();
        mw.swap();
        mw.push(ref.name());
        mw.swap();
        pushContext();
        mw.call(InstanceMethod.IPYDICT_SET_S);
        break;
      case UPVALUE:
        mw.setFieldFromStack(() -> loadUpvalueCell(ref.upvalue()), InstanceField.CELL_VALUE);
        break;
      case LOCAL:
        localHandler().store(ref.local());
        break;
      case TEMP:
        tempHandler().store(ref.temp());
        break;
    }
  }

  private void del(CRef ref, boolean raiseIfNotBound) {
    switch (ref.kind) {
      case GLOBAL:
        pushGlobals();
        mw.push(ref.name());
        pushContext();
        if (raiseIfNotBound) {
          mw.call(StaticMethod.JAPI_DELGLOBAL);
        } else {
          mw.call(InstanceMethod.IPYDICT_REMOVE_S);
          mw.pop();
        }
        break;
      case UPVALUE:
        dcheck(raiseIfNotBound);
        mw.setFieldNull(() -> loadUpvalueCell(ref.upvalue()), InstanceField.CELL_VALUE);
        break;
      case LOCAL:
        localHandler().del(ref.local(), raiseIfNotBound);
        break;
      case TEMP:
        throw InternalErrorException.notReached();
    }
  }

  protected void notImplemented(String what) {
    mw.push("not implemented: " + what);
    mw.call(StaticMethod.JAPI_OOPS);
  }

  private void throwNotImplemented(String what) {
    mw.throw_(InstanceMethod.NOTIMPLEMENTEDEXCEPTION_INIT_S, what);
  }

  private void callN(int args) {
    pushContext();
    switch (args) {
      case 0:
        mw.call(StaticMethod.FUNCLIB_CALL0);
        break;
      case 1:
        mw.call(StaticMethod.FUNCLIB_CALL1);
        break;
      case 2:
        mw.call(StaticMethod.FUNCLIB_CALL2);
        break;
      case 3:
        mw.call(StaticMethod.FUNCLIB_CALL3);
        break;
      default:
        throw new InternalErrorException("too many arguments");
    }
  }

  private void writeJump(CJump jump) {
    Label target = getLabel(jump.target);
    switch (jump.code) {
      case JUMP_ALWAYS:
        mw.jump(target);
        break;
      case IFTRUEBOOL:
        mw.jumpIfTrue(target);
        break;
      case IFFALSEBOOL:
        mw.jumpIfFalse(target);
        break;
      case IFNULLOBJ:
      case IFNULLEXC:
        mw.jumpIfNull(target);
        break;
      case IFTRUEOBJ:
        pushContext();
        mw.call(StaticMethod.OBJLIB_ISTRUE);
        mw.jumpIfTrue(target);
        break;
      case IFFALSEOBJ:
        pushContext();
        mw.call(StaticMethod.OBJLIB_ISTRUE);
        mw.jumpIfFalse(target);
        break;
    }
  }

  private static boolean isBoolCompOp(CompOp op) {
    switch (op) {
      case IN:
      case EQ:
      case GT:
      case LE:
      case LT:
      case NE:
      case GE:
      case NOT_IN:
        return false;
      case IS:
      case IS_NOT:
        return true;
    }

    throw InternalErrorException.notReached();
  }

  private void rawCompOp(CompOp op) {
    switch (op) {
      case EQ:
        pushContext();
        mw.call(StaticMethod.COMPARISONS_EQ);
        break;
      case NE:
        pushContext();
        mw.call(StaticMethod.COMPARISONS_NE);
        break;
      case LT:
        pushContext();
        mw.call(StaticMethod.COMPARISONS_LT);
        break;
      case LE:
        pushContext();
        mw.call(StaticMethod.COMPARISONS_LE);
        break;
      case GT:
        pushContext();
        mw.call(StaticMethod.COMPARISONS_GT);
        break;
      case GE:
        pushContext();
        mw.call(StaticMethod.COMPARISONS_GE);
        break;

      case IN:
        pushContext();
        mw.call(StaticMethod.COLLLIB_IN);
        break;
      case NOT_IN:
        pushContext();
        mw.call(StaticMethod.COLLLIB_NOTIN);
        break;

      case IS:
      case IS_NOT:
        {
          Label lTrue = new Label();
          if (op == CompOp.IS) {
            mw.jumpIfEqual(lTrue);
          } else {
            mw.jumpIfNotEqual(lTrue);
          }
          mw.push(false);
          Label done = new Label();
          mw.jump(done);
          mw.visitLabel(lTrue);
          mw.push(true);
          mw.visitLabel(done);
          break;
        }
    }
  }

  private void objCompOp(CompOp op) {
    rawCompOp(op);
    if (isBoolCompOp(op)) {
      mw.call(StaticMethod.PYBOOL_OF);
    }
  }

  private void boolCompOp(CompOp op) {
    rawCompOp(op);
    if (!isBoolCompOp(op)) {
      pushContext();
      mw.call(StaticMethod.OBJLIB_ISTRUE);
    }
  }

  private void binOp(int op) {
    pushContext();
    switch (BinOp.values()[op]) {
      case ADD:
        mw.call(StaticMethod.ARITHMETICS_ADD);
        break;
      case SUB:
        mw.call(StaticMethod.ARITHMETICS_SUB);
        break;
      case MUL:
        mw.call(StaticMethod.ARITHMETICS_MUL);
        break;
      case TRUEDIV:
        mw.call(StaticMethod.ARITHMETICS_DIV);
        break;
      case FLOORDIV:
        mw.call(StaticMethod.ARITHMETICS_FLOORDIV);
        break;
      case MOD:
        mw.call(StaticMethod.ARITHMETICS_MOD);
        break;
      case DIVMOD:
        mw.call(StaticMethod.ARITHMETICS_DIVMOD);
        break;
      case POW:
        mw.call(StaticMethod.ARITHMETICS_POW);
        break;
      case AND:
        mw.call(StaticMethod.ARITHMETICS_AND);
        break;
      case OR:
        mw.call(StaticMethod.ARITHMETICS_OR);
        break;
      case XOR:
        mw.call(StaticMethod.ARITHMETICS_XOR);
        break;
      case LSHIFT:
        mw.call(StaticMethod.ARITHMETICS_LSHIFT);
        break;
      case RSHIFT:
        mw.call(StaticMethod.ARITHMETICS_RSHIFT);
        break;
      case MATMUL:
        mw.call(StaticMethod.ARITHMETICS_MATMUL);
        break;
    }
  }

  private void unaryOp(int op) {
    pushContext();
    switch (UnaryOp.values()[op]) {
      case INVERT:
        mw.call(StaticMethod.ARITHMETICS_INVERT);
        break;
      case POS:
        mw.call(StaticMethod.ARITHMETICS_POS);
        break;
      case NEG:
        mw.call(StaticMethod.ARITHMETICS_NEG);
        break;
      case ABS:
        mw.call(StaticMethod.ARITHMETICS_ABS);
        break;
    }
  }

  private void makeList(int args) {
    switch (args) {
      case 0:
        mw.call(StaticMethod.PYLIST_OF0);
        break;
      case 1:
        mw.call(StaticMethod.PYLIST_OF1);
        break;
      case 2:
        mw.call(StaticMethod.PYLIST_OF2);
        break;
      case 3:
        mw.call(StaticMethod.PYLIST_OF3);
        break;
      case 4:
        mw.call(StaticMethod.PYLIST_OF4);
        break;
      default:
        throw InternalErrorException.notReached();
    }
  }

  private void makeSet(int args) {
    switch (args) {
      case 0:
        mw.call(StaticMethod.PYSET_OF_0);
        break;
      case 1:
        pushContext();
        mw.call(StaticMethod.PYSET_OF_1);
        break;
      case 2:
        pushContext();
        mw.call(StaticMethod.PYSET_OF_2);
        break;
      case 3:
        pushContext();
        mw.call(StaticMethod.PYSET_OF_3);
        break;
      case 4:
        pushContext();
        mw.call(StaticMethod.PYSET_OF_4);
        break;
      default:
        throw InternalErrorException.notReached();
    }
  }

  private void makeTuple(int args) {
    switch (args) {
      case 0:
        mw.getField(StaticField.PYTUPLE_EMPTY);
        break;
      case 1:
        mw.call(StaticMethod.PYTUPLE_OF1);
        break;
      case 2:
        mw.call(StaticMethod.PYTUPLE_OF2);
        break;
      case 3:
        mw.call(StaticMethod.PYTUPLE_OF3);
        break;
      case 4:
        mw.call(StaticMethod.PYTUPLE_OF4);
        break;
      default:
        throw InternalErrorException.notReached();
    }
  }

  private void push(AugOp op) {
    switch (op) {
      case IADD:
        mw.getField(StaticField.AUGOP_IADD);
        break;
      case ISUB:
        mw.getField(StaticField.AUGOP_ISUB);
        break;
      case IMUL:
        mw.getField(StaticField.AUGOP_IMUL);
        break;
      case ITRUEDIV:
        mw.getField(StaticField.AUGOP_IDIV);
        break;
      case IFLOORDIV:
        mw.getField(StaticField.AUGOP_IFLOORDIV);
        break;
      case IMOD:
        mw.getField(StaticField.AUGOP_IMOD);
        break;
      case IPOW:
        mw.getField(StaticField.AUGOP_IPOW);
        break;
      case IAND:
        mw.getField(StaticField.AUGOP_IBAND);
        break;
      case IOR:
        mw.getField(StaticField.AUGOP_IBOR);
        break;
      case IXOR:
        mw.getField(StaticField.AUGOP_IXOR);
        break;
      case ILSHIFT:
        mw.getField(StaticField.AUGOP_ILSHIFT);
        break;
      case IRSHIFT:
        mw.getField(StaticField.AUGOP_IRSHIFT);
        break;
      case IMATMUL:
        mw.getField(StaticField.AUGOP_IMATMUL);
        break;
    }
  }

  private void writeSwitch(CSwitch cs) {
    Label dflt = new Label();
    Label[] targets = new Label[cs.targets.length];
    for (int i = 0; i != cs.targets.length; ++i) {
      targets[i] = getLabel(cs.targets[i]);
    }
    mw.visitTableSwitchInsn(0, cs.targets.length - 1, dflt, targets);
    mw.visitLabel(dflt);
    mw.throw_(InstanceMethod.INTERNALERROR_INIT_S, "invalid switch value");
  }

  private void special(int ord) {
    switch (SpecialVar.values()[ord]) {
      case __DEBUG__:
        pushContext();
        mw.call(StaticMethod.JAPI_DEBUG);
        break;
      case STOP_ASYNC_ITERATION:
        mw.getField(StaticField.PYEXCEPTIONTYPE_BUILTIN_STOPASYNCITERATION);
        break;
    }
  }

  private void annotate(String name) {
    pushAnnotations();
    mw.swap();
    pushPyConst(PyStr.get(name));
    mw.swap();
    pushContext();
    mw.call(StaticMethod.COLLLIB_SETITEM);
  }

  private void augAssign(CRef ref, AugOp op) {
    load(ref);
    mw.swap();
    pushContext();

    switch (op) {
      case IADD:
        mw.call(StaticMethod.ARITHMETICS_IADD);
        break;
      case ISUB:
        mw.call(StaticMethod.ARITHMETICS_ISUB);
        break;
      case IMUL:
        mw.call(StaticMethod.ARITHMETICS_IMUL);
        break;
      case ITRUEDIV:
        mw.call(StaticMethod.ARITHMETICS_IDIV);
        break;
      case IFLOORDIV:
        mw.call(StaticMethod.ARITHMETICS_IFLOORDIV);
        break;
      case IMOD:
        mw.call(StaticMethod.ARITHMETICS_IMOD);
        break;
      case IPOW:
        mw.call(StaticMethod.ARITHMETICS_IPOW);
        break;
      case IMATMUL:
        mw.call(StaticMethod.ARITHMETICS_IMATMUL);
        break;
      case IAND:
        mw.call(StaticMethod.ARITHMETICS_IBAND);
        break;
      case IOR:
        mw.call(StaticMethod.ARITHMETICS_IBOR);
        break;
      case IXOR:
        mw.call(StaticMethod.ARITHMETICS_IXOR);
        break;
      case ILSHIFT:
        mw.call(StaticMethod.ARITHMETICS_ILSHIFT);
        break;
      case IRSHIFT:
        mw.call(StaticMethod.ARITHMETICS_IRSHIFT);
        break;
    }

    store(ref);
  }

  protected void writeInstr(CInstr instr) {
    switch (instr.kind()) {
      case LABEL:
        mw.visitLabel(getLabel((CLabel) instr));
        break;

      case JUMP:
        writeJump((CJump) instr);
        break;

      case LITERAL:
        pushPyConst(((CLiteral) instr).value);
        break;

      case CATCH:
        {
          //          CExcHandler h = ((CCatch) instr).excHandler;
          //          mw.visitTryCatchBlock(
          //              getLabel(h.start), getLabel(h.end), getLabel(h.handler),
          // Constants.N_THROWABLE);
          break;
        }

      case INSTRI:
        {
          CInstrI ci = (CInstrI) instr;
          switch (ci.code) {
            case CONST:
              pushConst(ci.arg);
              break;

            case MAKEARGS:
              switch (ci.arg) {
                case 0:
                  mw.call(StaticMethod.ARGS_OF_0);
                  break;
                case 1:
                  mw.call(StaticMethod.ARGS_OF_1);
                  break;
                case 2:
                  mw.call(StaticMethod.ARGS_OF_2);
                  break;
                default:
                  throw new InternalErrorException("too many arguments", ci);
              }
              break;

            case CALL:
              callN(ci.arg);
              break;

            case VCALL:
              callN(ci.arg);
              mw.pop();
              break;

            case COMPOP:
              objCompOp(CompOp.values()[ci.arg]);
              break;
            case BCOMPOP:
              boolCompOp(CompOp.values()[ci.arg]);
              break;

            case BINOP:
              binOp(ci.arg);
              break;

            case UNARYOP:
              unaryOp(ci.arg);
              break;

            case NEWARRAY:
              mw.newArray(Constants.N_IPYOBJ, ci.arg);
              break;

            case SETARRAY:
              mw.push(ci.arg);
              mw.swap();
              mw.visitInsn(AASTORE);
              break;

            case MAKELIST:
              makeList(ci.arg);
              break;
            case MAKETUP:
              makeTuple(ci.arg);
              break;
            case MAKESET:
              makeSet(ci.arg);
              break;

            case NEWSET:
              mw.createObj(InstanceMethod.PYSET_INIT_I, ci.arg);
              break;
            case NEWDICT:
              mw.createObj(InstanceMethod.PYDICT_INIT_I, ci.arg);
              break;
            case NEWLIST:
              mw.createObj(InstanceMethod.PYLIST_INIT_I, ci.arg);
              break;

            case NEWTUPB:
              mw.push(ci.arg);
              mw.call(StaticMethod.PYTUPLE_BUILDER);
              break;

            case GETTUPLE:
              mw.push(ci.arg);
              mw.call(InstanceMethod.PYTUPLE_GET);
              break;

            case UNPACKEXACT:
              mw.push(ci.arg);
              pushContext();
              mw.call(StaticMethod.JAPI_UNPACKEXACT);
              break;
            case UNPACKATLEAST:
              mw.push(ci.arg);
              pushContext();
              mw.call(StaticMethod.JAPI_UNPACKATLEAST);
              break;

            case AALOAD:
              mw.aaload(ci.arg);
              break;

            case AUGASSIGNINDEX:
              // op1[op2] @= op3
              push(AugOp.values()[ci.arg]);
              pushContext();
              mw.call(StaticMethod.JAPI_AUGASSIGN_INDEX);
              break;

            case SPECIAL:
              special(ci.arg);
              break;
          }
          break;
        }

      case MAKECLASS:
        makeClass(((CMakeClass) instr).klass);
        break;

      case MAKEFUNC:
        makeFunc(((CMakeFunc) instr).func);
        break;

      case INSTR0:
        {
          CInstr0 ci = (CInstr0) instr;
          switch (ci.code) {
            case LOADNULLOBJ:
            case LOADNULLARGS:
            case LOADNULLAOBJ:
              mw.pushNull();
              break;
            case LOADTRUE:
              mw.push(true);
              break;
            case LOADFALSE:
              mw.push(false);
              break;

            case RET:
              mw.returnTop();
              break;

            case PYTYPE:
              mw.call(InstanceMethod.IPYOBJ_TYPE);
              break;

            case TOBOOL:
              pushContext();
              mw.call(StaticMethod.OBJLIB_ISTRUE);
              break;
            case INVBOOL:
              {
                Label ifFalse = new Label();
                mw.jumpIfFalse(ifFalse);
                mw.push(false);
                Label done = new Label();
                mw.jump(done);
                mw.visitLabel(ifFalse);
                mw.push(true);
                mw.visitLabel(done);
                break;
              }
            case NOTOBJ:
              pushContext();
              mw.call(StaticMethod.OBJLIB_NOT);
              break;

            case DUP:
            case DUPBOOL:
            case DUPEXC:
            case DUPA:
              mw.dup();
              break;
            case SWAP:
              mw.swap();
              break;
            case DUPX1:
              mw.dupX1();
              break;
            case POP:
            case POPEXC:
            case POPBOOL:
              mw.pop();
              break;

            case RAISE:
              pushContext();
              mw.call(StaticMethod.EXCLIB_RAISE);
              mw.throw_();
              break;
            case RAISE_FROM:
              pushContext();
              mw.call(StaticMethod.EXCLIB_RAISE_FROM);
              mw.throw_();
              break;
            case RAISE_EX:
              pushContext();
              mw.call(StaticMethod.EXCLIB_RAISE_EX);
              mw.throw_();
              break;
            case THROW:
              mw.throw_();
              break;

            case ASSERTIONFAILED:
              pushContext();
              mw.call(StaticMethod.JAPI_ASSERTIONFAILED);
              mw.throw_();
              break;
            case ASSERTIONFAILEDMSG:
              pushContext();
              mw.call(StaticMethod.JAPI_ASSERTIONFAILEDMSG);
              mw.throw_();
              break;

            case CALLARGS:
              pushContext();
              mw.call(StaticMethod.FUNCLIB_CALL_ARGS);
              break;
            case VCALLARGS:
              pushContext();
              mw.call(StaticMethod.FUNCLIB_CALL_ARGS);
              mw.pop();
              break;

            case PARSEARGS0:
              parseArgs0();
              break;
            case PARSEARGS1:
              parseArgs1();
              break;
            case PARSEARGS:
              parseArgs();
              break;

            case LISTAPPEND:
              mw.call(InstanceMethod.PYLIST_APPENDNV);
              break;
            case LISTADDVOID:
              mw.call(InstanceMethod.PYLIST_APPEND);
              break;
            case TUPBADD:
              mw.call(InstanceMethod.PYTUPLE_BUILDER_ADD);
              break;
            case BUILDTUP:
              mw.call(InstanceMethod.PYTUPLE_BUILDER_BUILD);
              break;
            case NEWTUPLE:
              mw.call(StaticMethod.PYTUPLE_OF);
              break;
            case SETADDVOID:
              pushContext();
              mw.call(InstanceMethod.PYSET_ADD);
              break;
            case SETADD:
              pushContext();
              mw.call(StaticMethod.JAPI_SETADD);
              break;
            case SETITEM:
              pushContext();
              mw.call(StaticMethod.COLLLIB_SETITEM);
              break;
            case GETITEM:
              pushContext();
              mw.call(StaticMethod.COLLLIB_GETITEM);
              break;
            case DELITEM:
              pushContext();
              mw.call(StaticMethod.COLLLIB_DELITEM);
              break;
            case MAKESLICE:
              mw.call(StaticMethod.JAPI_SLICE);
              break;
            case LISTADDSTAR:
              pushContext();
              mw.call(StaticMethod.JAPI_LISTEXTEND);
              break;
            case TUPBADDSTAR:
              pushContext();
              mw.call(InstanceMethod.PYTUPLE_BUILDER_EXTEND);
              break;
            case SETADDSTAR:
              pushContext();
              mw.call(StaticMethod.JAPI_SETEXTEND);
              break;

            case LEN:
              pushContext();
              mw.call(StaticMethod.COLLLIB_LEN);
              mw.call(StaticMethod.PYINT_GET_I);
              break;

            case STR:
              pushContext();
              mw.call(StaticMethod.STRLIB_STR);
              break;
            case REPR:
              pushContext();
              mw.call(StaticMethod.STRLIB_REPR);
              break;
            case ASCII:
              pushContext();
              mw.call(StaticMethod.STRLIB_ASCII);
              break;

            case ISNOTNULL:
              {
                Label notNull = new Label();
                mw.jumpIfNonNull(notNull);
                pushPyConst(False);
                Label done = new Label();
                mw.jump(done);
                mw.visitLabel(notNull);
                pushPyConst(True);
                mw.visitLabel(done);
                break;
              }

            case FROMBOOL:
              mw.call(StaticMethod.PYBOOL_OF);
              break;

            case VDICTADD:
              pushContext();
              mw.call(StaticMethod.JAPI_DICTADD);
              mw.pop();
              break;
            case DICTADD:
              pushContext();
              mw.call(StaticMethod.JAPI_DICTADD);
              break;
            case DICTADDSTAR:
              pushContext();
              mw.call(StaticMethod.JAPI_DICTEXTEND);
              break;

            case BUILDSTR:
              mw.call(InstanceMethod.PYSTRBUILDER_BUILD);
              break;
            case STRBAPPEND:
              mw.checkCast(Constants.T_PYSTR);
              mw.call(InstanceMethod.PYSTRBUILDER_APPEND);
              break;
            case NEWSTRB:
              mw.createObj(InstanceMethod.PYSTRBUILDER_INIT);
              break;
            case FORMAT:
              pushContext();
              mw.call(StaticMethod.OBJFORMATLIB_FORMAT);
              break;

            case ADDKWARGDICT:
              pushContext();
              mw.call(InstanceMethod.ARGS_BUILDER_UNPACK_AND_ADD_KW);
              break;
            case ADDARGTUPLE:
              pushContext();
              mw.call(InstanceMethod.ARGS_BUILDER_UNPACK_AND_ADD_POS);
              break;
            case ADDARG:
              mw.call(InstanceMethod.ARGS_BUILDER_ADD_POS);
              break;

            case BUILDARGS:
              mw.call(InstanceMethod.ARGS_BUILDER_BUILD);
              break;

            case ITER:
              pushContext();
              mw.call(StaticMethod.ITERLIB_ITER);
              break;

            case FORITERNEXT:
              pushContext();
              mw.call(InstanceMethod.IPYFORITER_NEXT);
              break;
            case MAKEFORITER:
              pushContext();
              mw.call(StaticMethod.ITERLIB_MAKEFORITER);
              break;

            case SETCURRENTEXC:
              pushContext();
              mw.call(StaticMethod.EXCLIB_SETCURRENTEXC);
              break;
            case EXCMATCH:
              pushContext();
              mw.call(StaticMethod.JAPI_EXCMATCH);
              break;

            case NOP:
              mw.visitInsn(NOP);
              break;
            case IGNOP:
              break;

            case SYSEXCINFO:
              pushContext();
              mw.call(StaticMethod.SYSLIB_EXCINFO);
              break;

            case IMPORTSTAR:
              pushGlobals();
              pushContext();
              mw.call(StaticMethod.IMPORTLIB_IMPORTSTAR);
              break;
            case IMPORT:
              pushGlobals();
              pushContext();
              mw.call(StaticMethod.JAPI_IMPORT);
              break;

            case YIELD:
            case YIELDFROM:
            case AWAIT:
              // Handled by GeneratorMethodGen
              throw InternalErrorException.notReached(instr.toString(), instr);
          }
          break;
        }

      case INSTRS:
        {
          CInstrS ci = (CInstrS) instr;
          switch (ci.code) {
            case GETATTR:
              mw.push(ci.arg);
              pushContext();
              mw.call(StaticMethod.OBJLIB_GETATTR);
              break;
            case SETATTR:
              mw.push(ci.arg);
              mw.swap();
              pushContext();
              mw.call(StaticMethod.OBJLIB_SETATTR);
              break;
            case DELATTR:
              mw.push(ci.arg);
              pushContext();
              mw.call(StaticMethod.OBJLIB_DELATTR);
              break;

            case STRING:
              mw.push(ci.arg);
              break;

            case NOTIMPLEMENTED:
              throwNotImplemented(ci.arg);
              break;

            case DBC:
              if (Debug.ENABLED) {
                mw.push(ci.arg);
                mw.pop();
              }
              break;

            case STRBAPPENDS:
              mw.push(ci.arg);
              mw.call(InstanceMethod.PYSTRBUILDER_APPENDUNKNOWN);
              break;

            case AUGASSIGNATTR:
              mw.push(ci.arg);
              mw.swap();
              push(AugOp.values()[ci.intarg]);
              pushContext();
              mw.call(StaticMethod.JAPI_AUGASSIGN_ATTR);
              break;

            case ADDKWARG:
              mw.push(ci.arg);
              mw.swap();
              mw.call(InstanceMethod.ARGS_BUILDER_ADD_KW);
              break;
          }
          break;
        }

      case INSTRR:
        {
          CInstrR ci = (CInstrR) instr;
          switch (ci.code) {
            case STORE:
            case STOREEXC:
              store(ci.ref);
              break;
            case LOAD:
            case LOADSTRB:
            case LOADEXC:
            case LOADINT:
            case LOADBOOL:
              load(ci.ref);
              break;
            case DEL:
              del(ci.ref, true);
              break;
            case DELIFBOUND:
              del(ci.ref, false);
              break;

            case BEGINTEMP:
              tempHandler().beginTemp(ci.ref.temp());
              break;
            case ENDTEMP:
              tempHandler().endTemp(ci.ref.temp());
              break;
            case ERASETEMP:
              tempHandler().del(ci.ref.temp());
              break;
            case INITTEMP:
              tempHandler().initTemp(ci.ref.temp());
              break;

            case STORECONSTINT:
              mw.push(ci.arg);
              store(ci.ref);
              break;

            case CAPTUREEXC:
              pushContext();
              mw.call(StaticMethod.EXCLIB_CAPTUREEXC);
              store(ci.ref);
              break;

            case AUGASSIGN:
              augAssign(ci.ref, AugOp.values()[ci.arg]);
              break;

            case ANNOTATE:
              annotate(ci.ref.name());
              break;
          }
          break;
        }

      case INSTRI2:
        {
          CInstrI2 ci = (CInstrI2) instr;
          //noinspection SwitchStatementWithTooFewBranches
          switch (ci.code) {
            case NEWARGSB:
              mw.push(ci.arg1);
              mw.push(ci.arg2);
              pushContext();
              mw.call(StaticMethod.ARGS_BUILDER);
              break;
          }
          break;
        }

      case SWITCH:
        writeSwitch((CSwitch) instr);
        break;
    }
  }
}
