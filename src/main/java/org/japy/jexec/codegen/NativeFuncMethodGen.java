package org.japy.jexec.codegen;

import static org.japy.infra.validation.Debug.dcheck;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CLocalVar;
import org.japy.infra.validation.Debug;

final class NativeFuncMethodGen extends FuncMethodGen {
  // void call(Args args, PyContext ctx);
  private static final int IARGS = 1;
  private static final int ICTX = 2;
  private static final int IFIRSTVAR = 3;

  private static final boolean useJvmTemps = true;
  private final int iFirstTemp;
  private final @Nullable Label lTempStart;

  NativeFuncMethodGen(MethInfo<CFunc> methInfo, MethodVisitor mv, CodegenConfig config) {
    super(methInfo, mv, config);
    iFirstTemp = IFIRSTVAR + block.locals.length + block.cells.length;
    lTempStart = useJvmTemps ? null : new Label();
  }

  @Override
  protected void declareMethodParameters(MethodVisitor mv, Label start, Label end) {
    mv.visitLocalVariable("$args", Constants.D_ARGS, null, start, end, IARGS);
    mv.visitLocalVariable("$ctx", Constants.D_PYCONTEXT, null, start, end, ICTX);
  }

  @Override
  protected void loadArgs() {
    mw.loadLocal(IARGS);
  }

  @Override
  protected void pushContext() {
    mw.loadLocal(ICTX);
  }

  @Override
  protected LocalHandler createLocalHandler() {
    return new JvmLocalHandler(this, IFIRSTVAR, IFIRSTVAR + block.locals.length);
  }

  @Override
  protected TempHandler createTempHandler() {
    if (useJvmTemps) {
      return new JvmTempHandler(this, block, iFirstTemp);
    } else {
      return new ArrayTempHandler(mw, () -> mw.loadLocal(iFirstTemp));
    }
  }

  @Override
  protected void beginMethod() {
    super.beginMethod();

    if (Debug.ENABLED) {
      dcheck(!block.isGenerator() && !block.isAsync());
    }

    for (CLocalVar var : block.locals) {
      localHandler().begin(var);
      if (!block.isParam(var)) {
        localHandler().init(var);
      }
    }

    for (CLocalVar var : block.cells) {
      localHandler().begin(var);
      localHandler().init(var);
    }

    if (!useJvmTemps) {
      mw.visitLabel(lTempStart);
      mw.newArray(Constants.N_OBJECT, block.tempCount);
      mw.storeLocal(iFirstTemp);
    }
  }

  @Override
  protected void endMethod(Label last) {
    super.endMethod(last);

    for (CLocalVar var : block.cells) {
      localHandler().finish(var, last);
    }

    if (!useJvmTemps) {
      mw.visitLocalVariable("$temp", Constants.D_AOBJECT, null, lTempStart, last, iFirstTemp);
    }
  }
}
