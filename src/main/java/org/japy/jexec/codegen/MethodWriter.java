package org.japy.jexec.codegen;

import static org.japy.infra.validation.Debug.dcheck;

import java.util.Arrays;
import java.util.List;
import java.util.function.IntConsumer;
import java.util.function.IntFunction;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import org.japy.infra.loc.IHasLocation;
import org.japy.infra.loc.Location;
import org.japy.infra.validation.Debug;

@SuppressWarnings({"SameParameterValue", "unused"})
final class MethodWriter extends MethodVisitor implements Opcodes {
  private Location location = Location.UNKNOWN;
  private final boolean writeLineNumbers;

  MethodWriter(MethodVisitor mv, CodegenConfig config) {
    this(mv, config.lineNumbers());
  }

  MethodWriter(MethodVisitor mv, boolean writeLineNumbers) {
    super(Constants.ASM_VERSION, mv);
    this.writeLineNumbers = writeLineNumbers;
  }

  void at(IHasLocation hl) {
    Location newLocation = hl.location();
    if (newLocation.isUnknown()) {
      return;
    }

    if (writeLineNumbers && newLocation.line != location.line) {
      Label label = new Label();
      visitLabel(label);
      visitLineNumber(newLocation.line, label);
    }

    location = newLocation;
  }

  void checkCast(Type type) {
    visitTypeInsn(CHECKCAST, type.getInternalName());
  }

  void call(StaticMethod method) {
    visitMethodInsn(INVOKESTATIC, method.internalOwnerName, method.name, method.descriptor, false);
  }

  void callThisDot(InstanceMethod method) {
    if (Debug.ENABLED) {
      dcheck(method.paramCount == 0);
    }
    pushThis();
    call(method);
  }

  void callThisDot(InstanceMethod method, Runnable pushArgs) {
    if (Debug.ENABLED) {
      dcheck(method.paramCount != 0);
    }
    pushThis();
    pushArgs.run();
    call(method);
  }

  void call(InstanceMethod method) {
    visitMethodInsn(
        method.isConstructor()
            ? INVOKESPECIAL
            : (method.isInterface() ? INVOKEINTERFACE : INVOKEVIRTUAL),
        method.ownerInternalName,
        method.methodName(),
        method.descriptor,
        method.isInterface());
  }

  void dot(Runnable pushObj, InstanceField field) {
    pushObj.run();
    getField(field);
  }

  void getField(InstanceField field) {
    visitFieldInsn(GETFIELD, field.ownerName, field.name, field.descriptor);
  }

  void getField(StaticField field) {
    visitFieldInsn(GETSTATIC, field.ownerName, field.name, field.descriptor);
  }

  void thisDot(InstanceField field) {
    pushThis();
    getField(field);
  }

  void thisDot(InstanceField field1, InstanceField field2) {
    thisDot(field1);
    getField(field2);
  }

  void putField(InstanceField field) {
    visitFieldInsn(PUTFIELD, field.ownerName, field.name, field.descriptor);
  }

  void setFieldFromStack(Runnable pushObj, InstanceField field) {
    pushObj.run();
    swap();
    putField(field);
  }

  void putFieldNull(InstanceField field) {
    pushNull();
    putField(field);
  }

  void setFieldNull(Runnable pushObj, InstanceField field) {
    pushObj.run();
    pushNull();
    visitFieldInsn(PUTFIELD, field.ownerName, field.name, field.descriptor);
  }

  void setFieldOfThis(InstanceField field, Runnable pushValue) {
    pushThis();
    pushValue.run();
    visitFieldInsn(PUTFIELD, field.ownerName, field.name, field.descriptor);
  }

  void newArray(String elementTypeName, int length) {
    push(length);
    visitTypeInsn(ANEWARRAY, elementTypeName);
  }

  void newObjAndDup(Type type) {
    visitTypeInsn(NEW, type.getInternalName());
    dup();
  }

  void createObj(InstanceMethod constructor, Runnable pushArgs) {
    dcheck(constructor.isConstructor());
    newObjAndDup(constructor.ownerType);
    pushArgs.run();
    call(constructor);
  }

  void pushArray(String elementTypeName, int size, IntConsumer pushValue) {
    newArray(elementTypeName, size);
    for (int i = 0; i != size; ++i) {
      dup();
      push(i);
      pushValue.accept(i);
      visitInsn(AASTORE);
    }
  }

  void pushStringArray(List<String> strings) {
    pushArray(Constants.N_STRING, strings.size(), i -> push(strings.get(i)));
  }

  void pushStringArray(int size, IntFunction<String> getValue) {
    pushArray(Constants.N_STRING, size, i -> push(getValue.apply(i)));
  }

  void pushPrimitiveArray(int type, int storeInsn, int length, IntConsumer writeValue) {
    push(length);
    visitIntInsn(NEWARRAY, type);
    for (int i = 0; i < length; ++i) {
      dup();
      push(i);
      writeValue.accept(i);
      visitInsn(storeInsn);
    }
  }

  void pushThis() {
    loadLocal(0);
  }

  void loadLocal(int iLocal) {
    visitVarInsn(ALOAD, iLocal);
  }

  void storeLocal(int iLocal) {
    visitVarInsn(ASTORE, iLocal);
  }

  void storeLocalNull(int iLocal) {
    pushNull();
    visitVarInsn(ASTORE, iLocal);
  }

  void storeLocal(int iLocal, Runnable pushObj) {
    pushObj.run();
    visitVarInsn(ASTORE, iLocal);
  }

  void pushNull() {
    visitInsn(ACONST_NULL);
  }

  void aastoreNull(Runnable pushArray, int index) {
    pushArray.run();
    push(index);
    pushNull();
    visitInsn(AASTORE);
  }

  void aastoreNull(int index) {
    push(index);
    pushNull();
    visitInsn(AASTORE);
  }

  void aastore(Runnable pushArray, int index, Runnable pushValue) {
    pushArray.run();
    push(index);
    pushValue.run();
    visitInsn(AASTORE);
  }

  void aastoreFromStack(Runnable pushArray, int index) {
    pushArray.run();
    swap();
    push(index);
    swap();
    visitInsn(AASTORE);
  }

  void aaload(Runnable pushArray, int index) {
    pushArray.run();
    push(index);
    visitInsn(AALOAD);
  }

  void aaload(int index) {
    push(index);
    visitInsn(AALOAD);
  }

  void push(boolean v) {
    visitInsn(v ? ICONST_1 : ICONST_0);
  }

  void push(String v) {
    visitLdcInsn(v);
  }

  void push(int value) {
    switch (value) {
      case 0:
        visitInsn(ICONST_0);
        break;
      case 1:
        visitInsn(ICONST_1);
        break;
      case 2:
        visitInsn(ICONST_2);
        break;
      case 3:
        visitInsn(ICONST_3);
        break;
      case 4:
        visitInsn(ICONST_4);
        break;
      case 5:
        visitInsn(ICONST_5);
        break;
      case -1:
        visitInsn(ICONST_M1);
        break;
      default:
        if (Byte.MIN_VALUE <= value && value <= Byte.MAX_VALUE) {
          visitIntInsn(BIPUSH, value);
        } else if (Short.MIN_VALUE <= value && value <= Short.MAX_VALUE) {
          visitIntInsn(SIPUSH, value);
        } else {
          visitLdcInsn(value);
        }
    }
  }

  void push(long value) {
    if (value == 0) {
      visitInsn(LCONST_0);
    } else if (value == 1) {
      visitInsn(LCONST_1);
    } else {
      visitLdcInsn(value);
    }
  }

  void push(float value) {
    visitLdcInsn(value);
  }

  void push(double value) {
    visitLdcInsn(value);
  }

  void push(String[] strings) {
    pushStringArray(Arrays.asList(strings));
  }

  void createObj(InstanceMethod constructor) {
    if (Debug.ENABLED) {
      dcheck(constructor.paramCount() == 0);
    }
    createObj(constructor, () -> {});
  }

  void createObj(InstanceMethod constructor, String arg) {
    if (Debug.ENABLED) {
      dcheck(constructor.paramCount() == 1);
    }
    createObj(constructor, () -> push(arg));
  }

  void createObj(InstanceMethod constructor, int arg) {
    if (Debug.ENABLED) {
      dcheck(constructor.paramCount() == 1);
    }
    createObj(constructor, () -> push(arg));
  }

  void push(byte[] values) {
    pushPrimitiveArray(T_BYTE, BASTORE, values.length, i -> push(values[i]));
  }

  void push(byte[] values, int start, int count) {
    pushPrimitiveArray(T_BYTE, BASTORE, count, i -> push(values[start + i]));
  }

  void push(int[] values) {
    pushPrimitiveArray(T_INT, IASTORE, values.length, i -> push(values[i]));
  }

  Label mark() {
    Label label = new Label();
    visitLabel(label);
    return label;
  }

  void jump(Label label) {
    visitJumpInsn(GOTO, label);
  }

  void jumpIfTrue(Label label) {
    visitJumpInsn(IFNE, label);
  }

  void jumpIfFalse(Label label) {
    visitJumpInsn(IFEQ, label);
  }

  void jumpIfNull(Label label) {
    visitJumpInsn(IFNULL, label);
  }

  void jumpIfNonNull(Label label) {
    visitJumpInsn(IFNONNULL, label);
  }

  void jumpIfEqual(Label label) {
    visitJumpInsn(IF_ACMPEQ, label);
  }

  void jumpIfNotEqual(Label label) {
    visitJumpInsn(IF_ACMPNE, label);
  }

  void returnVoid() {
    visitInsn(RETURN);
  }

  void returnTop() {
    visitInsn(ARETURN);
  }

  void pop() {
    visitInsn(POP);
  }

  void pop(int howMany) {
    for (int i = 0; i != howMany; ++i) {
      visitInsn(POP);
    }
  }

  void dup() {
    visitInsn(DUP);
  }

  void dupX1() {
    visitInsn(DUP_X1);
  }

  void swap() {
    visitInsn(SWAP);
  }

  void throw_() {
    visitInsn(ATHROW);
  }

  void throw_(InstanceMethod constructor, String arg) {
    createObj(constructor, arg);
    visitInsn(ATHROW);
  }
}
