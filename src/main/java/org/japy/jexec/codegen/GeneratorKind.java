package org.japy.jexec.codegen;

enum GeneratorKind {
  GENERATOR,
  ASYNC_GENERATOR,
  COROUTINE,
}
