package org.japy.jexec.codegen;

import static org.objectweb.asm.Opcodes.*;

import java.lang.invoke.MethodType;
import java.util.Arrays;

import com.google.common.collect.Iterables;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CLocalVar;
import org.japy.compiler.code.CUpvalue;

final class GeneratorMethInfo extends MethInfo<CFunc> {
  // <init>(PyFuncHeader body, Args args, PyContext ctx)
  private static final int ICONSTR_BODY = 1;
  private static final int ICONSTR_ARGS = 2;
  private static final int ICONSTR_CTX = 3;

  public final GeneratorKind generatorKind;

  public GeneratorMethInfo(CFunc func) {
    super(Kind.GENERATOR, func);

    if (func.isAsync()) {
      if (func.isGenerator()) {
        generatorKind = GeneratorKind.ASYNC_GENERATOR;
      } else {
        generatorKind = GeneratorKind.COROUTINE;
      }
    } else {
      generatorKind = GeneratorKind.GENERATOR;
    }
  }

  @Override
  String baseClassName() {
    return Constants.N_JGENERATORIMPL;
  }

  @Override
  CUpvalue @Nullable [] upvalues() {
    return block.upvalues;
  }

  @Override
  CLocalVar @Nullable [] locals() {
    return block.locals;
  }

  @Override
  CLocalVar @Nullable [] cells() {
    return block.cells;
  }

  @Override
  void writeInit(MethodWriter mw) {
    // <init>(PyFuncHeader body, Args args, PyContext ctx)
    //   -> JGenerator(body, tempCount)
    mw.callThisDot(
        InstanceMethod.JGENERATORIMPL_INIT,
        () -> {
          mw.loadLocal(ICONSTR_BODY);
          mw.push(block.tempCount);
        });
    writeParseArgs(mw);
  }

  private void writeParseArgs(MethodWriter mw) {
    if (block.signature.paramCount() == 0) {
      mw.loadLocal(ICONSTR_ARGS);
      mw.push(block.name);
      mw.loadLocal(ICONSTR_CTX);
      mw.call(StaticMethod.ARGPARSER_VERIFYNOARGS);
      return;
    }

    mw.thisDot(InstanceField.GENERATORIMPL_HEADER, InstanceField.PYFUNCHEADER_ARGPARSER);
    mw.loadLocal(ICONSTR_ARGS);
    mw.push(block.name);
    mw.loadLocal(ICONSTR_CTX);

    if (block.signature.paramCount() == 1) {
      mw.call(InstanceMethod.ARGPARSER_PARSE1);
    } else {
      mw.call(InstanceMethod.ARGPARSER_PARSE);
    }

    assignParams(mw);
  }

  private void assignParams(MethodWriter mw) {
    int paramCount = block.signature.paramCount();
    for (int i = 0; i != paramCount; ++i) {
      if (paramCount != 1) {
        mw.dup();
        mw.aaload(i);
      }

      String name = block.signature.paramNames[i];
      CLocalVar v = block.getLocal(name, block);
      if (v.isCell) {
        mw.createObj(InstanceMethod.CELL_INIT_NULL);
        mw.dupX1();
        mw.swap();
        mw.putField(InstanceField.CELL_VALUE);
      }

      mw.pushThis();
      mw.swap();
      mw.visitFieldInsn(
          PUTFIELD, internalClassName, name, v.isCell ? Constants.D_CELL : Constants.D_IPYOBJ);
    }

    if (paramCount != 1) {
      mw.pop();
    }
  }

  @Override
  void declareFields(ClassVisitor cv) {
    for (CLocalVar v : Iterables.concat(Arrays.asList(block.locals), Arrays.asList(block.cells))) {
      cv.visitField(
          ACC_PRIVATE, v.name, v.isCell ? Constants.D_CELL : Constants.D_IPYOBJ, null, null);
    }
  }

  @Override
  void declareConstructorParameters(MethodVisitor mv, Label start, Label end) {
    mv.visitLocalVariable("body", Constants.D_PYFUNCHEADER, null, start, end, ICONSTR_BODY);
    mv.visitLocalVariable("args", Constants.D_ARGS, null, start, end, ICONSTR_ARGS);
    mv.visitLocalVariable("ctx", Constants.D_PYCONTEXT, null, start, end, ICONSTR_CTX);
  }

  @Override
  String initDescriptor() {
    return Constants.MD_GEN_GENERATOR_INIT;
  }

  @Override
  String methodName() {
    return Constants.GEN_GENERATOR_METHOD;
  }

  @Override
  MethodType methodType() {
    return Constants.MT_GEN_GENERATOR_METHOD;
  }

  @Override
  MethodGen<?> getMethodGen(MethodVisitor mv, CodegenConfig config) {
    return new GeneratorMethodGen(this, mv, config);
  }
}
