package org.japy.jexec.codegen;

import static org.japy.infra.util.ExcUtil.wrap;
import static org.japy.infra.validation.Debug.dcheck;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.objectweb.asm.Type;

import org.japy.jexec.obj.JClassExec;
import org.japy.jexec.obj.JFunc;
import org.japy.jexec.obj.JGeneratorImpl;
import org.japy.jexec.obj.JModuleExec;
import org.japy.kernel.exec.Cell;
import org.japy.kernel.types.func.PyFunc;
import org.japy.kernel.types.func.PyFuncHeader;
import org.japy.kernel.types.gen.GeneratorImpl;
import org.japy.kernel.types.gen.IPyGeneratorSupport;

@SuppressWarnings("ImmutableEnumChecker")
public enum InstanceField {
  // spotless:off

  CELL_VALUE(wrap(() -> Cell.class.getField("value"))),

  GENERATORIMPL_HEADER(wrap(() -> GeneratorImpl.class.getDeclaredField("header"))),
  GENERATORIMPL_SUPPORT(wrap(() -> GeneratorImpl.class.getDeclaredField("support"))),

  IPYGENERATORSUPPORT_CONTINUE_VALUE(wrap(() -> IPyGeneratorSupport.Continue.class.getField("value"))),

  JCLASSEXEC_CONSTANTS(wrap(() -> JClassExec.class.getDeclaredField("constants"))),
  JCLASSEXEC_UPVALUES(wrap(() -> JClassExec.class.getDeclaredField("upvalues"))),
  JCLASSEXEC_GLOBALS(wrap(() -> JClassExec.class.getDeclaredField("globals"))),
  JCLASSEXEC_BUILTINS(wrap(() -> JClassExec.class.getDeclaredField("builtins"))),

  JFUNC_CONSTANTS(wrap(() -> JFunc.class.getDeclaredField("constants"))),

  JGENERATORIMPL_NEXTPOINT(wrap(() -> JGeneratorImpl.class.getDeclaredField("nextPoint"))),
  JGENERATORIMPL_TEMPS(wrap(() -> JGeneratorImpl.class.getDeclaredField("temps"))),
  JGENERATORIMPL_CONSTANTS(wrap(() -> JGeneratorImpl.class.getDeclaredField("constants"))),

  JMODULEEXEC_CONSTANTS(wrap(() -> JModuleExec.class.getDeclaredField("constants"))),
  JMODULEEXEC_GLOBALS(wrap(() -> JModuleExec.class.getDeclaredField("globals"))),
  JMODULEEXEC_BUILTINS(wrap(() -> JModuleExec.class.getDeclaredField("builtins"))),

  PYFUNC_HEADER(wrap(() -> PyFunc.class.getDeclaredField("header"))),

  PYFUNCHEADER_UPVALUES(wrap(() -> PyFuncHeader.class.getDeclaredField("upvalues"))),
  PYFUNCHEADER_GLOBALS(wrap(() -> PyFuncHeader.class.getDeclaredField("globals"))),
  PYFUNCHEADER_BUILTINS(wrap(() -> PyFuncHeader.class.getDeclaredField("builtins"))),
  PYFUNCHEADER_ARGPARSER(wrap(() -> PyFuncHeader.class.getDeclaredField("argParser"))),

  ;
  // spotless:on

  public final String name;
  public final String ownerName;
  public final String descriptor;
  public final Type type;

  InstanceField(Field field) {
    dcheck(!Modifier.isStatic(field.getModifiers()));
    name = field.getName();
    ownerName = Type.getInternalName(field.getDeclaringClass());
    type = Type.getType(field.getType());
    descriptor = type.getDescriptor();
  }
}
