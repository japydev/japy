package org.japy.jexec.codegen;

import java.lang.invoke.MethodType;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import org.japy.jexec.obj.JClassExec;
import org.japy.jexec.obj.JFrameFunc;
import org.japy.jexec.obj.JGeneratorImpl;
import org.japy.jexec.obj.JModuleExec;
import org.japy.jexec.obj.JNativeFunc;
import org.japy.kernel.exec.Cell;
import org.japy.kernel.exec.Frame;
import org.japy.kernel.exec.PyContext;
import org.japy.kernel.types.IPyObj;
import org.japy.kernel.types.cls.IPyClass;
import org.japy.kernel.types.cls.ext.IPyBuiltinSubClassInstance;
import org.japy.kernel.types.cls.ext.IPyExtClass;
import org.japy.kernel.types.cls.ext.PyExtBuiltinSubClass;
import org.japy.kernel.types.cls.ext.adapters.IPyObjWithTruthAdapter;
import org.japy.kernel.types.coll.dict.IPyDict;
import org.japy.kernel.types.coll.dict.IPyRODict;
import org.japy.kernel.types.coll.dict.PyDict;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBaseException;
import org.japy.kernel.types.func.PyFuncHeader;
import org.japy.kernel.types.gen.IPyGeneratorSupport;
import org.japy.kernel.types.obj.IPyInstanceDict;
import org.japy.kernel.types.obj.PyInstanceDict;
import org.japy.kernel.types.obj.proto.IPyObjWithTruth;
import org.japy.kernel.util.Args;

public final class Constants {
  static final int ASM_VERSION = Opcodes.ASM9;
  static final int JAVA_VERSION = Opcodes.V1_8;

  // Full class path is org/japy/bin/<binName>
  static final String PACKAGE_INTERNAL_NAME_PREFIX = "org/japy/bin/";

  static final String EXT_PACKAGE_PREFIX = "org/japy/bin/base/";

  public static final String FIELD_LOCAL_NAMES = "LOCAL_NAMES";
  public static final String FIELD_CELL_NAMES = "CELL_NAMES";
  public static final String FIELD_UPVALUE_NAMES = "UPVALUE_NAMES";
  static final String FIELD_SOURCE_FILE = "SOURCE_FILE";
  static final String FIELD_SOURCE_LINE = "SOURCE_LINE";

  static final String N_CELL = Type.getInternalName(Cell.class);
  static final String N_OBJECT = Type.getInternalName(Object.class);
  static final String N_IPYOBJ = Type.getInternalName(IPyObj.class);
  static final String N_JCLASSEXEC = Type.getInternalName(JClassExec.class);
  static final String N_JFRAMEFUNC = Type.getInternalName(JFrameFunc.class);
  static final String N_JGENERATORIMPL = Type.getInternalName(JGeneratorImpl.class);
  static final String N_JMODULEEXEC = Type.getInternalName(JModuleExec.class);
  static final String N_JNATIVEFUNC = Type.getInternalName(JNativeFunc.class);
  static final String N_STRING = Type.getInternalName(String.class);
  static final String N_THROWABLE = Type.getInternalName(Throwable.class);
  static final String N_IPYEXTCLASS = Type.getInternalName(IPyExtClass.class);
  static final String N_IPYBUILTINSUBCLASSINSTANCE =
      Type.getInternalName(IPyBuiltinSubClassInstance.class);
  static final String N_IPYOBJWITHTRUTH = Type.getInternalName(IPyObjWithTruth.class);
  static final String N_IPYOBJWITHTRUTHADAPTER = Type.getInternalName(IPyObjWithTruthAdapter.class);
  static final String N_IPYGENERATORSUPPORT_CONTINUE =
      Type.getInternalName(IPyGeneratorSupport.Continue.class);

  static final String D_AOBJECT = Type.getDescriptor(Object[].class);
  static final String D_ACELL = Type.getDescriptor(Cell[].class);
  static final String D_ARGS = Type.getDescriptor(Args.class);
  static final String D_ASTRING = Type.getDescriptor(String[].class);
  static final String D_CELL = Type.getDescriptor(Cell.class);
  static final String D_FRAME = Type.getDescriptor(Frame.class);
  static final String D_INT = Type.getDescriptor(int.class);
  static final String D_IPYDICT = Type.getDescriptor(IPyDict.class);
  static final String D_IPYOBJ = Type.getDescriptor(IPyObj.class);
  static final String D_PYBASEEXCEPTION = Type.getDescriptor(PyBaseException.class);
  static final String D_IPYRODICT = Type.getDescriptor(IPyRODict.class);
  static final String D_PYCONTEXT = Type.getDescriptor(PyContext.class);
  static final String D_OBJECT = Type.getDescriptor(Object.class);
  static final String D_PYFUNCHEADER = Type.getDescriptor(PyFuncHeader.class);
  static final String D_PYFUNCHEADER_BUILDER = Type.getDescriptor(PyFuncHeader.Builder.class);
  static final String D_STRING = Type.getDescriptor(String.class);
  static final String D_THROWABLE = Type.getDescriptor(Throwable.class);
  static final String D_IPYEXTCLASS = Type.getDescriptor(IPyExtClass.class);
  static final String D_PYEXTBUILTINSUBCLASS = Type.getDescriptor(PyExtBuiltinSubClass.class);
  static final String D_PYINSTANCEDICT = Type.getDescriptor(PyInstanceDict.class);

  static final Type T_ACELL = Type.getType(Cell[].class);
  static final Type T_ARGS = Type.getType(Args.class);
  static final Type T_IPYCLASS = Type.getType(IPyClass.class);
  static final Type T_CELL = Type.getType(Cell.class);
  static final Type T_FRAME = Type.getType(Frame.class);
  static final Type T_IPYDICT = Type.getType(IPyDict.class);
  static final Type T_IPYOBJ = Type.getType(IPyObj.class);
  static final Type T_IPYRODICT = Type.getType(IPyRODict.class);
  static final Type T_PYCONTEXT = Type.getType(PyContext.class);
  static final Type T_PYFUNCHEADER = Type.getType(PyFuncHeader.class);
  static final Type T_PYDICT = Type.getType(PyDict.class);
  static final Type T_PYSTR = Type.getType(PyStr.class);
  static final Type T_THROWABLE = Type.getType(Throwable.class);
  static final Type T_IPYINSTANCEDICT = Type.getType(IPyInstanceDict.class);
  static final Type T_IPYGENERATORSUPPORT_CONTINUE =
      Type.getType(IPyGeneratorSupport.Continue.class);

  static final String GEN_CLASSEXEC_METHOD = "invoke";
  static final String GEN_FRAMEFUNC_METHOD = "call";
  static final String GEN_GENERATOR_METHOD = "resumeImpl";
  static final String GEN_MODULEEXEC_METHOD = "invoke";
  static final String GEN_NATIVEFUNC_METHOD = "invoke";

  static final String MD_CLINIT = Type.getMethodDescriptor(Type.VOID_TYPE);

  static final String MD_GEN_CLASSEXEC_INIT = Type.getMethodDescriptor(Type.VOID_TYPE, T_ACELL);
  static final String MD_GEN_FRAMEFUNC_INIT =
      Type.getMethodDescriptor(Type.VOID_TYPE, T_PYFUNCHEADER);
  static final String MD_GEN_GENERATOR_INIT =
      Type.getMethodDescriptor(Type.VOID_TYPE, T_PYFUNCHEADER, T_ARGS, T_PYCONTEXT);
  static final String MD_GEN_MODULEEXEC_INIT = Type.getMethodDescriptor(Type.VOID_TYPE);
  static final String MD_GEN_NATIVEFUNC_INIT =
      Type.getMethodDescriptor(Type.VOID_TYPE, T_PYFUNCHEADER);

  static final MethodType MT_GEN_CLASSEXEC_METHOD =
      MethodType.methodType(
          void.class, IPyDict.class, IPyDict.class, IPyRODict.class, Cell.class, PyContext.class);
  static final MethodType MT_GEN_FRAMEFUNC_METHOD =
      MethodType.methodType(IPyObj.class, Frame.class, Args.class, PyContext.class);
  static final MethodType MT_GEN_GENERATOR_METHOD =
      MethodType.methodType(IPyObj.class, IPyObj.class, PyBaseException.class, PyContext.class);
  static final MethodType MT_GEN_MODULEEXEC_METHOD =
      MethodType.methodType(void.class, IPyDict.class, IPyRODict.class, PyContext.class);
  static final MethodType MT_GEN_NATIVEFUNC_METHOD =
      MethodType.methodType(IPyObj.class, Args.class, PyContext.class);

  // spotless:off

  static final String MD_SUBCLASS_INIT = Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType(PyExtBuiltinSubClass.class), T_ARGS, T_PYCONTEXT);
  static final String MD_BUILTIN_INIT_WITH_CLASS = Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType(IPyClass.class), T_ARGS, T_PYCONTEXT);
  static final String MD_BUILTIN_INIT_NO_CLASS = Type.getMethodDescriptor(Type.VOID_TYPE, T_ARGS, T_PYCONTEXT);
  static final String MD_IPYOBJ_TYPE = Type.getMethodDescriptor(T_IPYCLASS);
  static final String MD_IPYOBJ_INSTANCEDICT = Type.getMethodDescriptor(T_IPYINSTANCEDICT);
  static final String MD_IPYCALLABLE_CALL = Type.getMethodDescriptor(T_IPYOBJ, T_ARGS, T_PYCONTEXT);
  static final String MD_OBJ_CTX = Type.getMethodDescriptor(T_IPYOBJ, T_PYCONTEXT);
  static final String MD_OBJ_OBJ_CTX = Type.getMethodDescriptor(T_IPYOBJ, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_OBJ_OBJ2_CTX = Type.getMethodDescriptor(T_IPYOBJ, T_IPYOBJ, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_OBJ_OBJ3_CTX = Type.getMethodDescriptor(T_IPYOBJ, T_IPYOBJ, T_IPYOBJ, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_VOID_CTX = Type.getMethodDescriptor(Type.VOID_TYPE, T_PYCONTEXT);
  static final String MD_VOID_OBJ_CTX = Type.getMethodDescriptor(Type.VOID_TYPE, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_VOID_OBJ2_CTX = Type.getMethodDescriptor(Type.VOID_TYPE, T_IPYOBJ, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_VOID_OBJ3_CTX = Type.getMethodDescriptor(Type.VOID_TYPE, T_IPYOBJ, T_IPYOBJ, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_BOOL_CTX = Type.getMethodDescriptor(Type.BOOLEAN_TYPE, T_PYCONTEXT);
  static final String MD_BOOL_OBJ_CTX = Type.getMethodDescriptor(Type.BOOLEAN_TYPE, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_BOOL_OBJ2_CTX = Type.getMethodDescriptor(Type.BOOLEAN_TYPE, T_IPYOBJ, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_BOOL_OBJ3_CTX = Type.getMethodDescriptor(Type.BOOLEAN_TYPE, T_IPYOBJ, T_IPYOBJ, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_INT_CTX = Type.getMethodDescriptor(Type.INT_TYPE, T_PYCONTEXT);
  static final String MD_INT_OBJ_CTX = Type.getMethodDescriptor(Type.INT_TYPE, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_INT_OBJ2_CTX = Type.getMethodDescriptor(Type.INT_TYPE, T_IPYOBJ, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_INT_OBJ3_CTX = Type.getMethodDescriptor(Type.INT_TYPE, T_IPYOBJ, T_IPYOBJ, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_PYSTR_CTX = Type.getMethodDescriptor(T_PYSTR, T_PYCONTEXT);
  static final String MD_PYSTR_OBJ_CTX = Type.getMethodDescriptor(T_PYSTR, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_PYSTR_OBJ2_CTX = Type.getMethodDescriptor(T_PYSTR, T_IPYOBJ, T_IPYOBJ, T_PYCONTEXT);
  static final String MD_PYSTR_OBJ3_CTX = Type.getMethodDescriptor(T_PYSTR, T_IPYOBJ, T_IPYOBJ, T_IPYOBJ, T_PYCONTEXT);

  // spotless:on

  static final String SUBCLASS_FIELD_TYPE = "type";
  static final String SUBCLASS_FIELD_DICT = "dict";
}
