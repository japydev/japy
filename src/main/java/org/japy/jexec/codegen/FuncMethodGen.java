package org.japy.jexec.codegen;

import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CLocalVar;

public abstract class FuncMethodGen extends MethodGen<CFunc> {
  protected FuncMethodGen(MethInfo<CFunc> methInfo, MethodVisitor mv, CodegenConfig config) {
    super(methInfo, mv, config);
  }

  protected abstract void loadArgs();

  @Override
  protected void pushUpvalues() {
    mw.thisDot(InstanceField.PYFUNC_HEADER, InstanceField.PYFUNCHEADER_UPVALUES);
  }

  @Override
  protected void pushConstants() {
    mw.thisDot(InstanceField.JFUNC_CONSTANTS);
  }

  @Override
  protected void pushGlobals() {
    mw.thisDot(InstanceField.PYFUNC_HEADER, InstanceField.PYFUNCHEADER_GLOBALS);
  }

  @Override
  protected void pushBuiltins() {
    mw.thisDot(InstanceField.PYFUNC_HEADER, InstanceField.PYFUNCHEADER_BUILTINS);
  }

  @Override
  protected void checkLocalOnStack(CLocalVar var) {
    mw.pushThis();
    mw.swap();
    mw.push(var.index);
    pushContext();
    if (var.isCell) {
      mw.call(InstanceMethod.JFUNC_GETCELLCHECKED);
    } else {
      mw.call(InstanceMethod.JFUNC_GETLOCALCHECKED);
    }
  }

  private void parseArgs(InstanceMethod parseMethod) {
    mw.thisDot(InstanceField.PYFUNC_HEADER, InstanceField.PYFUNCHEADER_ARGPARSER);
    loadArgs();
    mw.push(block.name);
    pushContext();
    mw.call(parseMethod);
  }

  @Override
  protected void parseArgs() {
    parseArgs(InstanceMethod.ARGPARSER_PARSE);
  }

  @Override
  protected void parseArgs1() {
    parseArgs(InstanceMethod.ARGPARSER_PARSE1);
  }

  @Override
  protected void parseArgs0() {
    loadArgs();
    mw.push(block.name);
    pushContext();
    mw.call(StaticMethod.ARGPARSER_VERIFYNOARGS);
  }
}
