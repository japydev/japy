package org.japy.jexec.codegen;

import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;

import org.japy.compiler.code.CBlock;
import org.japy.compiler.code.CTempVar;
import org.japy.compiler.code.CVarType;
import org.japy.compiler.impl.VarTypeInfo;
import org.japy.infra.validation.Debug;

final class JvmTempHandler implements TempHandler, Opcodes {
  private final MethodGen<?> gen;
  private final MethodWriter mw;
  private final int firstTemp;
  private final @Nullable Label[] tempLabels;

  JvmTempHandler(MethodGen<?> gen, CBlock block, int firstTemp) {
    this.gen = gen;
    this.mw = gen.mw;
    this.firstTemp = firstTemp;
    this.tempLabels = new Label[block.tempCount];
  }

  private int localIndex(CTempVar var) {
    return firstTemp + var.index;
  }

  private void loadLocal(CTempVar var) {
    mw.loadLocal(localIndex(var));
  }

  @Override
  public void load(CTempVar var) {
    if (var.isCell) {
      loadLocal(var);
      mw.getField(InstanceField.CELL_VALUE);
    } else {
      VarTypeInfo ti = VarTypeInfo.get(var.type);
      if (ti.isPrimitive()) {
        switch (ti.primitiveType()) {
          case BOOL:
          case INT:
            mw.visitVarInsn(ILOAD, localIndex(var));
            break;
        }
      } else {
        loadLocal(var);
      }
    }
  }

  @Override
  public void loadTempCell(int index) {
    mw.loadLocal(firstTemp + index);
  }

  @Override
  public void beginTemp(CTempVar var) {
    if (Debug.ENABLED) {
      dcheck(var.index < tempLabels.length);
      dcheck(tempLabels[var.index] == null);
    }

    tempLabels[var.index] = mw.mark();

    if (var.isCell) {
      mw.storeLocal(localIndex(var), () -> mw.createObj(InstanceMethod.CELL_INIT_NULL));
    }
  }

  @Override
  public void initTemp(CTempVar var) {
    VarTypeInfo ti = VarTypeInfo.get(var.type);
    if (ti.isPrimitive()) {
      switch (ti.primitiveType()) {
        case INT:
        case BOOL:
          mw.push(0);
          mw.visitVarInsn(ISTORE, localIndex(var));
          break;
      }
    } else if (!var.isCell) {
      mw.storeLocalNull(localIndex(var));
    }
  }

  private static String varTypeDescriptor(CTempVar var) {
    if (var.isCell) {
      dcheck(var.type == CVarType.PYOBJ);
      return Constants.D_CELL;
    }
    return VarTypeInfo.get(var.type).descriptor;
  }

  @Override
  public void endTemp(CTempVar var) {
    Label start = tempLabels[var.index];
    Label end = gen.labelForLocalVar();
    if (start.getOffset() < end.getOffset()) {
      mw.visitLocalVariable(
          var.name,
          varTypeDescriptor(var),
          null,
          dcheckNotNull(tempLabels[var.index]),
          end,
          localIndex(var));
    }
    tempLabels[var.index] = null;
  }

  @Override
  public void store(CTempVar var) {
    if (var.isCell) {
      mw.setFieldFromStack(() -> loadLocal(var), InstanceField.CELL_VALUE);
    } else {
      VarTypeInfo ti = VarTypeInfo.get(var.type);
      if (!ti.isPrimitive()) {
        mw.storeLocal(localIndex(var));
      } else {
        switch (ti.primitiveType()) {
          case BOOL:
          case INT:
            mw.visitVarInsn(ISTORE, localIndex(var));
            break;
        }
      }
    }
  }

  @Override
  public void del(CTempVar var) {
    mw.storeLocalNull(localIndex(var));
  }
}
