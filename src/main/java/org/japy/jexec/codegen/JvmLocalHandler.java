package org.japy.jexec.codegen;

import static org.japy.infra.validation.Debug.dcheck;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;

import org.japy.compiler.code.CLocalVar;

final class JvmLocalHandler implements LocalHandler, Opcodes {
  private final MethodGen<?> gen;
  private final MethodWriter mw;
  private final int firstVar;
  private final int firstCell;
  private @Nullable Label localStart;

  JvmLocalHandler(MethodGen<?> gen, int firstVar, int firstCell) {
    this.gen = gen;
    this.mw = gen.mw;
    this.firstVar = firstVar;
    this.firstCell = firstCell;
  }

  private int localIndex(CLocalVar var) {
    return (var.isCell ? firstCell : firstVar) + var.index;
  }

  private void loadLocal(CLocalVar var) {
    mw.loadLocal(localIndex(var));
  }

  @Override
  public void init(CLocalVar var) {
    if (var.isCell) {
      dcheck(firstCell >= 0);
      mw.storeLocal(localIndex(var), () -> mw.createObj(InstanceMethod.CELL_INIT_NULL));
    } else {
      dcheck(firstVar >= 0);
      mw.storeLocalNull(localIndex(var));
    }
  }

  @Override
  public void begin(CLocalVar var) {
    if (localStart == null) {
      localStart = mw.mark();
    }
  }

  @Override
  public void finish(CLocalVar var, Label last) {
    mw.visitLocalVariable(
        var.name, varTypeDescriptor(var), null, localStart, last, localIndex(var));
  }

  @Override
  public void load(CLocalVar var, boolean raiseIfNotBound) {
    loadLocal(var);
    if (var.isCell) {
      mw.getField(InstanceField.CELL_VALUE);
    }
    if (raiseIfNotBound) {
      gen.checkLocalOnStackIfNeeded(var);
    }
  }

  private static String varTypeDescriptor(CLocalVar var) {
    if (var.isCell) {
      return Constants.D_CELL;
    } else {
      return Constants.D_IPYOBJ;
    }
  }

  @Override
  public void store(CLocalVar var) {
    if (var.isCell) {
      mw.setFieldFromStack(() -> loadLocal(var), InstanceField.CELL_VALUE);
    } else {
      mw.storeLocal(localIndex(var));
    }
  }

  @Override
  public void del(CLocalVar var, boolean raiseIfNotBound) {
    int localIndex = localIndex(var);
    if (var.isCell) {
      mw.loadLocal(localIndex);
      if (raiseIfNotBound) {
        mw.dot(mw::dup, InstanceField.CELL_VALUE);
        gen.popAndCheckBoundLocal(var);
      }
      mw.putFieldNull(InstanceField.CELL_VALUE);
    } else {
      if (raiseIfNotBound) {
        mw.loadLocal(localIndex);
        gen.popAndCheckBoundLocal(var);
      }
      mw.storeLocalNull(localIndex);
    }
  }

  @Override
  public void loadCell(int index) {
    mw.loadLocal(firstCell + index);
  }
}
