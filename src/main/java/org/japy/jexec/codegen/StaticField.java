package org.japy.jexec.codegen;

import static org.japy.infra.util.ExcUtil.wrap;
import static org.japy.infra.validation.Debug.dcheck;
import static org.japy.infra.validation.Debug.dcheckNotNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.objectweb.asm.Type;

import org.japy.base.AugOp;
import org.japy.base.BinOp;
import org.japy.base.UnaryOp;
import org.japy.infra.coll.StrUtil;
import org.japy.infra.validation.Debug;
import org.japy.kernel.exec.Cell;
import org.japy.kernel.types.coll.seq.PyTuple;
import org.japy.kernel.types.coll.str.PyBytes;
import org.japy.kernel.types.coll.str.PyStr;
import org.japy.kernel.types.exc.py.PyBuiltinExc;
import org.japy.kernel.types.misc.PyEllipsis;
import org.japy.kernel.types.misc.PyNone;
import org.japy.kernel.types.num.PyBool;
import org.japy.kernel.types.num.PyInt;
import org.japy.kernel.util.ArgParser;
import org.japy.kernel.util.PyConstants;
import org.japy.kernel.util.Signature;

@SuppressWarnings("ImmutableEnumChecker")
public enum StaticField {
  // spotless:off

  ARGPARSER_NOPARAMS(wrap(() -> ArgParser.class.getField("NO_PARAMS"))),

  AUGOP_IADD(wrap(() -> AugOp.class.getField("IADD"))),
  AUGOP_ISUB(wrap(() -> AugOp.class.getField("ISUB"))),
  AUGOP_IMUL(wrap(() -> AugOp.class.getField("IMUL"))),
  AUGOP_IDIV(wrap(() -> AugOp.class.getField("ITRUEDIV"))),
  AUGOP_IFLOORDIV(wrap(() -> AugOp.class.getField("IFLOORDIV"))),
  AUGOP_IMOD(wrap(() -> AugOp.class.getField("IMOD"))),
  AUGOP_IPOW(wrap(() -> AugOp.class.getField("IPOW"))),
  AUGOP_IBAND(wrap(() -> AugOp.class.getField("IAND"))),
  AUGOP_IBOR(wrap(() -> AugOp.class.getField("IOR"))),
  AUGOP_IXOR(wrap(() -> AugOp.class.getField("IXOR"))),
  AUGOP_ILSHIFT(wrap(() -> AugOp.class.getField("ILSHIFT"))),
  AUGOP_IRSHIFT(wrap(() -> AugOp.class.getField("IRSHIFT"))),
  AUGOP_IMATMUL(wrap(() -> AugOp.class.getField("IMATMUL"))),

  BINOP_ADD(wrap(() -> BinOp.class.getField("ADD"))),
  BINOP_SUB(wrap(() -> BinOp.class.getField("SUB"))),
  BINOP_MUL(wrap(() -> BinOp.class.getField("MUL"))),
  BINOP_DIV(wrap(() -> BinOp.class.getField("TRUEDIV"))),
  BINOP_FLOORDIV(wrap(() -> BinOp.class.getField("FLOORDIV"))),
  BINOP_MOD(wrap(() -> BinOp.class.getField("MOD"))),
  BINOP_POW(wrap(() -> BinOp.class.getField("POW"))),
  BINOP_BAND(wrap(() -> BinOp.class.getField("AND"))),
  BINOP_BOR(wrap(() -> BinOp.class.getField("OR"))),
  BINOP_XOR(wrap(() -> BinOp.class.getField("XOR"))),
  BINOP_LSHIFT(wrap(() -> BinOp.class.getField("LSHIFT"))),
  BINOP_RSHIFT(wrap(() -> BinOp.class.getField("RSHIFT"))),
  BINOP_MATMUL(wrap(() -> BinOp.class.getField("MATMUL"))),
  BINOP_DIVMOD(wrap(() -> BinOp.class.getField("DIVMOD"))),

  CELL_EMPTYARRAY(wrap(() -> Cell.class.getField("EMPTY_ARRAY"))),

  PYBOOL_FALSE(wrap(() -> PyBool.class.getField("False"))),
  PYBOOL_TRUE(wrap(() -> PyBool.class.getField("True"))),
  PYBYTES_EMPTY(wrap(() -> PyBytes.class.getField("EMPTY"))),
  PYSTR_EMPTY(wrap(() -> PyStr.class.getField("EMPTY_STRING"))),
  PYTUPLE_EMPTY(wrap(() -> PyTuple.class.getField("EMPTY_TUPLE"))),
  PYELLIPSIS_ELLIPSIS(wrap(() -> PyEllipsis.class.getField("Ellipsis"))),
  PYNONE_NONE(wrap(() -> PyNone.class.getField("None"))),
  PYINT_ZERO(wrap(() -> PyInt.class.getField("ZERO"))),
  PYINT_ONE(wrap(() -> PyInt.class.getField("ONE"))),
  PYINT_NEGATIVEONE(wrap(() -> PyInt.class.getField("NEGATIVE_ONE"))),

  PYCONSTANTS_ANNOTATIONS(wrap(() -> PyConstants.class.getField("__ANNOTATIONS__"))),

  PYEXCEPTIONTYPE_BUILTIN_STOPASYNCITERATION(wrap(() -> PyBuiltinExc.class.getField("StopAsyncIteration"))),

  SIGNATURE_NOPARAMS(wrap(() -> Signature.class.getField("NO_PARAMS"))),

  STRUTIL_EMPTYSTRINGARRAY(wrap(() -> StrUtil.class.getField("EMPTY_STRING_ARRAY"))),

  UNOP_NEG(wrap(() -> UnaryOp.class.getField("NEG"))),
  UNOP_POS(wrap(() -> UnaryOp.class.getField("POS"))),
  UNOP_INVERT(wrap(() -> UnaryOp.class.getField("INVERT"))),
  UNOP_ABS(wrap(() -> UnaryOp.class.getField("ABS"))),

  ;
  // spotless:on

  public static StaticField binOp(BinOp op) {
    return BIN_OPS[op.ordinal()];
  }

  public static StaticField unaryOp(UnaryOp op) {
    return UN_OPS[op.ordinal()];
  }

  private static final StaticField[] BIN_OPS;
  private static final StaticField[] UN_OPS;

  static {
    BIN_OPS = new StaticField[BinOp.values().length];
    BIN_OPS[BinOp.ADD.ordinal()] = BINOP_ADD;
    BIN_OPS[BinOp.SUB.ordinal()] = BINOP_SUB;
    BIN_OPS[BinOp.MUL.ordinal()] = BINOP_MUL;
    BIN_OPS[BinOp.TRUEDIV.ordinal()] = BINOP_DIV;
    BIN_OPS[BinOp.FLOORDIV.ordinal()] = BINOP_FLOORDIV;
    BIN_OPS[BinOp.MOD.ordinal()] = BINOP_MOD;
    BIN_OPS[BinOp.POW.ordinal()] = BINOP_POW;
    BIN_OPS[BinOp.AND.ordinal()] = BINOP_BAND;
    BIN_OPS[BinOp.OR.ordinal()] = BINOP_BOR;
    BIN_OPS[BinOp.XOR.ordinal()] = BINOP_XOR;
    BIN_OPS[BinOp.LSHIFT.ordinal()] = BINOP_LSHIFT;
    BIN_OPS[BinOp.RSHIFT.ordinal()] = BINOP_RSHIFT;
    BIN_OPS[BinOp.MATMUL.ordinal()] = BINOP_MATMUL;
    BIN_OPS[BinOp.DIVMOD.ordinal()] = BINOP_DIVMOD;

    UN_OPS = new StaticField[UnaryOp.values().length];
    UN_OPS[UnaryOp.NEG.ordinal()] = UNOP_NEG;
    UN_OPS[UnaryOp.POS.ordinal()] = UNOP_POS;
    UN_OPS[UnaryOp.INVERT.ordinal()] = UNOP_INVERT;
    UN_OPS[UnaryOp.ABS.ordinal()] = UNOP_ABS;

    if (Debug.ENABLED) {
      for (int i = 0; i != BIN_OPS.length; ++i) {
        dcheckNotNull(BIN_OPS[i]);
      }
      for (int i = 0; i != UN_OPS.length; ++i) {
        dcheckNotNull(UN_OPS[i]);
      }
    }
  }

  public final String name;
  public final String ownerName;
  public final String descriptor;
  public final Type type;

  StaticField(Field field) {
    dcheck(Modifier.isStatic(field.getModifiers()));
    name = field.getName();
    ownerName = Type.getInternalName(field.getDeclaringClass());
    type = Type.getType(field.getType());
    descriptor = type.getDescriptor();
  }
}
