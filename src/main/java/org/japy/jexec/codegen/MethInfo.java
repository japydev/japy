package org.japy.jexec.codegen;

import java.lang.invoke.MethodType;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CBlock;
import org.japy.compiler.code.CLocalVar;
import org.japy.compiler.code.CUpvalue;

abstract class MethInfo<T extends CBlock> {
  public enum Kind {
    CLASS,
    MODULE,
    NATIVE_FUNC,
    FRAME_FUNC,
    GENERATOR,
  }

  public final Kind kind;
  public final T block;
  public final String internalClassName;

  protected MethInfo(Kind kind, T block) {
    this.kind = kind;
    this.block = block;
    this.internalClassName = Constants.PACKAGE_INTERNAL_NAME_PREFIX + block.binName;
  }

  abstract String baseClassName();

  CUpvalue @Nullable [] upvalues() {
    return null;
  }

  CLocalVar @Nullable [] locals() {
    return null;
  }

  CLocalVar @Nullable [] cells() {
    return null;
  }

  void declareFields(@SuppressWarnings("unused") ClassVisitor cv) {}

  abstract void writeInit(MethodWriter mw);

  abstract void declareConstructorParameters(MethodVisitor mv, Label start, Label end);

  abstract String initDescriptor();

  abstract String methodName();

  abstract MethodType methodType();

  abstract MethodGen<?> getMethodGen(MethodVisitor mv, CodegenConfig config);
}
