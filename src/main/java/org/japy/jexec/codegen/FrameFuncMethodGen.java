package org.japy.jexec.codegen;

import static org.japy.infra.validation.Debug.dcheck;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CFunc;
import org.japy.infra.validation.Debug;

// IPyObj call(Frame frame, Args args, PyContext ctx)
final class FrameFuncMethodGen extends FuncMethodGen {
  private static final int IFRAME = 1;
  private static final int IARGS = 2;
  private static final int ICTX = 3;
  private static final int IFIRSTLOCAL = 4;

  FrameFuncMethodGen(MethInfo<CFunc> methInfo, MethodVisitor mv, CodegenConfig config) {
    super(methInfo, mv, config);
  }

  @Override
  protected void declareMethodParameters(MethodVisitor mv, Label start, Label end) {
    mv.visitLocalVariable("frame", Constants.D_FRAME, null, start, end, IFRAME);
    mv.visitLocalVariable("args", Constants.D_ARGS, null, start, end, IARGS);
    mv.visitLocalVariable("ctx", Constants.D_PYCONTEXT, null, start, end, ICTX);
  }

  @Override
  protected void loadArgs() {
    mw.loadLocal(IARGS);
  }

  @Override
  protected void pushContext() {
    mw.loadLocal(ICTX);
  }

  @Override
  protected LocalHandler createLocalHandler() {
    return new FrameLocalHandler(this, () -> mw.loadLocal(IFRAME));
  }

  @Override
  protected TempHandler createTempHandler() {
    return new JvmTempHandler(this, block, IFIRSTLOCAL);
  }

  @Override
  protected void beginMethod() {
    super.beginMethod();

    if (Debug.ENABLED) {
      dcheck(!block.isGeneratorExpr() || block.isGenerator());
    }
  }
}
