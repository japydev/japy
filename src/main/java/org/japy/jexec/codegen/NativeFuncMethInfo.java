package org.japy.jexec.codegen;

import java.lang.invoke.MethodType;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CFunc;
import org.japy.compiler.code.CLocalVar;
import org.japy.compiler.code.CUpvalue;

final class NativeFuncMethInfo extends MethInfo<CFunc> {
  // <init>(PyFuncHeader header)
  private static final int ICONSTR_HEADER = 1;

  public NativeFuncMethInfo(CFunc func) {
    super(Kind.NATIVE_FUNC, func);
  }

  @Override
  String baseClassName() {
    return Constants.N_JNATIVEFUNC;
  }

  @Override
  CUpvalue @Nullable [] upvalues() {
    return block.upvalues;
  }

  @Override
  CLocalVar @Nullable [] locals() {
    return block.locals;
  }

  @Override
  CLocalVar @Nullable [] cells() {
    return block.cells;
  }

  @Override
  void writeInit(MethodWriter mw) {
    // <init>(PyFuncHeader header)
    //   -> JNativeFunc(header)
    mw.callThisDot(InstanceMethod.JNATIVEFUNC_INIT, () -> mw.loadLocal(ICONSTR_HEADER));
  }

  @Override
  void declareConstructorParameters(MethodVisitor mv, Label start, Label end) {
    mv.visitLocalVariable("header", Constants.D_PYFUNCHEADER, null, start, end, ICONSTR_HEADER);
  }

  @Override
  String initDescriptor() {
    return Constants.MD_GEN_NATIVEFUNC_INIT;
  }

  @Override
  String methodName() {
    return Constants.GEN_NATIVEFUNC_METHOD;
  }

  @Override
  MethodType methodType() {
    return Constants.MT_GEN_NATIVEFUNC_METHOD;
  }

  @Override
  MethodGen<?> getMethodGen(MethodVisitor mv, CodegenConfig config) {
    return new NativeFuncMethodGen(this, mv, config);
  }
}
