package org.japy.jexec.codegen;

import static org.japy.infra.validation.Debug.dcheckNotNull;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.util.CheckClassAdapter;

import org.japy.infra.validation.Debug;

public class ClassGenHelper implements Opcodes {
  private final ClassWriter classWriter;

  public final ClassVisitor cv;

  public final String thisClassName;
  public final String baseClassName;
  public final Class<?> baseClass;

  public ClassGenHelper(String thisClassName, Class<?> baseClass, String[] interfaces) {
    classWriter = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
    if (Debug.ENABLED) {
      cv = new CheckClassAdapter(classWriter, false);
    } else {
      cv = classWriter;
    }

    this.thisClassName = thisClassName;
    this.baseClass = baseClass;
    this.baseClassName = Type.getInternalName(baseClass);

    cv.visit(
        Constants.JAVA_VERSION,
        ACC_PUBLIC | ACC_FINAL | ACC_SUPER,
        thisClassName,
        null,
        baseClassName,
        interfaces);
  }

  public byte[] finishClass() {
    cv.visitEnd();
    return classWriter.toByteArray();
  }

  public MethodWriter beginMethod(String name, String descriptor) {
    MethodVisitor mv = cv.visitMethod(Opcodes.ACC_PUBLIC, name, descriptor, null, null);
    MethodWriter mw = new MethodWriter(mv, false);
    mw.visitCode();
    return mw;
  }

  public void finishMethod(MethodWriter mw) {
    dcheckNotNull(mw);
    mw.visitMaxs(0, 0);
    mw.visitEnd();
  }
}
