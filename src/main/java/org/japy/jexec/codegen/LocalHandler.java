package org.japy.jexec.codegen;

import org.objectweb.asm.Label;

import org.japy.compiler.code.CLocalVar;

interface LocalHandler {
  void init(CLocalVar var);

  void load(CLocalVar var, boolean raiseIfNotBound);

  void store(CLocalVar var);

  void del(CLocalVar var, boolean raiseIfNotBound);

  void loadCell(int index);

  void begin(CLocalVar var);

  void finish(CLocalVar var, Label last);
}
