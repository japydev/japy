package org.japy.jexec.codegen;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceClassVisitor;

public class ClassDumper {
  public static void writeClassFile(byte[] code, Path file) {
    try {
      Files.write(file, code);
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    }
  }

  public static void writeClassCode(byte[] code, Path file) {
    try (PrintWriter writer = new PrintWriter(file.toFile(), StandardCharsets.UTF_8)) {
      ClassReader classReader = new ClassReader(code);
      TraceClassVisitor cv = new TraceClassVisitor(null, new InsnNumPrinter(), writer);
      classReader.accept(cv, 0);
    } catch (IOException ex) {
      throw new UncheckedIOException(ex);
    }
  }

  private static class InsnNumPrinter extends Textifier {
    private int insn;

    InsnNumPrinter() {
      super(Constants.ASM_VERSION);
    }

    @Override
    protected Textifier createTextifier() {
      return new InsnNumPrinter();
    }

    private void addInsnNumber() {
      text.add(String.format("%03d:", ++insn));
    }

    @Override
    public void visitInsn(int opcode) {
      addInsnNumber();
      super.visitInsn(opcode);
    }

    @Override
    public void visitTableSwitchInsn(int min, int max, Label dflt, Label... labels) {
      addInsnNumber();
      super.visitTableSwitchInsn(min, max, dflt, labels);
    }

    @Override
    public void visitIntInsn(int opcode, int operand) {
      addInsnNumber();
      super.visitIntInsn(opcode, operand);
    }

    @Override
    public void visitIincInsn(int var, int increment) {
      addInsnNumber();
      super.visitIincInsn(var, increment);
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
      addInsnNumber();
      super.visitFieldInsn(opcode, owner, name, descriptor);
    }

    @Override
    public void visitInvokeDynamicInsn(
        String name,
        String descriptor,
        Handle bootstrapMethodHandle,
        Object... bootstrapMethodArguments) {
      addInsnNumber();
      super.visitInvokeDynamicInsn(
          name, descriptor, bootstrapMethodHandle, bootstrapMethodArguments);
    }

    @Override
    public void visitJumpInsn(int opcode, Label label) {
      addInsnNumber();
      super.visitJumpInsn(opcode, label);
    }

    @Override
    public void visitLdcInsn(Object value) {
      addInsnNumber();
      super.visitLdcInsn(value);
    }

    @Override
    public void visitLookupSwitchInsn(Label dflt, int[] keys, Label[] labels) {
      addInsnNumber();
      super.visitLookupSwitchInsn(dflt, keys, labels);
    }

    @Override
    public void visitMethodInsn(
        int opcode, String owner, String name, String descriptor, boolean isInterface) {
      addInsnNumber();
      super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
    }

    @Override
    public void visitMultiANewArrayInsn(String descriptor, int numDimensions) {
      addInsnNumber();
      super.visitMultiANewArrayInsn(descriptor, numDimensions);
    }

    @Override
    public void visitTypeInsn(int opcode, String type) {
      addInsnNumber();
      super.visitTypeInsn(opcode, type);
    }

    @Override
    public void visitVarInsn(int opcode, int var) {
      addInsnNumber();
      super.visitVarInsn(opcode, var);
    }
  }

  //  private static class PrintVisitor extends ClassVisitor {
  //    public PrintVisitor(int api, ClassVisitor cv) {
  //      super(api, cv);
  //    }
  //
  //    @Override
  //    public MethodVisitor visitMethod(int access, String name, String desc, String signature,
  // String[] exceptions) {
  //      MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
  //      Printer p = new Textifier(Constants.ASM_VERSION) {
  //        @Override
  //        public void visitMethodEnd() {
  //          print(new PrintWriter(System.out)); // print it after it has been visited
  //        }
  //      };
  //      return new TraceMethodVisitor(mv, p);
  //    }
  //  }
}
