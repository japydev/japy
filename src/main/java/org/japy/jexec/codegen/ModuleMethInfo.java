package org.japy.jexec.codegen;

import java.lang.invoke.MethodType;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import org.japy.compiler.code.CModule;

final class ModuleMethInfo extends MethInfo<CModule> {
  public ModuleMethInfo(CModule module) {
    super(Kind.MODULE, module);
  }

  @Override
  String baseClassName() {
    return Constants.N_JMODULEEXEC;
  }

  @Override
  void writeInit(MethodWriter mw) {
    // <>()
    // -> JModuleExec()
    mw.callThisDot(InstanceMethod.JMODULEEXEC_INIT);
  }

  @Override
  void declareConstructorParameters(MethodVisitor mv, Label start, Label end) {}

  @Override
  String initDescriptor() {
    return Constants.MD_GEN_MODULEEXEC_INIT;
  }

  @Override
  String methodName() {
    return Constants.GEN_MODULEEXEC_METHOD;
  }

  @Override
  MethodType methodType() {
    return Constants.MT_GEN_MODULEEXEC_METHOD;
  }

  @Override
  ModuleMethodGen getMethodGen(MethodVisitor mv, CodegenConfig config) {
    return new ModuleMethodGen(this, mv, config);
  }
}
